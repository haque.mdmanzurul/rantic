var mongoose = require('mongoose');

var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    moment = require('moment'),
    crypto = require('crypto'),
    config = require('server/config/config');


mongoose.connect(
    config.db,
    {}, function(err) {
    	console.log(err);
    }
);

var Comments = new Schema({
    text      : String,
    date      : Date,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Order Schema
 */
var OrderSchema = new Schema({
    topup_payment: {
        type: String
    },
    topup: {
        type: Boolean,
        default: false
    },
    order_tmp: {
        type: Number,
        default: 0
    },
    manual_verify: {
        type: Boolean,
        default: false
    },
    api_type: { type: String },
    from_api: {
        type: Boolean,
        default: false
    },
    emoticon: {
        type: String
    },
    eat_speed: {
        type: String
    },
    ig_mentions: {
        type: String
    },
    ig_media: {
        type: String
    },
    followers: {
        type: String
    },
     hashtag: {
        type: String
     },
     ig_comments_emoticon: {
        type: String
     },
     ig_comments: {
        type: String
     },
     transaction: {
        type: String
     },
     sale_id: {
        type: String
     },
     email_resend: {
        type: Boolean,
        default: false
     },
     last_updated: {
        type: Date,
        default: moment().add(1,'months')
     },
     payment_type: {
        type: String,
        default: ''
     },
     speed_service: {
        type: Boolean,
        default: false
     },
     verify_id: {
        type: String,
        default: ''
     },
     recurring: {
        type: Boolean,
        default: false
    },
    api_id:{
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
    url: {
        type: String,
        default: '',
        trim: true
    },
    type: {
        type: String,
        default: '',
        trim: true
    },
    old: {
	type: Boolean,
	default: false
    },
    completed: {
        type: Boolean,
        default: false
    },
    verified: {
        type: Boolean,
        default: false
    },
    cancelled: {
        type: Boolean,
        default: false
    },
    paid: {
        type: Boolean,
        default: false
    },
    checkout: {
        type: Boolean,
        default: false
    },
    reason: {
        type: String,
        default: '',
        trim: true
    },
    service: {
        label: {
            type: String,
            default: ''
        },
        value: {
            type: String,
            default: ''
        },
        multiplier: {
            type: Number,
            default: 0
        },
        price: {
            type: Number,
            default: 0
        }
    },
    discount: {
      code: {
        type: String,
        trim: true
      },
      type: {
        type: String,
        trim: true
      },
      percent: {
        type: Number,
        default: 0
      },
      price: {
        type: Number,
        default: 0
      },
      services: {}
    },
    countries: [{
        type: String,
        default: ''
    }],
    iframely: {},
    quantity: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        default: 0
    },
    orderCount: {
        type: Number,
        default: 0
    },
    beforeCount: {
        type: Number,
        default: 0
    },
    afterCount: {
        type: Number,
        default: 0
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    provider: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    payment: {
        type: Schema.ObjectId,
        ref: 'Payment'
    },
    comments  : [Comments],
    unread : {
      customer: {
        type: Number,
        default: 0
      },
      provider: {
        type: Number,
        default: 0
      },
      administrator: {
        type: Number,
        default: 0
      }
    }
});






var Packages = new Schema({
  price: 'Number',
  quantity: 'Number',
  label: 'String'
});

/**
 * Service Schema
 */
var ServiceSchema = new Schema({
  priority: {
  	type: Number
  },
  min_quantity: {
    type: Number
  },
  fixed_price: {
    type: Number
  },
  dropdown: {
    type: String
  },
  discount_id: {
    type: String
  },
  discount: {
    type: Boolean,
    default: false
  },
  discount_settings: {
    type: String
  },
  api_nr: { type: String },
  api_price: { type: Number},
  api_type: { type: String },
  api_code: { type: String },
  description: 'String',
  pattern: 'String',
  label: 'String',
  value: 'String',
  category: 'String',
  multiplier: 'Number',
  price: 'Number',
  minimum: 'Number',
  targeted: 'Boolean',
  disabled: 'Boolean',
  recurring: {
    'type': 'Boolean',
    'default': false
  },
  faster_speed: {
    type: Boolean,
    default: false
  },
  countries: [{
      type: String,
      default: ''
  }],
  packages: [Packages]
});

mongoose.model('Service', ServiceSchema);




  

    var Service = mongoose.model('Service');



 mongoose.model('Order', OrderSchema);


var Order = mongoose.model('Order');

Order.aggregate({ $group: { _id: "$service.value", total_price: { $sum: 1 } } }, function (err, result) {

	async.eachSeries(result, function(item, cb) {
	
		if(item._id) {
		Service.update({value: item._id}, { $set: { priority: parseInt(item.total_price) }}, function(err) {
			console.log(item);
			return cb();
		});
		} else return cb();
		
	}, function done() {
		console.log("DONE!")
	})
       
    });



