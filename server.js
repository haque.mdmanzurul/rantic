'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var passport = require('passport');
var logger = require('mean-logger');
var kue = require('kue');
var jobs = kue.createQueue();
var cors = require('cors')
var checkout = require('2co');
var heapdump = require('heapdump');
var path = require('path');
var express = require('express'), MongoStore = require('connect-mongo')(express);
global.fullPath = path.resolve(__dirname);


/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Initializing system variables
var config = require('./server/config/config');
console.log(config.db);
mongoose.Promise = require('bluebird');
//var db = mongoose.connect(config.db);

// connect to MongoDB

mongoose.connect(config.db, { useUnifiedTopology: true, useNewUrlParser: true , });
var db = mongoose.connection;
// When successfully connected
mongoose.connection.on('connected', function () {
    //db.getUser('ranticapp');

    //db.getUser('ranticapp');
    console.log('Mongoose default connection open to ' + config.db);
});


var app = require('./server/config/system/bootstrap')(passport, mongoose.connection);

// Express/Mongo session storage
// jobs.promote();
app.use(cors());
app.use(kue.app);

// Start the app by listening on <port>
app.listen(config.port);
console.log('Mean app started on port ' + config.port + ' (' + process.env.NODE_ENV + ')');
// Start the kue manager application
//kue.app.listen(6179);
//kue.app.set('title', 'Rantic Panel');
//console.log('Kue started on port 6179');

// Initializing logger
logger.init(app, passport, mongoose);
// Expose app
exports = module.exports = app;

//now mongo / mongoose is set up, boot the app...







