'use strict';

var paths = {
    js: ['*.js', 'server/**/*.js', 'public/**/*.js', 'test/**/*.js', '!test/coverage/**', '!public/system/lib/**'],
    html: ['public/**/views/**', 'server/views/**'],
    css: ['public/**/css/**', '!public/system/lib/**']
};

module.exports = function(grunt) {

    if (process.env.NODE_ENV !== 'production') {
        require('time-grunt')(grunt);
    }

    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        assets: grunt.file.readJSON('server/config/assets.json'),
        ngtemplates:  {
            /*
          'mean.auth': {
            //cwd: 'public',
            src: ['public/auth/views/*.html', '!public/auth/views/index.html'],
            dest: 'public/build/partials/partials.auth.js'
          },
          'mean.orders': {
            //cwd: 'public',
            src: 'public/orders/views/*.html',
            dest: 'public/build/partials/partials.orders.js'
          },
          'mean.services': {
            //cwd: 'public',
            src: 'public/services/views/*.html',
            dest: 'public/build/partials/partials.services.js'
          },
          'mean.discounts': {
            //cwd: 'public',
            src: 'public/discounts/views/*.html',
            dest: 'public/build/partials/partials.discounts.js'
          },
          */
          'mean.system': {
            //cwd: 'public',
            src: ['public/system/views/*.html', 'public/auth/views/index.html'],
            dest: 'public/build/partials/partials.system.js'
          },
          /*
          'mean.users': {
            //cwd: 'public',
            src: 'public/users/views/*.html',
            dest: 'public/build/partials/partials.users.js'
          },
          'mean.payments': {
            //cwd: 'public',
            src: 'public/payments/views/*.html',
            dest: 'public/build/partials/partials.payments.js'
          },
          */
          'mean.apis': {
            //cwd: 'public',
            src: 'public/api/views/*.html',
            dest: 'public/build/partials/partials.api.js'
          }
        },
        watch: {
            js: {
                files: paths.js,
                // tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            html: {
                files: paths.html,
                tasks: ['ngtemplates', 'uglify'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: paths.css,
                tasks: ['csslint'],
                options: {
                    livereload: true
                }
            }
        },
        /*jshint: {
            all: {
                src: paths.js,
                options: {
                    jshintrc: true
                }
            }
        },*/
        uglify: {
            options: {
              mangle: false,
              sourceMap: true,
              banner: ""
            },
            production: {
              files: '<%= assets.js %>'
            }
        },
        csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            all: {
                src: paths.css
            }
        },
        cssmin: {
            combine: {
                files: '<%= assets.css %>'
            }
        },
        nodemon: {
            dev: {
                script: 'server.js',
                options: {
                    args: [],
                    ignore: ['public/**', 'node_modules/**'],
                    ext: 'js,html',
                    nodeArgs: ['--debug'],
                    delayTime: 1,
                    env: {
                        PORT: require('./server/config/config').port
                    },
                    cwd: __dirname
                }
            }
        },
        concurrent: {
            tasks: ['nodemon', 'watch'],
            options: {
                logConcurrentOutput: true
            }
        },
        mochaTest: {
            options: {
                reporter: 'spec',
                require: 'server.js'
            },
            src: ['test/mocha/**/*.js']
        },
        env: {
          test: {
            NODE_ENV: 'test'
          }
        },
        karma: {
            unit: {
                configFile: 'test/karma/karma.conf.js'
            }
        }
    });

    //Load NPM tasks
    require('load-grunt-tasks')(grunt);

    //Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    //Default task(s).
    if (process.env.NODE_ENV === 'production') {
        grunt.registerTask('default', [/*'jshint', */'csslint', 'cssmin', 'ngtemplates', 'uglify', 'concurrent']);
    } else {
        grunt.registerTask('default', [/*'jshint', */'csslint', 'concurrent']);
    }

    grunt.registerTask('build', ['ngtemplates']);

    //Test task.
    grunt.registerTask('test', ['env:test', 'mochaTest', 'karma:unit']);

    // For Heroku users only.
    // Docs: https://github.com/linnovate/mean/wiki/Deploying-on-Heroku
    grunt.registerTask('heroku:production', [/*'jshint', */'csslint', 'cssmin', 'uglify']);
};
