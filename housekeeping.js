'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var passport = require('passport');
var logger = require('mean-logger');
var kue = require('kue');
var jobs = kue.createQueue();
var cors = require('cors')
var checkout = require('2co');
var util = require('./server/config/util');
var path = require('path');
global.fullPath = path.resolve(__dirname);
/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Initializing system variables
var config = require('./server/config/config');
var db = mongoose.connect(config.db);

// Bootstrap Models, Dependencies, Routes and the app as an express app
var app = require('./server/config/system/bootstrap')(passport, db);

util.walk(__dirname + '/server/jobs', '', function(path){
    require(path);
});

util.walk(__dirname + '/server/cron', '', function(path){
    require(path);
});

 jobs.promote();

// Start the kue manager application
kue.app.listen(6179);
kue.app.set('title', 'Rantic Panel');
console.log('Kue started on port 6179');

// Initializing logger
logger.init(app, passport, mongoose);

// Expose app
exports = module.exports = app;
