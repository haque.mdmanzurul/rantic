'use strict';

angular.module('mean.services').controller('ServicesController', ['Settings','$scope', '$state', '$stateParams', '$location', '$http', '$filter', 'Global', 'Services', 'ngTableParams', '$sce', '$q', function (Settings, $scope, $state, $stateParams, $location, $http, $filter, Global, Services, ngTableParams, $sce, $q) {
  $scope.service = null;

  $scope.countries = [
    {id: "AF", text: "Afghanistan"},
    {id: "AX", text: "Aland Islands"},
    {id: "AL", text: "Albania"},
    {id: "DZ", text: "Algeria"},
    {id: "AS", text: "American Samoa"},
    {id: "AD", text: "Andorra"},
    {id: "AO", text: "Angola"},
    {id: "AI", text: "Anguilla"},
    {id: "AQ", text: "Antarctica"},
    {id: "AG", text: "Antigua and Barbuda"},
    {id: "AR", text: "Argentina"},
    {id: "AM", text: "Armenia"},
    {id: "AW", text: "Aruba"},
    {id: "AP", text: "Asia/Pacific Region"},
    {id: "AU", text: "Australia"},
    {id: "AT", text: "Austria"},
    {id: "AZ", text: "Azerbaijan"},
    {id: "BS", text: "Bahamas"},
    {id: "BH", text: "Bahrain"},
    {id: "BD", text: "Bangladesh"},
    {id: "BB", text: "Barbados"},
    {id: "BY", text: "Belarus"},
    {id: "BE", text: "Belgium"},
    {id: "BZ", text: "Belize"},
    {id: "BJ", text: "Benin"},
    {id: "BM", text: "Bermuda"},
    {id: "BT", text: "Bhutan"},
    {id: "BO", text: "Bolivia"},
    {id: "BQ", text: "Bonaire, Saint Eustatius and Saba"},
    {id: "BA", text: "Bosnia and Herzegovina"},
    {id: "BW", text: "Botswana"},
    {id: "BV", text: "Bouvet Island"},
    {id: "BR", text: "Brazil"},
    {id: "IO", text: "British Indian Ocean Territory"},
    {id: "BN", text: "Brunei Darussalam"},
    {id: "BG", text: "Bulgaria"},
    {id: "BF", text: "Burkina Faso"},
    {id: "BI", text: "Burundi"},
    {id: "KH", text: "Cambodia"},
    {id: "CM", text: "Cameroon"},
    {id: "CA", text: "Canada"},
    {id: "CV", text: "Cape Verde"},
    {id: "KY", text: "Cayman Islands"},
    {id: "CF", text: "Central African Republic"},
    {id: "TD", text: "Chad"},
    {id: "CL", text: "Chile"},
    {id: "CN", text: "China"},
    {id: "CX", text: "Christmas Island"},
    {id: "CC", text: "Cocos (Keeling) Islands"},
    {id: "CO", text: "Colombia"},
    {id: "KM", text: "Comoros"},
    {id: "CG", text: "Congo"},
    {id: "CD", text: "Congo, The Democratic Republic of the"},
    {id: "CK", text: "Cook Islands"},
    {id: "CR", text: "Costa Rica"},
    {id: "CI", text: "Cote d&#039;Ivoire"},
    {id: "HR", text: "Croatia"},
    {id: "CU", text: "Cuba"},
    {id: "CW", text: "Curacao"},
    {id: "CY", text: "Cyprus"},
    {id: "CZ", text: "Czech Republic"},
    {id: "DK", text: "Denmark"},
    {id: "DJ", text: "Djibouti"},
    {id: "DM", text: "Dominica"},
    {id: "DO", text: "Dominican Republic"},
    {id: "EC", text: "Ecuador"},
    {id: "EG", text: "Egypt"},
    {id: "SV", text: "El Salvador"},
    {id: "GQ", text: "Equatorial Guinea"},
    {id: "ER", text: "Eritrea"},
    {id: "EE", text: "Estonia"},
    {id: "ET", text: "Ethiopia"},
    {id: "EU", text: "Europe"},
    {id: "FK", text: "Falkland Islands (Malvinas)"},
    {id: "FO", text: "Faroe Islands"},
    {id: "FJ", text: "Fiji"},
    {id: "FI", text: "Finland"},
    {id: "FR", text: "France"},
    {id: "GF", text: "French Guiana"},
    {id: "PF", text: "French Polynesia"},
    {id: "TF", text: "French Southern Territories"},
    {id: "GA", text: "Gabon"},
    {id: "GM", text: "Gambia"},
    {id: "GE", text: "Georgia"},
    {id: "DE", text: "Germany"},
    {id: "GH", text: "Ghana"},
    {id: "GI", text: "Gibraltar"},
    {id: "GR", text: "Greece"},
    {id: "GL", text: "Greenland"},
    {id: "GD", text: "Grenada"},
    {id: "GP", text: "Guadeloupe"},
    {id: "GU", text: "Guam"},
    {id: "GT", text: "Guatemala"},
    {id: "GG", text: "Guernsey"},
    {id: "GN", text: "Guinea"},
    {id: "GW", text: "Guinea-Bissau"},
    {id: "GY", text: "Guyana"},
    {id: "HT", text: "Haiti"},
    {id: "HM", text: "Heard Island and McDonald Islands"},
    {id: "VA", text: "Holy See (Vatican City State)"},
    {id: "HN", text: "Honduras"},
    {id: "HK", text: "Hong Kong"},
    {id: "HU", text: "Hungary"},
    {id: "IS", text: "Iceland"},
    {id: "IN", text: "India"},
    {id: "ID", text: "Indonesia"},
    {id: "IR", text: "Iran, Islamic Republic of"},
    {id: "IQ", text: "Iraq"},
    {id: "IE", text: "Ireland"},
    {id: "IM", text: "Isle of Man"},
    {id: "IL", text: "Israel"},
    {id: "IT", text: "Italy"},
    {id: "JM", text: "Jamaica"},
    {id: "JP", text: "Japan"},
    {id: "JE", text: "Jersey"},
    {id: "JO", text: "Jordan"},
    {id: "KZ", text: "Kazakhstan"},
    {id: "KE", text: "Kenya"},
    {id: "KI", text: "Kiribati"},
    {id: "KW", text: "Kuwait"},
    {id: "KG", text: "Kyrgyzstan"},
    {id: "LA", text: "Lao People&#039;s Democratic Republic"},
    {id: "LV", text: "Latvia"},
    {id: "LB", text: "Lebanon"},
    {id: "LS", text: "Lesotho"},
    {id: "LR", text: "Liberia"},
    {id: "LY", text: "Libyan Arab Jamahiriya"},
    {id: "LI", text: "Liechtenstein"},
    {id: "LT", text: "Lithuania"},
    {id: "LU", text: "Luxembourg"},
    {id: "MO", text: "Macao"},
    {id: "MK", text: "Macedonia"},
    {id: "MG", text: "Madagascar"},
    {id: "MW", text: "Malawi"},
    {id: "MY", text: "Malaysia"},
    {id: "MV", text: "Maldives"},
    {id: "ML", text: "Mali"},
    {id: "MT", text: "Malta"},
    {id: "MH", text: "Marshall Islands"},
    {id: "MQ", text: "Martinique"},
    {id: "MR", text: "Mauritania"},
    {id: "MU", text: "Mauritius"},
    {id: "YT", text: "Mayotte"},
    {id: "MX", text: "Mexico"},
    {id: "FM", text: "Micronesia, Federated States of"},
    {id: "MD", text: "Moldova, Republic of"},
    {id: "MC", text: "Monaco"},
    {id: "MN", text: "Mongolia"},
    {id: "ME", text: "Montenegro"},
    {id: "MS", text: "Montserrat"},
    {id: "MA", text: "Morocco"},
    {id: "MZ", text: "Mozambique"},
    {id: "MM", text: "Myanmar"},
    {id: "NA", text: "Namibia"},
    {id: "NR", text: "Nauru"},
    {id: "NP", text: "Nepal"},
    {id: "NL", text: "Netherlands"},
    {id: "NC", text: "New Caledonia"},
    {id: "NZ", text: "New Zealand"},
    {id: "NI", text: "Nicaragua"},
    {id: "NE", text: "Niger"},
    {id: "NG", text: "Nigeria"},
    {id: "NU", text: "Niue"},
    {id: "NF", text: "Norfolk Island"},
    {id: "KP", text: "North Korea"},
    {id: "MP", text: "Northern Mariana Islands"},
    {id: "NO", text: "Norway"},
    {id: "OM", text: "Oman"},
    {id: "PK", text: "Pakistan"},
    {id: "PW", text: "Palau"},
    {id: "PS", text: "Palestinian Territory"},
    {id: "PA", text: "Panama"},
    {id: "PG", text: "Papua New Guinea"},
    {id: "PY", text: "Paraguay"},
    {id: "PE", text: "Peru"},
    {id: "PH", text: "Philippines"},
    {id: "PN", text: "Pitcairn"},
    {id: "PL", text: "Poland"},
    {id: "PT", text: "Portugal"},
    {id: "PR", text: "Puerto Rico"},
    {id: "QA", text: "Qatar"},
    {id: "RE", text: "Reunion"},
    {id: "RO", text: "Romania"},
    {id: "RU", text: "Russian Federation"},
    {id: "RW", text: "Rwanda"},
    {id: "BL", text: "Saint Bartelemey"},
    {id: "SH", text: "Saint Helena"},
    {id: "KN", text: "Saint Kitts and Nevis"},
    {id: "LC", text: "Saint Lucia"},
    {id: "MF", text: "Saint Martin"},
    {id: "PM", text: "Saint Pierre and Miquelon"},
    {id: "VC", text: "Saint Vincent and the Grenadines"},
    {id: "WS", text: "Samoa"},
    {id: "SM", text: "San Marino"},
    {id: "ST", text: "Sao Tome and Principe"},
    {id: "SA", text: "Saudi Arabia"},
    {id: "SN", text: "Senegal"},
    {id: "RS", text: "Serbia"},
    {id: "SC", text: "Seychelles"},
    {id: "SL", text: "Sierra Leone"},
    {id: "SG", text: "Singapore"},
    {id: "SX", text: "Sint Maarten"},
    {id: "SK", text: "Slovakia"},
    {id: "SI", text: "Slovenia"},
    {id: "SB", text: "Solomon Islands"},
    {id: "SO", text: "Somalia"},
    {id: "ZA", text: "South Africa"},
    {id: "GS", text: "South Georgia and the South Sandwich Islands"},
    {id: "KR", text: "South Korea"},
    {id: "ES", text: "Spain"},
    {id: "LK", text: "Sri Lanka"},
    {id: "SD", text: "Sudan"},
    {id: "SR", text: "Suriname"},
    {id: "SJ", text: "Svalbard and Jan Mayen"},
    {id: "SZ", text: "Swaziland"},
    {id: "SE", text: "Sweden"},
    {id: "CH", text: "Switzerland"},
    {id: "SY", text: "Syrian Arab Republic"},
    {id: "TW", text: "Taiwan"},
    {id: "TJ", text: "Tajikistan"},
    {id: "TZ", text: "Tanzania, United Republic of"},
    {id: "TH", text: "Thailand"},
    {id: "TL", text: "Timor-Leste"},
    {id: "TG", text: "Togo"},
    {id: "TK", text: "Tokelau"},
    {id: "TO", text: "Tonga"},
    {id: "TT", text: "Trinidad and Tobago"},
    {id: "TN", text: "Tunisia"},
    {id: "TR", text: "Turkey"},
    {id: "TM", text: "Turkmenistan"},
    {id: "TC", text: "Turks and Caicos Islands"},
    {id: "TV", text: "Tuvalu"},
    {id: "UG", text: "Uganda"},
    {id: "UA", text: "Ukraine"},
    {id: "AE", text: "United Arab Emirates"},
    {id: "GB", text: "United Kingdom"},
    {id: "US", text: "United States"},
    {id: "UM", text: "United States Minor Outlying Islands"},
    {id: "UY", text: "Uruguay"},
    {id: "UZ", text: "Uzbekistan"},
    {id: "VU", text: "Vanuatu"},
    {id: "VE", text: "Venezuela"},
    {id: "VN", text: "Vietnam"},
    {id: "VG", text: "Virgin Islands, British"},
    {id: "VI", text: "Virgin Islands, U.S."},
    {id: "WF", text: "Wallis and Futuna"},
    {id: "EH", text: "Western Sahara"},
    {id: "YE", text: "Yemen"},
    {id: "ZM", text: "Zambia"},
    {id: "ZW", text: "Zimbabwe"}
  ];


  $scope.deleteDiscount = function() {
          var service = new Services({
            service: $scope.discount_service[0]
           });
           console.log("WTS?" + $scope.discount_service[0])
          service.$removeDiscount({service: $scope.discount_service[0]},function() {
                $state.go('settings service');
          });

  }

   $scope.groupByCategory = function (item){
    return item.category || 'Other Services';
  };
    $scope.selectedService = {
      service: ''
    };
    $scope.servicesFilter = function(column){
      $scope.initCreate();
      var promise = $q.defer();
      var list = [];

      var timeout = setInterval(function(){
        if($scope.services){
          _.each($scope.services, function(service){
            //_.each(vendor, function(service){
              list.push({
                id: service.value,
                title: $filter('cut')(service.label, true, 45)
              });
            //});
          });
          promise.resolve(list);
          clearInterval(timeout);
        }
      }, 10);

      return promise;
    }
    $scope.initCreate = function(){

      Services.query(function(result) {

        $scope.services = _.filter(result.data, function(service){
          if(!service.recurring) service.label = service.label + " - $" + service.price + " Per " + service.multiplier + "!";
          return !service.disabled;
        });
        });
    };


      $scope.$watch('selectedService', function(newValue, oldValue){
      if(newValue.service != ""){
        $scope.service = newValue.service;


      }



    }, true);


       $scope.$watch( 'service', function( newValue, oldValue ) {
        // Ignore initial setup.
        if ( newValue === oldValue ) {
            return;
        }


        _.forEach($scope.services, function(service) {
            if(newValue === service.value){
                  // this displays Speed Service checkbox
                $scope.serviceObject = service;
                $scope.serviceObject.selectedPackage = $scope.serviceObject.packages[0];
                return false;
            }
        });
    });
  $scope.find = function() {
      $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
          sorting: {
              "created": "desc"
          }
      }, {
          total: 0,
          getData: function($defer, params) {
            Services.query(params.url(), params.$params.filter, function(result) {
              $scope.services = result.data;
              params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });


  };

  $scope.update = function() {
    $scope.disableButton = true;
    var service = $scope.service;
    service.dropdown =  JSON.stringify(service.dropdown);
    service.discount_settings = JSON.stringify(service.discount_settings);

    service.$update(function() {
        $location.path('/services');
    });
  };

  $scope.remove = function(){
    $scope.service.$remove(function(){
        $location.path('/services');
    });
  };

  $scope.discount_settings = [{price: "", percentage: "", service: ""}];
  $scope.discount_service = [];
  $scope.customSettings = {};
  $scope.modifyDropDown = function(event, data) {
    if(data.title.match("hidden")) {
      data.title = data.title.split(" hidden")[0];
    }
    $scope.service.dropdown[data.index] = { title: data.title, api_code: data.api_code, fixed_price: data.fixed_price, min_quantity: data.min_quantity, api_url: data.api_url, api_hidden: data.api_hidden};

    var service = new Services({
      data: JSON.stringify($scope.service.dropdown),
      service: $scope.service.value
     });

    service.$createDropdown(
      function(response) {
        $state.go($state.current.name, {}, {reload: true});
    });





    event.preventDefault();

  }
  $scope.removeDropDown = function(event, data) {
    $scope.service.dropdown.splice(data.index, 1);
     var service = new Services({
      data: JSON.stringify($scope.service.dropdown),
      service: $scope.service.value
     });

    service.$createDropdown(
      function(response) {
        $state.go($state.current.name, {}, {reload: true});
    });

    event.preventDefault();

  }
  $scope.submitDropDown = function(event, data) {
    var tmp = [];
    $scope.service.dropdown.push({title: data.new_title, api_code: data.new_api_code, fixed_price: data.new_fixed_price, min_quantity: data.new_min_quantity, api_hidden: data.api_hidden});

    // Make sure it's legimate item
    for(var i in $scope.service.dropdown) {
      if($scope.service.dropdown[i].title) tmp.push($scope.service.dropdown[i]);
    }

     var service = new Services({
      data: JSON.stringify(tmp),
      service: $scope.service.value
     });

     service.$createDropdown(
      function(response) {

       // $scope.customSettings = [{title: '', api_code: '', addDropdown: false}];
        $state.go($state.current.name, {}, {reload: true});

      },
      function(response) {
        $scope.progress = false;
        vex.dialog.alert("Error while saving service");
      }
    );


    event.preventDefault();
  }
  $scope.addCustom = function(){
    $scope.discount_service.push($scope.serviceObject.value);
  }
  $scope.removeCustom = function(index) {
    $scope.discount_service.splice(index, 1);
  }
  $scope.modifyCustom = function(index, val) {
    if(val.length) $scope.discount_service[index] = val;
  }


    $scope.addFromDiscount = function(index) {

        $scope.discount_service.push(index);

    }

    $scope.removeFromDiscount = function(event, index) {
        $scope.discount_service.splice(index, 1);
        event.preventDefault();

  }

  $scope.removeDiscount = function(event, index) {

    $scope.discount_settings.splice(index, 1);

    event.preventDefault();
  }
  $scope.addDiscount = function(event, price, percentage, index, service, modify) {
        var serv = (modify) ? service : $scope.serviceObject.value
        $scope.discount_settings[index] = {price: price, percentage: percentage, service: serv};
        $scope.discount_settings.push({price: "", percentage: "", service: ""});
        event.preventDefault();

  }
  $scope.createDiscount = function() {
    var discount = new Services({
        data: JSON.stringify($scope.discount_settings),
        service: JSON.stringify($scope.discount_service)
    });
    discount.$createDiscount(function() {
          $state.go($state.current.name, {}, {reload: true});
    });
  }
  $scope.progress = false;
  $scope.create = function(){
    var self = this;
    $scope.disableButton = true;

    var service = new Services({
        label: $scope.service.label,
        value: $scope.service.value,
        category: $scope.service.category,
        pattern: $scope.service.pattern,
        multiplier: $scope.service.multiplier,
        price: $scope.service.price,
        targeted: $scope.service.targeted,
        disabled: $scope.service.disabled,
        recurring: $scope.service.recurring,
        packages: $scope.service.packages,
        countries: $scope.service.countries,
        description: $scope.service.description,
        api_price: $scope.service.api_price,
        api_code: $scope.service.api_code,
        api_type: $scope.service.api_type,
        api_nr: 1,
        discount: $scope.service.discount
    });


    service.$save(
      function(response) {
        $scope.progress = false;
        $location.path('/services');
      },
      function(response) {
        $scope.progress = false;
        vex.dialog.alert("Error while saving service");
      }
    );

  };

  $scope.findOne = function() {


      Services.getApiData(function(data) {

        var result = {}
        for(var i in data.data) {
          result[data.data[i].api_name] = data.data[i].api_url;
        }
         Services.get({
          serviceId: $stateParams.serviceId
      }, function(service) {

          $scope.service = service;
          $scope.service.apidata = result;
          try {
            $scope.service.discount_settings = (service.discount_settings && service.discount_settings.length) ? JSON.parse(service.discount_settings) : [{price: "", percentage: ""}];
          } catch(e) { $scope.service.discount_settings = [{price: "", percentage: ""}]; }
          if(!$scope.service.discount_settings.price) $scope.service.discount_settings = [{price: "", percentage: ""}];
          try {
            $scope.service.dropdown = JSON.parse(service.dropdown);

            for(var i in $scope.service.dropdown) {
              $scope.service.dropdown[i].index = i;
              console.log($scope.service.dropdown[i].api_hidden + ' testing')

              if($scope.service.dropdown[i].api_hidden === true && !$scope.service.dropdown[i].title.match(/hidden/)) $scope.service.dropdown[i].title += " hidden";
            }

          } catch(e) {

            $scope.service.dropdown = [{title: "", api_code: "", index: 0, fixed_price: false, min_quantity: false, api_url: "" }];
          }
      });


      });

  };

  $scope.goCreateService = function(){
    $state.go('create service');
  };

  $scope.goSettingsService = function(){
    $state.go('settings service');
  };
  $scope.editDiscount = function() {


    var discount = new Services({
        data: JSON.stringify($scope.discount_settings),
        id: $stateParams.serviceId,
        service: JSON.stringify($scope.discount_service)
    });

    discount.$editDiscount(function() {
         $state.go('settings service');
    });

  }

  $scope.fetchDiscount = function() {

     Services.fetchDiscount({
          serviceId: $stateParams.serviceId
      }, function(service) {
        service = service.data;
        service.service = service.service.split(",");
        service.price = service.price.split(",");
        service.percentage = service.percentage.split(",");
        $scope.discountData = [];
        var tmp = [];

        for(var i = 0; i < service.price.length;i++) {
            if(service.price[i].length) {
                $scope.discountData.push({service: service.service[i], price: service.price[i], percentage: service.percentage[i]});
                $scope.discount_service.push(service.service[i]);
            }
        }
        for(var i = 0; i < service.service.length;i++) {
            if(service.service[i].length) {
                if(tmp.indexOf(service.service[i]) == -1) {
                    $scope.discount_service.push(service.service[i]);
                    tmp.push(service.service[i]);
                }
            }
        }
        $scope.discount_service = tmp;
        $scope.discount_settings = $scope.discountData;



      });

  }
  $scope.serviceDiscount = function() {

    $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
          sorting: {
              "created": "desc"
          }
      }, {
          total: 0,
          getData: function($defer, params) {
            Services.discount(params.url(), params.$params.filter, function(result) {
                var tmp = result.data;

              params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });

  }
  $scope.addPackage = function(){
    if(!$scope.service.packages){
      $scope.service.packages = [];
    }

    $scope.service.packages.push({
      price: null,
      quantity: null
    });
  };

  $scope.removePackage = function(key){
    $scope.service.packages.splice(key, 1);
  };
}]);
