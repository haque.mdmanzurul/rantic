'use strict';

//Orders service used for services REST endpoint
angular.module('mean.services').factory('Services', ['$resource', function($resource) {
    return $resource('services/:serviceId', {
        serviceId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false
      },
      discount: {
        method: 'GET',
        url: 'services/discount'
      },
      createDiscount: {
        method: "POST",
        url: 'services/createDiscount'
      },
      fetchDiscount: {
        method: "POST",
        url: 'services/fetchDiscount'
      },
      editDiscount: {
        method: "POST",
        url: "services/editDiscount"
      },
      createDropdown: {
        method: 'POST',
        url: "services/createDropdown"
      },
      getApiData: {
        method: "POST",
        url: "services/getApiData",

},
        removeDiscount: {
                method: "DELETE",
                url: "services/removeDiscount"
        }
    });
}]);
