'use strict';

//Setting up route
angular.module('mean.services').config(['$stateProvider',
    function($stateProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0')
                    $timeout(deferred.resolve, 0);

                // Not Authenticated
                else {
                    $timeout(function() {
                        deferred.reject();
                    }, 0);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('all services', {
                url: '/services',
                templateUrl: 'public/services/views/list2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create service', {
                url: '/services/create',
                templateUrl: 'public/services/views/create2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('settings service', {
                url: '/services/settings',
                templateUrl: 'public/services/views/settings.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('modify service', {
                url: '/services/modify/:serviceId',
                templateUrl: 'public/services/views/modify.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            /* .state('edit service', {
                url: '/services/:serviceId/edit',
                templateUrl: 'public/services/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })*/
            .state('service by id', {
                url: '/services/:serviceId',
                templateUrl: 'public/services/views/view3.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);
