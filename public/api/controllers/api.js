'use strict';

angular.module('mean.apis').controller('ApiCtrl', ['$filter', '$q', '$scope',  'ngTableParams','Services', 'Global', 'ApiHelper', function ($filter, $q, $scope, ngTableParams, Services, Global, ApiHelper) {
    $scope.global = Global;

    $scope.selectedService = {
      service: ''
    };
    var API = ApiHelper;
    $scope.groupByCategory = function (item){
    return item.category || 'Other Services';
  };
  
    $scope.servicesFilter = function(column){
      $scope.initCreate();
      var promise = $q.defer();
      var list = [];

      var timeout = setInterval(function(){
        if($scope.services){
          _.each($scope.services, function(service){
            //_.each(vendor, function(service){
              list.push({
                id: service.value,
                title: $filter('cut')(service.label, true, 45)
              });
            //});
          });
          promise.resolve(list);
          clearInterval(timeout);
        }
      }, 10);

      return promise;
    }
    $scope.getOrderPrice = function(){
        if(!this.order) return 0;
        if(Global.hasRole('provider')){
            if(this.order.provider){
                return this.order.price * this.order.provider.profit;
            } else {
                return this.order.price * Global.user.profit;
            }
        } else {
            return this.order.price;
        }
    };

    $scope.find = function() {
      $scope.tableParams = new ngTableParams({
        page: 1,
        count: 50,
        sorting: {
          "created": "desc"
        }
      }, {
        total: 0,
        getData: function($defer, params) {
          API.get_all(params.url(), params.$params.filter, function(result) {

            $scope.orders = result.data;
            params.total(result.total);
            $defer.resolve(result.data);
          });
        }
      });
    };
    $scope.initCreate = function(){
     
      Services.query(function(result) {
        
        $scope.services = _.filter(result.data, function(service){
          if(!service.recurring) service.label = service.label + " - $" + service.price + " Per " + service.multiplier + "!";
          return !service.disabled;
        });
        });
    };


      $scope.$watch('selectedService', function(newValue, oldValue){
      if(newValue.service != ""){
        $scope.service = newValue.service;

        API.example({data: newValue.service}, function(result) {
            $scope.example_add = result.add;
            $scope.example_status = result.status;
            $scope.api_code = result.api_code;
            console.log(result);
        }, function(err) {
            vex.dialog.alert(err.data);
        });
      }



    }, true);


       $scope.$watch( 'service', function( newValue, oldValue ) {
        // Ignore initial setup.
        if ( newValue === oldValue ) {
            return;
        }


        _.forEach($scope.services, function(service) {
            if(newValue === service.value){
                  // this displays Speed Service checkbox
                $scope.serviceObject = service;
                $scope.serviceObject.selectedPackage = $scope.serviceObject.packages[0];
                return false;
            }
        });
    });

   

    
}]);