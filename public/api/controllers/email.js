'use strict';

angular.module('mean.apis').controller('EmailController', ['$filter', '$q', '$scope',  'ngTableParams','Services', 'Global', 'ApiHelper', function ($filter, $q, $scope, ngTableParams, Services, Global, ApiHelper) {

  $scope.statusChange = [{
    label: 'SELECT STATUS', selected: 'selected' }, {
    label: 'Complete' }, {
    label: 'Inprogress' }, {
    label: 'Cancelled' }, {
    label: 'Abandoned' }


  ];
    var API = ApiHelper;
      $scope.global = Global;
      $scope.selectedService = {
        service: ''
      };

    $scope.multiple_services = [];

    $scope.runner = function() {
      $scope.multiple_services.push($scope.selectedService.service);

    }


    $scope.removeLine = function(index, value) {
      $scope.multiple_services.splice(index, 1);

    }
    $scope.groupByCategory = function (item){
      return item.category || 'Other Services';
    };
    $scope.servicesFilter = function(column){
      $scope.initCreate();
      var promise = $q.defer();
      var list = [];

      var timeout = setInterval(function(){
        if($scope.services){
          _.each($scope.services, function(service){
              list.push({
                id: service.value,
                title: $filter('cut')(service.label, true, 45)
              });
            //});
          });
          promise.resolve(list);
          clearInterval(timeout);
        }
      }, 10);

      return promise;
    }
    $scope.initCreate = function(){
      setTimeout(function() {
      Services.query(function(result) {
	      result.data.unshift({ label: "Get all emails", value: "ALL" });
        $scope.services = _.filter(result.data, function(service){
          if(!service.recurring && service.value != "ALL") service.label = service.label + " - $" + service.price + " Per " + service.multiplier + "!";
          return !service.disabled;
        });


      });
    }, 3000);
    };

    $scope.tableParams = null;
    $scope.initialize = function(filter) {
       $scope.tableParams = new ngTableParams({

        sorting: {
          "created": "desc"
        }
      }, {
        total: 0,
        getData: function($defer, params) {
	if(!actionStart) return false;
           $scope.showLoader = true;
           if(filter) {

            return false;
           }
         API.emails(params.url(), params.$params.filter, function(result) {
	actionStart = false;
            $scope.showLoader = false;
            $scope.emails = result.data;
            params.total(result.total);
            $defer.resolve(result.data);
	
          });
        }
      });

     }
$scope.getEmails = function(filter) {
       $scope.tableParams = new ngTableParams({
       
        sorting: {
          "created.default": "desc"
        }
      }, {
        total: 0,
        getData: function($defer, params) {
        
          API.emailsubs(params.url(), params.$params.filter, function(result) {
            $scope.emails = result.data;
            params.total(result.total);
            $defer.resolve(result.data);
            console.log(result.data);
          });
        }
      });

     }


     $scope.bulkCopy = function(){
        if(!$scope.emails) return;

        var lines = [];
        _.each($scope.emails, function(email){
            lines.push(email.email);
        });

        vex.open({
            contentCSS: {
                width: '85%',
                'min-height': 300
            },
            content: 'Full email list:<br><textarea style="width: 100%; height: 300px;"></textarea>',
            afterOpen: function($vexContent) {
                $('textarea', $vexContent).html(lines.join('\n')).height(300);
                return $vexContent;
            }
        });
    };
	var actionStart = false;
      $scope.runSearch = function() {
       if($scope.statusChange.label.label != "SELECT STATUS") $scope.tableParams.$params.filter.status = $scope.statusChange.label.label;
       else $scope.tableParams.$params.filter.random = Math.floor(Math.random() * (100-1) + 1);
        $scope.tableParams.$params.filter['multiple_services'] = $scope.multiple_services;
	actionStart = true;
      }


    $scope.runSearch = function() {
      console.log("KK");
      $scope.tableParams.$params.filter['multiple_services'] = $scope.multiple_services;
      console.log($scope.tableParams.$params.filter);
    }
 




    $scope.$watch('selectedService', function(newValue, oldValue){
      if(newValue.service != ""){
        $scope.service = newValue.service;


      }



    }, true);


       $scope.$watch( 'service', function( newValue, oldValue ) {
        // Ignore initial setup.
        if ( newValue === oldValue ) {
            return;
        }


        _.forEach($scope.services, function(service) {
            if(newValue === service.value){
                  // this displays Speed Service checkbox
                $scope.serviceObject = service;
            if($scope.serviceObject.packages)    $scope.serviceObject.selectedPackage = $scope.serviceObject.packages[0];
                return false;
            }
        });
    });






}]);
