'use strict';

angular.module('mean.apis').controller('BlackListController', ['$filter', '$q', '$scope',  'ngTableParams','Services', 'Global', 'ApiHelper', function ($filter, $q, $scope, ngTableParams, Services, Global, ApiHelper) {
    
    
    var API = ApiHelper;
   $scope.rantic = {}
   $scope.addNew = function() {

    API.add_blacklist({data: $scope.rantic.add}, function(result) {


      vex.dialog.alert("Request complete.");
      $scope.rantic.add = "";
      $scope.rantic.showAdd = false;
      $scope.tableParams.reload();
    }, function(err) {

      vex.dialog.alert(err.data);

    })

   }

 

       $scope.tableParams = new ngTableParams({
        page: 1,
        count: 50,
        sorting: {
          "created": "desc"
        }
      }, {
        total: 0,
        getData: function($defer, params) {
          API.blacklist(params.url(), params.$params.filter, function(result) {
          
            $scope.blacklist = result.data;
            params.total(result.total);
            $defer.resolve(result.data);
          });
        }
      });
 



    
   

    $scope.del = function (row) {
      API.delete_blacklist({data: row._id}, function(result) {
        vex.dialog.alert("Request complete!");
        row.isEditing = false;
        $scope.tableParams.reload();
      }, function(err) {
        vex.dialog.alert(err.data);
      });
      
    }
    
   
    $scope.save = function (row) {
     
      API.edit_blacklist({data: row}, function(result) {
        vex.dialog.alert("Request complete!");
        row.isEditing = false;
        $scope.tableParams.reload();
      }, function(err) {
        vex.dialog.alert(err.data);
      });
     
    }
  










   
}]);