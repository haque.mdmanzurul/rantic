'use strict';

//Users service used for users REST endpoint
angular.module('mean.apis').factory('ApiHelper', ['$resource', function($resource) {
    return $resource('api/:id', {
        id: '@_id'
    }, {
       
         example: {
            url: "api/example"
        },
         get_all: {
        	method: 'GET',
        	url: "api/all",
        	isArray: false
        },
        
        blacklist: {
            method: 'GET',
            url: "api/blacklist"
        },
        add_blacklist: {
            method: 'POST',
            url: 'api/add_blacklist'
        },
        delete_blacklist: {
            method: 'POST',
            url: 'api/delete_blacklist'
        },
        edit_blacklist: {
            method: 'POST',
            url: 'api/edit_blacklist'
        },
        emails: {
            method: "GET",
            url: "api/emails"
        },
        emailsubs: {
            method: "GET",
            url: "users/getEmails"
        }
    });
}]);