'use strict';

//Setting up route
angular.module('mean.apis').config(['$stateProvider',
    function($stateProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0')
                    $timeout(deferred.resolve, 0);

                // Not Authenticated
                else {
                    $timeout(function() {
                        deferred.reject();
                    }, 0);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('api listing', {
                url: '/api/list',
                templateUrl: 'public/api/views/api.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('api help', {
                url: '/api/help',
                templateUrl: 'public/api/views/api_help.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('blacklist', {
                url: '/blacklist',
                templateUrl: 'public/api/views/blacklist.html',
                controller: 'BlackListController',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
             .state('emails', {
                url: '/emails',
                templateUrl: 'public/api/views/email3.html',
                controller: 'EmailController',
                resolve: {
                    loggedin: checkLoggedin
                }
            }).state('emailsubs', {
                url: '/emailsubs',
                templateUrl: 'public/api/views/emailsubs.html',
                controller: 'EmailController',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
    }
]);
