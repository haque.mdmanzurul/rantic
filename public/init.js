'use strict';

vex.defaultOptions.className = 'vex-theme-plain';

angular.element(document).ready(function() {
    //Fixing facebook bug with redirect
    if (window.location.hash === '#_=_') window.location.hash = '#!';

    //Then init the app
    angular.bootstrap(document, ['mean']);

});

// Dynamically add angular modules declared by packages
var packageModules = [];
for (var index in window.modules) {
    angular.module(window.modules[index].module, (window.modules[index].angularDependencies?window.modules[index].angularDependencies:[]));
    packageModules.push(window.modules[index].module);
}

// Default modules
var modules = ['angularModalService', 'ngCookies', 'ngResource', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tabs', 'dialogs.main', 'pascalprecht.translate', 'dialogs.default-translations', 'ui.router', 'angular-redactor','ui.select', 'mean.system', 'mean.orders', 'mean.auth', 'mean.users', 'mean.services', 'mean.discounts', 'mean.payments', 'mean.apis'];
modules = modules.concat(packageModules);

// Combined modules
angular.module('mean', modules);
