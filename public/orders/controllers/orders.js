'use strict';
angular.module('mean.orders').directive('requireMultiple', function () {
  return {
    require: 'ngModel',
    link: function postLink(scope, element, attrs, ngModel) {

      ngModel.$validators.required = function (value) {
        if (scope.serviceObject.targeted) {
          return angular.isArray(value) && value.length > 0;
        }
      }
    }
  };
});

angular.module('mean.orders', ['angular-clipboard']).factory('Speed', function () {
  return {
    display: function (service) {

      if (service.faster_speed) return true;
      else return false;

    }
  }
}).controller('OrdersController', ['Settings', 'Speed', '$document', '$timeout', 'Users', '$scope', '$stateParams', '$location', '$http', '$filter', 'Global', 'Payments', 'Orders', 'Services', 'Codes', 'ngTableParams', '$sce', '$q', '$state',
  function (Settings, Speed, $document, $timeout, Users, $scope, $stateParams, $location, $http, $filter, Global, Payments, Orders, Services, Codes, ngTableParams, $sce, $q, $state) {
    $scope.global = Global;
    $scope.meta = {};
    $scope.disableButton = false;
    $scope.newComment = '';
    $scope.price_watcher;
    $scope.price_counter = 0;
    $scope.comments = [{
      id: 'comment1'
    }];
    $scope.ig_mentions = [{
      id: 'ig1'
    }];
    $scope.formInProgress = false;
    $scope.fullDiscount = {
      haveDiscount: false
    };

    $scope.modifyApiCode = function (order) {

      $http.post('/order_modify_api/' + order._id, {
          api_id: order.api_id
        })
        .success(function (data, status) {})

    }

    $scope.refunded = function (order_id) {
      vex.dialog.buttons.YES.text = 'Confirm';
      vex.dialog.buttons.NO.text = 'Cancel';

      vex.dialog.confirm({
        message: 'Confirm to continue.',
        callback: function (yes) {
          if (yes) {
            Balance();
          }
        }
      });

      function Balance() {
        var order = new Orders({
          order_id: order_id
        });
        order.$refunded(function (resp) {
          vex.dialog.alert('Order marked as refunded');
          $state.go($state.current.name, {}, {
            reload: true
          })
        }, function (err) {
          vex.dialog.alert(err.data.message);
        });

      }

    }
    $scope.returnBalance = function (order_id, customer) {

      vex.dialog.buttons.YES.text = 'Confirm';
      vex.dialog.buttons.NO.text = 'Cancel';

      vex.dialog.confirm({
        message: 'Confirm to continue.',
        callback: function (yes) {
          if (yes) {
            Balance();
          }
        }
      });

      function Balance() {
        var order = new Orders({
          order_id: order_id
        });
        var db = customer ? "$returnBalance_customer" : "$returnBalance";
        order[db](function (resp) {
          vex.dialog.alert('Money has been refunded to your Rantic account!');
          $state.go($state.current.name, {}, {
            reload: true
          })
        }, function (err) {
          vex.dialog.alert(err.data.message);
        });

      }

    }

    $scope.resubmit = function () {

      var service = $scope.order.service.value;
      var data = {};

      data.comments = JSON.stringify($scope.ig_comments);
      data.hashtag = JSON.stringify($scope.order.hashtag);
      data.followers = JSON.stringify($scope.order.followers);
      data.ig_media = JSON.stringify($scope.order.ig_media);
      data.ig_mentions = JSON.stringify($scope.ig_mentions);
      data.order_id = $stateParams.orderId;
      data.service = service;
      data.orderCount = $scope.order.orderCount;
      data.url = $scope.order.url;
      data.dropdown_index = (typeof $scope.order.dropdown_index !== "undefined") ? $scope.order.dropdown_index : false;
      Orders.reorder(data, function (response) {
        vex.dialog.alert('Re-ordering complete!');
        $state.go($state.current.name, {}, {
          reload: true
        });
      }, function (err) {
        vex.dialog.alert(err.data);
      });

    }


    $scope.removeLine = function () {
      var line = $scope.comments.length - 1;

      if (line != 0) {
        $scope.quantity = line;
        $scope.comments.splice(line);
      }
    }

    $scope.addLine = function () {
      var line = $scope.comments.length + 1;
      $scope.quantity = line;
      if (line < 2000) $scope.comments.push({
        id: 'comment' + line
      });
    }









    $scope.countries = [
        {
        id: "AF",
        text: "Afghanistan"
      },
      {
        id: "AX",
        text: "Aland Islands"
      },
      {
        id: "AL",
        text: "Albania"
      },
      {
        id: "DZ",
        text: "Algeria"
      },
      {
        id: "AS",
        text: "American Samoa"
      },
      {
        id: "AD",
        text: "Andorra"
      },
      {
        id: "AO",
        text: "Angola"
      },
      {
        id: "AI",
        text: "Anguilla"
      },
      {
        id: "AQ",
        text: "Antarctica"
      },
      {
        id: "AG",
        text: "Antigua and Barbuda"
      },
      {
        id: "AR",
        text: "Argentina"
      },
      {
        id: "AM",
        text: "Armenia"
      },
      {
        id: "AW",
        text: "Aruba"
      },
      {
        id: "AP",
        text: "Asia/Pacific Region"
      },
      {
        id: "AU",
        text: "Australia"
      },
      {
        id: "AT",
        text: "Austria"
      },
      {
        id: "AZ",
        text: "Azerbaijan"
      },
      {
        id: "BS",
        text: "Bahamas"
      },
      {
        id: "BH",
        text: "Bahrain"
      },
      {
        id: "BD",
        text: "Bangladesh"
      },
      {
        id: "BB",
        text: "Barbados"
      },
      {
        id: "BY",
        text: "Belarus"
      },
      {
        id: "BE",
        text: "Belgium"
      },
      {
        id: "BZ",
        text: "Belize"
      },
      {
        id: "BJ",
        text: "Benin"
      },
      {
        id: "BM",
        text: "Bermuda"
      },
      {
        id: "BT",
        text: "Bhutan"
      },
      {
        id: "BO",
        text: "Bolivia"
      },
      {
        id: "BQ",
        text: "Bonaire, Saint Eustatius and Saba"
      },
      {
        id: "BA",
        text: "Bosnia and Herzegovina"
      },
      {
        id: "BW",
        text: "Botswana"
      },
      {
        id: "BV",
        text: "Bouvet Island"
      },
      {
        id: "BR",
        text: "Brazil"
      },
      {
        id: "IO",
        text: "British Indian Ocean Territory"
      },
      {
        id: "BN",
        text: "Brunei Darussalam"
      },
      {
        id: "BG",
        text: "Bulgaria"
      },
      {
        id: "BF",
        text: "Burkina Faso"
      },
      {
        id: "BI",
        text: "Burundi"
      },
      {
        id: "KH",
        text: "Cambodia"
      },
      {
        id: "CM",
        text: "Cameroon"
      },
      {
        id: "CA",
        text: "Canada"
      },
      {
        id: "CV",
        text: "Cape Verde"
      },
      {
        id: "KY",
        text: "Cayman Islands"
      },
      {
        id: "CF",
        text: "Central African Republic"
      },
      {
        id: "TD",
        text: "Chad"
      },
      {
        id: "CL",
        text: "Chile"
      },
      {
        id: "CN",
        text: "China"
      },
      {
        id: "CX",
        text: "Christmas Island"
      },
      {
        id: "CC",
        text: "Cocos (Keeling) Islands"
      },
      {
        id: "CO",
        text: "Colombia"
      },
      {
        id: "KM",
        text: "Comoros"
      },
      {
        id: "CG",
        text: "Congo"
      },
      {
        id: "CD",
        text: "Congo, The Democratic Republic of the"
      },
      {
        id: "CK",
        text: "Cook Islands"
      },
      {
        id: "CR",
        text: "Costa Rica"
      },
      {
        id: "CI",
        text: "Cote d&#039;Ivoire"
      },
      {
        id: "HR",
        text: "Croatia"
      },
      {
        id: "CU",
        text: "Cuba"
      },
      {
        id: "CW",
        text: "Curacao"
      },
      {
        id: "CY",
        text: "Cyprus"
      },
      {
        id: "CZ",
        text: "Czech Republic"
      },
      {
        id: "DK",
        text: "Denmark"
      },
      {
        id: "DJ",
        text: "Djibouti"
      },
      {
        id: "DM",
        text: "Dominica"
      },
      {
        id: "DO",
        text: "Dominican Republic"
      },
      {
        id: "EC",
        text: "Ecuador"
      },
      {
        id: "EG",
        text: "Egypt"
      },
      {
        id: "SV",
        text: "El Salvador"
      },
      {
        id: "GQ",
        text: "Equatorial Guinea"
      },
      {
        id: "ER",
        text: "Eritrea"
      },
      {
        id: "EE",
        text: "Estonia"
      },
      {
        id: "ET",
        text: "Ethiopia"
      },
      {
        id: "EU",
        text: "Europe"
      },
      {
        id: "FK",
        text: "Falkland Islands (Malvinas)"
      },
      {
        id: "FO",
        text: "Faroe Islands"
      },
      {
        id: "FJ",
        text: "Fiji"
      },
      {
        id: "FI",
        text: "Finland"
      },
      {
        id: "FR",
        text: "France"
      },
      {
        id: "GF",
        text: "French Guiana"
      },
      {
        id: "PF",
        text: "French Polynesia"
      },
      {
        id: "TF",
        text: "French Southern Territories"
      },
      {
        id: "GA",
        text: "Gabon"
      },
      {
        id: "GM",
        text: "Gambia"
      },
      {
        id: "GE",
        text: "Georgia"
      },
      {
        id: "DE",
        text: "Germany"
      },
      {
        id: "GH",
        text: "Ghana"
      },
      {
        id: "GI",
        text: "Gibraltar"
      },
      {
        id: "GR",
        text: "Greece"
      },
      {
        id: "GL",
        text: "Greenland"
      },
      {
        id: "GD",
        text: "Grenada"
      },
      {
        id: "GP",
        text: "Guadeloupe"
      },
      {
        id: "GU",
        text: "Guam"
      },
      {
        id: "GT",
        text: "Guatemala"
      },
      {
        id: "GG",
        text: "Guernsey"
      },
      {
        id: "GN",
        text: "Guinea"
      },
      {
        id: "GW",
        text: "Guinea-Bissau"
      },
      {
        id: "GY",
        text: "Guyana"
      },
      {
        id: "HT",
        text: "Haiti"
      },
      {
        id: "HM",
        text: "Heard Island and McDonald Islands"
      },
      {
        id: "VA",
        text: "Holy See (Vatican City State)"
      },
      {
        id: "HN",
        text: "Honduras"
      },
      {
        id: "HK",
        text: "Hong Kong"
      },
      {
        id: "HU",
        text: "Hungary"
      },
      {
        id: "IS",
        text: "Iceland"
      },
      {
        id: "IN",
        text: "India"
      },
      {
        id: "ID",
        text: "Indonesia"
      },
      {
        id: "IR",
        text: "Iran, Islamic Republic of"
      },
      {
        id: "IQ",
        text: "Iraq"
      },
      {
        id: "IE",
        text: "Ireland"
      },
      {
        id: "IM",
        text: "Isle of Man"
      },
      {
        id: "IL",
        text: "Israel"
      },
      {
        id: "IT",
        text: "Italy"
      },
      {
        id: "JM",
        text: "Jamaica"
      },
      {
        id: "JP",
        text: "Japan"
      },
      {
        id: "JE",
        text: "Jersey"
      },
      {
        id: "JO",
        text: "Jordan"
      },
      {
        id: "KZ",
        text: "Kazakhstan"
      },
      {
        id: "KE",
        text: "Kenya"
      },
      {
        id: "KI",
        text: "Kiribati"
      },
      {
        id: "KW",
        text: "Kuwait"
      },
      {
        id: "KG",
        text: "Kyrgyzstan"
      },
      {
        id: "LA",
        text: "Lao People&#039;s Democratic Republic"
      },
      {
        id: "LV",
        text: "Latvia"
      },
      {
        id: "LB",
        text: "Lebanon"
      },
      {
        id: "LS",
        text: "Lesotho"
      },
      {
        id: "LR",
        text: "Liberia"
      },
      {
        id: "LY",
        text: "Libyan Arab Jamahiriya"
      },
      {
        id: "LI",
        text: "Liechtenstein"
      },
      {
        id: "LT",
        text: "Lithuania"
      },
      {
        id: "LU",
        text: "Luxembourg"
      },
      {
        id: "MO",
        text: "Macao"
      },
      {
        id: "MK",
        text: "Macedonia"
      },
      {
        id: "MG",
        text: "Madagascar"
      },
      {
        id: "MW",
        text: "Malawi"
      },
      {
        id: "MY",
        text: "Malaysia"
      },
      {
        id: "MV",
        text: "Maldives"
      },
      {
        id: "ML",
        text: "Mali"
      },
      {
        id: "MT",
        text: "Malta"
      },
      {
        id: "MH",
        text: "Marshall Islands"
      },
      {
        id: "MQ",
        text: "Martinique"
      },
      {
        id: "MR",
        text: "Mauritania"
      },
      {
        id: "MU",
        text: "Mauritius"
      },
      {
        id: "YT",
        text: "Mayotte"
      },
      {
        id: "MX",
        text: "Mexico"
      },
      {
        id: "FM",
        text: "Micronesia, Federated States of"
      },
      {
        id: "MD",
        text: "Moldova, Republic of"
      },
      {
        id: "MC",
        text: "Monaco"
      },
      {
        id: "MN",
        text: "Mongolia"
      },
      {
        id: "ME",
        text: "Montenegro"
      },
      {
        id: "MS",
        text: "Montserrat"
      },
      {
        id: "MA",
        text: "Morocco"
      },
      {
        id: "MZ",
        text: "Mozambique"
      },
      {
        id: "MM",
        text: "Myanmar"
      },
      {
        id: "NA",
        text: "Namibia"
      },
      {
        id: "NR",
        text: "Nauru"
      },
      {
        id: "NP",
        text: "Nepal"
      },
      {
        id: "NL",
        text: "Netherlands"
      },
      {
        id: "NC",
        text: "New Caledonia"
      },
      {
        id: "NZ",
        text: "New Zealand"
      },
      {
        id: "NI",
        text: "Nicaragua"
      },
      {
        id: "NE",
        text: "Niger"
      },
      {
        id: "NG",
        text: "Nigeria"
      },
      {
        id: "NU",
        text: "Niue"
      },
      {
        id: "NF",
        text: "Norfolk Island"
      },
      {
        id: "KP",
        text: "North Korea"
      },
      {
        id: "MP",
        text: "Northern Mariana Islands"
      },
      {
        id: "NO",
        text: "Norway"
      },
      {
        id: "OM",
        text: "Oman"
      },
      {
        id: "PK",
        text: "Pakistan"
      },
      {
        id: "PW",
        text: "Palau"
      },
      {
        id: "PS",
        text: "Palestinian Territory"
      },
      {
        id: "PA",
        text: "Panama"
      },
      {
        id: "PG",
        text: "Papua New Guinea"
      },
      {
        id: "PY",
        text: "Paraguay"
      },
      {
        id: "PE",
        text: "Peru"
      },
      {
        id: "PH",
        text: "Philippines"
      },
      {
        id: "PN",
        text: "Pitcairn"
      },
      {
        id: "PL",
        text: "Poland"
      },
      {
        id: "PT",
        text: "Portugal"
      },
      {
        id: "PR",
        text: "Puerto Rico"
      },
      {
        id: "QA",
        text: "Qatar"
      },
      {
        id: "RE",
        text: "Reunion"
      },
      {
        id: "RO",
        text: "Romania"
      },
      {
        id: "RU",
        text: "Russian Federation"
      },
      {
        id: "RW",
        text: "Rwanda"
      },
      {
        id: "BL",
        text: "Saint Bartelemey"
      },
      {
        id: "SH",
        text: "Saint Helena"
      },
      {
        id: "KN",
        text: "Saint Kitts and Nevis"
      },
      {
        id: "LC",
        text: "Saint Lucia"
      },
      {
        id: "MF",
        text: "Saint Martin"
      },
      {
        id: "PM",
        text: "Saint Pierre and Miquelon"
      },
      {
        id: "VC",
        text: "Saint Vincent and the Grenadines"
      },
      {
        id: "WS",
        text: "Samoa"
      },
      {
        id: "SM",
        text: "San Marino"
      },
      {
        id: "ST",
        text: "Sao Tome and Principe"
      },
      {
        id: "SA",
        text: "Saudi Arabia"
      },
      {
        id: "SN",
        text: "Senegal"
      },
      {
        id: "RS",
        text: "Serbia"
      },
      {
        id: "SC",
        text: "Seychelles"
      },
      {
        id: "SL",
        text: "Sierra Leone"
      },
      {
        id: "SG",
        text: "Singapore"
      },
      {
        id: "SX",
        text: "Sint Maarten"
      },
      {
        id: "SK",
        text: "Slovakia"
      },
      {
        id: "SI",
        text: "Slovenia"
      },
      {
        id: "SB",
        text: "Solomon Islands"
      },
      {
        id: "SO",
        text: "Somalia"
      },
      {
        id: "ZA",
        text: "South Africa"
      },
      {
        id: "GS",
        text: "South Georgia and the South Sandwich Islands"
      },
      {
        id: "KR",
        text: "South Korea"
      },
      {
        id: "ES",
        text: "Spain"
      },
      {
        id: "LK",
        text: "Sri Lanka"
      },
      {
        id: "SD",
        text: "Sudan"
      },
      {
        id: "SR",
        text: "Suriname"
      },
      {
        id: "SJ",
        text: "Svalbard and Jan Mayen"
      },
      {
        id: "SZ",
        text: "Swaziland"
      },
      {
        id: "SE",
        text: "Sweden"
      },
      {
        id: "CH",
        text: "Switzerland"
      },
      {
        id: "SY",
        text: "Syrian Arab Republic"
      },
      {
        id: "TW",
        text: "Taiwan"
      },
      {
        id: "TJ",
        text: "Tajikistan"
      },
      {
        id: "TZ",
        text: "Tanzania, United Republic of"
      },
      {
        id: "TH",
        text: "Thailand"
      },
      {
        id: "TL",
        text: "Timor-Leste"
      },
      {
        id: "TG",
        text: "Togo"
      },
      {
        id: "TK",
        text: "Tokelau"
      },
      {
        id: "TO",
        text: "Tonga"
      },
      {
        id: "TT",
        text: "Trinidad and Tobago"
      },
      {
        id: "TN",
        text: "Tunisia"
      },
      {
        id: "TR",
        text: "Turkey"
      },
      {
        id: "TM",
        text: "Turkmenistan"
      },
      {
        id: "TC",
        text: "Turks and Caicos Islands"
      },
      {
        id: "TV",
        text: "Tuvalu"
      },
      {
        id: "UG",
        text: "Uganda"
      },
      {
        id: "UA",
        text: "Ukraine"
      },
      {
        id: "AE",
        text: "United Arab Emirates"
      },
      {
        id: "GB",
        text: "United Kingdom"
      },
      {
        id: "US",
        text: "United States"
      },
      {
        id: "UM",
        text: "United States Minor Outlying Islands"
      },
      {
        id: "UY",
        text: "Uruguay"
      },
      {
        id: "UZ",
        text: "Uzbekistan"
      },
      {
        id: "VU",
        text: "Vanuatu"
      },
      {
        id: "VE",
        text: "Venezuela"
      },
      {
        id: "VN",
        text: "Vietnam"
      },
      {
        id: "VG",
        text: "Virgin Islands, British"
      },
      {
        id: "VI",
        text: "Virgin Islands, U.S."
      },
      {
        id: "WF",
        text: "Wallis and Futuna"
      },
      {
        id: "EH",
        text: "Western Sahara"
      },
      {
        id: "YE",
        text: "Yemen"
      },
      {
        id: "ZM",
        text: "Zambia"
      },
      {
        id: "ZW",
        text: "Zimbabwe"
      }
    ];

    $scope.indexer = 0;
    $scope.updateQuantity = function () {
      $scope.quantity = $scope.serviceObject.dropdown[$scope.indexer].min_quantity;
      $scope.min_quantity = $scope.serviceObject.dropdown[$scope.indexer].min_quantity;
      $scope.min_price = $scope.serviceObject.dropdown[$scope.indexer].fixed_price;
      $scope.twTargeted = $scope.serviceObject.dropdown[$scope.indexer].api_code;
    }

    $scope.totalComments = function () {


      if ($scope.serviceObject.value == "fb_comments_customelite" && $scope.comments.length < 5) return false;
      if ($scope.serviceObject.value == "yt_custom_elite" && $scope.comments.length < 5) return false;
      if ($scope.serviceObject.value == "ig_custom" && $scope.comments.length < 5) return false;
      if ($scope.serviceObject.value == 'ig_mentions_hashtag' && $scope.hashtag.length < 1) return false;
      if ($scope.serviceObject.value == 'ig_mentions_followers' && $scope.followers.length < 1) return false;
      if ($scope.serviceObject.value == 'ig_mentions_likers' && $scope.ig_media.length < 1) return false;
      else return true;
    }


    $scope.checkComments = function () {
      var list = ["fb_comments_customelite", "yt_custom_elite", "ig_custom"];
      if ($scope.serviceObject) {
        if (list.indexOf($scope.serviceObject.value) != -1) {
          var tmp = $scope.comment.split("\n");
          if (tmp.indexOf("") !== -1) {
            vex.dialog.alert("No empty lines.");
            return false;
          }
          if ($scope.serviceObject.value != "ig_mentions") $scope.quantity = tmp.length;

        }

      }
    }

    $scope.actionInProgress = false;

    $scope.verify_fb = function (method, showNow) {
      var errorCheck = false;
      $timeout(function () {
        $scope.actionInProgress = false;

      }, 1500);
      if ($scope.cart_items) {
        _.forEach($scope.cart_items, function (i) {
          if (i.url == $scope.url && i.service.value == $scope.serviceObject.value) {

            errorCheck = true;
          }
        });




      }
      if (errorCheck) {
        vex.dialog.alert("Sorry, duplicated orders are not allowed!");
        return false;
      }
      Orders.duplicates({
        url: $scope.url,
        service: $scope.serviceObject.value
      }, function (data) {

        if ($scope.serviceObject.value == "unlimited_fb" || $scope.serviceObject.value == "musically_followers" || $scope.serviceObject.value == 'musically_likes') {
          for (var i in $scope.serviceObject.dropdown) {
            if ($scope.serviceObject.dropdown[i].api_code == $scope.twTargeted) {
              var price = $scope.serviceObject.dropdown[i].fixed_price;
              $scope.price = price;
              $scope.quantity = 1;


            }
          }
        }
        var list = ["fb_comments_customelite", "yt_custom_elite", "ig_custom"];

        if (list.indexOf($scope.serviceObject.value) != -1) {
          $scope.comments = [];
          var tmp = $scope.comment.split("\n");
          for (var i in tmp) {
            $scope.comments.push({
              id: i,
              name: tmp[i]
            });
          }

          var process = $scope.totalComments();
          if (!process) {
            vex.dialog.alert("Minimum comments amount is 5");
            return false;
          }
        } else $scope.comments = [];

        var instagram = [
          'ig_mentions_hashtag'

        ];

        var category_list = [




          'fb_eu',
          'fb_worldwide',
          'fb_usa',
          'fb_italian',
          'fb_arabic_likes',
          'fb_vip',
          'fb_arabic_likes',
          'Fb_targeted_likes_any'


        ];
        // get fb page categories
        if (instagram.indexOf($scope.serviceObject.value) != -1) {
          Orders.instagram({
            hashtag: $scope.hashtag
          }, function (response) {
            if (response.data) {
              vex.dialog.alert(response.data);
            } else $scope.addToCart(method);
          }, function (err) {
            vex.dialog.alert("There is no such hashtag or it's banned by instagram, please choose something else.");
          });

        } else if (category_list.indexOf($scope.serviceObject.value) != -1) {
          var send_request = Orders.fb_category({
            link: $scope.url
          }, function (response) {
            if (response.data) {
              vex.dialog.alert('Please change the category of your page to anything but  Music band / Song / Album / Record Label, Once promotion is done you can change the category back.');


            } else {
              $scope.addToCart(method, showNow);

            }

          })



        } else $scope.addToCart(method, showNow);




      }, function (err) {
        vex.dialog.alert(err.data);
        return false;
      });

    }


    $scope.api = {
      key: 0
    };
    $scope.cart_config = {
      stage_two: false
    };
    $scope.updateView = function () {
      $scope.cart_config.stage_two = true;
      $scope.cart_items = []
    }
    $scope.disableCart = function (action) {
      //$scope.cart_window(true);
      //if(!action) vex.dialog.alert("Please wait!");


    }
    $scope.changeAPI_ID = function (id, order_id) {
      var order = new Orders({
        new_key: id,
        order_id: order_id
      });
      order.$change_api(function (resp) {
        vex.dialog.alert('API KEY Changed!');
      }, function (err) {

        vex.dialog.alert(err.data);
      });

    }



    $scope.checkCart = function (launch_cartView) {



      if (localStorage.getItem('data')) {
        $scope.cart_is_empty = JSON.parse(localStorage.getItem('data')).length < 0 ? true : false;
        $scope.cart_items = JSON.parse(localStorage.getItem('data'));
        var arr = JSON.parse(localStorage.getItem('data'));

        $scope.amount = 0;
        for (var i = 0; i < JSON.parse(localStorage.getItem('data')).length; i++) {
          // arr[i].price = arr[i].price.split('$')[1];
          $scope.amount += parseFloat(arr[i].price);

        }



        $scope.cart_total = JSON.parse(localStorage.getItem('data')).length;
        if (JSON.parse(localStorage.getItem('data')).length) $scope.is_cart = true;


      }
      if (launch_cartView) setTimeout(function () {
        $scope.ViewCart();
      }, 1000);




      if (Global.user.balance != 0 && Global.user.balance < $scope.amount) {

        $scope.payment_amount = $scope.amount - Global.user.balance;

      } else {
        $scope.payment_amount = $scope.amount;
      }




    }
    $scope.paymentAction = false;
    $scope.refreshTable = function () {
      setTimeout(function () {
        $scope.cartParams.reload();

      }, 1000);

    }
    $scope.cart_items = [];
    $scope.cartParams = {};

    $scope.cartParams = new ngTableParams({
      page: 1,
      count: 100
    }, {
      counts: [],
      total: $scope.cart_items.length,
      getData: function ($defer, params) {
        $defer.resolve($scope.cart_items)
      }
    });
    $scope.ViewCart = function (normalize) {
      $scope.cart_window = function (close) {
        if (close) vex.close($vexContent);
        else {
          var $vexContent;
          $vexContent = vex.open({
            content: $('#cart_template').clone(true),
            input: $scope.amount,
            callback: function (data) {
              vex.close();

            }
          })
        }
      }
      $scope.cart_window(normalize);


    }

    $scope.cartRemove = function (name) {

      _.remove($scope.cart_items, {
        id: name


      });
      // if there are no items in cart

      if ($scope.cart_items.length == 0) $('.view-cart-info').css('display', 'none');


      localStorage.setItem('data', JSON.stringify($scope.cart_items));
      $scope.checkCart(); // refresh cart data

    }
    $scope.addCartPayment = function (method) {
      var error = false;
      var cart = JSON.parse(localStorage.getItem('data'));
      $scope.add_cart = {};
      $scope.add_cart.price = $scope.amount;
      $scope.add_cart.quantity = $scope.cart_total;
      $scope.add_cart.orderCount = 1;
      $scope.add_cart.service = 'add_cart';
      $scope.add_cart.url = 'add_cart';
      $scope.add_cart.email = cart[0].email;
      $scope.add_cart.discount = false;
      $scope.add_cart.cart = cart;
      $scope.create(method);
    }

    $scope.placeOrdernow = false;
    $scope.addToCart = function (launch_cartView, showNow) {
      var one_th = ['yt_monetized', 'yt_views_retention', 'dm_views', 'vm_views', 'fb_views_auto', 'yt_views_smlite_packages'];
      var fixed_quantity = [
        'pt_likes', 'pt_repins', 'pt_followers', 'vk_likes', 'vk_shares', 'vk_subscribers', 'vn_revines', 'vn_followers', 'vn_likes', 'vn_loops',
        'yt_dislikes', 'yt_usa', 'sc_downloads', 'sc_plays', 'dm_views', 'g_circles', 'g_ones', 'g_shares'
      ]

      if (one_th.indexOf($scope.serviceObject.value) != -1) {

        if (!Number.isInteger($scope.quantity / 1000)) {
          vex.dialog.alert('Quantity must be in thousands, ex 1000, not 1200!');
          return false;
        }
      }


      if (fixed_quantity.indexOf($scope.serviceObject.value) != -1) {

        var round_qty = $scope.quantity / 100;
        if (round_qty != Math.floor(($scope.quantity / 100))) {
          vex.dialog.alert('Quantity must be in hundreds, ex 1200, not 1230!');
          return false;
        }
      }

      if ($scope.serviceObject.maximum && $scope.quantity > $scope.serviceObject.maximum) {
        vex.dialog.alert("Maximum quantity is " + $scope.serviceObject.maximum);
        return false;
      }
      if (!$scope.url) {
        vex.dialog.alert("URL is required.");
        return false;
      }
      if ($scope.serviceObject.value == "ig_likes" || $scope.serviceObject.value == "ig_usalikes") {
        if ($scope.quantity < $scope.serviceObject.minimum) {
          vex.dialog.alert("Minimum quantity is " + $scope.serviceObject.minimum);
          return false;
        }
      }
      if ($scope.serviceObject.value == "yt_views_smlite_packages") {
        if ($scope.quantity < $scope.min_quantity) {
          vex.dialog.alert("Minimum quantity is " + $scope.min_quantity);
          return false;
        }
      }
      if ($scope.serviceObject.dropdown) {
        for (var i in $scope.serviceObject.dropdown) {
          if ($scope.serviceObject.dropdown[i].api_code == $scope.twTargeted && $scope.serviceObject.value !== "yt_views_smlite_packages") {
            if ($scope.serviceObject.dropdown[i].min_quantity && $scope.quantity < $scope.serviceObject.dropdown[i].min_quantity) {
              vex.dialog.alert('Minimum quantity is ' + $scope.serviceObject.dropdown[i].min_quantity);
              return false;
            }
          }
        }
      }
      if ($scope.serviceObject.minimum && $scope.quantity < $scope.serviceObject.minimum) {
        vex.dialog.alert("Minimum quantity is " + $scope.serviceObject.minimum);
        return false;
      }
      if (!$scope.serviceObject.minimum && $scope.quantity < $scope.serviceObject.multiplier && $scope.serviceObject.value != "ig_likes" && $scope.serviceObject.value != "ig_usalikes") {
        vex.dialog.alert('Minimum quantity is ' + $scope.serviceObject.multiplier);
        return false;
      }
      if ($scope.serviceObject.value == "yt_trending_views" && !$scope.twTargeted || $scope.serviceObject.value == "yt_sm_target" && !$scope.twTargeted || $scope.serviceObject.value == "tw_targeted" && !$scope.twTargeted) {
        vex.dialog.alert('Please select country.');

        return false;
      }
      if (!parseInt($scope.quantity)) {
        vex.dialog.alert('Quantity in numbers please.');
        return false;
      }
      if (localStorage.getItem('data') && localStorage.getItem('data').length > 2) {


        var arr = JSON.parse(localStorage.getItem('data'));

        if (arr.map(function (e) {
            return e.service.recurring;
          }).indexOf(true) != -1 && !$scope.serviceObject.recurring) {
          $('#quickOrderForm').trigger('reset');
          vex.dialog.alert('Recurring or regular orders only!');
          return false;
        }
        if (arr.map(function (e) {
            return e.service.recurring;
          }).indexOf(true) == -1 && $scope.serviceObject.recurring) {
          $('#quickOrderForm').trigger('reset');
          vex.dialog.alert('Recurring or regular orders only!');
          return false;
        } else if (arr.map(function (e) {
            return e.service.recurring;
          }).indexOf(true) != -1 && $scope.serviceObject.recurring) {
          $('#quickOrderForm').trigger('reset');
          vex.dialog.alert('Recurring orders can be ordered one at the time!');
          return false;
        }


      }
      if ($scope.placeOrdernow) $scope.placeOrder = true;

      vex.close($scope.cart_window);
      // if($scope.serviceObject.recurring) vex.dialog.alert("Add cart is not supported for recurring payments.");
      if ($scope.serviceObject.targeted && $scope.selectedCountries.countries.length === 0 || $scope.serviceObject.value == "tw_targeted" && !$scope.twTargeted_api) {
        vex.dialog.alert("Please select country.");
        console.log("first one", $scope.selectedCountries, $scope.twTargeted_api)
        $scope.placeOrder = false;
        return false;
      } else {
        $timeout(function () {
          if (showNow) {
            $scope.placeOrder = true;
            $scope.refreshTable();
          }
        }, 1500);
        $('.view-cart-info').css('display', 'initial'); // reset cart info to default
        if (!localStorage.getItem('data')) var arr = [];
        else var arr = JSON.parse(localStorage.getItem('data'));
        if ($scope.serviceObject.value != 'unlimited_fb' && $scope.serviceObject.value != 'musically_followers' && $scope.serviceObject.value != 'musically_likes') {
          var price = ($scope.discount) ? $scope.getServicePriceWithDiscount() : $scope.getServicePrice();
          price = price.split('$')[1];
        } else var price = $scope.price;
        if (price.match(',')) price = parseFloat(price.replace(',', '')).toFixed(2);
        $scope.dropdown_index;
        var emoticon = $scope.emoticon;
        emoticon = ($scope.serviceObject.value == "yt_views_smlite_packages") ? $scope.twTargeted : emoticon;
        emoticon = ($scope.serviceObject.value == "yt_sm_target") ? $scope.twTargeted_api : emoticon;
        emoticon = ($scope.serviceObject.value == "tw_targeted") ? $scope.twTargeted_api : emoticon;
        emoticon = ($scope.serviceObject.value == "yt_trending_views") ? $scope.twTargeted_api : emoticon;
        emoticon = ($scope.serviceObject.value == "unlimited_fb") ? $scope.twTargeted : emoticon;
        emoticon = ($scope.serviceObject.value == "musically_likes") ? $scope.twTargeted : emoticon;
        emoticon = ($scope.serviceObject.value == "musically_followers") ? $scope.twTargeted : emoticon;
        if ($scope.serviceObject.dropdown && emoticon) {
          var dropdown_index = false;
          for (var i in $scope.serviceObject.dropdown) {
            if ($scope.serviceObject.dropdown[i].api_code === emoticon) {
              dropdown_index = i;
            }
          }
          if (dropdown_index) $scope.dropdown_index = dropdown_index;
        }
        arr.push({
          indexer: $scope.indexer,
          dropdown_index: $scope.dropdown_index,
          emoticon: emoticon,
          eatSpeed: $scope.eatSpeed,
          ig_mentions: JSON.stringify($scope.comments),
          ig_media: $scope.ig_media,
          followers: $scope.followers,
          hashtag: $scope.hashtag,
          comments: JSON.stringify($scope.comments),
          countries: $scope.selectedCountries.countries,
          speed_service: $scope.fasterSpeed,
          recurring: $scope.serviceObject.recurring,
          url: $scope.url,
          quantity: $scope.quantity,
          orderCount: $scope.getServiceCount(true),
          discount: $scope.discountCode,
          service_label: $scope.serviceObject.label,
          service: $scope.serviceObject,
          email: $scope.email,
          name: $scope.url,
          real_price: price,
          price: price,
          id: Math.floor(Math.random() * 99999999)
        });
        localStorage.setItem('data', JSON.stringify(arr));
        $scope.checkCart(launch_cartView);


        $scope.comments = [{
          id: 'comment1'
        }];
        $scope.ig_mentions = [{
          id: 'ig1'
        }];
        if (!$scope.is_cart) $scope.is_cart = true;

      }


    }


    $scope.selectedCountries = {
      countries: []
    };

    $scope.selectedService = {
      service: ''
    };

    $scope.$watch('selectedService', function (newValue) {
      if (newValue) {
        $scope.service = newValue.service;
      }
    }, true);
    $scope.statusChangeValue = 'Complete';
    $scope.statusChange = [{
        label: 'Complete',
        selected: 'selected'
      }, {
        label: 'Inprogress'
      }, {
        label: 'Cancelled'
      }, {
        label: 'Abandoned'
      }


    ];

    $scope.creditPayment = false;
    $scope.abandonedCredit = function () {
      $("#creditForm").submit();

    }
    $scope.payForAbandoned = function (id, price, refund, method) {

      var order = new Orders({
        _id: id,
        price: price,
        balance: Global.user.balance,
        userid: Global.user._id,
        method: method,
        origin: encodeURIComponent(window.location.origin)
      });

      order.$abandoned(function (resp) {

        if (typeof resp.balance !== 'undefined') {
          if (!refund) {
            Global.user.balance = resp.balance;
            $scope.global.user.balance = resp.balance;
          } else {
            Global.user.refund_balance = resp.balance;
            $scope.global.user.refund_balance = resp.balance;
          }

          vex.dialog.alert('Thank-you for using our services! Payment was successful');
          $state.go($state.current.name, {}, {
            reload: true
          })
        } else {

          if (resp.continue && resp.continue == "paypal") window.top.location.href = window.location.origin + '/abandoned_paypal/' + id + '?origin=' + encodeURIComponent(window.location.origin);
          else {


            window.top.location.href = window.location.origin + '/abandoned_credit/' + id + '?origin=' + encodeURIComponent(window.location.origin);

          }
        }
      }, function (err) {
        vex.dialog.alert(err.data.message);
      });

    }

    $scope.changeStatus = function (id, value) {
      var order_data = [];
      order_data['_id'] = id;
      order_data['status'] = value;
      var order = new Orders(order_data);
      order.$edit_status(function (resp) {
        vex.dialog.alert('Status has been changed!');
      });
    }

    $scope.countChanger = function (id, field_name) {




      var order_field = [];
      order_field[field_name] = $('.' + field_name).val();
      order_field['_id'] = id;

      var order = new Orders(order_field);
      order.$edit_count(function (resp) {
        vex.dialog.alert('Count has been changed.');

      });
    }

    function serviceUpdater(service) {
      if (service.value == "yt_views_smlite_packages" || service.value == "unlimited_fb" || service.value == "musically_followers" || service.value == 'musically_likes' || service.value == 'tw_targeted') {

        $scope.twTargeted = service.dropdown[0].api_code;
        $scope.price = service.dropdown[0].fixed_price;
        $scope.min_price = $scope.price;
      } else if (service.value == "yt_trending_views" || service.value == "tw_targeted" || service.value == "yt_sm_target") {
        $scope.twTargeted_api = service.dropdown[service.dropdown.length - 1].api_code;
      }
      $scope.quantity = service.multiplier ? service.multiplier : 100;

    }
    $scope.twTargeted_api = false;
    $scope.initCreate = function () {
      setTimeout(function () {
        var link = "/services?v=37536804797128"
        $http.get(link).then(function (result) {
          result.data = result.data.data;
          $scope.services = _.filter(result.data, function (service) {
            if (!service.recurring) service.label = service.label + " - $" + service.price + " Per " + service.multiplier + "!";

            try {
              service.discount_settings = (service.discount_settings && service.discount_settings.length) ? JSON.parse(service.discount_settings) : [];
            } catch (e) {
              service.discount_settings = [];
            }
            try {
              service.dropdown = JSON.parse(service.dropdown);
              var tmp = []
              for (var i in service.dropdown) {
                if (!service.dropdown[i].api_hidden) tmp.push(service.dropdown[i]);
              }
              service.dropdown = tmp;
              for (var i in service.dropdown) {
                service.dropdown[i].index = i;
              }
              $scope.twTargeted_api = service.dropdown[0].api_code;

            } catch (e) {
              service.dropdown = [{
                title: "",
                api_code: "",
                index: 0,
                fixed_price: false
              }];
            }
            var tmp = [];
            for (var i in service.discount_settings.price) {
              tmp.push({
                price: service.discount_settings.price[i],
                percentage: service.discount_settings.percentage[i]
              });
            }
            service.discount_settings = tmp;
            service.selectedPackage = service.packages[0];



            serviceUpdater(service);




            return !service.disabled;
          });

        });
      }, 3000);
    };

    $scope.translateFn = function (value) {
      if (!$scope.serviceObject) {
        return value;
      }

      return value * $scope.serviceObject.multiplier;
    };

    $scope.urlFitsPattern = function (force_false) {

      if ($scope.serviceObject && $scope.serviceObject.value == 'ig_followers') {

        if ((new XRegExp($scope.serviceObject.pattern, 'i')).test($scope.url)) {
          $scope.form.$invalid = false;
          $scope.form.$valid = true;

          return true;
        }


      }

      if (force_false) {
        $scope.url_error = ($scope.serviceObject) ? $scope.serviceObject.error_message : "";
        return false;
      }

      if (!$scope.url) {
        return false;
      }

      if (!$scope.serviceObject) {
        return false;
      }

      if ((new XRegExp($scope.serviceObject.pattern, 'i')).test($scope.url)) {

        $scope.form.$invalid = false;
        $scope.form.$valid = true;
        return true;
      } else {


        $scope.url_error = $scope.serviceObject.error_message;

        $scope.form.url.$setValidity('required', false);
        return false;
      }
    };
    $scope.verifyServiceUrl = function () {


      if ($scope.serviceObject && (new XRegExp($scope.serviceObject.pattern, 'i')).test($scope.url)) {

        $scope.form.$invalid = false;
        $scope.form.$valid = true;
        $scope.form.url.$setValidity('required', true);
        return true;
      } else {
        //  $scope.url_error = $scope.serviceObject.error_message;



        $scope.urlFitsPattern(true);
        return true;
        // return $scope.url_error;
      }

    }

    var conditionalServices = function () {
      var patterns = {
        '^(http(s)?\:\/\/)?(.*?\\.)?(facebook|fb)\\.(com|me)': 'facebook',
        '^(http(s)?\:\/\/)?(.*?\\.)?instagram\\.com': 'instagram',
        '^(http(s)?\:\/\/)?(.*?\\.)?t(witter)?\\.(com|co)': 'twitter',
        '^(http(s)?\:\/\/)?(.*?\\.)?youtu(be)?\\.(com|be)': 'youtube',
        '^(http(s)?\:\/\/)?(.*?\\.)?pinterest\\.com': 'pinterest',
        '^(http(s)?\:\/\/)?(.*?\\.)?soundcloud\\.com': 'soundcloud',
        '^(http(s)?\:\/\/)?(.*?\\.)?(vk|vkontakte)\\.(com|ru)': 'vk',
        '^(http(s)?\:\/\/)?(.*?\\.)?vine\\.com': 'vine',
        '^(http(s)?\:\/\/)?(.*?\\.)?vimeo\\.com': 'vimeo',
        '^(http(s)?\:\/\/)?(.*?\\.)?dailymotion\\.com': 'dailymotion',
        '^(http(s)?\:\/\/)?(.*?\\.)?reverbnation\\.com': 'reverbnation'
      };

      var service = 'website';

      _.forIn(patterns, function (patternService, pattern) {
        // we want FALSE when a match was made
        if ((new XRegExp(pattern, 'i')).test($scope.url)) {
          service = patternService;
          return false;
        }
      });

      // if the type changed ...
      if ($scope.type != service) {
        // set the service type
        $scope.type = service;

        // set the selectbox for services
        $scope.services = services[service];

        // default to the first item
        $scope.service = $scope.services[0].value;

        $scope.quantity = $scope.serviceObject.multiplier ? $scope.serviceObject.multiplier : 100;

      }
    };

    $scope.isTrafficService = function () {
      if (!$scope.order) return false;
      if (!$scope.order.service) return false;

      var found = _.find(['wt_iframe', 'wt_popunder', 'wt_iframe_targeted', 'wt_popunder_targeted', 'wt_custom'], function (compareService) {
        if (compareService === $scope.order.service.value) {
          return true;
        }
      });

      return found;
    };

    var getMeta = function () {
      if (_.isEmpty($scope.url) || !$scope.url) {
        return;
      } else if (!$scope.url.match(re_weburl)) {
        return;
      }

      $scope.meta = null;
      $scope.fullMeta = null;

      $http({
          method: 'GET',
          url: '/iframely',
          params: {
            uri: $scope.url
          }
        })
        .success(function (data, status) {
          if (!_.isEmpty(data.meta)) {
            $scope.meta = data.meta;
            $scope.fullMeta = data;
          } else {
            $scope.meta = null;
            $scope.fullMeta = null;
          }
        })
        .error(function (data, status) {

        });
    };

    $scope.bulkCopy = function () {
      if (!$scope.orders) return;

      var lines = [];
      _.each($scope.orders, function (order) {
        if (order.cancelled || order.completed || order.url == "add_cart" || !order.paid || !order.checkout) return;
        lines.push(
          order.url + '|' + order.orderCount + '|' + order.service.value
        );
      });

      vex.open({
        contentCSS: {
          width: '85%',
          'min-height': 300
        },
        content: 'The following are orders in progress:<br><textarea style="width: 100%; height: 300px;"></textarea>',
        afterOpen: function ($vexContent) {
          $('textarea', $vexContent).html(lines.join('\n')).height(300);
          return $vexContent;
        }
      });
    };

    $scope.checkPayment = function() {      
      Orders.checkOrder({}, function () {
        return;
      }, function (err) {
        console.log(err);
      });
    }

    $scope.noMeta = function () {
      return _.isEmpty($scope.meta);
    };

    /* $scope.$watch( 'url', function( newValue, oldValue ) {
        // Ignore initial setup.
        if ( newValue === oldValue ) {
            return;
        }
        conditionalServices();
    }); */

    $scope.$watch('url', _.debounce(function () {
      getMeta();

    }, 3000));

    $scope.$watch('services', function (newValue, oldValue) {
      if (newValue && newValue.length > 10) {
        setTimeout(function () {
          $scope.$apply(function () {
            $scope.selectedService.service = $scope.services[0].value;
            //$scope.selectedService.service = 'fb_website';
          });
        }, 0);
      }
    });

    $scope.getServiceCountries = function () {
      if ($scope.serviceObject && $scope.serviceObject.countries && $scope.serviceObject.countries.length) {
        return _.filter($scope.countries, function (country) {
          return $scope.serviceObject.countries.indexOf(country.id) > -1;
        });
      } else {
        return $scope.countries;
      }
    };
    $scope.$watch('customSpeed', function () {
      if ($scope.serviceObject && $scope.serviceObject.label) {
        return $scope.getServicePrice(false, true);
      }
    })
    $scope.$watch('eatSpeed', function () {
      if ($scope.serviceObject && $scope.serviceObject.label) {
        return $scope.getServicePrice(false, true);
      }
    })
    $scope.$watch('service', function (newValue, oldValue) {
      // Ignore initial setup.
      if (newValue === oldValue) {
        return;
      }

      $scope.selectedCountries.countries = [];

      _.forEach($scope.services, function (service) {
        if (newValue === service.value) {


          $scope.show_speed = Speed.display(service);

          $scope.serviceObject = service;

          serviceUpdater($scope.serviceObject)


          return false;
        }
      });
    });

    $scope.getServiceMultiplier = function (pure) {
      var multiplier = 0;
      if ($scope.serviceObject) {
        multiplier = $scope.serviceObject.multiplier;
      }
      if (pure) {
        return multiplier;
      } else {
        return numeral(multiplier).format('0,0');
      }

    };

    $scope.getServiceCount = function (pure) {
      var count = 0;

      if ($scope.serviceObject && $scope.serviceObject.recurring) {
        count = (typeof $scope.serviceObject.selectedPackage != 'undefined' ? $scope.serviceObject.selectedPackage.quantity : 0);
      } else if ($scope.serviceObject) {
        count = (typeof $scope.quantity != 'undefined' ? $scope.quantity : 0); // * $scope.serviceObject.multiplier;
      }

      if (pure) {
        return count;
      } else {
        return numeral(count).format('0,0');
      }
    };

    $scope.getServicePrice = function (pure) {
      var price = 0;



      if ($scope.serviceObject && $scope.serviceObject.recurring) {
        price = (typeof $scope.serviceObject.selectedPackage != 'undefined' ? $scope.serviceObject.selectedPackage.price : 0);
      } else if ($scope.serviceObject) {

        if ($scope.serviceObject.value == "yt_views_smlite_packages") {

          $scope.serviceObject.price = $scope.min_price ? $scope.min_price : 0;
        }

        price = (typeof $scope.quantity != 'undefined' ? $scope.quantity / $scope.serviceObject.multiplier : 0) * $scope.serviceObject.price;
        /* custom for  yt_usa_new,  Youtube_views_new */
        if ($scope.serviceObject && $scope.serviceObject.value) {
          if ($scope.serviceObject.value == "fb_postlikeslite") {
            console.log("fbpostlikes")
            if ($scope.quantity >= $scope.serviceObject.minimum && $scope.quantity <= 100) price = 1.99;
          }
          if ($scope.serviceObject.value == "ig_likes" || $scope.serviceObject.value == "ig_usalikes") {

            if ($scope.quantity >= 20 && $scope.quantity <= 100) price = 0.99;
          }
          if ($scope.serviceObject.value == "Youtube_views_new" || $scope.serviceObject.value == "yt_usa_new") {

            if ($scope.eatSpeed == "FA") price = $scope.serviceObject.price * 0.50 + $scope.serviceObject.price;
            if ($scope.eatSpeed == "SF") price = $scope.serviceObject.price * 0.75 + $scope.serviceObject.price;



          }
        }
      }
      if ($scope.fasterSpeed) price = price + (price * 2);

      if ($scope.serviceObject && $scope.serviceObject.discount) {
        var range = false;
        $scope.serviceObject.discount_settings.sort(function (a, b) {
          return a.price - b.price;
        });
        for (var i in $scope.serviceObject.discount_settings) {
          if ($scope.serviceObject.discount_settings[i].price) {
            if (price >= $scope.serviceObject.discount_settings[i].price) range = i
          }

        }
        if (range) {
          var tmp_qty = $scope.serviceObject.discount_settings[range].percentage / 100;
          var qty = $scope.quantity * tmp_qty;
          $scope.totalQuantity = qty;
        } else $scope.totalQuantity = false;
      }

      if (pure) {
        return price;
      } else {
        return numeral(price).format('$0,0.00');
      }
    };

    $scope.getOrderPrice = function () {
      if (!this.order) return 0;
      if (Global.hasRole('provider')) {
        if (this.order.provider) {
          return this.order.price * this.order.provider.profit;
        } else {
          return this.order.price * Global.user.profit;
        }
      } else {
        return this.order.price;
      }
    };

    $scope.getOrderServicePrice = function () {
      if (!this.order) return 0;
      if (Global.hasRole('provider')) {
        if (this.order.provider) {
          return this.order.service.price * this.order.provider.profit;
        } else {
          return this.order.price * Global.user.profit;
        }
      } else {
        return this.order.service.price;
      }
    };


    $scope.getCommentHtml = function (contents) {
      return $sce.trustAsHtml(contents);
    }

    $scope.countriesToString = function () {
      if (!$scope.order) return '';
      var output = [];
      _.each($scope.order.countries, function (country) {
        _.each($scope.countries, function (countryOption) {
          if (country == countryOption.id) {
            output.push(countryOption.text);
            return false;
          }
        });
      });
      return output.join(', ');
    }


    function checkRecurring() {
      if (localStorage.getItem('data')) {
        var data = JSON.parse(localStorage.getItem('data'));
        return data[0].recurring;
      }
      return false;
    }



    $scope.countries = [{
        id: "AF",
        text: "Afghanistan"
      },
      {
        id: "AX",
        text: "Aland Islands"
      },
      {
        id: "AL",
        text: "Albania"
      },
      {
        id: "DZ",
        text: "Algeria"
      },
      {
        id: "AS",
        text: "American Samoa"
      },
      {
        id: "AD",
        text: "Andorra"
      },
      {
        id: "AO",
        text: "Angola"
      },
      {
        id: "AI",
        text: "Anguilla"
      },
      {
        id: "AQ",
        text: "Antarctica"
      },
      {
        id: "AG",
        text: "Antigua and Barbuda"
      },
      {
        id: "AR",
        text: "Argentina"
      },
      {
        id: "AM",
        text: "Armenia"
      },
      {
        id: "AW",
        text: "Aruba"
      },
      {
        id: "AP",
        text: "Asia/Pacific Region"
      },
      {
        id: "AU",
        text: "Australia"
      },
      {
        id: "AT",
        text: "Austria"
      },
      {
        id: "AZ",
        text: "Azerbaijan"
      },
      {
        id: "BS",
        text: "Bahamas"
      },
      {
        id: "BH",
        text: "Bahrain"
      },
      {
        id: "BD",
        text: "Bangladesh"
      },
      {
        id: "BB",
        text: "Barbados"
      },
      {
        id: "BY",
        text: "Belarus"
      },
      {
        id: "BE",
        text: "Belgium"
      },
      {
        id: "BZ",
        text: "Belize"
      },
      {
        id: "BJ",
        text: "Benin"
      },
      {
        id: "BM",
        text: "Bermuda"
      },
      {
        id: "BT",
        text: "Bhutan"
      },
      {
        id: "BO",
        text: "Bolivia"
      },
      {
        id: "BQ",
        text: "Bonaire, Saint Eustatius and Saba"
      },
      {
        id: "BA",
        text: "Bosnia and Herzegovina"
      },
      {
        id: "BW",
        text: "Botswana"
      },
      {
        id: "BV",
        text: "Bouvet Island"
      },
      {
        id: "BR",
        text: "Brazil"
      },
      {
        id: "IO",
        text: "British Indian Ocean Territory"
      },
      {
        id: "BN",
        text: "Brunei Darussalam"
      },
      {
        id: "BG",
        text: "Bulgaria"
      },
      {
        id: "BF",
        text: "Burkina Faso"
      },
      {
        id: "BI",
        text: "Burundi"
      },
      {
        id: "KH",
        text: "Cambodia"
      },
      {
        id: "CM",
        text: "Cameroon"
      },
      {
        id: "CA",
        text: "Canada"
      },
      {
        id: "CV",
        text: "Cape Verde"
      },
      {
        id: "KY",
        text: "Cayman Islands"
      },
      {
        id: "CF",
        text: "Central African Republic"
      },
      {
        id: "TD",
        text: "Chad"
      },
      {
        id: "CL",
        text: "Chile"
      },
      {
        id: "CN",
        text: "China"
      },
      {
        id: "CX",
        text: "Christmas Island"
      },
      {
        id: "CC",
        text: "Cocos (Keeling) Islands"
      },
      {
        id: "CO",
        text: "Colombia"
      },
      {
        id: "KM",
        text: "Comoros"
      },
      {
        id: "CG",
        text: "Congo"
      },
      {
        id: "CD",
        text: "Congo, The Democratic Republic of the"
      },
      {
        id: "CK",
        text: "Cook Islands"
      },
      {
        id: "CR",
        text: "Costa Rica"
      },
      {
        id: "CI",
        text: "Cote d&#039;Ivoire"
      },
      {
        id: "HR",
        text: "Croatia"
      },
      {
        id: "CU",
        text: "Cuba"
      },
      {
        id: "CW",
        text: "Curacao"
      },
      {
        id: "CY",
        text: "Cyprus"
      },
      {
        id: "CZ",
        text: "Czech Republic"
      },
      {
        id: "DK",
        text: "Denmark"
      },
      {
        id: "DJ",
        text: "Djibouti"
      },
      {
        id: "DM",
        text: "Dominica"
      },
      {
        id: "DO",
        text: "Dominican Republic"
      },
      {
        id: "EC",
        text: "Ecuador"
      },
      {
        id: "EG",
        text: "Egypt"
      },
      {
        id: "SV",
        text: "El Salvador"
      },
      {
        id: "GQ",
        text: "Equatorial Guinea"
      },
      {
        id: "ER",
        text: "Eritrea"
      },
      {
        id: "EE",
        text: "Estonia"
      },
      {
        id: "ET",
        text: "Ethiopia"
      },
      {
        id: "EU",
        text: "Europe"
      },
      {
        id: "FK",
        text: "Falkland Islands (Malvinas)"
      },
      {
        id: "FO",
        text: "Faroe Islands"
      },
      {
        id: "FJ",
        text: "Fiji"
      },
      {
        id: "FI",
        text: "Finland"
      },
      {
        id: "FR",
        text: "France"
      },
      {
        id: "GF",
        text: "French Guiana"
      },
      {
        id: "PF",
        text: "French Polynesia"
      },
      {
        id: "TF",
        text: "French Southern Territories"
      },
      {
        id: "GA",
        text: "Gabon"
      },
      {
        id: "GM",
        text: "Gambia"
      },
      {
        id: "GE",
        text: "Georgia"
      },
      {
        id: "DE",
        text: "Germany"
      },
      {
        id: "GH",
        text: "Ghana"
      },
      {
        id: "GI",
        text: "Gibraltar"
      },
      {
        id: "GR",
        text: "Greece"
      },
      {
        id: "GL",
        text: "Greenland"
      },
      {
        id: "GD",
        text: "Grenada"
      },
      {
        id: "GP",
        text: "Guadeloupe"
      },
      {
        id: "GU",
        text: "Guam"
      },
      {
        id: "GT",
        text: "Guatemala"
      },
      {
        id: "GG",
        text: "Guernsey"
      },
      {
        id: "GN",
        text: "Guinea"
      },
      {
        id: "GW",
        text: "Guinea-Bissau"
      },
      {
        id: "GY",
        text: "Guyana"
      },
      {
        id: "HT",
        text: "Haiti"
      },
      {
        id: "HM",
        text: "Heard Island and McDonald Islands"
      },
      {
        id: "VA",
        text: "Holy See (Vatican City State)"
      },
      {
        id: "HN",
        text: "Honduras"
      },
      {
        id: "HK",
        text: "Hong Kong"
      },
      {
        id: "HU",
        text: "Hungary"
      },
      {
        id: "IS",
        text: "Iceland"
      },
      {
        id: "IN",
        text: "India"
      },
      {
        id: "ID",
        text: "Indonesia"
      },
      {
        id: "IR",
        text: "Iran, Islamic Republic of"
      },
      {
        id: "IQ",
        text: "Iraq"
      },
      {
        id: "IE",
        text: "Ireland"
      },
      {
        id: "IM",
        text: "Isle of Man"
      },
      {
        id: "IL",
        text: "Israel"
      },
      {
        id: "IT",
        text: "Italy"
      },
      {
        id: "JM",
        text: "Jamaica"
      },
      {
        id: "JP",
        text: "Japan"
      },
      {
        id: "JE",
        text: "Jersey"
      },
      {
        id: "JO",
        text: "Jordan"
      },
      {
        id: "KZ",
        text: "Kazakhstan"
      },
      {
        id: "KE",
        text: "Kenya"
      },
      {
        id: "KI",
        text: "Kiribati"
      },
      {
        id: "KW",
        text: "Kuwait"
      },
      {
        id: "KG",
        text: "Kyrgyzstan"
      },
      {
        id: "LA",
        text: "Lao People&#039;s Democratic Republic"
      },
      {
        id: "LV",
        text: "Latvia"
      },
      {
        id: "LB",
        text: "Lebanon"
      },
      {
        id: "LS",
        text: "Lesotho"
      },
      {
        id: "LR",
        text: "Liberia"
      },
      {
        id: "LY",
        text: "Libyan Arab Jamahiriya"
      },
      {
        id: "LI",
        text: "Liechtenstein"
      },
      {
        id: "LT",
        text: "Lithuania"
      },
      {
        id: "LU",
        text: "Luxembourg"
      },
      {
        id: "MO",
        text: "Macao"
      },
      {
        id: "MK",
        text: "Macedonia"
      },
      {
        id: "MG",
        text: "Madagascar"
      },
      {
        id: "MW",
        text: "Malawi"
      },
      {
        id: "MY",
        text: "Malaysia"
      },
      {
        id: "MV",
        text: "Maldives"
      },
      {
        id: "ML",
        text: "Mali"
      },
      {
        id: "MT",
        text: "Malta"
      },
      {
        id: "MH",
        text: "Marshall Islands"
      },
      {
        id: "MQ",
        text: "Martinique"
      },
      {
        id: "MR",
        text: "Mauritania"
      },
      {
        id: "MU",
        text: "Mauritius"
      },
      {
        id: "YT",
        text: "Mayotte"
      },
      {
        id: "MX",
        text: "Mexico"
      },
      {
        id: "FM",
        text: "Micronesia, Federated States of"
      },
      {
        id: "MD",
        text: "Moldova, Republic of"
      },
      {
        id: "MC",
        text: "Monaco"
      },
      {
        id: "MN",
        text: "Mongolia"
      },
      {
        id: "ME",
        text: "Montenegro"
      },
      {
        id: "MS",
        text: "Montserrat"
      },
      {
        id: "MA",
        text: "Morocco"
      },
      {
        id: "MZ",
        text: "Mozambique"
      },
      {
        id: "MM",
        text: "Myanmar"
      },
      {
        id: "NA",
        text: "Namibia"
      },
      {
        id: "NR",
        text: "Nauru"
      },
      {
        id: "NP",
        text: "Nepal"
      },
      {
        id: "NL",
        text: "Netherlands"
      },
      {
        id: "NC",
        text: "New Caledonia"
      },
      {
        id: "NZ",
        text: "New Zealand"
      },
      {
        id: "NI",
        text: "Nicaragua"
      },
      {
        id: "NE",
        text: "Niger"
      },
      {
        id: "NG",
        text: "Nigeria"
      },
      {
        id: "NU",
        text: "Niue"
      },
      {
        id: "NF",
        text: "Norfolk Island"
      },
      {
        id: "KP",
        text: "North Korea"
      },
      {
        id: "MP",
        text: "Northern Mariana Islands"
      },
      {
        id: "NO",
        text: "Norway"
      },
      {
        id: "OM",
        text: "Oman"
      },
      {
        id: "PK",
        text: "Pakistan"
      },
      {
        id: "PW",
        text: "Palau"
      },
      {
        id: "PS",
        text: "Palestinian Territory"
      },
      {
        id: "PA",
        text: "Panama"
      },
      {
        id: "PG",
        text: "Papua New Guinea"
      },
      {
        id: "PY",
        text: "Paraguay"
      },
      {
        id: "PE",
        text: "Peru"
      },
      {
        id: "PH",
        text: "Philippines"
      },
      {
        id: "PN",
        text: "Pitcairn"
      },
      {
        id: "PL",
        text: "Poland"
      },
      {
        id: "PT",
        text: "Portugal"
      },
      {
        id: "PR",
        text: "Puerto Rico"
      },
      {
        id: "QA",
        text: "Qatar"
      },
      {
        id: "RE",
        text: "Reunion"
      },
      {
        id: "RO",
        text: "Romania"
      },
      {
        id: "RU",
        text: "Russian Federation"
      },
      {
        id: "RW",
        text: "Rwanda"
      },
      {
        id: "BL",
        text: "Saint Bartelemey"
      },
      {
        id: "SH",
        text: "Saint Helena"
      },
      {
        id: "KN",
        text: "Saint Kitts and Nevis"
      },
      {
        id: "LC",
        text: "Saint Lucia"
      },
      {
        id: "MF",
        text: "Saint Martin"
      },
      {
        id: "PM",
        text: "Saint Pierre and Miquelon"
      },
      {
        id: "VC",
        text: "Saint Vincent and the Grenadines"
      },
      {
        id: "WS",
        text: "Samoa"
      },
      {
        id: "SM",
        text: "San Marino"
      },
      {
        id: "ST",
        text: "Sao Tome and Principe"
      },
      {
        id: "SA",
        text: "Saudi Arabia"
      },
      {
        id: "SN",
        text: "Senegal"
      },
      {
        id: "RS",
        text: "Serbia"
      },
      {
        id: "SC",
        text: "Seychelles"
      },
      {
        id: "SL",
        text: "Sierra Leone"
      },
      {
        id: "SG",
        text: "Singapore"
      },
      {
        id: "SX",
        text: "Sint Maarten"
      },
      {
        id: "SK",
        text: "Slovakia"
      },
      {
        id: "SI",
        text: "Slovenia"
      },
      {
        id: "SB",
        text: "Solomon Islands"
      },
      {
        id: "SO",
        text: "Somalia"
      },
      {
        id: "ZA",
        text: "South Africa"
      },
      {
        id: "GS",
        text: "South Georgia and the South Sandwich Islands"
      },
      {
        id: "KR",
        text: "South Korea"
      },
      {
        id: "ES",
        text: "Spain"
      },
      {
        id: "LK",
        text: "Sri Lanka"
      },
      {
        id: "SD",
        text: "Sudan"
      },
      {
        id: "SR",
        text: "Suriname"
      },
      {
        id: "SJ",
        text: "Svalbard and Jan Mayen"
      },
      {
        id: "SZ",
        text: "Swaziland"
      },
      {
        id: "SE",
        text: "Sweden"
      },
      {
        id: "CH",
        text: "Switzerland"
      },
      {
        id: "SY",
        text: "Syrian Arab Republic"
      },
      {
        id: "TW",
        text: "Taiwan"
      },
      {
        id: "TJ",
        text: "Tajikistan"
      },
      {
        id: "TZ",
        text: "Tanzania, United Republic of"
      },
      {
        id: "TH",
        text: "Thailand"
      },
      {
        id: "TL",
        text: "Timor-Leste"
      },
      {
        id: "TG",
        text: "Togo"
      },
      {
        id: "TK",
        text: "Tokelau"
      },
      {
        id: "TO",
        text: "Tonga"
      },
      {
        id: "TT",
        text: "Trinidad and Tobago"
      },
      {
        id: "TN",
        text: "Tunisia"
      },
      {
        id: "TR",
        text: "Turkey"
      },
      {
        id: "TM",
        text: "Turkmenistan"
      },
      {
        id: "TC",
        text: "Turks and Caicos Islands"
      },
      {
        id: "TV",
        text: "Tuvalu"
      },
      {
        id: "UG",
        text: "Uganda"
      },
      {
        id: "UA",
        text: "Ukraine"
      },
      {
        id: "AE",
        text: "United Arab Emirates"
      },
      {
        id: "GB",
        text: "United Kingdom"
      },
      {
        id: "US",
        text: "United States"
      },
      {
        id: "UM",
        text: "United States Minor Outlying Islands"
      },
      {
        id: "UY",
        text: "Uruguay"
      },
      {
        id: "UZ",
        text: "Uzbekistan"
      },
      {
        id: "VU",
        text: "Vanuatu"
      },
      {
        id: "VE",
        text: "Venezuela"
      },
      {
        id: "VN",
        text: "Vietnam"
      },
      {
        id: "VG",
        text: "Virgin Islands, British"
      },
      {
        id: "VI",
        text: "Virgin Islands, U.S."
      },
      {
        id: "WF",
        text: "Wallis and Futuna"
      },
      {
        id: "EH",
        text: "Western Sahara"
      },
      {
        id: "YE",
        text: "Yemen"
      },
      {
        id: "ZM",
        text: "Zambia"
      },
      {
        id: "ZW",
        text: "Zimbabwe"
      }
    ];




    $scope.create = function (method) {
      var $dialog = null
      var self = this;
      var payment;
      $scope.disableButton = true;
      if ($scope.user_price) {
        $scope.email = $scope.user_price.email;
      }
      if ($scope.url && !$scope.url.match(/^https?/)) {
        $scope.url = 'http://' + $scope.url;
      }
      async.waterfall([
        function (callback) {
          payment = new Payments({
            price: $scope.add_cart.price,
            recurring: false,
            user: Global.user.email,
            cart: (method) ? $scope.add_cart.cart : [],
            url: (method) ? $scope.add_cart.url : self.url,
          });

          payment.$save(
            // success
            function (payment) {
              console.log('Payment data saved ', payment);
              callback(null, payment);
            },
            // error
            function (response) {
              vex.dialog.alert(response.data);
              callback(new Error('Error while initiating payment process'));
            }
          );

        },
        function (payment, callback) {
          $scope.id_array = payment.id_array;
          $scope.id_array = ($scope.id_array.length > 1) ? $scope.id_array.join(',') : $scope.id_array[0];
          var id_array = payment.id_array;
          payment = payment.payment;
          //  $dialog = vex.dialog.alert("Please wait while we create your orders!");
          var facebook_tracking_price_copy = self.getServicePrice(true);
          var order = new Orders({
            url: (method) ? $scope.add_cart.url : self.url,
            quantity: (method) ? $scope.add_cart.quantity : self.quantity,
            price: $scope.add_cart.price,
            orderCount: (method) ? $scope.add_cart.orderCount : self.getServiceCount(true),
            service: (method) ? $scope.add_cart.service : self.serviceObject,
            payment: payment._id,
            countries: self.selectedCountries.countries,
            discount: (method) ? $scope.add_cart.discount : $scope.discountCode,
            recurring: false,
          });
          console.log('order start');
          order.$save(function (response) {
            console.log('order save process', response);
            // (function loop(i) {
            //   console.log('order save process', response);
            //   if (i == $scope.add_cart.cart.length) {
            //     return buyContinue();
            //   }
            //   var order_cart = new Orders({
            //     url: $scope.add_cart.cart[i].url,
            //     quantity: $scope.add_cart.cart[i].quantity,
            //     price: $scope.add_cart.cart[i].price,
            //     orderCount: $scope.add_cart.cart[i].orderCount,
            //     service: $scope.add_cart.cart[i].service,
            //     payment: id_array[i],
            //     countries: $scope.add_cart.cart[i].countries,
            //     discount: $scope.add_cart.cart[i].discount || false,
            //     recurring: false,
            //     cart: false,
            //     speed_service: $scope.add_cart.cart[i].speed_service,
            //     ig_comments: $scope.add_cart.cart[i].comments,
            //     hashtag: $scope.add_cart.cart[i].hashtag,
            //     followers: $scope.add_cart.cart[i].followers,
            //     ig_media: $scope.add_cart.cart[i].ig_media,
            //     ig_mentions: $scope.add_cart.cart[i].ig_mentions,
            //     eat_speed: $scope.add_cart.cart[i].eatSpeed,
            //     emoticon: $scope.add_cart.cart[i].emoticon,
            //     dropdown_index: $scope.add_cart.cart[i].dropdown_index ? $scope.add_cart.cart[i].dropdown_index : null,
            //     indexer: $scope.indexer
            //   });
            //   order_cart.save(function (data) {
            //     console.log('order saved ' + i, data);
            //     return loop(i + 1);
            //   }, function (err) {
            //     console.log(err);
            //     if (response.status === 403) {
            //       vex.dialog.buttons.YES.text = 'Yes please!';
            //       vex.dialog.buttons.NO.text = 'No, thanks';
            //
            //       vex.dialog.alert('An error occured while processing your order');
            //       $scope.disableButton = false;
            //     } else {
            //       vex.dialog.buttons.YES.text = 'OK';
            //       vex.dialog.alert(err.data);
            //       $scope.disableButton = false;
            //     }
            //
            //   })
            //
            //
            // })(0);




/*
            function buyContinue() {

              self.url = '';
              self.quantity = 0;
              self.fullMeta = null;
              self.price = 0;
              self.serviceObject = '';

              $scope.disableButton = false;
              var url = null;
              if (method) {
                if (method == 'cc') $scope.paymentMethod = 'CreditCard';
                else $scope.paymentMethod = 'Paypal';

                // $scope.cart_window(true);
              }
              $scope.redirect_checkout = window.location.origin + '/return/' + payment._id.toString();
              $scope.checkout_id = payment._id.toString();
              if (method) url = window.location.origin + '/purchase/' + payment._id + '?origin=' + encodeURIComponent(window.location.origin) + '&paymentMethod=' + $scope.paymentMethod + '&add_cart=true&id_array=' + id_array;
              else url = window.location.origin + '/purchase/' + payment._id + '?origin=' + encodeURIComponent(window.location.origin) + '&paymentMethod=' + $scope.paymentMethod;

              console.log('Payment link created', payment);
              if (payment.needs_activation) {
                console.log('Account not active', payment);
                var query = {
                  url: url,
                  user: payment.user
                };
                Orders.user_order({
                  data: query
                }, function () {

                  $scope.disableCart(true);
                  vex.dialog.alert("Account details and password were sent to your email with a verification link, once you click on the link you will then be redirected to the payment page.");
                  return;

                }, function (err) {
                  $scope.disableCart(true);
                  vex.dialog.alert(err.data);
                });

              } else {

                console.log('Account active, redirecting....', payment);
                localStorage.setItem('data', []);
                window.top.location.href = url;





              }
            }*/


          });







        }
      ]);
    };








    $scope.hasUnread = function (order) {
      if (Global.hasRole('admin')) {
        return order.unread.administrator;
      } else if (Global.hasRole('provider')) {
        return order.unread.provider;
      } else {
        return order.unread.customer;
      }
    };

    $scope.remove = function (order) {
      vex.dialog.buttons.YES.text = 'Yes, delete it';
      vex.dialog.buttons.NO.text = 'Oops, no!';

      vex.dialog.confirm({
        message: 'You\'re really sure you want to delete this order? This action is irreversible.',
        callback: function (yes) {
          if (yes) {
            if (order) {
              order.$remove();

              for (var i in $scope.orders) {
                if ($scope.orders[i] === order) {
                  $scope.orders.splice(i, 1);
                }
              }
            } else {
              $scope.order.$remove();
              $location.path('orders');
            }
          }
        }
      });
    };

    $scope.cancel = function (order) {
      vex.dialog.buttons.YES.text = 'Cancel the order';
      vex.dialog.buttons.NO.text = 'Sorry, my mistake!';

      vex.dialog.prompt({
        message: 'Should I go ahead and cancel this order?',
        placeholder: 'Please state the reason for cancellation',
        callback: function (reason) {
          if (reason) {
            $scope.order.$cancel({
              reason: reason
            });
          }
        }
      });
    };

    $scope.complete = function (order) {
      var self = this;
      $scope.order.$complete({}, function (order) {
        if (order.afterCount < order.beforeCount + order.orderCount && order.beforeCount > 0) {
          vex.dialog.alert('Our system hasn\'t detected the right amount of views on this order upon completion. This order will be sent to review by an administrator. Your account will be credited only after this order has been verified to have been delivered.');
        } else {
          Global.user.balance = Global.user.balance + $scope.getOrderPrice();
        }
      });
    };

    $scope.approve = function (order) {
      var self = this;
      $scope.order.$approve({}, function (order) {});
    };

    $scope.unapprove = function (order) {
      var self = this;

    };

    $scope.claim = function (order) {
      $scope.order.$claim();
    };

    $scope.sendMessage = function (message) {

      if (_.isEmpty(message)) {
        vex.dialog.alert('Please enter some text for your message');
        return;
      }

      var newComment = {
        text: message.$content[0].innerHTML,
        date: new Date(),
        user: Global.user
      };


      $http.post('/orders/' + $scope.order._id + '/comment', newComment)
        .success(function (data, status) {
          if (!_.isArray($scope.order.comments)) {
            $scope.order.comments = [];
          }

          if ($scope.comments.indexOf($scope.newComment) === -1) {
            $scope.order.comments.push(newComment);
            $scope.comments.push($scope.newComment);
          }
          $scope.newOrderComment = '';
          $scope.messageSent.disabled = false;
        })
        .error(function (data, status) {
          vex.dialog.alert('Sorry, but we could not post your message at the moment');
        });

    };

    $scope.find = function () {
      $scope.tableParams = new ngTableParams({
        page: 1,
        count: 50,
        sorting: {
          "created": "desc"
        }
      }, {
        total: 0,
        getData: function ($defer, params) {
          Orders.query(params.url(), params.$params.filter, function (result) {
            if (result.session) return window.location.reload();

            $scope.orders = result.data;            
            params.total(result.total);
            $defer.resolve(result.data);
          });
        }
      });
    };

    $scope.findOne = function () {

      Orders.get({
        orderId: $stateParams.orderId
      }, function (order) {
        console.log(order)
        if (!order.cart) {
          $scope.comments = [];
          $scope.realComments = [];


          _.forEach(order.comments, function (value, key) {

            if ($scope.comments.indexOf(value.text) === -1) {
              $scope.comments.push(value.text);
              $scope.realComments.push(value);
            }
            if (key === order.comments.length - 1) order.comments = $scope.realComments;

          })

          $scope.order = order;
          $scope.ig_comments = order.ig_comments_emoticon ? order.ig_comments_emoticon : order.ig_comments;
          $scope.ig_comments = (order.ig_comments) ? JSON.parse($scope.ig_comments) : [];
          $scope.ig_mentions = (order.ig_mentions) ? JSON.parse(order.ig_mentions) : [];


          Services.query(function (result) {
            $scope.view_service = _.filter(result.data, function (service) {
              if (service.value == $scope.order.service.value) {
                $scope.countryList = (service && service.dropdown) ? JSON.parse(service.dropdown) : [];
              }

            })

          });

        } else {
          $scope.order = order.cart;
          $scope.order.cart = true;
          $scope.orderParams = new ngTableParams({
            page: 1,
            count: 100
          }, {
            counts: [],
            total: order.cart.length,
            getData: function ($defer, params) {
              $defer.resolve(order.cart)
            }
          });
        }

      });
    };

    $scope.getOrderPriceCart = function () {
      if (!this.order) return 0;

      var totalPrice = 0.0;
      for (var i in this.order) {
        if (this.order[i])
          if (!isNaN(parseFloat(this.order[i].price))) totalPrice += parseFloat(this.order[i].price);

      }
      return totalPrice;

    }

    $scope.servicesStatus = function (column) {
      var promise = $q.defer();
      var list = [];
      list.push({
        id: {
          paid: false,
          checkout: false
        },
        title: 'Abandoned'
      })
      list.push({
        id: {
          paid: true,
          completed: false,
          cancelled: false
        },
        title: 'In Progress'
      })
      list.push({
        id: {
          paid: true,
          completed: true,
          cancelled: false
        },
        title: 'Completed'
      })
      list.push({
        id: {
          cancelled: true
        },
        title: 'Cancelled'
      })
      list.push({
        id: {
          refund: true,
          refunded: false
        },
        title: 'Failed to deliver'
      })
      list.push({
        id: {
          refunded: true
        },
        title: 'Refunded'
      })

      promise.resolve(list);
      return promise;
    }

    $scope.servicesFilter = function (column) {
      $scope.initCreate();
      var promise = $q.defer();
      var list = [];

      var timeout = setInterval(function () {
        if ($scope.services) {
          _.each($scope.services, function (service) {
            //_.each(vendor, function(service){
            list.push({
              id: service.value,
              title: $filter('cut')(service.label, true, 45)
            });
            //});
          });
          promise.resolve(list);
          clearInterval(timeout);
        }
      }, 10);

      return promise;
    }

    $scope.groupSort = function (groups) {
      groups = groups.reverse();

      if ($location.search().prioritize_group && $location.search().prioritize_group.length) {
        groups = _.sortBy(groups, function (group) {
          if (group.name === $location.search().prioritize_group) {
            return 0;
          } else {
            return 1;
          }
        });
      }
      for (var i in groups) {
        groups[i].totalPriority = 0;
        for (var j in groups[i].items) {
          groups[i].totalPriority += parseInt(groups[i].items[j].priority);
        }
      }
      groups.sort(function (a, b) {
        return b.totalPriority - a.totalPriority;
      });


      if (!$location.search().prioritize_group) {

        for (var i in groups) {
          groups[i].items.sort(function (a, b) {
            if (!a.admin_priority) a.admin_priority = 0;
            if (!b.admin_priority) b.admin_priority = 0;
            if (a.admin_priority == 0 && b.admin_priority == 0) return;


            if (a.admin_priority != 0 && b.admin_priority == 0) return -1;

            if (a.admin_priority == 0 && b.admin_priority != 0) return 1;

            else return a.admin_priority - b.admin_priority;

          })



        }


        return groups;
      } else return groups;


    };

    $scope.groupByCategory = function (item) {
      return item.category || 'Other Services';
    };

    $scope.cancelDiscount = function () {
      $scope.discount = undefined;
      $scope.discountCode = '';
    };

    $scope.applyDiscount = function () {
      if (!$scope.discountCode) {
        return;
      }

      Codes.get({
        discountName: $scope.discountCode
      }, function (discount) {
        if (discount && _.contains(discount.services, $scope.selectedService.service)) {
          if ($scope.getServicePrice(true) < discount.min_price) {
            $scope.discountCode = null;
            vex.dialog.alert("This discount can only be applied if you purchase over $" + discount.min_price + " worth of value");
            return false;
          }

          $scope.discount = discount;
        } else {
          vex.dialog.alert('Service mismatch!');
        }
      });
    };

    $scope.getServicePriceWithDiscount = function (pure) {

      var price = $scope.getServicePrice(true);
      $scope.price_watcher = $scope.getServicePrice(true);
      if (!_.isEmpty($scope.discountCode) && $scope.discount && _.contains($scope.discount.services, $scope.selectedService.service)) {
        price = price - parseFloat($scope.discount.price);
        price = price - (price * parseFloat($scope.discount.percent));
      }

      if (pure) {
        return price;
      } else {
        return numeral(price).format('$0,0.00');
      }
    };


    $scope.updateUrl = function (id, url, customer) {

      var db = customer ? "customer_url" : "url";

      Orders[db]({
        order_id: id,
        url: url
      }, function () {
        vex.dialog.alert('URL updated!');
        $state.go($state.current.name, {}, {
          reload: true
        })
      }, function (err) {
        if (!err.data.msg) err.data.msg = "Unable to deliver, contact with admin.";
        vex.dialog.alert(err.data.msg);
      });




    }

    $scope.edit_order = function () {



      var order = $scope.order;

      order.url = $scope.url;
      order.price = $scope.serviceObject.selectedPackage ? $scope.serviceObject.selectedPackage.price : $scope.getServicePriceWithDiscount(true);
      order.orderCount = $scope.serviceObject.selectedPackage ? $scope.serviceObject.selectedPackage.quantity : $scope.quantity;
      order.user.balance = $scope.order.user.balance;
      order.user._id = $scope.order.user._id

      order.service.label = $scope.serviceObject.label;
      order.service.value = $scope.serviceObject.value;
      if (!order.updated) {
        order.updated = [];
      }
      order.updated.push(new Date().getTime());

      order.$update(function (result) {



        $location.path('/orders');
      });


    }

    $scope.save_new_order = function () {


      var order = new Orders({
        url: $scope.url,
        quantity: $scope.quantity,
        iframely: $scope.fullMeta,
        price: $scope.getServicePrice(true),
        orderCount: $scope.getServiceCount(true),
        service: $scope.serviceObject,
        countries: $scope.selectedCountries.countries,
        discount: $scope.discountCode,
        recurring: checkRecurring() || false,
        user: $scope.user_price._id

      });


      // order.user  = $scope.user;

      order.$admin_create(function (crap) {

      });
    }
    $scope.$watch('price_watcher', function (newValue) {


      if ($scope.user_price) {


        $scope.user_price.balance = $scope.user_price.balance + $scope.order.price - $scope.getServicePriceWithDiscount(true);
        $scope.order.price = $scope.getServicePriceWithDiscount(true);


        return $scope.user_price.balance;


      }


      if ($scope.order) {


        $scope.order.user.balance = $scope.order.user.balance + $scope.order.price - $scope.getServicePriceWithDiscount(true);
        $scope.order.price = $scope.getServicePriceWithDiscount(true);


        return $scope.order.user.balance;




      }
    }, true);

    $scope.getUser = function () {

      Orders.get({
        orderId: $stateParams.orderId
      }, function (order) {

        Users.get({
          userId: order.user._id
        }, function (user) {
          if (!user.profits) user.profits = {};
          $scope.user_price = user;
          $scope.order.price = 0; // hoax
        });
      });


    }







    $scope.priceUpdater = function (value) {

      setTimeout(function () {
        Orders.get({
          orderId: $stateParams.orderId
        }, function (order) {

          $scope.quantity = order.orderCount;
          $scope.url = order.url;
          $scope.service_label = order.service.label;
          $scope.selectedService.service = order.service.value;
          _.forEach($scope.services, function (service) {
            if (order.service.value === service.value) {

              $scope.serviceObject = service;

              $scope.serviceObject.selectedPackage = $scope.serviceObject.packages[0];
              $scope.serviceObject.selectedPackage.quantity = order.orderCount;
              $scope.serviceObject.selectedPackage.label = $scope.serviceObject.selectedPackage.label.split(' ')[1];

              $scope.serviceObject.selectedPackage.label = order.orderCount + ' ' + $scope.serviceObject.selectedPackage.label;
              $scope.serviceObject.selectedPackage.price = order.price;

              $scope.getServicePriceWithDiscount(true);
              return true;
            }
          });



        });



      }, 300);




    }
    $scope.createServiceList = function () {



      $scope.initCreate();
      var promise = $q.defer();
      $scope.service_list = [];

      var timeout = setInterval(function () {
        if ($scope.services) {
          _.each($scope.services, function (service) {
            //_.each(vendor, function(service){
            $scope.service_list.push({
              id: service.value,
              title: $filter('cut')(service.label, true, 45)
            });
            //});
          });
          promise.resolve($scope.service_list);
          clearInterval(timeout);
        }
      }, 10);

      return promise;











    }










  }
])