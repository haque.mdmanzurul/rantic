'use strict';

//Orders service used for orders REST endpoint
angular.module('mean.orders').factory('Orders', ['$resource', function ($resource) {
    return $resource('orders/:orderId', {
        orderId: '@_id'
    }, {
        abandoned: {
            method: "POST",
            url: "orders/abandoned_paid"
        },
        edit_status: {
            method: "POST",
            url: 'orders/edit_status'
        },
        edit_count: {
            method: "POST",
            url: 'orders/edit_count'
        },
        update: {
            method: 'PUT',
            url: 'orders/:orderId'
        },
        admin_create: {
            method: "POST",
            url: 'orders/admin_create'
        },
        query: {
            method: 'GET',
            isArray: false
        },
        claim: {
            method: 'POST',
            url: 'orders/:orderId/claim'
        },
        cancel: {
            method: 'POST',
            url: 'orders/:orderId/cancel'
        },
        complete: {
            method: 'POST',
            url: 'orders/:orderId/complete'
        },
        approve: {
            method: 'POST',
            url: 'orders/:orderId/approve'
        },
        unapprove: {
            method: 'POST',
            url: 'orders/:orderId/unapprove'
        },
        fb_category: {
            method: 'GET',
            url: '/fb_category'
        },
        change_api: {
            method: 'POST',
            url: '/orders/api_change'
        },
        instagram: {
            method: 'GET',
            url: '/instagram-hashtag'
        },
        reorder: {
            method: 'POST',
            url: 'orders/re_order'
        },
        returnBalance: {
            method: 'POST',
            url: 'orders/return_balance'
        },
        returnBalance_customer: {
            method: 'POST',
            url: 'orders/return_balance_customer'
        },

        refunded: {
            method: 'POST',
            url: 'orders/refunded'
        },

        url: {
            method: 'POST',
            url: 'orders/url'

        },
        customer_url: {
            method: 'POST',
            url: 'orders/customer_url'

        },
        duplicates: {
            method: "POST",
            url: "/orders/inprogress"
        },
        checkOrder: {
            method: "GET",
            url: "/orders/check"
        }

    });
}]);