'use strict';

//Setting up route
angular.module('mean.orders').config(['$stateProvider',
    function ($stateProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function ($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function (user) {
                // Authenticated
                if (user !== '0')
                    $timeout(deferred.resolve, 0);

                // Not Authenticated
                else {
                    $timeout(function () {
                        deferred.reject();
                    }, 0);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('all orders', {
                url: '/orders',
                templateUrl: 'public/orders/views/list2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create order', {
                url: '/orders/create',
                templateUrl: 'public/orders/views/create2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            /* .state('edit order', {
                url: '/orders/:orderId/edit',
                templateUrl: 'public/orders/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })*/
            .state('order by id', {
                url: '/orders/:orderId',
                templateUrl: 'public/orders/views/view2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            }).state('edit order', {
                url: '/orders/user/:orderId',
                templateUrl: 'public/orders/views/edit_order.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            }).state('admin create order', {
                url: '/orders/user/create/:orderId',
                templateUrl: 'public/orders/views/new_order.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
    }
]);