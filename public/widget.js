

angular.module('ngAppOrder', ['ngResource', 'vr.directives.slider', 'ui.select'])
.config(['$sceProvider', function($sceProvider){
  $sceProvider.enabled(false);
}]).directive('requireMultiple', function ($rootScope) {
    return {
        require: 'ngModel',
        link: function postLink(scope, element, attrs, ngModel) {

            ngModel.$validators.required = function (value) {

                if(scope.value_of_me) {

                if(angular.isArray(value) && value.length > 0) $scope.form.$setValidity('required', true); return true;
            }else return true;
          }
        }
    };
})
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();

          if(text.length < 3){
            itemMatches = true;
            break;
          }

          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
})
.filter('numeral', function () {
    return function (value, format) {
        if (!_.isNumber(value)) value = 0;
        if (!format) format = '0,0';

        return numeral(value).format(format);
    };
})
.factory('Orders', ['$resource', function($resource) {
    return $resource('/orders/:orderId', {
        orderId: '@_id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
        	method: 'GET',
        	isArray: false
        },
        claim: {
            method: 'POST',
            url: '/orders/:orderId/claim'
        },
        cancel: {
            method: 'POST',
            url: '/orders/:orderId/cancel'
        },
        complete: {
            method: 'POST',
            url: '/orders/:orderId/complete'
        },
        approve: {
            method: 'POST',
            url: '/orders/:orderId/approve'
        },
        unapprove: {
            method: 'POST',
            url: '/orders/:orderId/unapprove'
        },
        fb_category: {
        	method: 'GET',
        	url: '/fb_category'
        },
        instagram: {
        	method: 'GET',
        	url: '/instagram-hashtag'
        },
        user_order: {
            method: 'POST',
            url: '/users/deactivated_order'
        },
        duplicates: {
        	method: "POST",
        	url: "/orders/inprogress"
        }
    });
}])
.factory('Services', ['$resource', function($resource) {
    return $resource('/services/:serviceId', {
        serviceId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false
      }
    });
}])
.factory('Settings', ['$resource', function($resource) {
    return $resource('/settings/:settingName', {
        settingName: '@name'
    }, {
        query: {
          method: 'GET',
        	isArray: false
        },
        save: {
          method: 'POST',
          url: '/settings/:settingName'
        }
    });
}])
.factory('Payments', ['$resource', function($resource) {
    return $resource('/payments/:orderId', {
        orderId: '@_id'
    }, {
        claim: {
            method: 'POST',
            url: '/payments/:orderId/approve'
        },
        purchase: {
        method: 'GET',
        url: '/purchase/:paymentId'
      }
    });
}])
.factory('Codes', ['$resource', function($resource) {
    return $resource('/discounts/evaluate/:discountName', {
        discountName: '@_id'
    }, {
      query: {
      	method: 'GET',
      	isArray: false
      }
    });
}]).factory('Speed',function() {
	return {
		display: function(service) {

			if(service.faster_speed) return true;
			else return false;

		}
	}
})
.factory('Discounts', ['$resource', function($resource) {
    return $resource('/discounts/:discountId', {
        discountId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false
      }
    });
}])
.controller('ngAppOrderController', ['$timeout','Settings','Speed','$location', '$scope', '$http', 'Orders', 'Payments', 'Services', 'Discounts', 'Codes',  function($timeout,Settings, Speed,$location, $scope, $http, Orders, Payments, Services, Discounts, Codes) {
	vex.defaultOptions.className = 'vex-theme-plain';

	$scope.url = '';
	$scope.disableButton = false;
	$scope.quantity = 1;
  $scope.paymentMethod = 'Paypal';

  $scope.comments = [{id: 'comment1'}];
  $scope.ig_mentions = [{id: 'ig1'}];
  $scope.fullDiscount = { haveDiscount: false };



  $scope.removeLine = function() {
  	var line = $scope.comments.length-1;
  	$scope.quantity = line;
  	if(line != 0)	$scope.comments.splice(line);
  }

  $scope.addLine = function() {
  	var line = $scope.comments.length+1;
  	$scope.quantity = line;
  	if(line < 2000) $scope.comments.push({id: 'comment'+line});
  }








	$scope.countries = [
	    {id: "AF", text: "Afghanistan"},
	    {id: "AX", text: "Aland Islands"},
	    {id: "AL", text: "Albania"},
	    {id: "DZ", text: "Algeria"},
	    {id: "AS", text: "American Samoa"},
	    {id: "AD", text: "Andorra"},
	    {id: "AO", text: "Angola"},
	    {id: "AI", text: "Anguilla"},
	    {id: "AQ", text: "Antarctica"},
	    {id: "AG", text: "Antigua and Barbuda"},
	    {id: "AR", text: "Argentina"},
	    {id: "AM", text: "Armenia"},
	    {id: "AW", text: "Aruba"},
	    {id: "AP", text: "Asia/Pacific Region"},
	    {id: "AU", text: "Australia"},
	    {id: "AT", text: "Austria"},
	    {id: "AZ", text: "Azerbaijan"},
	    {id: "BS", text: "Bahamas"},
	    {id: "BH", text: "Bahrain"},
	    {id: "BD", text: "Bangladesh"},
	    {id: "BB", text: "Barbados"},
	    {id: "BY", text: "Belarus"},
	    {id: "BE", text: "Belgium"},
	    {id: "BZ", text: "Belize"},
	    {id: "BJ", text: "Benin"},
	    {id: "BM", text: "Bermuda"},
	    {id: "BT", text: "Bhutan"},
	    {id: "BO", text: "Bolivia"},
	    {id: "BQ", text: "Bonaire, Saint Eustatius and Saba"},
	    {id: "BA", text: "Bosnia and Herzegovina"},
	    {id: "BW", text: "Botswana"},
	    {id: "BV", text: "Bouvet Island"},
	    {id: "BR", text: "Brazil"},
	    {id: "IO", text: "British Indian Ocean Territory"},
	    {id: "BN", text: "Brunei Darussalam"},
	    {id: "BG", text: "Bulgaria"},
	    {id: "BF", text: "Burkina Faso"},
	    {id: "BI", text: "Burundi"},
	    {id: "KH", text: "Cambodia"},
	    {id: "CM", text: "Cameroon"},
	    {id: "CA", text: "Canada"},
	    {id: "CV", text: "Cape Verde"},
	    {id: "KY", text: "Cayman Islands"},
	    {id: "CF", text: "Central African Republic"},
	    {id: "TD", text: "Chad"},
	    {id: "CL", text: "Chile"},
	    {id: "CN", text: "China"},
	    {id: "CX", text: "Christmas Island"},
	    {id: "CC", text: "Cocos (Keeling) Islands"},
	    {id: "CO", text: "Colombia"},
	    {id: "KM", text: "Comoros"},
	    {id: "CG", text: "Congo"},
	    {id: "CD", text: "Congo, The Democratic Republic of the"},
	    {id: "CK", text: "Cook Islands"},
	    {id: "CR", text: "Costa Rica"},
	    {id: "CI", text: "Cote d&#039;Ivoire"},
	    {id: "HR", text: "Croatia"},
	    {id: "CU", text: "Cuba"},
	    {id: "CW", text: "Curacao"},
	    {id: "CY", text: "Cyprus"},
	    {id: "CZ", text: "Czech Republic"},
	    {id: "DK", text: "Denmark"},
	    {id: "DJ", text: "Djibouti"},
	    {id: "DM", text: "Dominica"},
	    {id: "DO", text: "Dominican Republic"},
	    {id: "EC", text: "Ecuador"},
	    {id: "EG", text: "Egypt"},
	    {id: "SV", text: "El Salvador"},
	    {id: "GQ", text: "Equatorial Guinea"},
	    {id: "ER", text: "Eritrea"},
	    {id: "EE", text: "Estonia"},
	    {id: "ET", text: "Ethiopia"},
	    {id: "EU", text: "Europe"},
	    {id: "FK", text: "Falkland Islands (Malvinas)"},
	    {id: "FO", text: "Faroe Islands"},
	    {id: "FJ", text: "Fiji"},
	    {id: "FI", text: "Finland"},
	    {id: "FR", text: "France"},
	    {id: "GF", text: "French Guiana"},
	    {id: "PF", text: "French Polynesia"},
	    {id: "TF", text: "French Southern Territories"},
	    {id: "GA", text: "Gabon"},
	    {id: "GM", text: "Gambia"},
	    {id: "GE", text: "Georgia"},
	    {id: "DE", text: "Germany"},
	    {id: "GH", text: "Ghana"},
	    {id: "GI", text: "Gibraltar"},
	    {id: "GR", text: "Greece"},
	    {id: "GL", text: "Greenland"},
	    {id: "GD", text: "Grenada"},
	    {id: "GP", text: "Guadeloupe"},
	    {id: "GU", text: "Guam"},
	    {id: "GT", text: "Guatemala"},
	    {id: "GG", text: "Guernsey"},
	    {id: "GN", text: "Guinea"},
	    {id: "GW", text: "Guinea-Bissau"},
	    {id: "GY", text: "Guyana"},
	    {id: "HT", text: "Haiti"},
	    {id: "HM", text: "Heard Island and McDonald Islands"},
	    {id: "VA", text: "Holy See (Vatican City State)"},
	    {id: "HN", text: "Honduras"},
	    {id: "HK", text: "Hong Kong"},
	    {id: "HU", text: "Hungary"},
	    {id: "IS", text: "Iceland"},
	    {id: "IN", text: "India"},
	    {id: "ID", text: "Indonesia"},
	    {id: "IR", text: "Iran, Islamic Republic of"},
	    {id: "IQ", text: "Iraq"},
	    {id: "IE", text: "Ireland"},
	    {id: "IM", text: "Isle of Man"},
	    {id: "IL", text: "Israel"},
	    {id: "IT", text: "Italy"},
	    {id: "JM", text: "Jamaica"},
	    {id: "JP", text: "Japan"},
	    {id: "JE", text: "Jersey"},
	    {id: "JO", text: "Jordan"},
	    {id: "KZ", text: "Kazakhstan"},
	    {id: "KE", text: "Kenya"},
	    {id: "KI", text: "Kiribati"},
	    {id: "KW", text: "Kuwait"},
	    {id: "KG", text: "Kyrgyzstan"},
	    {id: "LA", text: "Lao People&#039;s Democratic Republic"},
	    {id: "LV", text: "Latvia"},
	    {id: "LB", text: "Lebanon"},
	    {id: "LS", text: "Lesotho"},
	    {id: "LR", text: "Liberia"},
	    {id: "LY", text: "Libyan Arab Jamahiriya"},
	    {id: "LI", text: "Liechtenstein"},
	    {id: "LT", text: "Lithuania"},
	    {id: "LU", text: "Luxembourg"},
	    {id: "MO", text: "Macao"},
	    {id: "MK", text: "Macedonia"},
	    {id: "MG", text: "Madagascar"},
	    {id: "MW", text: "Malawi"},
	    {id: "MY", text: "Malaysia"},
	    {id: "MV", text: "Maldives"},
	    {id: "ML", text: "Mali"},
	    {id: "MT", text: "Malta"},
	    {id: "MH", text: "Marshall Islands"},
	    {id: "MQ", text: "Martinique"},
	    {id: "MR", text: "Mauritania"},
	    {id: "MU", text: "Mauritius"},
	    {id: "YT", text: "Mayotte"},
	    {id: "MX", text: "Mexico"},
	    {id: "FM", text: "Micronesia, Federated States of"},
	    {id: "MD", text: "Moldova, Republic of"},
	    {id: "MC", text: "Monaco"},
	    {id: "MN", text: "Mongolia"},
	    {id: "ME", text: "Montenegro"},
	    {id: "MS", text: "Montserrat"},
	    {id: "MA", text: "Morocco"},
	    {id: "MZ", text: "Mozambique"},
	    {id: "MM", text: "Myanmar"},
	    {id: "NA", text: "Namibia"},
	    {id: "NR", text: "Nauru"},
	    {id: "NP", text: "Nepal"},
	    {id: "NL", text: "Netherlands"},
	    {id: "NC", text: "New Caledonia"},
	    {id: "NZ", text: "New Zealand"},
	    {id: "NI", text: "Nicaragua"},
	    {id: "NE", text: "Niger"},
	    {id: "NG", text: "Nigeria"},
	    {id: "NU", text: "Niue"},
	    {id: "NF", text: "Norfolk Island"},
	    {id: "KP", text: "North Korea"},
	    {id: "MP", text: "Northern Mariana Islands"},
	    {id: "NO", text: "Norway"},
	    {id: "OM", text: "Oman"},
	    {id: "PK", text: "Pakistan"},
	    {id: "PW", text: "Palau"},
	    {id: "PS", text: "Palestinian Territory"},
	    {id: "PA", text: "Panama"},
	    {id: "PG", text: "Papua New Guinea"},
	    {id: "PY", text: "Paraguay"},
	    {id: "PE", text: "Peru"},
	    {id: "PH", text: "Philippines"},
	    {id: "PN", text: "Pitcairn"},
	    {id: "PL", text: "Poland"},
	    {id: "PT", text: "Portugal"},
	    {id: "PR", text: "Puerto Rico"},
	    {id: "QA", text: "Qatar"},
	    {id: "RE", text: "Reunion"},
	    {id: "RO", text: "Romania"},
	    {id: "RU", text: "Russian Federation"},
	    {id: "RW", text: "Rwanda"},
	    {id: "BL", text: "Saint Bartelemey"},
	    {id: "SH", text: "Saint Helena"},
	    {id: "KN", text: "Saint Kitts and Nevis"},
	    {id: "LC", text: "Saint Lucia"},
	    {id: "MF", text: "Saint Martin"},
	    {id: "PM", text: "Saint Pierre and Miquelon"},
	    {id: "VC", text: "Saint Vincent and the Grenadines"},
	    {id: "WS", text: "Samoa"},
	    {id: "SM", text: "San Marino"},
	    {id: "ST", text: "Sao Tome and Principe"},
	    {id: "SA", text: "Saudi Arabia"},
	    {id: "SN", text: "Senegal"},
	    {id: "RS", text: "Serbia"},
	    {id: "SC", text: "Seychelles"},
	    {id: "SL", text: "Sierra Leone"},
	    {id: "SG", text: "Singapore"},
	    {id: "SX", text: "Sint Maarten"},
	    {id: "SK", text: "Slovakia"},
	    {id: "SI", text: "Slovenia"},
	    {id: "SB", text: "Solomon Islands"},
	    {id: "SO", text: "Somalia"},
	    {id: "ZA", text: "South Africa"},
	    {id: "GS", text: "South Georgia and the South Sandwich Islands"},
	    {id: "KR", text: "South Korea"},
	    {id: "ES", text: "Spain"},
	    {id: "LK", text: "Sri Lanka"},
	    {id: "SD", text: "Sudan"},
	    {id: "SR", text: "Suriname"},
	    {id: "SJ", text: "Svalbard and Jan Mayen"},
	    {id: "SZ", text: "Swaziland"},
	    {id: "SE", text: "Sweden"},
	    {id: "CH", text: "Switzerland"},
	    {id: "SY", text: "Syrian Arab Republic"},
	    {id: "TW", text: "Taiwan"},
	    {id: "TJ", text: "Tajikistan"},
	    {id: "TZ", text: "Tanzania, United Republic of"},
	    {id: "TH", text: "Thailand"},
	    {id: "TL", text: "Timor-Leste"},
	    {id: "TG", text: "Togo"},
	    {id: "TK", text: "Tokelau"},
	    {id: "TO", text: "Tonga"},
	    {id: "TT", text: "Trinidad and Tobago"},
	    {id: "TN", text: "Tunisia"},
	    {id: "TR", text: "Turkey"},
	    {id: "TM", text: "Turkmenistan"},
	    {id: "TC", text: "Turks and Caicos Islands"},
	    {id: "TV", text: "Tuvalu"},
	    {id: "UG", text: "Uganda"},
	    {id: "UA", text: "Ukraine"},
	    {id: "AE", text: "United Arab Emirates"},
	    {id: "GB", text: "United Kingdom"},
	    {id: "US", text: "United States"},
	    {id: "UM", text: "United States Minor Outlying Islands"},
	    {id: "UY", text: "Uruguay"},
	    {id: "UZ", text: "Uzbekistan"},
	    {id: "VU", text: "Vanuatu"},
	    {id: "VE", text: "Venezuela"},
	    {id: "VN", text: "Vietnam"},
	    {id: "VG", text: "Virgin Islands, British"},
	    {id: "VI", text: "Virgin Islands, U.S."},
	    {id: "WF", text: "Wallis and Futuna"},
	    {id: "EH", text: "Western Sahara"},
	    {id: "YE", text: "Yemen"},
	    {id: "ZM", text: "Zambia"},
	    {id: "ZW", text: "Zimbabwe"}
	];
	$scope.indexer = 0;
	$scope.updateQuantity = function() {

				$scope.quantity = $scope.serviceObject.dropdown[$scope.indexer].min_quantity;
				$scope.min_quantity = $scope.serviceObject.dropdown[$scope.indexer].min_quantity;
				$scope.min_price = $scope.serviceObject.dropdown[$scope.indexer].fixed_price;
				$scope.twTargeted = $scope.serviceObject.dropdown[$scope.indexer].api_code;
  	}

	$scope.disableCart = function(action) {
		// $scope.cart_window(true);
		if(!action) vex.dialog.alert("Please wait!");


	}

	$scope.activate = function(hi) {


		if(hi == 'wt_iframe_targeted') $scope.form.$setValidity('required', false); $scope.value_of_me = true;

	}
		$scope.checkCart = function(launch_cartView) {

		if(localStorage.getItem('data')) {
			$scope.cart_items = JSON.parse(localStorage.getItem('data'));
		  var arr = JSON.parse(localStorage.getItem('data'));

		  $scope.amount = 0;
		  for(var i = 0; i < JSON.parse(localStorage.getItem('data')).length; i++) {


		  		$scope.amount += parseFloat(arr[i].price);

		   }

		  $scope.cart_total = JSON.parse(localStorage.getItem('data')).length;
		  if(JSON.parse(localStorage.getItem('data')).length) $scope.is_cart = true;

		}  if(launch_cartView) setTimeout(function() { $scope.ViewCart(); }, 1000);
	}

	$scope.ViewCart = function() {
		fbq('track', 'InitiateCheckout');

		$scope.cart_window = function(close) {
			if(close)  vex.close($vexContent);
			else {
			var $vexContent;
			$vexContent = 	vex.open({
			content: $('#cart_template').clone(true),
			input: $scope.amount,
			callback: function(data) {
				vex.close();

			}
		})
		}
	}
		$scope.cart_window();
		if(window.innerWidth <= 500) {
			$("html, body").animate({ scrollTop: 0 }, "slow");
		}




	}

	$scope.cartRemove = function(name) {
			_.remove($scope.cart_items,  {
				id: name


			});
			// if there are no items in cart
			if($scope.cart_items.length == 0) $('.view-cart-info').css('display','none');


			localStorage.setItem('data', JSON.stringify($scope.cart_items));
			$scope.checkCart(); // refresh cart data

	}
	$scope.addCartPayment = function(method) {

		var cart = JSON.parse(localStorage.getItem('data'));

		    $scope.add_cart = {};
	    $scope.add_cart.price = $scope.amount;
	    $scope.add_cart.quantity = $scope.cart_total;
	    $scope.add_cart.orderCount = 1;
	    $scope.add_cart.service = 'add_cart';
	    $scope.add_cart.url = 'add_cart';
	    $scope.add_cart.email = cart[0].email;
	    $scope.add_cart.discount = false;
	    $scope.add_cart.cart = cart;

	    vex.close($scope.cart_window);

		  $scope.create(method);


	}

	  $scope.totalComments = function() {

  	if($scope.serviceObject) {
  	if($scope.serviceObject.value == "fb_comments_customelite" && $scope.comments.length < 5) return false;
    if($scope.serviceObject.value == "yt_custom_elite" && $scope.comments.length < 5) return false;
  	if($scope.serviceObject.value == "ig_custom" && $scope.comments && $scope.comments.length < 5) return false;
  	if($scope.serviceObject.value == 'ig_mentions_hashtag' &&  !$scope.hashtag.length) return false;
  	if($scope.serviceObject.value == 'ig_mentions_followers' && !$scope.followers.length) return false;
  	if($scope.serviceObject.value == 'ig_mentions_likers' && !$scope.ig_media.length) return false;
  	else return true;
  }
  }

  	$scope.checkComments = function() {
  		var list = ["fb_comments_customelite","yt_custom_elite","ig_custom"];
  		if($scope.serviceObject) {
  			if(list.indexOf($scope.serviceObject.value) != -1) {
  				var tmp = $scope.comment.split("\n");
  				if(tmp.indexOf("") !== -1) {
            		vex.dialog.alert("No empty lines.");
            		return false;
          		}

  				if($scope.serviceObject.value != "ig_mentions") $scope.quantity = tmp.length;

  			}

  		}
  	}
  	$scope.actionInProgress = false;
	$scope.verify_fb = function() {
		var errorCheck = false;
    	$timeout(function() {
    		$scope.actionInProgress = false;
    	}, 1500);
    if($scope.cart_items) {

      _.forEach($scope.cart_items, function(i) {
        if(i.url == $scope.url && i.service.value == $scope.serviceObject.value) {

          errorCheck = true;
        }
      });




    }
    if(errorCheck) {
    	 vex.dialog.alert("Sorry, duplicated orders are not allowed!");
    	return false;
    }
		Orders.duplicates({url: $scope.url, service: $scope.serviceObject.value}, function(data) {

				if($scope.serviceObject.value == "unlimited_fb" || $scope.serviceObject.value == 'musically_followers' || $scope.serviceObject.value == "musically_likes") {
				for(var i in $scope.serviceObject.dropdown) {
					if($scope.serviceObject.dropdown[i].api_code == $scope.twTargeted) {
						var price = $scope.serviceObject.dropdown[i].fixed_price;
						$scope.price = price;
						$scope.quantity = 1;


					}
				}
			} else {
				var price = ($scope.discount) ? $scope.getServicePriceWithDiscount() : $scope.getServicePrice();
				price = price.split('$')[1];
			}
			if(price.match(',')) price = parseFloat(price.replace(',','')).toFixed(2);
			fbq('track', 'AddToCart', {
				value: price,
				currency: 'USD'
			});

		 var list = ["fb_comments_customelite","yt_custom_elite","ig_custom"];

    if(list.indexOf($scope.serviceObject.value) != -1) {
		$scope.comments = [];
		var tmp = $scope.comment.split("\n");
		for(var i in tmp) {
			$scope.comments.push({id: i, name: tmp[i]});
		}
		var process = $scope.totalComments();
		$scope.quantity = $scope.comments.length;
		if(!process) {
			vex.dialog.alert("Minimum comments amount is 5");
			return false;
		}

	} else $scope.comments = [];


			var instagram = [
			   'ig_mentions_hashtag',

			];

			var category_list = [
			  'fb_eu',
              'fb_worldwide',
              'fb_usa',
              'fb_italian',
              'fb_arabic_likes',
              'fb_vip',
              'fb_arabic_likes',
	     'Fb_targeted_likes_any'

			];
  	// get fb page categories
  	if(instagram.indexOf($scope.serviceObject.value) != -1) {
  		Orders.instagram({hashtag: $scope.hashtag}, function(response) {
  			if(response.data) {
  				vex.dialog.alert(response.data);
  			} else $scope.addToCart($scope.cart_add);
  		}, function(err) {
  			vex.dialog.alert("There is no such hashtag or it's banned by instagram, please choose something else.");
  		});

  	}
  	if(category_list.indexOf($scope.serviceObject.value) != -1) {

  		var send_request = Orders.fb_category({link: $scope.url},function(response) {
  			if(response.data) {
  				vex.dialog.alert('Please change the category of your page to anything but  Music band / Song / Album / Record Label, Once promotion is done you can change the category back.');


  			} else {
  			var test = category_list.indexOf($scope.serviceObject.value);
  				$scope.addToCart($scope.cart_add);

  			}

  		})



  	} else $scope.addToCart($scope.cart_add);

		}, function(err) {
			vex.dialog.alert(err.data);
			return false;
		});



	}
	$scope.addToCart = function(launch_cartView) {









		var one_th = ['yt_monetized','yt_views_retention','dm_views','vm_views', 'fb_views_auto','yt_views_smlite_packages'];
		var fixed_quantity = [
			'pt_likes','pt_repins','pt_followers','vk_likes','vk_shares','vk_subscribers','vn_revines','vn_followers','vn_likes','vn_loops',
			'yt_dislikes','yt_usa','sc_downloads','sc_plays','dm_views','g_circles','g_ones','g_shares'
		]
		console.log($scope.serviceObject.value, ' huhh');
			if(one_th.indexOf($scope.serviceObject.value) != -1) {

				if(!Number.isInteger($scope.quantity/1000)) {
					vex.dialog.alert('Quantity must be in thousands, ex 1000, not 1200!');
					return false;
				}
			}
			if(fixed_quantity.indexOf($scope.serviceObject.value) != -1) {

				var round_qty = $scope.quantity/100;
				if(round_qty != Math.floor(($scope.quantity/100))) {
					vex.dialog.alert('Quantity must be in hundreds, ex 1200, not 1230!');
					return false;
				}


			}

			if(!$scope.url) { vex.dialog.alert("URL is required."); return false; }
			if(!$scope.email) { vex.dialog.alert("Email is required."); return false; }
      if($scope.serviceObject.maximum && $scope.quantity > $scope.serviceObject.maximum) {
        vex.dialog.alert("Maximum quantity is " + $scope.serviceObject.maximum);
         return false;
      }
			 if($scope.serviceObject.value == "yt_views_smlite_packages") {
			 	if(parseInt($scope.quantity) < parseInt($scope.min_quantity)) {
			 		vex.dialog.alert("Minimum quantity is " + $scope.min_quantity);
			 		return false;
			 	}

        	}

        	if($scope.serviceObject.dropdown) {
        		for(var i in $scope.serviceObject.dropdown) {
        			if($scope.serviceObject.dropdown[i].api_code == $scope.twTargeted && $scope.serviceObject.value !== "yt_views_smlite_packages") {
        				if(parseInt($scope.quantity) < parseInt($scope.serviceObject.dropdown[i].min_quantity)) {
        					vex.dialog.alert('Minimum quantity is '+$scope.serviceObject.dropdown[i].min_quantity); return false;
        				}
        			}
        		}
        	}
        	if($scope.serviceObject.value == "ig_likes" || $scope.serviceObject.value == "ig_usalikes") {
      if($scope.quantity < $scope.serviceObject.minimum) {
         vex.dialog.alert("Minimum quantity is " + $scope.serviceObject.minimum);
          return false;
      }
    }
      if($scope.serviceObject.minimum && $scope.quantity < $scope.serviceObject.minimum) {
        vex.dialog.alert('Minimum quantity is ' + $scope.serviceObject.minimum); return false;
      }
			if(!$scope.serviceObject.minimum && $scope.quantity < $scope.serviceObject.multiplier && $scope.serviceObject.value != "ig_likes" && $scope.serviceObject.value != "ig_usalikes") { vex.dialog.alert('Minimum quantity is '+$scope.serviceObject.multiplier); return false; }
	        if($scope.serviceObject.value == "ig_likes" && $scope.quantity < $scope.min_quantity || $scope.serviceObject.value == "ig_usalikes" && $scope.quantity < $scope.min_quantity) { vex.dialog.alert('Minimum quantity is '+$scope.serviceObject.min_quantity); return false; }
			if($scope.serviceObject.value == "yt_trending_views" && !$scope.twTargeted || $scope.serviceObject.value == "yt_sm_target" && !$scope.twTargeted || $scope.serviceObject.value == "tw_targeted" && !$scope.twTargeted) { vex.dialog.alert('Please select country.'); return false; }
			if(!parseInt($scope.quantity)) { vex.dialog.alert('Quantity in numbers please.'); return false; }
			if($scope.serviceObject.targeted && !$scope.selectedCountries.countries || $scope.serviceObject.targeted && $scope.selectedCountries.countries.length === 0) { vex.dialog.alert("Please select country.");return false; }

			if(localStorage.getItem('data') && localStorage.getItem('data').length > 2) {


			 var arr = JSON.parse(localStorage.getItem('data'));

			 if(arr.map(function(e) { return e.service.recurring; }).indexOf(true) != -1 && !$scope.serviceObject.recurring) { $('#quickOrderForm').trigger('reset'); vex.dialog.alert('Recurring or regular orders only!'); return false; }
			 if(arr.map(function(e) { return e.service.recurring; }).indexOf(true) == -1 && $scope.serviceObject.recurring) { $('#quickOrderForm').trigger('reset'); vex.dialog.alert('Recurring or regular orders only!'); return false; }
			 else if(arr.map(function(e) { return e.service.recurring;}).indexOf(true) != -1 && $scope.serviceObject.recurring) { $('#quickOrderForm').trigger('reset'); vex.dialog.alert('Recurring orders can be ordered one at the time!'); return false; }

			}

			$scope.placeOrder=true;

			vex.close($scope.cart_window);



			$('.view-cart-info').css('display','initial'); // reset cart info to default
			if(!localStorage.getItem('data')) var arr = [];
			else var arr = JSON.parse(localStorage.getItem('data'));
			if($scope.serviceObject.value != 'unlimited_fb' && $scope.serviceObject.value != 'musically_likes' && $scope.serviceObject.value != 'musically_followers') {
				var price = ($scope.discount) ? $scope.getServicePriceWithDiscount() : $scope.getServicePrice();
				price = price.split('$')[1];
			}
			else var price = $scope.price;

      $scope.dropdown_index;

			if(price.match(',')) price = parseFloat(price.replace(',','')).toFixed(2);
			var emoticon = $scope.emoticon;
				emoticon = ($scope.serviceObject.value == "yt_views_smlite_packages") ? $scope.twTargeted : emoticon;
				emoticon = ($scope.serviceObject.value == "yt_sm_target") ? $scope.twTargeted_api : emoticon;
				emoticon = ($scope.serviceObject.value == "tw_targeted") ? $scope.twTargeted_api : emoticon;
        emoticon = ($scope.serviceObject.value == "yt_trending_views") ? $scope.twTargeted_api : emoticon;
				emoticon = ($scope.serviceObject.value == "unlimited_fb") ? $scope.twTargeted : emoticon;
				emoticon = ($scope.serviceObject.value == "musically_followers") ? $scope.twTargeted : emoticon;
				emoticon = ($scope.serviceObject.value == "musically_likes") ? $scope.twTargeted : emoticon;

        if($scope.serviceObject.value === "yt_views_smlite_packages") {
          var dropdown_index = false;
          for(var i in $scope.serviceObject.dropdown) {
            if($scope.serviceObject.dropdown[i].api_code === emoticon) {
              dropdown_index = i;
            }
          }
          if(dropdown_index) $scope.dropdown_index = dropdown_index;
        }

			arr.push({indexer: $scope.indexer, dropdown_index: $scope.dropdown_index ,emoticon: emoticon, eatSpeed: $scope.eatSpeed, ig_mentions: JSON.stringify($scope.comments), ig_media: $scope.ig_media, followers: $scope.followers, hashtag: $scope.hashtag, comments: JSON.stringify($scope.comments), countries: $scope.selectedCountries.countries ,speed_service: $scope.fasterSpeed ,recurring: $scope.serviceObject.recurring ,url: $scope.url, quantity: $scope.quantity,orderCount: $scope.getServiceCount(true),  discount: $scope.discountCode,service_label: $scope.serviceObject.label, service: $scope.serviceObject, email: $scope.email, name: $scope.url,real_price: price, price: price,  id: Math.floor(Math.random()*99999999)});
			localStorage.setItem('data', JSON.stringify(arr));

			$scope.comments = [{id: 'comment1'}];
			$scope.ig_mentions = [{id: 'ig1'}];
			$scope.checkCart(launch_cartView);



		if(!$scope.is_cart) $scope.is_cart = true;



	}





	$scope.selectedCountries = {
    countries: []
  };

  $scope.selectedService = {
    service: ''
  };

  $scope.$watch('selectedService', function(newValue){
    if(newValue){
      $scope.service = newValue.service;
    }
  }, true);

  function serviceUpdater(service) {
    if(service.value == "yt_views_smlite_packages" || service.value == "unlimited_fb" || service.value == "musically_followers" || service.value == 'musically_likes' || service.value == "tw_targeted") {

      $scope.twTargeted = (service.dropdown && service.dropdown[0]) ? service.dropdown[0].api_code : false;
      $scope.price = (service.dropdown && service.dropdown[0]) ? service.dropdown[0].fixed_price : false;
      $scope.min_price = $scope.price;
    } else if(service.value == "yt_trending_views" || service.value == "tw_targeted" || service.value == "yt_sm_target") {

      $scope.twTargeted_api = (service.dropdown && service.dropdown[service.dropdown.length-1]) ? service.dropdown[service.dropdown.length-1].api_code : false;
    }
          $scope.quantity = service.multiplier ? service.multiplier : 100;


}

  $scope.initCreate = function(){
    var rnd = Math.floor(Math.random() * 99999999999999) + 1
    var link = "/services?v=37536804797128"
    $http.get(link).then(function(result) {

      result.data = result.data.data;
      $scope.services = _.filter(result.data, function(service){
      	if(!service.recurring) service.label = service.label + " - $" + service.price + " Per " + service.multiplier + "!";

      	try {
                  service.discount_settings = (service.discount_settings && service.discount_settings.length) ? JSON.parse(service.discount_settings) : [];
                } catch(e) {
                  service.discount_settings = [];
                }
                try {
                  service.dropdown = JSON.parse(service.dropdown);
                  var tmp = []
                   for(var i in service.dropdown) {
                    if(!service.dropdown[i].api_hidden) tmp.push(service.dropdown[i]);
                  }
                  service.dropdown = tmp;
                  for(var i in service.dropdown) {
                    service.dropdown[i].index = i;
                  }
                } catch(e) {
                  service.dropdown = [{title: "", api_code: "", index: 0, fixed_price: false }];
                }
                var tmp = [];
                for(var i in service.discount_settings.price) {
                  tmp.push({price: service.discount_settings.price[i], percentage: service.discount_settings.percentage[i] });
                }
                service.discount_settings = tmp;
                service.selectedPackage = service.packages[0];


                serviceUpdater(service);


        return !service.disabled;
      });
      $scope.quantity = ($scope.serviceObject && $scope.serviceObject.multiplier) ? $scope.serviceObject.multiplier : 100;

    });
  };

  $scope.urlFitsPattern = function(){





    if(!$scope.url){
      return true;
    }

    if(!$scope.serviceObject){
      return false;
    }

    if((new XRegExp($scope.serviceObject.pattern, 'i')).test($scope.url)){

        return true;
    } else {
      $scope.error_message = $scope.serviceObject.error_message;
      return false;
    }
  };

	var setServiceObject = function(newValue, oldValue){
		_.forEach($scope.services, function(service) {
		    if(newValue === service.value){
		    	$scope.show_speed = Speed.display(service);
		        $scope.serviceObject = service;
		        serviceUpdater($scope.serviceObject);

		        if($scope.discount) $scope.getServicePriceWithDiscount();
		        else  $scope.getServicePrice();


		        return false;
		    }
		});
	};
  $scope.sortByURL = false;
  $scope.$watch('services', function( newValue, oldValue ) {



    if(newValue && newValue.length > 10){
      setTimeout(function(){
        $scope.$apply(function(){
          if($location.search().prioritize_group && $location.search().prioritize_group.length){

          	$scope.sortByURL = true;
            $scope.selectedService.service = _.chain($scope.services)
            .sortBy(function(service){
              if(service.category === $location.search().prioritize_group){
                return 0;
              } else {
                return 1;
              }
            })
            .first()
            .value()
            .value;
          } else {
            $scope.selectedService.service = $scope.services[0].value; //'fb_website';
          }
        });
      },0);
    }
  });

  $scope.getServiceCountries = function(){
    if($scope.serviceObject && $scope.serviceObject.countries && $scope.serviceObject.countries.length){
      return _.filter($scope.countries, function(country){
        return $scope.serviceObject.countries.indexOf(country.id) > -1;
      });
    } else {
      return $scope.countries;
    }
  };

	$scope.$watch('service', function( newValue, oldValue ) {



	    // Ignore initial setup.
	    if ( newValue === oldValue ) {
	        return;
	    }
	    $scope.selectedCountries = [];
	    setServiceObject(newValue, oldValue);
	});

	setServiceObject('wt_iframe', '');



	$scope.getServiceCount = function(pure) {
	    var count = 0;

	    if($scope.serviceObject && $scope.serviceObject.recurring){
        count = (typeof $scope.serviceObject.selectedPackage != 'undefined' ? $scope.serviceObject.selectedPackage.quantity : 0);
      } else if($scope.serviceObject) {
        count = (typeof $scope.quantity != 'undefined' ? $scope.quantity : 0);// * $scope.serviceObject.multiplier;
      }

	    if(pure){
	        return count;
	    } else {
	        return numeral(count).format('0,0');
	    }
	};

	$scope.getServicePrice = function(pure) {

	    var price = 0;
      if($scope.serviceObject && $scope.serviceObject.recurring){
        price = (typeof $scope.serviceObject.selectedPackage != 'undefined' ? $scope.serviceObject.selectedPackage.price : 0);
      } else if($scope.serviceObject) {
      	if($scope.serviceObject.value == "yt_views_smlite_packages") {

      		  $scope.serviceObject.price = $scope.min_price ? $scope.min_price : 0;

        }

        price = (typeof $scope.quantity != 'undefined' ? $scope.quantity/$scope.serviceObject.multiplier : 0) * $scope.serviceObject.price;
      	if($scope.serviceObject && $scope.serviceObject.value) {
          if($scope.serviceObject.value == "fb_postlikeslite") {
            if($scope.quantity >= $scope.serviceObject.minimum && $scope.quantity <= 100) price = 1.99;
          }
      		if($scope.serviceObject.value == "ig_likes" || $scope.serviceObject.value == "ig_usalikes") {

              if($scope.quantity >= 20 && $scope.quantity <= 100) price = 0.99;
            }
            if($scope.serviceObject.value == "Youtube_views_new" || $scope.serviceObject.value == "yt_usa_new") {

             // if($scope.eatSpeed == "NL") price = $scope.serviceObject.price * 0.25 + $scope.serviceObject.price;
              if($scope.eatSpeed == "FA") price = $scope.serviceObject.price * 0.50 + $scope.serviceObject.price;
              if($scope.eatSpeed == "SF") price = $scope.serviceObject.price * 0.75 + $scope.serviceObject.price;



            }
          }
      }

     if($scope.fasterSpeed) price = price+(price*0.5);
     if($scope.serviceObject && $scope.serviceObject.discount) {
          var range = false;
          $scope.serviceObject.discount_settings.sort(function(a,b) {
            return a.price - b.price;
          });
          for(var i in $scope.serviceObject.discount_settings) {
            if($scope.serviceObject.discount_settings[i].price) {
              if(price >= $scope.serviceObject.discount_settings[i].price) range = i
            }

          }
          if(range) {
              var tmp_qty = $scope.serviceObject.discount_settings[range].percentage/100;
              var qty = $scope.quantity*tmp_qty;
                //  qty = parseFloat(qty) + parseFloat($scope.quantity);
              $scope.totalQuantity = qty;
          } else $scope.totalQuantity = false;
     }

	    if(pure){
	        return price;
	    } else {
	        return numeral(price).format('$0,0.00');
	    }
	};

  $scope.groupSort = function(groups) {
    groups = groups.reverse();

    if($location.search().prioritize_group && $location.search().prioritize_group.length){
      groups = _.sortBy(groups, function(group) {
        if(group.name === $location.search().prioritize_group){
          return 0;
        } else {
          return 1;
        }
      });
    }
	for(var i in groups) {
	groups[i].totalPriority = 0;
		for(var j in groups[i].items) {
			if($location.search().prioritize_group && $location.search().prioritize_group.length &&  groups[i].items[j].category == $location.search().prioritize_group) {
				groups[i].totalPriority += 99999999;
			}
			else groups[i].totalPriority += parseInt(groups[i].items[j].priority);
		}
	}
	groups.sort(function(a,b) {
		return b.totalPriority - a.totalPriority;
	});
	if(!$location.search().prioritize_group) {

	  for(var i in groups) {
            groups[i].items.sort(function(a,b) {
              if(!a.admin_priority) a.admin_priority = 0;
              if(!b.admin_priority) b.admin_priority = 0;
              if(a.admin_priority == 0 && b.admin_priority == 0) return;


              if(a.admin_priority != 0 && b.admin_priority == 0) return -1;

              if(a.admin_priority == 0 && b.admin_priority != 0) return 1;

              else return a.admin_priority - b.admin_priority;

            })



          }


	return groups;
} else   return groups;
  };

  $scope.groupByCategory = function (item){
    return item.category || 'Other Services';
  };

  $scope.cancelDiscount = function(){
    $scope.discount = undefined;
    $scope.discountCode = '';
  };

  $scope.applyDiscount = function(){
    if(!$scope.discountCode){
      return;
    }

    Codes.get({
      discountName: $scope.discountCode
    }, function(discount) {
      if(discount && _.contains(discount.services, $scope.selectedService.service)){

      	if($scope.getServicePrice(true) < discount.min_price) {
      		$scope.discountCode = null;
      		vex.dialog.alert("This discount can only be applied if you purchase over $"+discount.min_price+" worth of value");
      		return false;
      	}


        $scope.discount = discount;
      } else {
        vex.dialog.alert('Service mismatch!');
      }
    });
  };

  $scope.getServicePriceWithDiscount = function(pure){
    var price = $scope.getServicePrice(true);

    if(!_.isEmpty($scope.discountCode) && $scope.discount && _.contains($scope.discount.services, $scope.selectedService.service)){
      price = price - parseFloat($scope.discount.price);
      price = price - (price * parseFloat($scope.discount.percent));
    }

    if(pure){
      return price;
    } else {
      return numeral(price).format('$0,0.00');
    }
  }

  function checkRecurring() {
  	 var data = JSON.parse(localStorage.getItem('data'));
  	 return data[0].recurring;
  }
	$scope.create = function(method) {
	    var self = this;
	    $scope.disableButton = true;
	    var no_error = false;
	     var $dialog = null

	    async.waterfall([
	    	function(callback){

		    	var payment = new Payments({
		    	    price: $scope.add_cart.price,
                    recurring: false,
		    	    user: (method) ? $scope.add_cart.email : self.email,
		    	    cart: (method) ? $scope.add_cart.cart : [],
		    	    url: (method) ? $scope.add_cart.url : self.url,

		    	});
		    	payment.user = ($scope.email) ? $scope.email : payment.user;

		    	for(var i in $scope.add_cart.cart) {
		    		$scope.add_cart.cart[i].email = payment.user;
		    	}
		    	payment.$save(
		    	    // success
		    	    function(payment) {
		    	        callback(null, payment);
		    	    },
		    	    // error
		    	    function(response){
		    	    	$scope.disableCart(true);
		    	        vex.dialog.alert(response.data);
		    	        callback(new Error('Error while initiating payment process'));
		    	    }
		    	);

		    }, function(payment, callback){

          if(self.url && !self.url.match(/^https?/)){
            self.url = 'http://' + self.url;
          }

          if(method) {
          		$scope.id_array = payment.id_array;
            	$scope.id_array = ($scope.id_array.length > 1) ? $scope.id_array.join(',') : $scope.id_array[0];
          		var id_array = payment.id_array;
          		payment = payment.payment;
          }

          		// initial add cart order
          		var facebook_tracking_price_copy = self.getServicePrice(true);
		    			var order = new Orders({
		    		    url: (method) ? $scope.add_cart.url : self.url,
		    	  	  	quantity: (method) ? $scope.add_cart.quantity : self.quantity,
		    	    	price: $scope.add_cart.price,
		    	    	orderCount: (method) ? $scope.add_cart.orderCount : self.getServiceCount(true),
		    	    	service: (method) ? $scope.add_cart.service : self.serviceObject,
		    	    	payment: payment._id,
		    	    	countries:  self.selectedCountries.countries,
            			discount: (method) ? $scope.add_cart.discount : $scope.discountCode,
              			recurring: checkRecurring(),


		    			});


		      	order.$save(function(response) {


	     					// loop through cart items
                (function loop(i) {

                  	if(i == $scope.add_cart.cart.length) return buyContinue();
                  		var order_cart = new Orders({
			         		url: $scope.add_cart.cart[i].url,
		  				  	quantity: $scope.add_cart.cart[i].quantity,
		    	    		price: $scope.add_cart.cart[i].price,
		    	    		orderCount: $scope.add_cart.cart[i].orderCount,
		    	    		service: $scope.add_cart.cart[i].service,
		    	    		payment: id_array[i],
		    	    		countries:  $scope.add_cart.cart[i].countries,
            			discount: $scope.add_cart.cart[i].discount || false,
              		recurring: false,
              		cart: false,
              		speed_service: $scope.add_cart.cart[i].speed_service,
              		ig_comments: $scope.add_cart.cart[i].comments,
              		hashtag: $scope.add_cart.cart[i].hashtag,
              		followers: $scope.add_cart.cart[i].followers,
              		ig_media: $scope.add_cart.cart[i].ig_media,
              		ig_mentions: $scope.add_cart.cart[i].ig_mentions,
              		eat_speed: $scope.add_cart.cart[i].eatSpeed,
              		emoticon: $scope.add_cart.cart[i].emoticon,
                  dropdown_index: $scope.add_cart.cart[i].dropdown_index ? $scope.add_cart.cart[i].dropdown_index : null,
			indexer: $scope.indexer
              	 		});

                  		order_cart.$save(function(response) {


                    	 return loop(i+1);

                  		}, function(err) {
                  			$scope.disableCart(true);
					$scope.inprocess = false;
					$scope.paymentAction = false;
					$scope.placeOrder = false;
                  			vex.dialog.alert(err.data);
                  			return false;


                  	});



              })(0);







		  //  } else buyContinue();


		function buyContinue() {

		    	        self.url = '';
		    	        self.quantity = 0;
		    	        self.fullMeta = null;
		    	        self.price = 0;
		    	        self.serviceObject = '';

		    	        $scope.disableButton = false;
		    	        var url = null;
		    	        if(method)	 {
		    	      		if(method == 'cc') $scope.paymentMethod = 'CreditCard';
		    	      		else $scope.paymentMethod = 'Paypal'

		    	      	}
		    	        if(method) url = window.location.origin + '/purchase/' + payment._id + '?origin=' + encodeURIComponent(window.location.origin) + '&paymentMethod=' + $scope.paymentMethod+'&add_cart=true&id_array='+id_array;
		    	        else url = window.location.origin + '/purchase/' + payment._id + '?origin=' + encodeURIComponent(window.location.origin) + '&paymentMethod=' + $scope.paymentMethod;

		    	      $scope.redirect_checkout = window.location.origin + '/return/' + payment._id.toString();
                  	  $scope.checkout_id = payment._id.toString();
		    	      if(payment.needs_activation) {
		    	      	var query = { url: url, user: payment.user};
		    	      	Orders.user_order({data: query}, function() {

		    	      		$scope.disableCart(true);
		    	      		vex.dialog.alert("Account details and password were sent to your email with a verification link, once you click on the link you will then be redirected to the payment page.");
		    	      		return;

		    	      	}, function(err) {
		    	      		$scope.disableCart(true);
		    	      		vex.dialog.alert(err.data);
		    	      	});

		    	      }
		    	      else {


                  	fbq('track', 'Purchase', {
								value: $scope.add_cart.price,
								currency: 'USD'
							});

                  	localStorage.setItem('data', []);

		    	    if(method) window.top.location.href = url;
		    	    else window.top.location.href = url;

                  		/*
							$success_vex = vex.dialog.alert({
		    	        	message: "Thank you for choosing Rantic! You will now be redirected to the payment processor to pay for your order.",
		    	        	callback: function(){
		    	        		localStorage.setItem('data', []);

		    	        		if(method) window.top.location.href = url;

		    	        		else window.top.location.href = url;

		    	        	}
		    	        });

							*/



		    	  	}
		    	    }

		    	    },
		    	    // error
		    	    function(response){

		    	        if(response.status === 403){
		    	            vex.dialog.buttons.YES.text = 'Yes please!';
		    	            vex.dialog.buttons.NO.text = 'No, thanks';

		    	            vex.dialog.alert('An error occured while processing your order');
		    	            $scope.disableButton = false;
		    	        } else {
		    	            vex.dialog.buttons.YES.text = 'OK';
		    	            vex.dialog.alert(response.data);
		    	            $scope.disableButton = false;
		    	        }
		    	    }
		    	);
		    }
	    ]);
	}


}]);
