angular.module('mean.orders').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/orders/views/create.html',
    "<section class='panel' data-ng-controller=\"OrdersController\" ng-init=\"initCreate()\">\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    Place an Order\n" +
    "  </header>\n" +
    "  <div class=\"panel-body\">\n" +
    "    <form class=\"form-horizontal\" name=\"form\" role=\"form\" data-ng-submit=\"create()\" novalidate>\n" +
    "      <div class='col-md-6'>\n" +
    "        <div class=\"form-group\">\n" +
    "          <label mean-token=\"'create-url'\" class=\"col-md-3 control-label\">URL</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"url\" id=\"url\" placeholder=\"Enter the URL here\" validate-url required>\n" +
    "            <span class=\"help-inline\" ng-show=\"form.$error.url || !urlFitsPattern()\">Looks like the URL is invalid or is not supported by the selected service.</span>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "          <label mean-token=\"'create-service'\" class=\"col-md-3 control-label\">Service</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <!-- <select name=\"service\" id=\"service\" data-ng-model=\"service\" class=\"form-control\" required>\n" +
    "              <option data-ng-repeat=\"service in services\" value=\"{{service.value}}\">{{service.label}}</option>\n" +
    "            </select> -->\n" +
    "            <ui-select ng-model=\"selectedService.service\" theme=\"select2\" style=\"width: 100%;\">\n" +
    "              <ui-select-match placeholder=\"Select a service...\">{{$select.selected.label}}</ui-select-match>\n" +
    "              <ui-select-choices group-by=\"groupByCategory\" group-filter=\"groupSort\" repeat=\"service.value as service in services | propsFilter: {label: $select.search}\">\n" +
    "                <div ng-bind-html=\"service.label | highlight: $select.search\"></div>\n" +
    "              </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\" ng-hide=\"!url || !serviceObject.targeted\">\n" +
    "          <label mean-token=\"'create-countries'\" class=\"col-md-3 control-label\">Countries</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <ui-select multiple ng-model=\"selectedCountries.countries\" theme=\"select2\" style=\"width: 100%;\">\n" +
    "              <ui-select-match placeholder=\"Select country...\">{{$item.text}}</ui-select-match>\n" +
    "              <ui-select-choices repeat=\"country.id as country in getServiceCountries() | propsFilter: {id: $select.search, text: $select.search}\">\n" +
    "                <div ng-bind-html=\"country.text | highlight: $select.search\"></div>\n" +
    "              </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\" ng-show=\"!serviceObject.recurring\">\n" +
    "          <label mean-token=\"'create-quantity'\" class=\"col-md-3 control-label\">How Many?</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <!-- <input type=\"text\" class=\"form-control\" data-ng-model=\"quantity\" id=\"quantity\" placeholder=\"\" required> -->\n" +
    "            <slider ng-disabled=\"!service\" ng-model=\"quantity\" type=\"text\" id=\"quantity\" floor=\"0\" ceiling=\"1000\" translate-fn=\"translateFn\"></slider>\n" +
    "            <div ng-hide=\"!service\" style=\"font-weight: bold;\">{{quantity * serviceObject.multiplier}} {{serviceObject.label}}</div>\n" +
    "            <span ng-hide=\"!service\" class=\"help-inline\">Note: You can only order this service in multiples of {{serviceObject.multiplier}}</span>\n" +
    "          </div>\n" +
    "          <!--<div class=\"col-md-7\">\n" +
    "            <div class='price-multiplier' ng-if=\"service\"><span>x</span>&nbsp;{{getServiceMultiplier()}}&nbsp;=&nbsp;{{getServiceCount()}} Total</div>\n" +
    "          </div>-->\n" +
    "        </div>\n" +
    "        <div class=\"form-group\" ng-show=\"serviceObject.recurring\">\n" +
    "          <label mean-token=\"'create-quantity'\" class=\"col-md-3 control-label\">How Many?</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <ui-select ng-model=\"serviceObject.selectedPackage\" theme=\"select2\" style=\"width: 100%;\">\n" +
    "              <ui-select-match placeholder=\"Select a package...\">{{$select.selected.label}}</ui-select-match>\n" +
    "              <ui-select-choices group-by=\"groupByCategory\" repeat=\"package in serviceObject.packages | propsFilter: {label: $select.search}\">\n" +
    "                <div ng-bind-html=\"package.label | highlight: $select.search\"></div>\n" +
    "              </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\" ng-hide=\"!url\">\n" +
    "          <label mean-token=\"'create-price'\" class=\"col-md-3 control-label\">Price</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <div class='price-field'>{{getServicePriceWithDiscount()}}</div>\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" ng-model=\"haveDiscount\" id=\"haveDiscount\" placeholder=\"\">I have a discount code</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\" ng-hide=\"url\">\n" +
    "          <label mean-token=\"'create-price'\" class=\"col-md-3 control-label\">Price</label>\n" +
    "          <div class=\"col-md-9\">\n" +
    "            <div class='price-field'>Enter URL above to get a price</div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\" ng-hide=\"!haveDiscount\">\n" +
    "          <label mean-token=\"'create-price'\" class=\"col-md-3 control-label\">Discount</label>\n" +
    "          <span ng-show=\"!discount.code\">\n" +
    "            <div class=\"col-md-5\">\n" +
    "              <input type=\"text\" class=\"form-control\" ng-model=\"discountCode\" id=\"discountCode\" placeholder=\"Enter your discount code here\">\n" +
    "            </div>\n" +
    "            <div class=\"col-md-4\">\n" +
    "              <a class='btn btn-default btn-block applyDiscountButton' ng-click=\"applyDiscount()\" style=\"line-height: 29px;\">Apply Discount</a>\n" +
    "            </div>\n" +
    "          </span>\n" +
    "          <span ng-show=\"discount.code\">\n" +
    "            <div class=\"col-md-5\" style=\"line-height: 42px;\">\n" +
    "              <div ng-if=\"discount.price\">{{discount.price | numeral:'$0,0.00'}} off</div>\n" +
    "              <div ng-if=\"discount.percent\">{{discount.percent  | numeral:'%0,0.00'}} off</div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-4\" ng-if=\"discount.code\">\n" +
    "              <a class='btn btn-default btn-block applyDiscountButton' ng-click=\"cancelDiscount()\" style=\"line-height: 29px;\">Cancel Discount</a>\n" +
    "            </div>\n" +
    "          </span>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class='col-md-6 link-info'>\n" +
    "        <span ng-hide=\"!url\">\n" +
    "          <h4>Link Information</h4>\n" +
    "          <div class='empty-meta' ng-show=\"noMeta()\">\n" +
    "            Sorry, but I can't pull any meta information for this link. Please make sure the link URL is typed correctly.\n" +
    "          </div>\n" +
    "          <div class=\"meta-scroll-container\">\n" +
    "            <div class=\"form-group\" ng-hide=\"noMeta()\" ng-repeat=\"(parameter, value) in meta\">\n" +
    "              <label class=\"col-md-3 control-label\">{{parameter}}</label>\n" +
    "              <div class=\"col-md-9 meta-value\">\n" +
    "                {{value}}\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </span>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <div class=\"col-md-offset-10 col-md-2\">\n" +
    "          <button type=\"submit\" class=\"btn btn-info col-md-12\" ng-disabled=\"disableButton\">Place Order</button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/orders/views/list.html',
    "<section data-ng-controller=\"OrdersController\" data-ng-init=\"find()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Orders\n" +
    "\t    <button class='btn btn-action pull-right' ng-show=\"global.hasRole('provider') || global.hasRole('admin')\" data-ng-click=\"bulkCopy()\">Bulk Copy</button>\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"ng-table-responsive table table-striped table-advance table-hover orders\">\n" +
    "\t\t    <tr data-ng-repeat=\"order in $data\">\n" +
    "\t\t    \t<td data-title=\"'Created'\" sortable=\"'created'\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{order.created | fromNow}}</a></td>\n" +
    "\t\t        <td data-title=\"'URL'\" sortable=\"'url'\" filter=\"{ 'url': 'text' }\">\n" +
    "\t\t\t\t\t\t\t<i class='fa fa-envelope' style=\"color: #aad; margin-right: 0.25em;\" ng-if=\"hasUnread(order)\"></i>\n" +
    "\t\t\t\t\t\t\t<a data-ng-href=\"#!/orders/{{order._id}}\">{{order.url | cut:true:55:' ...'}}</a>\n" +
    "\t\t\t\t\t\t</td>\n" +
    "\t\t        <td data-title=\"'Service'\" sortable=\"'service.label'\" filter=\"{ 'service': 'select' }\" filter-data=\"servicesFilter($column)\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{order.service.label | cut:true:35:' ...'}}</a></td>\n" +
    "\t\t        <td data-title=\"'Quantity'\" sortable=\"'orderCount'\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{order.orderCount | numeral}}</a>\n" +
    "\t\t        <td data-title=\"'Price'\" sortable=\"'price'\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{getOrderPrice() | numeral:'$0,0.00'}}</a></td>\n" +
    "\t\t\t\t\t\t<td data-title=\"'Email'\" sortable=\"'service.user.username'\" filter=\"{ 'username': 'text' }\">{{order.user.username | cut:true:35:' ...'}}</td>\n" +
    "\t\t        <td data-title=\"'Status'\" sortable=\"'status'\"><a data-ng-href=\"#!/orders/{{order._id}}\">\n" +
    "\t\t        \t<span ng-if=\"!order.paid && !order.checkout\" class='order-status-cancelled'>\n" +
    "\t\t\t        \tAbandoned Checkout\n" +
    "\t\t\t        </span>\n" +
    "\t\t\t        <span ng-if=\"!order.paid && order.checkout\" class='order-status-verifying'>\n" +
    "\t\t\t        \tAuthorizing Payment\n" +
    "\t\t\t        </span>\n" +
    "\t\t\t        <span ng-if=\"order.paid && order.completed && !order.cancelled && !order.verified\" class='order-status-verifying'>\n" +
    "\t\t\t        \tVerifying\n" +
    "\t\t\t        </span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && order.completed && !order.cancelled && order.verified\" class='order-status-completed'>\n" +
    "\t\t        \t\tCompleted\n" +
    "\t\t        \t</span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && order.cancelled\" class='order-status-cancelled'>\n" +
    "\t\t        \t\tCancelled\n" +
    "\t\t        \t</span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && !order.completed && !order.cancelled && !order.provider\" class='order-status-progress'>\n" +
    "\t\t        \t\tIn Progress\n" +
    "\t\t        \t</span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && !order.completed && !order.cancelled && order.provider\" class='order-status-progress'>\n" +
    "\t\t        \t\tIn Progress\n" +
    "\t\t        \t</span>\n" +
    "\t\t        </a></td>\n" +
    "\t\t      </a>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "\n" +
    "    <h1 data-ng-hide=\"!orders || orders.length || !global.hasRole('customer')\" class='no-orders'>No orders yet.<span> <br> Why don't you <a href=\"/#!/orders/create\">Place an Order</a>?</span></h1>\n" +
    "</section>\n"
  );


  $templateCache.put('public/orders/views/view.html',
    "<section class='panel' data-ng-controller=\"OrdersController\" data-ng-init=\"findOne()\">\n" +
    "    <div class='loading-container' ng-show=\"!order\">\n" +
    "        Loading...\n" +
    "    </div>\n" +
    "    <div class='screen-container' ng-hide=\"!order\">\n" +
    "        <header class=\"panel-heading higher-panel-heading\">\n" +
    "            Order for <a href=\"http://refhide.com/?{{order.url | prependHttp}}\" target=\"_blank\">{{order.url | cut:true:55:' ...'}}</a>\n" +
    "            <span data-ng-show=\"global.hasRole('admin')\">\n" +
    "                <a class=\"btn\" data-ng-click=\"remove();\">\n" +
    "                    <i class=\"glyphicon glyphicon-trash\"></i>\n" +
    "                </a>\n" +
    "            </span>\n" +
    "            <div class=\"pull-right\">Total Price: {{getOrderPrice() | numeral:'$0,0.00'}}</div>\n" +
    "            <span class='pull-right order-status order-status-progress' ng-if=\"order.paid && !order.provider && !order.completed && !order.cancelled\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"Your order is queued to be processed by Rantic. Keep an eye on this page to receive the updated status for your order.Due to a large volume of orders received and processed by Rantic, orders may remain in queue for several hours. If you believe that your order is taking too long, please contact support.\">In Progress</span>\n" +
    "            <span class='pull-right order-status order-status-completed' ng-if=\"order.paid && order.completed && !order.cancelled && order.verified\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"This order has been delivered.\">Completed</span>\n" +
    "            <span class='pull-right order-status order-status-verifying' ng-if=\"order.paid && order.completed && !order.cancelled && !order.verified\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"This order has been marked as completed by the vendor but needs to be verified by our administrators.\">Verifying</span>\n" +
    "            <span class='pull-right order-status order-status-progress' ng-if=\"order.paid && order.provider && !order.completed && !order.cancelled\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"Your order is being processed by Rantic. We will notify you upon completion!\">In Progress</span>\n" +
    "            <span class='pull-right order-status order-status-cancelled' ng-if=\"order.paid && order.cancelled\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"Your order has been cancelled. Stated reason: {{order.reason}}\">Cancelled</span>\n" +
    "            <span class='pull-right order-status order-status-verifying' ng-if=\"!order.paid && order.checkout\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"A payment process was initiated for this order that is awaiting authorization\">Authorizing Payment</span>\n" +
    "            <span class='pull-right order-status order-status-cancelled' ng-if=\"!order.paid && !order.checkout\" popover-trigger=\"mouseenter\" popover-placement=\"bottom\" popover=\"The customer has abandoned the checkout process\">Abandoned Checkout</span>\n" +
    "        </header>\n" +
    "        <div class=\"panel-body order-details-panel\">\n" +
    "            <h3 class='order-summary'><strong>{{order.orderCount | numeral}}</strong> units of the '<strong>{{order.service.label}}</strong>' service at a price of <strong>{{getOrderServicePrice() | numeral:'$0,0.00'}}</strong> per <strong>{{order.service.multiplier | numeral}}</strong> units</h3>\n" +
    "            <h3 class='order-summary' ng-show=\"order.discount.percent\">{{order.discount.percent  | numeral:'%0,0.00'}} off</h3>\n" +
    "            <h3 class='order-summary' ng-show=\"order.discount.price\">{{order.discount.price  | numeral:'$0,0.00'}} off</h3>\n" +
    "            <h3 class='order-summary' ng-show=\"global.hasRole('admin')\">Email: {{order.user.email}}</h3>\n" +
    "            <h3 class='order-summary' ng-show=\"countriesToString()\">Countries: {{countriesToString()}}</h3>\n" +
    "            <h3 class='order-summary order-status-cancelled' ng-show=\"order.cancelled\">This order has been cancelled. Stated reason: {{order.reason}}</h3>\n" +
    "            <div class='place-cancelled-order' ng-show=\"order.cancelled && global.hasRole('customer')\">\n" +
    "                If the issue causing the cancellation has been resolved by you, or if you believe this was a mistake, <a ui-sref='create order'>click here to place a new order</a>.\n" +
    "            </div>\n" +
    "            <div class='counts-container' ng-hide=\"isTrafficService()\">\n" +
    "                <div class='col-md-6 count-column'>\n" +
    "                    <div class='count' ng-hide=\"!order.beforeCount\">{{order.beforeCount | numeral}}</div>\n" +
    "                    <div class='count no-count' ng-show=\"!order.beforeCount\" popover-trigger=\"mouseenter\" popover-placement=\"top\" popover=\"If the order was placed less than a few hours ago, the count might take a while to appear.\">Unavailable</div>\n" +
    "                    <span>Initial Count</span>\n" +
    "                </div>\n" +
    "                <div class='col-md-6 count-column'>\n" +
    "                    <div class='count' ng-hide=\"!order.afterCount\">{{order.afterCount | numeral}}</div>\n" +
    "                    <div class='count no-count' ng-show=\"!order.afterCount\" popover-trigger=\"mouseenter\" popover-placement=\"top\" popover=\"If the order was placed less than a few hours ago, the count might take a while to appear.\">Unavailable</div>\n" +
    "                    <span>Latest Count</span>\n" +
    "                </div>\n" +
    "                <br class='clear'>\n" +
    "            </div>\n" +
    "            <div class='counts-container' ng-show=\"isTrafficService()\">\n" +
    "                <center>Counts are not available for this service. To see the progress of the order, please use Google Analytics on your resource.</center>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='panel-body order-actions-panel'>\n" +
    "            <div class='order-actions-container-left pull-left' ng-show=\"global.hasRole('admin') || global.hasRole('provider')\">\n" +
    "                <button class='btn btn-danger btn-action-delete' ng-hide=\"!global.hasRole('admin')\" data-ng-click=\"remove();\">Delete Order</button>\n" +
    "                <button class='btn btn-warning btn-action-cancel' ng-hide=\"order.cancelled || order.completed\" data-ng-click=\"cancel();\">Cancel Order</button>\n" +
    "            </div>\n" +
    "            <div class='order-actions-container-right pull-right' ng-show=\"global.hasRole('admin') || global.hasRole('provider')\">\n" +
    "                <button class='btn btn-danger btn-action-cancel' ng-show=\"order.paid && !order.cancelled && order.completed && !order.verified && global.hasRole('admin')\" data-ng-click=\"unapprove();\">Unapprove Order</button>\n" +
    "                <button class='btn btn-success btn-action-complete' ng-show=\"order.paid && !order.cancelled && order.completed && !order.verified && global.hasRole('admin')\" data-ng-click=\"approve();\">Approve Order</button>\n" +
    "                <button class='btn btn-success btn-action-complete' ng-hide=\"!order.paid || order.cancelled || order.completed || (!order.provider && global.hasRole('provider'))\" data-ng-click=\"complete();\">Complete Order</button>\n" +
    "                <button class='btn btn-action btn-action-claim' ng-hide=\"!order.paid || order.provider || global.hasRole('admin')\" data-ng-click=\"claim();\">Claim Order</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\">\n" +
    "            <h2>Order Messages</h2>\n" +
    "            <p ng-if=\"!order.comments\">You can get in touch with Rantic staff by posting a message directly on this order. We will get back to you shortly.</p>\n" +
    "            <div class='comments'>\n" +
    "                <div class='single-comment' ng-repeat=\"comment in order.comments\">\n" +
    "                    <h4>{{comment.user.name}}<span>{{comment.date | date:'yyyy-MM-dd HH:mm'}}</span></h4>\n" +
    "                    <span ng-bind-html=\"getCommentHtml(comment.text)\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class='new-comment'>\n" +
    "                <form method=\"post\">\n" +
    "                    <textarea ng-model=\"newComment\" id=\"commentEditor\" redactor=\"{buttons: ['formatting', '|', 'bold', 'italic']}\" cols=\"30\" rows=\"10\" />\n" +
    "                    <div class='new-comment-buttons'>\n" +
    "                        <button class='btn btn-action pull-right' ng-click=\"sendMessage()\">Send Message</button>\n" +
    "                    </div>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n"
  );

}]);
