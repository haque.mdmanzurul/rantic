angular.module('mean.apis').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/api/views/api.html',
    "\n" +
    "<section data-ng-controller=\"ApiCtrl\"  class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Orders \n" +
    "\t \n" +
    "\t    <button class='btn btn-action pull-right' ng-show=\"global.hasRole('provider') || global.hasRole('admin')\" data-ng-click=\"bulkCopy()\">Bulk Copy</button>\n" +
    "\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"ng-table-responsive table table-striped table-advance table-hover orders\">\n" +
    "\t\t    <tr data-ng-repeat=\"order in $data\" ng-if=\"order.url != 'add_cart'\">\n" +
    "\t\t    \t<td data-title=\"'Copy'\" >\n" +
    "\n" +
    "\t\t    \t\t<label clipboard text='copyMe'>\n" +
    "\t\t    \t\t<button class='btn btn-action' type='button' ng-init='copyMe=order.url'>Copy</button></label>\n" +
    "\t\t    \t\t  \n" +
    "\t\t    \t</td>\n" +
    "\t\t    \t<td data-title=\"'API ID'\" sortable=\"'api_id'\" filter=\"{ 'api_id': 'text' }\"> {{order.api_id}}</td>\n" +
    "\t\t    \t<td data-title=\"'Created'\" sortable=\"'created'\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{order.created | fromNow}}</a></td>\n" +
    "\t\t        <td data-title=\"'URL'\" sortable=\"'url'\" filter=\"{ 'url': 'text' }\">\n" +
    "\t\t\t\t\t\t\t<i class='fa fa-envelope' style=\"color: #aad; margin-right: 0.15em;\" ng-if=\"hasUnread(order)\"></i>\n" +
    "\t\t\t\t\t\t\t<a class='copy-url' data-ng-href=\"#!/orders/{{order._id}}\">{{order.url | cut:true:40:' ...'}}</a>\n" +
    "\t\t\t\t\t\t</td>\n" +
    "\t\t        <td data-title=\"'Service'\" sortable=\"'service.label'\" filter=\"{ 'service': 'select' }\" filter-data=\"servicesFilter($column)\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{order.service.label | cut:true:35:' ...'}}</a></td>\n" +
    "\t\t        <td data-title=\"'Quantity'\" sortable=\"'orderCount'\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{order.orderCount | numeral}}</a>\n" +
    "\t\t        <td data-title=\"'Price'\" sortable=\"'price'\"><a data-ng-href=\"#!/orders/{{order._id}}\">{{getOrderPrice() | numeral:'$0,0.00'}}</a></td>\n" +
    "\t\t\t\t\t\t<td data-title=\"'Email'\" sortable=\"'service.user.username'\" filter=\"{ 'username': 'text' }\">{{order.user.username | cut:true:35:' ...'}}</td>\n" +
    "\t\t        <td data-title=\"'Status'\" sortable=\"'status'\"><a data-ng-href=\"#!/orders/{{order._id}}\">\n" +
    "\t\t        \t<span ng-if=\"!order.paid && !order.checkout\" class='order-status-cancelled'>\n" +
    "\t\t\t        \tAbandoned Checkout\n" +
    "\t\t\t        </span>\n" +
    "\t\t\t        <span ng-if=\"!order.paid && order.checkout\" class='order-status-verifying'>\n" +
    "\t\t\t        \tAuthorizing Payment\n" +
    "\t\t\t        </span>\n" +
    "\n" +
    "\t\t\t         <span ng-if=\"order.verified && !order.completed\" class='order-status-verifying'>\n" +
    "\t\t\t        \tVerifying payment\n" +
    "\t\t\t        </span>\n" +
    "\n" +
    "\t\t\t        <span ng-if=\"order.paid && order.completed && !order.cancelled && !order.verified\" class='order-status-verifying'>\n" +
    "\t\t\t        \tVerifying\n" +
    "\t\t\t        </span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && order.completed && !order.cancelled && order.verified\" class='order-status-completed'>\n" +
    "\t\t        \t\tCompleted\n" +
    "\t\t        \t</span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && order.cancelled\" class='order-status-cancelled'>\n" +
    "\t\t        \t\tCancelled\n" +
    "\t\t        \t</span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && !order.completed && !order.cancelled && !order.provider && !order.verified\" class='order-status-progress'>\n" +
    "\t\t        \t\tIn Progress\n" +
    "\t\t        \t</span>\n" +
    "\t\t        \t<span ng-if=\"order.paid && !order.completed && !order.cancelled && order.provider && !order.verified\" class='order-status-progress'>\n" +
    "\t\t        \t\tIn Progress\n" +
    "\t\t        \t</span>\n" +
    "\t\t        </a></td>\n" +
    "\t\t      </a>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "\n" +
    "    <h1 data-ng-hide=\"!orders || orders.length || !global.hasRole('customer')\" class='no-orders'>No orders yet.<span> <br> Why don't you <a href=\"/#!/orders/create\">Place an Order</a>?</span></h1>\n" +
    "</section>\n" +
    "\n"
  );


  $templateCache.put('public/api/views/api_help.html',
    "<style type=\"text/css\">\n" +
    "      * {\n" +
    "        margin: 0;\n" +
    "        padding: 0;\n" +
    "      }\n" +
    "      body {\n" +
    "        font: 15px/1.5 'Open Sans', sans-serif;\n" +
    "      }\n" +
    "      body,\n" +
    "      a {\n" +
    "        color: #282828;\n" +
    "      }\n" +
    "      \n" +
    "     \n" +
    "      code {\n" +
    "        background: #fff;\n" +
    "        border: 1px solid #d8d8d8;\n" +
    "        display: block;\n" +
    "        font-size: 11px;\n" +
    "        margin-bottom: 2em;\n" +
    "        padding: 1em;\n" +
    "        word-wrap: break-word;\n" +
    "        white-space: normal;\n" +
    "      }\n" +
    "      code.request:hover {\n" +
    "        cursor: pointer;\n" +
    "      }\n" +
    "      code.request a:hover {\n" +
    "        text-decoration: underline;\n" +
    "      }\n" +
    "      code.request:before {\n" +
    "        color: #006ce1;\n" +
    "        content: 'GET ';\n" +
    "      }\n" +
    "      code.js {\n" +
    "        white-space-collapse: discard;\n" +
    "      }\n" +
    "      code.js:hover {\n" +
    "        cursor: pointer;\n" +
    "      }\n" +
    "      \n" +
    "    </style>\n" +
    "<section data-ng-controller=\"ApiCtrl\" data-ng-init=\"initCreate()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t   API Info\n" +
    "\t   <span class=\"pull-right\" ng-show=\"global.user.balance\">Balance: ${{global.user.balance}}</span>\n" +
    "\t</header>\n" +
    "\t<div class=\"col-md-4\">\n" +
    "\n" +
    "\t\t<div class=\"form-group\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "           \n" +
    "            <ui-select ng-model=\"selectedService.service\" theme=\"select2\" style=\"width: 100%;\">\n" +
    "              <ui-select-match placeholder=\"Select a service...\" >{{$select.selected.label}}</ui-select-match>\n" +
    "              <ui-select-choices group-by=\"groupByCategory\" group-filter=\"groupSort\" repeat=\"service.value as service in services | propsFilter: {label: $select.search}\"  >\n" +
    "                <div ng-bind-html=\"service.label | highlight: $select.search\" ></div>\n" +
    "              </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "          </div>\n" +
    "          \n" +
    "        </div>\n" +
    "\t\t\n" +
    "\t</div>\n" +
    "\t<div class=\"col-md-12\">\n" +
    "\t\t<div class=\"col-md-10 col-xs-10 col-sm-10\" ng-show=\"serviceObject.value\">\n" +
    "\t\t<div><h3 class=\"text-center\">{{serviceObject.label}}</h3></div>\n" +
    "\t\t<div><h5 class=\"text-center\">{{serviceObject.description}}</h5></div>\n" +
    "\t\t<div><h4>Price per minimum quantity - ${{serviceObject.price}}</h4></div>\n" +
    "\t\t<div><h4>Minimum quantity - {{serviceObject.multiplier}}</h4></div>\n" +
    "\t\t<div>\n" +
    "\t\t\t\n" +
    "\t\t\t <code>\n" +
    "\t\t\t \t<h5>Create new order</h5>\n" +
    "\t\t\t \t<span style=\"color: #006ce1\">GET </span><a>{{example_add}}</a>\n" +
    "\t\t\t \t<div>\n" +
    "\t\t\t \t\t<h4>\n" +
    "\t\t\t \t\t\t<span>service - {{api_code}}</span> <b> Each service has different api code, be careful!</b>\n" +
    "\t\t\t \t\t</h4>\n" +
    "\t\t\t \t\t<h4 ng-show=\"serviceObject.value == 'fb_comments_customelite' || serviceObject.value == 'ig_custom' \">\n" +
    "\t\t\t \t\t\tcomments - firstComment\\nSecondComment\\nThirdComment - Minimum is 5 comments, maxium 1000\n" +
    "\t\t\t \t\t</h4>\n" +
    "         \n" +
    "\t\t\t \t\t<h4 ng-show=\"serviceObject.value == 'fb_emoticons_elite'\" style=\"color: #282828;\">\n" +
    "\t\t\t \t\t\t<p> emoticon - 26 = Love</p>\n" +
    "\t\t\t \t\t\t<p> emoticon - 27 = HAHA</p>\n" +
    "\t\t\t \t\t\t<p> emoticon - 28 = Wow</p>\n" +
    "\t\t\t \t\t\t<p> emoticon - 29 = Sad</p>\n" +
    "\t\t\t \t\t\t<p> emoticon - 30 = Angry</p>\n" +
    "\n" +
    "\t\t\t \t\t</h4>\n" +
    "\t\t\t \t\t<h4 ng-show=\"serviceObject.value == 'yt_views_smlite_packages'\">\n" +
    "\t\t\t \t\t\t<p> emoticon - 1 = Fast 10k - 30k per day\n" +
    "            \t\t\t<p>emoticon - 2 = Very Fast - 20k - 50k per day\n" +
    "            \t\t\t<p>emoticon - 5 = Mega Fast 100k per day\n" +
    "            \t\t\t<p>emoticon - 6 = Ultra Fast - 200k per day\n" +
    "            \t\t\t<p>emoticon - 191 = Super viral fast - 600k per day\n" +
    "\t\t\t \t\t</h4>\n" +
    "\t\t\t \t\t<h4 ng-show=\"serviceObject.value == 'yt_sm_target'\">\n" +
    "\t\t\t \t\t\t     <p> emoticon - 11 = Youtube - Indians Views </p>\n" +
    "            \t\t\t<p> emoticon - 165 = Youtube - UK Views </p>\n" +
    "            \t\t\t<p> emoticon - 14 = Youtube - Russian Views </p>\n" +
    "            \t\t\t<p> emoticon - 15 = Youtube - Brazil Views </p>\n" +
    "            \t\t\t<p> emoticon - 166 = Youtube - Australia Views </p>\n" +
    "            \t\t\t<p> emoticon - 13 = Youtube - Saudi Arabia Views </p>\n" +
    "            \t\t\t<p> emoticon - 16 = Youtube - Egypt Views </p>\n" +
    "            \t\t\t<p> emoticon - 17 = Youtube France Views </p>\n" +
    "            \t\t\t<p> emoticon - 18 = Youtube Isral Views </p>\n" +
    "            \t\t\t<p> emoticon - 164 = Youtube - Germany Views </p>\n" +
    "            \t\t\t<p> emoticon - 167 = Youtube Netherlands View </p>\n" +
    "            \t\t\t<p> emoticon - 168 = Youtube - Czech Views</p>\n" +
    "\t\t\t \t\t</h4>\n" +
    "\t\t\t \t</div>\n" +
    "\t\t\t </code>\n" +
    "\t\t</div>\n" +
    "\t\t<div> \n" +
    "\t\t\t<code>\n" +
    "\t\t\t\t<h5>View order status</h5><span style=\"color: #006ce1\">GET </span><a>{{example_status}}</a>\n" +
    "\t\t\t</code>\n" +
    "    </div>\n" +
    "\n" +
    "    <div> \n" +
    "      <code>\n" +
    "        <h5>Example - check response status</h5>\n" +
    "        <a>{\n" +
    "          \"status\": \"Processing\"\n" +
    "        }</a> - <p style=\"color: black;display: inline;\">Possible responses = Processing, Waiting, Completed </p>\n" +
    "        <h5>Example error response </h5>\n" +
    "        <div>\n" +
    "          <a>{ \"error\": \"Invalid service\" }</a>\n" +
    "        </div>\n" +
    "        <h5>Example new order response</h5>\n" +
    "        <div>\n" +
    "          <a> {\n" +
    "            \"order\": \"1029292\"\n" +
    "          }\n" +
    "        </div>\n" +
    "      </code>\n" +
    "    </div>\n" +
    "\t\t\t\n" +
    "\t\n" +
    "\t</div>\n" +
    "\t\t\n" +
    "\t</div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/api/views/blacklist.html',
    "     \n" +
    "<section data-ng-controller=\"BlackListController\"  class=\"panel\">\n" +
    "\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 20px;\">\n" +
    "      <button class=\"btn btn-primary btn-lg\" ng-click=\"rantic.showAdd=true\">Add</button>\n" +
    "    </div>\n" +
    "    <div ng-show=\"rantic.showAdd\" class=\"col-md-12\" style=\"margin-top: 20px;\">\n" +
    "    <div class=\"form-group\">\n" +
    "      <div class=\"col-md-6\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"rantic.add\" placeholder=\"Enter String\" />\n" +
    "      </div>\n" +
    "      <div class=\"col-md-3\">\n" +
    "        <button class=\"btn btn-warning btn-lg\" ng-click=\"addNew()\">Submit</button>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "  </header>\n" +
    "\n" +
    "  <div class=\"col-md-12\">\n" +
    "\n" +
    "    <table ng-table=\"tableParams\" show-filter=\"true\" class=\"ng-table-responsive table table-striped table-advance table-hover orders editable-table demoTable\">\n" +
    "        <tr data-ng-repeat=\"item in $data\">\n" +
    "          <td data-title=\"'String'\" sortable=\"'string'\" filter=\"{ 'string': 'text' }\" ng-class=\"name.$dirty ? 'bg-warning' : ''\">\n" +
    "            <span class=\"editable-text\">{{item.string}}</span>\n" +
    "            <div ng-show=\"item.isEditing\" class=\"controls\" ng-class=\"name.$invalid && name.$dirty ? 'has-error' : ''\">\n" +
    "              <input type=\"text\" name=\"name\" ng-model=\"item.string\" class=\"editable-input form-control input-sm\" required />\n" +
    "            </div>\n" +
    "          </td>\n" +
    "          <td data-title=\"'Created'\" sortable=\"'created'\">{{item.created | fromNow }}</td>\n" +
    "           <td>\n" +
    "            <button class=\"btn btn-primary btn-sm\" ng-click=\"save(item)\" ng-if=\"item.isEditing\" ng-disabled=\"rowForm.$pristine || rowForm.$invalid\"><span class=\"glyphicon glyphicon-ok\"></span></button>\n" +
    "            <button class=\"btn btn-default btn-sm\" ng-click=\"item.isEditing = false\" ng-if=\"item.isEditing\"><span class=\"glyphicon glyphicon-remove\"></span></button>\n" +
    "            <button class=\"btn btn-default btn-sm\" ng-click=\"item.isEditing = true\" ng-if=\"!item.isEditing\"><span class=\"glyphicon glyphicon-pencil\"></span></button>\n" +
    "            <button class=\"btn btn-danger btn-sm\" ng-click=\"del(item)\" ng-if=\"!item.isEditing\"><span class=\"glyphicon glyphicon-trash\"></span></button>\n" +
    "          </td>\n" +
    "        </tr>\n" +
    "           \n" +
    "    </table>\n" +
    "\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "</section>\n"
  );


  $templateCache.put('public/api/views/email.html',
    "     \n" +
    "<section data-ng-controller=\"EmailController\"  class=\"panel\" ng-init=\"initialize(); initCreate();\">\n" +
    "  <header class=\"\" style=\"margin-top: 20px;\">\n" +
    "    <div class=\"col-md-6\">\n" +
    "\n" +
    "        <ui-select ng-model=\"selectedService.service\" theme=\"select2\" style=\"width: 100%;\" ng-change=\"runner()\">\n" +
    "          <ui-select-match placeholder=\"Select a service...\" >{{$select.selected.label}}</ui-select-match>\n" +
    "          <ui-select-choices group-by=\"groupByCategory\" group-filter=\"groupSort\" repeat=\"service.value as service in services | propsFilter: {label: $select.search}\"  >\n" +
    "                <div ng-bind-html=\"service.label | highlight: $select.search\" ></div>\n" +
    "          </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"col-md-6\">\n" +
    "      <button class='btn btn-action pull-right' ng-show=\"global.hasRole('provider') || global.hasRole('admin')\" data-ng-click=\"bulkCopy()\">Copy Emails</button>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-md-4 pull-left\" style=\"margin-top: 15px;\">\n" +
    "      <div ng-repeat=\"service in multiple_services\" >\n" +
    "        <span class=\"col-md-8\" style=\"margin-bottom: 5px;\">{{service}}</span>\n" +
    "        <span class=\"col-md-4\" style=\"margin-bottom: 5px;\"><a class='btn btn-sm btn-danger' ng-click='removeLine($index)' >-</a></span>\n" +
    "      </div>\n" +
    "     \n" +
    "    </div>\n" +
    "      <div class=\"col-md-12 pull-left\" ng-show=\"multiple_services.length\">\n" +
    "        <div class=\"col-md-offset-1 col-md-4\"><button class=\"btn btn-primary btn-lg\" ng-click=\"runSearch();\">Search</button></div>\n" +
    "      </div>\n" +
    "  </header>\n" +
    "\n" +
    "  <div class=\"col-md-12\">\n" +
    "  <img ng-show=\"showLoader\" src=\"http://3.bp.blogspot.com/-qAZEM0Gz3ow/VC5ctIPqBrI/AAAAAAAAMvQ/tIDMkxgzTI0/s1600/15-01%2B~%2BGIF%2B~%2BPlease%2BWait.gif\" />\n" +
    "    <table ng-hide=\"showLoader\" ng-table=\"tableParams\" show-filter=\"true\" class=\"ng-table-responsive table table-striped table-advance table-hover orders\">\n" +
    "        <tr data-ng-repeat=\"item in $data\">\n" +
    "          <td data-title=\"'Email'\"  filter=\"{ 'email': 'text' }\">\n" +
    "            {{item.email}}\n" +
    "          </td>\n" +
    "          <td data-title=\"'Service'\">{{item.service | cut:true:35:' ...'}}</td>\n" +
    "          <td data-title=\"'Total Spent'\"  filter=\"{ 'price': 'text' }\">{{item.price | numeral:'$0,0.00' }}</td>\n" +
    "           \n" +
    "        </tr>\n" +
    "           \n" +
    "    </table>\n" +
    "\n" +
    "\n" +
    "  </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "</section>\n"
  );

}]);
