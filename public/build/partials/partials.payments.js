angular.module('mean.payments').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/payments/views/checkout.html',
    "<section class='panel'>\n" +
    "  <div ng-controller=\"CheckoutController\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Top Up\n" +
    "    </header>\n" +
    "    <div class='checkout-container'>\n" +
    "      <tabset justified=\"true\">\n" +
    "        <tab ng-controller=\"BillingController\" heading=\"Billing Information\" active=\"tabs[0].active\" disabled=\"tabs[0].disabled\">\n" +
    "          <form class='checkout-form' action=\"https://www.2checkout.com/checkout/purchase\" method=\"post\">\n" +
    "            <div class='accept-cards'><img src=\"/public/images/paypal_accept_cards.png\"></div>\n" +
    "            <input type='hidden' name='sid' value='102379918' />\n" +
    "            <input type='hidden' name='mode' value='2CO' />\n" +
    "            <input type='hidden' name='li_0_product_id' ng-model=\"productId\" />\n" +
    "            <input type='hidden' name='li_0_type' value='product' />\n" +
    "            <input type='hidden' name='li_0_name' value='Rantic Credit' />\n" +
    "            <input type='hidden' name='li_0_merchant_order_id' ng-model=\"orderId\" />\n" +
    "            <input type=\"hidden\" name=\"paypal_direct\" ng-model=\"paypal_direct\" value=\"{{paypal_direct}}\">\n" +
    "            <input type=\"hidden\" id=\"return_url\" name=\"return_url\" value=\"https://panel.rantic.com/return/\">\n" +
    "            <input type=\"hidden\" id=\"x_receipt_link_url\" name=\"x_receipt_link_url\" value=\"https://panel.rantic.com/return/\">\n" +
    "\n" +
    "            <label>Payment Method</label>\n" +
    "            <div class=\"btn-group form-control payment-method\">\n" +
    "              <label class=\"btn btn-default\" ng-model=\"paymentMethod\" btn-radio=\"'CreditCard'\">Credit Card</label>\n" +
    "              <label class=\"btn btn-default\" ng-model=\"paymentMethod\" btn-radio=\"'Paypal'\">Paypal</label>\n" +
    "            </div>\n" +
    "            <label>Amount of credit to top up (in US$)</label>\n" +
    "            <input type='number' name='li_0_price' class='form-control' ng-model=\"price\" required/>\n" +
    "            <label>Account holder name</label>\n" +
    "            <input type='text' name='card_holder_name' ng-model=\"cardName\" class='form-control' required/>\n" +
    "            <label>Email</label>\n" +
    "            <input type='text' name='email' ng-model=\"cardEmail\" placeholder='Enter billing email' class='form-control' required/>\n" +
    "            <label>Phone</label>\n" +
    "            <input type='phone' name='phone' placeholder='614-921-2450' class='form-control' ng-model=\"phone\" required/>\n" +
    "            <label>Country</label>\n" +
    "            <select id=\"country\" name=\"country\" ng-model=\"country\" class='form-control'>\n" +
    "              <option value=\"\">Choose Country</option>\n" +
    "                <option value=\"USA\" selected=\"\">United States</option>\n" +
    "                <option value=\"GBR\">United Kingdom</option>\n" +
    "                <option value=\"CAN\">Canada</option>\n" +
    "                <option value=\"AUS\">Australia</option>\n" +
    "                <option disabled=\"\">—</option>\n" +
    "                <option value=\"ALA\">Åland Islands</option>\n" +
    "                <option value=\"AFG\">Afghanistan</option>\n" +
    "                <option value=\"ALB\">Albania</option>\n" +
    "                <option value=\"DZA\">Algeria</option>\n" +
    "                <option value=\"ASM\">American Samoa</option>\n" +
    "                <option value=\"AND\">Andorra</option>\n" +
    "                <option value=\"AGO\">Angola</option>\n" +
    "                <option value=\"AIA\">Anguilla</option>\n" +
    "                <option value=\"ATA\">Antarctica</option>\n" +
    "                <option value=\"ATG\">Antigua and Barbuda</option>\n" +
    "                <option value=\"ARG\">Argentina</option>\n" +
    "                <option value=\"ARM\">Armenia</option>\n" +
    "                <option value=\"ABW\">Aruba</option>\n" +
    "                <option value=\"AUS\">Australia</option>\n" +
    "                <option value=\"AUT\">Austria</option>\n" +
    "                <option value=\"AZE\">Azerbaijan</option>\n" +
    "                <option value=\"BHS\">Bahamas</option>\n" +
    "                <option value=\"BHR\">Bahrain</option>\n" +
    "                <option value=\"BGD\">Bangladesh</option>\n" +
    "                <option value=\"BRB\">Barbados</option>\n" +
    "                <option value=\"BLR\">Belarus</option>\n" +
    "                <option value=\"BEL\">Belgium</option>\n" +
    "                <option value=\"BLZ\">Belize</option>\n" +
    "                <option value=\"BEN\">Benin</option>\n" +
    "                <option value=\"BMU\">Bermuda</option>\n" +
    "                <option value=\"BTN\">Bhutan</option>\n" +
    "                <option value=\"BOL\">Bolivia</option>\n" +
    "                <option value=\"BES\">Bonaire, Sint Eustatius and Saba</option>\n" +
    "                <option value=\"BIH\">Bosnia and Herzegovina</option>\n" +
    "                <option value=\"BWA\">Botswana</option>\n" +
    "                <option value=\"BVT\">Bouvet Island</option>\n" +
    "                <option value=\"BRA\">Brazil</option>\n" +
    "                <option value=\"IOT\">British Indian Ocean Territory</option>\n" +
    "                <option value=\"BRN\">Brunei Darussalam</option>\n" +
    "                <option value=\"BGR\">Bulgaria</option>\n" +
    "                <option value=\"BFA\">Burkina Faso</option>\n" +
    "                <option value=\"BDI\">Burundi</option>\n" +
    "                <option value=\"KHM\">Cambodia</option>\n" +
    "                <option value=\"CMR\">Cameroon</option>\n" +
    "                <option value=\"CAN\">Canada</option>\n" +
    "                <option value=\"CPV\">Cape Verde</option>\n" +
    "                <option value=\"CYM\">Cayman Islands</option>\n" +
    "                <option value=\"CAF\">Central African Republic</option>\n" +
    "                <option value=\"TCD\">Chad</option>\n" +
    "                <option value=\"CHL\">Chile</option>\n" +
    "                <option value=\"CHN\">China</option>\n" +
    "                <option value=\"CXR\">Christmas Island</option>\n" +
    "                <option value=\"CCK\">Cocos (Keeling) Islands</option>\n" +
    "                <option value=\"COL\">Colombia</option>\n" +
    "                <option value=\"COM\">Comoros</option>\n" +
    "                <option value=\"COG\">Congo</option>\n" +
    "                <option value=\"COD\">Congo, the Democratic Republic of the</option>\n" +
    "                <option value=\"COK\">Cook Islands</option>\n" +
    "                <option value=\"CRI\">Costa Rica</option>\n" +
    "                <option value=\"CIV\">Cote D'ivoire</option>\n" +
    "                <option value=\"HRV\">Croatia (Hrvatska)</option>\n" +
    "                <option value=\"CYP\">Cyprus</option>\n" +
    "                <option value=\"CZE\">Czech Republic</option>\n" +
    "                <option value=\"DNK\">Denmark</option>\n" +
    "                <option value=\"DJI\">Djibouti</option>\n" +
    "                <option value=\"DMA\">Dominica</option>\n" +
    "                <option value=\"DOM\">Dominican Republic</option>\n" +
    "                <option value=\"ECU\">Ecuador</option>\n" +
    "                <option value=\"EGY\">Egypt</option>\n" +
    "                <option value=\"SLV\">El Salvador</option>\n" +
    "                <option value=\"GNQ\">Equatorial Guinea</option>\n" +
    "                <option value=\"ERI\">Eritrea</option>\n" +
    "                <option value=\"EST\">Estonia</option>\n" +
    "                <option value=\"ETH\">Ethiopia</option>\n" +
    "                <option value=\"FLK\">Falkland Islands (Malvinas)</option>\n" +
    "                <option value=\"FRO\">Faroe Islands</option>\n" +
    "                <option value=\"FJI\">Fiji</option>\n" +
    "                <option value=\"FIN\">Finland</option>\n" +
    "                <option value=\"FRA\">France</option>\n" +
    "                <option value=\"GUF\">French Guiana</option>\n" +
    "                <option value=\"PYF\">French Polynesia</option>\n" +
    "                <option value=\"ATF\">French Southern Territories</option>\n" +
    "                <option value=\"GAB\">Gabon</option>\n" +
    "                <option value=\"GMB\">Gambia</option>\n" +
    "                <option value=\"GEO\">Georgia</option>\n" +
    "                <option value=\"DEU\">Germany</option>\n" +
    "                <option value=\"GHA\">Ghana</option>\n" +
    "                <option value=\"GIB\">Gibraltar</option>\n" +
    "                <option value=\"GRC\">Greece</option>\n" +
    "                <option value=\"GRL\">Greenland</option>\n" +
    "                <option value=\"GRD\">Grenada</option>\n" +
    "                <option value=\"GLP\">Guadeloupe</option>\n" +
    "                <option value=\"GUM\">Guam</option>\n" +
    "                <option value=\"GTM\">Guatemala</option>\n" +
    "                <option value=\"GGY\">Guernsey</option>\n" +
    "                <option value=\"GIN\">Guinea</option>\n" +
    "                <option value=\"GNB\">Guinea-Bissau</option>\n" +
    "                <option value=\"GUY\">Guyana</option>\n" +
    "                <option value=\"HTI\">Haiti</option>\n" +
    "                <option value=\"HMD\">Heard Island and Mcdonald Islands</option>\n" +
    "                <option value=\"HND\">Honduras</option>\n" +
    "                <option value=\"HKG\">Hong Kong</option>\n" +
    "                <option value=\"HUN\">Hungary</option>\n" +
    "                <option value=\"ISL\">Iceland</option>\n" +
    "                <option value=\"IND\">India</option>\n" +
    "                <option value=\"IDN\">Indonesia</option>\n" +
    "                <option value=\"IRQ\">Iraq</option>\n" +
    "                <option value=\"IRL\">Ireland</option>\n" +
    "                <option value=\"IMN\">Isle of Man</option>\n" +
    "                <option value=\"ISR\">Israel</option>\n" +
    "                <option value=\"ITA\">Italy</option>\n" +
    "                <option value=\"JAM\">Jamaica</option>\n" +
    "                <option value=\"JPN\">Japan</option>\n" +
    "                <option value=\"JEY\">Jersey</option>\n" +
    "                <option value=\"JOR\">Jordan</option>\n" +
    "                <option value=\"KAZ\">Kazakhstan</option>\n" +
    "                <option value=\"KEN\">Kenya</option>\n" +
    "                <option value=\"KIR\">Kiribati</option>\n" +
    "                <option value=\"KOR\">Korea, Republic of</option>\n" +
    "                <option value=\"KWT\">Kuwait</option>\n" +
    "                <option value=\"KGZ\">Kyrgyzstan</option>\n" +
    "                <option value=\"LAO\">Lao People's Democratic Republic</option>\n" +
    "                <option value=\"LVA\">Latvia</option>\n" +
    "                <option value=\"LBN\">Lebanon</option>\n" +
    "                <option value=\"LSO\">Lesotho</option>\n" +
    "                <option value=\"LBR\">Liberia</option>\n" +
    "                <option value=\"LBY\">Libyan Arab Jamahiriya</option>\n" +
    "                <option value=\"LIE\">Liechtenstein</option>\n" +
    "                <option value=\"LTU\">Lithuania</option>\n" +
    "                <option value=\"LUX\">Luxembourg</option>\n" +
    "                <option value=\"MAC\">Macao</option>\n" +
    "                <option value=\"MKD\">Macedonia</option>\n" +
    "                <option value=\"MDG\">Madagascar</option>\n" +
    "                <option value=\"MWI\">Malawi</option>\n" +
    "                <option value=\"MYS\">Malaysia</option>\n" +
    "                <option value=\"MDV\">Maldives</option>\n" +
    "                <option value=\"MLI\">Mali</option>\n" +
    "                <option value=\"MLT\">Malta</option>\n" +
    "                <option value=\"MHL\">Marshall Islands</option>\n" +
    "                <option value=\"MTQ\">Martinique</option>\n" +
    "                <option value=\"MRT\">Mauritania</option>\n" +
    "                <option value=\"MUS\">Mauritius</option>\n" +
    "                <option value=\"MYT\">Mayotte</option>\n" +
    "                <option value=\"MEX\">Mexico</option>\n" +
    "                <option value=\"FSM\">Micronesia, Federated States of</option>\n" +
    "                <option value=\"MDA\">Moldova, Republic of</option>\n" +
    "                <option value=\"MCO\">Monaco</option>\n" +
    "                <option value=\"MNG\">Mongolia</option>\n" +
    "                <option value=\"MNE\">Montenegro</option>\n" +
    "                <option value=\"MSR\">Montserrat</option>\n" +
    "                <option value=\"MAR\">Morocco</option>\n" +
    "                <option value=\"MOZ\">Mozambique</option>\n" +
    "                <option value=\"MMR\">Myanmar</option>\n" +
    "                <option value=\"NAM\">Namibia</option>\n" +
    "                <option value=\"NRU\">Nauru</option>\n" +
    "                <option value=\"NPL\">Nepal</option>\n" +
    "                <option value=\"NLD\">Netherlands</option>\n" +
    "                <option value=\"ANT\">Netherlands Antilles</option>\n" +
    "                <option value=\"NCL\">New Caledonia</option>\n" +
    "                <option value=\"NZL\">New Zealand</option>\n" +
    "                <option value=\"NIC\">Nicaragua</option>\n" +
    "                <option value=\"NER\">Niger</option>\n" +
    "                <option value=\"NGA\">Nigeria</option>\n" +
    "                <option value=\"NIU\">Niue</option>\n" +
    "                <option value=\"NFK\">Norfolk Island</option>\n" +
    "                <option value=\"MNP\">Northern Mariana Islands</option>\n" +
    "                <option value=\"NOR\">Norway</option>\n" +
    "                <option value=\"OMN\">Oman</option>\n" +
    "                <option value=\"PAK\">Pakistan</option>\n" +
    "                <option value=\"PLW\">Palau</option>\n" +
    "                <option value=\"PSE\">Palestinian Territory, Occupied</option>\n" +
    "                <option value=\"PAN\">Panama</option>\n" +
    "                <option value=\"PNG\">Papua New Guinea</option>\n" +
    "                <option value=\"PRY\">Paraguay</option>\n" +
    "                <option value=\"PER\">Peru</option>\n" +
    "                <option value=\"PHL\">Philippines</option>\n" +
    "                <option value=\"PCN\">Pitcairn</option>\n" +
    "                <option value=\"POL\">Poland</option>\n" +
    "                <option value=\"PRT\">Portugal</option>\n" +
    "                <option value=\"PRI\">Puerto Rico</option>\n" +
    "                <option value=\"QAT\">Qatar</option>\n" +
    "                <option value=\"REU\">Reunion</option>\n" +
    "                <option value=\"ROU\">Romania</option>\n" +
    "                <option value=\"RUS\">Russian Federation</option>\n" +
    "                <option value=\"RWA\">Rwanda</option>\n" +
    "                <option value=\"SHN\">Saint Helena</option>\n" +
    "                <option value=\"KNA\">Saint Kitts and Nevis</option>\n" +
    "                <option value=\"LCA\">Saint Lucia</option>\n" +
    "                <option value=\"SPM\">Saint Pierre and Miquelon</option>\n" +
    "                <option value=\"VCT\">Saint Vincent and the Grenadines</option>\n" +
    "                <option value=\"WSM\">Samoa</option>\n" +
    "                <option value=\"SMR\">San Marino</option>\n" +
    "                <option value=\"STP\">Sao Tome and Principe</option>\n" +
    "                <option value=\"SAU\">Saudi Arabia</option>\n" +
    "                <option value=\"SEN\">Senegal</option>\n" +
    "                <option value=\"SRB\">Serbia</option>\n" +
    "                <option value=\"SCG\">Serbia and Montenegro</option>\n" +
    "                <option value=\"SYC\">Seychelles</option>\n" +
    "                <option value=\"SLE\">Sierra Leone</option>\n" +
    "                <option value=\"SGP\">Singapore</option>\n" +
    "                <option value=\"SVK\">Slovakia</option>\n" +
    "                <option value=\"SVN\">Slovenia</option>\n" +
    "                <option value=\"SLB\">Solomon Islands</option>\n" +
    "                <option value=\"SOM\">Somalia</option>\n" +
    "                <option value=\"ZAF\">South Africa</option>\n" +
    "                <option value=\"SGS\">South Georgia and the South Sandwich Islands</option>\n" +
    "                <option value=\"ESP\">Spain</option>\n" +
    "                <option value=\"LKA\">Sri Lanka</option>\n" +
    "                <option value=\"SUR\">Suriname</option>\n" +
    "                <option value=\"SJM\">Svalbard and Jan Mayen Islands</option>\n" +
    "                <option value=\"SWZ\">Swaziland</option>\n" +
    "                <option value=\"SWE\">Sweden</option>\n" +
    "                <option value=\"CHE\">Switzerland</option>\n" +
    "                <option value=\"TWN\">Taiwan</option>\n" +
    "                <option value=\"TJK\">Tajikistan</option>\n" +
    "                <option value=\"TZA\">Tanzania, United Republic of</option>\n" +
    "                <option value=\"THA\">Thailand</option>\n" +
    "                <option value=\"TLS\">Timor-Leste</option>\n" +
    "                <option value=\"TGO\">Togo</option>\n" +
    "                <option value=\"TKL\">Tokelau</option>\n" +
    "                <option value=\"TON\">Tonga</option>\n" +
    "                <option value=\"TTO\">Trinidad and Tobago</option>\n" +
    "                <option value=\"TUN\">Tunisia</option>\n" +
    "                <option value=\"TUR\">Turkey</option>\n" +
    "                <option value=\"TKM\">Turkmenistan</option>\n" +
    "                <option value=\"TCA\">Turks and Caicos Islands</option>\n" +
    "                <option value=\"TUV\">Tuvalu</option>\n" +
    "                <option value=\"UGA\">Uganda</option>\n" +
    "                <option value=\"UKR\">Ukraine</option>\n" +
    "                <option value=\"ARE\">United Arab Emirates</option>\n" +
    "                <option value=\"GBR\">United Kingdom</option>\n" +
    "                <option value=\"USA\" selected=\"\">United States</option>\n" +
    "                <option value=\"UMI\">United States Minor Outlying Islands</option>\n" +
    "                <option value=\"URY\">Uruguay</option>\n" +
    "                <option value=\"UZB\">Uzbekistan</option>\n" +
    "                <option value=\"VUT\">Vanuatu</option>\n" +
    "                <option value=\"VAT\">Vatican City State (Holy See)</option>\n" +
    "                <option value=\"VEN\">Venezuela</option>\n" +
    "                <option value=\"VNM\">Viet Nam</option>\n" +
    "                <option value=\"VGB\">Virgin Islands, British</option>\n" +
    "                <option value=\"VIR\">Virgin Islands, U.S.</option>\n" +
    "                <option value=\"WLF\">Wallis and Futuna Islands</option>\n" +
    "                <option value=\"ESH\">Western Sahara</option>\n" +
    "                <option value=\"YEM\">Yemen</option>\n" +
    "                <option value=\"YUG\">Yugoslavia</option>\n" +
    "                <option value=\"ZAR\">Zaire</option>\n" +
    "                <option value=\"ZMB\">Zambia</option>\n" +
    "                <option value=\"ZWE\">Zimbabwe</option>\n" +
    "            </select>\n" +
    "            <label>Address</label>\n" +
    "            <input type='text' name='street_address' placeholder=\"Enter billing address\" class='form-control' ng-model=\"address\" required/>\n" +
    "            <input type='text' name='street_address2' placeholder=\"Enter billing address 2\" class='form-control' ng-model=\"address2\" required/>\n" +
    "            <label>City</label>\n" +
    "            <input type='text' name='city' placeholder=\"Enter billing city\" class='form-control' ng-model=\"city\" required/>\n" +
    "            <label>State</label>\n" +
    "            <select name=\"state\" id=\"state\" ng-model=\"billingState\" class='form-control' ng-show=\"country == 'USA'\">\n" +
    "              <option value=\"\">Select one</option>\n" +
    "              <option value=\"AL\">Alabama</option>\n" +
    "              <option value=\"AK\">Alaska</option>\n" +
    "              <option value=\"AS\">American Samoa</option>\n" +
    "              <option value=\"AZ\">Arizona</option>\n" +
    "              <option value=\"AR\">Arkansas</option>\n" +
    "              <option value=\"AA\">Armed Forces Americas</option>\n" +
    "              <option value=\"AE\">Armed Forces Europe, Middle East, &amp; Canada</option>\n" +
    "              <option value=\"AP\">Armed Forces Pacific</option>\n" +
    "              <option value=\"CA\">California</option>\n" +
    "              <option value=\"CO\">Colorado</option>\n" +
    "              <option value=\"CT\">Connecticut</option>\n" +
    "              <option value=\"DE\">Delaware</option>\n" +
    "              <option value=\"DC\">District of Columbia</option>\n" +
    "              <option value=\"FM\">Federated States of Micronesia</option>\n" +
    "              <option value=\"FL\">Florida</option>\n" +
    "              <option value=\"GA\">Georgia</option>\n" +
    "              <option value=\"GU\">Guam</option>\n" +
    "              <option value=\"HI\">Hawaii</option>\n" +
    "              <option value=\"ID\">Idaho</option>\n" +
    "              <option value=\"IL\">Illinois</option>\n" +
    "              <option value=\"IN\">Indiana</option>\n" +
    "              <option value=\"IA\">Iowa</option>\n" +
    "              <option value=\"KS\">Kansas</option>\n" +
    "              <option value=\"KY\">Kentucky</option>\n" +
    "              <option value=\"LA\">Louisiana</option>\n" +
    "              <option value=\"ME\">Maine</option>\n" +
    "              <option value=\"MH\">Marshall Islands</option>\n" +
    "              <option value=\"MD\">Maryland</option>\n" +
    "              <option value=\"MA\">Massachusetts</option>\n" +
    "              <option value=\"MI\">Michigan</option>\n" +
    "              <option value=\"MN\">Minnesota</option>\n" +
    "              <option value=\"MS\">Mississippi</option>\n" +
    "              <option value=\"MO\">Missouri</option>\n" +
    "              <option value=\"MT\">Montana</option>\n" +
    "              <option value=\"NE\">Nebraska</option>\n" +
    "              <option value=\"NV\">Nevada</option>\n" +
    "              <option value=\"NH\">New Hampshire</option>\n" +
    "              <option value=\"NJ\">New Jersey</option>\n" +
    "              <option value=\"NM\">New Mexico</option>\n" +
    "              <option value=\"NY\">New York</option>\n" +
    "              <option value=\"NC\">North Carolina</option>\n" +
    "              <option value=\"ND\">North Dakota</option>\n" +
    "              <option value=\"MP\">Northern Mariana Islands</option>\n" +
    "              <option value=\"OH\">Ohio</option>\n" +
    "              <option value=\"OK\">Oklahoma</option>\n" +
    "              <option value=\"OR\">Oregon</option>\n" +
    "              <option value=\"PW\">Palau</option>\n" +
    "              <option value=\"PA\">Pennsylvania</option>\n" +
    "              <option value=\"PR\">Puerto Rico</option>\n" +
    "              <option value=\"RI\">Rhode Island</option>\n" +
    "              <option value=\"SC\">South Carolina</option>\n" +
    "              <option value=\"SD\">South Dakota</option>\n" +
    "              <option value=\"TN\">Tennessee</option>\n" +
    "              <option value=\"TX\">Texas</option>\n" +
    "              <option value=\"UT\">Utah</option>\n" +
    "              <option value=\"VT\">Vermont</option>\n" +
    "              <option value=\"VI\">Virgin Islands</option>\n" +
    "              <option value=\"VA\">Virginia</option>\n" +
    "              <option value=\"WA\">Washington</option>\n" +
    "              <option value=\"WV\">West Virginia</option>\n" +
    "              <option value=\"WI\">Wisconsin</option>\n" +
    "              <option value=\"WY\">Wyoming</option>\n" +
    "            </select>\n" +
    "            <label>ZIP code</label>\n" +
    "            <input type='text' name='zip' placeholder=\"123456\" class='form-control' ng-model=\"zip\" required/>\n" +
    "            <button class='btn btn-block btn-success' ng-click='submitBilling($event)'>Continue to Checkout &#10095;</button>\n" +
    "          </form>\n" +
    "        </tab>\n" +
    "        <tab ng-controller=\"PaymentController\" heading=\"Payment & Checkout\" disabled=\"tabs[1].disabled\" active=\"tabs[1].active\">\n" +
    "          <form class='checkout-form' id=\"ccForm\">\n" +
    "            <div class='accept-cards'><img src=\"/public/images/paypal_accept_cards.png\"></div>\n" +
    "            <input id=\"token\" name=\"token\" type=\"hidden\" value=\"\">\n" +
    "            <div class='form-group'>\n" +
    "                <label>Card Number</label>\n" +
    "                <input placeholder=\"1234123412341234\" class='form-control' ng-model=\"ccNo\" id=\"ccNo\" type=\"text\" size=\"20\" value=\"\" autocomplete=\"off\" required />\n" +
    "            </div>\n" +
    "            <label>Expiration Date (MM/YYYY)</label>\n" +
    "            <div class='form-group row'>\n" +
    "              <div class='col-md-6'>\n" +
    "                <input placeholder=\"01\" class='form-control' type=\"text\" size=\"2\" id=\"expMonth\" ng-model=\"expMonth\" required />\n" +
    "              </div>\n" +
    "              <div class='col-md-6'>\n" +
    "                <input placeholder=\"2025\" class='form-control' type=\"text\" size=\"2\" id=\"expYear\" ng-model=\"expYear\" required />\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class='form-group'>\n" +
    "              <label>CVC</label>\n" +
    "              <input class='form-control' placeholder=\"123\" id=\"cvv\" ng-model=\"cvv\" size=\"4\" type=\"text\" value=\"\" autocomplete=\"off\" required />\n" +
    "            </div>\n" +
    "            <button class='btn btn-block btn-success' ng-click='submitPayment($event)' ng-disabled=\"paymentButtonDisabled\">{{paymentButtonText}}</button>\n" +
    "          </form>\n" +
    "        </tab>\n" +
    "      </tabset>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/payments/views/create.html',
    "<section class='panel' data-ng-controller=\"ServicesController\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Edit Service\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"create()\" novalidate>\n" +
    "            <div class='col-md-12'>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Long Name</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"service.label\" id=\"label\" placeholder=\"Human readable service name\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Codename</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"service.value\" id=\"value\" placeholder=\"Machine readable service name\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Pattern</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"service.pattern\" id=\"pattern\" placeholder=\"Valid regex pattern to check this service URL against\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Multiplier</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"service.multiplier\" id=\"multiplier\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Minimum Quantity</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"service.minimum\" id=\"minimum\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Price</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"service.price\" id=\"price\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group col-md-12\">\n" +
    "                    <div class=\"checkbox\">\n" +
    "                      <label><input type=\"checkbox\" data-ng-model=\"service.targeted\" id=\"targeted\" placeholder=\"\">Country targeted service</label>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-9 col-md-3\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-lg btn-info col-md-12\" ng-disabled=\"disableButton\">Save Service</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/payments/views/list.html',
    "<section data-ng-controller=\"PaymentsController\" data-ng-init=\"find()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Payments\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"false\" class=\"ng-table-responsive table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"payment in $data\">\n" +
    "\t        <td data-title=\"'Date'\" sortable=\"'created'\">{{payment.created | fromNow}}</td>\n" +
    "\t\t\t\t\t<td data-title=\"'Price'\" sortable=\"'price'\">{{payment.price | numeral:'$0,0.00'}}</td>\n" +
    "\t\t\t\t\t<td data-title=\"'Email'\">{{payment.user.email}}</td>\n" +
    "\t\t\t\t\t<td data-title=\"'Order'\" sortable=\"'order'\"><a data-ng-href=\"#!/orders/{{payment.order._id}}\">{{payment.order.url | cut:true:35:' ...'}}</a></td>\n" +
    "\t\t\t\t\t<td data-title=\"'Checkout'\" sortable=\"'checkout'\">\n" +
    "\t\t\t\t\t\t<span ng-if=\"payment.checkout\" style=\"color: #3a3;\">Completed</span>\n" +
    "\t\t\t\t\t\t<span ng-if=\"!payment.checkout\" style=\"color: #a33;\">Abandoned</span>\n" +
    "\t\t\t\t\t</td>\n" +
    "\t\t\t\t\t<td data-title=\"'Approved'\" sortable=\"'approved'\">\n" +
    "\t\t\t\t\t\t<span ng-if=\"payment.approved\" style=\"color: #3a3;\">Yes</span>\n" +
    "\t\t\t\t\t\t<span ng-if=\"!payment.approved\" style=\"color: #a33;\">No</span>\n" +
    "\t\t\t\t\t</td>\n" +
    "\t\t\t\t\t<td data-title=\"'Fraud'\" sortable=\"'fraud'\">\n" +
    "\t\t\t\t\t\t<span ng-if=\"!payment.fraud\" style=\"color: #3a3;\">Pass</span>\n" +
    "\t\t\t\t\t\t<span ng-if=\"payment.fraud\" style=\"color: #a33;\">Fail</span>\n" +
    "\t\t\t\t\t</td>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/payments/views/view.html',
    "<section class='panel' data-ng-controller=\"ServicesController\" ng-init=\"findOne()\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Edit Service\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"update()\" novalidate>\n" +
    "            <div class='col-md-12'>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Long Name</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"service.label\" id=\"label\" placeholder=\"Human readable service name\" value=\"{{service.label}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Codename</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"service.value\" id=\"value\" placeholder=\"Machine readable service name\" value=\"{{service.value}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Pattern</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"service.pattern\" id=\"pattern\" placeholder=\"Valid regex pattern to check this service URL against\" value=\"{{service.pattern}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Multiplier</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"service.multiplier\" id=\"multiplier\" value=\"{{service.multiplier}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Minimum Quantity</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"service.minimum\" id=\"minimum\" value=\"{{service.minimum}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Price</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"service.price\" id=\"price\" value=\"{{service.price}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group col-md-12\">\n" +
    "                    <div class=\"checkbox\">\n" +
    "                      <label><input type=\"checkbox\" data-ng-model=\"service.targeted\" id=\"targeted\" placeholder=\"\">Country targeted service</label>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-6 col-md-3\">\n" +
    "                      <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" ng-click=\"remove()\" ng-disabled=\"disableButton\">Remove Service</button>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-3\">\n" +
    "                      <button type=\"submit\" class=\"btn btn-info btn-lg btn-block\" ng-disabled=\"disableButton\">Save Service</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</section>\n"
  );

}]);
