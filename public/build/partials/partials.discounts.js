angular.module('mean.discounts').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/discounts/views/create.html',
    "<section class='panel' data-ng-controller=\"DiscountsController\" ng-init=\"initServices()\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Create Discount\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"create()\" novalidate>\n" +
    "            <div class='col-md-12'>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Discount Code</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"discount.code\" id=\"code\" placeholder=\"Discount code\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Price</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"discount.price\" id=\"price\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Percent</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"discount.percent\" id=\"percent\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Services</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                      <ui-select ng-model=\"discount.services\" theme=\"select2\" style=\"width: 100%;\" multiple>\n" +
    "                        <ui-select-match placeholder=\"Select a service...\">{{$item.label}}</ui-select-match>\n" +
    "                        <ui-select-choices group-by=\"groupByCategory\" repeat=\"service.value as service in services | propsFilter: {label: $select.search}\">\n" +
    "                          <div ng-bind-html=\"service.label | highlight: $select.search\"></div>\n" +
    "                        </ui-select-choices>\n" +
    "                      </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-9 col-md-3\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-lg btn-info col-md-12\" ng-disabled=\"disableButton\">Save Discount</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/discounts/views/list.html',
    "<section data-ng-controller=\"DiscountsController\" data-ng-init=\"find()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Discounts\n" +
    "\t\t\t<button class='btn btn-action pull-right' data-ng-click=\"goCreateDiscount()\">Create Discount</button>\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"ng-table-responsive table table-striped table-advance table-hover discounts\">\n" +
    "\t\t    <tr data-ng-repeat=\"discount in $data\">\n" +
    "\t        <td data-title=\"'Code'\" sortable=\"'code'\" filter=\"{ 'code': 'text' }\"><a data-ng-href=\"#!/discounts/{{discount._id}}\">{{discount.code | cut:true:55:' ...'}}</a></td>\n" +
    "\t\t\t\t\t<td data-title=\"'Price Discount'\" sortable=\"'price'\"><a data-ng-href=\"#!/discounts/{{discount._id}}\">{{discount.price | numeral:'$0,0.00'}}</a></td>\n" +
    "\t\t\t\t\t<td data-title=\"'Percent Discount'\" sortable=\"'percent'\"><a data-ng-href=\"#!/discounts/{{discount._id}}\">{{discount.percent | numeral:'%0,0.00'}}</a></td>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/discounts/views/view.html',
    "<section class='panel' data-ng-controller=\"DiscountsController\" ng-init=\"findOne()\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Edit Discount\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"update()\" novalidate>\n" +
    "            <div class='col-md-12'>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Discount Code</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"discount.code\" id=\"code\" placeholder=\"Discount code\" value=\"{{discount.code}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Price</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"discount.price\" id=\"price\" value=\"{{discount.price}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Percent</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"number\" class=\"form-control\" data-ng-model=\"discount.percent\" id=\"percent\" value=\"{{discount.percent}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-md-2 control-label\">Services</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                      <ui-select ng-model=\"discount.services\" theme=\"select2\" style=\"width: 100%;\" multiple>\n" +
    "                        <ui-select-match placeholder=\"Select a service...\">{{$item.label}}</ui-select-match>\n" +
    "                        <ui-select-choices group-by=\"groupByCategory\" repeat=\"service.value as service in services | propsFilter: {label: $select.search}\">\n" +
    "                          <div ng-bind-html=\"service.label | highlight: $select.search\"></div>\n" +
    "                        </ui-select-choices>\n" +
    "                      </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-6 col-md-3\">\n" +
    "                      <button type=\"button\" class=\"btn btn-info btn-lg btn-block\" ng-click=\"remove()\" ng-disabled=\"disableButton\">Remove Discount</button>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-3\">\n" +
    "                      <button type=\"submit\" class=\"btn btn-info btn-lg btn-block\" ng-disabled=\"disableButton\">Save Discount</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</section>\n"
  );

}]);
