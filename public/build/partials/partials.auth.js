angular.module('mean.auth').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/auth/views/login.html',
    "<div data-ng-controller=\"LoginCtrl\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "      Login\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"alert alert-danger animated fadeIn\" ng-show=\"loginerror\">{{loginerror}}</div>\n" +
    "            <form  ng-submit=\"login()\" class=\"signin form-horizontal\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"email\" class=\"col-md-4 control-label\">Email</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input required id=\"email\" type=\"email\" name=\"email\" placeholder=\"Email\" class=\"form-control\" ng-model=\"user.email\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"password\" class=\"col-md-4 control-label\">Password</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input required id=\"password\" type=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\" ng-model=\"user.password\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-4 col-md-8\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary\">Sign in</button>&nbsp;\n" +
    "                        or&nbsp;<a ui-sref='auth.register' class=\"show-signup\">Sign up</a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-4 col-md-8\">\n" +
    "                        Forgot your password? <a data-ng-click=\"forgotPassword()\" href=\"#\">Click here</a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('public/auth/views/register.html',
    "<div data-ng-controller=\"RegisterCtrl\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Register\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div ng-repeat=\"error in registerError\">\n" +
    "                <div class=\"alert alert-danger animated fadeIn\">{{error.msg}}</div>\n" +
    "            </div>\n" +
    "            <div class=\"alert alert-danger animated fadeIn\" ng-show=\"usernameError\">{{usernameError}}</div>\n" +
    "            <form ng-submit=\"register()\" class=\"signup form-horizontal\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"name\" class=\"col-md-4 control-label\">Full Name</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input id=\"name\" type=\"text\" name=\"name\" placeholder=\"Full name\" class=\"form-control\" ng-model=\"user.name\"/>\n" +
    "                    </div>\n" +
    "                    <label for=\"email\" class=\"col-md-4 control-label\">Email</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input id=\"email\" type=\"email\" name=\"email\" placeholder=\"Email\" class=\"form-control\" ng-model=\"user.email\"/>\n" +
    "                    </div>\n" +
    "                    <label for=\"password\" class=\"col-md-4 control-label\">Password</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input id=\"password\" type=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\" ng-model=\"user.password\"/>\n" +
    "                    </div>\n" +
    "                    <label for=\"confirmPassword\" class=\"col-md-4 control-label\">Repeat Password</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input id=\"confirmPassword\" type=\"password\" name=\"confirmPassword\" placeholder=\"Password\" class=\"form-control\" ng-model=\"user.confirmPassword\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-4 col-md-8\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary\">Sign up</button>&nbsp;\n" +
    "                        or&nbsp;<a ui-sref='auth.login' class=\"show-login\">login</a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
  
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('public/auth/views/reset.html',
    "<div data-ng-controller=\"ResetCtrl\">\n" +
    "    <header class=\"panel-heading\">\n" +
    "        Reset Password\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <form  ng-submit=\"reset()\" class=\"signin form-horizontal\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"password\" class=\"col-md-4 control-label\">Password</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input id=\"password\" type=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\" ng-model=\"user.password\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"confirmPassword\" class=\"col-md-4 control-label\">Repeat Password</label>\n" +
    "                    <div class=\"col-md-8\">\n" +
    "                        <input id=\"confirmPassword\" type=\"password\" name=\"confirmPassword\" placeholder=\"Password\" class=\"form-control\" ng-model=\"user.confirmPassword\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-4 col-md-8\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary\">Reset Password</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
           
    "    </div>\n" +
    "</div>"
  );

}]);
