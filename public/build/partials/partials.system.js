angular.module('mean.system').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/system/views/API.html',
    "<section data-ng-controller=\"APIController\" data-ng-init=\"findOrders()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t Orders list vol\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\"  class=\"table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"log in $data\" >\n" +
    "\t\t    \t{{log}}\n" +
    "\n" +
    "\t        <td data-title=\"'Date'\" > {{log.create_time | date:'yyyy-MM-dd HH:mm:ss Z' }}</td>\n" +
    "\n" +
    "\t\t\t <td data-title=\"'Submitted to API'\" sortable=\"'total'\"> {{log.total}}</td>\n" +
    "\t\t\t\n" +
    "\t\n" +
    "\t<td  data-title=\"'Order'\" ><a data-ng-href=\"#!/orders/{{log.id}}\">{{log.id | cut:true:75:' ...'}}</a></td>\n" +
    "\t\n" +
    "\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/comments.html',
    "<section data-ng-controller=\"CommentsController\" data-ng-init=\"find()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Payments\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"payment in $data\">\n" +
    "\t        <td data-title=\"'Date'\" sortable=\"'created'\">{{payment[0].created | fromNow}}</td>\n" +
    "\n" +
    "\t\t\t<td data-title=\"'Price'\" sortable=\"'price'\">{{payment[0].price | numeral:'$0,0.00'}}</td>\n" +
    "\n" +
    "\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t<td data-title=\"'Email'\" sortable=\"'username'\" filter=\"{ 'username': 'text' }\">\n" +
    "\t\t\t<a data-ng-href=\"#!/orders/{{payment[0]._id}}\">{{payment[0].user.username  | cut:true:75:' ...' }}</a></td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\t\t\t<td data-title=\"'Service'\"  filter=\"{ 'service': 'select' }\" filter-data=\"serviceFinder($column)\"><a data-ng-href=\"#!/orders/{{payment[0]._id}}\">\n" +
    "\t\t\t\t{{ payment[0].service.label | cut:true:75:' ...'}}</a></td>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\t\t\t\t\t<td data-title=\"'Order'\" sortable=\"'order'\"><a data-ng-href=\"#!/orders/{{payment[0]._id}}\">{{payment[0].url | cut:true:35:' ...'}}</a></td>\n" +
    "\t\t\t\t\t<td data-title=\"'Checkout'\" sortable=\"'checkout'\">\n" +
    "\t\t\t\t\t\t<span ng-if=\"payment[0].checkout\" style=\"color: #3a3;\">Completed</span>\n" +
    "\t\t\t\t\t\t<span ng-if=\"!payment[0].checkout\" style=\"color: #a33;\">Abandoned</span>\n" +
    "\t\t\t\t\t</td>\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t<td data-title=\"'Fraud'\" sortable=\"'fraud'\">\n" +
    "\t\t\t\t\t\t<span ng-if=\"!payment[0].fraud\" style=\"color: #3a3;\">Pass</span>\n" +
    "\t\t\t\t\t\t<span ng-if=\"payment[0].fraud\" style=\"color: #a33;\">Fail</span>\n" +
    "\t\t\t\t\t</td>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/faq.html',
    "<section data-ng-controller='FaqController' class='panel'>\n" +
    "    <div class='screen-container'>\n" +
    "        <header class=\"panel-heading higher-panel-heading\">\n" +
    "            Frequently Asked Questions\n" +
    "        </header>\n" +
    "        <div class=\"panel-body\" ng-hide=\"global.hasRole('provider')\">\n" +
    "<p>By placing an order with Rantic you automatically agree with our <a href=\"http://www.rantic.com/terms/\">terms of service</a>.</p>\n" +
    "\n" +
    "            <h2>FAQ</h2>\n" +
    "            <h3>How long does it take for you to deliver?</h3>\n" +
    "            <p>It takes us generally from 1 hour - 72 hours (It may take up more for bigger orders, We offer no gurantee of fast delivery due to the nature of this business) it all comes down to the days and orders in queue.</p>\n" +
    "            <h3>Is your service safe?</h3>\n" +
    "            <p>Yes we are using the safest methods possible to expand you and your business.</p>\n" +
    "            <h3>Do you provide real human followers views & likes?</h3>\n" +
    "            <p>Yes we provide authentic services.</p>\n" +
    "            <h3>Where do the views/followers/likes come from?</h3>\n" +
    "            <p>It is worldwide traffic/followers/views/likes.</p>\n" +
    "            <h3>Are your views adsense safe?</h3>\n" +
    "            <p>Yes absolutely.</p>\n" +
    "            <h3>How do I go viral ?</h3>\n" +
    "            <p>Usually before being featured on news or anywhere else the first step should be expanding your image and we are here to do that :).</p>\n" +
    "            <h3>Do you guarantee that the new followers will interact with me?</h3>\n" +
    "            <p>We DO NOT guarantee your new followers will interact with you, we simply guarantee you to get the followers you pay for.</p>\n" +
    "            <h3>Do you guarantee that 100% of your accounts will have a profile picture?</h3>\n" +
    "            <p>We DO NOT guarantee 100% of our accounts will have a profile picture, full bio and uploaded pictures, although we strive to make this the reality for all services that we provide.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"panel-body\" ng-show=\"global.hasRole('provider')\">\n" +
    "            <h3>A client provided an order with a private or removed page/video/profile what do I do?</h3>\n" +
    "            <p>On the orders page click on the the URL then you will see the order details, you will have a cancel option (yellow button) on the bottom left where you can cancel the order and provide explanation.</p>\n" +
    "\n" +
    "            <h3>When do I get paid?</h3>\n" +
    "            <p>Weekly when all orders are properly verified.</p>\n" +
    "\n" +
    "            <h3>I see some orders but when I click on the details & check the count I see a claim button what's that?</h3>\n" +
    "            <p>Sometimes when 2 workers working on the same service (fb likes) one needs to claim the order for the system to count you as the worker for that particular order. Once you claim the order it will be yours.</p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/header.html',
    "<div data-ng-controller=\"HeaderController\">\n" +
    "  <div class=\"sidebar-brand\">\n" +
    "    <a href=\"/\">\n" +
    "      <img src='/public/images/swenzy.png'>\n" +
    "    </a>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"account\">\n" +
    "    <ul class=\"nav nav-pills nav-stacked\" data-ng-hide=\"global.authenticated\">\n" +
    "        <li><a ui-sref='auth.register'>Register</a>\n" +
    "        </li>\n" +
    "        <li class=\"divider-vertical\"></li>\n" +
    "        <li><a ui-sref='auth.login'>Login</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <ul class=\"nav nav-pills nav-stacked\" data-ng-show=\"global.authenticated\">\n" +
    "        <li>\n" +
    "            <a href=\"#\">\n" +
    "                {{global.user.name}} <b class=\"caret\"></b>\n" +
    "            </a>\n" +
    "            <ul class=\"nav nav-pills nav-stacked\">\n" +
    "                <li class='balance balance-{{global.user.balance}}' data-ng-show=\"global.authenticated && global.user.balance > 0\"><a href=\"#\">Balance: {{global.user.balance | numeral:'$0,0.00'}}</a>\n" +
    "                </li> \n" +
    "                <li><a href=\"/logout\">Signout</a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "<!--    <ul class=\"nav nav-pills nav-stacked\" data-ng-hide=\"!global.authenticated || hasRole('admin') || hasRole('provider')\">\n" +
    "        <li class=\"divider-vertical\"></li>\n" +
    "        <li><a href=\"#\" ng-click=\"topUp();\">Top Up</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "-->\n" +
    "  </div>\n" +
    "\n" +
    "  <ul class=\"nav nav-pills nav-stacked\">\n" +
    "    <li data-ng-repeat=\"item in menus.main\" ui-route=\"/{{item.link}}\" ng-class=\"{active: $uiRoute}\">\n" +
    "        <a mean-token=\"item.link\" ui-sref='{{item.link}}' ng-click=\"updateUserInfo();\">{{item.title}}</a>\n" +
    "    </li>\n" +
    "    <li ui-route=\"faq\" ng-class=\"{active: $uiRoute}\">\n" +
    "        <a mean-token=\"item.link\" ui-sref='faq'>FAQ</a>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/index.html',
    "<section data-ng-controller=\"IndexController\" data-ng-init=\"initIndex()\" class='purple-bg'>\n" +
    "  <div class='hero-container'>\n" +
    "    <div class='hero' ng-hide=\"global.authenticated\">\n" +
    "      <h1 mean-token=\"'home-default'\">Welcome to Rantic Panel</h1>\n" +
    "      <a class='btn btn-lg btn-success' ng-href='#!/register'>Register</a> &nbsp;&nbsp;\n" +
    "      <a class='btn btn-lg btn-success' ng-href='#!/login'>Login</a>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/logs.html',
    "<section data-ng-controller=\"CommentsController\" data-ng-init=\"findLogs()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t  Logs\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"log in $data\" ng-if=\"log.data.order.url != 'add_cart'\">\n" +
    "\n" +
    "\n" +
    "\t        <td data-title=\"'Date'\" sortable=\"'created'\"> {{log.data.created | date:'yyyy-MM-dd HH:mm:ss Z' }}</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\n" +
    "\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t<td data-title=\"'Email'\" sortable=\"'username'\" filter=\"{ 'username': 'text' }\">\n" +
    "\t\t\t{{log.data.user.username  | cut:true:75:' ...' }}\n" +
    "\t\t\t\n" +
    "\n" +
    "\n" +
    "\t\t</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\t\t\t\n" +
    "\t<td  data-title=\"'Action'\" sortable=\"'order'\">\n" +
    "\t\t\n" +
    "\t\t<span ng-if=\"log.message == 'order created' || log.message == 'payment created' || log.message == 'Topup created' \" >\n" +
    "\t\t\t<a style=\"color: #F39B13;\" data-ng-href=\"#!/userActions/{{log.data.user._id}}\">\n" +
    "\t\t\t\t{{log.message}}</a>\n" +
    "\t\t</span>\n" +
    "\n" +
    "\t\t<span ng-if=\"log.message == 'order inprogress' || log.message == 'payment successful' || log.message == 'order completed' || log.message == 'TopUp'\">\n" +
    "\t\t\t<a style=\"color: #27AE61;\" data-ng-href=\"#!/userActions/{{log.data.user._id}}\">\n" +
    "\t\t\t\t{{log.message}}\n" +
    "\t\t\t</a>\n" +
    "\t\t</span>\n" +
    "\n" +
    "\t\t<span ng-if=\"log.data.action_type == 'Order cancelled' || log.message == 'order abandoned'\" >\n" +
    "\t\t\t<a style=\"color: #a33;\" data-ng-href=\"#!/userActions/{{log.data.user._id}}\">\n" +
    "\t\t\t\t{{log.message}}</a>\n" +
    "\t\t</span>\n" +
    " <!-- red #a33 -->\n" +
    "\t\t\n" +
    "\n" +
    "\t</td>\n" +
    "\t<td ng-if='log.data.order.url' data-title=\"'Order'\" sortable=\"'order'\"><a data-ng-href=\"#!/orders/{{log.data.order._id}}\">{{log.data.order.url | cut:true:75:' ...'}}</a></td>\n" +
    "\t\n" +
    "\t<td  data-title=\"'Price'\" sortable=\"'price'\">{{log.data.price}} </td>\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/manual.html',
    "<section data-ng-controller=\"ManualController\" data-ng-init=\"findOrders()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t  Manual\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"log in $data\" ng-if=\"log.url != 'add_cart'\">\n" +
    "\n" +
    "\t\t    <td data-title=\"'Type'\"> \n" +
    "\n" +
    "\t\t    \t<p ng-if=\"log.topup\">TopUP </p>\n" +
    "\t\t    \t<p ng-if=\"!log.topup\"><a data-ng-href=\"#!/orders/{{log._id}}\">Order</a></p>\n" +
    "\t\t    </td>\n" +
    "\n" +
    "\t        <td data-title=\"'Date'\" sortable=\"'created'\"> {{log.created | date:'yyyy-MM-dd HH:mm:ss Z' }}</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\n" +
    "\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t<td data-title=\"'Email'\" sortable=\"'username'\" >\n" +
    "\t\t\t{{log.user.username  | cut:true:75:' ...' }}\n" +
    "\t\t\t\n" +
    "\n" +
    "\n" +
    "\t\t</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\t\t\t\n" +
    "\t\n" +
    "\t\n" +
    "\t\n" +
    "\t<td  data-title=\"'Price'\" sortable=\"'price'\">{{log.price}} </td>\n" +
    "\t<td data-title=\"'Action'\" > <button ng-if=\"log.topup\" class=\"btn btn-sm btn-primary\" ng-click=\"verify(log)\"> Verify </button> </td>\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/newsflash.html',
    "<div ng-controller=\"NewsflashController\" ng-init=\"initNewsflash()\">\n" +
    "  <div class=\"bs-callout bs-callout-danger\" ng-show=\"newsflash.show\">\n" +
    "    <h4>{{newsflash.title}}</h4>\n" +
    "    <p>{{newsflash.message}}</p>\n" +
    "  </div>\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/orders_list.html',
    "<section data-ng-controller=\"APIController\"  class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t Orders list vol\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"log in $data\" >\n" +
    "\n" +
    "\n" +
    "\t        <td data-title=\"'Date'\" sortable=\"'created'\"> {{log.data.created | date:'yyyy-MM-dd HH:mm:ss Z' }}</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\t\t\t\n" +
    "\t\n" +
    "\t<td  data-title=\"'Order'\" sortable=\"'order'\"><a data-ng-href=\"#!/orders/{{log.data.order._id}}\">{{log.data.order._id | cut:true:75:' ...'}}</a></td>\n" +
    "\t\n" +
    "\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/system/views/setting2.html',
    "<div ng-controller=\"SettingsController\" ng-init=\"initSettings()\" class='panel'>\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    Newsflash Settings\n" +
    "  </header>\n" +
    "  <div class=\"panel-body\">\n" +
    "    <form class=\"form-horizontal\" name=\"form\" role=\"form\" data-ng-submit=\"saveSettings()\" novalidate>\n" +
    "      <div class='col-md-6'>\n" +
    "        <h4>Customers</h4>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Title</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.customer.title\" id=\"newsflash.value.customer.title\" placeholder=\"\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Message</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <textarea type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.customer.message\" id=\"newsflash.value.customer.message\" placeholder=\"\"></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" data-ng-model=\"newsflash.value.customer.show\" id=\"newsflash.value.customer.show\" placeholder=\"\">Show Customer Newsflash</label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class='col-md-6'>\n" +
    "        <h4>Providers</h4>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Title</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.provider.title\" id=\"newsflash.value.provider.title\" placeholder=\"\" required>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Message</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <textarea type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.provider.message\" id=\"newsflash.value.provider.message\" placeholder=\"\" required></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "         <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Topup min price</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"topup_settings.topup_price\" id=\"newsflash.value.provider.message\" placeholder=\"\" required/>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"form-group col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" data-ng-model=\"newsflash.value.provider.show\" id=\"newsflash.value.provider.show\" placeholder=\"\">Show Provider Newsflash</label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <div class='col-md-offset-10 col-md-2'>\n" +
    "          <button type=\"submit\" class=\"btn btn-info col-md-12\" ng-disabled=\"disableButton\">Save Settings</button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/settings.html',
    "<div ng-controller=\"SettingsController\" ng-init=\"initSettings()\" class='panel'>\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    Newsflash Settings timmu\n" +
    "  </header>\n" +
    "  <div class=\"panel-body\">\n" +
    "    <form class=\"form-horizontal\" name=\"form\" role=\"form\" data-ng-submit=\"saveSettings()\" novalidate>\n" +
    "      <div class='col-md-6'>\n" +
    "        <h4>Customers</h4>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Title</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.customer.title\" id=\"newsflash.value.customer.title\" placeholder=\"\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Message</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <textarea type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.customer.message\" id=\"newsflash.value.customer.message\" placeholder=\"\"></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" data-ng-model=\"newsflash.value.customer.show\" id=\"newsflash.value.customer.show\" placeholder=\"\">Show Customer Newsflash</label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class='col-md-6'>\n" +
    "        <h4>Providers</h4>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Title</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.provider.title\" id=\"newsflash.value.provider.title\" placeholder=\"\" required>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Message</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <textarea type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.provider.message\" id=\"newsflash.value.provider.message\" placeholder=\"\" required></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "         <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Topup min price</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"topup_settings.topup_price\" id=\"newsflash.value.provider.message\" placeholder=\"\" required/>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"form-group col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" data-ng-model=\"newsflash.value.provider.show\" id=\"newsflash.value.provider.show\" placeholder=\"\">Show Provider Newsflash</label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <div class='col-md-offset-10 col-md-2'>\n" +
    "          <button type=\"submit\" class=\"btn btn-info col-md-12\" ng-disabled=\"disableButton\">Save Settings</button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/settings2.html',
    "<div ng-controller=\"SettingsController\" ng-init=\"initSettings()\" class='panel'>\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    Newsflash Settings\n" +
    "  </header>\n" +
    "  <div class=\"panel-body\">\n" +
    "    <form class=\"form-horizontal\" name=\"form\" role=\"form\" data-ng-submit=\"saveSettings()\" novalidate>\n" +
    "      <div class='col-md-6'>\n" +
    "        <h4>Customers</h4>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Title</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.customer.title\" id=\"newsflash.value.customer.title\" placeholder=\"\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Message</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <textarea type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.customer.message\" id=\"newsflash.value.customer.message\" placeholder=\"\"></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" data-ng-model=\"newsflash.value.customer.show\" id=\"newsflash.value.customer.show\" placeholder=\"\">Show Customer Newsflash</label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "       >\n" +
    "       \n" +
    "      </div>\n" +
    "      <div class='col-md-6'>\n" +
    "        <h4>Providers</h4>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Title</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <input type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.provider.title\" id=\"newsflash.value.provider.title\" placeholder=\"\" required>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label class=\"col-md-12 control-label\">Message</label>\n" +
    "            <div class=\"col-md-12\">\n" +
    "              <textarea type=\"text\" class=\"form-control\" data-ng-model=\"newsflash.value.provider.message\" id=\"newsflash.value.provider.message\" placeholder=\"\" required></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        \n" +
    "\n" +
    "        <div class=\"form-group col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label><input type=\"checkbox\" data-ng-model=\"newsflash.value.provider.show\" id=\"newsflash.value.provider.show\" placeholder=\"\">Show Provider Newsflash</label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <div class='col-md-offset-10 col-md-2'>\n" +
    "          <button type=\"submit\" class=\"btn btn-info col-md-12\" ng-disabled=\"disableButton\">Save Settings</button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/sidebar.html',
    "<div class=\"page-sidebar\" data-ng-controller=\"HeaderController\">\n" +
    "\n" +
    "  <div class=\"sidebar-brand\">\n" +
    "    <a href=\"/\">\n" +
    "      <img src='/public/images/swenzy.png'>\n" +
    "    </a>\n" +
    "  </div>\n" +
    "\n" +
    "  <ul class=\"nav nav-pills nav-stacked\">\n" +
    "    <li data-ng-repeat=\"item in menus.main\" ui-route=\"/{{item.link}}\" ng-class=\"{active: $uiRoute}\">\n" +
    "        <a mean-token=\"item.link\" ui-sref='{{item.link}}'>{{item.title}}</a>\n" +
    "    </li>\n" +
    "    <li ui-route=\"faq\" ng-class=\"{active: $uiRoute}\">\n" +
    "        <a mean-token=\"item.link\" ui-sref='faq'>FAQ</a>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/success.html',
    "\n" +
    " <script>\n" +
    "(function() {\n" +
    "var _fbq = window._fbq || (window._fbq = []);\n" +
    "if (!_fbq.loaded) {\n" +
    "var fbds = document.createElement('script');\n" +
    "fbds.async = true;\n" +
    "fbds.src = '//connect.facebook.net/en_US/fbds.js';\n" +
    "var s = document.getElementsByTagName('script')[0];\n" +
    "s.parentNode.insertBefore(fbds, s);\n" +
    "_fbq.loaded = true;\n" +
    "}\n" +
    "})();\n" +
    "var item_price = window.location.href.split('success/')[1];\n" +
    "window._fbq = window._fbq || [];\n" +
    "window._fbq.push(['track', '6032131294940', {'value':item_price,'currency':'USD'}]);\n" +
    "window._fbq.push(['track', '6032141800940', {'value':item_price,'currency':'USD'}]);\n" +
    "window._fbq.push(['track', '6032141826740', {'value':item_price,'currency':'USD'}]);\n" +
    "window._fbq.push(['track', '6032141847140', {'value':item_price,'currency':'USD'}]);\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "  \n" +
    "   \tsetTimeout(function() {\n" +
    "     window.location.replace('/#!/orders');\n" +
    "     document.location.reload(true);\n" +
    "    }, 1000);\n" +
    " </script>\n" +
    "\n" +
    "<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6032141847140&amp;cd[value]=\"+item_price+\"&amp;cd[currency]=USD&amp;noscript=1\" /></noscript>\n" +
    "\n" +
    "\n" +
    "<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6032141826740&amp;cd[value]=\"+item_price+\"&amp;cd[currency]=USD&amp;noscript=1\" /><noscript>\n" +
    "\n" +
    "\n" +
    "<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6032141800940&amp;cd[value]=\"+item_price+\"&amp;cd[currency]=USD&amp;noscript=1\" /></noscript>\n" +
    "\n" +
    " <noscript><img height='1' width='1' alt='' style='display:none' src='https://www.facebook.com/tr?ev=6032131294940&amp;cd[value]='+item_price+'&amp;cd[currency]=USD&amp;noscript=1'/></noscript>"
  );


  $templateCache.put('public/system/views/topup.html',
    "<div class=\"modal-topup\">\n" +
    "  <form action='https://www.2checkout.com/checkout/purchase' method='post' class='checkout-form'>\n" +
    "    <h1>Top up your balance</h1>\n" +
    "    <div class='accept-cards'><img src=\"/public/images/paypal_accept_cards.png\"></div>\n" +
    "    <input type='hidden' name='sid' value='102379918' />\n" +
    "    <input type='hidden' name='mode' value='2CO' />\n" +
    "    <input type='hidden' name='li_0_product_id' ng-model=\"productId\" />\n" +
    "    <input type='hidden' name='li_0_type' value='product' />\n" +
    "    <input type='hidden' name='li_0_name' value='Rantic Credit' />\n" +
    "    <input type='hidden' name='li_0_merchant_order_id' ng-model=\"orderId\" />\n" +
    "    <label>Amount of credit to top up mmmmmmmmmmmmm(in US$)</label>\n" +
    "    <input type='number' name='li_0_price' class='form-control' ng-model=\"price\" required/>\n" +
    "    <label>Account holder name</label>\n" +
    "    <input type='text' name='card_holder_name' ng-model=\"cardName\" class='form-control' required/>\n" +
    "    <label>Country</label>\n" +
    "    <select id=\"country\" name=\"country\" ng-model=\"country\" class='form-control'>\n" +
    "      <option value=\"\">Choose Country</option>\n" +
    "        <option value=\"USA\" selected=\"\">United States</option>\n" +
    "        <option value=\"GBR\">United Kingdom</option>\n" +
    "        <option value=\"CAN\">Canada</option>\n" +
    "        <option value=\"AUS\">Australia</option>\n" +
    "        <option disabled=\"\">—</option>\n" +
    "        <option value=\"ALA\">Åland Islands</option>\n" +
    "        <option value=\"AFG\">Afghanistan</option>\n" +
    "        <option value=\"ALB\">Albania</option>\n" +
    "        <option value=\"DZA\">Algeria</option>\n" +
    "        <option value=\"ASM\">American Samoa</option>\n" +
    "        <option value=\"AND\">Andorra</option>\n" +
    "        <option value=\"AGO\">Angola</option>\n" +
    "        <option value=\"AIA\">Anguilla</option>\n" +
    "        <option value=\"ATA\">Antarctica</option>\n" +
    "        <option value=\"ATG\">Antigua and Barbuda</option>\n" +
    "        <option value=\"ARG\">Argentina</option>\n" +
    "        <option value=\"ARM\">Armenia</option>\n" +
    "        <option value=\"ABW\">Aruba</option>\n" +
    "        <option value=\"AUS\">Australia</option>\n" +
    "        <option value=\"AUT\">Austria</option>\n" +
    "        <option value=\"AZE\">Azerbaijan</option>\n" +
    "        <option value=\"BHS\">Bahamas</option>\n" +
    "        <option value=\"BHR\">Bahrain</option>\n" +
    "        <option value=\"BGD\">Bangladesh</option>\n" +
    "        <option value=\"BRB\">Barbados</option>\n" +
    "        <option value=\"BLR\">Belarus</option>\n" +
    "        <option value=\"BEL\">Belgium</option>\n" +
    "        <option value=\"BLZ\">Belize</option>\n" +
    "        <option value=\"BEN\">Benin</option>\n" +
    "        <option value=\"BMU\">Bermuda</option>\n" +
    "        <option value=\"BTN\">Bhutan</option>\n" +
    "        <option value=\"BOL\">Bolivia</option>\n" +
    "        <option value=\"BES\">Bonaire, Sint Eustatius and Saba</option>\n" +
    "        <option value=\"BIH\">Bosnia and Herzegovina</option>\n" +
    "        <option value=\"BWA\">Botswana</option>\n" +
    "        <option value=\"BVT\">Bouvet Island</option>\n" +
    "        <option value=\"BRA\">Brazil</option>\n" +
    "        <option value=\"IOT\">British Indian Ocean Territory</option>\n" +
    "        <option value=\"BRN\">Brunei Darussalam</option>\n" +
    "        <option value=\"BGR\">Bulgaria</option>\n" +
    "        <option value=\"BFA\">Burkina Faso</option>\n" +
    "        <option value=\"BDI\">Burundi</option>\n" +
    "        <option value=\"KHM\">Cambodia</option>\n" +
    "        <option value=\"CMR\">Cameroon</option>\n" +
    "        <option value=\"CAN\">Canada</option>\n" +
    "        <option value=\"CPV\">Cape Verde</option>\n" +
    "        <option value=\"CYM\">Cayman Islands</option>\n" +
    "        <option value=\"CAF\">Central African Republic</option>\n" +
    "        <option value=\"TCD\">Chad</option>\n" +
    "        <option value=\"CHL\">Chile</option>\n" +
    "        <option value=\"CHN\">China</option>\n" +
    "        <option value=\"CXR\">Christmas Island</option>\n" +
    "        <option value=\"CCK\">Cocos (Keeling) Islands</option>\n" +
    "        <option value=\"COL\">Colombia</option>\n" +
    "        <option value=\"COM\">Comoros</option>\n" +
    "        <option value=\"COG\">Congo</option>\n" +
    "        <option value=\"COD\">Congo, the Democratic Republic of the</option>\n" +
    "        <option value=\"COK\">Cook Islands</option>\n" +
    "        <option value=\"CRI\">Costa Rica</option>\n" +
    "        <option value=\"CIV\">Cote D'ivoire</option>\n" +
    "        <option value=\"HRV\">Croatia (Hrvatska)</option>\n" +
    "        <option value=\"CYP\">Cyprus</option>\n" +
    "        <option value=\"CZE\">Czech Republic</option>\n" +
    "        <option value=\"DNK\">Denmark</option>\n" +
    "        <option value=\"DJI\">Djibouti</option>\n" +
    "        <option value=\"DMA\">Dominica</option>\n" +
    "        <option value=\"DOM\">Dominican Republic</option>\n" +
    "        <option value=\"ECU\">Ecuador</option>\n" +
    "        <option value=\"EGY\">Egypt</option>\n" +
    "        <option value=\"SLV\">El Salvador</option>\n" +
    "        <option value=\"GNQ\">Equatorial Guinea</option>\n" +
    "        <option value=\"ERI\">Eritrea</option>\n" +
    "        <option value=\"EST\">Estonia</option>\n" +
    "        <option value=\"ETH\">Ethiopia</option>\n" +
    "        <option value=\"FLK\">Falkland Islands (Malvinas)</option>\n" +
    "        <option value=\"FRO\">Faroe Islands</option>\n" +
    "        <option value=\"FJI\">Fiji</option>\n" +
    "        <option value=\"FIN\">Finland</option>\n" +
    "        <option value=\"FRA\">France</option>\n" +
    "        <option value=\"GUF\">French Guiana</option>\n" +
    "        <option value=\"PYF\">French Polynesia</option>\n" +
    "        <option value=\"ATF\">French Southern Territories</option>\n" +
    "        <option value=\"GAB\">Gabon</option>\n" +
    "        <option value=\"GMB\">Gambia</option>\n" +
    "        <option value=\"GEO\">Georgia</option>\n" +
    "        <option value=\"DEU\">Germany</option>\n" +
    "        <option value=\"GHA\">Ghana</option>\n" +
    "        <option value=\"GIB\">Gibraltar</option>\n" +
    "        <option value=\"GRC\">Greece</option>\n" +
    "        <option value=\"GRL\">Greenland</option>\n" +
    "        <option value=\"GRD\">Grenada</option>\n" +
    "        <option value=\"GLP\">Guadeloupe</option>\n" +
    "        <option value=\"GUM\">Guam</option>\n" +
    "        <option value=\"GTM\">Guatemala</option>\n" +
    "        <option value=\"GGY\">Guernsey</option>\n" +
    "        <option value=\"GIN\">Guinea</option>\n" +
    "        <option value=\"GNB\">Guinea-Bissau</option>\n" +
    "        <option value=\"GUY\">Guyana</option>\n" +
    "        <option value=\"HTI\">Haiti</option>\n" +
    "        <option value=\"HMD\">Heard Island and Mcdonald Islands</option>\n" +
    "        <option value=\"HND\">Honduras</option>\n" +
    "        <option value=\"HKG\">Hong Kong</option>\n" +
    "        <option value=\"HUN\">Hungary</option>\n" +
    "        <option value=\"ISL\">Iceland</option>\n" +
    "        <option value=\"IND\">India</option>\n" +
    "        <option value=\"IDN\">Indonesia</option>\n" +
    "        <option value=\"IRQ\">Iraq</option>\n" +
    "        <option value=\"IRL\">Ireland</option>\n" +
    "        <option value=\"IMN\">Isle of Man</option>\n" +
    "        <option value=\"ISR\">Israel</option>\n" +
    "        <option value=\"ITA\">Italy</option>\n" +
    "        <option value=\"JAM\">Jamaica</option>\n" +
    "        <option value=\"JPN\">Japan</option>\n" +
    "        <option value=\"JEY\">Jersey</option>\n" +
    "        <option value=\"JOR\">Jordan</option>\n" +
    "        <option value=\"KAZ\">Kazakhstan</option>\n" +
    "        <option value=\"KEN\">Kenya</option>\n" +
    "        <option value=\"KIR\">Kiribati</option>\n" +
    "        <option value=\"KOR\">Korea, Republic of</option>\n" +
    "        <option value=\"KWT\">Kuwait</option>\n" +
    "        <option value=\"KGZ\">Kyrgyzstan</option>\n" +
    "        <option value=\"LAO\">Lao People's Democratic Republic</option>\n" +
    "        <option value=\"LVA\">Latvia</option>\n" +
    "        <option value=\"LBN\">Lebanon</option>\n" +
    "        <option value=\"LSO\">Lesotho</option>\n" +
    "        <option value=\"LBR\">Liberia</option>\n" +
    "        <option value=\"LBY\">Libyan Arab Jamahiriya</option>\n" +
    "        <option value=\"LIE\">Liechtenstein</option>\n" +
    "        <option value=\"LTU\">Lithuania</option>\n" +
    "        <option value=\"LUX\">Luxembourg</option>\n" +
    "        <option value=\"MAC\">Macao</option>\n" +
    "        <option value=\"MKD\">Macedonia</option>\n" +
    "        <option value=\"MDG\">Madagascar</option>\n" +
    "        <option value=\"MWI\">Malawi</option>\n" +
    "        <option value=\"MYS\">Malaysia</option>\n" +
    "        <option value=\"MDV\">Maldives</option>\n" +
    "        <option value=\"MLI\">Mali</option>\n" +
    "        <option value=\"MLT\">Malta</option>\n" +
    "        <option value=\"MHL\">Marshall Islands</option>\n" +
    "        <option value=\"MTQ\">Martinique</option>\n" +
    "        <option value=\"MRT\">Mauritania</option>\n" +
    "        <option value=\"MUS\">Mauritius</option>\n" +
    "        <option value=\"MYT\">Mayotte</option>\n" +
    "        <option value=\"MEX\">Mexico</option>\n" +
    "        <option value=\"FSM\">Micronesia, Federated States of</option>\n" +
    "        <option value=\"MDA\">Moldova, Republic of</option>\n" +
    "        <option value=\"MCO\">Monaco</option>\n" +
    "        <option value=\"MNG\">Mongolia</option>\n" +
    "        <option value=\"MNE\">Montenegro</option>\n" +
    "        <option value=\"MSR\">Montserrat</option>\n" +
    "        <option value=\"MAR\">Morocco</option>\n" +
    "        <option value=\"MOZ\">Mozambique</option>\n" +
    "        <option value=\"MMR\">Myanmar</option>\n" +
    "        <option value=\"NAM\">Namibia</option>\n" +
    "        <option value=\"NRU\">Nauru</option>\n" +
    "        <option value=\"NPL\">Nepal</option>\n" +
    "        <option value=\"NLD\">Netherlands</option>\n" +
    "        <option value=\"ANT\">Netherlands Antilles</option>\n" +
    "        <option value=\"NCL\">New Caledonia</option>\n" +
    "        <option value=\"NZL\">New Zealand</option>\n" +
    "        <option value=\"NIC\">Nicaragua</option>\n" +
    "        <option value=\"NER\">Niger</option>\n" +
    "        <option value=\"NGA\">Nigeria</option>\n" +
    "        <option value=\"NIU\">Niue</option>\n" +
    "        <option value=\"NFK\">Norfolk Island</option>\n" +
    "        <option value=\"MNP\">Northern Mariana Islands</option>\n" +
    "        <option value=\"NOR\">Norway</option>\n" +
    "        <option value=\"OMN\">Oman</option>\n" +
    "        <option value=\"PAK\">Pakistan</option>\n" +
    "        <option value=\"PLW\">Palau</option>\n" +
    "        <option value=\"PSE\">Palestinian Territory, Occupied</option>\n" +
    "        <option value=\"PAN\">Panama</option>\n" +
    "        <option value=\"PNG\">Papua New Guinea</option>\n" +
    "        <option value=\"PRY\">Paraguay</option>\n" +
    "        <option value=\"PER\">Peru</option>\n" +
    "        <option value=\"PHL\">Philippines</option>\n" +
    "        <option value=\"PCN\">Pitcairn</option>\n" +
    "        <option value=\"POL\">Poland</option>\n" +
    "        <option value=\"PRT\">Portugal</option>\n" +
    "        <option value=\"PRI\">Puerto Rico</option>\n" +
    "        <option value=\"QAT\">Qatar</option>\n" +
    "        <option value=\"REU\">Reunion</option>\n" +
    "        <option value=\"ROU\">Romania</option>\n" +
    "        <option value=\"RUS\">Russian Federation</option>\n" +
    "        <option value=\"RWA\">Rwanda</option>\n" +
    "        <option value=\"SHN\">Saint Helena</option>\n" +
    "        <option value=\"KNA\">Saint Kitts and Nevis</option>\n" +
    "        <option value=\"LCA\">Saint Lucia</option>\n" +
    "        <option value=\"SPM\">Saint Pierre and Miquelon</option>\n" +
    "        <option value=\"VCT\">Saint Vincent and the Grenadines</option>\n" +
    "        <option value=\"WSM\">Samoa</option>\n" +
    "        <option value=\"SMR\">San Marino</option>\n" +
    "        <option value=\"STP\">Sao Tome and Principe</option>\n" +
    "        <option value=\"SAU\">Saudi Arabia</option>\n" +
    "        <option value=\"SEN\">Senegal</option>\n" +
    "        <option value=\"SRB\">Serbia</option>\n" +
    "        <option value=\"SCG\">Serbia and Montenegro</option>\n" +
    "        <option value=\"SYC\">Seychelles</option>\n" +
    "        <option value=\"SLE\">Sierra Leone</option>\n" +
    "        <option value=\"SGP\">Singapore</option>\n" +
    "        <option value=\"SVK\">Slovakia</option>\n" +
    "        <option value=\"SVN\">Slovenia</option>\n" +
    "        <option value=\"SLB\">Solomon Islands</option>\n" +
    "        <option value=\"SOM\">Somalia</option>\n" +
    "        <option value=\"ZAF\">South Africa</option>\n" +
    "        <option value=\"SGS\">South Georgia and the South Sandwich Islands</option>\n" +
    "        <option value=\"ESP\">Spain</option>\n" +
    "        <option value=\"LKA\">Sri Lanka</option>\n" +
    "        <option value=\"SUR\">Suriname</option>\n" +
    "        <option value=\"SJM\">Svalbard and Jan Mayen Islands</option>\n" +
    "        <option value=\"SWZ\">Swaziland</option>\n" +
    "        <option value=\"SWE\">Sweden</option>\n" +
    "        <option value=\"CHE\">Switzerland</option>\n" +
    "        <option value=\"TWN\">Taiwan</option>\n" +
    "        <option value=\"TJK\">Tajikistan</option>\n" +
    "        <option value=\"TZA\">Tanzania, United Republic of</option>\n" +
    "        <option value=\"THA\">Thailand</option>\n" +
    "        <option value=\"TLS\">Timor-Leste</option>\n" +
    "        <option value=\"TGO\">Togo</option>\n" +
    "        <option value=\"TKL\">Tokelau</option>\n" +
    "        <option value=\"TON\">Tonga</option>\n" +
    "        <option value=\"TTO\">Trinidad and Tobago</option>\n" +
    "        <option value=\"TUN\">Tunisia</option>\n" +
    "        <option value=\"TUR\">Turkey</option>\n" +
    "        <option value=\"TKM\">Turkmenistan</option>\n" +
    "        <option value=\"TCA\">Turks and Caicos Islands</option>\n" +
    "        <option value=\"TUV\">Tuvalu</option>\n" +
    "        <option value=\"UGA\">Uganda</option>\n" +
    "        <option value=\"UKR\">Ukraine</option>\n" +
    "        <option value=\"ARE\">United Arab Emirates</option>\n" +
    "        <option value=\"GBR\">United Kingdom</option>\n" +
    "        <option value=\"USA\" selected=\"\">United States</option>\n" +
    "        <option value=\"UMI\">United States Minor Outlying Islands</option>\n" +
    "        <option value=\"URY\">Uruguay</option>\n" +
    "        <option value=\"UZB\">Uzbekistan</option>\n" +
    "        <option value=\"VUT\">Vanuatu</option>\n" +
    "        <option value=\"VAT\">Vatican City State (Holy See)</option>\n" +
    "        <option value=\"VEN\">Venezuela</option>\n" +
    "        <option value=\"VNM\">Viet Nam</option>\n" +
    "        <option value=\"VGB\">Virgin Islands, British</option>\n" +
    "        <option value=\"VIR\">Virgin Islands, U.S.</option>\n" +
    "        <option value=\"WLF\">Wallis and Futuna Islands</option>\n" +
    "        <option value=\"ESH\">Western Sahara</option>\n" +
    "        <option value=\"YEM\">Yemen</option>\n" +
    "        <option value=\"YUG\">Yugoslavia</option>\n" +
    "        <option value=\"ZAR\">Zaire</option>\n" +
    "        <option value=\"ZMB\">Zambia</option>\n" +
    "        <option value=\"ZWE\">Zimbabwe</option>\n" +
    "    </select>\n" +
    "    <label>Address</label>\n" +
    "    <input type='text' name='street_address' placeholder=\"Enter billing address\" class='form-control' ng-model=\"address\" required/>\n" +
    "    <input type='text' name='street_address2' placeholder=\"Enter billing address 2\" class='form-control' ng-model=\"address2\" required/>\n" +
    "    <label>City</label>\n" +
    "    <input type='text' name='city' placeholder=\"Enter billing city\" class='form-control' ng-model=\"city\" required/>\n" +
    "    <label>State</label>\n" +
    "    <select name=\"state\" id=\"state\" class='form-control' ng-model=\"state\" ng-show=\"country == 'USA'\">\n" +
    "      <option value=\"\">Select one</option>\n" +
    "      <option value=\"AL\">Alabama</option>\n" +
    "      <option value=\"AK\">Alaska</option>\n" +
    "      <option value=\"AS\">American Samoa</option>\n" +
    "      <option value=\"AZ\">Arizona</option>\n" +
    "      <option value=\"AR\">Arkansas</option>\n" +
    "      <option value=\"AA\">Armed Forces Americas</option>\n" +
    "      <option value=\"AE\">Armed Forces Europe, Middle East, &amp; Canada</option>\n" +
    "      <option value=\"AP\">Armed Forces Pacific</option>\n" +
    "      <option value=\"CA\">California</option>\n" +
    "      <option value=\"CO\">Colorado</option>\n" +
    "      <option value=\"CT\">Connecticut</option>\n" +
    "      <option value=\"DE\">Delaware</option>\n" +
    "      <option value=\"DC\">District of Columbia</option>\n" +
    "      <option value=\"FM\">Federated States of Micronesia</option>\n" +
    "      <option value=\"FL\">Florida</option>\n" +
    "      <option value=\"GA\">Georgia</option>\n" +
    "      <option value=\"GU\">Guam</option>\n" +
    "      <option value=\"HI\">Hawaii</option>\n" +
    "      <option value=\"ID\">Idaho</option>\n" +
    "      <option value=\"IL\">Illinois</option>\n" +
    "      <option value=\"IN\">Indiana</option>\n" +
    "      <option value=\"IA\">Iowa</option>\n" +
    "      <option value=\"KS\">Kansas</option>\n" +
    "      <option value=\"KY\">Kentucky</option>\n" +
    "      <option value=\"LA\">Louisiana</option>\n" +
    "      <option value=\"ME\">Maine</option>\n" +
    "      <option value=\"MH\">Marshall Islands</option>\n" +
    "      <option value=\"MD\">Maryland</option>\n" +
    "      <option value=\"MA\">Massachusetts</option>\n" +
    "      <option value=\"MI\">Michigan</option>\n" +
    "      <option value=\"MN\">Minnesota</option>\n" +
    "      <option value=\"MS\">Mississippi</option>\n" +
    "      <option value=\"MO\">Missouri</option>\n" +
    "      <option value=\"MT\">Montana</option>\n" +
    "      <option value=\"NE\">Nebraska</option>\n" +
    "      <option value=\"NV\">Nevada</option>\n" +
    "      <option value=\"NH\">New Hampshire</option>\n" +
    "      <option value=\"NJ\">New Jersey</option>\n" +
    "      <option value=\"NM\">New Mexico</option>\n" +
    "      <option value=\"NY\">New York</option>\n" +
    "      <option value=\"NC\">North Carolina</option>\n" +
    "      <option value=\"ND\">North Dakota</option>\n" +
    "      <option value=\"MP\">Northern Mariana Islands</option>\n" +
    "      <option value=\"OH\">Ohio</option>\n" +
    "      <option value=\"OK\">Oklahoma</option>\n" +
    "      <option value=\"OR\">Oregon</option>\n" +
    "      <option value=\"PW\">Palau</option>\n" +
    "      <option value=\"PA\">Pennsylvania</option>\n" +
    "      <option value=\"PR\">Puerto Rico</option>\n" +
    "      <option value=\"RI\">Rhode Island</option>\n" +
    "      <option value=\"SC\">South Carolina</option>\n" +
    "      <option value=\"SD\">South Dakota</option>\n" +
    "      <option value=\"TN\">Tennessee</option>\n" +
    "      <option value=\"TX\">Texas</option>\n" +
    "      <option value=\"UT\">Utah</option>\n" +
    "      <option value=\"VT\">Vermont</option>\n" +
    "      <option value=\"VI\">Virgin Islands</option>\n" +
    "      <option value=\"VA\">Virginia</option>\n" +
    "      <option value=\"WA\">Washington</option>\n" +
    "      <option value=\"WV\">West Virginia</option>\n" +
    "      <option value=\"WI\">Wisconsin</option>\n" +
    "      <option value=\"WY\">Wyoming</option>\n" +
    "    </select>\n" +
    "    <label>ZIP code</label>\n" +
    "    <input type='text' name='zip' placeholder=\"123456\" class='form-control' ng-model=\"zip\" required/>\n" +
    "    <label>Email</label>\n" +
    "    <input type='text' name='email' ng-model=\"cardEmail\" placeholder='Enter billing email' class='form-control' required/>\n" +
    "    <label>Phone</label>\n" +
    "    <input type='phone' name='phone' placeholder='614-921-2450' class='form-control' ng-model=\"phone\" required/>\n" +
    "    <button class='btn btn-block btn-success' ng-click='submit($event)'>Purchase Balance</button>\n" +
    "  </form>\n" +
    "  <!-- <tabset>\n" +
    "    <tab heading=\"Information\">Static content</tab>\n" +
    "    <tab heading=\"Person\">Static content</tab>\n" +
    "    <tab select=\"alertMe()\">\n" +
    "      <tab-heading>\n" +
    "        <i class=\"glyphicon glyphicon-bell\"></i> Alert!\n" +
    "      </tab-heading>\n" +
    "      I've got an HTML heading, and a select callback. Pretty cool!\n" +
    "    </tab>\n" +
    "  </tabset>-->\n" +
    "  <script src=\"https://www.2checkout.com/static/checkout/javascript/direct.min.js\"></script>\n" +
    "</div>\n"
  );


  $templateCache.put('public/system/views/user_actions.html',
    "<section data-ng-controller=\"ActionsController\" data-ng-init=\"findUserActions()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t  User Actions \n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"table table-striped table-advance table-hover payments\">\n" +
    "\t\t    <tr data-ng-repeat=\"log in $data\" ng-if=\"log.data.order.url != 'add_cart'\">\n" +
    "\t        <td data-title=\"'Date'\" sortable=\"'create_time'\"> {{log.data.created | date:'yyyy-MM-dd HH:mm:ss Z' }}</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\n" +
    "\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t<td data-title=\"'Email'\" sortable=\"'username'\" >\n" +
    "\t\t\t{{log.data.user.username  | cut:true:75:' ...' }}</td>\n" +
    "\n" +
    "\t\t\t\n" +
    "\t\t\t<td  data-title=\"'Action'\" sortable=\"'order'\">\n" +
    "\t\t\n" +
    "\t\t<span ng-if=\"log.message == 'order created' || log.message == 'payment created' || log.message == 'Topup created'  \" style=\"color: #F39B13;\" >\n" +
    "\t\t\t{{log.message}}\n" +
    "\t\t</span>\n" +
    "\n" +
    "\t\t<span ng-if=\"log.message == 'order inprogress' || log.message == 'payment successful' || log.message == 'order completed' || log.message == 'TopUp'\" style=\"color: #27AE61;\" >\n" +
    "\t\t{{log.message}}\n" +
    "\t\t</span>\n" +
    "\n" +
    "\t\t<span ng-if=\"log.data.action_type == 'Order cancelled' || log.message == 'order abandoned'\" >\n" +
    "\t\t\t<a style=\"color: #a33;\" data-ng-href=\"#!/userActions/{{log.data.user._id}}\">{{log.message}}</a>\n" +
    "\t\t</span>\n" +
    "\n" +
    "\t\t\n" +
    "\n" +
    "\t</td>\n" +
    "\t<td ng-if='log.data.order.url' data-title=\"'Order'\" sortable=\"'order'\"><a data-ng-href=\"#!/orders/{{log.order_id}}\">{{log.data.order.url | cut:true:75:' ...'}}</a></td>\n" +
    "\t\n" +
    "\t<td  data-title=\"'Price'\" sortable=\"'price'\">{{log.data.price}} </td>\n" +
    "\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t\t\t\t\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/auth/views/index.html',
    "<div class='panel'>\n" +
    "    <div ui-view></div>\n" +
    "</div>\n"
  );

}]);
