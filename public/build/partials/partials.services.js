angular.module('mean.services').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/services/views/create.html',
    "<section class='panel' data-ng-controller=\"ServicesController\">\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    Edit Service\n" +
    "  </header>\n" +
    "  <div class=\"panel-body\">\n" +
    "    <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"create()\" novalidate>\n" +
    "      <div class='col-md-12'>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Long Name</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.label\" id=\"label\" placeholder=\"Human readable service name\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "         <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Description</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.description\" id=\"label\" placeholder=\"Description - not required\">\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Codename</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.value\" id=\"value\" placeholder=\"Machine readable service name\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Pattern</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.pattern\" id=\"pattern\" placeholder=\"Valid regex pattern to check this service URL against\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Category</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.category\" id=\"category\" placeholder=\"Category of the service\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label>\n" +
    "                <input type=\"checkbox\" data-ng-model=\"service.recurring\" id=\"recurring\" placeholder=\"\">Recurring Payment</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <span ng-if=\"!service.recurring\">\n" +
    "          <div class=\"form-group row\">\n" +
    "            <label class=\"col-md-2 control-label\">Multiplier</label>\n" +
    "            <div class=\"col-md-10\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"service.multiplier\" id=\"multiplier\" required>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"form-group row\">\n" +
    "            <label class=\"col-md-2 control-label\">Minimum Quantity</label>\n" +
    "            <div class=\"col-md-10\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"service.minimum\" id=\"minimum\" required>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"form-group row\">\n" +
    "            <label class=\"col-md-2 control-label\">Price</label>\n" +
    "            <div class=\"col-md-10\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"service.price\" id=\"price\" required>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </span>\n" +
    "        <span ng-if=\"service.recurring\">\n" +
    "          <div ng-if=\"!service.packages.length\" class=\"col-md-12\" style=\"margin-bottom: 1em;\">No packages added yet</div>\n" +
    "          <div class=\"form-group row\" ng-repeat=\"(key, package) in service.packages\" style=\"margin-bottom: 10px;\">\n" +
    "            <div style=\"padding: 10px; margin: 0px 1em; background: #fff; border: 1px solid #eee;\" class=\"clearfix\">\n" +
    "              <div class=\"row\">\n" +
    "                <label class=\"col-md-2 control-label\">Label</label>\n" +
    "                <div class=\"col-md-10\">\n" +
    "                  <input type=\"text\" class=\"form-control\" data-ng-model=\"package.label\" id=\"label\" required>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "              <div class=\"row\">\n" +
    "                <label class=\"col-md-2 control-label\">Price</label>\n" +
    "                <div class=\"col-md-10\">\n" +
    "                  <input type=\"number\" class=\"form-control\" data-ng-model=\"package.price\" id=\"price\" required>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "              <div class=\"row\">\n" +
    "                <label class=\"col-md-2 control-label\">Quantity</label>\n" +
    "                <div class=\"col-md-10\">\n" +
    "                  <input type=\"number\" class=\"form-control\" data-ng-model=\"package.quantity\" id=\"quantity\" required>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "              <div class=\"row\">\n" +
    "                <div class=\"col-md-offset-10 col-md-2\">\n" +
    "                  <a class=\"btn btn-danger btn-block\" ng-click=\"removePackage(key)\">Remove</a>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <a class=\"btn btn-default\" ng-click=\"addPackage()\">Add Package</a>\n" +
    "        </span>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label>\n" +
    "                <input type=\"checkbox\" data-ng-model=\"service.disabled\" id=\"disabled\" placeholder=\"\">Hide service</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label>\n" +
    "                <input type=\"checkbox\" data-ng-model=\"service.targeted\" id=\"targeted\" placeholder=\"\">Country targeted service</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-offset-9 col-md-3\">\n" +
    "            <button type=\"submit\" class=\"btn btn-lg btn-info col-md-12\" ng-disabled=\"disableButton\">Save Service</button>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/services/views/list.html',
    "<section data-ng-controller=\"ServicesController\" data-ng-init=\"find()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Services\n" +
    "\t\t\t<button class='btn btn-action pull-right' data-ng-click=\"goCreateService()\">Create Service</button>\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" show-filter=\"true\" class=\"ng-table-responsive table table-striped table-advance table-hover services\">\n" +
    "\t\t    <tr data-ng-repeat=\"order in $data\">\n" +
    "\t        <td data-title=\"'Long Name'\" sortable=\"'label'\" filter=\"{ 'label': 'text' }\"><a data-ng-href=\"#!/services/{{order._id}}\">{{order.label | cut:true:55:' ...'}}</a></td>\n" +
    "\t\t\t\t\t<td data-title=\"'Codename'\" sortable=\"'value'\" filter=\"{ 'value': 'text' }\"><a data-ng-href=\"#!/services/{{order._id}}\">{{order.value | cut:true:55:' ...'}}</a></td>\n" +
    "\t\t\t\t\t<td data-title=\"'Category'\" sortable=\"'category'\" filter=\"{ 'category': 'text' }\"><a data-ng-href=\"#!/services/{{order._id}}\">{{order.category | cut:true:55:' ...'}}</a></td>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );


  $templateCache.put('public/services/views/view.html',
    "<section class='panel' data-ng-controller=\"ServicesController\" ng-init=\"findOne()\">\n" +
    "  <header class=\"panel-heading higher-panel-heading\">\n" +
    "    Edit Service\n" +
    "  </header>\n" +
    "  <div class=\"panel-body\">\n" +
    "    <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"update()\" novalidate>\n" +
    "      <div class='col-md-12'>\n" +
    "         <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">API Code</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.api_code\" id=\"label\" placeholder=\"API service id for order.\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Long Name</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.label\" id=\"label\" placeholder=\"Human readable service name\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Description</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.description\" id=\"label\" placeholder=\"Description - not required\">\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Codename</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.value\" id=\"value\" placeholder=\"Machine readable service name\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Pattern</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.pattern\" id=\"pattern\" placeholder=\"Valid regex pattern to check this service URL against\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <label class=\"col-md-2 control-label\">Category</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <input type=\"text\" class=\"form-control\" data-ng-model=\"service.category\" id=\"category\" placeholder=\"Category of the service\" required>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label>\n" +
    "                <input type=\"checkbox\" data-ng-model=\"service.recurring\" id=\"recurring\" placeholder=\"\">Recurring Payment</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <span ng-if=\"!service.recurring\">\n" +
    "          <div class=\"form-group row\">\n" +
    "            <label class=\"col-md-2 control-label\">Multiplier</label>\n" +
    "            <div class=\"col-md-10\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"service.multiplier\" id=\"multiplier\" required>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"form-group row\">\n" +
    "            <label class=\"col-md-2 control-label\">Minimum Quantity</label>\n" +
    "            <div class=\"col-md-10\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"service.minimum\" id=\"minimum\" required>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"form-group row\">\n" +
    "            <label class=\"col-md-2 control-label\">Price</label>\n" +
    "            <div class=\"col-md-10\">\n" +
    "              <input type=\"number\" class=\"form-control\" data-ng-model=\"service.price\" id=\"price\" required>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </span>\n" +
    "        <span ng-if=\"service.recurring\">\n" +
    "          <div ng-if=\"!service.packages.length\" class=\"col-md-12\" style=\"margin-bottom: 1em;\">No packages added yet</div>\n" +
    "          <div class=\"form-group row\" ng-repeat=\"(key, package) in service.packages\" style=\"margin-bottom: 10px;\">\n" +
    "            <div style=\"padding: 10px; margin: 0px 1em; background: #fff; border: 1px solid #eee;\" class=\"clearfix\">\n" +
    "              <div class=\"row\">\n" +
    "                <label class=\"col-md-2 control-label\">Label</label>\n" +
    "                <div class=\"col-md-10\">\n" +
    "                  <input type=\"text\" class=\"form-control\" data-ng-model=\"package.label\" id=\"label\" required>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "              <div class=\"row\">\n" +
    "                <label class=\"col-md-2 control-label\">Price</label>\n" +
    "                <div class=\"col-md-10\">\n" +
    "                  <input type=\"number\" class=\"form-control\" data-ng-model=\"package.price\" id=\"price\" required>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "              <div class=\"row\">\n" +
    "                <label class=\"col-md-2 control-label\">Quantity</label>\n" +
    "                <div class=\"col-md-10\">\n" +
    "                  <input type=\"number\" class=\"form-control\" data-ng-model=\"package.quantity\" id=\"quantity\" required>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "              <div class=\"row\">\n" +
    "                <div class=\"col-md-offset-10 col-md-2\">\n" +
    "                  <a class=\"btn btn-danger btn-block\" ng-click=\"removePackage(key)\">Remove</a>\n" +
    "                </div>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <a class=\"btn btn-default\" ng-click=\"addPackage()\">Add Package</a>\n" +
    "        </span>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label>\n" +
    "                <input type=\"checkbox\" data-ng-model=\"service.disabled\" id=\"disabled\" placeholder=\"\">Hide service</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-12\">\n" +
    "            <div class=\"checkbox\">\n" +
    "              <label>\n" +
    "                <input type=\"checkbox\" data-ng-model=\"service.targeted\" id=\"targeted\" placeholder=\"\">Country targeted service</label>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\" ng-show=\"service.targeted\">\n" +
    "          <label mean-token=\"'limit-countries'\" class=\"col-md-2 control-label\">Limit to Countries</label>\n" +
    "          <div class=\"col-md-10\">\n" +
    "            <ui-select multiple ng-model=\"service.countries\" theme=\"select2\" style=\"width: 100%;\">\n" +
    "              <ui-select-match placeholder=\"Select country...\">{{$item.text}}</ui-select-match>\n" +
    "              <ui-select-choices repeat=\"country.id as country in countries | propsFilter: {id: $select.search, text: $select.search}\">\n" +
    "                <div ng-bind-html=\"country.text | highlight: $select.search\"></div>\n" +
    "              </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group row\">\n" +
    "          <div class=\"col-md-offset-9 col-md-3\">\n" +
    "            <button type=\"submit\" class=\"btn btn-lg btn-info col-md-12\" ng-disabled=\"disableButton\">Save Service</button>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </div>\n" +
    "</section>\n"
  );

}]);
