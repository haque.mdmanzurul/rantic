angular.module('mean.users').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('public/users/views/edit.html',
    "<section class='panel' data-ng-controller=\"UsersController\" ng-init=\"findOne()\">\n" +
    "    <header class=\"panel-heading higher-panel-heading\">\n" +
    "        Edit User\n" +
    "    </header>\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form class=\"form-horizontal row\" name=\"form\" role=\"form\" data-ng-submit=\"update()\" novalidate>\n" +
    "            <div class='col-md-12'>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-name'\" class=\"col-md-2 control-label\">Full Name</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"user.name\" id=\"name\" placeholder=\"John Smith\" value=\"{{user.name}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-username'\" class=\"col-md-2 control-label\">Username</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"user.username\" id=\"username\" placeholder=\"Enter the Email here\" value=\"{{user.username}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-email'\" class=\"col-md-2 control-label\">Email</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"user.email\" id=\"email\" placeholder=\"Enter the Email here\" value=\"{{user.email}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-balance'\" class=\"col-md-2 control-label\">Balance ($USD)</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"user.balance\" id=\"balance\" placeholder=\"Enter the Email here\" value=\"{{user.balance | numeral}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-profit'\" class=\"col-md-2 control-label\">Default Profit (0 - 1)</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"user.profit\" id=\"profit\" placeholder=\"Enter the Email here\" value=\"{{user.profit}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-discount'\" class=\"col-md-2 control-label\">Discount (0 - 1)</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-ng-model=\"user.discount\" id=\"discount\" placeholder=\"Enter the Email here\" value=\"{{user.discount}}\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-roles'\" class=\"col-md-2 control-label\">Roles</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input\n" +
    "                            type=\"hidden\"\n" +
    "                            ui-select2=\"select2OptionsRoles\"\n" +
    "                            ng-model=\"user.roles\"\n" +
    "                            >\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-services'\" class=\"col-md-2 control-label\">Services</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <input\n" +
    "                            type=\"hidden\"\n" +
    "                            ui-select2=\"select2OptionsServices\"\n" +
    "                            ng-model=\"user.services\"\n" +
    "                            >\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label mean-token=\"'create-services'\" class=\"col-md-2 control-label\">Profit Margins</label>\n" +
    "                    <div class=\"col-md-10\">\n" +
    "                        <div data-ng-repeat=\"service in user.services\">\n" +
    "                            <label>{{getServiceText(service)}}</label>\n" +
    "                            <input type=\"text\" class='form-control col-md-12' value=\"{{user.profits[service]}}\" data-ng-model=\"user.profits[service]\" placeholder=\"{{user.profit}}\" />\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-offset-10 col-md-2\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-info col-md-12\" ng-disabled=\"disableButton\">Save User</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</section>\n"
  );


  $templateCache.put('public/users/views/list.html',
    "<section data-ng-controller=\"UsersController\" data-ng-init=\"find()\" class=\"panel\">\n" +
    "\t<header class=\"panel-heading higher-panel-heading\">\n" +
    "\t    Users\n" +
    "\t</header>\n" +
    "\t<span>\n" +
    "\t\t<table ng-table=\"tableParams\" class=\"table table-striped table-advance table-hover users\" show-filter=\"true\">\n" +
    "\t\t    <tr data-ng-repeat=\"user in $data\">\n" +
    "\t\t    \t<td data-title=\"'Name'\" sortable=\"'name'\" filter=\"{ 'name': 'text' }\"><a data-ng-href=\"#!/users/{{user._id}}\">{{user.name | cut:true:75:' ...'}}</a></td>\n" +
    "\t\t        <td data-title=\"'Email'\" sortable=\"'email'\" filter=\"{ 'email': 'text' }\"><a data-ng-href=\"#!/users/{{user._id}}\">{{user.email | cut:true:75:' ...'}}</a></td>\n" +
    "\t\t        <td data-title=\"'Balance'\" sortable=\"'balance'\"><a data-ng-href=\"#!/users/{{user._id}}\">{{user.balance | numeral:'$0,0.00'}}</a></td>\n" +
    "\t\t        <td data-title=\"'Role'\"><a data-ng-href=\"#!/users/{{user._id}}\">\n" +
    "\t\t        \t<span ng-show=\"hasRole('admin', user)\">Administrator</span>\n" +
    "\t\t        \t<span ng-show=\"hasRole('customer', user)\">Customer</span>\n" +
    "\t\t        \t<span ng-show=\"hasRole('provider', user)\">Provider</span>\n" +
    "\t\t        </a></td>\n" +
    "\t\t      </a>\n" +
    "\t\t    </tr>\n" +
    "\t\t</table>\n" +
    "\t</span>\n" +
    "</section>\n"
  );

}]);
