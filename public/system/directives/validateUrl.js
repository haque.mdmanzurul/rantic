'use strict';

angular.module('mean').directive('validateUrl', function($http) {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            function validate(value) {
              if(!ngModel.$viewValue) return false;
              ngModel.$setValidity('url', (_.isNull(ngModel.$viewValue.match(re_weburl)) ? false : true));
            }

            scope.$watch( function() {
              return ngModel.$viewValue;
            }, validate);
        }
      };
});