
'use strict';

//Setting up route
angular.module('mean.system').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      // For unmatched routes:
      $urlRouterProvider.otherwise('/');

      // states for my app
      $stateProvider
        .state('home', {
          url: '/',
          templateUrl: 'public/system/views/index.html'
        })
       .state('auth', {
          templateUrl: 'public/auth/views/index.html'
        })
        .state('success', {
          url: '/success/:price',
          templateUrl: 'public/system/views/success.html'
        })
        .state('settings', {
          url: '/settings',
          templateUrl: 'public/system/views/settings.html'
        })
        .state('comments', {
          url: '/comments',
          templateUrl: 'public/system/views/comments.html'
        })
        
        .state('faq', {
          url: '/faq',
          templateUrl: 'public/system/views/faq.html'
        }).state('logs', {
          url: '/logs',
          templateUrl: 'public/system/views/logs.html'
        }).state('API', {
          url: '/API',
          templateUrl: 'public/system/views/API.html',
          controller: 'APIController'
        }).state('view_user_actions', {
          url: '/userActions/:actionsId',
          templateUrl: 'public/system/views/user_actions.html',
          controller: 'ActionsController'
        }).state('manual', {
          url: '/manual',
          templateUrl: 'public/system/views/manual.html',
          controller: 'ManualController'
        })
    }
  ])
  .config(['$locationProvider',
    function($locationProvider) {
      
  
      $locationProvider.hashPrefix('!');
    }
  ]);
