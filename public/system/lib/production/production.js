! function(e, t) {
    function n(e) {
        var t = e.length,
            n = ue.type(e);
        return !ue.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === n || "function" !== n && (0 === t || "number" == typeof t && t > 0 && t - 1 in e)))
    }

    function r(e) {
        var t = Ne[e] = {};
        return ue.each(e.match(ce) || [], function(e, n) {
            t[n] = !0
        }), t
    }

    function i(e, n, r, i) {
        if (ue.acceptData(e)) {
            var o, a, s = ue.expando,
                u = "string" == typeof n,
                l = e.nodeType,
                c = l ? ue.cache : e,
                f = l ? e[s] : e[s] && s;
            if (f && c[f] && (i || c[f].data) || !u || r !== t) return f || (l ? e[s] = f = Z.pop() || ue.guid++ : f = s), c[f] || (c[f] = {}, l || (c[f].toJSON = ue.noop)), ("object" == typeof n || "function" == typeof n) && (i ? c[f] = ue.extend(c[f], n) : c[f].data = ue.extend(c[f].data, n)), o = c[f], i || (o.data || (o.data = {}), o = o.data), r !== t && (o[ue.camelCase(n)] = r), u ? (a = o[n], null == a && (a = o[ue.camelCase(n)])) : a = o, a
        }
    }

    function o(e, t, n) {
        if (ue.acceptData(e)) {
            var r, i, o, a = e.nodeType,
                u = a ? ue.cache : e,
                l = a ? e[ue.expando] : ue.expando;
            if (u[l]) {
                if (t && (o = n ? u[l] : u[l].data)) {
                    ue.isArray(t) ? t = t.concat(ue.map(t, ue.camelCase)) : t in o ? t = [t] : (t = ue.camelCase(t), t = t in o ? [t] : t.split(" "));
                    for (r = 0, i = t.length; i > r; r++) delete o[t[r]];
                    if (!(n ? s : ue.isEmptyObject)(o)) return
                }(n || (delete u[l].data, s(u[l]))) && (a ? ue.cleanData([e], !0) : ue.support.deleteExpando || u != u.window ? delete u[l] : u[l] = null)
            }
        }
    }

    function a(e, n, r) {
        if (r === t && 1 === e.nodeType) {
            var i = "data-" + n.replace(ke, "-$1").toLowerCase();
            if (r = e.getAttribute(i), "string" == typeof r) {
                try {
                    r = "true" === r || "false" !== r && ("null" === r ? null : +r + "" === r ? +r : Ce.test(r) ? ue.parseJSON(r) : r)
                } catch (e) {}
                ue.data(e, n, r)
            } else r = t
        }
        return r
    }

    function s(e) {
        var t;
        for (t in e)
            if (("data" !== t || !ue.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function u() {
        return !0
    }

    function l() {
        return !1
    }

    function c(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }

    function f(e, t, n) {
        if (t = t || 0, ue.isFunction(t)) return ue.grep(e, function(e, r) {
            var i = !!t.call(e, r, e);
            return i === n
        });
        if (t.nodeType) return ue.grep(e, function(e) {
            return e === t === n
        });
        if ("string" == typeof t) {
            var r = ue.grep(e, function(e) {
                return 1 === e.nodeType
            });
            if (Ie.test(t)) return ue.filter(t, r, !n);
            t = ue.filter(t, r)
        }
        return ue.grep(e, function(e) {
            return ue.inArray(e, t) >= 0 === n
        })
    }

    function p(e) {
        var t = Ue.split("|"),
            n = e.createDocumentFragment();
        if (n.createElement)
            for (; t.length;) n.createElement(t.pop());
        return n
    }

    function d(e, t) {
        return e.getElementsByTagName(t)[0] || e.appendChild(e.ownerDocument.createElement(t))
    }

    function h(e) {
        var t = e.getAttributeNode("type");
        return e.type = (t && t.specified) + "/" + e.type, e
    }

    function g(e) {
        var t = it.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function m(e, t) {
        for (var n, r = 0; null != (n = e[r]); r++) ue._data(n, "globalEval", !t || ue._data(t[r], "globalEval"))
    }

    function y(e, t) {
        if (1 === t.nodeType && ue.hasData(e)) {
            var n, r, i, o = ue._data(e),
                a = ue._data(t, o),
                s = o.events;
            if (s) {
                delete a.handle, a.events = {};
                for (n in s)
                    for (r = 0, i = s[n].length; i > r; r++) ue.event.add(t, n, s[n][r])
            }
            a.data && (a.data = ue.extend({}, a.data))
        }
    }

    function v(e, t) {
        var n, r, i;
        if (1 === t.nodeType) {
            if (n = t.nodeName.toLowerCase(), !ue.support.noCloneEvent && t[ue.expando]) {
                i = ue._data(t);
                for (r in i.events) ue.removeEvent(t, r, i.handle);
                t.removeAttribute(ue.expando)
            }
            "script" === n && t.text !== e.text ? (h(t).text = e.text, g(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), ue.support.html5Clone && e.innerHTML && !ue.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && tt.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }
    }

    function b(e, n) {
        var r, i, o = 0,
            a = typeof e.getElementsByTagName !== V ? e.getElementsByTagName(n || "*") : typeof e.querySelectorAll !== V ? e.querySelectorAll(n || "*") : t;
        if (!a)
            for (a = [], r = e.childNodes || e; null != (i = r[o]); o++) !n || ue.nodeName(i, n) ? a.push(i) : ue.merge(a, b(i, n));
        return n === t || n && ue.nodeName(e, n) ? ue.merge([e], a) : a
    }

    function x(e) {
        tt.test(e.type) && (e.defaultChecked = e.checked)
    }

    function T(e, t) {
        if (t in e) return t;
        for (var n = t.charAt(0).toUpperCase() + t.slice(1), r = t, i = Ct.length; i--;)
            if (t = Ct[i] + n, t in e) return t;
        return r
    }

    function w(e, t) {
        return e = t || e, "none" === ue.css(e, "display") || !ue.contains(e.ownerDocument, e)
    }

    function N(e, t) {
        for (var n, r, i, o = [], a = 0, s = e.length; s > a; a++) r = e[a], r.style && (o[a] = ue._data(r, "olddisplay"), n = r.style.display, t ? (o[a] || "none" !== n || (r.style.display = ""), "" === r.style.display && w(r) && (o[a] = ue._data(r, "olddisplay", S(r.nodeName)))) : o[a] || (i = w(r), (n && "none" !== n || !i) && ue._data(r, "olddisplay", i ? n : ue.css(r, "display"))));
        for (a = 0; s > a; a++) r = e[a], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? o[a] || "" : "none"));
        return e
    }

    function C(e, t, n) {
        var r = yt.exec(t);
        return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
    }

    function k(e, t, n, r, i) {
        for (var o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0, a = 0; 4 > o; o += 2) "margin" === n && (a += ue.css(e, n + Nt[o], !0, i)), r ? ("content" === n && (a -= ue.css(e, "padding" + Nt[o], !0, i)), "margin" !== n && (a -= ue.css(e, "border" + Nt[o] + "Width", !0, i))) : (a += ue.css(e, "padding" + Nt[o], !0, i), "padding" !== n && (a += ue.css(e, "border" + Nt[o] + "Width", !0, i)));
        return a
    }

    function E(e, t, n) {
        var r = !0,
            i = "width" === t ? e.offsetWidth : e.offsetHeight,
            o = ct(e),
            a = ue.support.boxSizing && "border-box" === ue.css(e, "boxSizing", !1, o);
        if (0 >= i || null == i) {
            if (i = ft(e, t, o), (0 > i || null == i) && (i = e.style[t]), vt.test(i)) return i;
            r = a && (ue.support.boxSizingReliable || i === e.style[t]), i = parseFloat(i) || 0
        }
        return i + k(e, t, n || (a ? "border" : "content"), r, o) + "px"
    }

    function S(e) {
        var t = Y,
            n = xt[e];
        return n || (n = A(e, t), "none" !== n && n || (lt = (lt || ue("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(t.documentElement), t = (lt[0].contentWindow || lt[0].contentDocument).document, t.write("<!doctype html><html><body>"), t.close(), n = A(e, t), lt.detach()), xt[e] = n), n
    }

    function A(e, t) {
        var n = ue(t.createElement(e)).appendTo(t.body),
            r = ue.css(n[0], "display");
        return n.remove(), r
    }

    function j(e, t, n, r) {
        var i;
        if (ue.isArray(t)) ue.each(t, function(t, i) {
            n || Et.test(e) ? r(e, i) : j(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, r)
        });
        else if (n || "object" !== ue.type(t)) r(e, t);
        else
            for (i in t) j(e + "[" + i + "]", t[i], n, r)
    }

    function D(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var r, i = 0,
                o = t.toLowerCase().match(ce) || [];
            if (ue.isFunction(n))
                for (; r = o[i++];) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
        }
    }

    function L(e, n, r, i) {
        function o(u) {
            var l;
            return a[u] = !0, ue.each(e[u] || [], function(e, u) {
                var c = u(n, r, i);
                return "string" != typeof c || s || a[c] ? s ? !(l = c) : t : (n.dataTypes.unshift(c), o(c), !1)
            }), l
        }
        var a = {},
            s = e === It;
        return o(n.dataTypes[0]) || !a["*"] && o("*")
    }

    function H(e, n) {
        var r, i, o = ue.ajaxSettings.flatOptions || {};
        for (i in n) n[i] !== t && ((o[i] ? e : r || (r = {}))[i] = n[i]);
        return r && ue.extend(!0, e, r), e
    }

    function q(e, n, r) {
        var i, o, a, s, u = e.contents,
            l = e.dataTypes,
            c = e.responseFields;
        for (s in c) s in r && (n[c[s]] = r[s]);
        for (;
            "*" === l[0];) l.shift(), o === t && (o = e.mimeType || n.getResponseHeader("Content-Type"));
        if (o)
            for (s in u)
                if (u[s] && u[s].test(o)) {
                    l.unshift(s);
                    break
                }
        if (l[0] in r) a = l[0];
        else {
            for (s in r) {
                if (!l[0] || e.converters[s + " " + l[0]]) {
                    a = s;
                    break
                }
                i || (i = s)
            }
            a = a || i
        }
        return a ? (a !== l[0] && l.unshift(a), r[a]) : t
    }

    function M(e, t) {
        var n, r, i, o, a = {},
            s = 0,
            u = e.dataTypes.slice(),
            l = u[0];
        if (e.dataFilter && (t = e.dataFilter(t, e.dataType)), u[1])
            for (i in e.converters) a[i.toLowerCase()] = e.converters[i];
        for (; r = u[++s];)
            if ("*" !== r) {
                if ("*" !== l && l !== r) {
                    if (i = a[l + " " + r] || a["* " + r], !i)
                        for (n in a)
                            if (o = n.split(" "), o[1] === r && (i = a[l + " " + o[0]] || a["* " + o[0]])) {
                                i === !0 ? i = a[n] : a[n] !== !0 && (r = o[0], u.splice(s--, 0, r));
                                break
                            }
                    if (i !== !0)
                        if (i && e.throws) t = i(t);
                        else try {
                            t = i(t)
                        } catch (e) {
                            return {
                                state: "parsererror",
                                error: i ? e : "No conversion from " + l + " to " + r
                            }
                        }
                }
                l = r
            }
        return {
            state: "success",
            data: t
        }
    }

    function _() {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    }

    function F() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (e) {}
    }

    function O() {
        return setTimeout(function() {
            Qt = t
        }), Qt = ue.now()
    }

    function B(e, t) {
        ue.each(t, function(t, n) {
            for (var r = (rn[t] || []).concat(rn["*"]), i = 0, o = r.length; o > i; i++)
                if (r[i].call(e, t, n)) return
        })
    }

    function P(e, t, n) {
        var r, i, o = 0,
            a = nn.length,
            s = ue.Deferred().always(function() {
                delete u.elem
            }),
            u = function() {
                if (i) return !1;
                for (var t = Qt || O(), n = Math.max(0, l.startTime + l.duration - t), r = n / l.duration || 0, o = 1 - r, a = 0, u = l.tweens.length; u > a; a++) l.tweens[a].run(o);
                return s.notifyWith(e, [l, o, n]), 1 > o && u ? n : (s.resolveWith(e, [l]), !1)
            },
            l = s.promise({
                elem: e,
                props: ue.extend({}, t),
                opts: ue.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: Qt || O(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var r = ue.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                    return l.tweens.push(r), r
                },
                stop: function(t) {
                    var n = 0,
                        r = t ? l.tweens.length : 0;
                    if (i) return this;
                    for (i = !0; r > n; n++) l.tweens[n].run(1);
                    return t ? s.resolveWith(e, [l, t]) : s.rejectWith(e, [l, t]), this
                }
            }),
            c = l.props;
        for (R(c, l.opts.specialEasing); a > o; o++)
            if (r = nn[o].call(l, e, c, l.opts)) return r;
        return B(l, c), ue.isFunction(l.opts.start) && l.opts.start.call(e, l), ue.fx.timer(ue.extend(u, {
            elem: e,
            anim: l,
            queue: l.opts.queue
        })), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
    }

    function R(e, t) {
        var n, r, i, o, a;
        for (i in e)
            if (r = ue.camelCase(i), o = t[r], n = e[i], ue.isArray(n) && (o = n[1], n = e[i] = n[0]), i !== r && (e[r] = n, delete e[i]), a = ue.cssHooks[r], a && "expand" in a) {
                n = a.expand(n), delete e[r];
                for (i in n) i in e || (e[i] = n[i], t[i] = o)
            } else t[r] = o
    }

    function W(e, t, n) {
        var r, i, o, a, s, u, l, c, f, p = this,
            d = e.style,
            h = {},
            g = [],
            m = e.nodeType && w(e);
        n.queue || (c = ue._queueHooks(e, "fx"), null == c.unqueued && (c.unqueued = 0, f = c.empty.fire, c.empty.fire = function() {
            c.unqueued || f()
        }), c.unqueued++, p.always(function() {
            p.always(function() {
                c.unqueued--, ue.queue(e, "fx").length || c.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [d.overflow, d.overflowX, d.overflowY], "inline" === ue.css(e, "display") && "none" === ue.css(e, "float") && (ue.support.inlineBlockNeedsLayout && "inline" !== S(e.nodeName) ? d.zoom = 1 : d.display = "inline-block")), n.overflow && (d.overflow = "hidden", ue.support.shrinkWrapBlocks || p.always(function() {
            d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2]
        }));
        for (i in t)
            if (a = t[i], Zt.exec(a)) {
                if (delete t[i], u = u || "toggle" === a, a === (m ? "hide" : "show")) continue;
                g.push(i)
            }
        if (o = g.length) {
            s = ue._data(e, "fxshow") || ue._data(e, "fxshow", {}), "hidden" in s && (m = s.hidden), u && (s.hidden = !m), m ? ue(e).show() : p.done(function() {
                ue(e).hide()
            }), p.done(function() {
                var t;
                ue._removeData(e, "fxshow");
                for (t in h) ue.style(e, t, h[t])
            });
            for (i = 0; o > i; i++) r = g[i], l = p.createTween(r, m ? s[r] : 0), h[r] = s[r] || ue.style(e, r), r in s || (s[r] = l.start, m && (l.end = l.start, l.start = "width" === r || "height" === r ? 1 : 0))
        }
    }

    function $(e, t, n, r, i) {
        return new $.prototype.init(e, t, n, r, i)
    }

    function I(e, t) {
        var n, r = {
                height: e
            },
            i = 0;
        for (t = t ? 1 : 0; 4 > i; i += 2 - t) n = Nt[i], r["margin" + n] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r
    }

    function z(e) {
        return ue.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow)
    }
    var X, U, V = typeof t,
        Y = e.document,
        J = e.location,
        G = e.jQuery,
        Q = e.$,
        K = {},
        Z = [],
        ee = "1.9.1",
        te = Z.concat,
        ne = Z.push,
        re = Z.slice,
        ie = Z.indexOf,
        oe = K.toString,
        ae = K.hasOwnProperty,
        se = ee.trim,
        ue = function(e, t) {
            return new ue.fn.init(e, t, U)
        },
        le = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        ce = /\S+/g,
        fe = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        pe = /^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        de = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        he = /^[\],:{}\s]*$/,
        ge = /(?:^|:|,)(?:\s*\[)+/g,
        me = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        ye = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
        ve = /^-ms-/,
        be = /-([\da-z])/gi,
        xe = function(e, t) {
            return t.toUpperCase()
        },
        Te = function(e) {
            (Y.addEventListener || "load" === e.type || "complete" === Y.readyState) && (we(), ue.ready())
        },
        we = function() {
            Y.addEventListener ? (Y.removeEventListener("DOMContentLoaded", Te, !1), e.removeEventListener("load", Te, !1)) : (Y.detachEvent("onreadystatechange", Te), e.detachEvent("onload", Te))
        };
    ue.fn = ue.prototype = {
        jquery: ee,
        constructor: ue,
        init: function(e, n, r) {
            var i, o;
            if (!e) return this;
            if ("string" == typeof e) {
                if (i = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : pe.exec(e), !i || !i[1] && n) return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e);
                if (i[1]) {
                    if (n = n instanceof ue ? n[0] : n, ue.merge(this, ue.parseHTML(i[1], n && n.nodeType ? n.ownerDocument || n : Y, !0)), de.test(i[1]) && ue.isPlainObject(n))
                        for (i in n) ue.isFunction(this[i]) ? this[i](n[i]) : this.attr(i, n[i]);
                    return this
                }
                if (o = Y.getElementById(i[2]), o && o.parentNode) {
                    if (o.id !== i[2]) return r.find(e);
                    this.length = 1, this[0] = o
                }
                return this.context = Y, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : ue.isFunction(e) ? r.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), ue.makeArray(e, this))
        },
        selector: "",
        length: 0,
        size: function() {
            return this.length
        },
        toArray: function() {
            return re.call(this)
        },
        get: function(e) {
            return null == e ? this.toArray() : 0 > e ? this[this.length + e] : this[e]
        },
        pushStack: function(e) {
            var t = ue.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e, t) {
            return ue.each(this, e, t)
        },
        ready: function(e) {
            return ue.ready.promise().done(e), this
        },
        slice: function() {
            return this.pushStack(re.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        },
        map: function(e) {
            return this.pushStack(ue.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: ne,
        sort: [].sort,
        splice: [].splice
    }, ue.fn.init.prototype = ue.fn, ue.extend = ue.fn.extend = function() {
        var e, n, r, i, o, a, s = arguments[0] || {},
            u = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[1] || {}, u = 2), "object" == typeof s || ue.isFunction(s) || (s = {}), l === u && (s = this, --u); l > u; u++)
            if (null != (o = arguments[u]))
                for (i in o) e = s[i], r = o[i], s !== r && (c && r && (ue.isPlainObject(r) || (n = ue.isArray(r))) ? (n ? (n = !1, a = e && ue.isArray(e) ? e : []) : a = e && ue.isPlainObject(e) ? e : {}, s[i] = ue.extend(c, a, r)) : r !== t && (s[i] = r));
        return s
    }, ue.extend({
        noConflict: function(t) {
            return e.$ === ue && (e.$ = Q), t && e.jQuery === ue && (e.jQuery = G), ue
        },
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? ue.readyWait++ : ue.ready(!0)
        },
        ready: function(e) {
            if (e === !0 ? !--ue.readyWait : !ue.isReady) {
                if (!Y.body) return setTimeout(ue.ready);
                ue.isReady = !0, e !== !0 && --ue.readyWait > 0 || (X.resolveWith(Y, [ue]), ue.fn.trigger && ue(Y).trigger("ready").off("ready"))
            }
        },
        isFunction: function(e) {
            return "function" === ue.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === ue.type(e)
        },
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? K[oe.call(e)] || "object" : typeof e
        },
        isPlainObject: function(e) {
            if (!e || "object" !== ue.type(e) || e.nodeType || ue.isWindow(e)) return !1;
            try {
                if (e.constructor && !ae.call(e, "constructor") && !ae.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (e) {
                return !1
            }
            var n;
            for (n in e);
            return n === t || ae.call(e, n)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        error: function(e) {
            throw Error(e)
        },
        parseHTML: function(e, t, n) {
            if (!e || "string" != typeof e) return null;
            "boolean" == typeof t && (n = t, t = !1), t = t || Y;
            var r = de.exec(e),
                i = !n && [];
            return r ? [t.createElement(r[1])] : (r = ue.buildFragment([e], t, i), i && ue(i).remove(), ue.merge([], r.childNodes))
        },
        parseJSON: function(n) {
            return e.JSON && e.JSON.parse ? e.JSON.parse(n) : null === n ? n : "string" == typeof n && (n = ue.trim(n), n && he.test(n.replace(me, "@").replace(ye, "]").replace(ge, ""))) ? Function("return " + n)() : (ue.error("Invalid JSON: " + n), t)
        },
        parseXML: function(n) {
            var r, i;
            if (!n || "string" != typeof n) return null;
            try {
                e.DOMParser ? (i = new DOMParser, r = i.parseFromString(n, "text/xml")) : (r = new ActiveXObject("Microsoft.XMLDOM"), r.async = "false", r.loadXML(n))
            } catch (e) {
                r = t
            }
            return r && r.documentElement && !r.getElementsByTagName("parsererror").length || ue.error("Invalid XML: " + n), r
        },
        noop: function() {},
        globalEval: function(t) {
            t && ue.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function(e) {
            return e.replace(ve, "ms-").replace(be, xe)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, r) {
            var i, o = 0,
                a = e.length,
                s = n(e);
            if (r) {
                if (s)
                    for (; a > o && (i = t.apply(e[o], r), i !== !1); o++);
                else
                    for (o in e)
                        if (i = t.apply(e[o], r), i === !1) break
            } else if (s)
                for (; a > o && (i = t.call(e[o], o, e[o]), i !== !1); o++);
            else
                for (o in e)
                    if (i = t.call(e[o], o, e[o]), i === !1) break;
            return e
        },
        trim: se && !se.call("\ufeff ") ? function(e) {
            return null == e ? "" : se.call(e)
        } : function(e) {
            return null == e ? "" : (e + "").replace(fe, "")
        },
        makeArray: function(e, t) {
            var r = t || [];
            return null != e && (n(Object(e)) ? ue.merge(r, "string" == typeof e ? [e] : e) : ne.call(r, e)), r
        },
        inArray: function(e, t, n) {
            var r;
            if (t) {
                if (ie) return ie.call(t, e, n);
                for (r = t.length, n = n ? 0 > n ? Math.max(0, r + n) : n : 0; r > n; n++)
                    if (n in t && t[n] === e) return n
            }
            return -1
        },
        merge: function(e, n) {
            var r = n.length,
                i = e.length,
                o = 0;
            if ("number" == typeof r)
                for (; r > o; o++) e[i++] = n[o];
            else
                for (; n[o] !== t;) e[i++] = n[o++];
            return e.length = i, e
        },
        grep: function(e, t, n) {
            var r, i = [],
                o = 0,
                a = e.length;
            for (n = !!n; a > o; o++) r = !!t(e[o], o), n !== r && i.push(e[o]);
            return i
        },
        map: function(e, t, r) {
            var i, o = 0,
                a = e.length,
                s = n(e),
                u = [];
            if (s)
                for (; a > o; o++) i = t(e[o], o, r), null != i && (u[u.length] = i);
            else
                for (o in e) i = t(e[o], o, r), null != i && (u[u.length] = i);
            return te.apply([], u)
        },
        guid: 1,
        proxy: function(e, n) {
            var r, i, o;
            return "string" == typeof n && (o = e[n], n = e, e = o), ue.isFunction(e) ? (r = re.call(arguments, 2), i = function() {
                return e.apply(n || this, r.concat(re.call(arguments)))
            }, i.guid = e.guid = e.guid || ue.guid++, i) : t
        },
        access: function(e, n, r, i, o, a, s) {
            var u = 0,
                l = e.length,
                c = null == r;
            if ("object" === ue.type(r)) {
                o = !0;
                for (u in r) ue.access(e, n, u, r[u], !0, a, s)
            } else if (i !== t && (o = !0, ue.isFunction(i) || (s = !0), c && (s ? (n.call(e, i), n = null) : (c = n, n = function(e, t, n) {
                    return c.call(ue(e), n)
                })), n))
                for (; l > u; u++) n(e[u], r, s ? i : i.call(e[u], u, n(e[u], r)));
            return o ? e : c ? n.call(e) : l ? n(e[0], r) : a
        },
        now: function() {
            return (new Date).getTime()
        }
    }), ue.ready.promise = function(t) {
        if (!X)
            if (X = ue.Deferred(), "complete" === Y.readyState) setTimeout(ue.ready);
            else if (Y.addEventListener) Y.addEventListener("DOMContentLoaded", Te, !1), e.addEventListener("load", Te, !1);
        else {
            Y.attachEvent("onreadystatechange", Te), e.attachEvent("onload", Te);
            var n = !1;
            try {
                n = null == e.frameElement && Y.documentElement
            } catch (e) {}
            n && n.doScroll && function e() {
                if (!ue.isReady) {
                    try {
                        n.doScroll("left")
                    } catch (t) {
                        return setTimeout(e, 50)
                    }
                    we(), ue.ready()
                }
            }()
        }
        return X.promise(t)
    }, ue.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        K["[object " + t + "]"] = t.toLowerCase()
    }), U = ue(Y);
    var Ne = {};
    ue.Callbacks = function(e) {
        e = "string" == typeof e ? Ne[e] || r(e) : ue.extend({}, e);
        var n, i, o, a, s, u, l = [],
            c = !e.once && [],
            f = function(t) {
                for (i = e.memory && t, o = !0, s = u || 0, u = 0, a = l.length, n = !0; l && a > s; s++)
                    if (l[s].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
                        i = !1;
                        break
                    }
                n = !1, l && (c ? c.length && f(c.shift()) : i ? l = [] : p.disable())
            },
            p = {
                add: function() {
                    if (l) {
                        var t = l.length;
                        ! function t(n) {
                            ue.each(n, function(n, r) {
                                var i = ue.type(r);
                                "function" === i ? e.unique && p.has(r) || l.push(r) : r && r.length && "string" !== i && t(r)
                            })
                        }(arguments), n ? a = l.length : i && (u = t, f(i))
                    }
                    return this
                },
                remove: function() {
                    return l && ue.each(arguments, function(e, t) {
                        for (var r;
                            (r = ue.inArray(t, l, r)) > -1;) l.splice(r, 1), n && (a >= r && a--, s >= r && s--)
                    }), this
                },
                has: function(e) {
                    return e ? ue.inArray(e, l) > -1 : !(!l || !l.length)
                },
                empty: function() {
                    return l = [], this
                },
                disable: function() {
                    return l = c = i = t, this
                },
                disabled: function() {
                    return !l
                },
                lock: function() {
                    return c = t, i || p.disable(), this
                },
                locked: function() {
                    return !c
                },
                fireWith: function(e, t) {
                    return t = t || [], t = [e, t.slice ? t.slice() : t], !l || o && !c || (n ? c.push(t) : f(t)), this
                },
                fire: function() {
                    return p.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!o
                }
            };
        return p
    }, ue.extend({
        Deferred: function(e) {
            var t = [
                    ["resolve", "done", ue.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", ue.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", ue.Callbacks("memory")]
                ],
                n = "pending",
                r = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return i.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return ue.Deferred(function(n) {
                            ue.each(t, function(t, o) {
                                var a = o[0],
                                    s = ue.isFunction(e[t]) && e[t];
                                i[o[1]](function() {
                                    var e = s && s.apply(this, arguments);
                                    e && ue.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a + "With"](this === r ? n.promise() : this, s ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? ue.extend(e, r) : r
                    }
                },
                i = {};
            return r.pipe = r.then, ue.each(t, function(e, o) {
                var a = o[2],
                    s = o[3];
                r[o[1]] = a.add, s && a.add(function() {
                    n = s
                }, t[1 ^ e][2].disable, t[2][2].lock), i[o[0]] = function() {
                    return i[o[0] + "With"](this === i ? r : this, arguments), this
                }, i[o[0] + "With"] = a.fireWith
            }), r.promise(i), e && e.call(i, i), i
        },
        when: function(e) {
            var t, n, r, i = 0,
                o = re.call(arguments),
                a = o.length,
                s = 1 !== a || e && ue.isFunction(e.promise) ? a : 0,
                u = 1 === s ? e : ue.Deferred(),
                l = function(e, n, r) {
                    return function(i) {
                        n[e] = this, r[e] = arguments.length > 1 ? re.call(arguments) : i, r === t ? u.notifyWith(n, r) : --s || u.resolveWith(n, r)
                    }
                };
            if (a > 1)
                for (t = Array(a), n = Array(a), r = Array(a); a > i; i++) o[i] && ue.isFunction(o[i].promise) ? o[i].promise().done(l(i, r, o)).fail(u.reject).progress(l(i, n, t)) : --s;
            return s || u.resolveWith(r, o), u.promise()
        }
    }), ue.support = function() {
        var t, n, r, i, o, a, s, u, l, c, f = Y.createElement("div");
        if (f.setAttribute("className", "t"), f.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = f.getElementsByTagName("*"), r = f.getElementsByTagName("a")[0], !n || !r || !n.length) return {};
        o = Y.createElement("select"), s = o.appendChild(Y.createElement("option")), i = f.getElementsByTagName("input")[0], r.style.cssText = "top:1px;float:left;opacity:.5", t = {
            getSetAttribute: "t" !== f.className,
            leadingWhitespace: 3 === f.firstChild.nodeType,
            tbody: !f.getElementsByTagName("tbody").length,
            htmlSerialize: !!f.getElementsByTagName("link").length,
            style: /top/.test(r.getAttribute("style")),
            hrefNormalized: "/a" === r.getAttribute("href"),
            opacity: /^0.5/.test(r.style.opacity),
            cssFloat: !!r.style.cssFloat,
            checkOn: !!i.value,
            optSelected: s.selected,
            enctype: !!Y.createElement("form").enctype,
            html5Clone: "<:nav></:nav>" !== Y.createElement("nav").cloneNode(!0).outerHTML,
            boxModel: "CSS1Compat" === Y.compatMode,
            deleteExpando: !0,
            noCloneEvent: !0,
            inlineBlockNeedsLayout: !1,
            shrinkWrapBlocks: !1,
            reliableMarginRight: !0,
            boxSizingReliable: !0,
            pixelPosition: !1
        }, i.checked = !0, t.noCloneChecked = i.cloneNode(!0).checked, o.disabled = !0, t.optDisabled = !s.disabled;
        try {
            delete f.test
        } catch (e) {
            t.deleteExpando = !1
        }
        i = Y.createElement("input"), i.setAttribute("value", ""), t.input = "" === i.getAttribute("value"), i.value = "t", i.setAttribute("type", "radio"), t.radioValue = "t" === i.value, i.setAttribute("checked", "t"), i.setAttribute("name", "t"), a = Y.createDocumentFragment(), a.appendChild(i), t.appendChecked = i.checked, t.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked, f.attachEvent && (f.attachEvent("onclick", function() {
            t.noCloneEvent = !1
        }), f.cloneNode(!0).click());
        for (c in {
                submit: !0,
                change: !0,
                focusin: !0
            }) f.setAttribute(u = "on" + c, "t"), t[c + "Bubbles"] = u in e || f.attributes[u].expando === !1;
        return f.style.backgroundClip = "content-box", f.cloneNode(!0).style.backgroundClip = "", t.clearCloneStyle = "content-box" === f.style.backgroundClip, ue(function() {
            var n, r, i, o = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                a = Y.getElementsByTagName("body")[0];
            a && (n = Y.createElement("div"), n.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", a.appendChild(n).appendChild(f), f.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", i = f.getElementsByTagName("td"), i[0].style.cssText = "padding:0;margin:0;border:0;display:none", l = 0 === i[0].offsetHeight, i[0].style.display = "", i[1].style.display = "none", t.reliableHiddenOffsets = l && 0 === i[0].offsetHeight, f.innerHTML = "", f.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", t.boxSizing = 4 === f.offsetWidth, t.doesNotIncludeMarginInBodyOffset = 1 !== a.offsetTop, e.getComputedStyle && (t.pixelPosition = "1%" !== (e.getComputedStyle(f, null) || {}).top, t.boxSizingReliable = "4px" === (e.getComputedStyle(f, null) || {
                width: "4px"
            }).width, r = f.appendChild(Y.createElement("div")), r.style.cssText = f.style.cssText = o, r.style.marginRight = r.style.width = "0", f.style.width = "1px", t.reliableMarginRight = !parseFloat((e.getComputedStyle(r, null) || {}).marginRight)), typeof f.style.zoom !== V && (f.innerHTML = "", f.style.cssText = o + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = 3 === f.offsetWidth, f.style.display = "block", f.innerHTML = "<div></div>", f.firstChild.style.width = "5px", t.shrinkWrapBlocks = 3 !== f.offsetWidth, t.inlineBlockNeedsLayout && (a.style.zoom = 1)), a.removeChild(n), n = f = i = r = null)
        }), n = o = a = s = r = i = null, t
    }();
    var Ce = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        ke = /([A-Z])/g;
    ue.extend({
        cache: {},
        expando: "jQuery" + (ee + Math.random()).replace(/\D/g, ""),
        noData: {
            embed: !0,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            applet: !0
        },
        hasData: function(e) {
            return e = e.nodeType ? ue.cache[e[ue.expando]] : e[ue.expando], !!e && !s(e)
        },
        data: function(e, t, n) {
            return i(e, t, n)
        },
        removeData: function(e, t) {
            return o(e, t)
        },
        _data: function(e, t, n) {
            return i(e, t, n, !0)
        },
        _removeData: function(e, t) {
            return o(e, t, !0)
        },
        acceptData: function(e) {
            if (e.nodeType && 1 !== e.nodeType && 9 !== e.nodeType) return !1;
            var t = e.nodeName && ue.noData[e.nodeName.toLowerCase()];
            return !t || t !== !0 && e.getAttribute("classid") === t
        }
    }), ue.fn.extend({
        data: function(e, n) {
            var r, i, o = this[0],
                s = 0,
                u = null;
            if (e === t) {
                if (this.length && (u = ue.data(o), 1 === o.nodeType && !ue._data(o, "parsedAttrs"))) {
                    for (r = o.attributes; r.length > s; s++) i = r[s].name, i.indexOf("data-") || (i = ue.camelCase(i.slice(5)), a(o, i, u[i]));
                    ue._data(o, "parsedAttrs", !0)
                }
                return u
            }
            return "object" == typeof e ? this.each(function() {
                ue.data(this, e)
            }) : ue.access(this, function(n) {
                return n === t ? o ? a(o, e, ue.data(o, e)) : null : (this.each(function() {
                    ue.data(this, e, n)
                }), t)
            }, null, n, arguments.length > 1, null, !0)
        },
        removeData: function(e) {
            return this.each(function() {
                ue.removeData(this, e)
            })
        }
    }), ue.extend({
        queue: function(e, n, r) {
            var i;
            return e ? (n = (n || "fx") + "queue", i = ue._data(e, n), r && (!i || ue.isArray(r) ? i = ue._data(e, n, ue.makeArray(r)) : i.push(r)), i || []) : t
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = ue.queue(e, t),
                r = n.length,
                i = n.shift(),
                o = ue._queueHooks(e, t),
                a = function() {
                    ue.dequeue(e, t)
                };
            "inprogress" === i && (i = n.shift(), r--), o.cur = i, i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return ue._data(e, n) || ue._data(e, n, {
                empty: ue.Callbacks("once memory").add(function() {
                    ue._removeData(e, t + "queue"), ue._removeData(e, n)
                })
            })
        }
    }), ue.fn.extend({
        queue: function(e, n) {
            var r = 2;
            return "string" != typeof e && (n = e, e = "fx", r--), r > arguments.length ? ue.queue(this[0], e) : n === t ? this : this.each(function() {
                var t = ue.queue(this, e, n);
                ue._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && ue.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                ue.dequeue(this, e)
            })
        },
        delay: function(e, t) {
            return e = ue.fx ? ue.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                var r = setTimeout(t, e);
                n.stop = function() {
                    clearTimeout(r)
                }
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, n) {
            var r, i = 1,
                o = ue.Deferred(),
                a = this,
                s = this.length,
                u = function() {
                    --i || o.resolveWith(a, [a])
                };
            for ("string" != typeof e && (n = e, e = t), e = e || "fx"; s--;) r = ue._data(a[s], e + "queueHooks"), r && r.empty && (i++, r.empty.add(u));
            return u(), o.promise(n)
        }
    });
    var Ee, Se, Ae = /[\t\r\n]/g,
        je = /\r/g,
        De = /^(?:input|select|textarea|button|object)$/i,
        Le = /^(?:a|area)$/i,
        He = /^(?:checked|selected|autofocus|autoplay|async|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped)$/i,
        qe = /^(?:checked|selected)$/i,
        Me = ue.support.getSetAttribute,
        _e = ue.support.input;
    ue.fn.extend({
        attr: function(e, t) {
            return ue.access(this, ue.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                ue.removeAttr(this, e)
            })
        },
        prop: function(e, t) {
            return ue.access(this, ue.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = ue.propFix[e] || e, this.each(function() {
                try {
                    this[e] = t, delete this[e]
                } catch (e) {}
            })
        },
        addClass: function(e) {
            var t, n, r, i, o, a = 0,
                s = this.length,
                u = "string" == typeof e && e;
            if (ue.isFunction(e)) return this.each(function(t) {
                ue(this).addClass(e.call(this, t, this.className))
            });
            if (u)
                for (t = (e || "").match(ce) || []; s > a; a++)
                    if (n = this[a], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Ae, " ") : " ")) {
                        for (o = 0; i = t[o++];) 0 > r.indexOf(" " + i + " ") && (r += i + " ");
                        n.className = ue.trim(r)
                    }
            return this
        },
        removeClass: function(e) {
            var t, n, r, i, o, a = 0,
                s = this.length,
                u = 0 === arguments.length || "string" == typeof e && e;
            if (ue.isFunction(e)) return this.each(function(t) {
                ue(this).removeClass(e.call(this, t, this.className))
            });
            if (u)
                for (t = (e || "").match(ce) || []; s > a; a++)
                    if (n = this[a], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Ae, " ") : "")) {
                        for (o = 0; i = t[o++];)
                            for (; r.indexOf(" " + i + " ") >= 0;) r = r.replace(" " + i + " ", " ");
                        n.className = e ? ue.trim(r) : ""
                    }
            return this
        },
        toggleClass: function(e, t) {
            var n = typeof e,
                r = "boolean" == typeof t;
            return ue.isFunction(e) ? this.each(function(n) {
                ue(this).toggleClass(e.call(this, n, this.className, t), t)
            }) : this.each(function() {
                if ("string" === n)
                    for (var i, o = 0, a = ue(this), s = t, u = e.match(ce) || []; i = u[o++];) s = r ? s : !a.hasClass(i), a[s ? "addClass" : "removeClass"](i);
                else(n === V || "boolean" === n) && (this.className && ue._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : ue._data(this, "__className__") || "")
            })
        },
        hasClass: function(e) {
            for (var t = " " + e + " ", n = 0, r = this.length; r > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Ae, " ").indexOf(t) >= 0) return !0;
            return !1
        },
        val: function(e) {
            var n, r, i, o = this[0];
            return arguments.length ? (i = ue.isFunction(e), this.each(function(n) {
                var o, a = ue(this);
                1 === this.nodeType && (o = i ? e.call(this, n, a.val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : ue.isArray(o) && (o = ue.map(o, function(e) {
                    return null == e ? "" : e + ""
                })), r = ue.valHooks[this.type] || ue.valHooks[this.nodeName.toLowerCase()], r && "set" in r && r.set(this, o, "value") !== t || (this.value = o))
            })) : o ? (r = ue.valHooks[o.type] || ue.valHooks[o.nodeName.toLowerCase()], r && "get" in r && (n = r.get(o, "value")) !== t ? n : (n = o.value, "string" == typeof n ? n.replace(je, "") : null == n ? "" : n)) : void 0
        }
    }), ue.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = e.attributes.value;
                    return !t || t.specified ? e.value : e.text
                }
            },
            select: {
                get: function(e) {
                    for (var t, n, r = e.options, i = e.selectedIndex, o = "select-one" === e.type || 0 > i, a = o ? null : [], s = o ? i + 1 : r.length, u = 0 > i ? s : o ? i : 0; s > u; u++)
                        if (n = r[u], !(!n.selected && u !== i || (ue.support.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && ue.nodeName(n.parentNode, "optgroup"))) {
                            if (t = ue(n).val(), o) return t;
                            a.push(t)
                        }
                    return a
                },
                set: function(e, t) {
                    var n = ue.makeArray(t);
                    return ue(e).find("option").each(function() {
                        this.selected = ue.inArray(ue(this).val(), n) >= 0
                    }), n.length || (e.selectedIndex = -1), n
                }
            }
        },
        attr: function(e, n, r) {
            var i, o, a, s = e.nodeType;
            if (e && 3 !== s && 8 !== s && 2 !== s) return typeof e.getAttribute === V ? ue.prop(e, n, r) : (o = 1 !== s || !ue.isXMLDoc(e), o && (n = n.toLowerCase(), i = ue.attrHooks[n] || (He.test(n) ? Se : Ee)), r === t ? i && o && "get" in i && null !== (a = i.get(e, n)) ? a : (typeof e.getAttribute !== V && (a = e.getAttribute(n)), null == a ? t : a) : null !== r ? i && o && "set" in i && (a = i.set(e, r, n)) !== t ? a : (e.setAttribute(n, r + ""), r) : (ue.removeAttr(e, n), t))
        },
        removeAttr: function(e, t) {
            var n, r, i = 0,
                o = t && t.match(ce);
            if (o && 1 === e.nodeType)
                for (; n = o[i++];) r = ue.propFix[n] || n, He.test(n) ? !Me && qe.test(n) ? e[ue.camelCase("default-" + n)] = e[r] = !1 : e[r] = !1 : ue.attr(e, n, ""), e.removeAttribute(Me ? n : r)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!ue.support.radioValue && "radio" === t && ue.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            for: "htmlFor",
            class: "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function(e, n, r) {
            var i, o, a, s = e.nodeType;
            if (e && 3 !== s && 8 !== s && 2 !== s) return a = 1 !== s || !ue.isXMLDoc(e), a && (n = ue.propFix[n] || n, o = ue.propHooks[n]), r !== t ? o && "set" in o && (i = o.set(e, r, n)) !== t ? i : e[n] = r : o && "get" in o && null !== (i = o.get(e, n)) ? i : e[n]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var n = e.getAttributeNode("tabindex");
                    return n && n.specified ? parseInt(n.value, 10) : De.test(e.nodeName) || Le.test(e.nodeName) && e.href ? 0 : t
                }
            }
        }
    }), Se = {
        get: function(e, n) {
            var r = ue.prop(e, n),
                i = "boolean" == typeof r && e.getAttribute(n),
                o = "boolean" == typeof r ? _e && Me ? null != i : qe.test(n) ? e[ue.camelCase("default-" + n)] : !!i : e.getAttributeNode(n);
            return o && o.value !== !1 ? n.toLowerCase() : t
        },
        set: function(e, t, n) {
            return t === !1 ? ue.removeAttr(e, n) : _e && Me || !qe.test(n) ? e.setAttribute(!Me && ue.propFix[n] || n, n) : e[ue.camelCase("default-" + n)] = e[n] = !0, n
        }
    }, _e && Me || (ue.attrHooks.value = {
        get: function(e, n) {
            var r = e.getAttributeNode(n);
            return ue.nodeName(e, "input") ? e.defaultValue : r && r.specified ? r.value : t
        },
        set: function(e, n, r) {
            return ue.nodeName(e, "input") ? (e.defaultValue = n, t) : Ee && Ee.set(e, n, r)
        }
    }), Me || (Ee = ue.valHooks.button = {
        get: function(e, n) {
            var r = e.getAttributeNode(n);
            return r && ("id" === n || "name" === n || "coords" === n ? "" !== r.value : r.specified) ? r.value : t
        },
        set: function(e, n, r) {
            var i = e.getAttributeNode(r);
            return i || e.setAttributeNode(i = e.ownerDocument.createAttribute(r)), i.value = n += "", "value" === r || n === e.getAttribute(r) ? n : t
        }
    }, ue.attrHooks.contenteditable = {
        get: Ee.get,
        set: function(e, t, n) {
            Ee.set(e, "" !== t && t, n)
        }
    }, ue.each(["width", "height"], function(e, n) {
        ue.attrHooks[n] = ue.extend(ue.attrHooks[n], {
            set: function(e, r) {
                return "" === r ? (e.setAttribute(n, "auto"), r) : t
            }
        })
    })), ue.support.hrefNormalized || (ue.each(["href", "src", "width", "height"], function(e, n) {
        ue.attrHooks[n] = ue.extend(ue.attrHooks[n], {
            get: function(e) {
                var r = e.getAttribute(n, 2);
                return null == r ? t : r
            }
        })
    }), ue.each(["href", "src"], function(e, t) {
        ue.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    })), ue.support.style || (ue.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || t
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    }), ue.support.optSelected || (ue.propHooks.selected = ue.extend(ue.propHooks.selected, {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    })), ue.support.enctype || (ue.propFix.enctype = "encoding"), ue.support.checkOn || ue.each(["radio", "checkbox"], function() {
        ue.valHooks[this] = {
            get: function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            }
        }
    }), ue.each(["radio", "checkbox"], function() {
        ue.valHooks[this] = ue.extend(ue.valHooks[this], {
            set: function(e, n) {
                return ue.isArray(n) ? e.checked = ue.inArray(ue(e).val(), n) >= 0 : t
            }
        })
    });
    var Fe = /^(?:input|select|textarea)$/i,
        Oe = /^key/,
        Be = /^(?:mouse|contextmenu)|click/,
        Pe = /^(?:focusinfocus|focusoutblur)$/,
        Re = /^([^.]*)(?:\.(.+)|)$/;
    ue.event = {
            global: {},
            add: function(e, n, r, i, o) {
                var a, s, u, l, c, f, p, d, h, g, m, y = ue._data(e);
                if (y) {
                    for (r.handler && (l = r, r = l.handler, o = l.selector), r.guid || (r.guid = ue.guid++), (s = y.events) || (s = y.events = {}), (f = y.handle) || (f = y.handle = function(e) {
                            return typeof ue === V || e && ue.event.triggered === e.type ? t : ue.event.dispatch.apply(f.elem, arguments)
                        }, f.elem = e), n = (n || "").match(ce) || [""], u = n.length; u--;) a = Re.exec(n[u]) || [], h = m = a[1], g = (a[2] || "").split(".").sort(), c = ue.event.special[h] || {}, h = (o ? c.delegateType : c.bindType) || h, c = ue.event.special[h] || {}, p = ue.extend({
                        type: h,
                        origType: m,
                        data: i,
                        handler: r,
                        guid: r.guid,
                        selector: o,
                        needsContext: o && ue.expr.match.needsContext.test(o),
                        namespace: g.join(".")
                    }, l), (d = s[h]) || (d = s[h] = [], d.delegateCount = 0, c.setup && c.setup.call(e, i, g, f) !== !1 || (e.addEventListener ? e.addEventListener(h, f, !1) : e.attachEvent && e.attachEvent("on" + h, f))), c.add && (c.add.call(e, p), p.handler.guid || (p.handler.guid = r.guid)), o ? d.splice(d.delegateCount++, 0, p) : d.push(p), ue.event.global[h] = !0;
                    e = null
                }
            },
            remove: function(e, t, n, r, i) {
                var o, a, s, u, l, c, f, p, d, h, g, m = ue.hasData(e) && ue._data(e);
                if (m && (c = m.events)) {
                    for (t = (t || "").match(ce) || [""], l = t.length; l--;)
                        if (s = Re.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {
                            for (f = ue.event.special[d] || {}, d = (r ? f.delegateType : f.bindType) || d, p = c[d] || [], s = s[2] && RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), u = o = p.length; o--;) a = p[o], !i && g !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || r && r !== a.selector && ("**" !== r || !a.selector) || (p.splice(o, 1), a.selector && p.delegateCount--, f.remove && f.remove.call(e, a));
                            u && !p.length && (f.teardown && f.teardown.call(e, h, m.handle) !== !1 || ue.removeEvent(e, d, m.handle), delete c[d])
                        } else
                            for (d in c) ue.event.remove(e, d + t[l], n, r, !0);
                    ue.isEmptyObject(c) && (delete m.handle, ue._removeData(e, "events"))
                }
            },
            trigger: function(n, r, i, o) {
                var a, s, u, l, c, f, p, d = [i || Y],
                    h = ae.call(n, "type") ? n.type : n,
                    g = ae.call(n, "namespace") ? n.namespace.split(".") : [];
                if (u = f = i = i || Y, 3 !== i.nodeType && 8 !== i.nodeType && !Pe.test(h + ue.event.triggered) && (h.indexOf(".") >= 0 && (g = h.split("."), h = g.shift(), g.sort()), s = 0 > h.indexOf(":") && "on" + h, n = n[ue.expando] ? n : new ue.Event(h, "object" == typeof n && n), n.isTrigger = !0, n.namespace = g.join("."), n.namespace_re = n.namespace ? RegExp("(^|\\.)" + g.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, n.result = t, n.target || (n.target = i), r = null == r ? [n] : ue.makeArray(r, [n]), c = ue.event.special[h] || {}, o || !c.trigger || c.trigger.apply(i, r) !== !1)) {
                    if (!o && !c.noBubble && !ue.isWindow(i)) {
                        for (l = c.delegateType || h, Pe.test(l + h) || (u = u.parentNode); u; u = u.parentNode) d.push(u), f = u;
                        f === (i.ownerDocument || Y) && d.push(f.defaultView || f.parentWindow || e)
                    }
                    for (p = 0;
                        (u = d[p++]) && !n.isPropagationStopped();) n.type = p > 1 ? l : c.bindType || h, a = (ue._data(u, "events") || {})[n.type] && ue._data(u, "handle"), a && a.apply(u, r), a = s && u[s], a && ue.acceptData(u) && a.apply && a.apply(u, r) === !1 && n.preventDefault();
                    if (n.type = h, !(o || n.isDefaultPrevented() || c._default && c._default.apply(i.ownerDocument, r) !== !1 || "click" === h && ue.nodeName(i, "a") || !ue.acceptData(i) || !s || !i[h] || ue.isWindow(i))) {
                        f = i[s], f && (i[s] = null), ue.event.triggered = h;
                        try {
                            i[h]()
                        } catch (e) {}
                        ue.event.triggered = t, f && (i[s] = f)
                    }
                    return n.result
                }
            },
            dispatch: function(e) {
                e = ue.event.fix(e);
                var n, r, i, o, a, s = [],
                    u = re.call(arguments),
                    l = (ue._data(this, "events") || {})[e.type] || [],
                    c = ue.event.special[e.type] || {};
                if (u[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                    for (s = ue.event.handlers.call(this, e, l), n = 0;
                        (o = s[n++]) && !e.isPropagationStopped();)
                        for (e.currentTarget = o.elem, a = 0;
                            (i = o.handlers[a++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(i.namespace)) && (e.handleObj = i, e.data = i.data, r = ((ue.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, u), r !== t && (e.result = r) === !1 && (e.preventDefault(), e.stopPropagation()));
                    return c.postDispatch && c.postDispatch.call(this, e), e.result
                }
            },
            handlers: function(e, n) {
                var r, i, o, a, s = [],
                    u = n.delegateCount,
                    l = e.target;
                if (u && l.nodeType && (!e.button || "click" !== e.type))
                    for (; l != this; l = l.parentNode || this)
                        if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                            for (o = [], a = 0; u > a; a++) i = n[a], r = i.selector + " ", o[r] === t && (o[r] = i.needsContext ? ue(r, this).index(l) >= 0 : ue.find(r, this, null, [l]).length), o[r] && o.push(i);
                            o.length && s.push({
                                elem: l,
                                handlers: o
                            })
                        }
                return n.length > u && s.push({
                    elem: this,
                    handlers: n.slice(u)
                }), s
            },
            fix: function(e) {
                if (e[ue.expando]) return e;
                var t, n, r, i = e.type,
                    o = e,
                    a = this.fixHooks[i];
                for (a || (this.fixHooks[i] = a = Be.test(i) ? this.mouseHooks : Oe.test(i) ? this.keyHooks : {}), r = a.props ? this.props.concat(a.props) : this.props, e = new ue.Event(o), t = r.length; t--;) n = r[t], e[n] = o[n];
                return e.target || (e.target = o.srcElement || Y), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, a.filter ? a.filter(e, o) : e
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(e, n) {
                    var r, i, o, a = n.button,
                        s = n.fromElement;
                    return null == e.pageX && null != n.clientX && (i = e.target.ownerDocument || Y, o = i.documentElement, r = i.body, e.pageX = n.clientX + (o && o.scrollLeft || r && r.scrollLeft || 0) - (o && o.clientLeft || r && r.clientLeft || 0), e.pageY = n.clientY + (o && o.scrollTop || r && r.scrollTop || 0) - (o && o.clientTop || r && r.clientTop || 0)), !e.relatedTarget && s && (e.relatedTarget = s === e.target ? n.toElement : s), e.which || a === t || (e.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0), e
                }
            },
            special: {
                load: {
                    noBubble: !0
                },
                click: {
                    trigger: function() {
                        return ue.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : t
                    }
                },
                focus: {
                    trigger: function() {
                        if (this !== Y.activeElement && this.focus) try {
                            return this.focus(), !1
                        } catch (e) {}
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === Y.activeElement && this.blur ? (this.blur(), !1) : t
                    },
                    delegateType: "focusout"
                },
                beforeunload: {
                    postDispatch: function(e) {
                        e.result !== t && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function(e, t, n, r) {
                var i = ue.extend(new ue.Event, n, {
                    type: e,
                    isSimulated: !0,
                    originalEvent: {}
                });
                r ? ue.event.trigger(i, null, t) : ue.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
            }
        }, ue.removeEvent = Y.removeEventListener ? function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n, !1)
        } : function(e, t, n) {
            var r = "on" + t;
            e.detachEvent && (typeof e[r] === V && (e[r] = null), e.detachEvent(r, n))
        }, ue.Event = function(e, n) {
            return this instanceof ue.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? u : l) : this.type = e, n && ue.extend(this, n), this.timeStamp = e && e.timeStamp || ue.now(), this[ue.expando] = !0, t) : new ue.Event(e, n)
        }, ue.Event.prototype = {
            isDefaultPrevented: l,
            isPropagationStopped: l,
            isImmediatePropagationStopped: l,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = u, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = u, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
            },
            stopImmediatePropagation: function() {
                this.isImmediatePropagationStopped = u, this.stopPropagation()
            }
        }, ue.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        }, function(e, t) {
            ue.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var n, r = this,
                        i = e.relatedTarget,
                        o = e.handleObj;
                    return (!i || i !== r && !ue.contains(r, i)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
                }
            }
        }), ue.support.submitBubbles || (ue.event.special.submit = {
            setup: function() {
                return !ue.nodeName(this, "form") && (ue.event.add(this, "click._submit keypress._submit", function(e) {
                    var n = e.target,
                        r = ue.nodeName(n, "input") || ue.nodeName(n, "button") ? n.form : t;
                    r && !ue._data(r, "submitBubbles") && (ue.event.add(r, "submit._submit", function(e) {
                        e._submit_bubble = !0
                    }), ue._data(r, "submitBubbles", !0))
                }), t)
            },
            postDispatch: function(e) {
                e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && ue.event.simulate("submit", this.parentNode, e, !0))
            },
            teardown: function() {
                return !ue.nodeName(this, "form") && (ue.event.remove(this, "._submit"), t)
            }
        }), ue.support.changeBubbles || (ue.event.special.change = {
            setup: function() {
                return Fe.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ue.event.add(this, "propertychange._change", function(e) {
                    "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
                }), ue.event.add(this, "click._change", function(e) {
                    this._just_changed && !e.isTrigger && (this._just_changed = !1), ue.event.simulate("change", this, e, !0)
                })), !1) : (ue.event.add(this, "beforeactivate._change", function(e) {
                    var t = e.target;
                    Fe.test(t.nodeName) && !ue._data(t, "changeBubbles") && (ue.event.add(t, "change._change", function(e) {
                        !this.parentNode || e.isSimulated || e.isTrigger || ue.event.simulate("change", this.parentNode, e, !0)
                    }), ue._data(t, "changeBubbles", !0))
                }), t)
            },
            handle: function(e) {
                var n = e.target;
                return this !== n || e.isSimulated || e.isTrigger || "radio" !== n.type && "checkbox" !== n.type ? e.handleObj.handler.apply(this, arguments) : t
            },
            teardown: function() {
                return ue.event.remove(this, "._change"), !Fe.test(this.nodeName)
            }
        }), ue.support.focusinBubbles || ue.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var n = 0,
                r = function(e) {
                    ue.event.simulate(t, e.target, ue.event.fix(e), !0)
                };
            ue.event.special[t] = {
                setup: function() {
                    0 === n++ && Y.addEventListener(e, r, !0)
                },
                teardown: function() {
                    0 === --n && Y.removeEventListener(e, r, !0)
                }
            }
        }), ue.fn.extend({
            on: function(e, n, r, i, o) {
                var a, s;
                if ("object" == typeof e) {
                    "string" != typeof n && (r = r || n, n = t);
                    for (a in e) this.on(a, n, r, e[a], o);
                    return this
                }
                if (null == r && null == i ? (i = n, r = n = t) : null == i && ("string" == typeof n ? (i = r, r = t) : (i = r, r = n, n = t)), i === !1) i = l;
                else if (!i) return this;
                return 1 === o && (s = i, i = function(e) {
                    return ue().off(e), s.apply(this, arguments)
                }, i.guid = s.guid || (s.guid = ue.guid++)), this.each(function() {
                    ue.event.add(this, e, i, r, n)
                })
            },
            one: function(e, t, n, r) {
                return this.on(e, t, n, r, 1)
            },
            off: function(e, n, r) {
                var i, o;
                if (e && e.preventDefault && e.handleObj) return i = e.handleObj, ue(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
                if ("object" == typeof e) {
                    for (o in e) this.off(o, n, e[o]);
                    return this
                }
                return (n === !1 || "function" == typeof n) && (r = n, n = t), r === !1 && (r = l), this.each(function() {
                    ue.event.remove(this, e, r, n)
                })
            },
            bind: function(e, t, n) {
                return this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, n, r) {
                return this.on(t, e, n, r)
            },
            undelegate: function(e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            },
            trigger: function(e, t) {
                return this.each(function() {
                    ue.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, n) {
                var r = this[0];
                return r ? ue.event.trigger(e, n, r, !0) : t
            }
        }),
        function(e, t) {
            function n(e) {
                return he.test(e + "")
            }

            function r() {
                var e, t = [];
                return e = function(n, r) {
                    return t.push(n += " ") > C.cacheLength && delete e[t.shift()], e[n] = r
                }
            }

            function i(e) {
                return e[P] = !0, e
            }

            function o(e) {
                var t = L.createElement("div");
                try {
                    return e(t)
                } catch (e) {
                    return !1
                } finally {
                    t = null
                }
            }

            function a(e, t, n, r) {
                var i, o, a, s, u, l, c, d, h, g;
                if ((t ? t.ownerDocument || t : R) !== L && D(t), t = t || L, n = n || [], !e || "string" != typeof e) return n;
                if (1 !== (s = t.nodeType) && 9 !== s) return [];
                if (!q && !r) {
                    if (i = ge.exec(e))
                        if (a = i[1]) {
                            if (9 === s) {
                                if (o = t.getElementById(a), !o || !o.parentNode) return n;
                                if (o.id === a) return n.push(o), n
                            } else if (t.ownerDocument && (o = t.ownerDocument.getElementById(a)) && O(t, o) && o.id === a) return n.push(o), n
                        } else {
                            if (i[2]) return Q.apply(n, K.call(t.getElementsByTagName(e), 0)), n;
                            if ((a = i[3]) && W.getByClassName && t.getElementsByClassName) return Q.apply(n, K.call(t.getElementsByClassName(a), 0)), n
                        }
                    if (W.qsa && !M.test(e)) {
                        if (c = !0, d = P, h = t, g = 9 === s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                            for (l = f(e), (c = t.getAttribute("id")) ? d = c.replace(ve, "\\$&") : t.setAttribute("id", d), d = "[id='" + d + "'] ", u = l.length; u--;) l[u] = d + p(l[u]);
                            h = de.test(e) && t.parentNode || t, g = l.join(",")
                        }
                        if (g) try {
                            return Q.apply(n, K.call(h.querySelectorAll(g), 0)), n
                        } catch (e) {} finally {
                            c || t.removeAttribute("id")
                        }
                    }
                }
                return x(e.replace(ae, "$1"), t, n, r)
            }

            function s(e, t) {
                var n = t && e,
                    r = n && (~t.sourceIndex || Y) - (~e.sourceIndex || Y);
                if (r) return r;
                if (n)
                    for (; n = n.nextSibling;)
                        if (n === t) return -1;
                return e ? 1 : -1
            }

            function u(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return "input" === n && t.type === e
                }
            }

            function l(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && t.type === e
                }
            }

            function c(e) {
                return i(function(t) {
                    return t = +t, i(function(n, r) {
                        for (var i, o = e([], n.length, t), a = o.length; a--;) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                    })
                })
            }

            function f(e, t) {
                var n, r, i, o, s, u, l, c = X[e + " "];
                if (c) return t ? 0 : c.slice(0);
                for (s = e, u = [], l = C.preFilter; s;) {
                    (!n || (r = se.exec(s))) && (r && (s = s.slice(r[0].length) || s), u.push(i = [])), n = !1, (r = le.exec(s)) && (n = r.shift(), i.push({
                        value: n,
                        type: r[0].replace(ae, " ")
                    }), s = s.slice(n.length));
                    for (o in C.filter) !(r = pe[o].exec(s)) || l[o] && !(r = l[o](r)) || (n = r.shift(), i.push({
                        value: n,
                        type: o,
                        matches: r
                    }), s = s.slice(n.length));
                    if (!n) break
                }
                return t ? s.length : s ? a.error(e) : X(e, u).slice(0)
            }

            function p(e) {
                for (var t = 0, n = e.length, r = ""; n > t; t++) r += e[t].value;
                return r
            }

            function d(e, t, n) {
                var r = t.dir,
                    i = n && "parentNode" === r,
                    o = I++;
                return t.first ? function(t, n, o) {
                    for (; t = t[r];)
                        if (1 === t.nodeType || i) return e(t, n, o)
                } : function(t, n, a) {
                    var s, u, l, c = $ + " " + o;
                    if (a) {
                        for (; t = t[r];)
                            if ((1 === t.nodeType || i) && e(t, n, a)) return !0
                    } else
                        for (; t = t[r];)
                            if (1 === t.nodeType || i)
                                if (l = t[P] || (t[P] = {}), (u = l[r]) && u[0] === c) {
                                    if ((s = u[1]) === !0 || s === N) return s === !0
                                } else if (u = l[r] = [c], u[1] = e(t, n, a) || N, u[1] === !0) return !0
                }
            }

            function h(e) {
                return e.length > 1 ? function(t, n, r) {
                    for (var i = e.length; i--;)
                        if (!e[i](t, n, r)) return !1;
                    return !0
                } : e[0]
            }

            function g(e, t, n, r, i) {
                for (var o, a = [], s = 0, u = e.length, l = null != t; u > s; s++)(o = e[s]) && (!n || n(o, r, i)) && (a.push(o), l && t.push(s));
                return a
            }

            function m(e, t, n, r, o, a) {
                return r && !r[P] && (r = m(r)), o && !o[P] && (o = m(o, a)), i(function(i, a, s, u) {
                    var l, c, f, p = [],
                        d = [],
                        h = a.length,
                        m = i || b(t || "*", s.nodeType ? [s] : s, []),
                        y = !e || !i && t ? m : g(m, p, e, s, u),
                        v = n ? o || (i ? e : h || r) ? [] : a : y;
                    if (n && n(y, v, s, u), r)
                        for (l = g(v, d), r(l, [], s, u), c = l.length; c--;)(f = l[c]) && (v[d[c]] = !(y[d[c]] = f));
                    if (i) {
                        if (o || e) {
                            if (o) {
                                for (l = [], c = v.length; c--;)(f = v[c]) && l.push(y[c] = f);
                                o(null, v = [], l, u)
                            }
                            for (c = v.length; c--;)(f = v[c]) && (l = o ? Z.call(i, f) : p[c]) > -1 && (i[l] = !(a[l] = f))
                        }
                    } else v = g(v === a ? v.splice(h, v.length) : v), o ? o(null, a, v, u) : Q.apply(a, v)
                })
            }

            function y(e) {
                for (var t, n, r, i = e.length, o = C.relative[e[0].type], a = o || C.relative[" "], s = o ? 1 : 0, u = d(function(e) {
                        return e === t
                    }, a, !0), l = d(function(e) {
                        return Z.call(t, e) > -1
                    }, a, !0), c = [function(e, n, r) {
                        return !o && (r || n !== j) || ((t = n).nodeType ? u(e, n, r) : l(e, n, r))
                    }]; i > s; s++)
                    if (n = C.relative[e[s].type]) c = [d(h(c), n)];
                    else {
                        if (n = C.filter[e[s].type].apply(null, e[s].matches), n[P]) {
                            for (r = ++s; i > r && !C.relative[e[r].type]; r++);
                            return m(s > 1 && h(c), s > 1 && p(e.slice(0, s - 1)).replace(ae, "$1"), n, r > s && y(e.slice(s, r)), i > r && y(e = e.slice(r)), i > r && p(e))
                        }
                        c.push(n)
                    }
                return h(c)
            }

            function v(e, t) {
                var n = 0,
                    r = t.length > 0,
                    o = e.length > 0,
                    s = function(i, s, u, l, c) {
                        var f, p, d, h = [],
                            m = 0,
                            y = "0",
                            v = i && [],
                            b = null != c,
                            x = j,
                            T = i || o && C.find.TAG("*", c && s.parentNode || s),
                            w = $ += null == x ? 1 : Math.random() || .1;
                        for (b && (j = s !== L && s, N = n); null != (f = T[y]); y++) {
                            if (o && f) {
                                for (p = 0; d = e[p++];)
                                    if (d(f, s, u)) {
                                        l.push(f);
                                        break
                                    }
                                b && ($ = w, N = ++n)
                            }
                            r && ((f = !d && f) && m--, i && v.push(f))
                        }
                        if (m += y, r && y !== m) {
                            for (p = 0; d = t[p++];) d(v, h, s, u);
                            if (i) {
                                if (m > 0)
                                    for (; y--;) v[y] || h[y] || (h[y] = G.call(l));
                                h = g(h)
                            }
                            Q.apply(l, h), b && !i && h.length > 0 && m + t.length > 1 && a.uniqueSort(l)
                        }
                        return b && ($ = w, j = x), v
                    };
                return r ? i(s) : s
            }

            function b(e, t, n) {
                for (var r = 0, i = t.length; i > r; r++) a(e, t[r], n);
                return n
            }

            function x(e, t, n, r) {
                var i, o, a, s, u, l = f(e);
                if (!r && 1 === l.length) {
                    if (o = l[0] = l[0].slice(0), o.length > 2 && "ID" === (a = o[0]).type && 9 === t.nodeType && !q && C.relative[o[1].type]) {
                        if (t = C.find.ID(a.matches[0].replace(xe, Te), t)[0], !t) return n;
                        e = e.slice(o.shift().value.length)
                    }
                    for (i = pe.needsContext.test(e) ? 0 : o.length; i-- && (a = o[i], !C.relative[s = a.type]);)
                        if ((u = C.find[s]) && (r = u(a.matches[0].replace(xe, Te), de.test(o[0].type) && t.parentNode || t))) {
                            if (o.splice(i, 1), e = r.length && p(o), !e) return Q.apply(n, K.call(r, 0)), n;
                            break
                        }
                }
                return S(e, l)(r, t, q, n, de.test(e)), n
            }

            function T() {}
            var w, N, C, k, E, S, A, j, D, L, H, q, M, _, F, O, B, P = "sizzle" + -new Date,
                R = e.document,
                W = {},
                $ = 0,
                I = 0,
                z = r(),
                X = r(),
                U = r(),
                V = typeof t,
                Y = 1 << 31,
                J = [],
                G = J.pop,
                Q = J.push,
                K = J.slice,
                Z = J.indexOf || function(e) {
                    for (var t = 0, n = this.length; n > t; t++)
                        if (this[t] === e) return t;
                    return -1
                },
                ee = "[\\x20\\t\\r\\n\\f]",
                te = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                ne = te.replace("w", "w#"),
                re = "([*^$|!~]?=)",
                ie = "\\[" + ee + "*(" + te + ")" + ee + "*(?:" + re + ee + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + ne + ")|)|)" + ee + "*\\]",
                oe = ":(" + te + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + ie.replace(3, 8) + ")*)|.*)\\)|)",
                ae = RegExp("^" + ee + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ee + "+$", "g"),
                se = RegExp("^" + ee + "*," + ee + "*"),
                le = RegExp("^" + ee + "*([\\x20\\t\\r\\n\\f>+~])" + ee + "*"),
                ce = RegExp(oe),
                fe = RegExp("^" + ne + "$"),
                pe = {
                    ID: RegExp("^#(" + te + ")"),
                    CLASS: RegExp("^\\.(" + te + ")"),
                    NAME: RegExp("^\\[name=['\"]?(" + te + ")['\"]?\\]"),
                    TAG: RegExp("^(" + te.replace("w", "w*") + ")"),
                    ATTR: RegExp("^" + ie),
                    PSEUDO: RegExp("^" + oe),
                    CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ee + "*(even|odd|(([+-]|)(\\d*)n|)" + ee + "*(?:([+-]|)" + ee + "*(\\d+)|))" + ee + "*\\)|)", "i"),
                    needsContext: RegExp("^" + ee + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ee + "*((?:-\\d)?\\d*)" + ee + "*\\)|)(?=[^-]|$)", "i")
                },
                de = /[\x20\t\r\n\f]*[+~]/,
                he = /^[^{]+\{\s*\[native code/,
                ge = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                me = /^(?:input|select|textarea|button)$/i,
                ye = /^h\d$/i,
                ve = /'|\\/g,
                be = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
                xe = /\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,
                Te = function(e, t) {
                    var n = "0x" + t - 65536;
                    return n !== n ? t : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(55296 | n >> 10, 56320 | 1023 & n)
                };
            try {
                K.call(R.documentElement.childNodes, 0)[0].nodeType
            } catch (e) {
                K = function(e) {
                    for (var t, n = []; t = this[e++];) n.push(t);
                    return n
                }
            }
            E = a.isXML = function(e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return !!t && "HTML" !== t.nodeName
            }, D = a.setDocument = function(e) {
                var r = e ? e.ownerDocument || e : R;
                return r !== L && 9 === r.nodeType && r.documentElement ? (L = r, H = r.documentElement, q = E(r), W.tagNameNoComments = o(function(e) {
                    return e.appendChild(r.createComment("")), !e.getElementsByTagName("*").length
                }), W.attributes = o(function(e) {
                    e.innerHTML = "<select></select>";
                    var t = typeof e.lastChild.getAttribute("multiple");
                    return "boolean" !== t && "string" !== t
                }), W.getByClassName = o(function(e) {
                    return e.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>", !(!e.getElementsByClassName || !e.getElementsByClassName("e").length) && (e.lastChild.className = "e", 2 === e.getElementsByClassName("e").length)
                }), W.getByName = o(function(e) {
                    e.id = P + 0, e.innerHTML = "<a name='" + P + "'></a><div name='" + P + "'></div>", H.insertBefore(e, H.firstChild);
                    var t = r.getElementsByName && r.getElementsByName(P).length === 2 + r.getElementsByName(P + 0).length;
                    return W.getIdNotName = !r.getElementById(P), H.removeChild(e), t
                }), C.attrHandle = o(function(e) {
                    return e.innerHTML = "<a href='#'></a>", e.firstChild && typeof e.firstChild.getAttribute !== V && "#" === e.firstChild.getAttribute("href")
                }) ? {} : {
                    href: function(e) {
                        return e.getAttribute("href", 2)
                    },
                    type: function(e) {
                        return e.getAttribute("type")
                    }
                }, W.getIdNotName ? (C.find.ID = function(e, t) {
                    if (typeof t.getElementById !== V && !q) {
                        var n = t.getElementById(e);
                        return n && n.parentNode ? [n] : []
                    }
                }, C.filter.ID = function(e) {
                    var t = e.replace(xe, Te);
                    return function(e) {
                        return e.getAttribute("id") === t
                    }
                }) : (C.find.ID = function(e, n) {
                    if (typeof n.getElementById !== V && !q) {
                        var r = n.getElementById(e);
                        return r ? r.id === e || typeof r.getAttributeNode !== V && r.getAttributeNode("id").value === e ? [r] : t : []
                    }
                }, C.filter.ID = function(e) {
                    var t = e.replace(xe, Te);
                    return function(e) {
                        var n = typeof e.getAttributeNode !== V && e.getAttributeNode("id");
                        return n && n.value === t
                    }
                }), C.find.TAG = W.tagNameNoComments ? function(e, n) {
                    return typeof n.getElementsByTagName !== V ? n.getElementsByTagName(e) : t
                } : function(e, t) {
                    var n, r = [],
                        i = 0,
                        o = t.getElementsByTagName(e);
                    if ("*" === e) {
                        for (; n = o[i++];) 1 === n.nodeType && r.push(n);
                        return r
                    }
                    return o
                }, C.find.NAME = W.getByName && function(e, n) {
                    return typeof n.getElementsByName !== V ? n.getElementsByName(name) : t
                }, C.find.CLASS = W.getByClassName && function(e, n) {
                    return typeof n.getElementsByClassName === V || q ? t : n.getElementsByClassName(e)
                }, _ = [], M = [":focus"], (W.qsa = n(r.querySelectorAll)) && (o(function(e) {
                    e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || M.push("\\[" + ee + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"), e.querySelectorAll(":checked").length || M.push(":checked")
                }), o(function(e) {
                    e.innerHTML = "<input type='hidden' i=''/>", e.querySelectorAll("[i^='']").length && M.push("[*^$]=" + ee + "*(?:\"\"|'')"), e.querySelectorAll(":enabled").length || M.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), M.push(",.*:")
                })), (W.matchesSelector = n(F = H.matchesSelector || H.mozMatchesSelector || H.webkitMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && o(function(e) {
                    W.disconnectedMatch = F.call(e, "div"), F.call(e, "[s!='']:x"), _.push("!=", oe)
                }), M = RegExp(M.join("|")), _ = RegExp(_.join("|")), O = n(H.contains) || H.compareDocumentPosition ? function(e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e,
                        r = t && t.parentNode;
                    return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
                } : function(e, t) {
                    if (t)
                        for (; t = t.parentNode;)
                            if (t === e) return !0;
                    return !1
                }, B = H.compareDocumentPosition ? function(e, t) {
                    var n;
                    return e === t ? (A = !0, 0) : (n = t.compareDocumentPosition && e.compareDocumentPosition && e.compareDocumentPosition(t)) ? 1 & n || e.parentNode && 11 === e.parentNode.nodeType ? e === r || O(R, e) ? -1 : t === r || O(R, t) ? 1 : 0 : 4 & n ? -1 : 1 : e.compareDocumentPosition ? -1 : 1
                } : function(e, t) {
                    var n, i = 0,
                        o = e.parentNode,
                        a = t.parentNode,
                        u = [e],
                        l = [t];
                    if (e === t) return A = !0, 0;
                    if (!o || !a) return e === r ? -1 : t === r ? 1 : o ? -1 : a ? 1 : 0;
                    if (o === a) return s(e, t);
                    for (n = e; n = n.parentNode;) u.unshift(n);
                    for (n = t; n = n.parentNode;) l.unshift(n);
                    for (; u[i] === l[i];) i++;
                    return i ? s(u[i], l[i]) : u[i] === R ? -1 : l[i] === R ? 1 : 0
                }, A = !1, [0, 0].sort(B), W.detectDuplicates = A, L) : L
            }, a.matches = function(e, t) {
                return a(e, null, null, t)
            }, a.matchesSelector = function(e, t) {
                if ((e.ownerDocument || e) !== L && D(e), t = t.replace(be, "='$1']"), !(!W.matchesSelector || q || _ && _.test(t) || M.test(t))) try {
                    var n = F.call(e, t);
                    if (n || W.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
                } catch (e) {}
                return a(t, L, null, [e]).length > 0
            }, a.contains = function(e, t) {
                return (e.ownerDocument || e) !== L && D(e), O(e, t)
            }, a.attr = function(e, t) {
                var n;
                return (e.ownerDocument || e) !== L && D(e), q || (t = t.toLowerCase()), (n = C.attrHandle[t]) ? n(e) : q || W.attributes ? e.getAttribute(t) : ((n = e.getAttributeNode(t)) || e.getAttribute(t)) && e[t] === !0 ? t : n && n.specified ? n.value : null
            }, a.error = function(e) {
                throw Error("Syntax error, unrecognized expression: " + e)
            }, a.uniqueSort = function(e) {
                var t, n = [],
                    r = 1,
                    i = 0;
                if (A = !W.detectDuplicates, e.sort(B), A) {
                    for (; t = e[r]; r++) t === e[r - 1] && (i = n.push(r));
                    for (; i--;) e.splice(n[i], 1)
                }
                return e
            }, k = a.getText = function(e) {
                var t, n = "",
                    r = 0,
                    i = e.nodeType;
                if (i) {
                    if (1 === i || 9 === i || 11 === i) {
                        if ("string" == typeof e.textContent) return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) n += k(e)
                    } else if (3 === i || 4 === i) return e.nodeValue
                } else
                    for (; t = e[r]; r++) n += k(t);
                return n
            }, C = a.selectors = {
                cacheLength: 50,
                createPseudo: i,
                match: pe,
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(e) {
                        return e[1] = e[1].replace(xe, Te), e[3] = (e[4] || e[5] || "").replace(xe, Te), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function(e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || a.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && a.error(e[0]), e
                    },
                    PSEUDO: function(e) {
                        var t, n = !e[5] && e[2];
                        return pe.CHILD.test(e[0]) ? null : (e[4] ? e[2] = e[4] : n && ce.test(n) && (t = f(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(e) {
                        return "*" === e ? function() {
                            return !0
                        } : (e = e.replace(xe, Te).toLowerCase(), function(t) {
                            return t.nodeName && t.nodeName.toLowerCase() === e
                        })
                    },
                    CLASS: function(e) {
                        var t = z[e + " "];
                        return t || (t = RegExp("(^|" + ee + ")" + e + "(" + ee + "|$)")) && z(e, function(e) {
                            return t.test(e.className || typeof e.getAttribute !== V && e.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(e, t, n) {
                        return function(r) {
                            var i = a.attr(r, e);
                            return null == i ? "!=" === t : !t || (i += "", "=" === t ? i === n : "!=" === t ? i !== n : "^=" === t ? n && 0 === i.indexOf(n) : "*=" === t ? n && i.indexOf(n) > -1 : "$=" === t ? n && i.slice(-n.length) === n : "~=" === t ? (" " + i + " ").indexOf(n) > -1 : "|=" === t && (i === n || i.slice(0, n.length + 1) === n + "-"))
                        }
                    },
                    CHILD: function(e, t, n, r, i) {
                        var o = "nth" !== e.slice(0, 3),
                            a = "last" !== e.slice(-4),
                            s = "of-type" === t;
                        return 1 === r && 0 === i ? function(e) {
                            return !!e.parentNode
                        } : function(t, n, u) {
                            var l, c, f, p, d, h, g = o !== a ? "nextSibling" : "previousSibling",
                                m = t.parentNode,
                                y = s && t.nodeName.toLowerCase(),
                                v = !u && !s;
                            if (m) {
                                if (o) {
                                    for (; g;) {
                                        for (f = t; f = f[g];)
                                            if (s ? f.nodeName.toLowerCase() === y : 1 === f.nodeType) return !1;
                                        h = g = "only" === e && !h && "nextSibling"
                                    }
                                    return !0
                                }
                                if (h = [a ? m.firstChild : m.lastChild], a && v) {
                                    for (c = m[P] || (m[P] = {}), l = c[e] || [], d = l[0] === $ && l[1], p = l[0] === $ && l[2], f = d && m.childNodes[d]; f = ++d && f && f[g] || (p = d = 0) || h.pop();)
                                        if (1 === f.nodeType && ++p && f === t) {
                                            c[e] = [$, d, p];
                                            break
                                        }
                                } else if (v && (l = (t[P] || (t[P] = {}))[e]) && l[0] === $) p = l[1];
                                else
                                    for (;
                                        (f = ++d && f && f[g] || (p = d = 0) || h.pop()) && ((s ? f.nodeName.toLowerCase() !== y : 1 !== f.nodeType) || !++p || (v && ((f[P] || (f[P] = {}))[e] = [$, p]), f !== t)););
                                return p -= i, p === r || 0 === p % r && p / r >= 0
                            }
                        }
                    },
                    PSEUDO: function(e, t) {
                        var n, r = C.pseudos[e] || C.setFilters[e.toLowerCase()] || a.error("unsupported pseudo: " + e);
                        return r[P] ? r(t) : r.length > 1 ? (n = [e, e, "", t], C.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function(e, n) {
                            for (var i, o = r(e, t), a = o.length; a--;) i = Z.call(e, o[a]), e[i] = !(n[i] = o[a])
                        }) : function(e) {
                            return r(e, 0, n)
                        }) : r
                    }
                },
                pseudos: {
                    not: i(function(e) {
                        var t = [],
                            n = [],
                            r = S(e.replace(ae, "$1"));
                        return r[P] ? i(function(e, t, n, i) {
                            for (var o, a = r(e, null, i, []), s = e.length; s--;)(o = a[s]) && (e[s] = !(t[s] = o))
                        }) : function(e, i, o) {
                            return t[0] = e, r(t, null, o, n), !n.pop()
                        }
                    }),
                    has: i(function(e) {
                        return function(t) {
                            return a(e, t).length > 0
                        }
                    }),
                    contains: i(function(e) {
                        return function(t) {
                            return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
                        }
                    }),
                    lang: i(function(e) {
                        return fe.test(e || "") || a.error("unsupported lang: " + e), e = e.replace(xe, Te).toLowerCase(),
                            function(t) {
                                var n;
                                do
                                    if (n = q ? t.getAttribute("xml:lang") || t.getAttribute("lang") : t.lang) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    }),
                    target: function(t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id
                    },
                    root: function(e) {
                        return e === H
                    },
                    focus: function(e) {
                        return e === L.activeElement && (!L.hasFocus || L.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    enabled: function(e) {
                        return e.disabled === !1
                    },
                    disabled: function(e) {
                        return e.disabled === !0
                    },
                    checked: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    },
                    selected: function(e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    },
                    empty: function(e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeName > "@" || 3 === e.nodeType || 4 === e.nodeType) return !1;
                        return !0
                    },
                    parent: function(e) {
                        return !C.pseudos.empty(e)
                    },
                    header: function(e) {
                        return ye.test(e.nodeName)
                    },
                    input: function(e) {
                        return me.test(e.nodeName)
                    },
                    button: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function(e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || t.toLowerCase() === e.type)
                    },
                    first: c(function() {
                        return [0]
                    }),
                    last: c(function(e, t) {
                        return [t - 1]
                    }),
                    eq: c(function(e, t, n) {
                        return [0 > n ? n + t : n]
                    }),
                    even: c(function(e, t) {
                        for (var n = 0; t > n; n += 2) e.push(n);
                        return e
                    }),
                    odd: c(function(e, t) {
                        for (var n = 1; t > n; n += 2) e.push(n);
                        return e
                    }),
                    lt: c(function(e, t, n) {
                        for (var r = 0 > n ? n + t : n; --r >= 0;) e.push(r);
                        return e
                    }),
                    gt: c(function(e, t, n) {
                        for (var r = 0 > n ? n + t : n; t > ++r;) e.push(r);
                        return e
                    })
                }
            };
            for (w in {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) C.pseudos[w] = u(w);
            for (w in {
                    submit: !0,
                    reset: !0
                }) C.pseudos[w] = l(w);
            S = a.compile = function(e, t) {
                var n, r = [],
                    i = [],
                    o = U[e + " "];
                if (!o) {
                    for (t || (t = f(e)), n = t.length; n--;) o = y(t[n]), o[P] ? r.push(o) : i.push(o);
                    o = U(e, v(i, r))
                }
                return o
            }, C.pseudos.nth = C.pseudos.eq, C.filters = T.prototype = C.pseudos, C.setFilters = new T, D(), a.attr = ue.attr, ue.find = a, ue.expr = a.selectors, ue.expr[":"] = ue.expr.pseudos, ue.unique = a.uniqueSort, ue.text = a.getText, ue.isXMLDoc = a.isXML, ue.contains = a.contains
        }(e);
    var We = /Until$/,
        $e = /^(?:parents|prev(?:Until|All))/,
        Ie = /^.[^:#\[\.,]*$/,
        ze = ue.expr.match.needsContext,
        Xe = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    ue.fn.extend({
        find: function(e) {
            var t, n, r, i = this.length;
            if ("string" != typeof e) return r = this, this.pushStack(ue(e).filter(function() {
                for (t = 0; i > t; t++)
                    if (ue.contains(r[t], this)) return !0
            }));
            for (n = [], t = 0; i > t; t++) ue.find(e, this[t], n);
            return n = this.pushStack(i > 1 ? ue.unique(n) : n), n.selector = (this.selector ? this.selector + " " : "") + e, n
        },
        has: function(e) {
            var t, n = ue(e, this),
                r = n.length;
            return this.filter(function() {
                for (t = 0; r > t; t++)
                    if (ue.contains(this, n[t])) return !0
            })
        },
        not: function(e) {
            return this.pushStack(f(this, e, !1))
        },
        filter: function(e) {
            return this.pushStack(f(this, e, !0))
        },
        is: function(e) {
            return !!e && ("string" == typeof e ? ze.test(e) ? ue(e, this.context).index(this[0]) >= 0 : ue.filter(e, this).length > 0 : this.filter(e).length > 0)
        },
        closest: function(e, t) {
            for (var n, r = 0, i = this.length, o = [], a = ze.test(e) || "string" != typeof e ? ue(e, t || this.context) : 0; i > r; r++)
                for (n = this[r]; n && n.ownerDocument && n !== t && 11 !== n.nodeType;) {
                    if (a ? a.index(n) > -1 : ue.find.matchesSelector(n, e)) {
                        o.push(n);
                        break
                    }
                    n = n.parentNode
                }
            return this.pushStack(o.length > 1 ? ue.unique(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? ue.inArray(this[0], ue(e)) : ue.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            var n = "string" == typeof e ? ue(e, t) : ue.makeArray(e && e.nodeType ? [e] : e),
                r = ue.merge(this.get(), n);
            return this.pushStack(ue.unique(r))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), ue.fn.andSelf = ue.fn.addBack, ue.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return ue.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return ue.dir(e, "parentNode", n)
        },
        next: function(e) {
            return c(e, "nextSibling")
        },
        prev: function(e) {
            return c(e, "previousSibling")
        },
        nextAll: function(e) {
            return ue.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return ue.dir(e, "previousSibling");
        },
        nextUntil: function(e, t, n) {
            return ue.dir(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return ue.dir(e, "previousSibling", n)
        },
        siblings: function(e) {
            return ue.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return ue.sibling(e.firstChild)
        },
        contents: function(e) {
            return ue.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ue.merge([], e.childNodes)
        }
    }, function(e, t) {
        ue.fn[e] = function(n, r) {
            var i = ue.map(this, t, n);
            return We.test(e) || (r = n), r && "string" == typeof r && (i = ue.filter(r, i)), i = this.length > 1 && !Xe[e] ? ue.unique(i) : i, this.length > 1 && $e.test(e) && (i = i.reverse()), this.pushStack(i)
        }
    }), ue.extend({
        filter: function(e, t, n) {
            return n && (e = ":not(" + e + ")"), 1 === t.length ? ue.find.matchesSelector(t[0], e) ? [t[0]] : [] : ue.find.matches(e, t)
        },
        dir: function(e, n, r) {
            for (var i = [], o = e[n]; o && 9 !== o.nodeType && (r === t || 1 !== o.nodeType || !ue(o).is(r));) 1 === o.nodeType && i.push(o), o = o[n];
            return i
        },
        sibling: function(e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }
    });
    var Ue = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Ve = / jQuery\d+="(?:null|\d+)"/g,
        Ye = RegExp("<(?:" + Ue + ")[\\s/>]", "i"),
        Je = /^\s+/,
        Ge = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Qe = /<([\w:]+)/,
        Ke = /<tbody/i,
        Ze = /<|&#?\w+;/,
        et = /<(?:script|style|link)/i,
        tt = /^(?:checkbox|radio)$/i,
        nt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        rt = /^$|\/(?:java|ecma)script/i,
        it = /^true\/(.*)/,
        ot = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        at = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ue.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        },
        st = p(Y),
        ut = st.appendChild(Y.createElement("div"));
    at.optgroup = at.option, at.tbody = at.tfoot = at.colgroup = at.caption = at.thead, at.th = at.td, ue.fn.extend({
        text: function(e) {
            return ue.access(this, function(e) {
                return e === t ? ue.text(this) : this.empty().append((this[0] && this[0].ownerDocument || Y).createTextNode(e))
            }, null, e, arguments.length)
        },
        wrapAll: function(e) {
            if (ue.isFunction(e)) return this.each(function(t) {
                ue(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = ue(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return ue.isFunction(e) ? this.each(function(t) {
                ue(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = ue(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = ue.isFunction(e);
            return this.each(function(n) {
                ue(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                ue.nodeName(this, "body") || ue(this).replaceWith(this.childNodes)
            }).end()
        },
        append: function() {
            return this.domManip(arguments, !0, function(e) {
                (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && this.appendChild(e)
            })
        },
        prepend: function() {
            return this.domManip(arguments, !0, function(e) {
                (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && this.insertBefore(e, this.firstChild)
            })
        },
        before: function() {
            return this.domManip(arguments, !1, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, !1, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            for (var n, r = 0; null != (n = this[r]); r++)(!e || ue.filter(e, [n]).length > 0) && (t || 1 !== n.nodeType || ue.cleanData(b(n)), n.parentNode && (t && ue.contains(n.ownerDocument, n) && m(b(n, "script")), n.parentNode.removeChild(n)));
            return this
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && ue.cleanData(b(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                e.options && ue.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function() {
                return ue.clone(this, e, t)
            })
        },
        html: function(e) {
            return ue.access(this, function(e) {
                var n = this[0] || {},
                    r = 0,
                    i = this.length;
                if (e === t) return 1 === n.nodeType ? n.innerHTML.replace(Ve, "") : t;
                if (!("string" != typeof e || et.test(e) || !ue.support.htmlSerialize && Ye.test(e) || !ue.support.leadingWhitespace && Je.test(e) || at[(Qe.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(Ge, "<$1></$2>");
                    try {
                        for (; i > r; r++) n = this[r] || {}, 1 === n.nodeType && (ue.cleanData(b(n, !1)), n.innerHTML = e);
                        n = 0
                    } catch (e) {}
                }
                n && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function(e) {
            var t = ue.isFunction(e);
            return t || "string" == typeof e || (e = ue(e).not(this).detach()), this.domManip([e], !0, function(e) {
                var t = this.nextSibling,
                    n = this.parentNode;
                n && (ue(this).remove(), n.insertBefore(e, t))
            })
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, n, r) {
            e = te.apply([], e);
            var i, o, a, s, u, l, c = 0,
                f = this.length,
                p = this,
                m = f - 1,
                y = e[0],
                v = ue.isFunction(y);
            if (v || !(1 >= f || "string" != typeof y || ue.support.checkClone) && nt.test(y)) return this.each(function(i) {
                var o = p.eq(i);
                v && (e[0] = y.call(this, i, n ? o.html() : t)), o.domManip(e, n, r)
            });
            if (f && (l = ue.buildFragment(e, this[0].ownerDocument, !1, this), i = l.firstChild, 1 === l.childNodes.length && (l = i), i)) {
                for (n = n && ue.nodeName(i, "tr"), s = ue.map(b(l, "script"), h), a = s.length; f > c; c++) o = l, c !== m && (o = ue.clone(o, !0, !0), a && ue.merge(s, b(o, "script"))), r.call(n && ue.nodeName(this[c], "table") ? d(this[c], "tbody") : this[c], o, c);
                if (a)
                    for (u = s[s.length - 1].ownerDocument, ue.map(s, g), c = 0; a > c; c++) o = s[c], rt.test(o.type || "") && !ue._data(o, "globalEval") && ue.contains(u, o) && (o.src ? ue.ajax({
                        url: o.src,
                        type: "GET",
                        dataType: "script",
                        async: !1,
                        global: !1,
                        throws: !0
                    }) : ue.globalEval((o.text || o.textContent || o.innerHTML || "").replace(ot, "")));
                l = i = null
            }
            return this
        }
    }), ue.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        ue.fn[e] = function(e) {
            for (var n, r = 0, i = [], o = ue(e), a = o.length - 1; a >= r; r++) n = r === a ? this : this.clone(!0), ue(o[r])[t](n), ne.apply(i, n.get());
            return this.pushStack(i)
        }
    }), ue.extend({
        clone: function(e, t, n) {
            var r, i, o, a, s, u = ue.contains(e.ownerDocument, e);
            if (ue.support.html5Clone || ue.isXMLDoc(e) || !Ye.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (ut.innerHTML = e.outerHTML, ut.removeChild(o = ut.firstChild)), !(ue.support.noCloneEvent && ue.support.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ue.isXMLDoc(e)))
                for (r = b(o), s = b(e), a = 0; null != (i = s[a]); ++a) r[a] && v(i, r[a]);
            if (t)
                if (n)
                    for (s = s || b(e), r = r || b(o), a = 0; null != (i = s[a]); a++) y(i, r[a]);
                else y(e, o);
            return r = b(o, "script"), r.length > 0 && m(r, !u && b(e, "script")), r = s = i = null, o
        },
        buildFragment: function(e, t, n, r) {
            for (var i, o, a, s, u, l, c, f = e.length, d = p(t), h = [], g = 0; f > g; g++)
                if (o = e[g], o || 0 === o)
                    if ("object" === ue.type(o)) ue.merge(h, o.nodeType ? [o] : o);
                    else if (Ze.test(o)) {
                for (s = s || d.appendChild(t.createElement("div")), u = (Qe.exec(o) || ["", ""])[1].toLowerCase(), c = at[u] || at._default, s.innerHTML = c[1] + o.replace(Ge, "<$1></$2>") + c[2], i = c[0]; i--;) s = s.lastChild;
                if (!ue.support.leadingWhitespace && Je.test(o) && h.push(t.createTextNode(Je.exec(o)[0])), !ue.support.tbody)
                    for (o = "table" !== u || Ke.test(o) ? "<table>" !== c[1] || Ke.test(o) ? 0 : s : s.firstChild, i = o && o.childNodes.length; i--;) ue.nodeName(l = o.childNodes[i], "tbody") && !l.childNodes.length && o.removeChild(l);
                for (ue.merge(h, s.childNodes), s.textContent = ""; s.firstChild;) s.removeChild(s.firstChild);
                s = d.lastChild
            } else h.push(t.createTextNode(o));
            for (s && d.removeChild(s), ue.support.appendChecked || ue.grep(b(h, "input"), x), g = 0; o = h[g++];)
                if ((!r || -1 === ue.inArray(o, r)) && (a = ue.contains(o.ownerDocument, o), s = b(d.appendChild(o), "script"), a && m(s), n))
                    for (i = 0; o = s[i++];) rt.test(o.type || "") && n.push(o);
            return s = null, d
        },
        cleanData: function(e, t) {
            for (var n, r, i, o, a = 0, s = ue.expando, u = ue.cache, l = ue.support.deleteExpando, c = ue.event.special; null != (n = e[a]); a++)
                if ((t || ue.acceptData(n)) && (i = n[s], o = i && u[i])) {
                    if (o.events)
                        for (r in o.events) c[r] ? ue.event.remove(n, r) : ue.removeEvent(n, r, o.handle);
                    u[i] && (delete u[i], l ? delete n[s] : typeof n.removeAttribute !== V ? n.removeAttribute(s) : n[s] = null, Z.push(i))
                }
        }
    });
    var lt, ct, ft, pt = /alpha\([^)]*\)/i,
        dt = /opacity\s*=\s*([^)]*)/,
        ht = /^(top|right|bottom|left)$/,
        gt = /^(none|table(?!-c[ea]).+)/,
        mt = /^margin/,
        yt = RegExp("^(" + le + ")(.*)$", "i"),
        vt = RegExp("^(" + le + ")(?!px)[a-z%]+$", "i"),
        bt = RegExp("^([+-])=(" + le + ")", "i"),
        xt = {
            BODY: "block"
        },
        Tt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        wt = {
            letterSpacing: 0,
            fontWeight: 400
        },
        Nt = ["Top", "Right", "Bottom", "Left"],
        Ct = ["Webkit", "O", "Moz", "ms"];
    ue.fn.extend({
        css: function(e, n) {
            return ue.access(this, function(e, n, r) {
                var i, o, a = {},
                    s = 0;
                if (ue.isArray(n)) {
                    for (o = ct(e), i = n.length; i > s; s++) a[n[s]] = ue.css(e, n[s], !1, o);
                    return a
                }
                return r !== t ? ue.style(e, n, r) : ue.css(e, n)
            }, e, n, arguments.length > 1)
        },
        show: function() {
            return N(this, !0)
        },
        hide: function() {
            return N(this)
        },
        toggle: function(e) {
            var t = "boolean" == typeof e;
            return this.each(function() {
                (t ? e : w(this)) ? ue(this).show(): ue(this).hide()
            })
        }
    }), ue.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = ft(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: ue.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, n, r, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, a, s, u = ue.camelCase(n),
                    l = e.style;
                if (n = ue.cssProps[u] || (ue.cssProps[u] = T(l, u)), s = ue.cssHooks[n] || ue.cssHooks[u], r === t) return s && "get" in s && (o = s.get(e, !1, i)) !== t ? o : l[n];
                if (a = typeof r, "string" === a && (o = bt.exec(r)) && (r = (o[1] + 1) * o[2] + parseFloat(ue.css(e, n)), a = "number"), !(null == r || "number" === a && isNaN(r) || ("number" !== a || ue.cssNumber[u] || (r += "px"), ue.support.clearCloneStyle || "" !== r || 0 !== n.indexOf("background") || (l[n] = "inherit"), s && "set" in s && (r = s.set(e, r, i)) === t))) try {
                    l[n] = r
                } catch (e) {}
            }
        },
        css: function(e, n, r, i) {
            var o, a, s, u = ue.camelCase(n);
            return n = ue.cssProps[u] || (ue.cssProps[u] = T(e.style, u)), s = ue.cssHooks[n] || ue.cssHooks[u], s && "get" in s && (a = s.get(e, !0, r)), a === t && (a = ft(e, n, i)), "normal" === a && n in wt && (a = wt[n]), "" === r || r ? (o = parseFloat(a), r === !0 || ue.isNumeric(o) ? o || 0 : a) : a
        },
        swap: function(e, t, n, r) {
            var i, o, a = {};
            for (o in t) a[o] = e.style[o], e.style[o] = t[o];
            i = n.apply(e, r || []);
            for (o in t) e.style[o] = a[o];
            return i
        }
    }), e.getComputedStyle ? (ct = function(t) {
        return e.getComputedStyle(t, null)
    }, ft = function(e, n, r) {
        var i, o, a, s = r || ct(e),
            u = s ? s.getPropertyValue(n) || s[n] : t,
            l = e.style;
        return s && ("" !== u || ue.contains(e.ownerDocument, e) || (u = ue.style(e, n)), vt.test(u) && mt.test(n) && (i = l.width, o = l.minWidth, a = l.maxWidth, l.minWidth = l.maxWidth = l.width = u, u = s.width, l.width = i, l.minWidth = o, l.maxWidth = a)), u
    }) : Y.documentElement.currentStyle && (ct = function(e) {
        return e.currentStyle
    }, ft = function(e, n, r) {
        var i, o, a, s = r || ct(e),
            u = s ? s[n] : t,
            l = e.style;
        return null == u && l && l[n] && (u = l[n]), vt.test(u) && !ht.test(n) && (i = l.left, o = e.runtimeStyle, a = o && o.left, a && (o.left = e.currentStyle.left), l.left = "fontSize" === n ? "1em" : u, u = l.pixelLeft + "px", l.left = i, a && (o.left = a)), "" === u ? "auto" : u
    }), ue.each(["height", "width"], function(e, n) {
        ue.cssHooks[n] = {
            get: function(e, r, i) {
                return r ? 0 === e.offsetWidth && gt.test(ue.css(e, "display")) ? ue.swap(e, Tt, function() {
                    return E(e, n, i)
                }) : E(e, n, i) : t
            },
            set: function(e, t, r) {
                var i = r && ct(e);
                return C(e, t, r ? k(e, n, r, ue.support.boxSizing && "border-box" === ue.css(e, "boxSizing", !1, i), i) : 0)
            }
        }
    }), ue.support.opacity || (ue.cssHooks.opacity = {
        get: function(e, t) {
            return dt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var n = e.style,
                r = e.currentStyle,
                i = ue.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                o = r && r.filter || n.filter || "";
            n.zoom = 1, (t >= 1 || "" === t) && "" === ue.trim(o.replace(pt, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || r && !r.filter) || (n.filter = pt.test(o) ? o.replace(pt, i) : o + " " + i)
        }
    }), ue(function() {
        ue.support.reliableMarginRight || (ue.cssHooks.marginRight = {
            get: function(e, n) {
                return n ? ue.swap(e, {
                    display: "inline-block"
                }, ft, [e, "marginRight"]) : t
            }
        }), !ue.support.pixelPosition && ue.fn.position && ue.each(["top", "left"], function(e, n) {
            ue.cssHooks[n] = {
                get: function(e, r) {
                    return r ? (r = ft(e, n), vt.test(r) ? ue(e).position()[n] + "px" : r) : t
                }
            }
        })
    }), ue.expr && ue.expr.filters && (ue.expr.filters.hidden = function(e) {
        return 0 >= e.offsetWidth && 0 >= e.offsetHeight || !ue.support.reliableHiddenOffsets && "none" === (e.style && e.style.display || ue.css(e, "display"))
    }, ue.expr.filters.visible = function(e) {
        return !ue.expr.filters.hidden(e)
    }), ue.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        ue.cssHooks[e + t] = {
            expand: function(n) {
                for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > r; r++) i[e + Nt[r] + t] = o[r] || o[r - 2] || o[0];
                return i
            }
        }, mt.test(e) || (ue.cssHooks[e + t].set = C)
    });
    var kt = /%20/g,
        Et = /\[\]$/,
        St = /\r?\n/g,
        At = /^(?:submit|button|image|reset|file)$/i,
        jt = /^(?:input|select|textarea|keygen)/i;
    ue.fn.extend({
        serialize: function() {
            return ue.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = ue.prop(this, "elements");
                return e ? ue.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !ue(this).is(":disabled") && jt.test(this.nodeName) && !At.test(e) && (this.checked || !tt.test(e))
            }).map(function(e, t) {
                var n = ue(this).val();
                return null == n ? null : ue.isArray(n) ? ue.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(St, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(St, "\r\n")
                }
            }).get()
        }
    }), ue.param = function(e, n) {
        var r, i = [],
            o = function(e, t) {
                t = ue.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (n === t && (n = ue.ajaxSettings && ue.ajaxSettings.traditional), ue.isArray(e) || e.jquery && !ue.isPlainObject(e)) ue.each(e, function() {
            o(this.name, this.value)
        });
        else
            for (r in e) j(r, e[r], n, o);
        return i.join("&").replace(kt, "+")
    }, ue.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        ue.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), ue.fn.hover = function(e, t) {
        return this.mouseenter(e).mouseleave(t || e)
    };
    var Dt, Lt, Ht = ue.now(),
        qt = /\?/,
        Mt = /#.*$/,
        _t = /([?&])_=[^&]*/,
        Ft = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Ot = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Bt = /^(?:GET|HEAD)$/,
        Pt = /^\/\//,
        Rt = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        Wt = ue.fn.load,
        $t = {},
        It = {},
        zt = "*/".concat("*");
    try {
        Lt = J.href
    } catch (e) {
        Lt = Y.createElement("a"), Lt.href = "", Lt = Lt.href
    }
    Dt = Rt.exec(Lt.toLowerCase()) || [], ue.fn.load = function(e, n, r) {
        if ("string" != typeof e && Wt) return Wt.apply(this, arguments);
        var i, o, a, s = this,
            u = e.indexOf(" ");
        return u >= 0 && (i = e.slice(u, e.length), e = e.slice(0, u)), ue.isFunction(n) ? (r = n, n = t) : n && "object" == typeof n && (a = "POST"), s.length > 0 && ue.ajax({
            url: e,
            type: a,
            dataType: "html",
            data: n
        }).done(function(e) {
            o = arguments, s.html(i ? ue("<div>").append(ue.parseHTML(e)).find(i) : e)
        }).complete(r && function(e, t) {
            s.each(r, o || [e.responseText, t, e])
        }), this
    }, ue.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        ue.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), ue.each(["get", "post"], function(e, n) {
        ue[n] = function(e, r, i, o) {
            return ue.isFunction(r) && (o = o || i, i = r, r = t), ue.ajax({
                url: e,
                type: n,
                dataType: o,
                data: r,
                success: i
            })
        }
    }), ue.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Lt,
            type: "GET",
            isLocal: Ot.test(Dt[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": zt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },
            converters: {
                "* text": e.String,
                "text html": !0,
                "text json": ue.parseJSON,
                "text xml": ue.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? H(H(e, ue.ajaxSettings), t) : H(ue.ajaxSettings, e)
        },
        ajaxPrefilter: D($t),
        ajaxTransport: D(It),
        ajax: function(e, n) {
            function r(e, n, r, i) {
                var o, f, v, b, T, N = n;
                2 !== x && (x = 2, u && clearTimeout(u), c = t, s = i || "", w.readyState = e > 0 ? 4 : 0, r && (b = q(p, w, r)), e >= 200 && 300 > e || 304 === e ? (p.ifModified && (T = w.getResponseHeader("Last-Modified"), T && (ue.lastModified[a] = T), T = w.getResponseHeader("etag"), T && (ue.etag[a] = T)), 204 === e ? (o = !0, N = "nocontent") : 304 === e ? (o = !0, N = "notmodified") : (o = M(p, b), N = o.state, f = o.data, v = o.error, o = !v)) : (v = N, (e || !N) && (N = "error", 0 > e && (e = 0))), w.status = e, w.statusText = (n || N) + "", o ? g.resolveWith(d, [f, N, w]) : g.rejectWith(d, [w, N, v]), w.statusCode(y), y = t, l && h.trigger(o ? "ajaxSuccess" : "ajaxError", [w, p, o ? f : v]), m.fireWith(d, [w, N]), l && (h.trigger("ajaxComplete", [w, p]), --ue.active || ue.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (n = e, e = t), n = n || {};
            var i, o, a, s, u, l, c, f, p = ue.ajaxSetup({}, n),
                d = p.context || p,
                h = p.context && (d.nodeType || d.jquery) ? ue(d) : ue.event,
                g = ue.Deferred(),
                m = ue.Callbacks("once memory"),
                y = p.statusCode || {},
                v = {},
                b = {},
                x = 0,
                T = "canceled",
                w = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === x) {
                            if (!f)
                                for (f = {}; t = Ft.exec(s);) f[t[1].toLowerCase()] = t[2];
                            t = f[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === x ? s : null
                    },
                    setRequestHeader: function(e, t) {
                        var n = e.toLowerCase();
                        return x || (e = b[n] = b[n] || e, v[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return x || (p.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (2 > x)
                                for (t in e) y[t] = [y[t], e[t]];
                            else w.always(e[w.status]);
                        return this
                    },
                    abort: function(e) {
                        var t = e || T;
                        return c && c.abort(t), r(0, t), this
                    }
                };
            if (g.promise(w).complete = m.add, w.success = w.done, w.error = w.fail, p.url = ((e || p.url || Lt) + "").replace(Mt, "").replace(Pt, Dt[1] + "//"), p.type = n.method || n.type || p.method || p.type, p.dataTypes = ue.trim(p.dataType || "*").toLowerCase().match(ce) || [""], null == p.crossDomain && (i = Rt.exec(p.url.toLowerCase()), p.crossDomain = !(!i || i[1] === Dt[1] && i[2] === Dt[2] && (i[3] || ("http:" === i[1] ? 80 : 443)) == (Dt[3] || ("http:" === Dt[1] ? 80 : 443)))), p.data && p.processData && "string" != typeof p.data && (p.data = ue.param(p.data, p.traditional)), L($t, p, n, w), 2 === x) return w;
            l = p.global, l && 0 === ue.active++ && ue.event.trigger("ajaxStart"), p.type = p.type.toUpperCase(), p.hasContent = !Bt.test(p.type), a = p.url, p.hasContent || (p.data && (a = p.url += (qt.test(a) ? "&" : "?") + p.data, delete p.data), p.cache === !1 && (p.url = _t.test(a) ? a.replace(_t, "$1_=" + Ht++) : a + (qt.test(a) ? "&" : "?") + "_=" + Ht++)), p.ifModified && (ue.lastModified[a] && w.setRequestHeader("If-Modified-Since", ue.lastModified[a]), ue.etag[a] && w.setRequestHeader("If-None-Match", ue.etag[a])), (p.data && p.hasContent && p.contentType !== !1 || n.contentType) && w.setRequestHeader("Content-Type", p.contentType), w.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + zt + "; q=0.01" : "") : p.accepts["*"]);
            for (o in p.headers) w.setRequestHeader(o, p.headers[o]);
            if (p.beforeSend && (p.beforeSend.call(d, w, p) === !1 || 2 === x)) return w.abort();
            T = "abort";
            for (o in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) w[o](p[o]);
            if (c = L(It, p, n, w)) {
                w.readyState = 1, l && h.trigger("ajaxSend", [w, p]), p.async && p.timeout > 0 && (u = setTimeout(function() {
                    w.abort("timeout")
                }, p.timeout));
                try {
                    x = 1, c.send(v, r)
                } catch (e) {
                    if (!(2 > x)) throw e;
                    r(-1, e)
                }
            } else r(-1, "No Transport");
            return w
        },
        getScript: function(e, n) {
            return ue.get(e, t, n, "script")
        },
        getJSON: function(e, t, n) {
            return ue.get(e, t, n, "json")
        }
    }), ue.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return ue.globalEval(e), e
            }
        }
    }), ue.ajaxPrefilter("script", function(e) {
        e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), ue.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var n, r = Y.head || ue("head")[0] || Y.documentElement;
            return {
                send: function(t, i) {
                    n = Y.createElement("script"), n.async = !0, e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function(e, t) {
                        (t || !n.readyState || /loaded|complete/.test(n.readyState)) && (n.onload = n.onreadystatechange = null, n.parentNode && n.parentNode.removeChild(n), n = null, t || i(200, "success"))
                    }, r.insertBefore(n, r.firstChild)
                },
                abort: function() {
                    n && n.onload(t, !0)
                }
            }
        }
    });
    var Xt = [],
        Ut = /(=)\?(?=&|$)|\?\?/;
    ue.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = Xt.pop() || ue.expando + "_" + Ht++;
            return this[e] = !0, e
        }
    }), ue.ajaxPrefilter("json jsonp", function(n, r, i) {
        var o, a, s, u = n.jsonp !== !1 && (Ut.test(n.url) ? "url" : "string" == typeof n.data && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Ut.test(n.data) && "data");
        return u || "jsonp" === n.dataTypes[0] ? (o = n.jsonpCallback = ue.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, u ? n[u] = n[u].replace(Ut, "$1" + o) : n.jsonp !== !1 && (n.url += (qt.test(n.url) ? "&" : "?") + n.jsonp + "=" + o), n.converters["script json"] = function() {
            return s || ue.error(o + " was not called"), s[0]
        }, n.dataTypes[0] = "json", a = e[o], e[o] = function() {
            s = arguments
        }, i.always(function() {
            e[o] = a, n[o] && (n.jsonpCallback = r.jsonpCallback, Xt.push(o)), s && ue.isFunction(a) && a(s[0]), s = a = t
        }), "script") : t
    });
    var Vt, Yt, Jt = 0,
        Gt = e.ActiveXObject && function() {
            var e;
            for (e in Vt) Vt[e](t, !0)
        };
    ue.ajaxSettings.xhr = e.ActiveXObject ? function() {
        return !this.isLocal && _() || F()
    } : _, Yt = ue.ajaxSettings.xhr(), ue.support.cors = !!Yt && "withCredentials" in Yt, Yt = ue.support.ajax = !!Yt, Yt && ue.ajaxTransport(function(n) {
        if (!n.crossDomain || ue.support.cors) {
            var r;
            return {
                send: function(i, o) {
                    var a, s, u = n.xhr();
                    if (n.username ? u.open(n.type, n.url, n.async, n.username, n.password) : u.open(n.type, n.url, n.async), n.xhrFields)
                        for (s in n.xhrFields) u[s] = n.xhrFields[s];
                    n.mimeType && u.overrideMimeType && u.overrideMimeType(n.mimeType), n.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
                    try {
                        for (s in i) u.setRequestHeader(s, i[s])
                    } catch (e) {}
                    u.send(n.hasContent && n.data || null), r = function(e, i) {
                        var s, l, c, f;
                        try {
                            if (r && (i || 4 === u.readyState))
                                if (r = t, a && (u.onreadystatechange = ue.noop, Gt && delete Vt[a]), i) 4 !== u.readyState && u.abort();
                                else {
                                    f = {}, s = u.status, l = u.getAllResponseHeaders(), "string" == typeof u.responseText && (f.text = u.responseText);
                                    try {
                                        c = u.statusText
                                    } catch (e) {
                                        c = ""
                                    }
                                    s || !n.isLocal || n.crossDomain ? 1223 === s && (s = 204) : s = f.text ? 200 : 404
                                }
                        } catch (e) {
                            i || o(-1, e)
                        }
                        f && o(s, c, f, l)
                    }, n.async ? 4 === u.readyState ? setTimeout(r) : (a = ++Jt, Gt && (Vt || (Vt = {}, ue(e).unload(Gt)), Vt[a] = r), u.onreadystatechange = r) : r()
                },
                abort: function() {
                    r && r(t, !0)
                }
            }
        }
    });
    var Qt, Kt, Zt = /^(?:toggle|show|hide)$/,
        en = RegExp("^(?:([+-])=|)(" + le + ")([a-z%]*)$", "i"),
        tn = /queueHooks$/,
        nn = [W],
        rn = {
            "*": [function(e, t) {
                var n, r, i = this.createTween(e, t),
                    o = en.exec(t),
                    a = i.cur(),
                    s = +a || 0,
                    u = 1,
                    l = 20;
                if (o) {
                    if (n = +o[2], r = o[3] || (ue.cssNumber[e] ? "" : "px"), "px" !== r && s) {
                        s = ue.css(i.elem, e, !0) || n || 1;
                        do u = u || ".5", s /= u, ue.style(i.elem, e, s + r); while (u !== (u = i.cur() / a) && 1 !== u && --l)
                    }
                    i.unit = r, i.start = s, i.end = o[1] ? s + (o[1] + 1) * n : n
                }
                return i
            }]
        };
    ue.Animation = ue.extend(P, {
        tweener: function(e, t) {
            ue.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            for (var n, r = 0, i = e.length; i > r; r++) n = e[r], rn[n] = rn[n] || [], rn[n].unshift(t)
        },
        prefilter: function(e, t) {
            t ? nn.unshift(e) : nn.push(e)
        }
    }), ue.Tween = $, $.prototype = {
        constructor: $,
        init: function(e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (ue.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = $.propHooks[this.prop];
            return e && e.get ? e.get(this) : $.propHooks._default.get(this)
        },
        run: function(e) {
            var t, n = $.propHooks[this.prop];
            return this.pos = t = this.options.duration ? ue.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : $.propHooks._default.set(this), this
        }
    }, $.prototype.init.prototype = $.prototype, $.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = ue.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                ue.fx.step[e.prop] ? ue.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[ue.cssProps[e.prop]] || ue.cssHooks[e.prop]) ? ue.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, $.propHooks.scrollTop = $.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, ue.each(["toggle", "show", "hide"], function(e, t) {
        var n = ue.fn[t];
        ue.fn[t] = function(e, r, i) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(I(t, !0), e, r, i)
        }
    }), ue.fn.extend({
        fadeTo: function(e, t, n, r) {
            return this.filter(w).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, n, r)
        },
        animate: function(e, t, n, r) {
            var i = ue.isEmptyObject(e),
                o = ue.speed(t, n, r),
                a = function() {
                    var t = P(this, ue.extend({}, e), o);
                    a.finish = function() {
                        t.stop(!0)
                    }, (i || ue._data(this, "finish")) && t.stop(!0)
                };
            return a.finish = a, i || o.queue === !1 ? this.each(a) : this.queue(o.queue, a)
        },
        stop: function(e, n, r) {
            var i = function(e) {
                var t = e.stop;
                delete e.stop, t(r)
            };
            return "string" != typeof e && (r = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                var t = !0,
                    n = null != e && e + "queueHooks",
                    o = ue.timers,
                    a = ue._data(this);
                if (n) a[n] && a[n].stop && i(a[n]);
                else
                    for (n in a) a[n] && a[n].stop && tn.test(n) && i(a[n]);
                for (n = o.length; n--;) o[n].elem !== this || null != e && o[n].queue !== e || (o[n].anim.stop(r), t = !1, o.splice(n, 1));
                (t || !r) && ue.dequeue(this, e)
            })
        },
        finish: function(e) {
            return e !== !1 && (e = e || "fx"), this.each(function() {
                var t, n = ue._data(this),
                    r = n[e + "queue"],
                    i = n[e + "queueHooks"],
                    o = ue.timers,
                    a = r ? r.length : 0;
                for (n.finish = !0, ue.queue(this, e, []), i && i.cur && i.cur.finish && i.cur.finish.call(this), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                for (t = 0; a > t; t++) r[t] && r[t].finish && r[t].finish.call(this);
                delete n.finish
            })
        }
    }), ue.each({
        slideDown: I("show"),
        slideUp: I("hide"),
        slideToggle: I("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, t) {
        ue.fn[e] = function(e, n, r) {
            return this.animate(t, e, n, r)
        }
    }), ue.speed = function(e, t, n) {
        var r = e && "object" == typeof e ? ue.extend({}, e) : {
            complete: n || !n && t || ue.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !ue.isFunction(t) && t
        };
        return r.duration = ue.fx.off ? 0 : "number" == typeof r.duration ? r.duration : r.duration in ue.fx.speeds ? ue.fx.speeds[r.duration] : ue.fx.speeds._default, (null == r.queue || r.queue === !0) && (r.queue = "fx"), r.old = r.complete, r.complete = function() {
            ue.isFunction(r.old) && r.old.call(this), r.queue && ue.dequeue(this, r.queue)
        }, r
    }, ue.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, ue.timers = [], ue.fx = $.prototype.init, ue.fx.tick = function() {
        var e, n = ue.timers,
            r = 0;
        for (Qt = ue.now(); n.length > r; r++) e = n[r], e() || n[r] !== e || n.splice(r--, 1);
        n.length || ue.fx.stop(), Qt = t
    }, ue.fx.timer = function(e) {
        e() && ue.timers.push(e) && ue.fx.start()
    }, ue.fx.interval = 13, ue.fx.start = function() {
        Kt || (Kt = setInterval(ue.fx.tick, ue.fx.interval))
    }, ue.fx.stop = function() {
        clearInterval(Kt), Kt = null
    }, ue.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, ue.fx.step = {}, ue.expr && ue.expr.filters && (ue.expr.filters.animated = function(e) {
        return ue.grep(ue.timers, function(t) {
            return e === t.elem
        }).length
    }), ue.fn.offset = function(e) {
        if (arguments.length) return e === t ? this : this.each(function(t) {
            ue.offset.setOffset(this, e, t)
        });
        var n, r, i = {
                top: 0,
                left: 0
            },
            o = this[0],
            a = o && o.ownerDocument;
        return a ? (n = a.documentElement, ue.contains(n, o) ? (typeof o.getBoundingClientRect !== V && (i = o.getBoundingClientRect()), r = z(a), {
            top: i.top + (r.pageYOffset || n.scrollTop) - (n.clientTop || 0),
            left: i.left + (r.pageXOffset || n.scrollLeft) - (n.clientLeft || 0)
        }) : i) : void 0
    }, ue.offset = {
        setOffset: function(e, t, n) {
            var r = ue.css(e, "position");
            "static" === r && (e.style.position = "relative");
            var i, o, a = ue(e),
                s = a.offset(),
                u = ue.css(e, "top"),
                l = ue.css(e, "left"),
                c = ("absolute" === r || "fixed" === r) && ue.inArray("auto", [u, l]) > -1,
                f = {},
                p = {};
            c ? (p = a.position(), i = p.top, o = p.left) : (i = parseFloat(u) || 0, o = parseFloat(l) || 0), ue.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (f.top = t.top - s.top + i), null != t.left && (f.left = t.left - s.left + o), "using" in t ? t.using.call(e, f) : a.css(f)
        }
    }, ue.fn.extend({
        position: function() {
            if (this[0]) {
                var e, t, n = {
                        top: 0,
                        left: 0
                    },
                    r = this[0];
                return "fixed" === ue.css(r, "position") ? t = r.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), ue.nodeName(e[0], "html") || (n = e.offset()), n.top += ue.css(e[0], "borderTopWidth", !0), n.left += ue.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - n.top - ue.css(r, "marginTop", !0),
                    left: t.left - n.left - ue.css(r, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || Y.documentElement; e && !ue.nodeName(e, "html") && "static" === ue.css(e, "position");) e = e.offsetParent;
                return e || Y.documentElement
            })
        }
    }), ue.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, n) {
        var r = /Y/.test(n);
        ue.fn[e] = function(i) {
            return ue.access(this, function(e, i, o) {
                var a = z(e);
                return o === t ? a ? n in a ? a[n] : a.document.documentElement[i] : e[i] : (a ? a.scrollTo(r ? ue(a).scrollLeft() : o, r ? o : ue(a).scrollTop()) : e[i] = o, t)
            }, e, i, arguments.length, null)
        }
    }), ue.each({
        Height: "height",
        Width: "width"
    }, function(e, n) {
        ue.each({
            padding: "inner" + e,
            content: n,
            "": "outer" + e
        }, function(r, i) {
            ue.fn[i] = function(i, o) {
                var a = arguments.length && (r || "boolean" != typeof i),
                    s = r || (i === !0 || o === !0 ? "margin" : "border");
                return ue.access(this, function(n, r, i) {
                    var o;
                    return ue.isWindow(n) ? n.document.documentElement["client" + e] : 9 === n.nodeType ? (o = n.documentElement, Math.max(n.body["scroll" + e], o["scroll" + e], n.body["offset" + e], o["offset" + e], o["client" + e])) : i === t ? ue.css(n, r, s) : ue.style(n, r, i, s)
                }, n, a ? i : t, a, null)
            }
        })
    }), e.jQuery = e.$ = ue, "function" == typeof define && define.amd && define.amd.jQuery && define("jquery", [], function() {
        return ue
    })
}(window);
! function(t, e, n) {
    "use strict";

    function r(t, e) {
        return e = e || Error,
            function() {
                var n, r, i = arguments[0],
                    o = "[" + (t ? t + ":" : "") + i + "] ",
                    a = arguments[1],
                    s = arguments;
                for (n = o + a.replace(/\{\d+\}/g, function(t) {
                        var e = +t.slice(1, -1);
                        return e + 2 < s.length ? ht(s[e + 2]) : t
                    }), n = n + "\nhttp://errors.angularjs.org/1.3.12/" + (t ? t + "/" : "") + i, r = 2; r < arguments.length; r++) n = n + (2 == r ? "?" : "&") + "p" + (r - 2) + "=" + encodeURIComponent(ht(arguments[r]));
                return new e(n)
            }
    }

    function i(t) {
        if (null == t || A(t)) return !1;
        var e = t.length;
        return !(t.nodeType !== mr || !e) || (w(t) || lr(t) || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
    }

    function o(t, e, n) {
        var r, a;
        if (t)
            if (S(t))
                for (r in t) "prototype" == r || "length" == r || "name" == r || t.hasOwnProperty && !t.hasOwnProperty(r) || e.call(n, t[r], r, t);
            else if (lr(t) || i(t)) {
            var s = "object" != typeof t;
            for (r = 0, a = t.length; r < a; r++)(s || r in t) && e.call(n, t[r], r, t)
        } else if (t.forEach && t.forEach !== o) t.forEach(e, n, t);
        else
            for (r in t) t.hasOwnProperty(r) && e.call(n, t[r], r, t);
        return t
    }

    function a(t) {
        return Object.keys(t).sort()
    }

    function s(t, e, n) {
        for (var r = a(t), i = 0; i < r.length; i++) e.call(n, t[r[i]], r[i]);
        return r
    }

    function u(t) {
        return function(e, n) {
            t(n, e)
        }
    }

    function c() {
        return ++ur
    }

    function l(t, e) {
        e ? t.$$hashKey = e : delete t.$$hashKey
    }

    function f(t) {
        for (var e = t.$$hashKey, n = 1, r = arguments.length; n < r; n++) {
            var i = arguments[n];
            if (i)
                for (var o = Object.keys(i), a = 0, s = o.length; a < s; a++) {
                    var u = o[a];
                    t[u] = i[u]
                }
        }
        return l(t, e), t
    }

    function h(t) {
        return parseInt(t, 10)
    }

    function $(t, e) {
        return f(Object.create(t), e)
    }

    function p() {}

    function d(t) {
        return t
    }

    function v(t) {
        return function() {
            return t
        }
    }

    function m(t) {
        return "undefined" == typeof t
    }

    function g(t) {
        return "undefined" != typeof t
    }

    function y(t) {
        return null !== t && "object" == typeof t
    }

    function w(t) {
        return "string" == typeof t
    }

    function b(t) {
        return "number" == typeof t
    }

    function x(t) {
        return "[object Date]" === or.call(t)
    }

    function S(t) {
        return "function" == typeof t
    }

    function C(t) {
        return "[object RegExp]" === or.call(t)
    }

    function A(t) {
        return t && t.window === t
    }

    function k(t) {
        return t && t.$evalAsync && t.$watch
    }

    function E(t) {
        return "[object File]" === or.call(t)
    }

    function O(t) {
        return "[object FormData]" === or.call(t)
    }

    function T(t) {
        return "[object Blob]" === or.call(t)
    }

    function M(t) {
        return "boolean" == typeof t
    }

    function N(t) {
        return t && S(t.then)
    }

    function V(t) {
        return !(!t || !(t.nodeName || t.prop && t.attr && t.find))
    }

    function D(t) {
        var e, n = {},
            r = t.split(",");
        for (e = 0; e < r.length; e++) n[r[e]] = !0;
        return n
    }

    function j(t) {
        return Gn(t.nodeName || t[0] && t[0].nodeName)
    }

    function P(t, e) {
        var n = t.indexOf(e);
        return n >= 0 && t.splice(n, 1), e
    }

    function R(t, e, n, r) {
        if (A(t) || k(t)) throw ar("cpws", "Can't copy! Making copies of Window or Scope instances is not supported.");
        if (e) {
            if (t === e) throw ar("cpi", "Can't copy! Source and destination are identical.");
            if (n = n || [], r = r || [], y(t)) {
                var i = n.indexOf(t);
                if (i !== -1) return r[i];
                n.push(t), r.push(e)
            }
            var a;
            if (lr(t)) {
                e.length = 0;
                for (var s = 0; s < t.length; s++) a = R(t[s], null, n, r), y(t[s]) && (n.push(t[s]), r.push(a)), e.push(a)
            } else {
                var u = e.$$hashKey;
                lr(e) ? e.length = 0 : o(e, function(t, n) {
                    delete e[n]
                });
                for (var c in t) t.hasOwnProperty(c) && (a = R(t[c], null, n, r), y(t[c]) && (n.push(t[c]), r.push(a)), e[c] = a);
                l(e, u)
            }
        } else if (e = t, t)
            if (lr(t)) e = R(t, [], n, r);
            else if (x(t)) e = new Date(t.getTime());
        else if (C(t)) e = new RegExp(t.source, t.toString().match(/[^\/]*$/)[0]), e.lastIndex = t.lastIndex;
        else if (y(t)) {
            var f = Object.create(Object.getPrototypeOf(t));
            e = R(t, f, n, r)
        }
        return e
    }

    function _(t, e) {
        if (lr(t)) {
            e = e || [];
            for (var n = 0, r = t.length; n < r; n++) e[n] = t[n]
        } else if (y(t)) {
            e = e || {};
            for (var i in t) "$" === i.charAt(0) && "$" === i.charAt(1) || (e[i] = t[i])
        }
        return e || t
    }

    function I(t, e) {
        if (t === e) return !0;
        if (null === t || null === e) return !1;
        if (t !== t && e !== e) return !0;
        var r, i, o, a = typeof t,
            s = typeof e;
        if (a == s && "object" == a) {
            if (!lr(t)) {
                if (x(t)) return !!x(e) && I(t.getTime(), e.getTime());
                if (C(t) && C(e)) return t.toString() == e.toString();
                if (k(t) || k(e) || A(t) || A(e) || lr(e)) return !1;
                o = {};
                for (i in t)
                    if ("$" !== i.charAt(0) && !S(t[i])) {
                        if (!I(t[i], e[i])) return !1;
                        o[i] = !0
                    }
                for (i in e)
                    if (!o.hasOwnProperty(i) && "$" !== i.charAt(0) && e[i] !== n && !S(e[i])) return !1;
                return !0
            }
            if (!lr(e)) return !1;
            if ((r = t.length) == e.length) {
                for (i = 0; i < r; i++)
                    if (!I(t[i], e[i])) return !1;
                return !0
            }
        }
        return !1
    }

    function q(t, e, n) {
        return t.concat(nr.call(e, n))
    }

    function U(t, e) {
        return nr.call(t, e || 0)
    }

    function F(t, e) {
        var n = arguments.length > 2 ? U(arguments, 2) : [];
        return !S(e) || e instanceof RegExp ? e : n.length ? function() {
            return arguments.length ? e.apply(t, q(n, arguments, 0)) : e.apply(t, n)
        } : function() {
            return arguments.length ? e.apply(t, arguments) : e.call(t)
        }
    }

    function H(t, r) {
        var i = r;
        return "string" == typeof t && "$" === t.charAt(0) && "$" === t.charAt(1) ? i = n : A(r) ? i = "$WINDOW" : r && e === r ? i = "$DOCUMENT" : k(r) && (i = "$SCOPE"), i
    }

    function L(t, e) {
        return "undefined" == typeof t ? n : (b(e) || (e = e ? 2 : null), JSON.stringify(t, H, e))
    }

    function B(t) {
        return w(t) ? JSON.parse(t) : t
    }

    function z(t) {
        t = Qn(t).clone();
        try {
            t.empty()
        } catch (t) {}
        var e = Qn("<div>").append(t).html();
        try {
            return t[0].nodeType === gr ? Gn(e) : e.match(/^(<[^>]+>)/)[1].replace(/^<([\w\-]+)/, function(t, e) {
                return "<" + Gn(e)
            })
        } catch (t) {
            return Gn(e)
        }
    }

    function W(t) {
        try {
            return decodeURIComponent(t)
        } catch (t) {}
    }

    function G(t) {
        var e, n, r = {};
        return o((t || "").split("&"), function(t) {
            if (t && (e = t.replace(/\+/g, "%20").split("="), n = W(e[0]), g(n))) {
                var i = !g(e[1]) || W(e[1]);
                Jn.call(r, n) ? lr(r[n]) ? r[n].push(i) : r[n] = [r[n], i] : r[n] = i
            }
        }), r
    }

    function J(t) {
        var e = [];
        return o(t, function(t, n) {
            lr(t) ? o(t, function(t) {
                e.push(Z(n, !0) + (t === !0 ? "" : "=" + Z(t, !0)))
            }) : e.push(Z(n, !0) + (t === !0 ? "" : "=" + Z(t, !0)))
        }), e.length ? e.join("&") : ""
    }

    function Y(t) {
        return Z(t, !0).replace(/%26/gi, "&").replace(/%3D/gi, "=").replace(/%2B/gi, "+")
    }

    function Z(t, e) {
        return encodeURIComponent(t).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%3B/gi, ";").replace(/%20/g, e ? "%20" : "+")
    }

    function K(t, e) {
        var n, r, i = pr.length;
        for (t = Qn(t), r = 0; r < i; ++r)
            if (n = pr[r] + e, w(n = t.attr(n))) return n;
        return null
    }

    function X(t, e) {
        var n, r, i = {};
        o(pr, function(e) {
            var i = e + "app";
            !n && t.hasAttribute && t.hasAttribute(i) && (n = t, r = t.getAttribute(i))
        }), o(pr, function(e) {
            var i, o = e + "app";
            !n && (i = t.querySelector("[" + o.replace(":", "\\:") + "]")) && (n = i, r = i.getAttribute(o))
        }), n && (i.strictDi = null !== K(n, "strict-di"), e(n, r ? [r] : [], i))
    }

    function Q(n, r, i) {
        y(i) || (i = {});
        var a = {
            strictDi: !1
        };
        i = f(a, i);
        var s = function() {
                if (n = Qn(n), n.injector()) {
                    var t = n[0] === e ? "document" : z(n);
                    throw ar("btstrpd", "App Already Bootstrapped with this Element '{0}'", t.replace(/</, "&lt;").replace(/>/, "&gt;"))
                }
                r = r || [], r.unshift(["$provide", function(t) {
                    t.value("$rootElement", n)
                }]), i.debugInfoEnabled && r.push(["$compileProvider", function(t) {
                    t.debugInfoEnabled(!0)
                }]), r.unshift("ng");
                var o = Bt(r, i.strictDi);
                return o.invoke(["$rootScope", "$rootElement", "$compile", "$injector", function(t, e, n, r) {
                    t.$apply(function() {
                        e.data("$injector", r), n(e)(t)
                    })
                }]), o
            },
            u = /^NG_ENABLE_DEBUG_INFO!/,
            c = /^NG_DEFER_BOOTSTRAP!/;
        return t && u.test(t.name) && (i.debugInfoEnabled = !0, t.name = t.name.replace(u, "")), t && !c.test(t.name) ? s() : (t.name = t.name.replace(c, ""), sr.resumeBootstrap = function(t) {
            return o(t, function(t) {
                r.push(t)
            }), s()
        }, void(S(sr.resumeDeferredBootstrap) && sr.resumeDeferredBootstrap()))
    }

    function tt() {
        t.name = "NG_ENABLE_DEBUG_INFO!" + t.name, t.location.reload()
    }

    function et(t) {
        var e = sr.element(t).injector();
        if (!e) throw ar("test", "no injector found for element argument to getTestability");
        return e.get("$$testability")
    }

    function nt(t, e) {
        return e = e || "_", t.replace(dr, function(t, n) {
            return (n ? e : "") + t.toLowerCase()
        })
    }

    function rt() {
        var e;
        vr || (tr = t.jQuery, tr && tr.fn.on ? (Qn = tr, f(tr.fn, {
            scope: Rr.scope,
            isolateScope: Rr.isolateScope,
            controller: Rr.controller,
            injector: Rr.injector,
            inheritedData: Rr.inheritedData
        }), e = tr.cleanData, tr.cleanData = function(t) {
            var n;
            if (cr) cr = !1;
            else
                for (var r, i = 0; null != (r = t[i]); i++) n = tr._data(r, "events"), n && n.$destroy && tr(r).triggerHandler("$destroy");
            e(t)
        }) : Qn = wt, sr.element = Qn, vr = !0)
    }

    function it(t, e, n) {
        if (!t) throw ar("areq", "Argument '{0}' is {1}", e || "?", n || "required");
        return t
    }

    function ot(t, e, n) {
        return n && lr(t) && (t = t[t.length - 1]), it(S(t), e, "not a function, got " + (t && "object" == typeof t ? t.constructor.name || "Object" : typeof t)), t
    }

    function at(t, e) {
        if ("hasOwnProperty" === t) throw ar("badname", "hasOwnProperty is not a valid {0} name", e)
    }

    function st(t, e, n) {
        if (!e) return t;
        for (var r, i = e.split("."), o = t, a = i.length, s = 0; s < a; s++) r = i[s], t && (t = (o = t)[r]);
        return !n && S(t) ? F(o, t) : t
    }

    function ut(t) {
        var e = t[0],
            n = t[t.length - 1],
            r = [e];
        do {
            if (e = e.nextSibling, !e) break;
            r.push(e)
        } while (e !== n);
        return Qn(r)
    }

    function ct() {
        return Object.create(null)
    }

    function lt(t) {
        function e(t, e, n) {
            return t[e] || (t[e] = n())
        }
        var n = r("$injector"),
            i = r("ng"),
            o = e(t, "angular", Object);
        return o.$$minErr = o.$$minErr || r, e(o, "module", function() {
            var t = {};
            return function(r, o, a) {
                var s = function(t, e) {
                    if ("hasOwnProperty" === t) throw i("badname", "hasOwnProperty is not a valid {0} name", e)
                };
                return s(r, "module"), o && t.hasOwnProperty(r) && (t[r] = null), e(t, r, function() {
                    function t(t, n, r, i) {
                        return i || (i = e),
                            function() {
                                return i[r || "push"]([t, n, arguments]), c
                            }
                    }
                    if (!o) throw n("nomod", "Module '{0}' is not available! You either misspelled the module name or forgot to load it. If registering a module ensure that you specify the dependencies as the second argument.", r);
                    var e = [],
                        i = [],
                        s = [],
                        u = t("$injector", "invoke", "push", i),
                        c = {
                            _invokeQueue: e,
                            _configBlocks: i,
                            _runBlocks: s,
                            requires: o,
                            name: r,
                            provider: t("$provide", "provider"),
                            factory: t("$provide", "factory"),
                            service: t("$provide", "service"),
                            value: t("$provide", "value"),
                            constant: t("$provide", "constant", "unshift"),
                            animation: t("$animateProvider", "register"),
                            filter: t("$filterProvider", "register"),
                            controller: t("$controllerProvider", "register"),
                            directive: t("$compileProvider", "directive"),
                            config: u,
                            run: function(t) {
                                return s.push(t), this
                            }
                        };
                    return a && u(a), c
                })
            }
        })
    }

    function ft(t) {
        var e = [];
        return JSON.stringify(t, function(t, n) {
            if (n = H(t, n), y(n)) {
                if (e.indexOf(n) >= 0) return "<<already seen>>";
                e.push(n)
            }
            return n
        })
    }

    function ht(t) {
        return "function" == typeof t ? t.toString().replace(/ \{[\s\S]*$/, "") : "undefined" == typeof t ? "undefined" : "string" != typeof t ? ft(t) : t
    }

    function $t(e) {
        f(e, {
            bootstrap: Q,
            copy: R,
            extend: f,
            equals: I,
            element: Qn,
            forEach: o,
            injector: Bt,
            noop: p,
            bind: F,
            toJson: L,
            fromJson: B,
            identity: d,
            isUndefined: m,
            isDefined: g,
            isString: w,
            isFunction: S,
            isObject: y,
            isNumber: b,
            isElement: V,
            isArray: lr,
            version: xr,
            isDate: x,
            lowercase: Gn,
            uppercase: Yn,
            callbacks: {
                counter: 0
            },
            getTestability: et,
            $$minErr: r,
            $$csp: $r,
            reloadWithDebugInfo: tt
        }), er = lt(t);
        try {
            er("ngLocale")
        } catch (t) {
            er("ngLocale", []).provider("$locale", ve)
        }
        er("ng", ["ngLocale"], ["$provide", function(t) {
            t.provider({
                $$sanitizeUri: Je
            }), t.provider("$compile", Kt).directive({
                a: Oi,
                input: zi,
                textarea: zi,
                form: Di,
                script: Po,
                select: Io,
                style: Uo,
                option: qo,
                ngBind: Ji,
                ngBindHtml: Zi,
                ngBindTemplate: Yi,
                ngClass: Xi,
                ngClassEven: to,
                ngClassOdd: Qi,
                ngCloak: eo,
                ngController: no,
                ngForm: ji,
                ngHide: To,
                ngIf: oo,
                ngInclude: ao,
                ngInit: uo,
                ngNonBindable: So,
                ngPluralize: Co,
                ngRepeat: Ao,
                ngShow: Oo,
                ngStyle: Mo,
                ngSwitch: No,
                ngSwitchWhen: Vo,
                ngSwitchDefault: Do,
                ngOptions: _o,
                ngTransclude: jo,
                ngModel: wo,
                ngList: co,
                ngChange: Ki,
                pattern: Ho,
                ngPattern: Ho,
                required: Fo,
                ngRequired: Fo,
                minlength: Bo,
                ngMinlength: Bo,
                maxlength: Lo,
                ngMaxlength: Lo,
                ngValue: Gi,
                ngModelOptions: xo
            }).directive({
                ngInclude: so
            }).directive(Ti).directive(ro), t.provider({
                $anchorScroll: zt,
                $animate: Wr,
                $browser: Jt,
                $cacheFactory: Yt,
                $controller: ee,
                $document: ne,
                $exceptionHandler: re,
                $filter: sn,
                $interpolate: pe,
                $interval: de,
                $http: le,
                $httpBackend: he,
                $location: Me,
                $log: Ne,
                $parse: He,
                $rootScope: Ge,
                $q: Le,
                $$q: Be,
                $sce: Xe,
                $sceDelegate: Ke,
                $sniffer: Qe,
                $templateCache: Zt,
                $templateRequest: tn,
                $$testability: en,
                $timeout: nn,
                $window: an,
                $$rAF: We,
                $$asyncCallback: Wt,
                $$jqLite: qt
            })
        }])
    }

    function pt() {
        return ++Cr
    }

    function dt(t) {
        return t.replace(Er, function(t, e, n, r) {
            return r ? n.toUpperCase() : n
        }).replace(Or, "Moz$1")
    }

    function vt(t) {
        return !Vr.test(t)
    }

    function mt(t) {
        var e = t.nodeType;
        return e === mr || !e || e === wr
    }

    function gt(t, e) {
        var n, r, i, a, s = e.createDocumentFragment(),
            u = [];
        if (vt(t)) u.push(e.createTextNode(t));
        else {
            for (n = n || s.appendChild(e.createElement("div")), r = (Dr.exec(t) || ["", ""])[1].toLowerCase(), i = Pr[r] || Pr._default, n.innerHTML = i[1] + t.replace(jr, "<$1></$2>") + i[2], a = i[0]; a--;) n = n.lastChild;
            u = q(u, n.childNodes), n = s.firstChild, n.textContent = ""
        }
        return s.textContent = "", s.innerHTML = "", o(u, function(t) {
            s.appendChild(t)
        }), s
    }

    function yt(t, n) {
        n = n || e;
        var r;
        return (r = Nr.exec(t)) ? [n.createElement(r[1])] : (r = gt(t, n)) ? r.childNodes : []
    }

    function wt(t) {
        if (t instanceof wt) return t;
        var e;
        if (w(t) && (t = fr(t), e = !0), !(this instanceof wt)) {
            if (e && "<" != t.charAt(0)) throw Mr("nosel", "Looking up elements via selectors is not supported by jqLite! See: http://docs.angularjs.org/api/angular.element");
            return new wt(t)
        }
        e ? Mt(this, yt(t)) : Mt(this, t)
    }

    function bt(t) {
        return t.cloneNode(!0)
    }

    function xt(t, e) {
        if (e || Ct(t), t.querySelectorAll)
            for (var n = t.querySelectorAll("*"), r = 0, i = n.length; r < i; r++) Ct(n[r])
    }

    function St(t, e, n, r) {
        if (g(r)) throw Mr("offargs", "jqLite#off() does not support the `selector` argument");
        var i = At(t),
            a = i && i.events,
            s = i && i.handle;
        if (s)
            if (e) o(e.split(" "), function(e) {
                if (g(n)) {
                    var r = a[e];
                    if (P(r || [], n), r && r.length > 0) return
                }
                kr(t, e, s), delete a[e]
            });
            else
                for (e in a) "$destroy" !== e && kr(t, e, s), delete a[e]
    }

    function Ct(t, e) {
        var r = t.ng339,
            i = r && Sr[r];
        if (i) {
            if (e) return void delete i.data[e];
            i.handle && (i.events.$destroy && i.handle({}, "$destroy"), St(t)), delete Sr[r], t.ng339 = n
        }
    }

    function At(t, e) {
        var r = t.ng339,
            i = r && Sr[r];
        return e && !i && (t.ng339 = r = pt(), i = Sr[r] = {
            events: {},
            data: {},
            handle: n
        }), i
    }

    function kt(t, e, n) {
        if (mt(t)) {
            var r = g(n),
                i = !r && e && !y(e),
                o = !e,
                a = At(t, !i),
                s = a && a.data;
            if (r) s[e] = n;
            else {
                if (o) return s;
                if (i) return s && s[e];
                f(s, e)
            }
        }
    }

    function Et(t, e) {
        return !!t.getAttribute && (" " + (t.getAttribute("class") || "") + " ").replace(/[\n\t]/g, " ").indexOf(" " + e + " ") > -1
    }

    function Ot(t, e) {
        e && t.setAttribute && o(e.split(" "), function(e) {
            t.setAttribute("class", fr((" " + (t.getAttribute("class") || "") + " ").replace(/[\n\t]/g, " ").replace(" " + fr(e) + " ", " ")))
        })
    }

    function Tt(t, e) {
        if (e && t.setAttribute) {
            var n = (" " + (t.getAttribute("class") || "") + " ").replace(/[\n\t]/g, " ");
            o(e.split(" "), function(t) {
                t = fr(t), n.indexOf(" " + t + " ") === -1 && (n += t + " ")
            }), t.setAttribute("class", fr(n))
        }
    }

    function Mt(t, e) {
        if (e)
            if (e.nodeType) t[t.length++] = e;
            else {
                var n = e.length;
                if ("number" == typeof n && e.window !== e) {
                    if (n)
                        for (var r = 0; r < n; r++) t[t.length++] = e[r]
                } else t[t.length++] = e
            }
    }

    function Nt(t, e) {
        return Vt(t, "$" + (e || "ngController") + "Controller")
    }

    function Vt(t, e, r) {
        t.nodeType == wr && (t = t.documentElement);
        for (var i = lr(e) ? e : [e]; t;) {
            for (var o = 0, a = i.length; o < a; o++)
                if ((r = Qn.data(t, i[o])) !== n) return r;
            t = t.parentNode || t.nodeType === br && t.host
        }
    }

    function Dt(t) {
        for (xt(t, !0); t.firstChild;) t.removeChild(t.firstChild)
    }

    function jt(t, e) {
        e || xt(t);
        var n = t.parentNode;
        n && n.removeChild(t)
    }

    function Pt(e, n) {
        n = n || t, "complete" === n.document.readyState ? n.setTimeout(e) : Qn(n).on("load", e)
    }

    function Rt(t, e) {
        var n = _r[e.toLowerCase()];
        return n && Ir[j(t)] && n
    }

    function _t(t, e) {
        var n = t.nodeName;
        return ("INPUT" === n || "TEXTAREA" === n) && qr[e]
    }

    function It(t, e) {
        var n = function(n, r) {
            n.isDefaultPrevented = function() {
                return n.defaultPrevented
            };
            var i = e[r || n.type],
                o = i ? i.length : 0;
            if (o) {
                if (m(n.immediatePropagationStopped)) {
                    var a = n.stopImmediatePropagation;
                    n.stopImmediatePropagation = function() {
                        n.immediatePropagationStopped = !0, n.stopPropagation && n.stopPropagation(), a && a.call(n)
                    }
                }
                n.isImmediatePropagationStopped = function() {
                    return n.immediatePropagationStopped === !0
                }, o > 1 && (i = _(i));
                for (var s = 0; s < o; s++) n.isImmediatePropagationStopped() || i[s].call(t, n)
            }
        };
        return n.elem = t, n
    }

    function qt() {
        this.$get = function() {
            return f(wt, {
                hasClass: function(t, e) {
                    return t.attr && (t = t[0]), Et(t, e)
                },
                addClass: function(t, e) {
                    return t.attr && (t = t[0]), Tt(t, e)
                },
                removeClass: function(t, e) {
                    return t.attr && (t = t[0]), Ot(t, e)
                }
            })
        }
    }

    function Ut(t, e) {
        var n = t && t.$$hashKey;
        if (n) return "function" == typeof n && (n = t.$$hashKey()), n;
        var r = typeof t;
        return n = "function" == r || "object" == r && null !== t ? t.$$hashKey = r + ":" + (e || c)() : r + ":" + t
    }

    function Ft(t, e) {
        if (e) {
            var n = 0;
            this.nextUid = function() {
                return ++n
            }
        }
        o(t, this.put, this)
    }

    function Ht(t) {
        var e = t.toString().replace(Lr, ""),
            n = e.match(Ur);
        return n ? "function(" + (n[1] || "").replace(/[\s\r\n]+/, " ") + ")" : "fn"
    }

    function Lt(t, e, n) {
        var r, i, a, s;
        if ("function" == typeof t) {
            if (!(r = t.$inject)) {
                if (r = [], t.length) {
                    if (e) throw w(n) && n || (n = t.name || Ht(t)), Br("strictdi", "{0} is not using explicit annotation and cannot be invoked in strict mode", n);
                    i = t.toString().replace(Lr, ""), a = i.match(Ur), o(a[1].split(Fr), function(t) {
                        t.replace(Hr, function(t, e, n) {
                            r.push(n)
                        })
                    })
                }
                t.$inject = r
            }
        } else lr(t) ? (s = t.length - 1, ot(t[s], "fn"), r = t.slice(0, s)) : ot(t, "fn", !0);
        return r
    }

    function Bt(t, e) {
        function r(t) {
            return function(e, n) {
                return y(e) ? void o(e, u(t)) : t(e, n)
            }
        }

        function i(t, e) {
            if (at(t, "service"), (S(e) || lr(e)) && (e = k.instantiate(e)), !e.$get) throw Br("pget", "Provider '{0}' must define $get factory method.", t);
            return A[t + b] = e
        }

        function a(t, e) {
            return function() {
                var n = O.invoke(e, this);
                if (m(n)) throw Br("undef", "Provider '{0}' must return a value from $get factory method.", t);
                return n
            }
        }

        function s(t, e, n) {
            return i(t, {
                $get: n !== !1 ? a(t, e) : e
            })
        }

        function c(t, e) {
            return s(t, ["$injector", function(t) {
                return t.instantiate(e)
            }])
        }

        function l(t, e) {
            return s(t, v(e), !1)
        }

        function f(t, e) {
            at(t, "constant"), A[t] = e, E[t] = e
        }

        function h(t, e) {
            var n = k.get(t + b),
                r = n.$get;
            n.$get = function() {
                var t = O.invoke(r, n);
                return O.invoke(e, null, {
                    $delegate: t
                })
            }
        }

        function $(t) {
            var e, n = [];
            return o(t, function(t) {
                function r(t) {
                    var e, n;
                    for (e = 0, n = t.length; e < n; e++) {
                        var r = t[e],
                            i = k.get(r[0]);
                        i[r[1]].apply(i, r[2])
                    }
                }
                if (!C.get(t)) {
                    C.put(t, !0);
                    try {
                        w(t) ? (e = er(t), n = n.concat($(e.requires)).concat(e._runBlocks), r(e._invokeQueue), r(e._configBlocks)) : S(t) ? n.push(k.invoke(t)) : lr(t) ? n.push(k.invoke(t)) : ot(t, "module")
                    } catch (e) {
                        throw lr(t) && (t = t[t.length - 1]), e.message && e.stack && e.stack.indexOf(e.message) == -1 && (e = e.message + "\n" + e.stack), Br("modulerr", "Failed to instantiate module {0} due to:\n{1}", t, e.stack || e.message || e)
                    }
                }
            }), n
        }

        function d(t, n) {
            function r(e, r) {
                if (t.hasOwnProperty(e)) {
                    if (t[e] === g) throw Br("cdep", "Circular dependency found: {0}", e + " <- " + x.join(" <- "));
                    return t[e]
                }
                try {
                    return x.unshift(e), t[e] = g, t[e] = n(e, r)
                } catch (n) {
                    throw t[e] === g && delete t[e], n
                } finally {
                    x.shift()
                }
            }

            function i(t, n, i, o) {
                "string" == typeof i && (o = i, i = null);
                var a, s, u, c = [],
                    l = Bt.$$annotate(t, e, o);
                for (s = 0, a = l.length; s < a; s++) {
                    if (u = l[s], "string" != typeof u) throw Br("itkn", "Incorrect injection token! Expected service name as string, got {0}", u);
                    c.push(i && i.hasOwnProperty(u) ? i[u] : r(u, o))
                }
                return lr(t) && (t = t[a]), t.apply(n, c)
            }

            function o(t, e, n) {
                var r = Object.create((lr(t) ? t[t.length - 1] : t).prototype || null),
                    o = i(t, r, e, n);
                return y(o) || S(o) ? o : r
            }
            return {
                invoke: i,
                instantiate: o,
                get: r,
                annotate: Bt.$$annotate,
                has: function(e) {
                    return A.hasOwnProperty(e + b) || t.hasOwnProperty(e)
                }
            }
        }
        e = e === !0;
        var g = {},
            b = "Provider",
            x = [],
            C = new Ft([], !0),
            A = {
                $provide: {
                    provider: r(i),
                    factory: r(s),
                    service: r(c),
                    value: r(l),
                    constant: r(f),
                    decorator: h
                }
            },
            k = A.$injector = d(A, function(t, e) {
                throw sr.isString(e) && x.push(e), Br("unpr", "Unknown provider: {0}", x.join(" <- "))
            }),
            E = {},
            O = E.$injector = d(E, function(t, e) {
                var r = k.get(t + b, e);
                return O.invoke(r.$get, r, n, t)
            });
        return o($(t), function(t) {
            O.invoke(t || p)
        }), O
    }

    function zt() {
        var t = !0;
        this.disableAutoScrolling = function() {
            t = !1
        }, this.$get = ["$window", "$location", "$rootScope", function(e, n, r) {
            function i(t) {
                var e = null;
                return Array.prototype.some.call(t, function(t) {
                    if ("a" === j(t)) return e = t, !0
                }), e
            }

            function o() {
                var t = s.yOffset;
                if (S(t)) t = t();
                else if (V(t)) {
                    var n = t[0],
                        r = e.getComputedStyle(n);
                    t = "fixed" !== r.position ? 0 : n.getBoundingClientRect().bottom
                } else b(t) || (t = 0);
                return t
            }

            function a(t) {
                if (t) {
                    t.scrollIntoView();
                    var n = o();
                    if (n) {
                        var r = t.getBoundingClientRect().top;
                        e.scrollBy(0, r - n)
                    }
                } else e.scrollTo(0, 0)
            }

            function s() {
                var t, e = n.hash();
                e ? (t = u.getElementById(e)) ? a(t) : (t = i(u.getElementsByName(e))) ? a(t) : "top" === e && a(null) : a(null)
            }
            var u = e.document;
            return t && r.$watch(function() {
                return n.hash()
            }, function(t, e) {
                t === e && "" === t || Pt(function() {
                    r.$evalAsync(s)
                })
            }), s
        }]
    }

    function Wt() {
        this.$get = ["$$rAF", "$timeout", function(t, e) {
            return t.supported ? function(e) {
                return t(e)
            } : function(t) {
                return e(t, 0, !1)
            }
        }]
    }

    function Gt(t, e, r, i) {
        function a(t) {
            try {
                t.apply(null, U(arguments, 1))
            } finally {
                if (S--, 0 === S)
                    for (; C.length;) try {
                        C.pop()()
                    } catch (t) {
                        r.error(t)
                    }
            }
        }

        function s(t) {
            var e = t.indexOf("#");
            return e === -1 ? "" : t.substr(e + 1)
        }

        function u(t, e) {
            ! function n() {
                o(k, function(t) {
                    t()
                }), A = e(n, t)
            }()
        }

        function c() {
            l(), f()
        }

        function l() {
            E = t.history.state, E = m(E) ? null : E, I(E, j) && (E = j), j = E
        }

        function f() {
            T === $.url() && O === E || (T = $.url(), O = E, o(V, function(t) {
                t($.url(), E)
            }))
        }

        function h(t) {
            try {
                return decodeURIComponent(t)
            } catch (e) {
                return t
            }
        }
        var $ = this,
            d = e[0],
            v = t.location,
            g = t.history,
            y = t.setTimeout,
            b = t.clearTimeout,
            x = {};
        $.isMock = !1;
        var S = 0,
            C = [];
        $.$$completeOutstandingRequest = a, $.$$incOutstandingRequestCount = function() {
            S++
        }, $.notifyWhenNoOutstandingRequests = function(t) {
            o(k, function(t) {
                t()
            }), 0 === S ? t() : C.push(t)
        };
        var A, k = [];
        $.addPollFn = function(t) {
            return m(A) && u(100, y), k.push(t), t
        };
        var E, O, T = v.href,
            M = e.find("base"),
            N = null;
        l(), O = E, $.url = function(e, n, r) {
            if (m(r) && (r = null), v !== t.location && (v = t.location), g !== t.history && (g = t.history), e) {
                var o = O === r;
                if (T === e && (!i.history || o)) return $;
                var a = T && be(T) === be(e);
                return T = e, O = r, !i.history || a && o ? (a || (N = e), n ? v.replace(e) : a ? v.hash = s(e) : v.href = e) : (g[n ? "replaceState" : "pushState"](r, "", e), l(), O = E), $
            }
            return N || v.href.replace(/%27/g, "'")
        }, $.state = function() {
            return E
        };
        var V = [],
            D = !1,
            j = null;
        $.onUrlChange = function(e) {
            return D || (i.history && Qn(t).on("popstate", c), Qn(t).on("hashchange", c), D = !0), V.push(e), e
        }, $.$$checkUrlChange = f, $.baseHref = function() {
            var t = M.attr("href");
            return t ? t.replace(/^(https?\:)?\/\/[^\/]*/, "") : ""
        };
        var P = {},
            R = "",
            _ = $.baseHref();
        $.cookies = function(t, e) {
            var i, o, a, s, u;
            if (!t) {
                if (d.cookie !== R)
                    for (R = d.cookie, o = R.split("; "), P = {}, s = 0; s < o.length; s++) a = o[s], u = a.indexOf("="), u > 0 && (t = h(a.substring(0, u)), P[t] === n && (P[t] = h(a.substring(u + 1))));
                return P
            }
            e === n ? d.cookie = encodeURIComponent(t) + "=;path=" + _ + ";expires=Thu, 01 Jan 1970 00:00:00 GMT" : w(e) && (i = (d.cookie = encodeURIComponent(t) + "=" + encodeURIComponent(e) + ";path=" + _).length + 1, i > 4096 && r.warn("Cookie '" + t + "' possibly not set or overflowed because it was too large (" + i + " > 4096 bytes)!"))
        }, $.defer = function(t, e) {
            var n;
            return S++, n = y(function() {
                delete x[n], a(t)
            }, e || 0), x[n] = !0, n
        }, $.defer.cancel = function(t) {
            return !!x[t] && (delete x[t], b(t), a(p), !0)
        }
    }

    function Jt() {
        this.$get = ["$window", "$log", "$sniffer", "$document", function(t, e, n, r) {
            return new Gt(t, r, e, n)
        }]
    }

    function Yt() {
        this.$get = function() {
            function t(t, n) {
                function i(t) {
                    t != h && ($ ? $ == t && ($ = t.n) : $ = t, o(t.n, t.p), o(t, h), h = t, h.n = null)
                }

                function o(t, e) {
                    t != e && (t && (t.p = e), e && (e.n = t))
                }
                if (t in e) throw r("$cacheFactory")("iid", "CacheId '{0}' is already taken!", t);
                var a = 0,
                    s = f({}, n, {
                        id: t
                    }),
                    u = {},
                    c = n && n.capacity || Number.MAX_VALUE,
                    l = {},
                    h = null,
                    $ = null;
                return e[t] = {
                    put: function(t, e) {
                        if (c < Number.MAX_VALUE) {
                            var n = l[t] || (l[t] = {
                                key: t
                            });
                            i(n)
                        }
                        if (!m(e)) return t in u || a++, u[t] = e, a > c && this.remove($.key), e
                    },
                    get: function(t) {
                        if (c < Number.MAX_VALUE) {
                            var e = l[t];
                            if (!e) return;
                            i(e)
                        }
                        return u[t]
                    },
                    remove: function(t) {
                        if (c < Number.MAX_VALUE) {
                            var e = l[t];
                            if (!e) return;
                            e == h && (h = e.p), e == $ && ($ = e.n), o(e.n, e.p), delete l[t]
                        }
                        delete u[t], a--
                    },
                    removeAll: function() {
                        u = {}, a = 0, l = {}, h = $ = null
                    },
                    destroy: function() {
                        u = null, s = null, l = null, delete e[t]
                    },
                    info: function() {
                        return f({}, s, {
                            size: a
                        })
                    }
                }
            }
            var e = {};
            return t.info = function() {
                var t = {};
                return o(e, function(e, n) {
                    t[n] = e.info()
                }), t
            }, t.get = function(t) {
                return e[t]
            }, t
        }
    }

    function Zt() {
        this.$get = ["$cacheFactory", function(t) {
            return t("templates")
        }]
    }

    function Kt(t, r) {
        function i(t, e) {
            var n = /^\s*([@&]|=(\*?))(\??)\s*(\w*)\s*$/,
                r = {};
            return o(t, function(t, i) {
                var o = t.match(n);
                if (!o) throw Gr("iscp", "Invalid isolate scope definition for directive '{0}'. Definition: {... {1}: '{2}' ...}", e, i, t);
                r[i] = {
                    mode: o[1][0],
                    collection: "*" === o[2],
                    optional: "?" === o[3],
                    attrName: o[4] || i
                }
            }), r
        }
        var a = {},
            s = "Directive",
            c = /^\s*directive\:\s*([\w\-]+)\s+(.*)$/,
            l = /(([\w\-]+)(?:\:([^;]+))?;?)/,
            h = D("ngSrc,ngSrcset,src,srcset"),
            m = /^(?:(\^\^?)?(\?)?(\^\^?)?)?/,
            b = /^(on[a-z]+|formaction)$/;
        this.directive = function e(n, r) {
            return at(n, "directive"), w(n) ? (it(r, "directiveFactory"), a.hasOwnProperty(n) || (a[n] = [], t.factory(n + s, ["$injector", "$exceptionHandler", function(t, e) {
                var r = [];
                return o(a[n], function(o, a) {
                    try {
                        var s = t.invoke(o);
                        S(s) ? s = {
                            compile: v(s)
                        } : !s.compile && s.link && (s.compile = v(s.link)), s.priority = s.priority || 0, s.index = a, s.name = s.name || n, s.require = s.require || s.controller && s.name, s.restrict = s.restrict || "EA", y(s.scope) && (s.$$isolateBindings = i(s.scope, s.name)), r.push(s)
                    } catch (t) {
                        e(t)
                    }
                }), r
            }])), a[n].push(r)) : o(n, u(e)), this
        }, this.aHrefSanitizationWhitelist = function(t) {
            return g(t) ? (r.aHrefSanitizationWhitelist(t), this) : r.aHrefSanitizationWhitelist()
        }, this.imgSrcSanitizationWhitelist = function(t) {
            return g(t) ? (r.imgSrcSanitizationWhitelist(t), this) : r.imgSrcSanitizationWhitelist()
        };
        var x = !0;
        this.debugInfoEnabled = function(t) {
            return g(t) ? (x = t, this) : x
        }, this.$get = ["$injector", "$interpolate", "$exceptionHandler", "$templateRequest", "$parse", "$controller", "$rootScope", "$document", "$sce", "$animate", "$$sanitizeUri", function(t, r, i, u, v, g, C, A, E, O, T) {
            function M(t, e) {
                try {
                    t.addClass(e)
                } catch (t) {}
            }

            function N(t, e, n, r, i) {
                t instanceof Qn || (t = Qn(t)), o(t, function(e, n) {
                    e.nodeType == gr && e.nodeValue.match(/\S+/) && (t[n] = Qn(e).wrap("<span></span>").parent()[0])
                });
                var a = D(t, e, t, n, r, i);
                N.$$addScopeClass(t);
                var s = null;
                return function(e, n, r) {
                    it(e, "scope"), r = r || {};
                    var i = r.parentBoundTranscludeFn,
                        o = r.transcludeControllers,
                        u = r.futureParentElement;
                    i && i.$$boundTransclude && (i = i.$$boundTransclude), s || (s = V(u));
                    var c;
                    if (c = "html" !== s ? Qn(X(s, Qn("<div>").append(t).html())) : n ? Rr.clone.call(t) : t, o)
                        for (var l in o) c.data("$" + l + "Controller", o[l].instance);
                    return N.$$addScopeInfo(c, e), n && n(c, e), a && a(e, c, c, i), c
                }
            }

            function V(t) {
                var e = t && t[0];
                return e && "foreignobject" !== j(e) && e.toString().match(/SVG/) ? "svg" : "html"
            }

            function D(t, e, r, i, o, a) {
                function s(t, r, i, o) {
                    var a, s, u, c, l, f, h, $, v;
                    if (p) {
                        var m = r.length;
                        for (v = new Array(m), l = 0; l < d.length; l += 3) h = d[l], v[h] = r[h]
                    } else v = r;
                    for (l = 0, f = d.length; l < f;) u = v[d[l++]], a = d[l++], s = d[l++], a ? (a.scope ? (c = t.$new(), N.$$addScopeInfo(Qn(u), c)) : c = t, $ = a.transcludeOnThisElement ? R(t, a.transclude, o, a.elementTranscludeOnThisElement) : !a.templateOnThisElement && o ? o : !o && e ? R(t, e) : null, a(s, c, u, i, $)) : s && s(t, u.childNodes, n, o)
                }
                for (var u, c, l, f, h, $, p, d = [], v = 0; v < t.length; v++) u = new at, c = _(t[v], [], u, 0 === v ? i : n, o), l = c.length ? H(c, t[v], u, e, r, null, [], [], a) : null, l && l.scope && N.$$addScopeClass(u.$$element), h = l && l.terminal || !(f = t[v].childNodes) || !f.length ? null : D(f, l ? (l.transcludeOnThisElement || !l.templateOnThisElement) && l.transclude : e), (l || h) && (d.push(v, l, h), $ = !0, p = p || l), a = null;
                return $ ? s : null
            }

            function R(t, e, n, r) {
                var i = function(r, i, o, a, s) {
                    return r || (r = t.$new(!1, s), r.$$transcluded = !0), e(r, i, {
                        parentBoundTranscludeFn: n,
                        transcludeControllers: o,
                        futureParentElement: a
                    })
                };
                return i
            }

            function _(t, e, n, r, i) {
                var o, a, s = t.nodeType,
                    u = n.$attr;
                switch (s) {
                    case mr:
                        B(e, Xt(j(t)), "E", r, i);
                        for (var f, h, $, p, d, v, m = t.attributes, g = 0, b = m && m.length; g < b; g++) {
                            var x = !1,
                                S = !1;
                            f = m[g], h = f.name, d = fr(f.value), p = Xt(h), (v = ft.test(p)) && (h = h.replace(Jr, "").substr(8).replace(/_(.)/g, function(t, e) {
                                return e.toUpperCase()
                            }));
                            var C = p.replace(/(Start|End)$/, "");
                            W(C) && p === C + "Start" && (x = h, S = h.substr(0, h.length - 5) + "end", h = h.substr(0, h.length - 6)), $ = Xt(h.toLowerCase()), u[$] = h, !v && n.hasOwnProperty($) || (n[$] = d, Rt(t, $) && (n[$] = !0)), tt(t, e, d, $, v), B(e, $, "A", r, i, x, S)
                        }
                        if (a = t.className, y(a) && (a = a.animVal), w(a) && "" !== a)
                            for (; o = l.exec(a);) $ = Xt(o[2]), B(e, $, "C", r, i) && (n[$] = fr(o[3])), a = a.substr(o.index + o[0].length);
                        break;
                    case gr:
                        K(e, t.nodeValue);
                        break;
                    case yr:
                        try {
                            o = c.exec(t.nodeValue), o && ($ = Xt(o[1]), B(e, $, "M", r, i) && (n[$] = fr(o[2])))
                        } catch (t) {}
                }
                return e.sort(Y), e
            }

            function q(t, e, n) {
                var r = [],
                    i = 0;
                if (e && t.hasAttribute && t.hasAttribute(e)) {
                    do {
                        if (!t) throw Gr("uterdir", "Unterminated attribute, found '{0}' but no matching '{1}' found.", e, n);
                        t.nodeType == mr && (t.hasAttribute(e) && i++, t.hasAttribute(n) && i--), r.push(t), t = t.nextSibling
                    } while (i > 0)
                } else r.push(t);
                return Qn(r)
            }

            function F(t, e, n) {
                return function(r, i, o, a, s) {
                    return i = q(i[0], e, n), t(r, i, o, a, s)
                }
            }

            function H(t, a, s, u, c, l, f, h, $) {
                function p(t, e, n, r) {
                    t && (n && (t = F(t, n, r)), t.require = A.require, t.directiveName = E, (j === A || A.$$isolateScope) && (t = rt(t, {
                        isolateScope: !0
                    })), f.push(t)), e && (n && (e = F(e, n, r)), e.require = A.require, e.directiveName = E, (j === A || A.$$isolateScope) && (e = rt(e, {
                        isolateScope: !0
                    })), h.push(e))
                }

                function d(t, e, n, r) {
                    var i, a, s = "data",
                        u = !1,
                        c = n;
                    if (w(e)) {
                        if (a = e.match(m), e = e.substring(a[0].length), a[3] && (a[1] ? a[3] = null : a[1] = a[3]), "^" === a[1] ? s = "inheritedData" : "^^" === a[1] && (s = "inheritedData", c = n.parent()), "?" === a[2] && (u = !0), i = null, r && "data" === s && (i = r[e]) && (i = i.instance), i = i || c[s]("$" + e + "Controller"), !i && !u) throw Gr("ctreq", "Controller '{0}', required by directive '{1}', can't be found!", e, t);
                        return i || null
                    }
                    return lr(e) && (i = [], o(e, function(e) {
                        i.push(d(t, e, n, r))
                    })), i
                }

                function b(t, e, i, u, c) {
                    function l(t, e, r) {
                        var i;
                        return k(t) || (r = e, e = t, t = n), W && (i = b), r || (r = W ? S.parent() : S), c(t, e, i, r, T)
                    }
                    var $, p, m, y, w, b, x, S, A;
                    if (a === i ? (A = s, S = s.$$element) : (S = Qn(i), A = new at(S, s)), j && (w = e.$new(!0)), c && (x = l, x.$$boundTransclude = c), D && (C = {}, b = {}, o(D, function(t) {
                            var n, r = {
                                $scope: t === j || t.$$isolateScope ? w : e,
                                $element: S,
                                $attrs: A,
                                $transclude: x
                            };
                            y = t.controller, "@" == y && (y = A[t.name]), n = g(y, r, !0, t.controllerAs), b[t.name] = n, W || S.data("$" + t.name + "Controller", n.instance), C[t.name] = n
                        })), j) {
                        N.$$addScopeInfo(S, w, !0, !(P && (P === j || P === j.$$originalDirective))), N.$$addScopeClass(S, !0);
                        var E = C && C[j.name],
                            O = w;
                        E && E.identifier && j.bindToController === !0 && (O = E.instance), o(w.$$isolateBindings = j.$$isolateBindings, function(t, n) {
                            var i, o, a, s, u = t.attrName,
                                c = t.optional,
                                l = t.mode;
                            switch (l) {
                                case "@":
                                    A.$observe(u, function(t) {
                                        O[n] = t
                                    }), A.$$observers[u].$$scope = e, A[u] && (O[n] = r(A[u])(e));
                                    break;
                                case "=":
                                    if (c && !A[u]) return;
                                    o = v(A[u]), s = o.literal ? I : function(t, e) {
                                        return t === e || t !== t && e !== e
                                    }, a = o.assign || function() {
                                        throw i = O[n] = o(e), Gr("nonassign", "Expression '{0}' used with directive '{1}' is non-assignable!", A[u], j.name)
                                    }, i = O[n] = o(e);
                                    var f = function(t) {
                                        return s(t, O[n]) || (s(t, i) ? a(e, t = O[n]) : O[n] = t), i = t
                                    };
                                    f.$stateful = !0;
                                    var h;
                                    h = t.collection ? e.$watchCollection(A[u], f) : e.$watch(v(A[u], f), null, o.literal), w.$on("$destroy", h);
                                    break;
                                case "&":
                                    o = v(A[u]), O[n] = function(t) {
                                        return o(e, t)
                                    }
                            }
                        })
                    }
                    for (C && (o(C, function(t) {
                            t()
                        }), C = null), $ = 0, p = f.length; $ < p; $++) m = f[$], ot(m, m.isolateScope ? w : e, S, A, m.require && d(m.directiveName, m.require, S, b), x);
                    var T = e;
                    for (j && (j.template || null === j.templateUrl) && (T = w), t && t(T, i.childNodes, n, c), $ = h.length - 1; $ >= 0; $--) m = h[$], ot(m, m.isolateScope ? w : e, S, A, m.require && d(m.directiveName, m.require, S, b), x)
                }
                $ = $ || {};
                for (var x, C, A, E, O, T, M, V = -Number.MAX_VALUE, D = $.controllerDirectives, j = $.newIsolateScopeDirective, P = $.templateDirective, R = $.nonTlbTranscludeDirective, H = !1, B = !1, W = $.hasElementTranscludeDirective, Y = s.$$element = Qn(a), K = l, Q = u, tt = 0, nt = t.length; tt < nt; tt++) {
                    A = t[tt];
                    var it = A.$$start,
                        st = A.$$end;
                    if (it && (Y = q(a, it, st)), O = n, V > A.priority) break;
                    if ((M = A.scope) && (A.templateUrl || (y(M) ? (Z("new/isolated scope", j || x, A, Y), j = A) : Z("new/isolated scope", j, A, Y)), x = x || A), E = A.name, !A.templateUrl && A.controller && (M = A.controller, D = D || {}, Z("'" + E + "' controller", D[E], A, Y), D[E] = A), (M = A.transclude) && (H = !0, A.$$tlb || (Z("transclusion", R, A, Y), R = A), "element" == M ? (W = !0, V = A.priority, O = Y, Y = s.$$element = Qn(e.createComment(" " + E + ": " + s[E] + " ")), a = Y[0], et(c, U(O), a), Q = N(O, u, V, K && K.name, {
                            nonTlbTranscludeDirective: R
                        })) : (O = Qn(bt(a)).contents(), Y.empty(), Q = N(O, u))), A.template)
                        if (B = !0, Z("template", P, A, Y), P = A, M = S(A.template) ? A.template(Y, s) : A.template, M = lt(M), A.replace) {
                            if (K = A, O = vt(M) ? [] : te(X(A.templateNamespace, fr(M))), a = O[0], 1 != O.length || a.nodeType !== mr) throw Gr("tplrt", "Template for directive '{0}' must have exactly one root element. {1}", E, "");
                            et(c, Y, a);
                            var ut = {
                                    $attr: {}
                                },
                                ct = _(a, [], ut),
                                ft = t.splice(tt + 1, t.length - (tt + 1));
                            j && L(ct), t = t.concat(ct).concat(ft), G(s, ut), nt = t.length
                        } else Y.html(M);
                    if (A.templateUrl) B = !0, Z("template", P, A, Y), P = A, A.replace && (K = A), b = J(t.splice(tt, t.length - tt), Y, s, c, H && Q, f, h, {
                        controllerDirectives: D,
                        newIsolateScopeDirective: j,
                        templateDirective: P,
                        nonTlbTranscludeDirective: R
                    }), nt = t.length;
                    else if (A.compile) try {
                        T = A.compile(Y, s, Q), S(T) ? p(null, T, it, st) : T && p(T.pre, T.post, it, st)
                    } catch (t) {
                        i(t, z(Y))
                    }
                    A.terminal && (b.terminal = !0, V = Math.max(V, A.priority))
                }
                return b.scope = x && x.scope === !0, b.transcludeOnThisElement = H, b.elementTranscludeOnThisElement = W, b.templateOnThisElement = B, b.transclude = Q, $.hasElementTranscludeDirective = W, b
            }

            function L(t) {
                for (var e = 0, n = t.length; e < n; e++) t[e] = $(t[e], {
                    $$isolateScope: !0
                })
            }

            function B(e, r, o, u, c, l, f) {
                if (r === c) return null;
                var h = null;
                if (a.hasOwnProperty(r))
                    for (var p, d = t.get(r + s), v = 0, m = d.length; v < m; v++) try {
                        p = d[v], (u === n || u > p.priority) && p.restrict.indexOf(o) != -1 && (l && (p = $(p, {
                            $$start: l,
                            $$end: f
                        })), e.push(p), h = p)
                    } catch (t) {
                        i(t)
                    }
                return h;
            }

            function W(e) {
                if (a.hasOwnProperty(e))
                    for (var n, r = t.get(e + s), i = 0, o = r.length; i < o; i++)
                        if (n = r[i], n.multiElement) return !0;
                return !1
            }

            function G(t, e) {
                var n = e.$attr,
                    r = t.$attr,
                    i = t.$$element;
                o(t, function(r, i) {
                    "$" != i.charAt(0) && (e[i] && e[i] !== r && (r += ("style" === i ? ";" : " ") + e[i]), t.$set(i, r, !0, n[i]))
                }), o(e, function(e, o) {
                    "class" == o ? (M(i, e), t.class = (t.class ? t.class + " " : "") + e) : "style" == o ? (i.attr("style", i.attr("style") + ";" + e), t.style = (t.style ? t.style + ";" : "") + e) : "$" == o.charAt(0) || t.hasOwnProperty(o) || (t[o] = e, r[o] = n[o])
                })
            }

            function J(t, e, n, r, i, a, s, c) {
                var l, f, h = [],
                    p = e[0],
                    d = t.shift(),
                    v = $(d, {
                        templateUrl: null,
                        transclude: null,
                        replace: null,
                        $$originalDirective: d
                    }),
                    m = S(d.templateUrl) ? d.templateUrl(e, n) : d.templateUrl,
                    g = d.templateNamespace;
                return e.empty(), u(E.getTrustedResourceUrl(m)).then(function(u) {
                        var $, w, b, x;
                        if (u = lt(u), d.replace) {
                            if (b = vt(u) ? [] : te(X(g, fr(u))), $ = b[0], 1 != b.length || $.nodeType !== mr) throw Gr("tplrt", "Template for directive '{0}' must have exactly one root element. {1}", d.name, m);
                            w = {
                                $attr: {}
                            }, et(r, e, $);
                            var S = _($, [], w);
                            y(d.scope) && L(S), t = S.concat(t), G(n, w)
                        } else $ = p, e.html(u);
                        for (t.unshift(v), l = H(t, $, n, i, e, d, a, s, c), o(r, function(t, n) {
                                t == $ && (r[n] = e[0])
                            }), f = D(e[0].childNodes, i); h.length;) {
                            var C = h.shift(),
                                A = h.shift(),
                                k = h.shift(),
                                E = h.shift(),
                                O = e[0];
                            if (!C.$$destroyed) {
                                if (A !== p) {
                                    var T = A.className;
                                    c.hasElementTranscludeDirective && d.replace || (O = bt($)), et(k, Qn(A), O), M(Qn(O), T)
                                }
                                x = l.transcludeOnThisElement ? R(C, l.transclude, E) : E, l(f, C, O, r, x)
                            }
                        }
                        h = null
                    }),
                    function(t, e, n, r, i) {
                        var o = i;
                        e.$$destroyed || (h ? h.push(e, n, r, o) : (l.transcludeOnThisElement && (o = R(e, l.transclude, i)), l(f, e, n, r, o)))
                    }
            }

            function Y(t, e) {
                var n = e.priority - t.priority;
                return 0 !== n ? n : t.name !== e.name ? t.name < e.name ? -1 : 1 : t.index - e.index
            }

            function Z(t, e, n, r) {
                if (e) throw Gr("multidir", "Multiple directives [{0}, {1}] asking for {2} on: {3}", e.name, n.name, t, z(r))
            }

            function K(t, e) {
                var n = r(e, !0);
                n && t.push({
                    priority: 0,
                    compile: function(t) {
                        var e = t.parent(),
                            r = !!e.length;
                        return r && N.$$addBindingClass(e),
                            function(t, e) {
                                var i = e.parent();
                                r || N.$$addBindingClass(i), N.$$addBindingInfo(i, n.expressions), t.$watch(n, function(t) {
                                    e[0].nodeValue = t
                                })
                            }
                    }
                })
            }

            function X(t, n) {
                switch (t = Gn(t || "html")) {
                    case "svg":
                    case "math":
                        var r = e.createElement("div");
                        return r.innerHTML = "<" + t + ">" + n + "</" + t + ">", r.childNodes[0].childNodes;
                    default:
                        return n
                }
            }

            function Q(t, e) {
                if ("srcdoc" == e) return E.HTML;
                var n = j(t);
                return "xlinkHref" == e || "form" == n && "action" == e || "img" != n && ("src" == e || "ngSrc" == e) ? E.RESOURCE_URL : void 0
            }

            function tt(t, e, n, i, o) {
                var a = Q(t, i);
                o = h[i] || o;
                var s = r(n, !0, a, o);
                if (s) {
                    if ("multiple" === i && "select" === j(t)) throw Gr("selmulti", "Binding to the 'multiple' attribute is not supported. Element: {0}", z(t));
                    e.push({
                        priority: 100,
                        compile: function() {
                            return {
                                pre: function(t, e, u) {
                                    var c = u.$$observers || (u.$$observers = {});
                                    if (b.test(i)) throw Gr("nodomevents", "Interpolations for HTML DOM event attributes are disallowed.  Please use the ng- versions (such as ng-click instead of onclick) instead.");
                                    var l = u[i];
                                    l !== n && (s = l && r(l, !0, a, o), n = l), s && (u[i] = s(t), (c[i] || (c[i] = [])).$$inter = !0, (u.$$observers && u.$$observers[i].$$scope || t).$watch(s, function(t, e) {
                                        "class" === i && t != e ? u.$updateClass(t, e) : u.$set(i, t)
                                    }))
                                }
                            }
                        }
                    })
                }
            }

            function et(t, n, r) {
                var i, o, a = n[0],
                    s = n.length,
                    u = a.parentNode;
                if (t)
                    for (i = 0, o = t.length; i < o; i++)
                        if (t[i] == a) {
                            t[i++] = r;
                            for (var c = i, l = c + s - 1, f = t.length; c < f; c++, l++) l < f ? t[c] = t[l] : delete t[c];
                            t.length -= s - 1, t.context === a && (t.context = r);
                            break
                        }
                u && u.replaceChild(r, a);
                var h = e.createDocumentFragment();
                h.appendChild(a), Qn(r).data(Qn(a).data()), tr ? (cr = !0, tr.cleanData([a])) : delete Qn.cache[a[Qn.expando]];
                for (var $ = 1, p = n.length; $ < p; $++) {
                    var d = n[$];
                    Qn(d).remove(), h.appendChild(d), delete n[$]
                }
                n[0] = r, n.length = 1
            }

            function rt(t, e) {
                return f(function() {
                    return t.apply(null, arguments)
                }, t, e)
            }

            function ot(t, e, n, r, o, a) {
                try {
                    t(e, n, r, o, a)
                } catch (t) {
                    i(t, z(n))
                }
            }
            var at = function(t, e) {
                if (e) {
                    var n, r, i, o = Object.keys(e);
                    for (n = 0, r = o.length; n < r; n++) i = o[n], this[i] = e[i]
                } else this.$attr = {};
                this.$$element = t
            };
            at.prototype = {
                $normalize: Xt,
                $addClass: function(t) {
                    t && t.length > 0 && O.addClass(this.$$element, t)
                },
                $removeClass: function(t) {
                    t && t.length > 0 && O.removeClass(this.$$element, t)
                },
                $updateClass: function(t, e) {
                    var n = Qt(t, e);
                    n && n.length && O.addClass(this.$$element, n);
                    var r = Qt(e, t);
                    r && r.length && O.removeClass(this.$$element, r)
                },
                $set: function(t, e, r, a) {
                    var s, u = this.$$element[0],
                        c = Rt(u, t),
                        l = _t(u, t),
                        f = t;
                    if (c ? (this.$$element.prop(t, e), a = c) : l && (this[l] = e, f = l), this[t] = e, a ? this.$attr[t] = a : (a = this.$attr[t], a || (this.$attr[t] = a = nt(t, "-"))), s = j(this.$$element), "a" === s && "href" === t || "img" === s && "src" === t) this[t] = e = T(e, "src" === t);
                    else if ("img" === s && "srcset" === t) {
                        for (var h = "", $ = fr(e), p = /(\s+\d+x\s*,|\s+\d+w\s*,|\s+,|,\s+)/, d = /\s/.test($) ? p : /(,)/, v = $.split(d), m = Math.floor(v.length / 2), g = 0; g < m; g++) {
                            var y = 2 * g;
                            h += T(fr(v[y]), !0), h += " " + fr(v[y + 1])
                        }
                        var w = fr(v[2 * g]).split(/\s/);
                        h += T(fr(w[0]), !0), 2 === w.length && (h += " " + fr(w[1])), this[t] = e = h
                    }
                    r !== !1 && (null === e || e === n ? this.$$element.removeAttr(a) : this.$$element.attr(a, e));
                    var b = this.$$observers;
                    b && o(b[f], function(t) {
                        try {
                            t(e)
                        } catch (t) {
                            i(t)
                        }
                    })
                },
                $observe: function(t, e) {
                    var n = this,
                        r = n.$$observers || (n.$$observers = ct()),
                        i = r[t] || (r[t] = []);
                    return i.push(e), C.$evalAsync(function() {
                            !i.$$inter && n.hasOwnProperty(t) && e(n[t])
                        }),
                        function() {
                            P(i, e)
                        }
                }
            };
            var st = r.startSymbol(),
                ut = r.endSymbol(),
                lt = "{{" == st || "}}" == ut ? d : function(t) {
                    return t.replace(/\{\{/g, st).replace(/}}/g, ut)
                },
                ft = /^ngAttr[A-Z]/;
            return N.$$addBindingInfo = x ? function(t, e) {
                var n = t.data("$binding") || [];
                lr(e) ? n = n.concat(e) : n.push(e), t.data("$binding", n)
            } : p, N.$$addBindingClass = x ? function(t) {
                M(t, "ng-binding")
            } : p, N.$$addScopeInfo = x ? function(t, e, n, r) {
                var i = n ? r ? "$isolateScopeNoTemplate" : "$isolateScope" : "$scope";
                t.data(i, e)
            } : p, N.$$addScopeClass = x ? function(t, e) {
                M(t, e ? "ng-isolate-scope" : "ng-scope")
            } : p, N
        }]
    }

    function Xt(t) {
        return dt(t.replace(Jr, ""))
    }

    function Qt(t, e) {
        var n = "",
            r = t.split(/\s+/),
            i = e.split(/\s+/);
        t: for (var o = 0; o < r.length; o++) {
            for (var a = r[o], s = 0; s < i.length; s++)
                if (a == i[s]) continue t;
            n += (n.length > 0 ? " " : "") + a
        }
        return n
    }

    function te(t) {
        t = Qn(t);
        var e = t.length;
        if (e <= 1) return t;
        for (; e--;) {
            var n = t[e];
            n.nodeType === yr && rr.call(t, e, 1)
        }
        return t
    }

    function ee() {
        var t = {},
            e = !1,
            i = /^(\S+)(\s+as\s+(\w+))?$/;
        this.register = function(e, n) {
            at(e, "controller"), y(e) ? f(t, e) : t[e] = n
        }, this.allowGlobals = function() {
            e = !0
        }, this.$get = ["$injector", "$window", function(o, a) {
            function s(t, e, n, i) {
                if (!t || !y(t.$scope)) throw r("$controller")("noscp", "Cannot export controller '{0}' as '{1}'! No $scope object provided via `locals`.", i, e);
                t.$scope[e] = n
            }
            return function(r, u, c, l) {
                var h, $, p, d;
                if (c = c === !0, l && w(l) && (d = l), w(r)) {
                    if ($ = r.match(i), !$) throw Yr("ctrlfmt", "Badly formed controller string '{0}'. Must match `__name__ as __id__` or `__name__`.", r);
                    p = $[1], d = d || $[3], r = t.hasOwnProperty(p) ? t[p] : st(u.$scope, p, !0) || (e ? st(a, p, !0) : n), ot(r, p, !0)
                }
                if (c) {
                    var v = (lr(r) ? r[r.length - 1] : r).prototype;
                    return h = Object.create(v || null), d && s(u, d, h, p || r.name), f(function() {
                        return o.invoke(r, h, u, p), h
                    }, {
                        instance: h,
                        identifier: d
                    })
                }
                return h = o.instantiate(r, u, p), d && s(u, d, h, p || r.name), h
            }
        }]
    }

    function ne() {
        this.$get = ["$window", function(t) {
            return Qn(t.document)
        }]
    }

    function re() {
        this.$get = ["$log", function(t) {
            return function(e, n) {
                t.error.apply(t, arguments)
            }
        }]
    }

    function ie(t, e) {
        if (w(t)) {
            var n = t.replace(ti, "").trim();
            if (n) {
                var r = e("Content-Type");
                (r && 0 === r.indexOf(Zr) || oe(n)) && (t = B(n))
            }
        }
        return t
    }

    function oe(t) {
        var e = t.match(Xr);
        return e && Qr[e[0]].test(t)
    }

    function ae(t) {
        var e, n, r, i = ct();
        return t ? (o(t.split("\n"), function(t) {
            r = t.indexOf(":"), e = Gn(fr(t.substr(0, r))), n = fr(t.substr(r + 1)), e && (i[e] = i[e] ? i[e] + ", " + n : n)
        }), i) : i
    }

    function se(t) {
        var e = y(t) ? t : n;
        return function(n) {
            if (e || (e = ae(t)), n) {
                var r = e[Gn(n)];
                return void 0 === r && (r = null), r
            }
            return e
        }
    }

    function ue(t, e, n, r) {
        return S(r) ? r(t, e, n) : (o(r, function(r) {
            t = r(t, e, n)
        }), t)
    }

    function ce(t) {
        return 200 <= t && t < 300
    }

    function le() {
        var t = this.defaults = {
                transformResponse: [ie],
                transformRequest: [function(t) {
                    return !y(t) || E(t) || T(t) || O(t) ? t : L(t)
                }],
                headers: {
                    common: {
                        Accept: "application/json, text/plain, */*"
                    },
                    post: _(Kr),
                    put: _(Kr),
                    patch: _(Kr)
                },
                xsrfCookieName: "XSRF-TOKEN",
                xsrfHeaderName: "X-XSRF-TOKEN"
            },
            e = !1;
        this.useApplyAsync = function(t) {
            return g(t) ? (e = !!t, this) : e
        };
        var i = this.interceptors = [];
        this.$get = ["$httpBackend", "$browser", "$cacheFactory", "$rootScope", "$q", "$injector", function(a, u, c, l, h, $) {
            function p(e) {
                function i(t) {
                    var e = f({}, t);
                    return t.data ? e.data = ue(t.data, t.headers, t.status, u.transformResponse) : e.data = t.data, ce(t.status) ? e : h.reject(e)
                }

                function a(t) {
                    var e, n = {};
                    return o(t, function(t, r) {
                        S(t) ? (e = t(), null != e && (n[r] = e)) : n[r] = t
                    }), n
                }

                function s(e) {
                    var n, r, i, o = t.headers,
                        s = f({}, e.headers);
                    o = f({}, o.common, o[Gn(e.method)]);
                    t: for (n in o) {
                        r = Gn(n);
                        for (i in s)
                            if (Gn(i) === r) continue t;
                        s[n] = o[n]
                    }
                    return a(s)
                }
                if (!sr.isObject(e)) throw r("$http")("badreq", "Http request configuration must be an object.  Received: {0}", e);
                var u = f({
                    method: "get",
                    transformRequest: t.transformRequest,
                    transformResponse: t.transformResponse
                }, e);
                u.headers = s(e), u.method = Yn(u.method);
                var c = function(e) {
                        var r = e.headers,
                            a = ue(e.data, se(r), n, e.transformRequest);
                        return m(a) && o(r, function(t, e) {
                            "content-type" === Gn(e) && delete r[e]
                        }), m(e.withCredentials) && !m(t.withCredentials) && (e.withCredentials = t.withCredentials), b(e, a).then(i, i)
                    },
                    l = [c, n],
                    $ = h.when(u);
                for (o(k, function(t) {
                        (t.request || t.requestError) && l.unshift(t.request, t.requestError), (t.response || t.responseError) && l.push(t.response, t.responseError)
                    }); l.length;) {
                    var p = l.shift(),
                        d = l.shift();
                    $ = $.then(p, d)
                }
                return $.success = function(t) {
                    return $.then(function(e) {
                        t(e.data, e.status, e.headers, u)
                    }), $
                }, $.error = function(t) {
                    return $.then(null, function(e) {
                        t(e.data, e.status, e.headers, u)
                    }), $
                }, $
            }

            function d(t) {
                o(arguments, function(t) {
                    p[t] = function(e, n) {
                        return p(f(n || {}, {
                            method: t,
                            url: e
                        }))
                    }
                })
            }

            function v(t) {
                o(arguments, function(t) {
                    p[t] = function(e, n, r) {
                        return p(f(r || {}, {
                            method: t,
                            url: e,
                            data: n
                        }))
                    }
                })
            }

            function b(r, i) {
                function o(t, n, r, i) {
                    function o() {
                        s(n, t, r, i)
                    }
                    $ && (ce(t) ? $.put(x, [t, n, ae(r), i]) : $.remove(x)), e ? l.$applyAsync(o) : (o(), l.$$phase || l.$apply())
                }

                function s(t, e, n, i) {
                    e = Math.max(e, 0), (ce(e) ? v.resolve : v.reject)({
                        data: t,
                        status: e,
                        headers: se(n),
                        config: r,
                        statusText: i
                    })
                }

                function c(t) {
                    s(t.data, t.status, _(t.headers()), t.statusText)
                }

                function f() {
                    var t = p.pendingRequests.indexOf(r);
                    t !== -1 && p.pendingRequests.splice(t, 1)
                }
                var $, d, v = h.defer(),
                    w = v.promise,
                    b = r.headers,
                    x = C(r.url, r.params);
                if (p.pendingRequests.push(r), w.then(f, f), !r.cache && !t.cache || r.cache === !1 || "GET" !== r.method && "JSONP" !== r.method || ($ = y(r.cache) ? r.cache : y(t.cache) ? t.cache : A), $ && (d = $.get(x), g(d) ? N(d) ? d.then(c, c) : lr(d) ? s(d[1], d[0], _(d[2]), d[3]) : s(d, 200, {}, "OK") : $.put(x, w)), m(d)) {
                    var S = on(r.url) ? u.cookies()[r.xsrfCookieName || t.xsrfCookieName] : n;
                    S && (b[r.xsrfHeaderName || t.xsrfHeaderName] = S), a(r.method, x, i, o, b, r.timeout, r.withCredentials, r.responseType)
                }
                return w
            }

            function C(t, e) {
                if (!e) return t;
                var n = [];
                return s(e, function(t, e) {
                    null === t || m(t) || (lr(t) || (t = [t]), o(t, function(t) {
                        y(t) && (t = x(t) ? t.toISOString() : L(t)), n.push(Z(e) + "=" + Z(t))
                    }))
                }), n.length > 0 && (t += (t.indexOf("?") == -1 ? "?" : "&") + n.join("&")), t
            }
            var A = c("$http"),
                k = [];
            return o(i, function(t) {
                k.unshift(w(t) ? $.get(t) : $.invoke(t))
            }), p.pendingRequests = [], d("get", "delete", "head", "jsonp"), v("post", "put", "patch"), p.defaults = t, p
        }]
    }

    function fe() {
        return new t.XMLHttpRequest
    }

    function he() {
        this.$get = ["$browser", "$window", "$document", function(t, e, n) {
            return $e(t, fe, t.defer, e.angular.callbacks, n[0])
        }]
    }

    function $e(t, e, r, i, a) {
        function s(t, e, n) {
            var r = a.createElement("script"),
                o = null;
            return r.type = "text/javascript", r.src = t, r.async = !0, o = function(t) {
                kr(r, "load", o), kr(r, "error", o), a.body.removeChild(r), r = null;
                var s = -1,
                    u = "unknown";
                t && ("load" !== t.type || i[e].called || (t = {
                    type: "error"
                }), u = t.type, s = "error" === t.type ? 404 : 200), n && n(s, u)
            }, Ar(r, "load", o), Ar(r, "error", o), a.body.appendChild(r), o
        }
        return function(a, u, c, l, f, h, $, d) {
            function v() {
                w && w(), b && b.abort()
            }

            function m(e, i, o, a, s) {
                S !== n && r.cancel(S), w = b = null, e(i, o, a, s), t.$$completeOutstandingRequest(p)
            }
            if (t.$$incOutstandingRequestCount(), u = u || t.url(), "jsonp" == Gn(a)) {
                var y = "_" + (i.counter++).toString(36);
                i[y] = function(t) {
                    i[y].data = t, i[y].called = !0
                };
                var w = s(u.replace("JSON_CALLBACK", "angular.callbacks." + y), y, function(t, e) {
                    m(l, t, i[y].data, "", e), i[y] = p
                })
            } else {
                var b = e();
                b.open(a, u, !0), o(f, function(t, e) {
                    g(t) && b.setRequestHeader(e, t)
                }), b.onload = function() {
                    var t = b.statusText || "",
                        e = "response" in b ? b.response : b.responseText,
                        n = 1223 === b.status ? 204 : b.status;
                    0 === n && (n = e ? 200 : "file" == rn(u).protocol ? 404 : 0), m(l, n, e, b.getAllResponseHeaders(), t)
                };
                var x = function() {
                    m(l, -1, null, null, "")
                };
                if (b.onerror = x, b.onabort = x, $ && (b.withCredentials = !0), d) try {
                    b.responseType = d
                } catch (t) {
                    if ("json" !== d) throw t
                }
                b.send(c || null)
            }
            if (h > 0) var S = r(v, h);
            else N(h) && h.then(v)
        }
    }

    function pe() {
        var t = "{{",
            e = "}}";
        this.startSymbol = function(e) {
            return e ? (t = e, this) : t
        }, this.endSymbol = function(t) {
            return t ? (e = t, this) : e
        }, this.$get = ["$parse", "$exceptionHandler", "$sce", function(n, r, i) {
            function o(t) {
                return "\\\\\\" + t
            }

            function a(o, a, h, $) {
                function p(n) {
                    return n.replace(c, t).replace(l, e)
                }

                function d(t) {
                    try {
                        return t = T(t), $ && !g(t) ? t : M(t)
                    } catch (t) {
                        var e = ei("interr", "Can't interpolate: {0}\n{1}", o, t.toString());
                        r(e)
                    }
                }
                $ = !!$;
                for (var v, y, w, b = 0, x = [], C = [], A = o.length, k = [], E = []; b < A;) {
                    if ((v = o.indexOf(t, b)) == -1 || (y = o.indexOf(e, v + s)) == -1) {
                        b !== A && k.push(p(o.substring(b)));
                        break
                    }
                    b !== v && k.push(p(o.substring(b, v))), w = o.substring(v + s, y), x.push(w), C.push(n(w, d)), b = y + u, E.push(k.length), k.push("")
                }
                if (h && k.length > 1) throw ei("noconcat", "Error while interpolating: {0}\nStrict Contextual Escaping disallows interpolations that concatenate multiple expressions when a trusted value is required.  See http://docs.angularjs.org/api/ng.$sce", o);
                if (!a || x.length) {
                    var O = function(t) {
                            for (var e = 0, n = x.length; e < n; e++) {
                                if ($ && m(t[e])) return;
                                k[E[e]] = t[e]
                            }
                            return k.join("")
                        },
                        T = function(t) {
                            return h ? i.getTrusted(h, t) : i.valueOf(t)
                        },
                        M = function(t) {
                            if (null == t) return "";
                            switch (typeof t) {
                                case "string":
                                    break;
                                case "number":
                                    t = "" + t;
                                    break;
                                default:
                                    t = L(t)
                            }
                            return t
                        };
                    return f(function(t) {
                        var e = 0,
                            n = x.length,
                            i = new Array(n);
                        try {
                            for (; e < n; e++) i[e] = C[e](t);
                            return O(i)
                        } catch (t) {
                            var a = ei("interr", "Can't interpolate: {0}\n{1}", o, t.toString());
                            r(a)
                        }
                    }, {
                        exp: o,
                        expressions: x,
                        $$watchDelegate: function(t, e, n) {
                            var r;
                            return t.$watchGroup(C, function(n, i) {
                                var o = O(n);
                                S(e) && e.call(this, o, n !== i ? r : o, t), r = o
                            }, n)
                        }
                    })
                }
            }
            var s = t.length,
                u = e.length,
                c = new RegExp(t.replace(/./g, o), "g"),
                l = new RegExp(e.replace(/./g, o), "g");
            return a.startSymbol = function() {
                return t
            }, a.endSymbol = function() {
                return e
            }, a
        }]
    }

    function de() {
        this.$get = ["$rootScope", "$window", "$q", "$$q", function(t, e, n, r) {
            function i(i, a, s, u) {
                var c = e.setInterval,
                    l = e.clearInterval,
                    f = 0,
                    h = g(u) && !u,
                    $ = (h ? r : n).defer(),
                    p = $.promise;
                return s = g(s) ? s : 0, p.then(null, null, i), p.$$intervalId = c(function() {
                    $.notify(f++), s > 0 && f >= s && ($.resolve(f), l(p.$$intervalId), delete o[p.$$intervalId]), h || t.$apply()
                }, a), o[p.$$intervalId] = $, p
            }
            var o = {};
            return i.cancel = function(t) {
                return !!(t && t.$$intervalId in o) && (o[t.$$intervalId].reject("canceled"), e.clearInterval(t.$$intervalId), delete o[t.$$intervalId], !0)
            }, i
        }]
    }

    function ve() {
        this.$get = function() {
            return {
                id: "en-us",
                NUMBER_FORMATS: {
                    DECIMAL_SEP: ".",
                    GROUP_SEP: ",",
                    PATTERNS: [{
                        minInt: 1,
                        minFrac: 0,
                        maxFrac: 3,
                        posPre: "",
                        posSuf: "",
                        negPre: "-",
                        negSuf: "",
                        gSize: 3,
                        lgSize: 3
                    }, {
                        minInt: 1,
                        minFrac: 2,
                        maxFrac: 2,
                        posPre: "¤",
                        posSuf: "",
                        negPre: "(¤",
                        negSuf: ")",
                        gSize: 3,
                        lgSize: 3
                    }],
                    CURRENCY_SYM: "$"
                },
                DATETIME_FORMATS: {
                    MONTH: "January,February,March,April,May,June,July,August,September,October,November,December".split(","),
                    SHORTMONTH: "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),
                    DAY: "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),
                    SHORTDAY: "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),
                    AMPMS: ["AM", "PM"],
                    medium: "MMM d, y h:mm:ss a",
                    short: "M/d/yy h:mm a",
                    fullDate: "EEEE, MMMM d, y",
                    longDate: "MMMM d, y",
                    mediumDate: "MMM d, y",
                    shortDate: "M/d/yy",
                    mediumTime: "h:mm:ss a",
                    shortTime: "h:mm a"
                },
                pluralCat: function(t) {
                    return 1 === t ? "one" : "other"
                }
            }
        }
    }

    function me(t) {
        for (var e = t.split("/"), n = e.length; n--;) e[n] = Y(e[n]);
        return e.join("/")
    }

    function ge(t, e) {
        var n = rn(t);
        e.$$protocol = n.protocol, e.$$host = n.hostname, e.$$port = h(n.port) || ri[n.protocol] || null
    }

    function ye(t, e) {
        var n = "/" !== t.charAt(0);
        n && (t = "/" + t);
        var r = rn(t);
        e.$$path = decodeURIComponent(n && "/" === r.pathname.charAt(0) ? r.pathname.substring(1) : r.pathname), e.$$search = G(r.search), e.$$hash = decodeURIComponent(r.hash), e.$$path && "/" != e.$$path.charAt(0) && (e.$$path = "/" + e.$$path)
    }

    function we(t, e) {
        if (0 === e.indexOf(t)) return e.substr(t.length)
    }

    function be(t) {
        var e = t.indexOf("#");
        return e == -1 ? t : t.substr(0, e)
    }

    function xe(t) {
        return t.replace(/(#.+)|#$/, "$1")
    }

    function Se(t) {
        return t.substr(0, be(t).lastIndexOf("/") + 1)
    }

    function Ce(t) {
        return t.substring(0, t.indexOf("/", t.indexOf("//") + 2))
    }

    function Ae(t, e) {
        this.$$html5 = !0, e = e || "";
        var r = Se(t);
        ge(t, this), this.$$parse = function(t) {
            var e = we(r, t);
            if (!w(e)) throw ii("ipthprfx", 'Invalid url "{0}", missing path prefix "{1}".', t, r);
            ye(e, this), this.$$path || (this.$$path = "/"), this.$$compose()
        }, this.$$compose = function() {
            var t = J(this.$$search),
                e = this.$$hash ? "#" + Y(this.$$hash) : "";
            this.$$url = me(this.$$path) + (t ? "?" + t : "") + e, this.$$absUrl = r + this.$$url.substr(1)
        }, this.$$parseLinkUrl = function(i, o) {
            if (o && "#" === o[0]) return this.hash(o.slice(1)), !0;
            var a, s, u;
            return (a = we(t, i)) !== n ? (s = a, u = (a = we(e, a)) !== n ? r + (we("/", a) || a) : t + s) : (a = we(r, i)) !== n ? u = r + a : r == i + "/" && (u = r), u && this.$$parse(u), !!u
        }
    }

    function ke(t, e) {
        var n = Se(t);
        ge(t, this), this.$$parse = function(r) {
            function i(t, e, n) {
                var r, i = /^\/[A-Z]:(\/.*)/;
                return 0 === e.indexOf(n) && (e = e.replace(n, "")), i.exec(e) ? t : (r = i.exec(t), r ? r[1] : t)
            }
            var o, a = we(t, r) || we(n, r);
            "#" === a.charAt(0) ? (o = we(e, a), m(o) && (o = a)) : o = this.$$html5 ? a : "", ye(o, this), this.$$path = i(this.$$path, o, t), this.$$compose()
        }, this.$$compose = function() {
            var n = J(this.$$search),
                r = this.$$hash ? "#" + Y(this.$$hash) : "";
            this.$$url = me(this.$$path) + (n ? "?" + n : "") + r, this.$$absUrl = t + (this.$$url ? e + this.$$url : "")
        }, this.$$parseLinkUrl = function(e, n) {
            return be(t) == be(e) && (this.$$parse(e), !0)
        }
    }

    function Ee(t, e) {
        this.$$html5 = !0, ke.apply(this, arguments);
        var n = Se(t);
        this.$$parseLinkUrl = function(r, i) {
            if (i && "#" === i[0]) return this.hash(i.slice(1)), !0;
            var o, a;
            return t == be(r) ? o = r : (a = we(n, r)) ? o = t + e + a : n === r + "/" && (o = n), o && this.$$parse(o), !!o
        }, this.$$compose = function() {
            var n = J(this.$$search),
                r = this.$$hash ? "#" + Y(this.$$hash) : "";
            this.$$url = me(this.$$path) + (n ? "?" + n : "") + r, this.$$absUrl = t + e + this.$$url
        }
    }

    function Oe(t) {
        return function() {
            return this[t]
        }
    }

    function Te(t, e) {
        return function(n) {
            return m(n) ? this[t] : (this[t] = e(n), this.$$compose(), this)
        }
    }

    function Me() {
        var t = "",
            e = {
                enabled: !1,
                requireBase: !0,
                rewriteLinks: !0
            };
        this.hashPrefix = function(e) {
            return g(e) ? (t = e, this) : t
        }, this.html5Mode = function(t) {
            return M(t) ? (e.enabled = t, this) : y(t) ? (M(t.enabled) && (e.enabled = t.enabled), M(t.requireBase) && (e.requireBase = t.requireBase), M(t.rewriteLinks) && (e.rewriteLinks = t.rewriteLinks), this) : e
        }, this.$get = ["$rootScope", "$browser", "$sniffer", "$rootElement", "$window", function(n, r, i, o, a) {
            function s(t, e, n) {
                var i = c.url(),
                    o = c.$$state;
                try {
                    r.url(t, e, n), c.$$state = r.state()
                } catch (t) {
                    throw c.url(i), c.$$state = o, t
                }
            }

            function u(t, e) {
                n.$broadcast("$locationChangeSuccess", c.absUrl(), t, c.$$state, e)
            }
            var c, l, f, h = r.baseHref(),
                $ = r.url();
            if (e.enabled) {
                if (!h && e.requireBase) throw ii("nobase", "$location in HTML5 mode requires a <base> tag to be present!");
                f = Ce($) + (h || "/"), l = i.history ? Ae : Ee
            } else f = be($), l = ke;
            c = new l(f, "#" + t), c.$$parseLinkUrl($, $), c.$$state = r.state();
            var p = /^\s*(javascript|mailto):/i;
            o.on("click", function(t) {
                if (e.rewriteLinks && !t.ctrlKey && !t.metaKey && !t.shiftKey && 2 != t.which && 2 != t.button) {
                    for (var i = Qn(t.target);
                        "a" !== j(i[0]);)
                        if (i[0] === o[0] || !(i = i.parent())[0]) return;
                    var s = i.prop("href"),
                        u = i.attr("href") || i.attr("xlink:href");
                    y(s) && "[object SVGAnimatedString]" === s.toString() && (s = rn(s.animVal).href), p.test(s) || !s || i.attr("target") || t.isDefaultPrevented() || c.$$parseLinkUrl(s, u) && (t.preventDefault(), c.absUrl() != r.url() && (n.$apply(), a.angular["ff-684208-preventDefault"] = !0))
                }
            }), c.absUrl() != $ && r.url(c.absUrl(), !0);
            var d = !0;
            return r.onUrlChange(function(t, e) {
                n.$evalAsync(function() {
                    var r, i = c.absUrl(),
                        o = c.$$state;
                    c.$$parse(t), c.$$state = e, r = n.$broadcast("$locationChangeStart", t, i, e, o).defaultPrevented, c.absUrl() === t && (r ? (c.$$parse(i), c.$$state = o, s(i, !1, o)) : (d = !1, u(i, o)))
                }), n.$$phase || n.$digest()
            }), n.$watch(function() {
                var t = xe(r.url()),
                    e = xe(c.absUrl()),
                    o = r.state(),
                    a = c.$$replace,
                    l = t !== e || c.$$html5 && i.history && o !== c.$$state;
                (d || l) && (d = !1, n.$evalAsync(function() {
                    var e = c.absUrl(),
                        r = n.$broadcast("$locationChangeStart", e, t, c.$$state, o).defaultPrevented;
                    c.absUrl() === e && (r ? (c.$$parse(t), c.$$state = o) : (l && s(e, a, o === c.$$state ? null : c.$$state), u(t, o)))
                })), c.$$replace = !1
            }), c
        }]
    }

    function Ne() {
        var t = !0,
            e = this;
        this.debugEnabled = function(e) {
            return g(e) ? (t = e, this) : t
        }, this.$get = ["$window", function(n) {
            function r(t) {
                return t instanceof Error && (t.stack ? t = t.message && t.stack.indexOf(t.message) === -1 ? "Error: " + t.message + "\n" + t.stack : t.stack : t.sourceURL && (t = t.message + "\n" + t.sourceURL + ":" + t.line)), t
            }

            function i(t) {
                var e = n.console || {},
                    i = e[t] || e.log || p,
                    a = !1;
                try {
                    a = !!i.apply
                } catch (t) {}
                return a ? function() {
                    var t = [];
                    return o(arguments, function(e) {
                        t.push(r(e))
                    }), i.apply(e, t)
                } : function(t, e) {
                    i(t, null == e ? "" : e)
                }
            }
            return {
                log: i("log"),
                info: i("info"),
                warn: i("warn"),
                error: i("error"),
                debug: function() {
                    var n = i("debug");
                    return function() {
                        t && n.apply(e, arguments)
                    }
                }()
            }
        }]
    }

    function Ve(t, e) {
        if ("__defineGetter__" === t || "__defineSetter__" === t || "__lookupGetter__" === t || "__lookupSetter__" === t || "__proto__" === t) throw ai("isecfld", "Attempting to access a disallowed field in Angular expressions! Expression: {0}", e);
        return t
    }

    function De(t, e) {
        if (t) {
            if (t.constructor === t) throw ai("isecfn", "Referencing Function in Angular expressions is disallowed! Expression: {0}", e);
            if (t.window === t) throw ai("isecwindow", "Referencing the Window in Angular expressions is disallowed! Expression: {0}", e);
            if (t.children && (t.nodeName || t.prop && t.attr && t.find)) throw ai("isecdom", "Referencing DOM nodes in Angular expressions is disallowed! Expression: {0}", e);
            if (t === Object) throw ai("isecobj", "Referencing Object in Angular expressions is disallowed! Expression: {0}", e)
        }
        return t
    }

    function je(t, e) {
        if (t) {
            if (t.constructor === t) throw ai("isecfn", "Referencing Function in Angular expressions is disallowed! Expression: {0}", e);
            if (t === si || t === ui || t === ci) throw ai("isecff", "Referencing call, apply or bind in Angular expressions is disallowed! Expression: {0}", e)
        }
    }

    function Pe(t) {
        return t.constant
    }

    function Re(t, e, n, r, i) {
        De(t, i), De(e, i);
        for (var o, a = n.split("."), s = 0; a.length > 1; s++) {
            o = Ve(a.shift(), i);
            var u = 0 === s && e && e[o] || t[o];
            u || (u = {}, t[o] = u), t = De(u, i)
        }
        return o = Ve(a.shift(), i), De(t[o], i), t[o] = r, r
    }

    function _e(t) {
        return "constructor" == t
    }

    function Ie(t, e, r, i, o, a, s) {
        Ve(t, a), Ve(e, a), Ve(r, a), Ve(i, a), Ve(o, a);
        var u = function(t) {
                return De(t, a)
            },
            c = s || _e(t) ? u : d,
            l = s || _e(e) ? u : d,
            f = s || _e(r) ? u : d,
            h = s || _e(i) ? u : d,
            $ = s || _e(o) ? u : d;
        return function(a, s) {
            var u = s && s.hasOwnProperty(t) ? s : a;
            return null == u ? u : (u = c(u[t]), e ? null == u ? n : (u = l(u[e]), r ? null == u ? n : (u = f(u[r]), i ? null == u ? n : (u = h(u[i]), o ? null == u ? n : u = $(u[o]) : u) : u) : u) : u)
        }
    }

    function qe(t, e) {
        return function(n, r) {
            return t(n, r, De, e)
        }
    }

    function Ue(t, e, r) {
        var i = e.expensiveChecks,
            a = i ? vi : di,
            s = a[t];
        if (s) return s;
        var u = t.split("."),
            c = u.length;
        if (e.csp) s = c < 6 ? Ie(u[0], u[1], u[2], u[3], u[4], r, i) : function(t, e) {
            var o, a = 0;
            do o = Ie(u[a++], u[a++], u[a++], u[a++], u[a++], r, i)(t, e), e = n, t = o; while (a < c);
            return o
        };
        else {
            var l = "";
            i && (l += "s = eso(s, fe);\nl = eso(l, fe);\n");
            var f = i;
            o(u, function(t, e) {
                Ve(t, r);
                var n = (e ? "s" : '((l&&l.hasOwnProperty("' + t + '"))?l:s)') + "." + t;
                (i || _e(t)) && (n = "eso(" + n + ", fe)", f = !0), l += "if(s == null) return undefined;\ns=" + n + ";\n"
            }), l += "return s;";
            var h = new Function("s", "l", "eso", "fe", l);
            h.toString = v(l), f && (h = qe(h, r)), s = h
        }
        return s.sharedGetter = !0, s.assign = function(e, n, r) {
            return Re(e, r, t, n, t)
        }, a[t] = s, s
    }

    function Fe(t) {
        return S(t.valueOf) ? t.valueOf() : mi.call(t)
    }

    function He() {
        var t = ct(),
            e = ct();
        this.$get = ["$filter", "$sniffer", function(n, r) {
            function i(t) {
                var e = t;
                return t.sharedGetter && (e = function(e, n) {
                    return t(e, n)
                }, e.literal = t.literal, e.constant = t.constant, e.assign = t.assign), e
            }

            function a(t, e) {
                for (var n = 0, r = t.length; n < r; n++) {
                    var i = t[n];
                    i.constant || (i.inputs ? a(i.inputs, e) : e.indexOf(i) === -1 && e.push(i))
                }
                return e
            }

            function s(t, e) {
                return null == t || null == e ? t === e : ("object" != typeof t || (t = Fe(t), "object" != typeof t)) && (t === e || t !== t && e !== e)
            }

            function u(t, e, n, r) {
                var i, o = r.$$inputs || (r.$$inputs = a(r.inputs, []));
                if (1 === o.length) {
                    var u = s;
                    return o = o[0], t.$watch(function(t) {
                        var e = o(t);
                        return s(e, u) || (i = r(t), u = e && Fe(e)), i
                    }, e, n)
                }
                for (var c = [], l = 0, f = o.length; l < f; l++) c[l] = s;
                return t.$watch(function(t) {
                    for (var e = !1, n = 0, a = o.length; n < a; n++) {
                        var u = o[n](t);
                        (e || (e = !s(u, c[n]))) && (c[n] = u && Fe(u))
                    }
                    return e && (i = r(t)), i
                }, e, n)
            }

            function c(t, e, n, r) {
                var i, o;
                return i = t.$watch(function(t) {
                    return r(t)
                }, function(t, n, r) {
                    o = t, S(e) && e.apply(this, arguments), g(t) && r.$$postDigest(function() {
                        g(o) && i()
                    })
                }, n)
            }

            function l(t, e, n, r) {
                function i(t) {
                    var e = !0;
                    return o(t, function(t) {
                        g(t) || (e = !1)
                    }), e
                }
                var a, s;
                return a = t.$watch(function(t) {
                    return r(t)
                }, function(t, n, r) {
                    s = t, S(e) && e.call(this, t, n, r), i(t) && r.$$postDigest(function() {
                        i(s) && a()
                    })
                }, n)
            }

            function f(t, e, n, r) {
                var i;
                return i = t.$watch(function(t) {
                    return r(t)
                }, function(t, n, r) {
                    S(e) && e.apply(this, arguments), i()
                }, n)
            }

            function h(t, e) {
                if (!e) return t;
                var n = t.$$watchDelegate,
                    r = n !== l && n !== c,
                    i = r ? function(n, r) {
                        var i = t(n, r);
                        return e(i, n, r)
                    } : function(n, r) {
                        var i = t(n, r),
                            o = e(i, n, r);
                        return g(i) ? o : i
                    };
                return t.$$watchDelegate && t.$$watchDelegate !== u ? i.$$watchDelegate = t.$$watchDelegate : e.$stateful || (i.$$watchDelegate = u, i.inputs = [t]), i
            }
            var $ = {
                    csp: r.csp,
                    expensiveChecks: !1
                },
                d = {
                    csp: r.csp,
                    expensiveChecks: !0
                };
            return function(r, o, a) {
                var s, v, m;
                switch (typeof r) {
                    case "string":
                        m = r = r.trim();
                        var g = a ? e : t;
                        if (s = g[m], !s) {
                            ":" === r.charAt(0) && ":" === r.charAt(1) && (v = !0, r = r.substring(2));
                            var y = a ? d : $,
                                w = new $i(y),
                                b = new pi(w, n, y);
                            s = b.parse(r), s.constant ? s.$$watchDelegate = f : v ? (s = i(s), s.$$watchDelegate = s.literal ? l : c) : s.inputs && (s.$$watchDelegate = u), g[m] = s
                        }
                        return h(s, o);
                    case "function":
                        return h(r, o);
                    default:
                        return h(p, o)
                }
            }
        }]
    }

    function Le() {
        this.$get = ["$rootScope", "$exceptionHandler", function(t, e) {
            return ze(function(e) {
                t.$evalAsync(e)
            }, e)
        }]
    }

    function Be() {
        this.$get = ["$browser", "$exceptionHandler", function(t, e) {
            return ze(function(e) {
                t.defer(e)
            }, e)
        }]
    }

    function ze(t, e) {
        function i(t, e, n) {
            function r(e) {
                return function(n) {
                    i || (i = !0, e.call(t, n))
                }
            }
            var i = !1;
            return [r(e), r(n)]
        }

        function a() {
            this.$$state = {
                status: 0
            }
        }

        function s(t, e) {
            return function(n) {
                e.call(t, n)
            }
        }

        function u(t) {
            var r, i, o;
            o = t.pending, t.processScheduled = !1, t.pending = n;
            for (var a = 0, s = o.length; a < s; ++a) {
                i = o[a][0], r = o[a][t.status];
                try {
                    S(r) ? i.resolve(r(t.value)) : 1 === t.status ? i.resolve(t.value) : i.reject(t.value)
                } catch (t) {
                    i.reject(t), e(t)
                }
            }
        }

        function c(e) {
            !e.processScheduled && e.pending && (e.processScheduled = !0, t(function() {
                u(e)
            }))
        }

        function l() {
            this.promise = new a, this.resolve = s(this, this.resolve), this.reject = s(this, this.reject), this.notify = s(this, this.notify)
        }

        function f(t) {
            var e = new l,
                n = 0,
                r = lr(t) ? [] : {};
            return o(t, function(t, i) {
                n++, m(t).then(function(t) {
                    r.hasOwnProperty(i) || (r[i] = t, --n || e.resolve(r))
                }, function(t) {
                    r.hasOwnProperty(i) || e.reject(t)
                })
            }), 0 === n && e.resolve(r), e.promise
        }
        var h = r("$q", TypeError),
            $ = function() {
                return new l
            };
        a.prototype = {
            then: function(t, e, n) {
                var r = new l;
                return this.$$state.pending = this.$$state.pending || [], this.$$state.pending.push([r, t, e, n]), this.$$state.status > 0 && c(this.$$state), r.promise
            },
            catch: function(t) {
                return this.then(null, t)
            },
            finally: function(t, e) {
                return this.then(function(e) {
                    return v(e, !0, t)
                }, function(e) {
                    return v(e, !1, t)
                }, e)
            }
        }, l.prototype = {
            resolve: function(t) {
                this.promise.$$state.status || (t === this.promise ? this.$$reject(h("qcycle", "Expected promise to be resolved with value other than itself '{0}'", t)) : this.$$resolve(t))
            },
            $$resolve: function(t) {
                var n, r;
                r = i(this, this.$$resolve, this.$$reject);
                try {
                    (y(t) || S(t)) && (n = t && t.then), S(n) ? (this.promise.$$state.status = -1, n.call(t, r[0], r[1], this.notify)) : (this.promise.$$state.value = t, this.promise.$$state.status = 1, c(this.promise.$$state))
                } catch (t) {
                    r[1](t), e(t)
                }
            },
            reject: function(t) {
                this.promise.$$state.status || this.$$reject(t)
            },
            $$reject: function(t) {
                this.promise.$$state.value = t, this.promise.$$state.status = 2, c(this.promise.$$state)
            },
            notify: function(n) {
                var r = this.promise.$$state.pending;
                this.promise.$$state.status <= 0 && r && r.length && t(function() {
                    for (var t, i, o = 0, a = r.length; o < a; o++) {
                        i = r[o][0], t = r[o][3];
                        try {
                            i.notify(S(t) ? t(n) : n)
                        } catch (t) {
                            e(t)
                        }
                    }
                })
            }
        };
        var p = function(t) {
                var e = new l;
                return e.reject(t), e.promise
            },
            d = function(t, e) {
                var n = new l;
                return e ? n.resolve(t) : n.reject(t), n.promise
            },
            v = function(t, e, n) {
                var r = null;
                try {
                    S(n) && (r = n())
                } catch (t) {
                    return d(t, !1)
                }
                return N(r) ? r.then(function() {
                    return d(t, e)
                }, function(t) {
                    return d(t, !1)
                }) : d(t, e)
            },
            m = function(t, e, n, r) {
                var i = new l;
                return i.resolve(t), i.promise.then(e, n, r)
            },
            g = function t(e) {
                function n(t) {
                    i.resolve(t)
                }

                function r(t) {
                    i.reject(t)
                }
                if (!S(e)) throw h("norslvr", "Expected resolverFn, got '{0}'", e);
                if (!(this instanceof t)) return new t(e);
                var i = new l;
                return e(n, r), i.promise
            };
        return g.defer = $, g.reject = p, g.when = m, g.all = f, g
    }

    function We() {
        this.$get = ["$window", "$timeout", function(t, e) {
            var n = t.requestAnimationFrame || t.webkitRequestAnimationFrame,
                r = t.cancelAnimationFrame || t.webkitCancelAnimationFrame || t.webkitCancelRequestAnimationFrame,
                i = !!n,
                o = i ? function(t) {
                    var e = n(t);
                    return function() {
                        r(e)
                    }
                } : function(t) {
                    var n = e(t, 16.66, !1);
                    return function() {
                        e.cancel(n)
                    }
                };
            return o.supported = i, o
        }]
    }

    function Ge() {
        var t = 10,
            e = r("$rootScope"),
            n = null,
            a = null;
        this.digestTtl = function(e) {
            return arguments.length && (t = e), t
        }, this.$get = ["$injector", "$exceptionHandler", "$parse", "$browser", function(r, s, u, l) {
            function f() {
                this.$id = c(), this.$$phase = this.$parent = this.$$watchers = this.$$nextSibling = this.$$prevSibling = this.$$childHead = this.$$childTail = null, this.$root = this, this.$$destroyed = !1, this.$$listeners = {}, this.$$listenerCount = {}, this.$$isolateBindings = null
            }

            function h(t) {
                if (b.$$phase) throw e("inprog", "{0} already in progress", b.$$phase);
                b.$$phase = t
            }

            function $() {
                b.$$phase = null
            }

            function d(t, e, n) {
                do t.$$listenerCount[n] -= e, 0 === t.$$listenerCount[n] && delete t.$$listenerCount[n]; while (t = t.$parent)
            }

            function v() {}

            function g() {
                for (; A.length;) try {
                    A.shift()()
                } catch (t) {
                    s(t)
                }
                a = null
            }

            function w() {
                null === a && (a = l.defer(function() {
                    b.$apply(g)
                }))
            }
            f.prototype = {
                constructor: f,
                $new: function(t, e) {
                    function n() {
                        r.$$destroyed = !0
                    }
                    var r;
                    return e = e || this, t ? (r = new f, r.$root = this.$root) : (this.$$ChildScope || (this.$$ChildScope = function() {
                        this.$$watchers = this.$$nextSibling = this.$$childHead = this.$$childTail = null, this.$$listeners = {}, this.$$listenerCount = {}, this.$id = c(), this.$$ChildScope = null
                    }, this.$$ChildScope.prototype = this), r = new this.$$ChildScope), r.$parent = e, r.$$prevSibling = e.$$childTail, e.$$childHead ? (e.$$childTail.$$nextSibling = r, e.$$childTail = r) : e.$$childHead = e.$$childTail = r, (t || e != this) && r.$on("$destroy", n), r
                },
                $watch: function(t, e, r) {
                    var i = u(t);
                    if (i.$$watchDelegate) return i.$$watchDelegate(this, e, r, i);
                    var o = this,
                        a = o.$$watchers,
                        s = {
                            fn: e,
                            last: v,
                            get: i,
                            exp: t,
                            eq: !!r
                        };
                    return n = null, S(e) || (s.fn = p), a || (a = o.$$watchers = []), a.unshift(s),
                        function() {
                            P(a, s), n = null
                        }
                },
                $watchGroup: function(t, e) {
                    function n() {
                        u = !1, c ? (c = !1, e(i, i, s)) : e(i, r, s)
                    }
                    var r = new Array(t.length),
                        i = new Array(t.length),
                        a = [],
                        s = this,
                        u = !1,
                        c = !0;
                    if (!t.length) {
                        var l = !0;
                        return s.$evalAsync(function() {
                                l && e(i, i, s)
                            }),
                            function() {
                                l = !1
                            }
                    }
                    return 1 === t.length ? this.$watch(t[0], function(t, n, o) {
                        i[0] = t, r[0] = n, e(i, t === n ? i : r, o)
                    }) : (o(t, function(t, e) {
                        var o = s.$watch(t, function(t, o) {
                            i[e] = t, r[e] = o, u || (u = !0, s.$evalAsync(n))
                        });
                        a.push(o)
                    }), function() {
                        for (; a.length;) a.shift()()
                    })
                },
                $watchCollection: function(t, e) {
                    function n(t) {
                        o = t;
                        var e, n, r, s, u;
                        if (!m(o)) {
                            if (y(o))
                                if (i(o)) {
                                    a !== $ && (a = $, v = a.length = 0, f++), e = o.length, v !== e && (f++, a.length = v = e);
                                    for (var c = 0; c < e; c++) u = a[c], s = o[c], r = u !== u && s !== s, r || u === s || (f++, a[c] = s)
                                } else {
                                    a !== p && (a = p = {}, v = 0, f++), e = 0;
                                    for (n in o) o.hasOwnProperty(n) && (e++, s = o[n], u = a[n], n in a ? (r = u !== u && s !== s, r || u === s || (f++, a[n] = s)) : (v++, a[n] = s, f++));
                                    if (v > e) {
                                        f++;
                                        for (n in a) o.hasOwnProperty(n) || (v--, delete a[n])
                                    }
                                }
                            else a !== o && (a = o, f++);
                            return f
                        }
                    }

                    function r() {
                        if (d ? (d = !1, e(o, o, c)) : e(o, s, c), l)
                            if (y(o))
                                if (i(o)) {
                                    s = new Array(o.length);
                                    for (var t = 0; t < o.length; t++) s[t] = o[t]
                                } else {
                                    s = {};
                                    for (var n in o) Jn.call(o, n) && (s[n] = o[n])
                                }
                        else s = o
                    }
                    n.$stateful = !0;
                    var o, a, s, c = this,
                        l = e.length > 1,
                        f = 0,
                        h = u(t, n),
                        $ = [],
                        p = {},
                        d = !0,
                        v = 0;
                    return this.$watch(h, r)
                },
                $digest: function() {
                    var r, i, o, u, c, f, p, d, m, y, w = t,
                        A = this,
                        k = [];
                    h("$digest"), l.$$checkUrlChange(), this === b && null !== a && (l.defer.cancel(a), g()), n = null;
                    do {
                        for (f = !1, d = A; x.length;) {
                            try {
                                y = x.shift(), y.scope.$eval(y.expression, y.locals)
                            } catch (t) {
                                s(t)
                            }
                            n = null
                        }
                        t: do {
                            if (u = d.$$watchers)
                                for (c = u.length; c--;) try {
                                    if (r = u[c])
                                        if ((i = r.get(d)) === (o = r.last) || (r.eq ? I(i, o) : "number" == typeof i && "number" == typeof o && isNaN(i) && isNaN(o))) {
                                            if (r === n) {
                                                f = !1;
                                                break t
                                            }
                                        } else f = !0, n = r, r.last = r.eq ? R(i, null) : i, r.fn(i, o === v ? i : o, d), w < 5 && (m = 4 - w, k[m] || (k[m] = []), k[m].push({
                                            msg: S(r.exp) ? "fn: " + (r.exp.name || r.exp.toString()) : r.exp,
                                            newVal: i,
                                            oldVal: o
                                        }))
                                } catch (t) {
                                    s(t)
                                }
                            if (!(p = d.$$childHead || d !== A && d.$$nextSibling))
                                for (; d !== A && !(p = d.$$nextSibling);) d = d.$parent
                        } while (d = p);
                        if ((f || x.length) && !w--) throw $(), e("infdig", "{0} $digest() iterations reached. Aborting!\nWatchers fired in the last 5 iterations: {1}", t, k)
                    } while (f || x.length);
                    for ($(); C.length;) try {
                        C.shift()()
                    } catch (t) {
                        s(t)
                    }
                },
                $destroy: function() {
                    if (!this.$$destroyed) {
                        var t = this.$parent;
                        if (this.$broadcast("$destroy"), this.$$destroyed = !0, this !== b) {
                            for (var e in this.$$listenerCount) d(this, this.$$listenerCount[e], e);
                            t.$$childHead == this && (t.$$childHead = this.$$nextSibling), t.$$childTail == this && (t.$$childTail = this.$$prevSibling), this.$$prevSibling && (this.$$prevSibling.$$nextSibling = this.$$nextSibling), this.$$nextSibling && (this.$$nextSibling.$$prevSibling = this.$$prevSibling), this.$destroy = this.$digest = this.$apply = this.$evalAsync = this.$applyAsync = p, this.$on = this.$watch = this.$watchGroup = function() {
                                return p
                            }, this.$$listeners = {}, this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead = this.$$childTail = this.$root = this.$$watchers = null
                        }
                    }
                },
                $eval: function(t, e) {
                    return u(t)(this, e)
                },
                $evalAsync: function(t, e) {
                    b.$$phase || x.length || l.defer(function() {
                        x.length && b.$digest()
                    }), x.push({
                        scope: this,
                        expression: t,
                        locals: e
                    })
                },
                $$postDigest: function(t) {
                    C.push(t)
                },
                $apply: function(t) {
                    try {
                        return h("$apply"), this.$eval(t)
                    } catch (t) {
                        s(t)
                    } finally {
                        $();
                        try {
                            b.$digest()
                        } catch (t) {
                            throw s(t), t
                        }
                    }
                },
                $applyAsync: function(t) {
                    function e() {
                        n.$eval(t)
                    }
                    var n = this;
                    t && A.push(e), w()
                },
                $on: function(t, e) {
                    var n = this.$$listeners[t];
                    n || (this.$$listeners[t] = n = []), n.push(e);
                    var r = this;
                    do r.$$listenerCount[t] || (r.$$listenerCount[t] = 0), r.$$listenerCount[t]++; while (r = r.$parent);
                    var i = this;
                    return function() {
                        var r = n.indexOf(e);
                        r !== -1 && (n[r] = null, d(i, 1, t))
                    }
                },
                $emit: function(t, e) {
                    var n, r, i, o = [],
                        a = this,
                        u = !1,
                        c = {
                            name: t,
                            targetScope: a,
                            stopPropagation: function() {
                                u = !0
                            },
                            preventDefault: function() {
                                c.defaultPrevented = !0
                            },
                            defaultPrevented: !1
                        },
                        l = q([c], arguments, 1);
                    do {
                        for (n = a.$$listeners[t] || o, c.currentScope = a, r = 0, i = n.length; r < i; r++)
                            if (n[r]) try {
                                n[r].apply(null, l)
                            } catch (t) {
                                s(t)
                            } else n.splice(r, 1), r--, i--;
                        if (u) return c.currentScope = null, c;
                        a = a.$parent
                    } while (a);
                    return c.currentScope = null, c
                },
                $broadcast: function(t, e) {
                    var n = this,
                        r = n,
                        i = n,
                        o = {
                            name: t,
                            targetScope: n,
                            preventDefault: function() {
                                o.defaultPrevented = !0
                            },
                            defaultPrevented: !1
                        };
                    if (!n.$$listenerCount[t]) return o;
                    for (var a, u, c, l = q([o], arguments, 1); r = i;) {
                        for (o.currentScope = r, a = r.$$listeners[t] || [], u = 0, c = a.length; u < c; u++)
                            if (a[u]) try {
                                a[u].apply(null, l)
                            } catch (t) {
                                s(t)
                            } else a.splice(u, 1), u--, c--;
                        if (!(i = r.$$listenerCount[t] && r.$$childHead || r !== n && r.$$nextSibling))
                            for (; r !== n && !(i = r.$$nextSibling);) r = r.$parent
                    }
                    return o.currentScope = null, o
                }
            };
            var b = new f,
                x = b.$$asyncQueue = [],
                C = b.$$postDigestQueue = [],
                A = b.$$applyAsyncQueue = [];
            return b
        }]
    }

    function Je() {
        var t = /^\s*(https?|ftp|mailto|tel|file):/,
            e = /^\s*((https?|ftp|file|blob):|data:image\/)/;
        this.aHrefSanitizationWhitelist = function(e) {
            return g(e) ? (t = e, this) : t
        }, this.imgSrcSanitizationWhitelist = function(t) {
            return g(t) ? (e = t, this) : e
        }, this.$get = function() {
            return function(n, r) {
                var i, o = r ? e : t;
                return i = rn(n).href, "" === i || i.match(o) ? n : "unsafe:" + i
            }
        }
    }

    function Ye(t) {
        if ("self" === t) return t;
        if (w(t)) {
            if (t.indexOf("***") > -1) throw gi("iwcard", "Illegal sequence *** in string matcher.  String: {0}", t);
            return t = hr(t).replace("\\*\\*", ".*").replace("\\*", "[^:/.?&;]*"), new RegExp("^" + t + "$")
        }
        if (C(t)) return new RegExp("^" + t.source + "$");
        throw gi("imatcher", 'Matchers may only be "self", string patterns or RegExp objects')
    }

    function Ze(t) {
        var e = [];
        return g(t) && o(t, function(t) {
            e.push(Ye(t))
        }), e
    }

    function Ke() {
        this.SCE_CONTEXTS = yi;
        var t = ["self"],
            e = [];
        this.resourceUrlWhitelist = function(e) {
            return arguments.length && (t = Ze(e)), t
        }, this.resourceUrlBlacklist = function(t) {
            return arguments.length && (e = Ze(t)), e
        }, this.$get = ["$injector", function(r) {
            function i(t, e) {
                return "self" === t ? on(e) : !!t.exec(e.href)
            }

            function o(n) {
                var r, o, a = rn(n.toString()),
                    s = !1;
                for (r = 0, o = t.length; r < o; r++)
                    if (i(t[r], a)) {
                        s = !0;
                        break
                    }
                if (s)
                    for (r = 0, o = e.length; r < o; r++)
                        if (i(e[r], a)) {
                            s = !1;
                            break
                        }
                return s
            }

            function a(t) {
                var e = function(t) {
                    this.$$unwrapTrustedValue = function() {
                        return t
                    }
                };
                return t && (e.prototype = new t), e.prototype.valueOf = function() {
                    return this.$$unwrapTrustedValue()
                }, e.prototype.toString = function() {
                    return this.$$unwrapTrustedValue().toString()
                }, e
            }

            function s(t, e) {
                var r = h.hasOwnProperty(t) ? h[t] : null;
                if (!r) throw gi("icontext", "Attempted to trust a value in invalid context. Context: {0}; Value: {1}", t, e);
                if (null === e || e === n || "" === e) return e;
                if ("string" != typeof e) throw gi("itype", "Attempted to trust a non-string value in a content requiring a string: Context: {0}", t);
                return new r(e)
            }

            function u(t) {
                return t instanceof f ? t.$$unwrapTrustedValue() : t
            }

            function c(t, e) {
                if (null === e || e === n || "" === e) return e;
                var r = h.hasOwnProperty(t) ? h[t] : null;
                if (r && e instanceof r) return e.$$unwrapTrustedValue();
                if (t === yi.RESOURCE_URL) {
                    if (o(e)) return e;
                    throw gi("insecurl", "Blocked loading resource from url not allowed by $sceDelegate policy.  URL: {0}", e.toString())
                }
                if (t === yi.HTML) return l(e);
                throw gi("unsafe", "Attempting to use an unsafe value in a safe context.")
            }
            var l = function(t) {
                throw gi("unsafe", "Attempting to use an unsafe value in a safe context.")
            };
            r.has("$sanitize") && (l = r.get("$sanitize"));
            var f = a(),
                h = {};
            return h[yi.HTML] = a(f), h[yi.CSS] = a(f), h[yi.URL] = a(f), h[yi.JS] = a(f), h[yi.RESOURCE_URL] = a(h[yi.URL]), {
                trustAs: s,
                getTrusted: c,
                valueOf: u
            }
        }]
    }

    function Xe() {
        var t = !0;
        this.enabled = function(e) {
            return arguments.length && (t = !!e), t
        }, this.$get = ["$parse", "$sceDelegate", function(e, n) {
            if (t && Xn < 8) throw gi("iequirks", "Strict Contextual Escaping does not support Internet Explorer version < 11 in quirks mode.  You can fix this by adding the text <!doctype html> to the top of your HTML document.  See http://docs.angularjs.org/api/ng.$sce for more information.");
            var r = _(yi);
            r.isEnabled = function() {
                return t
            }, r.trustAs = n.trustAs, r.getTrusted = n.getTrusted, r.valueOf = n.valueOf, t || (r.trustAs = r.getTrusted = function(t, e) {
                return e
            }, r.valueOf = d), r.parseAs = function(t, n) {
                var i = e(n);
                return i.literal && i.constant ? i : e(n, function(e) {
                    return r.getTrusted(t, e)
                })
            };
            var i = r.parseAs,
                a = r.getTrusted,
                s = r.trustAs;
            return o(yi, function(t, e) {
                var n = Gn(e);
                r[dt("parse_as_" + n)] = function(e) {
                    return i(t, e)
                }, r[dt("get_trusted_" + n)] = function(e) {
                    return a(t, e)
                }, r[dt("trust_as_" + n)] = function(e) {
                    return s(t, e)
                }
            }), r
        }]
    }

    function Qe() {
        this.$get = ["$window", "$document", function(t, e) {
            var n, r, i = {},
                o = h((/android (\d+)/.exec(Gn((t.navigator || {}).userAgent)) || [])[1]),
                a = /Boxee/i.test((t.navigator || {}).userAgent),
                s = e[0] || {},
                u = /^(Moz|webkit|ms)(?=[A-Z])/,
                c = s.body && s.body.style,
                l = !1,
                f = !1;
            if (c) {
                for (var $ in c)
                    if (r = u.exec($)) {
                        n = r[0], n = n.substr(0, 1).toUpperCase() + n.substr(1);
                        break
                    }
                n || (n = "WebkitOpacity" in c && "webkit"), l = !!("transition" in c || n + "Transition" in c), f = !!("animation" in c || n + "Animation" in c), !o || l && f || (l = w(s.body.style.webkitTransition), f = w(s.body.style.webkitAnimation))
            }
            return {
                history: !(!t.history || !t.history.pushState || o < 4 || a),
                hasEvent: function(t) {
                    if ("input" === t && Xn <= 11) return !1;
                    if (m(i[t])) {
                        var e = s.createElement("div");
                        i[t] = "on" + t in e
                    }
                    return i[t]
                },
                csp: $r(),
                vendorPrefix: n,
                transitions: l,
                animations: f,
                android: o
            }
        }]
    }

    function tn() {
        this.$get = ["$templateCache", "$http", "$q", function(t, e, n) {
            function r(i, o) {
                function a(t) {
                    if (!o) throw Gr("tpload", "Failed to load template: {0}", i);
                    return n.reject(t)
                }
                r.totalPendingRequests++;
                var s = e.defaults && e.defaults.transformResponse;
                lr(s) ? s = s.filter(function(t) {
                    return t !== ie
                }) : s === ie && (s = null);
                var u = {
                    cache: t,
                    transformResponse: s
                };
                return e.get(i, u).finally(function() {
                    r.totalPendingRequests--
                }).then(function(t) {
                    return t.data
                }, a)
            }
            return r.totalPendingRequests = 0, r
        }]
    }

    function en() {
        this.$get = ["$rootScope", "$browser", "$location", function(t, e, n) {
            var r = {};
            return r.findBindings = function(t, e, n) {
                var r = t.getElementsByClassName("ng-binding"),
                    i = [];
                return o(r, function(t) {
                    var r = sr.element(t).data("$binding");
                    r && o(r, function(r) {
                        if (n) {
                            var o = new RegExp("(^|\\s)" + hr(e) + "(\\s|\\||$)");
                            o.test(r) && i.push(t)
                        } else r.indexOf(e) != -1 && i.push(t)
                    })
                }), i
            }, r.findModels = function(t, e, n) {
                for (var r = ["ng-", "data-ng-", "ng\\:"], i = 0; i < r.length; ++i) {
                    var o = n ? "=" : "*=",
                        a = "[" + r[i] + "model" + o + '"' + e + '"]',
                        s = t.querySelectorAll(a);
                    if (s.length) return s
                }
            }, r.getLocation = function() {
                return n.url()
            }, r.setLocation = function(e) {
                e !== n.url() && (n.url(e), t.$digest())
            }, r.whenStable = function(t) {
                e.notifyWhenNoOutstandingRequests(t)
            }, r
        }]
    }

    function nn() {
        this.$get = ["$rootScope", "$browser", "$q", "$$q", "$exceptionHandler", function(t, e, n, r, i) {
            function o(o, s, u) {
                var c, l = g(u) && !u,
                    f = (l ? r : n).defer(),
                    h = f.promise;
                return c = e.defer(function() {
                    try {
                        f.resolve(o())
                    } catch (t) {
                        f.reject(t), i(t)
                    } finally {
                        delete a[h.$$timeoutId]
                    }
                    l || t.$apply()
                }, s), h.$$timeoutId = c, a[c] = f, h
            }
            var a = {};
            return o.cancel = function(t) {
                return !!(t && t.$$timeoutId in a) && (a[t.$$timeoutId].reject("canceled"), delete a[t.$$timeoutId], e.defer.cancel(t.$$timeoutId))
            }, o
        }]
    }

    function rn(t) {
        var e = t;
        return Xn && (wi.setAttribute("href", e), e = wi.href), wi.setAttribute("href", e), {
            href: wi.href,
            protocol: wi.protocol ? wi.protocol.replace(/:$/, "") : "",
            host: wi.host,
            search: wi.search ? wi.search.replace(/^\?/, "") : "",
            hash: wi.hash ? wi.hash.replace(/^#/, "") : "",
            hostname: wi.hostname,
            port: wi.port,
            pathname: "/" === wi.pathname.charAt(0) ? wi.pathname : "/" + wi.pathname
        }
    }

    function on(t) {
        var e = w(t) ? rn(t) : t;
        return e.protocol === bi.protocol && e.host === bi.host
    }

    function an() {
        this.$get = v(t)
    }

    function sn(t) {
        function e(r, i) {
            if (y(r)) {
                var a = {};
                return o(r, function(t, n) {
                    a[n] = e(n, t)
                }), a
            }
            return t.factory(r + n, i)
        }
        var n = "Filter";
        this.register = e, this.$get = ["$injector", function(t) {
            return function(e) {
                return t.get(e + n)
            }
        }], e("currency", fn), e("date", xn), e("filter", un), e("json", Sn), e("limitTo", Cn), e("lowercase", ki), e("number", hn), e("orderBy", An), e("uppercase", Ei)
    }

    function un() {
        return function(t, e, n) {
            if (!lr(t)) return t;
            var r, i;
            switch (typeof e) {
                case "function":
                    r = e;
                    break;
                case "boolean":
                case "number":
                case "string":
                    i = !0;
                case "object":
                    r = cn(e, n, i);
                    break;
                default:
                    return t
            }
            return t.filter(r)
        }
    }

    function cn(t, e, n) {
        var r, i = y(t) && "$" in t;
        return e === !0 ? e = I : S(e) || (e = function(t, e) {
            return !y(t) && !y(e) && (t = Gn("" + t), e = Gn("" + e), t.indexOf(e) !== -1)
        }), r = function(r) {
            return i && !y(r) ? ln(r, t.$, e, !1) : ln(r, t, e, n)
        }
    }

    function ln(t, e, n, r, i) {
        var o = typeof t,
            a = typeof e;
        if ("string" === a && "!" === e.charAt(0)) return !ln(t, e.substring(1), n, r);
        if (lr(t)) return t.some(function(t) {
            return ln(t, e, n, r)
        });
        switch (o) {
            case "object":
                var s;
                if (r) {
                    for (s in t)
                        if ("$" !== s.charAt(0) && ln(t[s], e, n, !0)) return !0;
                    return !i && ln(t, e, n, !1)
                }
                if ("object" === a) {
                    for (s in e) {
                        var u = e[s];
                        if (!S(u)) {
                            var c = "$" === s,
                                l = c ? t : t[s];
                            if (!ln(l, u, n, c, c)) return !1
                        }
                    }
                    return !0
                }
                return n(t, e);
            case "function":
                return !1;
            default:
                return n(t, e)
        }
    }

    function fn(t) {
        var e = t.NUMBER_FORMATS;
        return function(t, n, r) {
            return m(n) && (n = e.CURRENCY_SYM), m(r) && (r = e.PATTERNS[1].maxFrac), null == t ? t : $n(t, e.PATTERNS[1], e.GROUP_SEP, e.DECIMAL_SEP, r).replace(/\u00A4/g, n)
        }
    }

    function hn(t) {
        var e = t.NUMBER_FORMATS;
        return function(t, n) {
            return null == t ? t : $n(t, e.PATTERNS[0], e.GROUP_SEP, e.DECIMAL_SEP, n)
        }
    }

    function $n(t, e, n, r, i) {
        if (!isFinite(t) || y(t)) return "";
        var o = t < 0;
        t = Math.abs(t);
        var a = t + "",
            s = "",
            u = [],
            c = !1;
        if (a.indexOf("e") !== -1) {
            var l = a.match(/([\d\.]+)e(-?)(\d+)/);
            l && "-" == l[2] && l[3] > i + 1 ? t = 0 : (s = a, c = !0)
        }
        if (c) i > 0 && t < 1 && (s = t.toFixed(i), t = parseFloat(s));
        else {
            var f = (a.split(xi)[1] || "").length;
            m(i) && (i = Math.min(Math.max(e.minFrac, f), e.maxFrac)), t = +(Math.round(+(t.toString() + "e" + i)).toString() + "e" + -i);
            var h = ("" + t).split(xi),
                $ = h[0];
            h = h[1] || "";
            var p, d = 0,
                v = e.lgSize,
                g = e.gSize;
            if ($.length >= v + g)
                for (d = $.length - v, p = 0; p < d; p++)(d - p) % g === 0 && 0 !== p && (s += n), s += $.charAt(p);
            for (p = d; p < $.length; p++)($.length - p) % v === 0 && 0 !== p && (s += n), s += $.charAt(p);
            for (; h.length < i;) h += "0";
            i && "0" !== i && (s += r + h.substr(0, i))
        }
        return 0 === t && (o = !1), u.push(o ? e.negPre : e.posPre, s, o ? e.negSuf : e.posSuf), u.join("")
    }

    function pn(t, e, n) {
        var r = "";
        for (t < 0 && (r = "-", t = -t), t = "" + t; t.length < e;) t = "0" + t;
        return n && (t = t.substr(t.length - e)), r + t
    }

    function dn(t, e, n, r) {
        return n = n || 0,
            function(i) {
                var o = i["get" + t]();
                return (n > 0 || o > -n) && (o += n), 0 === o && n == -12 && (o = 12), pn(o, e, r)
            }
    }

    function vn(t, e) {
        return function(n, r) {
            var i = n["get" + t](),
                o = Yn(e ? "SHORT" + t : t);
            return r[o][i]
        }
    }

    function mn(t) {
        var e = -1 * t.getTimezoneOffset(),
            n = e >= 0 ? "+" : "";
        return n += pn(Math[e > 0 ? "floor" : "ceil"](e / 60), 2) + pn(Math.abs(e % 60), 2)
    }

    function gn(t) {
        var e = new Date(t, 0, 1).getDay();
        return new Date(t, 0, (e <= 4 ? 5 : 12) - e)
    }

    function yn(t) {
        return new Date(t.getFullYear(), t.getMonth(), t.getDate() + (4 - t.getDay()))
    }

    function wn(t) {
        return function(e) {
            var n = gn(e.getFullYear()),
                r = yn(e),
                i = +r - +n,
                o = 1 + Math.round(i / 6048e5);
            return pn(o, t)
        }
    }

    function bn(t, e) {
        return t.getHours() < 12 ? e.AMPMS[0] : e.AMPMS[1]
    }

    function xn(t) {
        function e(t) {
            var e;
            if (e = t.match(n)) {
                var r = new Date(0),
                    i = 0,
                    o = 0,
                    a = e[8] ? r.setUTCFullYear : r.setFullYear,
                    s = e[8] ? r.setUTCHours : r.setHours;
                e[9] && (i = h(e[9] + e[10]), o = h(e[9] + e[11])), a.call(r, h(e[1]), h(e[2]) - 1, h(e[3]));
                var u = h(e[4] || 0) - i,
                    c = h(e[5] || 0) - o,
                    l = h(e[6] || 0),
                    f = Math.round(1e3 * parseFloat("0." + (e[7] || 0)));
                return s.call(r, u, c, l, f), r
            }
            return t
        }
        var n = /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;
        return function(n, r, i) {
            var a, s, u = "",
                c = [];
            if (r = r || "mediumDate", r = t.DATETIME_FORMATS[r] || r, w(n) && (n = Ai.test(n) ? h(n) : e(n)), b(n) && (n = new Date(n)), !x(n)) return n;
            for (; r;) s = Ci.exec(r), s ? (c = q(c, s, 1), r = c.pop()) : (c.push(r), r = null);
            return i && "UTC" === i && (n = new Date(n.getTime()), n.setMinutes(n.getMinutes() + n.getTimezoneOffset())), o(c, function(e) {
                a = Si[e], u += a ? a(n, t.DATETIME_FORMATS) : e.replace(/(^'|'$)/g, "").replace(/''/g, "'")
            }), u
        }
    }

    function Sn() {
        return function(t, e) {
            return m(e) && (e = 2), L(t, e)
        }
    }

    function Cn() {
        return function(t, e) {
            return b(t) && (t = t.toString()), lr(t) || w(t) ? (e = Math.abs(Number(e)) === 1 / 0 ? Number(e) : h(e), e ? e > 0 ? t.slice(0, e) : t.slice(e) : w(t) ? "" : []) : t
        }
    }

    function An(t) {
        return function(e, n, r) {
            function o(t, e) {
                for (var r = 0; r < n.length; r++) {
                    var i = n[r](t, e);
                    if (0 !== i) return i
                }
                return 0
            }

            function a(t, e) {
                return e ? function(e, n) {
                    return t(n, e)
                } : t
            }

            function s(t) {
                switch (typeof t) {
                    case "number":
                    case "boolean":
                    case "string":
                        return !0;
                    default:
                        return !1
                }
            }

            function u(t) {
                return null === t ? "null" : "function" == typeof t.valueOf && (t = t.valueOf(), s(t)) ? t : "function" == typeof t.toString && (t = t.toString(), s(t)) ? t : ""
            }

            function c(t, e) {
                var n = typeof t,
                    r = typeof e;
                return n === r && "object" === n && (t = u(t), e = u(e)), n === r ? ("string" === n && (t = t.toLowerCase(), e = e.toLowerCase()), t === e ? 0 : t < e ? -1 : 1) : n < r ? -1 : 1
            }
            return i(e) ? (n = lr(n) ? n : [n], 0 === n.length && (n = ["+"]), n = n.map(function(e) {
                var n = !1,
                    r = e || d;
                if (w(e)) {
                    if ("+" != e.charAt(0) && "-" != e.charAt(0) || (n = "-" == e.charAt(0), e = e.substring(1)), "" === e) return a(c, n);
                    if (r = t(e), r.constant) {
                        var i = r();
                        return a(function(t, e) {
                            return c(t[i], e[i])
                        }, n)
                    }
                }
                return a(function(t, e) {
                    return c(r(t), r(e))
                }, n)
            }), nr.call(e).sort(a(o, r))) : e
        }
    }

    function kn(t) {
        return S(t) && (t = {
            link: t
        }), t.restrict = t.restrict || "AC", v(t)
    }

    function En(t, e) {
        t.$name = e
    }

    function On(t, e, r, i, a) {
        var s = this,
            u = [],
            c = s.$$parentForm = t.parent().controller("form") || Mi;
        s.$error = {}, s.$$success = {}, s.$pending = n, s.$name = a(e.name || e.ngForm || "")(r), s.$dirty = !1, s.$pristine = !0, s.$valid = !0, s.$invalid = !1, s.$submitted = !1, c.$addControl(s), s.$rollbackViewValue = function() {
            o(u, function(t) {
                t.$rollbackViewValue()
            })
        }, s.$commitViewValue = function() {
            o(u, function(t) {
                t.$commitViewValue()
            })
        }, s.$addControl = function(t) {
            at(t.$name, "input"), u.push(t), t.$name && (s[t.$name] = t)
        }, s.$$renameControl = function(t, e) {
            var n = t.$name;
            s[n] === t && delete s[n], s[e] = t, t.$name = e
        }, s.$removeControl = function(t) {
            t.$name && s[t.$name] === t && delete s[t.$name], o(s.$pending, function(e, n) {
                s.$setValidity(n, null, t)
            }), o(s.$error, function(e, n) {
                s.$setValidity(n, null, t)
            }), o(s.$$success, function(e, n) {
                s.$setValidity(n, null, t)
            }), P(u, t)
        }, Ln({
            ctrl: this,
            $element: t,
            set: function(t, e, n) {
                var r = t[e];
                if (r) {
                    var i = r.indexOf(n);
                    i === -1 && r.push(n)
                } else t[e] = [n]
            },
            unset: function(t, e, n) {
                var r = t[e];
                r && (P(r, n), 0 === r.length && delete t[e])
            },
            parentForm: c,
            $animate: i
        }), s.$setDirty = function() {
            i.removeClass(t, ho), i.addClass(t, $o), s.$dirty = !0, s.$pristine = !1, c.$setDirty()
        }, s.$setPristine = function() {
            i.setClass(t, ho, $o + " " + Ni), s.$dirty = !1, s.$pristine = !0, s.$submitted = !1, o(u, function(t) {
                t.$setPristine()
            })
        }, s.$setUntouched = function() {
            o(u, function(t) {
                t.$setUntouched()
            })
        }, s.$setSubmitted = function() {
            i.addClass(t, Ni), s.$submitted = !0, c.$setSubmitted()
        }
    }

    function Tn(t) {
        t.$formatters.push(function(e) {
            return t.$isEmpty(e) ? e : e.toString()
        })
    }

    function Mn(t, e, n, r, i, o) {
        Nn(t, e, n, r, i, o), Tn(r)
    }

    function Nn(t, e, n, r, i, o) {
        var a = Gn(e[0].type);
        if (!i.android) {
            var s = !1;
            e.on("compositionstart", function(t) {
                s = !0
            }), e.on("compositionend", function() {
                s = !1, u()
            })
        }
        var u = function(t) {
            if (c && (o.defer.cancel(c), c = null), !s) {
                var i = e.val(),
                    u = t && t.type;
                "password" === a || n.ngTrim && "false" === n.ngTrim || (i = fr(i)), (r.$viewValue !== i || "" === i && r.$$hasNativeValidators) && r.$setViewValue(i, u)
            }
        };
        if (i.hasEvent("input")) e.on("input", u);
        else {
            var c, l = function(t, e, n) {
                c || (c = o.defer(function() {
                    c = null, e && e.value === n || u(t)
                }))
            };
            e.on("keydown", function(t) {
                var e = t.keyCode;
                91 === e || 15 < e && e < 19 || 37 <= e && e <= 40 || l(t, this, this.value)
            }), i.hasEvent("paste") && e.on("paste cut", l)
        }
        e.on("change", u), r.$render = function() {
            e.val(r.$isEmpty(r.$viewValue) ? "" : r.$viewValue)
        }
    }

    function Vn(t, e) {
        if (x(t)) return t;
        if (w(t)) {
            Fi.lastIndex = 0;
            var n = Fi.exec(t);
            if (n) {
                var r = +n[1],
                    i = +n[2],
                    o = 0,
                    a = 0,
                    s = 0,
                    u = 0,
                    c = gn(r),
                    l = 7 * (i - 1);
                return e && (o = e.getHours(), a = e.getMinutes(), s = e.getSeconds(), u = e.getMilliseconds()), new Date(r, 0, c.getDate() + l, o, a, s, u)
            }
        }
        return NaN
    }

    function Dn(t, e) {
        return function(n, r) {
            var i, a;
            if (x(n)) return n;
            if (w(n)) {
                if ('"' == n.charAt(0) && '"' == n.charAt(n.length - 1) && (n = n.substring(1, n.length - 1)), Pi.test(n)) return new Date(n);
                if (t.lastIndex = 0, i = t.exec(n)) return i.shift(), a = r ? {
                    yyyy: r.getFullYear(),
                    MM: r.getMonth() + 1,
                    dd: r.getDate(),
                    HH: r.getHours(),
                    mm: r.getMinutes(),
                    ss: r.getSeconds(),
                    sss: r.getMilliseconds() / 1e3
                } : {
                    yyyy: 1970,
                    MM: 1,
                    dd: 1,
                    HH: 0,
                    mm: 0,
                    ss: 0,
                    sss: 0
                }, o(i, function(t, n) {
                    n < e.length && (a[e[n]] = +t)
                }), new Date(a.yyyy, a.MM - 1, a.dd, a.HH, a.mm, a.ss || 0, 1e3 * a.sss || 0)
            }
            return NaN
        }
    }

    function jn(t, e, r, i) {
        return function(o, a, s, u, c, l, f) {
            function h(t) {
                return t && !(t.getTime && t.getTime() !== t.getTime())
            }

            function $(t) {
                return g(t) ? x(t) ? t : r(t) : n
            }
            Pn(o, a, s, u), Nn(o, a, s, u, c, l);
            var p, d = u && u.$options && u.$options.timezone;
            if (u.$$parserName = t, u.$parsers.push(function(t) {
                    if (u.$isEmpty(t)) return null;
                    if (e.test(t)) {
                        var i = r(t, p);
                        return "UTC" === d && i.setMinutes(i.getMinutes() - i.getTimezoneOffset()), i
                    }
                    return n
                }), u.$formatters.push(function(t) {
                    if (t && !x(t)) throw go("datefmt", "Expected `{0}` to be a date", t);
                    if (h(t)) {
                        if (p = t, p && "UTC" === d) {
                            var e = 6e4 * p.getTimezoneOffset();
                            p = new Date(p.getTime() + e)
                        }
                        return f("date")(t, i, d)
                    }
                    return p = null, ""
                }), g(s.min) || s.ngMin) {
                var v;
                u.$validators.min = function(t) {
                    return !h(t) || m(v) || r(t) >= v
                }, s.$observe("min", function(t) {
                    v = $(t), u.$validate()
                })
            }
            if (g(s.max) || s.ngMax) {
                var y;
                u.$validators.max = function(t) {
                    return !h(t) || m(y) || r(t) <= y
                }, s.$observe("max", function(t) {
                    y = $(t), u.$validate()
                })
            }
        }
    }

    function Pn(t, e, r, i) {
        var o = e[0],
            a = i.$$hasNativeValidators = y(o.validity);
        a && i.$parsers.push(function(t) {
            var r = e.prop(Wn) || {};
            return r.badInput && !r.typeMismatch ? n : t
        })
    }

    function Rn(t, e, r, i, o, a) {
        if (Pn(t, e, r, i), Nn(t, e, r, i, o, a), i.$$parserName = "number", i.$parsers.push(function(t) {
                return i.$isEmpty(t) ? null : Ii.test(t) ? parseFloat(t) : n
            }), i.$formatters.push(function(t) {
                if (!i.$isEmpty(t)) {
                    if (!b(t)) throw go("numfmt", "Expected `{0}` to be a number", t);
                    t = t.toString()
                }
                return t
            }), r.min || r.ngMin) {
            var s;
            i.$validators.min = function(t) {
                return i.$isEmpty(t) || m(s) || t >= s
            }, r.$observe("min", function(t) {
                g(t) && !b(t) && (t = parseFloat(t, 10)), s = b(t) && !isNaN(t) ? t : n, i.$validate()
            })
        }
        if (r.max || r.ngMax) {
            var u;
            i.$validators.max = function(t) {
                return i.$isEmpty(t) || m(u) || t <= u
            }, r.$observe("max", function(t) {
                g(t) && !b(t) && (t = parseFloat(t, 10)), u = b(t) && !isNaN(t) ? t : n, i.$validate()
            })
        }
    }

    function _n(t, e, n, r, i, o) {
        Nn(t, e, n, r, i, o), Tn(r), r.$$parserName = "url", r.$validators.url = function(t, e) {
            var n = t || e;
            return r.$isEmpty(n) || Ri.test(n)
        }
    }

    function In(t, e, n, r, i, o) {
        Nn(t, e, n, r, i, o), Tn(r), r.$$parserName = "email", r.$validators.email = function(t, e) {
            var n = t || e;
            return r.$isEmpty(n) || _i.test(n)
        }
    }

    function qn(t, e, n, r) {
        m(n.name) && e.attr("name", c());
        var i = function(t) {
            e[0].checked && r.$setViewValue(n.value, t && t.type)
        };
        e.on("click", i), r.$render = function() {
            var t = n.value;
            e[0].checked = t == r.$viewValue
        }, n.$observe("value", r.$render)
    }

    function Un(t, e, n, i, o) {
        var a;
        if (g(i)) {
            if (a = t(i), !a.constant) throw r("ngModel")("constexpr", "Expected constant expression for `{0}`, but saw `{1}`.", n, i);
            return a(e)
        }
        return o
    }

    function Fn(t, e, n, r, i, o, a, s) {
        var u = Un(s, t, "ngTrueValue", n.ngTrueValue, !0),
            c = Un(s, t, "ngFalseValue", n.ngFalseValue, !1),
            l = function(t) {
                r.$setViewValue(e[0].checked, t && t.type)
            };
        e.on("click", l), r.$render = function() {
            e[0].checked = r.$viewValue
        }, r.$isEmpty = function(t) {
            return t === !1
        }, r.$formatters.push(function(t) {
            return I(t, u)
        }), r.$parsers.push(function(t) {
            return t ? u : c
        })
    }

    function Hn(t, e) {
        return t = "ngClass" + t, ["$animate", function(n) {
            function r(t, e) {
                var n = [];
                t: for (var r = 0; r < t.length; r++) {
                    for (var i = t[r], o = 0; o < e.length; o++)
                        if (i == e[o]) continue t;
                    n.push(i)
                }
                return n
            }

            function i(t) {
                if (lr(t)) return t;
                if (w(t)) return t.split(" ");
                if (y(t)) {
                    var e = [];
                    return o(t, function(t, n) {
                        t && (e = e.concat(n.split(" ")))
                    }), e
                }
                return t
            }
            return {
                restrict: "AC",
                link: function(a, s, u) {
                    function c(t) {
                        var e = f(t, 1);
                        u.$addClass(e)
                    }

                    function l(t) {
                        var e = f(t, -1);
                        u.$removeClass(e)
                    }

                    function f(t, e) {
                        var n = s.data("$classCounts") || {},
                            r = [];
                        return o(t, function(t) {
                            (e > 0 || n[t]) && (n[t] = (n[t] || 0) + e, n[t] === +(e > 0) && r.push(t))
                        }), s.data("$classCounts", n), r.join(" ")
                    }

                    function h(t, e) {
                        var i = r(e, t),
                            o = r(t, e);
                        i = f(i, 1), o = f(o, -1), i && i.length && n.addClass(s, i), o && o.length && n.removeClass(s, o)
                    }

                    function $(t) {
                        if (e === !0 || a.$index % 2 === e) {
                            var n = i(t || []);
                            if (p) {
                                if (!I(t, p)) {
                                    var r = i(p);
                                    h(r, n)
                                }
                            } else c(n)
                        }
                        p = _(t)
                    }
                    var p;
                    a.$watch(u[t], $, !0), u.$observe("class", function(e) {
                        $(a.$eval(u[t]))
                    }), "ngClass" !== t && a.$watch("$index", function(n, r) {
                        var o = 1 & n;
                        if (o !== (1 & r)) {
                            var s = i(a.$eval(u[t]));
                            o === e ? c(s) : l(s)
                        }
                    })
                }
            }
        }]
    }

    function Ln(t) {
        function e(t, e, u) {
            e === n ? r("$pending", t, u) : i("$pending", t, u), M(e) ? e ? (f(s.$error, t, u), l(s.$$success, t, u)) : (l(s.$error, t, u), f(s.$$success, t, u)) : (f(s.$error, t, u), f(s.$$success, t, u)), s.$pending ? (o(mo, !0), s.$valid = s.$invalid = n, a("", null)) : (o(mo, !1), s.$valid = Bn(s.$error), s.$invalid = !s.$valid, a("", s.$valid));
            var c;
            c = s.$pending && s.$pending[t] ? n : !s.$error[t] && (!!s.$$success[t] || null), a(t, c), h.$setValidity(t, c, s)
        }

        function r(t, e, n) {
            s[t] || (s[t] = {}), l(s[t], e, n)
        }

        function i(t, e, r) {
            s[t] && f(s[t], e, r), Bn(s[t]) && (s[t] = n)
        }

        function o(t, e) {
            e && !c[t] ? ($.addClass(u, t), c[t] = !0) : !e && c[t] && ($.removeClass(u, t), c[t] = !1)
        }

        function a(t, e) {
            t = t ? "-" + nt(t, "-") : "", o(lo + t, e === !0), o(fo + t, e === !1)
        }
        var s = t.ctrl,
            u = t.$element,
            c = {},
            l = t.set,
            f = t.unset,
            h = t.parentForm,
            $ = t.$animate;
        c[fo] = !(c[lo] = u.hasClass(lo)), s.$setValidity = e
    }

    function Bn(t) {
        if (t)
            for (var e in t) return !1;
        return !0
    }
    var zn = /^\/(.+)\/([a-z]*)$/,
        Wn = "validity",
        Gn = function(t) {
            return w(t) ? t.toLowerCase() : t
        },
        Jn = Object.prototype.hasOwnProperty,
        Yn = function(t) {
            return w(t) ? t.toUpperCase() : t
        },
        Zn = function(t) {
            return w(t) ? t.replace(/[A-Z]/g, function(t) {
                return String.fromCharCode(32 | t.charCodeAt(0))
            }) : t
        },
        Kn = function(t) {
            return w(t) ? t.replace(/[a-z]/g, function(t) {
                return String.fromCharCode(t.charCodeAt(0) & -33)
            }) : t
        };
    "i" !== "I".toLowerCase() && (Gn = Zn, Yn = Kn);
    var Xn, Qn, tr, er, nr = [].slice,
        rr = [].splice,
        ir = [].push,
        or = Object.prototype.toString,
        ar = r("ng"),
        sr = t.angular || (t.angular = {}),
        ur = 0;
    Xn = e.documentMode, p.$inject = [], d.$inject = [];
    var cr, lr = Array.isArray,
        fr = function(t) {
            return w(t) ? t.trim() : t
        },
        hr = function(t) {
            return t.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08")
        },
        $r = function() {
            if (g($r.isActive_)) return $r.isActive_;
            var t = !(!e.querySelector("[ng-csp]") && !e.querySelector("[data-ng-csp]"));
            if (!t) try {
                new Function("")
            } catch (e) {
                t = !0
            }
            return $r.isActive_ = t
        },
        pr = ["ng-", "data-ng-", "ng:", "x-ng-"],
        dr = /[A-Z]/g,
        vr = !1,
        mr = 1,
        gr = 3,
        yr = 8,
        wr = 9,
        br = 11,
        xr = {
            full: "1.3.12",
            major: 1,
            minor: 3,
            dot: 12,
            codeName: "outlandish-knitting"
        };
    wt.expando = "ng339";
    var Sr = wt.cache = {},
        Cr = 1,
        Ar = function(t, e, n) {
            t.addEventListener(e, n, !1)
        },
        kr = function(t, e, n) {
            t.removeEventListener(e, n, !1)
        };
    wt._data = function(t) {
        return this.cache[t[this.expando]] || {}
    };
    var Er = /([\:\-\_]+(.))/g,
        Or = /^moz([A-Z])/,
        Tr = {
            mouseleave: "mouseout",
            mouseenter: "mouseover"
        },
        Mr = r("jqLite"),
        Nr = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        Vr = /<|&#?\w+;/,
        Dr = /<([\w:]+)/,
        jr = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Pr = {
            option: [1, '<select multiple="multiple">', "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Pr.optgroup = Pr.option, Pr.tbody = Pr.tfoot = Pr.colgroup = Pr.caption = Pr.thead, Pr.th = Pr.td;
    var Rr = wt.prototype = {
            ready: function(n) {
                function r() {
                    i || (i = !0, n())
                }
                var i = !1;
                "complete" === e.readyState ? setTimeout(r) : (this.on("DOMContentLoaded", r), wt(t).on("load", r))
            },
            toString: function() {
                var t = [];
                return o(this, function(e) {
                    t.push("" + e)
                }), "[" + t.join(", ") + "]"
            },
            eq: function(t) {
                return Qn(t >= 0 ? this[t] : this[this.length + t])
            },
            length: 0,
            push: ir,
            sort: [].sort,
            splice: [].splice
        },
        _r = {};
    o("multiple,selected,checked,disabled,readOnly,required,open".split(","), function(t) {
        _r[Gn(t)] = t
    });
    var Ir = {};
    o("input,select,option,textarea,button,form,details".split(","), function(t) {
        Ir[t] = !0
    });
    var qr = {
        ngMinlength: "minlength",
        ngMaxlength: "maxlength",
        ngMin: "min",
        ngMax: "max",
        ngPattern: "pattern"
    };
    o({
        data: kt,
        removeData: Ct
    }, function(t, e) {
        wt[e] = t
    }), o({
        data: kt,
        inheritedData: Vt,
        scope: function(t) {
            return Qn.data(t, "$scope") || Vt(t.parentNode || t, ["$isolateScope", "$scope"])
        },
        isolateScope: function(t) {
            return Qn.data(t, "$isolateScope") || Qn.data(t, "$isolateScopeNoTemplate")
        },
        controller: Nt,
        injector: function(t) {
            return Vt(t, "$injector")
        },
        removeAttr: function(t, e) {
            t.removeAttribute(e)
        },
        hasClass: Et,
        css: function(t, e, n) {
            return e = dt(e), g(n) ? void(t.style[e] = n) : t.style[e]
        },
        attr: function(t, e, r) {
            var i = Gn(e);
            if (_r[i]) {
                if (!g(r)) return t[e] || (t.attributes.getNamedItem(e) || p).specified ? i : n;
                r ? (t[e] = !0, t.setAttribute(e, i)) : (t[e] = !1, t.removeAttribute(i))
            } else if (g(r)) t.setAttribute(e, r);
            else if (t.getAttribute) {
                var o = t.getAttribute(e, 2);
                return null === o ? n : o
            }
        },
        prop: function(t, e, n) {
            return g(n) ? void(t[e] = n) : t[e]
        },
        text: function() {
            function t(t, e) {
                if (m(e)) {
                    var n = t.nodeType;
                    return n === mr || n === gr ? t.textContent : ""
                }
                t.textContent = e
            }
            return t.$dv = "", t
        }(),
        val: function(t, e) {
            if (m(e)) {
                if (t.multiple && "select" === j(t)) {
                    var n = [];
                    return o(t.options, function(t) {
                        t.selected && n.push(t.value || t.text)
                    }), 0 === n.length ? null : n
                }
                return t.value
            }
            t.value = e
        },
        html: function(t, e) {
            return m(e) ? t.innerHTML : (xt(t, !0), void(t.innerHTML = e))
        },
        empty: Dt
    }, function(t, e) {
        wt.prototype[e] = function(e, r) {
            var i, o, a = this.length;
            if (t !== Dt && (2 == t.length && t !== Et && t !== Nt ? e : r) === n) {
                if (y(e)) {
                    for (i = 0; i < a; i++)
                        if (t === kt) t(this[i], e);
                        else
                            for (o in e) t(this[i], o, e[o]);
                    return this
                }
                for (var s = t.$dv, u = s === n ? Math.min(a, 1) : a, c = 0; c < u; c++) {
                    var l = t(this[c], e, r);
                    s = s ? s + l : l
                }
                return s
            }
            for (i = 0; i < a; i++) t(this[i], e, r);
            return this
        }
    }), o({
        removeData: Ct,
        on: function t(e, n, r, i) {
            if (g(i)) throw Mr("onargs", "jqLite#on() does not support the `selector` or `eventData` parameters");
            if (mt(e)) {
                var o = At(e, !0),
                    a = o.events,
                    s = o.handle;
                s || (s = o.handle = It(e, a));
                for (var u = n.indexOf(" ") >= 0 ? n.split(" ") : [n], c = u.length; c--;) {
                    n = u[c];
                    var l = a[n];
                    l || (a[n] = [], "mouseenter" === n || "mouseleave" === n ? t(e, Tr[n], function(t) {
                        var e = this,
                            r = t.relatedTarget;
                        r && (r === e || e.contains(r)) || s(t, n)
                    }) : "$destroy" !== n && Ar(e, n, s), l = a[n]), l.push(r)
                }
            }
        },
        off: St,
        one: function(t, e, n) {
            t = Qn(t), t.on(e, function r() {
                t.off(e, n), t.off(e, r)
            }), t.on(e, n)
        },
        replaceWith: function(t, e) {
            var n, r = t.parentNode;
            xt(t), o(new wt(e), function(e) {
                n ? r.insertBefore(e, n.nextSibling) : r.replaceChild(e, t), n = e
            })
        },
        children: function(t) {
            var e = [];
            return o(t.childNodes, function(t) {
                t.nodeType === mr && e.push(t)
            }), e
        },
        contents: function(t) {
            return t.contentDocument || t.childNodes || []
        },
        append: function(t, e) {
            var n = t.nodeType;
            if (n === mr || n === br) {
                e = new wt(e);
                for (var r = 0, i = e.length; r < i; r++) {
                    var o = e[r];
                    t.appendChild(o)
                }
            }
        },
        prepend: function(t, e) {
            if (t.nodeType === mr) {
                var n = t.firstChild;
                o(new wt(e), function(e) {
                    t.insertBefore(e, n)
                })
            }
        },
        wrap: function(t, e) {
            e = Qn(e).eq(0).clone()[0];
            var n = t.parentNode;
            n && n.replaceChild(e, t), e.appendChild(t)
        },
        remove: jt,
        detach: function(t) {
            jt(t, !0)
        },
        after: function(t, e) {
            var n = t,
                r = t.parentNode;
            e = new wt(e);
            for (var i = 0, o = e.length; i < o; i++) {
                var a = e[i];
                r.insertBefore(a, n.nextSibling), n = a
            }
        },
        addClass: Tt,
        removeClass: Ot,
        toggleClass: function(t, e, n) {
            e && o(e.split(" "), function(e) {
                var r = n;
                m(r) && (r = !Et(t, e)), (r ? Tt : Ot)(t, e)
            })
        },
        parent: function(t) {
            var e = t.parentNode;
            return e && e.nodeType !== br ? e : null
        },
        next: function(t) {
            return t.nextElementSibling
        },
        find: function(t, e) {
            return t.getElementsByTagName ? t.getElementsByTagName(e) : []
        },
        clone: bt,
        triggerHandler: function(t, e, n) {
            var r, i, a, s = e.type || e,
                u = At(t),
                c = u && u.events,
                l = c && c[s];
            l && (r = {
                preventDefault: function() {
                    this.defaultPrevented = !0
                },
                isDefaultPrevented: function() {
                    return this.defaultPrevented === !0
                },
                stopImmediatePropagation: function() {
                    this.immediatePropagationStopped = !0
                },
                isImmediatePropagationStopped: function() {
                    return this.immediatePropagationStopped === !0
                },
                stopPropagation: p,
                type: s,
                target: t
            }, e.type && (r = f(r, e)), i = _(l), a = n ? [r].concat(n) : [r], o(i, function(e) {
                r.isImmediatePropagationStopped() || e.apply(t, a)
            }))
        }
    }, function(t, e) {
        wt.prototype[e] = function(e, n, r) {
            for (var i, o = 0, a = this.length; o < a; o++) m(i) ? (i = t(this[o], e, n, r), g(i) && (i = Qn(i))) : Mt(i, t(this[o], e, n, r));
            return g(i) ? i : this
        }, wt.prototype.bind = wt.prototype.on, wt.prototype.unbind = wt.prototype.off
    }), Ft.prototype = {
        put: function(t, e) {
            this[Ut(t, this.nextUid)] = e
        },
        get: function(t) {
            return this[Ut(t, this.nextUid)]
        },
        remove: function(t) {
            var e = this[t = Ut(t, this.nextUid)];
            return delete this[t], e
        }
    };
    var Ur = /^function\s*[^\(]*\(\s*([^\)]*)\)/m,
        Fr = /,/,
        Hr = /^\s*(_?)(\S+?)\1\s*$/,
        Lr = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm,
        Br = r("$injector");
    Bt.$$annotate = Lt;
    var zr = r("$animate"),
        Wr = ["$provide", function(t) {
            this.$$selectors = {}, this.register = function(e, n) {
                var r = e + "-animation";
                if (e && "." != e.charAt(0)) throw zr("notcsel", "Expecting class selector starting with '.' got '{0}'.", e);
                this.$$selectors[e.substr(1)] = r, t.factory(r, n)
            }, this.classNameFilter = function(t) {
                return 1 === arguments.length && (this.$$classNameFilter = t instanceof RegExp ? t : null), this.$$classNameFilter
            }, this.$get = ["$$q", "$$asyncCallback", "$rootScope", function(t, e, n) {
                function r(e) {
                    var r, i = t.defer();
                    return i.promise.$$cancelFn = function() {
                        r && r()
                    }, n.$$postDigest(function() {
                        r = e(function() {
                            i.resolve()
                        })
                    }), i.promise
                }

                function i(t, e) {
                    var n = [],
                        r = [],
                        i = ct();
                    return o((t.attr("class") || "").split(/\s+/), function(t) {
                        i[t] = !0
                    }), o(e, function(t, e) {
                        var o = i[e];
                        t === !1 && o ? r.push(e) : t !== !0 || o || n.push(e)
                    }), n.length + r.length > 0 && [n.length ? n : null, r.length ? r : null]
                }

                function a(t, e, n) {
                    for (var r = 0, i = e.length; r < i; ++r) {
                        var o = e[r];
                        t[o] = n
                    }
                }

                function s() {
                    return c || (c = t.defer(), e(function() {
                        c.resolve(), c = null
                    })), c.promise
                }

                function u(t, e) {
                    if (sr.isObject(e)) {
                        var n = f(e.from || {}, e.to || {});
                        t.css(n)
                    }
                }
                var c;
                return {
                    animate: function(t, e, n) {
                        return u(t, {
                            from: e,
                            to: n
                        }), s()
                    },
                    enter: function(t, e, n, r) {
                        return u(t, r), n ? n.after(t) : e.prepend(t), s()
                    },
                    leave: function(t, e) {
                        return t.remove(), s()
                    },
                    move: function(t, e, n, r) {
                        return this.enter(t, e, n, r)
                    },
                    addClass: function(t, e, n) {
                        return this.setClass(t, e, [], n)
                    },
                    $$addClassImmediately: function(t, e, n) {
                        return t = Qn(t), e = w(e) ? e : lr(e) ? e.join(" ") : "", o(t, function(t) {
                            Tt(t, e)
                        }), u(t, n), s()
                    },
                    removeClass: function(t, e, n) {
                        return this.setClass(t, [], e, n)
                    },
                    $$removeClassImmediately: function(t, e, n) {
                        return t = Qn(t), e = w(e) ? e : lr(e) ? e.join(" ") : "", o(t, function(t) {
                            Ot(t, e)
                        }), u(t, n), s()
                    },
                    setClass: function(t, e, n, o) {
                        var s = this,
                            u = "$$animateClasses",
                            c = !1;
                        t = Qn(t);
                        var l = t.data(u);
                        l ? o && l.options && (l.options = sr.extend(l.options || {}, o)) : (l = {
                            classes: {},
                            options: o
                        }, c = !0);
                        var f = l.classes;
                        return e = lr(e) ? e : e.split(" "), n = lr(n) ? n : n.split(" "), a(f, e, !0), a(f, n, !1), c && (l.promise = r(function(e) {
                            var n = t.data(u);
                            if (t.removeData(u), n) {
                                var r = i(t, n.classes);
                                r && s.$$setClassImmediately(t, r[0], r[1], n.options)
                            }
                            e()
                        }), t.data(u, l)), l.promise
                    },
                    $$setClassImmediately: function(t, e, n, r) {
                        return e && this.$$addClassImmediately(t, e), n && this.$$removeClassImmediately(t, n), u(t, r), s()
                    },
                    enabled: p,
                    cancel: p
                }
            }]
        }],
        Gr = r("$compile");
    Kt.$inject = ["$provide", "$$sanitizeUriProvider"];
    var Jr = /^((?:x|data)[\:\-_])/i,
        Yr = r("$controller"),
        Zr = "application/json",
        Kr = {
            "Content-Type": Zr + ";charset=utf-8"
        },
        Xr = /^\[|^\{(?!\{)/,
        Qr = {
            "[": /]$/,
            "{": /}$/
        },
        ti = /^\)\]\}',?\n/,
        ei = r("$interpolate"),
        ni = /^([^\?#]*)(\?([^#]*))?(#(.*))?$/,
        ri = {
            http: 80,
            https: 443,
            ftp: 21
        },
        ii = r("$location"),
        oi = {
            $$html5: !1,
            $$replace: !1,
            absUrl: Oe("$$absUrl"),
            url: function(t) {
                if (m(t)) return this.$$url;
                var e = ni.exec(t);
                return (e[1] || "" === t) && this.path(decodeURIComponent(e[1])), (e[2] || e[1] || "" === t) && this.search(e[3] || ""), this.hash(e[5] || ""), this
            },
            protocol: Oe("$$protocol"),
            host: Oe("$$host"),
            port: Oe("$$port"),
            path: Te("$$path", function(t) {
                return t = null !== t ? t.toString() : "", "/" == t.charAt(0) ? t : "/" + t
            }),
            search: function(t, e) {
                switch (arguments.length) {
                    case 0:
                        return this.$$search;
                    case 1:
                        if (w(t) || b(t)) t = t.toString(), this.$$search = G(t);
                        else {
                            if (!y(t)) throw ii("isrcharg", "The first argument of the `$location#search()` call must be a string or an object.");
                            t = R(t, {}), o(t, function(e, n) {
                                null == e && delete t[n]
                            }), this.$$search = t
                        }
                        break;
                    default:
                        m(e) || null === e ? delete this.$$search[t] : this.$$search[t] = e
                }
                return this.$$compose(), this
            },
            hash: Te("$$hash", function(t) {
                return null !== t ? t.toString() : ""
            }),
            replace: function() {
                return this.$$replace = !0, this
            }
        };
    o([Ee, ke, Ae], function(t) {
        t.prototype = Object.create(oi), t.prototype.state = function(e) {
            if (!arguments.length) return this.$$state;
            if (t !== Ae || !this.$$html5) throw ii("nostate", "History API state support is available only in HTML5 mode and only in browsers supporting HTML5 History API");
            return this.$$state = m(e) ? null : e, this
        }
    });
    var ai = r("$parse"),
        si = Function.prototype.call,
        ui = Function.prototype.apply,
        ci = Function.prototype.bind,
        li = ct();
    o({
        null: function() {
            return null
        },
        true: function() {
            return !0
        },
        false: function() {
            return !1
        },
        undefined: function() {}
    }, function(t, e) {
        t.constant = t.literal = t.sharedGetter = !0, li[e] = t
    }), li.this = function(t) {
        return t
    }, li.this.sharedGetter = !0;
    var fi = f(ct(), {
            "+": function(t, e, r, i) {
                return r = r(t, e), i = i(t, e), g(r) ? g(i) ? r + i : r : g(i) ? i : n
            },
            "-": function(t, e, n, r) {
                return n = n(t, e), r = r(t, e), (g(n) ? n : 0) - (g(r) ? r : 0)
            },
            "*": function(t, e, n, r) {
                return n(t, e) * r(t, e)
            },
            "/": function(t, e, n, r) {
                return n(t, e) / r(t, e)
            },
            "%": function(t, e, n, r) {
                return n(t, e) % r(t, e)
            },
            "===": function(t, e, n, r) {
                return n(t, e) === r(t, e)
            },
            "!==": function(t, e, n, r) {
                return n(t, e) !== r(t, e)
            },
            "==": function(t, e, n, r) {
                return n(t, e) == r(t, e)
            },
            "!=": function(t, e, n, r) {
                return n(t, e) != r(t, e)
            },
            "<": function(t, e, n, r) {
                return n(t, e) < r(t, e)
            },
            ">": function(t, e, n, r) {
                return n(t, e) > r(t, e)
            },
            "<=": function(t, e, n, r) {
                return n(t, e) <= r(t, e)
            },
            ">=": function(t, e, n, r) {
                return n(t, e) >= r(t, e)
            },
            "&&": function(t, e, n, r) {
                return n(t, e) && r(t, e)
            },
            "||": function(t, e, n, r) {
                return n(t, e) || r(t, e)
            },
            "!": function(t, e, n) {
                return !n(t, e)
            },
            "=": !0,
            "|": !0
        }),
        hi = {
            n: "\n",
            f: "\f",
            r: "\r",
            t: "\t",
            v: "\v",
            "'": "'",
            '"': '"'
        },
        $i = function(t) {
            this.options = t
        };
    $i.prototype = {
        constructor: $i,
        lex: function(t) {
            for (this.text = t, this.index = 0, this.tokens = []; this.index < this.text.length;) {
                var e = this.text.charAt(this.index);
                if ('"' === e || "'" === e) this.readString(e);
                else if (this.isNumber(e) || "." === e && this.isNumber(this.peek())) this.readNumber();
                else if (this.isIdent(e)) this.readIdent();
                else if (this.is(e, "(){}[].,;:?")) this.tokens.push({
                    index: this.index,
                    text: e
                }), this.index++;
                else if (this.isWhitespace(e)) this.index++;
                else {
                    var n = e + this.peek(),
                        r = n + this.peek(2),
                        i = fi[e],
                        o = fi[n],
                        a = fi[r];
                    if (i || o || a) {
                        var s = a ? r : o ? n : e;
                        this.tokens.push({
                            index: this.index,
                            text: s,
                            operator: !0
                        }), this.index += s.length
                    } else this.throwError("Unexpected next character ", this.index, this.index + 1)
                }
            }
            return this.tokens
        },
        is: function(t, e) {
            return e.indexOf(t) !== -1
        },
        peek: function(t) {
            var e = t || 1;
            return this.index + e < this.text.length && this.text.charAt(this.index + e)
        },
        isNumber: function(t) {
            return "0" <= t && t <= "9" && "string" == typeof t
        },
        isWhitespace: function(t) {
            return " " === t || "\r" === t || "\t" === t || "\n" === t || "\v" === t || " " === t
        },
        isIdent: function(t) {
            return "a" <= t && t <= "z" || "A" <= t && t <= "Z" || "_" === t || "$" === t
        },
        isExpOperator: function(t) {
            return "-" === t || "+" === t || this.isNumber(t)
        },
        throwError: function(t, e, n) {
            n = n || this.index;
            var r = g(e) ? "s " + e + "-" + this.index + " [" + this.text.substring(e, n) + "]" : " " + n;
            throw ai("lexerr", "Lexer Error: {0} at column{1} in expression [{2}].", t, r, this.text)
        },
        readNumber: function() {
            for (var t = "", e = this.index; this.index < this.text.length;) {
                var n = Gn(this.text.charAt(this.index));
                if ("." == n || this.isNumber(n)) t += n;
                else {
                    var r = this.peek();
                    if ("e" == n && this.isExpOperator(r)) t += n;
                    else if (this.isExpOperator(n) && r && this.isNumber(r) && "e" == t.charAt(t.length - 1)) t += n;
                    else {
                        if (!this.isExpOperator(n) || r && this.isNumber(r) || "e" != t.charAt(t.length - 1)) break;
                        this.throwError("Invalid exponent")
                    }
                }
                this.index++
            }
            this.tokens.push({
                index: e,
                text: t,
                constant: !0,
                value: Number(t)
            })
        },
        readIdent: function() {
            for (var t = this.index; this.index < this.text.length;) {
                var e = this.text.charAt(this.index);
                if (!this.isIdent(e) && !this.isNumber(e)) break;
                this.index++
            }
            this.tokens.push({
                index: t,
                text: this.text.slice(t, this.index),
                identifier: !0
            })
        },
        readString: function(t) {
            var e = this.index;
            this.index++;
            for (var n = "", r = t, i = !1; this.index < this.text.length;) {
                var o = this.text.charAt(this.index);
                if (r += o, i) {
                    if ("u" === o) {
                        var a = this.text.substring(this.index + 1, this.index + 5);
                        a.match(/[\da-f]{4}/i) || this.throwError("Invalid unicode escape [\\u" + a + "]"), this.index += 4, n += String.fromCharCode(parseInt(a, 16))
                    } else {
                        var s = hi[o];
                        n += s || o
                    }
                    i = !1
                } else if ("\\" === o) i = !0;
                else {
                    if (o === t) return this.index++, void this.tokens.push({
                        index: e,
                        text: r,
                        constant: !0,
                        value: n
                    });
                    n += o
                }
                this.index++
            }
            this.throwError("Unterminated quote", e)
        }
    };
    var pi = function(t, e, n) {
        this.lexer = t, this.$filter = e, this.options = n
    };
    pi.ZERO = f(function() {
        return 0
    }, {
        sharedGetter: !0,
        constant: !0
    }), pi.prototype = {
        constructor: pi,
        parse: function(t) {
            this.text = t, this.tokens = this.lexer.lex(t);
            var e = this.statements();
            return 0 !== this.tokens.length && this.throwError("is an unexpected token", this.tokens[0]), e.literal = !!e.literal, e.constant = !!e.constant, e
        },
        primary: function() {
            var t;
            this.expect("(") ? (t = this.filterChain(), this.consume(")")) : this.expect("[") ? t = this.arrayDeclaration() : this.expect("{") ? t = this.object() : this.peek().identifier && this.peek().text in li ? t = li[this.consume().text] : this.peek().identifier ? t = this.identifier() : this.peek().constant ? t = this.constant() : this.throwError("not a primary expression", this.peek());
            for (var e, n; e = this.expect("(", "[", ".");) "(" === e.text ? (t = this.functionCall(t, n), n = null) : "[" === e.text ? (n = t, t = this.objectIndex(t)) : "." === e.text ? (n = t, t = this.fieldAccess(t)) : this.throwError("IMPOSSIBLE");
            return t
        },
        throwError: function(t, e) {
            throw ai("syntax", "Syntax Error: Token '{0}' {1} at column {2} of the expression [{3}] starting at [{4}].", e.text, t, e.index + 1, this.text, this.text.substring(e.index))
        },
        peekToken: function() {
            if (0 === this.tokens.length) throw ai("ueoe", "Unexpected end of expression: {0}", this.text);
            return this.tokens[0]
        },
        peek: function(t, e, n, r) {
            return this.peekAhead(0, t, e, n, r)
        },
        peekAhead: function(t, e, n, r, i) {
            if (this.tokens.length > t) {
                var o = this.tokens[t],
                    a = o.text;
                if (a === e || a === n || a === r || a === i || !e && !n && !r && !i) return o
            }
            return !1
        },
        expect: function(t, e, n, r) {
            var i = this.peek(t, e, n, r);
            return !!i && (this.tokens.shift(), i)
        },
        consume: function(t) {
            if (0 === this.tokens.length) throw ai("ueoe", "Unexpected end of expression: {0}", this.text);
            var e = this.expect(t);
            return e || this.throwError("is unexpected, expecting [" + t + "]", this.peek()), e
        },
        unaryFn: function(t, e) {
            var n = fi[t];
            return f(function(t, r) {
                return n(t, r, e)
            }, {
                constant: e.constant,
                inputs: [e]
            })
        },
        binaryFn: function(t, e, n, r) {
            var i = fi[e];
            return f(function(e, r) {
                return i(e, r, t, n)
            }, {
                constant: t.constant && n.constant,
                inputs: !r && [t, n]
            })
        },
        identifier: function() {
            for (var t = this.consume().text; this.peek(".") && this.peekAhead(1).identifier && !this.peekAhead(2, "(");) t += this.consume().text + this.consume().text;
            return Ue(t, this.options, this.text)
        },
        constant: function() {
            var t = this.consume().value;
            return f(function() {
                return t
            }, {
                constant: !0,
                literal: !0
            })
        },
        statements: function() {
            for (var t = [];;)
                if (this.tokens.length > 0 && !this.peek("}", ")", ";", "]") && t.push(this.filterChain()), !this.expect(";")) return 1 === t.length ? t[0] : function(e, n) {
                    for (var r, i = 0, o = t.length; i < o; i++) r = t[i](e, n);
                    return r
                }
        },
        filterChain: function() {
            for (var t, e = this.expression(); t = this.expect("|");) e = this.filter(e);
            return e
        },
        filter: function(t) {
            var e, r, i = this.$filter(this.consume().text);
            if (this.peek(":"))
                for (e = [], r = []; this.expect(":");) e.push(this.expression());
            var o = [t].concat(e || []);
            return f(function(o, a) {
                var s = t(o, a);
                if (r) {
                    r[0] = s;
                    for (var u = e.length; u--;) r[u + 1] = e[u](o, a);
                    return i.apply(n, r)
                }
                return i(s)
            }, {
                constant: !i.$stateful && o.every(Pe),
                inputs: !i.$stateful && o
            })
        },
        expression: function() {
            return this.assignment()
        },
        assignment: function() {
            var t, e, n = this.ternary();
            return (e = this.expect("=")) ? (n.assign || this.throwError("implies assignment but [" + this.text.substring(0, e.index) + "] can not be assigned to", e), t = this.ternary(), f(function(e, r) {
                return n.assign(e, t(e, r), r)
            }, {
                inputs: [n, t]
            })) : n
        },
        ternary: function() {
            var t, e, n = this.logicalOR();
            if ((e = this.expect("?")) && (t = this.assignment(), this.consume(":"))) {
                var r = this.assignment();
                return f(function(e, i) {
                    return n(e, i) ? t(e, i) : r(e, i)
                }, {
                    constant: n.constant && t.constant && r.constant
                })
            }
            return n
        },
        logicalOR: function() {
            for (var t, e = this.logicalAND(); t = this.expect("||");) e = this.binaryFn(e, t.text, this.logicalAND(), !0);
            return e
        },
        logicalAND: function() {
            for (var t, e = this.equality(); t = this.expect("&&");) e = this.binaryFn(e, t.text, this.equality(), !0);
            return e
        },
        equality: function() {
            for (var t, e = this.relational(); t = this.expect("==", "!=", "===", "!==");) e = this.binaryFn(e, t.text, this.relational());
            return e
        },
        relational: function() {
            for (var t, e = this.additive(); t = this.expect("<", ">", "<=", ">=");) e = this.binaryFn(e, t.text, this.additive());
            return e
        },
        additive: function() {
            for (var t, e = this.multiplicative(); t = this.expect("+", "-");) e = this.binaryFn(e, t.text, this.multiplicative());
            return e
        },
        multiplicative: function() {
            for (var t, e = this.unary(); t = this.expect("*", "/", "%");) e = this.binaryFn(e, t.text, this.unary());
            return e
        },
        unary: function() {
            var t;
            return this.expect("+") ? this.primary() : (t = this.expect("-")) ? this.binaryFn(pi.ZERO, t.text, this.unary()) : (t = this.expect("!")) ? this.unaryFn(t.text, this.unary()) : this.primary()
        },
        fieldAccess: function(t) {
            var e = this.identifier();
            return f(function(r, i, o) {
                var a = o || t(r, i);
                return null == a ? n : e(a)
            }, {
                assign: function(n, r, i) {
                    var o = t(n, i);
                    return o || t.assign(n, o = {}, i), e.assign(o, r)
                }
            })
        },
        objectIndex: function(t) {
            var e = this.text,
                r = this.expression();
            return this.consume("]"), f(function(i, o) {
                var a, s = t(i, o),
                    u = r(i, o);
                return Ve(u, e), s ? a = De(s[u], e) : n
            }, {
                assign: function(n, i, o) {
                    var a = Ve(r(n, o), e),
                        s = De(t(n, o), e);
                    return s || t.assign(n, s = {}, o), s[a] = i
                }
            })
        },
        functionCall: function(t, e) {
            var r = [];
            if (")" !== this.peekToken().text)
                do r.push(this.expression()); while (this.expect(","));
            this.consume(")");
            var i = this.text,
                o = r.length ? [] : null;
            return function(a, s) {
                var u = e ? e(a, s) : g(e) ? n : a,
                    c = t(a, s, u) || p;
                if (o)
                    for (var l = r.length; l--;) o[l] = De(r[l](a, s), i);
                De(u, i), je(c, i);
                var f = c.apply ? c.apply(u, o) : c(o[0], o[1], o[2], o[3], o[4]);
                return o && (o.length = 0), De(f, i)
            }
        },
        arrayDeclaration: function() {
            var t = [];
            if ("]" !== this.peekToken().text)
                do {
                    if (this.peek("]")) break;
                    t.push(this.expression())
                } while (this.expect(","));
            return this.consume("]"), f(function(e, n) {
                for (var r = [], i = 0, o = t.length; i < o; i++) r.push(t[i](e, n));
                return r
            }, {
                literal: !0,
                constant: t.every(Pe),
                inputs: t
            })
        },
        object: function() {
            var t = [],
                e = [];
            if ("}" !== this.peekToken().text)
                do {
                    if (this.peek("}")) break;
                    var n = this.consume();
                    n.constant ? t.push(n.value) : n.identifier ? t.push(n.text) : this.throwError("invalid key", n), this.consume(":"), e.push(this.expression())
                } while (this.expect(","));
            return this.consume("}"), f(function(n, r) {
                for (var i = {}, o = 0, a = e.length; o < a; o++) i[t[o]] = e[o](n, r);
                return i
            }, {
                literal: !0,
                constant: e.every(Pe),
                inputs: e
            })
        }
    };
    var di = ct(),
        vi = ct(),
        mi = Object.prototype.valueOf,
        gi = r("$sce"),
        yi = {
            HTML: "html",
            CSS: "css",
            URL: "url",
            RESOURCE_URL: "resourceUrl",
            JS: "js"
        },
        Gr = r("$compile"),
        wi = e.createElement("a"),
        bi = rn(t.location.href);
    sn.$inject = ["$provide"], fn.$inject = ["$locale"], hn.$inject = ["$locale"];
    var xi = ".",
        Si = {
            yyyy: dn("FullYear", 4),
            yy: dn("FullYear", 2, 0, !0),
            y: dn("FullYear", 1),
            MMMM: vn("Month"),
            MMM: vn("Month", !0),
            MM: dn("Month", 2, 1),
            M: dn("Month", 1, 1),
            dd: dn("Date", 2),
            d: dn("Date", 1),
            HH: dn("Hours", 2),
            H: dn("Hours", 1),
            hh: dn("Hours", 2, -12),
            h: dn("Hours", 1, -12),
            mm: dn("Minutes", 2),
            m: dn("Minutes", 1),
            ss: dn("Seconds", 2),
            s: dn("Seconds", 1),
            sss: dn("Milliseconds", 3),
            EEEE: vn("Day"),
            EEE: vn("Day", !0),
            a: bn,
            Z: mn,
            ww: wn(2),
            w: wn(1)
        },
        Ci = /((?:[^yMdHhmsaZEw']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|d+|H+|h+|m+|s+|a|Z|w+))(.*)/,
        Ai = /^\-?\d+$/;
    xn.$inject = ["$locale"];
    var ki = v(Gn),
        Ei = v(Yn);
    An.$inject = ["$parse"];
    var Oi = v({
            restrict: "E",
            compile: function(t, e) {
                if (!e.href && !e.xlinkHref && !e.name) return function(t, e) {
                    if ("a" === e[0].nodeName.toLowerCase()) {
                        var n = "[object SVGAnimatedString]" === or.call(e.prop("href")) ? "xlink:href" : "href";
                        e.on("click", function(t) {
                            e.attr(n) || t.preventDefault()
                        })
                    }
                }
            }
        }),
        Ti = {};
    o(_r, function(t, e) {
        if ("multiple" != t) {
            var n = Xt("ng-" + e);
            Ti[n] = function() {
                return {
                    restrict: "A",
                    priority: 100,
                    link: function(t, r, i) {
                        t.$watch(i[n], function(t) {
                            i.$set(e, !!t)
                        })
                    }
                }
            }
        }
    }), o(qr, function(t, e) {
        Ti[e] = function() {
            return {
                priority: 100,
                link: function(t, n, r) {
                    if ("ngPattern" === e && "/" == r.ngPattern.charAt(0)) {
                        var i = r.ngPattern.match(zn);
                        if (i) return void r.$set("ngPattern", new RegExp(i[1], i[2]))
                    }
                    t.$watch(r[e], function(t) {
                        r.$set(e, t)
                    })
                }
            }
        }
    }), o(["src", "srcset", "href"], function(t) {
        var e = Xt("ng-" + t);
        Ti[e] = function() {
            return {
                priority: 99,
                link: function(n, r, i) {
                    var o = t,
                        a = t;
                    "href" === t && "[object SVGAnimatedString]" === or.call(r.prop("href")) && (a = "xlinkHref", i.$attr[a] = "xlink:href", o = null), i.$observe(e, function(e) {
                        return e ? (i.$set(a, e), void(Xn && o && r.prop(o, i[a]))) : void("href" === t && i.$set(a, null))
                    })
                }
            }
        }
    });
    var Mi = {
            $addControl: p,
            $$renameControl: En,
            $removeControl: p,
            $setValidity: p,
            $setDirty: p,
            $setPristine: p,
            $setSubmitted: p
        },
        Ni = "ng-submitted";
    On.$inject = ["$element", "$attrs", "$scope", "$animate", "$interpolate"];
    var Vi = function(t) {
            return ["$timeout", function(e) {
                var r = {
                    name: "form",
                    restrict: t ? "EAC" : "E",
                    controller: On,
                    compile: function(t) {
                        return t.addClass(ho).addClass(lo), {
                            pre: function(t, r, i, o) {
                                if (!("action" in i)) {
                                    var a = function(e) {
                                        t.$apply(function() {
                                            o.$commitViewValue(), o.$setSubmitted()
                                        }), e.preventDefault()
                                    };
                                    Ar(r[0], "submit", a), r.on("$destroy", function() {
                                        e(function() {
                                            kr(r[0], "submit", a)
                                        }, 0, !1)
                                    })
                                }
                                var s = o.$$parentForm,
                                    u = o.$name;
                                u && (Re(t, null, u, o, u), i.$observe(i.name ? "name" : "ngForm", function(e) {
                                    u !== e && (Re(t, null, u, n, u), u = e, Re(t, null, u, o, u), s.$$renameControl(o, u))
                                })), r.on("$destroy", function() {
                                    s.$removeControl(o), u && Re(t, null, u, n, u), f(o, Mi)
                                })
                            }
                        }
                    }
                };
                return r
            }]
        },
        Di = Vi(),
        ji = Vi(!0),
        Pi = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,
        Ri = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
        _i = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,
        Ii = /^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/,
        qi = /^(\d{4})-(\d{2})-(\d{2})$/,
        Ui = /^(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,
        Fi = /^(\d{4})-W(\d\d)$/,
        Hi = /^(\d{4})-(\d\d)$/,
        Li = /^(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,
        Bi = {
            text: Mn,
            date: jn("date", qi, Dn(qi, ["yyyy", "MM", "dd"]), "yyyy-MM-dd"),
            "datetime-local": jn("datetimelocal", Ui, Dn(Ui, ["yyyy", "MM", "dd", "HH", "mm", "ss", "sss"]), "yyyy-MM-ddTHH:mm:ss.sss"),
            time: jn("time", Li, Dn(Li, ["HH", "mm", "ss", "sss"]), "HH:mm:ss.sss"),
            week: jn("week", Fi, Vn, "yyyy-Www"),
            month: jn("month", Hi, Dn(Hi, ["yyyy", "MM"]), "yyyy-MM"),
            number: Rn,
            url: _n,
            email: In,
            radio: qn,
            checkbox: Fn,
            hidden: p,
            button: p,
            submit: p,
            reset: p,
            file: p
        },
        zi = ["$browser", "$sniffer", "$filter", "$parse", function(t, e, n, r) {
            return {
                restrict: "E",
                require: ["?ngModel"],
                link: {
                    pre: function(i, o, a, s) {
                        s[0] && (Bi[Gn(a.type)] || Bi.text)(i, o, a, s[0], e, t, n, r)
                    }
                }
            }
        }],
        Wi = /^(true|false|\d+)$/,
        Gi = function() {
            return {
                restrict: "A",
                priority: 100,
                compile: function(t, e) {
                    return Wi.test(e.ngValue) ? function(t, e, n) {
                        n.$set("value", t.$eval(n.ngValue))
                    } : function(t, e, n) {
                        t.$watch(n.ngValue, function(t) {
                            n.$set("value", t)
                        })
                    }
                }
            }
        },
        Ji = ["$compile", function(t) {
            return {
                restrict: "AC",
                compile: function(e) {
                    return t.$$addBindingClass(e),
                        function(e, r, i) {
                            t.$$addBindingInfo(r, i.ngBind), r = r[0], e.$watch(i.ngBind, function(t) {
                                r.textContent = t === n ? "" : t
                            })
                        }
                }
            }
        }],
        Yi = ["$interpolate", "$compile", function(t, e) {
            return {
                compile: function(r) {
                    return e.$$addBindingClass(r),
                        function(r, i, o) {
                            var a = t(i.attr(o.$attr.ngBindTemplate));
                            e.$$addBindingInfo(i, a.expressions), i = i[0], o.$observe("ngBindTemplate", function(t) {
                                i.textContent = t === n ? "" : t
                            })
                        }
                }
            }
        }],
        Zi = ["$sce", "$parse", "$compile", function(t, e, n) {
            return {
                restrict: "A",
                compile: function(r, i) {
                    var o = e(i.ngBindHtml),
                        a = e(i.ngBindHtml, function(t) {
                            return (t || "").toString()
                        });
                    return n.$$addBindingClass(r),
                        function(e, r, i) {
                            n.$$addBindingInfo(r, i.ngBindHtml), e.$watch(a, function() {
                                r.html(t.getTrustedHtml(o(e)) || "")
                            })
                        }
                }
            }
        }],
        Ki = v({
            restrict: "A",
            require: "ngModel",
            link: function(t, e, n, r) {
                r.$viewChangeListeners.push(function() {
                    t.$eval(n.ngChange)
                })
            }
        }),
        Xi = Hn("", !0),
        Qi = Hn("Odd", 0),
        to = Hn("Even", 1),
        eo = kn({
            compile: function(t, e) {
                e.$set("ngCloak", n), t.removeClass("ng-cloak")
            }
        }),
        no = [function() {
            return {
                restrict: "A",
                scope: !0,
                controller: "@",
                priority: 500
            }
        }],
        ro = {},
        io = {
            blur: !0,
            focus: !0
        };
    o("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "), function(t) {
        var e = Xt("ng-" + t);
        ro[e] = ["$parse", "$rootScope", function(n, r) {
            return {
                restrict: "A",
                compile: function(i, o) {
                    var a = n(o[e], null, !0);
                    return function(e, n) {
                        n.on(t, function(n) {
                            var i = function() {
                                a(e, {
                                    $event: n
                                })
                            };
                            io[t] && r.$$phase ? e.$evalAsync(i) : e.$apply(i)
                        })
                    }
                }
            }
        }]
    });
    var oo = ["$animate", function(t) {
            return {
                multiElement: !0,
                transclude: "element",
                priority: 600,
                terminal: !0,
                restrict: "A",
                $$tlb: !0,
                link: function(n, r, i, o, a) {
                    var s, u, c;
                    n.$watch(i.ngIf, function(n) {
                        n ? u || a(function(n, o) {
                            u = o, n[n.length++] = e.createComment(" end ngIf: " + i.ngIf + " "), s = {
                                clone: n
                            }, t.enter(n, r.parent(), r)
                        }) : (c && (c.remove(), c = null), u && (u.$destroy(), u = null), s && (c = ut(s.clone), t.leave(c).then(function() {
                            c = null
                        }), s = null))
                    })
                }
            }
        }],
        ao = ["$templateRequest", "$anchorScroll", "$animate", "$sce", function(t, e, n, r) {
            return {
                restrict: "ECA",
                priority: 400,
                terminal: !0,
                transclude: "element",
                controller: sr.noop,
                compile: function(i, o) {
                    var a = o.ngInclude || o.src,
                        s = o.onload || "",
                        u = o.autoscroll;
                    return function(i, o, c, l, f) {
                        var h, $, p, d = 0,
                            v = function() {
                                $ && ($.remove(), $ = null), h && (h.$destroy(), h = null), p && (n.leave(p).then(function() {
                                    $ = null
                                }), $ = p, p = null)
                            };
                        i.$watch(r.parseAsResourceUrl(a), function(r) {
                            var a = function() {
                                    !g(u) || u && !i.$eval(u) || e()
                                },
                                c = ++d;
                            r ? (t(r, !0).then(function(t) {
                                if (c === d) {
                                    var e = i.$new();
                                    l.template = t;
                                    var u = f(e, function(t) {
                                        v(), n.enter(t, null, o).then(a)
                                    });
                                    h = e, p = u, h.$emit("$includeContentLoaded", r), i.$eval(s)
                                }
                            }, function() {
                                c === d && (v(), i.$emit("$includeContentError", r))
                            }), i.$emit("$includeContentRequested", r)) : (v(), l.template = null)
                        })
                    }
                }
            }
        }],
        so = ["$compile", function(t) {
            return {
                restrict: "ECA",
                priority: -400,
                require: "ngInclude",
                link: function(n, r, i, o) {
                    return /SVG/.test(r[0].toString()) ? (r.empty(), void t(gt(o.template, e).childNodes)(n, function(t) {
                        r.append(t)
                    }, {
                        futureParentElement: r
                    })) : (r.html(o.template), void t(r.contents())(n))
                }
            }
        }],
        uo = kn({
            priority: 450,
            compile: function() {
                return {
                    pre: function(t, e, n) {
                        t.$eval(n.ngInit)
                    }
                }
            }
        }),
        co = function() {
            return {
                restrict: "A",
                priority: 100,
                require: "ngModel",
                link: function(t, e, r, i) {
                    var a = e.attr(r.$attr.ngList) || ", ",
                        s = "false" !== r.ngTrim,
                        u = s ? fr(a) : a,
                        c = function(t) {
                            if (!m(t)) {
                                var e = [];
                                return t && o(t.split(u), function(t) {
                                    t && e.push(s ? fr(t) : t)
                                }), e
                            }
                        };
                    i.$parsers.push(c), i.$formatters.push(function(t) {
                        return lr(t) ? t.join(a) : n
                    }), i.$isEmpty = function(t) {
                        return !t || !t.length
                    }
                }
            }
        },
        lo = "ng-valid",
        fo = "ng-invalid",
        ho = "ng-pristine",
        $o = "ng-dirty",
        po = "ng-untouched",
        vo = "ng-touched",
        mo = "ng-pending",
        go = new r("ngModel"),
        yo = ["$scope", "$exceptionHandler", "$attrs", "$element", "$parse", "$animate", "$timeout", "$rootScope", "$q", "$interpolate", function(t, e, r, i, a, s, u, c, l, f) {
            this.$viewValue = Number.NaN, this.$modelValue = Number.NaN, this.$$rawModelValue = n, this.$validators = {}, this.$asyncValidators = {}, this.$parsers = [], this.$formatters = [], this.$viewChangeListeners = [], this.$untouched = !0, this.$touched = !1, this.$pristine = !0, this.$dirty = !1, this.$valid = !0, this.$invalid = !1, this.$error = {}, this.$$success = {}, this.$pending = n, this.$name = f(r.name || "", !1)(t);
            var h = a(r.ngModel),
                $ = h.assign,
                d = h,
                v = $,
                y = null,
                w = this;
            this.$$setOptions = function(t) {
                if (w.$options = t, t && t.getterSetter) {
                    var e = a(r.ngModel + "()"),
                        n = a(r.ngModel + "($$$p)");
                    d = function(t) {
                        var n = h(t);
                        return S(n) && (n = e(t)), n
                    }, v = function(t, e) {
                        S(h(t)) ? n(t, {
                            $$$p: w.$modelValue
                        }) : $(t, w.$modelValue)
                    }
                } else if (!h.assign) throw go("nonassign", "Expression '{0}' is non-assignable. Element: {1}", r.ngModel, z(i))
            }, this.$render = p, this.$isEmpty = function(t) {
                return m(t) || "" === t || null === t || t !== t
            };
            var x = i.inheritedData("$formController") || Mi,
                C = 0;
            Ln({
                ctrl: this,
                $element: i,
                set: function(t, e) {
                    t[e] = !0
                },
                unset: function(t, e) {
                    delete t[e]
                },
                parentForm: x,
                $animate: s
            }), this.$setPristine = function() {
                w.$dirty = !1, w.$pristine = !0, s.removeClass(i, $o), s.addClass(i, ho)
            }, this.$setDirty = function() {
                w.$dirty = !0, w.$pristine = !1, s.removeClass(i, ho), s.addClass(i, $o), x.$setDirty()
            }, this.$setUntouched = function() {
                w.$touched = !1, w.$untouched = !0, s.setClass(i, po, vo)
            }, this.$setTouched = function() {
                w.$touched = !0, w.$untouched = !1, s.setClass(i, vo, po)
            }, this.$rollbackViewValue = function() {
                u.cancel(y), w.$viewValue = w.$$lastCommittedViewValue, w.$render()
            }, this.$validate = function() {
                if (!b(w.$modelValue) || !isNaN(w.$modelValue)) {
                    var t = w.$$lastCommittedViewValue,
                        e = w.$$rawModelValue,
                        r = w.$$parserName || "parse",
                        i = !w.$error[r] && n,
                        o = w.$valid,
                        a = w.$modelValue,
                        s = w.$options && w.$options.allowInvalid;
                    w.$$runValidators(i, e, t, function(t) {
                        s || o === t || (w.$modelValue = t ? e : n, w.$modelValue !== a && w.$$writeModelToScope())
                    })
                }
            }, this.$$runValidators = function(t, e, r, i) {
                function a(t) {
                    var e = w.$$parserName || "parse";
                    if (t === n) c(e, null);
                    else if (c(e, t), !t) return o(w.$validators, function(t, e) {
                        c(e, null)
                    }), o(w.$asyncValidators, function(t, e) {
                        c(e, null)
                    }), !1;
                    return !0
                }

                function s() {
                    var t = !0;
                    return o(w.$validators, function(n, i) {
                        var o = n(e, r);
                        t = t && o, c(i, o)
                    }), !!t || (o(w.$asyncValidators, function(t, e) {
                        c(e, null)
                    }), !1)
                }

                function u() {
                    var t = [],
                        i = !0;
                    o(w.$asyncValidators, function(o, a) {
                        var s = o(e, r);
                        if (!N(s)) throw go("$asyncValidators", "Expected asynchronous validator to return a promise but got '{0}' instead.", s);
                        c(a, n), t.push(s.then(function() {
                            c(a, !0)
                        }, function(t) {
                            i = !1, c(a, !1)
                        }))
                    }), t.length ? l.all(t).then(function() {
                        f(i)
                    }, p) : f(!0)
                }

                function c(t, e) {
                    h === C && w.$setValidity(t, e)
                }

                function f(t) {
                    h === C && i(t)
                }
                C++;
                var h = C;
                return a(t) && s() ? void u() : void f(!1)
            }, this.$commitViewValue = function() {
                var t = w.$viewValue;
                u.cancel(y), (w.$$lastCommittedViewValue !== t || "" === t && w.$$hasNativeValidators) && (w.$$lastCommittedViewValue = t, w.$pristine && this.$setDirty(), this.$$parseAndValidate())
            }, this.$$parseAndValidate = function() {
                function e() {
                    w.$modelValue !== s && w.$$writeModelToScope()
                }
                var r = w.$$lastCommittedViewValue,
                    i = r,
                    o = !m(i) || n;
                if (o)
                    for (var a = 0; a < w.$parsers.length; a++)
                        if (i = w.$parsers[a](i), m(i)) {
                            o = !1;
                            break
                        }
                b(w.$modelValue) && isNaN(w.$modelValue) && (w.$modelValue = d(t));
                var s = w.$modelValue,
                    u = w.$options && w.$options.allowInvalid;
                w.$$rawModelValue = i, u && (w.$modelValue = i, e()), w.$$runValidators(o, i, w.$$lastCommittedViewValue, function(t) {
                    u || (w.$modelValue = t ? i : n, e())
                })
            }, this.$$writeModelToScope = function() {
                v(t, w.$modelValue), o(w.$viewChangeListeners, function(t) {
                    try {
                        t()
                    } catch (t) {
                        e(t)
                    }
                })
            }, this.$setViewValue = function(t, e) {
                w.$viewValue = t, w.$options && !w.$options.updateOnDefault || w.$$debounceViewValueCommit(e)
            }, this.$$debounceViewValueCommit = function(e) {
                var n, r = 0,
                    i = w.$options;
                i && g(i.debounce) && (n = i.debounce, b(n) ? r = n : b(n[e]) ? r = n[e] : b(n.default) && (r = n.default)), u.cancel(y), r ? y = u(function() {
                    w.$commitViewValue()
                }, r) : c.$$phase ? w.$commitViewValue() : t.$apply(function() {
                    w.$commitViewValue()
                })
            }, t.$watch(function() {
                var e = d(t);
                if (e !== w.$modelValue) {
                    w.$modelValue = w.$$rawModelValue = e;
                    for (var r = w.$formatters, i = r.length, o = e; i--;) o = r[i](o);
                    w.$viewValue !== o && (w.$viewValue = w.$$lastCommittedViewValue = o, w.$render(), w.$$runValidators(n, e, o, p))
                }
                return e
            })
        }],
        wo = ["$rootScope", function(t) {
            return {
                restrict: "A",
                require: ["ngModel", "^?form", "^?ngModelOptions"],
                controller: yo,
                priority: 1,
                compile: function(e) {
                    return e.addClass(ho).addClass(po).addClass(lo), {
                        pre: function(t, e, n, r) {
                            var i = r[0],
                                o = r[1] || Mi;
                            i.$$setOptions(r[2] && r[2].$options), o.$addControl(i), n.$observe("name", function(t) {
                                i.$name !== t && o.$$renameControl(i, t)
                            }), t.$on("$destroy", function() {
                                o.$removeControl(i)
                            })
                        },
                        post: function(e, n, r, i) {
                            var o = i[0];
                            o.$options && o.$options.updateOn && n.on(o.$options.updateOn, function(t) {
                                o.$$debounceViewValueCommit(t && t.type)
                            }), n.on("blur", function(n) {
                                o.$touched || (t.$$phase ? e.$evalAsync(o.$setTouched) : e.$apply(o.$setTouched))
                            })
                        }
                    }
                }
            }
        }],
        bo = /(\s+|^)default(\s+|$)/,
        xo = function() {
            return {
                restrict: "A",
                controller: ["$scope", "$attrs", function(t, e) {
                    var r = this;
                    this.$options = t.$eval(e.ngModelOptions), this.$options.updateOn !== n ? (this.$options.updateOnDefault = !1, this.$options.updateOn = fr(this.$options.updateOn.replace(bo, function() {
                        return r.$options.updateOnDefault = !0, " "
                    }))) : this.$options.updateOnDefault = !0
                }]
            }
        },
        So = kn({
            terminal: !0,
            priority: 1e3
        }),
        Co = ["$locale", "$interpolate", function(t, e) {
            var n = /{}/g,
                r = /^when(Minus)?(.+)$/;
            return {
                restrict: "EA",
                link: function(i, a, s) {
                    function u(t) {
                        a.text(t || "")
                    }
                    var c, l = s.count,
                        f = s.$attr.when && a.attr(s.$attr.when),
                        h = s.offset || 0,
                        $ = i.$eval(f) || {},
                        p = {},
                        d = e.startSymbol(),
                        v = e.endSymbol(),
                        m = d + l + "-" + h + v,
                        g = sr.noop;
                    o(s, function(t, e) {
                        var n = r.exec(e);
                        if (n) {
                            var i = (n[1] ? "-" : "") + Gn(n[2]);
                            $[i] = a.attr(s.$attr[e])
                        }
                    }), o($, function(t, r) {
                        p[r] = e(t.replace(n, m))
                    }), i.$watch(l, function(e) {
                        var n = parseFloat(e),
                            r = isNaN(n);
                        r || n in $ || (n = t.pluralCat(n - h)), n === c || r && isNaN(c) || (g(), g = i.$watch(p[n], u), c = n)
                    })
                }
            }
        }],
        Ao = ["$parse", "$animate", function(t, a) {
            var s = "$$NG_REMOVED",
                u = r("ngRepeat"),
                c = function(t, e, n, r, i, o, a) {
                    t[n] = r, i && (t[i] = o), t.$index = e, t.$first = 0 === e, t.$last = e === a - 1, t.$middle = !(t.$first || t.$last), t.$odd = !(t.$even = 0 === (1 & e))
                },
                l = function(t) {
                    return t.clone[0]
                },
                f = function(t) {
                    return t.clone[t.clone.length - 1]
                };
            return {
                restrict: "A",
                multiElement: !0,
                transclude: "element",
                priority: 1e3,
                terminal: !0,
                $$tlb: !0,
                compile: function(r, h) {
                    var $ = h.ngRepeat,
                        p = e.createComment(" end ngRepeat: " + $ + " "),
                        d = $.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
                    if (!d) throw u("iexp", "Expected expression in form of '_item_ in _collection_[ track by _id_]' but got '{0}'.", $);
                    var v = d[1],
                        m = d[2],
                        g = d[3],
                        y = d[4];
                    if (d = v.match(/^(?:(\s*[\$\w]+)|\(\s*([\$\w]+)\s*,\s*([\$\w]+)\s*\))$/), !d) throw u("iidexp", "'_item_' in '_item_ in _collection_' should be an identifier or '(_key_, _value_)' expression, but got '{0}'.", v);
                    var w = d[3] || d[1],
                        b = d[2];
                    if (g && (!/^[$a-zA-Z_][$a-zA-Z0-9_]*$/.test(g) || /^(null|undefined|this|\$index|\$first|\$middle|\$last|\$even|\$odd|\$parent|\$root|\$id)$/.test(g))) throw u("badident", "alias '{0}' is invalid --- must be a valid JS identifier which is not a reserved name.", g);
                    var x, S, C, A, k = {
                        $id: Ut
                    };
                    return y ? x = t(y) : (C = function(t, e) {
                            return Ut(e)
                        }, A = function(t) {
                            return t
                        }),
                        function(t, e, r, h, d) {
                            x && (S = function(e, n, r) {
                                return b && (k[b] = e), k[w] = n, k.$index = r, x(t, k)
                            });
                            var v = ct();
                            t.$watchCollection(m, function(r) {
                                var h, m, y, x, k, E, O, T, M, N, V, D, j = e[0],
                                    P = ct();
                                if (g && (t[g] = r), i(r)) M = r, T = S || C;
                                else {
                                    T = S || A, M = [];
                                    for (var R in r) r.hasOwnProperty(R) && "$" != R.charAt(0) && M.push(R);
                                    M.sort()
                                }
                                for (x = M.length, V = new Array(x), h = 0; h < x; h++)
                                    if (k = r === M ? h : M[h], E = r[k], O = T(k, E, h), v[O]) N = v[O], delete v[O], P[O] = N, V[h] = N;
                                    else {
                                        if (P[O]) throw o(V, function(t) {
                                            t && t.scope && (v[t.id] = t)
                                        }), u("dupes", "Duplicates in a repeater are not allowed. Use 'track by' expression to specify unique keys. Repeater: {0}, Duplicate key: {1}, Duplicate value: {2}", $, O, E);
                                        V[h] = {
                                            id: O,
                                            scope: n,
                                            clone: n
                                        }, P[O] = !0
                                    }
                                for (var _ in v) {
                                    if (N = v[_], D = ut(N.clone), a.leave(D), D[0].parentNode)
                                        for (h = 0, m = D.length; h < m; h++) D[h][s] = !0;
                                    N.scope.$destroy()
                                }
                                for (h = 0; h < x; h++)
                                    if (k = r === M ? h : M[h], E = r[k], N = V[h], N.scope) {
                                        y = j;
                                        do y = y.nextSibling; while (y && y[s]);
                                        l(N) != y && a.move(ut(N.clone), null, Qn(j)), j = f(N), c(N.scope, h, w, E, b, k, x)
                                    } else d(function(t, e) {
                                        N.scope = e;
                                        var n = p.cloneNode(!1);
                                        t[t.length++] = n, a.enter(t, null, Qn(j)), j = n, N.clone = t, P[N.id] = N, c(N.scope, h, w, E, b, k, x)
                                    });
                                v = P
                            })
                        }
                }
            }
        }],
        ko = "ng-hide",
        Eo = "ng-hide-animate",
        Oo = ["$animate", function(t) {
            return {
                restrict: "A",
                multiElement: !0,
                link: function(e, n, r) {
                    e.$watch(r.ngShow, function(e) {
                        t[e ? "removeClass" : "addClass"](n, ko, {
                            tempClasses: Eo
                        })
                    })
                }
            }
        }],
        To = ["$animate", function(t) {
            return {
                restrict: "A",
                multiElement: !0,
                link: function(e, n, r) {
                    e.$watch(r.ngHide, function(e) {
                        t[e ? "addClass" : "removeClass"](n, ko, {
                            tempClasses: Eo
                        })
                    })
                }
            }
        }],
        Mo = kn(function(t, e, n) {
            t.$watchCollection(n.ngStyle, function(t, n) {
                n && t !== n && o(n, function(t, n) {
                    e.css(n, "")
                }), t && e.css(t)
            })
        }),
        No = ["$animate", function(t) {
            return {
                restrict: "EA",
                require: "ngSwitch",
                controller: ["$scope", function() {
                    this.cases = {}
                }],
                link: function(n, r, i, a) {
                    var s = i.ngSwitch || i.on,
                        u = [],
                        c = [],
                        l = [],
                        f = [],
                        h = function(t, e) {
                            return function() {
                                t.splice(e, 1)
                            }
                        };
                    n.$watch(s, function(n) {
                        var r, i;
                        for (r = 0, i = l.length; r < i; ++r) t.cancel(l[r]);
                        for (l.length = 0, r = 0, i = f.length; r < i; ++r) {
                            var s = ut(c[r].clone);
                            f[r].$destroy();
                            var $ = l[r] = t.leave(s);
                            $.then(h(l, r))
                        }
                        c.length = 0, f.length = 0, (u = a.cases["!" + n] || a.cases["?"]) && o(u, function(n) {
                            n.transclude(function(r, i) {
                                f.push(i);
                                var o = n.element;
                                r[r.length++] = e.createComment(" end ngSwitchWhen: ");
                                var a = {
                                    clone: r
                                };
                                c.push(a), t.enter(r, o.parent(), o)
                            })
                        })
                    })
                }
            }
        }],
        Vo = kn({
            transclude: "element",
            priority: 1200,
            require: "^ngSwitch",
            multiElement: !0,
            link: function(t, e, n, r, i) {
                r.cases["!" + n.ngSwitchWhen] = r.cases["!" + n.ngSwitchWhen] || [], r.cases["!" + n.ngSwitchWhen].push({
                    transclude: i,
                    element: e
                })
            }
        }),
        Do = kn({
            transclude: "element",
            priority: 1200,
            require: "^ngSwitch",
            multiElement: !0,
            link: function(t, e, n, r, i) {
                r.cases["?"] = r.cases["?"] || [], r.cases["?"].push({
                    transclude: i,
                    element: e
                })
            }
        }),
        jo = kn({
            restrict: "EAC",
            link: function(t, e, n, i, o) {
                if (!o) throw r("ngTransclude")("orphan", "Illegal use of ngTransclude directive in the template! No parent directive that requires a transclusion found. Element: {0}", z(e));
                o(function(t) {
                    e.empty(), e.append(t)
                })
            }
        }),
        Po = ["$templateCache", function(t) {
            return {
                restrict: "E",
                terminal: !0,
                compile: function(e, n) {
                    if ("text/ng-template" == n.type) {
                        var r = n.id,
                            i = e[0].text;
                        t.put(r, i)
                    }
                }
            }
        }],
        Ro = r("ngOptions"),
        _o = v({
            restrict: "A",
            terminal: !0
        }),
        Io = ["$compile", "$parse", function(t, r) {
            var i = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,
                s = {
                    $setViewValue: p
                };
            return {
                restrict: "E",
                require: ["select", "?ngModel"],
                controller: ["$element", "$scope", "$attrs", function(t, e, n) {
                    var r, i, o = this,
                        a = {},
                        u = s;
                    o.databound = n.ngModel, o.init = function(t, e, n) {
                        u = t, r = e, i = n
                    }, o.addOption = function(e, n) {
                        at(e, '"option value"'), a[e] = !0, u.$viewValue == e && (t.val(e), i.parent() && i.remove()), n && n[0].hasAttribute("selected") && (n[0].selected = !0)
                    }, o.removeOption = function(t) {
                        this.hasOption(t) && (delete a[t], u.$viewValue === t && this.renderUnknownOption(t))
                    }, o.renderUnknownOption = function(e) {
                        var n = "? " + Ut(e) + " ?";
                        i.val(n), t.prepend(i), t.val(n), i.prop("selected", !0)
                    }, o.hasOption = function(t) {
                        return a.hasOwnProperty(t)
                    }, e.$on("$destroy", function() {
                        o.renderUnknownOption = p
                    })
                }],
                link: function(s, u, c, l) {
                    function f(t, e, n, r) {
                        n.$render = function() {
                            var t = n.$viewValue;
                            r.hasOption(t) ? (A.parent() && A.remove(), e.val(t), "" === t && p.prop("selected", !0)) : m(t) && p ? e.val("") : r.renderUnknownOption(t)
                        }, e.on("change", function() {
                            t.$apply(function() {
                                A.parent() && A.remove(), n.$setViewValue(e.val())
                            })
                        })
                    }

                    function h(t, e, n) {
                        var r;
                        n.$render = function() {
                            var t = new Ft(n.$viewValue);
                            o(e.find("option"), function(e) {
                                e.selected = g(t.get(e.value))
                            })
                        }, t.$watch(function() {
                            I(r, n.$viewValue) || (r = _(n.$viewValue), n.$render())
                        }), e.on("change", function() {
                            t.$apply(function() {
                                var t = [];
                                o(e.find("option"), function(e) {
                                    e.selected && t.push(e.value)
                                }), n.$setViewValue(t)
                            })
                        })
                    }

                    function $(e, s, u) {
                        function c(t, n, r) {
                            return I[E] = r, M && (I[M] = n), t(e, I)
                        }

                        function l() {
                            e.$apply(function() {
                                var t, n = D(e) || [];
                                if (y) t = [], o(s.val(), function(e) {
                                    e = P ? R[e] : e, t.push(f(e, n[e]))
                                });
                                else {
                                    var r = P ? R[s.val()] : s.val();
                                    t = f(r, n[r])
                                }
                                u.$setViewValue(t), m()
                            })
                        }

                        function f(t, e) {
                            if ("?" === t) return n;
                            if ("" === t) return null;
                            var r = T ? T : V;
                            return c(r, t, e)
                        }

                        function h() {
                            var t, n = D(e);
                            if (n && lr(n)) {
                                t = new Array(n.length);
                                for (var r = 0, i = n.length; r < i; r++) t[r] = c(k, r, n[r]);
                                return t
                            }
                            if (n) {
                                t = {};
                                for (var o in n) n.hasOwnProperty(o) && (t[o] = c(k, o, n[o]))
                            }
                            return t
                        }

                        function $(t) {
                            var e;
                            if (y)
                                if (P && lr(t)) {
                                    e = new Ft([]);
                                    for (var n = 0; n < t.length; n++) e.put(c(P, null, t[n]), !0)
                                } else e = new Ft(t);
                            else P && (t = c(P, null, t));
                            return function(n, r) {
                                var i;
                                return i = P ? P : T ? T : V, y ? g(e.remove(c(i, n, r))) : t === c(i, n, r)
                            }
                        }

                        function p() {
                            x || (e.$$postDigest(m), x = !0)
                        }

                        function v(t, e, n) {
                            t[e] = t[e] || 0, t[e] += n ? 1 : -1
                        }

                        function m() {
                            x = !1;
                            var t, n, r, i, l, f, h, p, m, w, A, E, O, T, V, j, q, U = {
                                    "": []
                                },
                                F = [""],
                                H = u.$viewValue,
                                L = D(e) || [],
                                B = M ? a(L) : L,
                                z = {},
                                W = $(H),
                                G = !1;
                            for (R = {}, E = 0; w = B.length, E < w; E++) h = E, M && (h = B[E], "$" === h.charAt(0)) || (p = L[h],
                                t = c(N, h, p) || "", (n = U[t]) || (n = U[t] = [], F.push(t)), O = W(h, p), G = G || O, j = c(k, h, p), j = g(j) ? j : "", q = P ? P(e, I) : M ? B[E] : E, P && (R[q] = h), n.push({
                                    id: q,
                                    label: j,
                                    selected: O
                                }));
                            for (y || (b || null === H ? U[""].unshift({
                                    id: "",
                                    label: "",
                                    selected: !G
                                }) : G || U[""].unshift({
                                    id: "?",
                                    label: "",
                                    selected: !0
                                })), A = 0, m = F.length; A < m; A++) {
                                for (t = F[A], n = U[t], _.length <= A ? (i = {
                                        element: C.clone().attr("label", t),
                                        label: n.label
                                    }, l = [i], _.push(l), s.append(i.element)) : (l = _[A], i = l[0], i.label != t && i.element.attr("label", i.label = t)), T = null, E = 0, w = n.length; E < w; E++) r = n[E], (f = l[E + 1]) ? (T = f.element, f.label !== r.label && (v(z, f.label, !1), v(z, r.label, !0), T.text(f.label = r.label), T.prop("label", f.label)), f.id !== r.id && T.val(f.id = r.id), T[0].selected !== r.selected && (T.prop("selected", f.selected = r.selected), Xn && T.prop("selected", f.selected))) : ("" === r.id && b ? V = b : (V = S.clone()).val(r.id).prop("selected", r.selected).attr("selected", r.selected).prop("label", r.label).text(r.label), l.push(f = {
                                    element: V,
                                    label: r.label,
                                    id: r.id,
                                    selected: r.selected
                                }), v(z, r.label, !0), T ? T.after(V) : i.element.append(V), T = V);
                                for (E++; l.length > E;) r = l.pop(), v(z, r.label, !1), r.element.remove()
                            }
                            for (; _.length > A;) {
                                for (n = _.pop(), E = 1; E < n.length; ++E) v(z, n[E].label, !1);
                                n[0].element.remove()
                            }
                            o(z, function(t, e) {
                                t > 0 ? d.addOption(e) : t < 0 && d.removeOption(e)
                            })
                        }
                        var A;
                        if (!(A = w.match(i))) throw Ro("iexp", "Expected expression in form of '_select_ (as _label_)? for (_key_,)?_value_ in _collection_' but got '{0}'. Element: {1}", w, z(s));
                        var k = r(A[2] || A[1]),
                            E = A[4] || A[6],
                            O = / as /.test(A[0]) && A[1],
                            T = O ? r(O) : null,
                            M = A[5],
                            N = r(A[3] || ""),
                            V = r(A[2] ? A[1] : E),
                            D = r(A[7]),
                            j = A[8],
                            P = j ? r(A[8]) : null,
                            R = {},
                            _ = [
                                [{
                                    element: s,
                                    label: ""
                                }]
                            ],
                            I = {};
                        b && (t(b)(e), b.removeClass("ng-scope"), b.remove()), s.empty(), s.on("change", l), u.$render = m, e.$watchCollection(D, p), e.$watchCollection(h, p), y && e.$watchCollection(function() {
                            return u.$modelValue
                        }, p)
                    }
                    if (l[1]) {
                        for (var p, d = l[0], v = l[1], y = c.multiple, w = c.ngOptions, b = !1, x = !1, S = Qn(e.createElement("option")), C = Qn(e.createElement("optgroup")), A = S.clone(), k = 0, E = u.children(), O = E.length; k < O; k++)
                            if ("" === E[k].value) {
                                p = b = E.eq(k);
                                break
                            }
                        d.init(v, b, A), y && (v.$isEmpty = function(t) {
                            return !t || 0 === t.length
                        }), w ? $(s, u, v) : y ? h(s, u, v) : f(s, u, v, d)
                    }
                }
            }
        }],
        qo = ["$interpolate", function(t) {
            var e = {
                addOption: p,
                removeOption: p
            };
            return {
                restrict: "E",
                priority: 100,
                compile: function(n, r) {
                    if (m(r.value)) {
                        var i = t(n.text(), !0);
                        i || r.$set("value", n.text())
                    }
                    return function(t, n, r) {
                        var o = "$selectController",
                            a = n.parent(),
                            s = a.data(o) || a.parent().data(o);
                        s && s.databound || (s = e), i ? t.$watch(i, function(t, e) {
                            r.$set("value", t), e !== t && s.removeOption(e), s.addOption(t, n)
                        }) : s.addOption(r.value, n), n.on("$destroy", function() {
                            s.removeOption(r.value)
                        })
                    }
                }
            }
        }],
        Uo = v({
            restrict: "E",
            terminal: !1
        }),
        Fo = function() {
            return {
                restrict: "A",
                require: "?ngModel",
                link: function(t, e, n, r) {
                    r && (n.required = !0, r.$validators.required = function(t, e) {
                        return !n.required || !r.$isEmpty(e)
                    }, n.$observe("required", function() {
                        r.$validate()
                    }))
                }
            }
        },
        Ho = function() {
            return {
                restrict: "A",
                require: "?ngModel",
                link: function(t, e, i, o) {
                    if (o) {
                        var a, s = i.ngPattern || i.pattern;
                        i.$observe("pattern", function(t) {
                            if (w(t) && t.length > 0 && (t = new RegExp("^" + t + "$")), t && !t.test) throw r("ngPattern")("noregexp", "Expected {0} to be a RegExp but was {1}. Element: {2}", s, t, z(e));
                            a = t || n, o.$validate()
                        }), o.$validators.pattern = function(t) {
                            return o.$isEmpty(t) || m(a) || a.test(t)
                        }
                    }
                }
            }
        },
        Lo = function() {
            return {
                restrict: "A",
                require: "?ngModel",
                link: function(t, e, n, r) {
                    if (r) {
                        var i = -1;
                        n.$observe("maxlength", function(t) {
                            var e = h(t);
                            i = isNaN(e) ? -1 : e, r.$validate()
                        }), r.$validators.maxlength = function(t, e) {
                            return i < 0 || r.$isEmpty(e) || e.length <= i
                        }
                    }
                }
            }
        },
        Bo = function() {
            return {
                restrict: "A",
                require: "?ngModel",
                link: function(t, e, n, r) {
                    if (r) {
                        var i = 0;
                        n.$observe("minlength", function(t) {
                            i = h(t) || 0, r.$validate()
                        }), r.$validators.minlength = function(t, e) {
                            return r.$isEmpty(e) || e.length >= i
                        }
                    }
                }
            }
        };
    return t.angular.bootstrap ? void console.log("WARNING: Tried to load angular more than once.") : (rt(), $t(sr), void Qn(e).ready(function() {
        X(e, Q)
    }))
}(window, document), !window.angular.$$csp() && window.angular.element(document).find("head").prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\\:form{display:block;}</style>');
! function(r, e, t) {
    "use strict";

    function n(r) {
        return null != r && "" !== r && "hasOwnProperty" !== r && s.test("." + r)
    }

    function a(r, e) {
        if (!n(e)) throw i("badmember", 'Dotted member path "@{0}" is invalid.', e);
        for (var a = e.split("."), o = 0, s = a.length; o < s && r !== t; o++) {
            var c = a[o];
            r = null !== r ? r[c] : t
        }
        return r
    }

    function o(r, t) {
        t = t || {}, e.forEach(t, function(r, e) {
            delete t[e]
        });
        for (var n in r) !r.hasOwnProperty(n) || "$" === n.charAt(0) && "$" === n.charAt(1) || (t[n] = r[n]);
        return t
    }
    var i = e.$$minErr("$resource"),
        s = /^(\.[a-zA-Z_$][0-9a-zA-Z_$]*)+$/;
    e.module("ngResource", ["ng"]).factory("$resource", ["$http", "$q", function(r, n) {
        function s(r) {
            return c(r, !0).replace(/%26/gi, "&").replace(/%3D/gi, "=").replace(/%2B/gi, "+")
        }

        function c(r, e) {
            return encodeURIComponent(r).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, e ? "%20" : "+")
        }

        function u(r, e) {
            this.template = r, this.defaults = e || {}, this.urlParams = {}
        }

        function p(s, c, $) {
            function v(r, e) {
                var t = {};
                return e = m({}, c, e), h(e, function(e, n) {
                    g(e) && (e = e()), t[n] = e && e.charAt && "@" == e.charAt(0) ? a(r, e.substr(1)) : e
                }), t
            }

            function w(r) {
                return r.resource
            }

            function y(r) {
                o(r || {}, this)
            }
            var E = new u(s);
            return $ = m({}, l, $), h($, function(a, s) {
                var c = /^(POST|PUT|PATCH)$/i.test(a.method);
                y[s] = function(s, u, p, l) {
                    var $, A, b, P = {};
                    switch (arguments.length) {
                        case 4:
                            b = l, A = p;
                        case 3:
                        case 2:
                            if (!g(u)) {
                                P = s, $ = u, A = p;
                                break
                            }
                            if (g(s)) {
                                A = s, b = u;
                                break
                            }
                            A = u, b = p;
                        case 1:
                            g(s) ? A = s : c ? $ = s : P = s;
                            break;
                        case 0:
                            break;
                        default:
                            throw i("badargs", "Expected up to 4 arguments [params, data, success, error], got {0} arguments", arguments.length)
                    }
                    var T = this instanceof y,
                        x = T ? $ : a.isArray ? [] : new y($),
                        O = {},
                        R = a.interceptor && a.interceptor.response || w,
                        D = a.interceptor && a.interceptor.responseError || t;
                    h(a, function(r, e) {
                        "params" != e && "isArray" != e && "interceptor" != e && (O[e] = d(r))
                    }), c && (O.data = $), E.setUrlParams(O, m({}, v($, a.params || {}), P), a.url);
                    var j = r(O).then(function(r) {
                        var t = r.data,
                            n = x.$promise;
                        if (t) {
                            if (e.isArray(t) !== !!a.isArray) throw i("badcfg", "Error in resource configuration. Expected response to contain an {0} but got an {1}", a.isArray ? "array" : "object", e.isArray(t) ? "array" : "object");
                            a.isArray ? (x.length = 0, h(t, function(r) {
                                "object" == typeof r ? x.push(new y(r)) : x.push(r)
                            })) : (o(t, x), x.$promise = n)
                        }
                        return x.$resolved = !0, r.resource = x, r
                    }, function(r) {
                        return x.$resolved = !0, (b || f)(r), n.reject(r)
                    });
                    return j = j.then(function(r) {
                        var e = R(r);
                        return (A || f)(e, r.headers), e
                    }, D), T ? j : (x.$promise = j, x.$resolved = !1, x)
                }, y.prototype["$" + s] = function(r, e, t) {
                    g(r) && (t = e, e = r, r = {});
                    var n = y[s].call(this, r, this, e, t);
                    return n.$promise || n
                }
            }), y.bind = function(r) {
                return p(s, m({}, c, r), $)
            }, y
        }
        var l = {
                get: {
                    method: "GET"
                },
                save: {
                    method: "POST"
                },
                query: {
                    method: "GET",
                    isArray: !0
                },
                remove: {
                    method: "DELETE"
                },
                delete: {
                    method: "DELETE"
                }
            },
            f = e.noop,
            h = e.forEach,
            m = e.extend,
            d = e.copy,
            g = e.isFunction;
        return u.prototype = {
            setUrlParams: function(r, t, n) {
                var a, o, c = this,
                    u = n || c.template,
                    p = c.urlParams = {};
                h(u.split(/\W/), function(r) {
                    if ("hasOwnProperty" === r) throw i("badname", "hasOwnProperty is not a valid parameter name.");
                    !new RegExp("^\\d+$").test(r) && r && new RegExp("(^|[^\\\\]):" + r + "(\\W|$)").test(u) && (p[r] = !0)
                }), u = u.replace(/\\:/g, ":"), t = t || {}, h(c.urlParams, function(r, n) {
                    a = t.hasOwnProperty(n) ? t[n] : c.defaults[n], e.isDefined(a) && null !== a ? (o = s(a), u = u.replace(new RegExp(":" + n + "(\\W|$)", "g"), function(r, e) {
                        return o + e
                    })) : u = u.replace(new RegExp("(/?):" + n + "(\\W|$)", "g"), function(r, e, t) {
                        return "/" == t.charAt(0) ? t : e + t
                    })
                }), u = u.replace(/\/+$/, "") || "/", u = u.replace(/\/\.(?=\w+($|\?))/, "."), r.url = u.replace(/\/\\\./, "/."), h(t, function(e, t) {
                    c.urlParams[t] || (r.params = r.params || {}, r.params[t] = e)
                })
            }
        }, p
    }])
}(window, window.angular);
! function(n, t, e) {
    "use strict";
    t.module("ngAnimate", ["ng"]).directive("ngAnimateChildren", function() {
        var n = "$$ngAnimateChildren";
        return function(e, a, i) {
            var r = i.ngAnimateChildren;
            t.isString(r) && 0 === r.length ? a.data(n, !0) : e.$watch(r, function(t) {
                a.data(n, !!t)
            })
        }
    }).factory("$$animateReflow", ["$$rAF", "$document", function(n, t) {
        var e = t[0].body;
        return function(t) {
            return n(function() {
                e.offsetWidth + 1;
                t()
            })
        }
    }]).config(["$provide", "$animateProvider", function(a, i) {
        function r(n) {
            for (var t = 0; t < n.length; t++) {
                var e = n[t];
                if (e.nodeType == g) return e
            }
        }

        function s(n) {
            return n && t.element(n)
        }

        function o(n) {
            return t.element(r(n))
        }

        function u(n, t) {
            return r(n) == r(t)
        }
        var l, c = t.noop,
            f = t.forEach,
            v = i.$$selectors,
            d = t.isArray,
            m = t.isString,
            p = t.isObject,
            g = 1,
            C = "$$ngAnimateState",
            h = "$$ngAnimateChildren",
            $ = "ng-animate",
            b = {
                running: !0
            };
        a.decorator("$animate", ["$delegate", "$$q", "$injector", "$sniffer", "$rootElement", "$$asyncCallback", "$rootScope", "$document", "$templateRequest", "$$jqLite", function(n, e, a, g, y, D, A, w, k, x) {
            function S(n, t) {
                var e = n.data(C) || {};
                return t && (e.running = !0, e.structural = !0, n.data(C, e)), e.disabled || e.running && e.structural
            }

            function B(n) {
                var t, a = e.defer();
                return a.promise.$$cancelFn = function() {
                    t && t()
                }, A.$$postDigest(function() {
                    t = n(function() {
                        a.resolve()
                    })
                }), a.promise
            }

            function F(n) {
                if (p(n)) return n.tempClasses && m(n.tempClasses) && (n.tempClasses = n.tempClasses.split(/\s+/)), n
            }

            function M(n, t, e) {
                e = e || {};
                var a = {};
                f(e, function(n, t) {
                    f(t.split(" "), function(t) {
                        a[t] = n
                    })
                });
                var i = Object.create(null);
                f((n.attr("class") || "").split(/\s+/), function(n) {
                    i[n] = !0
                });
                var r = [],
                    s = [];
                return f(t && t.classes || [], function(n, t) {
                    var e = i[t],
                        o = a[t] || {};
                    n === !1 ? (e || "addClass" == o.event) && s.push(t) : n === !0 && (e && "removeClass" != o.event || r.push(t))
                }), r.length + s.length > 0 && [r.join(" "), s.join(" ")]
            }

            function E(n) {
                if (n) {
                    var t = [],
                        e = {},
                        i = n.substr(1).split(".");
                    (g.transitions || g.animations) && t.push(a.get(v[""]));
                    for (var r = 0; r < i.length; r++) {
                        var s = i[r],
                            o = v[s];
                        o && !e[s] && (t.push(a.get(o)), e[s] = !0)
                    }
                    return t
                }
            }

            function R(n, e, a, i) {
                function r(n, t) {
                    var e = n[t],
                        a = n["before" + t.charAt(0).toUpperCase() + t.substr(1)];
                    if (e || a) return "leave" == t && (a = e, e = null), D.push({
                        event: t,
                        fn: e
                    }), $.push({
                        event: t,
                        fn: a
                    }), !0
                }

                function s(t, e, r) {
                    function s(n) {
                        if (e) {
                            if ((e[n] || c)(), ++v < o.length) return;
                            e = null
                        }
                        r()
                    }
                    var o = [];
                    f(t, function(n) {
                        n.fn && o.push(n)
                    });
                    var v = 0;
                    f(o, function(t, r) {
                        var o = function() {
                            s(r)
                        };
                        switch (t.event) {
                            case "setClass":
                                e.push(t.fn(n, u, l, o, i));
                                break;
                            case "animate":
                                e.push(t.fn(n, a, i.from, i.to, o));
                                break;
                            case "addClass":
                                e.push(t.fn(n, u || a, o, i));
                                break;
                            case "removeClass":
                                e.push(t.fn(n, l || a, o, i));
                                break;
                            default:
                                e.push(t.fn(n, o, i))
                        }
                    }), e && 0 === e.length && r()
                }
                var o = n[0];
                if (o) {
                    i && (i.to = i.to || {}, i.from = i.from || {});
                    var u, l;
                    d(a) && (u = a[0], l = a[1], u ? l ? a = u + " " + l : (a = u, e = "addClass") : (a = l, e = "removeClass"));
                    var v = "setClass" == e,
                        m = v || "addClass" == e || "removeClass" == e || "animate" == e,
                        p = n.attr("class"),
                        g = p + " " + a;
                    if (K(g)) {
                        var C = c,
                            h = [],
                            $ = [],
                            b = c,
                            y = [],
                            D = [],
                            A = (" " + g).replace(/\s+/g, ".");
                        return f(E(A), function(n) {
                            var t = r(n, e);
                            !t && v && (r(n, "addClass"), r(n, "removeClass"))
                        }), {
                            node: o,
                            event: e,
                            className: a,
                            isClassBased: m,
                            isSetClassOperation: v,
                            applyStyles: function() {
                                i && n.css(t.extend(i.from || {}, i.to || {}))
                            },
                            before: function(n) {
                                C = n, s($, h, function() {
                                    C = c, n()
                                })
                            },
                            after: function(n) {
                                b = n, s(D, y, function() {
                                    b = c, n()
                                })
                            },
                            cancel: function() {
                                h && (f(h, function(n) {
                                    (n || c)(!0)
                                }), C(!0)), y && (f(y, function(n) {
                                    (n || c)(!0)
                                }), b(!0))
                            }
                        }
                    }
                }
            }

            function N(n, e, a, i, r, s, o, u) {
                function v(t) {
                    var i = "$animate:" + t;
                    A && A[i] && A[i].length > 0 && D(function() {
                        a.triggerHandler(i, {
                            event: n,
                            className: e
                        })
                    })
                }

                function d() {
                    v("before")
                }

                function m() {
                    v("after")
                }

                function p() {
                    v("close"), u()
                }

                function g() {
                    g.hasBeenRun || (g.hasBeenRun = !0, s())
                }

                function h() {
                    if (!h.hasBeenRun) {
                        y && y.applyStyles(), h.hasBeenRun = !0, o && o.tempClasses && f(o.tempClasses, function(n) {
                            l.removeClass(a, n)
                        });
                        var t = a.data(C);
                        t && (y && y.isClassBased ? T(a, e) : (D(function() {
                            var t = a.data(C) || {};
                            N == t.index && T(a, e, n)
                        }), a.data(C, t))), p()
                    }
                }
                var b = c,
                    y = R(a, n, e, o);
                if (!y) return g(), d(), m(), h(), b;
                n = y.event, e = y.className;
                var A = t.element._data(y.node);
                if (A = A && A.events, i || (i = r ? r.parent() : a.parent()), j(a, i)) return g(), d(), m(), h(), b;
                var w = a.data(C) || {},
                    k = w.active || {},
                    x = w.totalActive || 0,
                    S = w.last,
                    B = !1;
                if (x > 0) {
                    var F = [];
                    if (y.isClassBased) {
                        if ("setClass" == S.event) F.push(S), T(a, e);
                        else if (k[e]) {
                            var M = k[e];
                            M.event == n ? B = !0 : (F.push(M), T(a, e))
                        }
                    } else if ("leave" == n && k["ng-leave"]) B = !0;
                    else {
                        for (var E in k) F.push(k[E]);
                        w = {}, T(a, !0)
                    }
                    F.length > 0 && f(F, function(n) {
                        n.cancel()
                    })
                }
                if (!y.isClassBased || y.isSetClassOperation || "animate" == n || B || (B = "addClass" == n == a.hasClass(e)), B) return g(), d(), m(), p(), b;
                k = w.active || {}, x = w.totalActive || 0, "leave" == n && a.one("$destroy", function(n) {
                    var e = t.element(this),
                        a = e.data(C);
                    if (a) {
                        var i = a.active["ng-leave"];
                        i && (i.cancel(), T(e, "ng-leave"))
                    }
                }), l.addClass(a, $), o && o.tempClasses && f(o.tempClasses, function(n) {
                    l.addClass(a, n)
                });
                var N = P++;
                return x++, k[e] = y, a.data(C, {
                    last: y,
                    active: k,
                    index: N,
                    totalActive: x
                }), d(), y.before(function(t) {
                    var i = a.data(C);
                    t = t || !i || !i.active[e] || y.isClassBased && i.active[e].event != n, g(), t === !0 ? h() : (m(), y.after(h))
                }), y.cancel
            }

            function O(n) {
                var e = r(n);
                if (e) {
                    var a = t.isFunction(e.getElementsByClassName) ? e.getElementsByClassName($) : e.querySelectorAll("." + $);
                    f(a, function(n) {
                        n = t.element(n);
                        var e = n.data(C);
                        e && e.active && f(e.active, function(n) {
                            n.cancel()
                        })
                    })
                }
            }

            function T(n, t) {
                if (u(n, y)) b.disabled || (b.running = !1, b.structural = !1);
                else if (t) {
                    var e = n.data(C) || {},
                        a = t === !0;
                    !a && e.active && e.active[t] && (e.totalActive--, delete e.active[t]), !a && e.totalActive || (l.removeClass(n, $), n.removeData(C))
                }
            }

            function j(n, e) {
                if (b.disabled) return !0;
                if (u(n, y)) return b.running;
                var a, i, r;
                do {
                    if (0 === e.length) break;
                    var s = u(e, y),
                        o = s ? b : e.data(C) || {};
                    if (o.disabled) return !0;
                    if (s && (r = !0), a !== !1) {
                        var l = e.data(h);
                        t.isDefined(l) && (a = l)
                    }
                    i = i || o.running || o.last && !o.last.isClassBased
                } while (e = e.parent());
                return !r || !a && i
            }
            l = x, y.data(C, b);
            var I = A.$watch(function() {
                    return k.totalPendingRequests
                }, function(n, t) {
                    0 === n && (I(), A.$$postDigest(function() {
                        A.$$postDigest(function() {
                            b.running = !1
                        })
                    }))
                }),
                P = 0,
                q = i.classNameFilter(),
                K = q ? function(n) {
                    return q.test(n)
                } : function() {
                    return !0
                };
            return {
                animate: function(n, t, e, a, i) {
                    return a = a || "ng-inline-animate", i = F(i) || {}, i.from = e ? t : null, i.to = e ? e : t, B(function(t) {
                        return N("animate", a, o(n), null, null, c, i, t)
                    })
                },
                enter: function(e, a, i, r) {
                    return r = F(r), e = t.element(e), a = s(a), i = s(i), S(e, !0), n.enter(e, a, i), B(function(n) {
                        return N("enter", "ng-enter", o(e), a, i, c, r, n)
                    })
                },
                leave: function(e, a) {
                    return a = F(a), e = t.element(e), O(e), S(e, !0), B(function(t) {
                        return N("leave", "ng-leave", o(e), null, null, function() {
                            n.leave(e)
                        }, a, t)
                    })
                },
                move: function(e, a, i, r) {
                    return r = F(r), e = t.element(e), a = s(a), i = s(i), O(e), S(e, !0), n.move(e, a, i), B(function(n) {
                        return N("move", "ng-move", o(e), a, i, c, r, n)
                    })
                },
                addClass: function(n, t, e) {
                    return this.setClass(n, t, [], e)
                },
                removeClass: function(n, t, e) {
                    return this.setClass(n, [], t, e)
                },
                setClass: function(e, a, i, s) {
                    s = F(s);
                    var u = "$$animateClasses";
                    if (e = t.element(e), e = o(e), S(e)) return n.$$setClassImmediately(e, a, i, s);
                    var l, c = e.data(u),
                        v = !!c;
                    return c || (c = {}, c.classes = {}), l = c.classes, a = d(a) ? a : a.split(" "), f(a, function(n) {
                        n && n.length && (l[n] = !0)
                    }), i = d(i) ? i : i.split(" "), f(i, function(n) {
                        n && n.length && (l[n] = !1)
                    }), v ? (s && c.options && (c.options = t.extend(c.options || {}, s)), c.promise) : (e.data(u, c = {
                        classes: l,
                        options: s
                    }), c.promise = B(function(t) {
                        var a = e.parent(),
                            i = r(e),
                            s = i.parentNode;
                        if (!s || s.$$NG_REMOVED || i.$$NG_REMOVED) return void t();
                        var o = e.data(u);
                        e.removeData(u);
                        var l = e.data(C) || {},
                            c = M(e, o, l.active);
                        return c ? N("setClass", c, e, a, null, function() {
                            c[0] && n.$$addClassImmediately(e, c[0]), c[1] && n.$$removeClassImmediately(e, c[1])
                        }, o.options, t) : t()
                    }))
                },
                cancel: function(n) {
                    n.$$cancelFn()
                },
                enabled: function(n, t) {
                    switch (arguments.length) {
                        case 2:
                            if (n) T(t);
                            else {
                                var e = t.data(C) || {};
                                e.disabled = !0, t.data(C, e)
                            }
                            break;
                        case 1:
                            b.disabled = !n;
                            break;
                        default:
                            n = !b.disabled
                    }
                    return !!n
                }
            }
        }]), i.register("", ["$window", "$sniffer", "$timeout", "$$animateReflow", function(a, i, s, o) {
            function u() {
                T || (T = o(function() {
                    z = [], T = null, L = {}
                }))
            }

            function v(n, t) {
                T && T(), z.push(t), T = o(function() {
                    f(z, function(n) {
                        n()
                    }), z = [], T = null, L = {}
                })
            }

            function p(n, e) {
                var a = r(n);
                n = t.element(a), X.push(n);
                var i = Date.now() + e;
                i <= Q || (s.cancel(J), Q = i, J = s(function() {
                    C(X), X = []
                }, e, !1))
            }

            function C(n) {
                f(n, function(n) {
                    var t = n.data(_);
                    t && f(t.closeAnimationFns, function(n) {
                        n()
                    })
                })
            }

            function h(n, t) {
                var e = t ? L[t] : null;
                if (!e) {
                    var i = 0,
                        r = 0,
                        s = 0,
                        o = 0;
                    f(n, function(n) {
                        if (n.nodeType == g) {
                            var t = a.getComputedStyle(n) || {},
                                e = t[M + j];
                            i = Math.max($(e), i);
                            var u = t[M + P];
                            r = Math.max($(u), r);
                            t[R + P];
                            o = Math.max($(t[R + P]), o);
                            var l = $(t[R + j]);
                            l > 0 && (l *= parseInt(t[R + q], 10) || 1), s = Math.max(l, s)
                        }
                    }), e = {
                        total: 0,
                        transitionDelay: r,
                        transitionDuration: i,
                        animationDelay: o,
                        animationDuration: s
                    }, t && (L[t] = e)
                }
                return e
            }

            function $(n) {
                var t = 0,
                    e = m(n) ? n.split(/\s*,\s*/) : [];
                return f(e, function(n) {
                    t = Math.max(parseFloat(n) || 0, t)
                }), t
            }

            function b(n) {
                var t = n.parent(),
                    e = t.data(W);
                return e || (t.data(W, ++U), e = U), e + "-" + r(n).getAttribute("class")
            }

            function y(n, t, e, a) {
                var i = ["ng-enter", "ng-leave", "ng-move"].indexOf(e) >= 0,
                    s = b(t),
                    o = s + " " + e,
                    u = L[o] ? ++L[o].total : 0,
                    c = {};
                if (u > 0) {
                    var f = e + "-stagger",
                        v = s + " " + f,
                        d = !L[v];
                    d && l.addClass(t, f), c = h(t, v), d && l.removeClass(t, f)
                }
                l.addClass(t, e);
                var m = t.data(_) || {},
                    p = h(t, o),
                    g = p.transitionDuration,
                    C = p.animationDuration;
                if (i && 0 === g && 0 === C) return l.removeClass(t, e), !1;
                var $ = a || i && g > 0,
                    y = C > 0 && c.animationDelay > 0 && 0 === c.animationDuration,
                    D = m.closeAnimationFns || [];
                t.data(_, {
                    stagger: c,
                    cacheKey: o,
                    running: m.running || 0,
                    itemIndex: u,
                    blockTransition: $,
                    closeAnimationFns: D
                });
                var k = r(t);
                return $ && (A(k, !0), a && t.css(a)), y && w(k, !0), !0
            }

            function D(n, t, e, a, i) {
                function o() {
                    t.off(P, u), l.removeClass(t, d), l.removeClass(t, m), j && s.cancel(j), B(t, e);
                    var n = r(t);
                    for (var a in C) n.style.removeProperty(C[a])
                }

                function u(n) {
                    n.stopPropagation();
                    var t = n.originalEvent || n,
                        e = t.$manualTimeStamp || t.timeStamp || Date.now(),
                        i = parseFloat(t.elapsedTime.toFixed(G));
                    Math.max(e - I, 0) >= R && i >= F && a()
                }
                var c = r(t),
                    v = t.data(_);
                if (c.getAttribute("class").indexOf(e) == -1 || !v) return void a();
                var d = "",
                    m = "";
                f(e.split(" "), function(n, t) {
                    var e = (t > 0 ? " " : "") + n;
                    d += e + "-active", m += e + "-pending"
                });
                var g = "",
                    C = [],
                    $ = v.itemIndex,
                    b = v.stagger,
                    y = 0;
                if ($ > 0) {
                    var D = 0;
                    b.transitionDelay > 0 && 0 === b.transitionDuration && (D = b.transitionDelay * $);
                    var k = 0;
                    b.animationDelay > 0 && 0 === b.animationDuration && (k = b.animationDelay * $, C.push(O + "animation-play-state")), y = Math.round(100 * Math.max(D, k)) / 100
                }
                y || (l.addClass(t, d), v.blockTransition && A(c, !1));
                var x = v.cacheKey + " " + d,
                    S = h(t, x),
                    F = Math.max(S.transitionDuration, S.animationDuration);
                if (0 === F) return l.removeClass(t, d), B(t, e), void a();
                !y && i && Object.keys(i).length > 0 && (S.transitionDuration || (t.css("transition", S.animationDuration + "s linear all"), C.push("transition")), t.css(i));
                var M = Math.max(S.transitionDelay, S.animationDelay),
                    R = M * H;
                if (C.length > 0) {
                    var T = c.getAttribute("style") || "";
                    ";" !== T.charAt(T.length - 1) && (T += ";"), c.setAttribute("style", T + " " + g)
                }
                var j, I = Date.now(),
                    P = N + " " + E,
                    q = (M + F) * V,
                    K = (y + q) * H;
                return y > 0 && (l.addClass(t, m), j = s(function() {
                    j = null, S.transitionDuration > 0 && A(c, !1), S.animationDuration > 0 && w(c, !1), l.addClass(t, d), l.removeClass(t, m), i && (0 === S.transitionDuration && t.css("transition", S.animationDuration + "s linear all"), t.css(i), C.push("transition"))
                }, y * H, !1)), t.on(P, u), v.closeAnimationFns.push(function() {
                    o(), a()
                }), v.running++, p(t, K), o
            }

            function A(n, t) {
                n.style[M + I] = t ? "none" : ""
            }

            function w(n, t) {
                n.style[R + K] = t ? "paused" : ""
            }

            function k(n, t, e, a) {
                if (y(n, t, e, a)) return function(n) {
                    n && B(t, e)
                }
            }

            function x(n, t, e, a, i) {
                return t.data(_) ? D(n, t, e, a, i) : (B(t, e), void a())
            }

            function S(n, t, e, a, i) {
                var r = k(n, t, e, i.from);
                if (!r) return u(), void a();
                var s = r;
                return v(t, function() {
                        s = x(n, t, e, a, i.to)
                    }),
                    function(n) {
                        (s || c)(n)
                    }
            }

            function B(n, t) {
                l.removeClass(n, t);
                var e = n.data(_);
                e && (e.running && e.running--, e.running && 0 !== e.running || n.removeData(_))
            }

            function F(n, t) {
                var e = "";
                return n = d(n) ? n : n.split(/\s+/), f(n, function(n, a) {
                    n && n.length > 0 && (e += (a > 0 ? " " : "") + n + t)
                }), e
            }
            var M, E, R, N, O = "";
            n.ontransitionend === e && n.onwebkittransitionend !== e ? (O = "-webkit-", M = "WebkitTransition", E = "webkitTransitionEnd transitionend") : (M = "transition", E = "transitionend"), n.onanimationend === e && n.onwebkitanimationend !== e ? (O = "-webkit-", R = "WebkitAnimation", N = "webkitAnimationEnd animationend") : (R = "animation", N = "animationend");
            var T, j = "Duration",
                I = "Property",
                P = "Delay",
                q = "IterationCount",
                K = "PlayState",
                W = "$$ngAnimateKey",
                _ = "$$ngAnimateCSS3Data",
                G = 3,
                V = 1.5,
                H = 1e3,
                L = {},
                U = 0,
                z = [],
                J = null,
                Q = 0,
                X = [];
            return {
                animate: function(n, t, e, a, i, r) {
                    return r = r || {}, r.from = e, r.to = a, S("animate", n, t, i, r)
                },
                enter: function(n, t, e) {
                    return e = e || {}, S("enter", n, "ng-enter", t, e)
                },
                leave: function(n, t, e) {
                    return e = e || {}, S("leave", n, "ng-leave", t, e)
                },
                move: function(n, t, e) {
                    return e = e || {}, S("move", n, "ng-move", t, e)
                },
                beforeSetClass: function(n, t, e, a, i) {
                    i = i || {};
                    var r = F(e, "-remove") + " " + F(t, "-add"),
                        s = k("setClass", n, r, i.from);
                    return s ? (v(n, a), s) : (u(), void a())
                },
                beforeAddClass: function(n, t, e, a) {
                    a = a || {};
                    var i = k("addClass", n, F(t, "-add"), a.from);
                    return i ? (v(n, e), i) : (u(), void e())
                },
                beforeRemoveClass: function(n, t, e, a) {
                    a = a || {};
                    var i = k("removeClass", n, F(t, "-remove"), a.from);
                    return i ? (v(n, e), i) : (u(), void e())
                },
                setClass: function(n, t, e, a, i) {
                    i = i || {}, e = F(e, "-remove"), t = F(t, "-add");
                    var r = e + " " + t;
                    return x("setClass", n, r, a, i.to)
                },
                addClass: function(n, t, e, a) {
                    return a = a || {}, x("addClass", n, F(t, "-add"), e, a.to)
                },
                removeClass: function(n, t, e, a) {
                    return a = a || {}, x("removeClass", n, F(t, "-remove"), e, a.to)
                }
            }
        }])
    }])
}(window, window.angular);
! function(e, t, r) {
    "use strict";

    function n() {
        this.$get = ["$$sanitizeUri", function(e) {
            return function(t) {
                var r = [];
                return s(t, c(r, function(t, r) {
                    return !/^unsafe/.test(e(t, r))
                })), r.join("")
            }
        }]
    }

    function a(e) {
        var r = [],
            n = c(r, t.noop);
        return n.chars(e), r.join("")
    }

    function i(e) {
        var t, r = {},
            n = e.split(",");
        for (t = 0; t < n.length; t++) r[n[t]] = !0;
        return r
    }

    function s(e, r) {
        function n(e, n, i, s) {
            if (n = t.lowercase(n), C[n])
                for (; k.last() && A[k.last()];) a("", k.last());
            $[n] && k.last() == n && a("", n), s = v[n] || !!s, s || k.push(n);
            var l = {};
            i.replace(f, function(e, t, r, n, a) {
                var i = r || n || a || "";
                l[t] = o(i)
            }), r.start && r.start(n, l, s)
        }

        function a(e, n) {
            var a, i = 0;
            if (n = t.lowercase(n))
                for (i = k.length - 1; i >= 0 && k[i] != n; i--);
            if (i >= 0) {
                for (a = k.length - 1; a >= i; a--) r.end && r.end(k[a]);
                k.length = i
            }
        }
        "string" != typeof e && (e = null === e || "undefined" == typeof e ? "" : "" + e);
        var i, s, l, c, k = [],
            y = e;
        for (k.last = function() {
                return k[k.length - 1]
            }; e;) {
            if (c = "", s = !0, k.last() && T[k.last()] ? (e = e.replace(new RegExp("(.*)<\\s*\\/\\s*" + k.last() + "[^>]*>", "i"), function(e, t) {
                    return t = t.replace(m, "$1").replace(x, "$1"), r.chars && r.chars(o(t)), ""
                }), a("", k.last())) : (0 === e.indexOf("<!--") ? (i = e.indexOf("--", 4), i >= 0 && e.lastIndexOf("-->", i) === i && (r.comment && r.comment(e.substring(4, i)), e = e.substring(i + 3), s = !1)) : b.test(e) ? (l = e.match(b), l && (e = e.replace(l[0], ""), s = !1)) : g.test(e) ? (l = e.match(p), l && (e = e.substring(l[0].length), l[0].replace(p, a), s = !1)) : d.test(e) && (l = e.match(h), l ? (l[4] && (e = e.substring(l[0].length), l[0].replace(h, n)), s = !1) : (c += "<", e = e.substring(1))), s && (i = e.indexOf("<"), c += i < 0 ? e : e.substring(0, i), e = i < 0 ? "" : e.substring(i), r.chars && r.chars(o(c)))), e == y) throw u("badparse", "The sanitizer was unable to parse the following block of html: {0}", e);
            y = e
        }
        a()
    }

    function o(e) {
        if (!e) return "";
        var t = P.exec(e),
            r = t[1],
            n = t[3],
            a = t[2];
        return a && (j.innerHTML = a.replace(/</g, "&lt;"), a = "textContent" in j ? j.textContent : j.innerText), r + a + n
    }

    function l(e) {
        return e.replace(/&/g, "&amp;").replace(k, function(e) {
            var t = e.charCodeAt(0),
                r = e.charCodeAt(1);
            return "&#" + (1024 * (t - 55296) + (r - 56320) + 65536) + ";"
        }).replace(y, function(e) {
            return "&#" + e.charCodeAt(0) + ";"
        }).replace(/</g, "&lt;").replace(/>/g, "&gt;")
    }

    function c(e, r) {
        var n = !1,
            a = t.bind(e, e.push);
        return {
            start: function(e, i, s) {
                e = t.lowercase(e), !n && T[e] && (n = e), n || E[e] !== !0 || (a("<"), a(e), t.forEach(i, function(n, i) {
                    var s = t.lowercase(i),
                        o = "img" === e && "src" === s || "background" === s;
                    S[s] !== !0 || F[s] === !0 && !r(n, o) || (a(" "), a(i), a('="'), a(l(n)), a('"'))
                }), a(s ? "/>" : ">"))
            },
            end: function(e) {
                e = t.lowercase(e), n || E[e] !== !0 || (a("</"), a(e), a(">")), e == n && (n = !1)
            },
            chars: function(e) {
                n || a(l(e))
            }
        }
    }
    var u = t.$$minErr("$sanitize"),
        h = /^<((?:[a-zA-Z])[\w:-]*)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*(>?)/,
        p = /^<\/\s*([\w:-]+)[^>]*>/,
        f = /([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,
        d = /^</,
        g = /^<\//,
        m = /<!--(.*?)-->/g,
        b = /<!DOCTYPE([^>]*?)>/i,
        x = /<!\[CDATA\[(.*?)]]>/g,
        k = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g,
        y = /([^\#-~| |!])/g,
        v = i("area,br,col,hr,img,wbr"),
        w = i("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),
        z = i("rp,rt"),
        $ = t.extend({}, z, w),
        C = t.extend({}, w, i("address,article,aside,blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6,header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul")),
        A = t.extend({}, z, i("a,abbr,acronym,b,bdi,bdo,big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small,span,strike,strong,sub,sup,time,tt,u,var")),
        D = i("animate,animateColor,animateMotion,animateTransform,circle,defs,desc,ellipse,font-face,font-face-name,font-face-src,g,glyph,hkern,image,linearGradient,line,marker,metadata,missing-glyph,mpath,path,polygon,polyline,radialGradient,rect,set,stop,svg,switch,text,title,tspan,use"),
        T = i("script,style"),
        E = t.extend({}, v, C, A, $, D),
        F = i("background,cite,href,longdesc,src,usemap,xlink:href"),
        q = i("abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,scope,scrolling,shape,size,span,start,summary,target,title,type,valign,value,vspace,width"),
        O = i("accent-height,accumulate,additive,alphabetic,arabic-form,ascent,attributeName,attributeType,baseProfile,bbox,begin,by,calcMode,cap-height,class,color,color-rendering,content,cx,cy,d,dx,dy,descent,display,dur,end,fill,fill-rule,font-family,font-size,font-stretch,font-style,font-variant,font-weight,from,fx,fy,g1,g2,glyph-name,gradientUnits,hanging,height,horiz-adv-x,horiz-origin-x,ideographic,k,keyPoints,keySplines,keyTimes,lang,marker-end,marker-mid,marker-start,markerHeight,markerUnits,markerWidth,mathematical,max,min,offset,opacity,orient,origin,overline-position,overline-thickness,panose-1,path,pathLength,points,preserveAspectRatio,r,refX,refY,repeatCount,repeatDur,requiredExtensions,requiredFeatures,restart,rotate,rx,ry,slope,stemh,stemv,stop-color,stop-opacity,strikethrough-position,strikethrough-thickness,stroke,stroke-dasharray,stroke-dashoffset,stroke-linecap,stroke-linejoin,stroke-miterlimit,stroke-opacity,stroke-width,systemLanguage,target,text-anchor,to,transform,type,u1,u2,underline-position,underline-thickness,unicode,unicode-range,units-per-em,values,version,viewBox,visibility,width,widths,x,x-height,x1,x2,xlink:actuate,xlink:arcrole,xlink:role,xlink:show,xlink:title,xlink:type,xml:base,xml:lang,xml:space,xmlns,xmlns:xlink,y,y1,y2,zoomAndPan"),
        S = t.extend({}, F, O, q),
        j = document.createElement("pre"),
        P = /^(\s*)([\s\S]*?)(\s*)$/;
    t.module("ngSanitize", []).provider("$sanitize", n), t.module("ngSanitize").filter("linky", ["$sanitize", function(e) {
        var r = /((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"”’]/,
            n = /^mailto:/;
        return function(i, s) {
            function o(e) {
                e && f.push(a(e))
            }

            function l(e, r) {
                f.push("<a "), t.isDefined(s) && f.push('target="', s, '" '), f.push('href="', e.replace(/"/g, "&quot;"), '">'), o(r), f.push("</a>")
            }
            if (!i) return i;
            for (var c, u, h, p = i, f = []; c = p.match(r);) u = c[0], c[2] || c[4] || (u = (c[3] ? "http://" : "mailto:") + u), h = c.index, o(p.substr(0, h)), l(u, c[0].replace(n, "")), p = p.substring(h + c[0].length);
            return o(p), e(f.join(""))
        }
    }])
}(window, window.angular);
angular.module("ui.bootstrap", ["ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.dateparser", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdown", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead"]), angular.module("ui.bootstrap.transition", []).factory("$transition", ["$q", "$timeout", "$rootScope", function(e, t, n) {
    function a(e) {
        for (var t in e)
            if (void 0 !== r.style[t]) return e[t]
    }
    var i = function(a, r, o) {
            o = o || {};
            var l = e.defer(),
                s = i[o.animation ? "animationEndEventName" : "transitionEndEventName"],
                c = function(e) {
                    n.$apply(function() {
                        a.unbind(s, c), l.resolve(a)
                    })
                };
            return s && a.bind(s, c), t(function() {
                angular.isString(r) ? a.addClass(r) : angular.isFunction(r) ? r(a) : angular.isObject(r) && a.css(r), s || l.resolve(a)
            }), l.promise.cancel = function() {
                s && a.unbind(s, c), l.reject("Transition cancelled")
            }, l.promise
        },
        r = document.createElement("trans"),
        o = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            transition: "transitionend"
        },
        l = {
            WebkitTransition: "webkitAnimationEnd",
            MozTransition: "animationend",
            OTransition: "oAnimationEnd",
            transition: "animationend"
        };
    return i.transitionEndEventName = a(o), i.animationEndEventName = a(l), i
}]), angular.module("ui.bootstrap.collapse", ["ui.bootstrap.transition"]).directive("collapse", ["$transition", function(e) {
    return {
        link: function(t, n, a) {
            function i(t) {
                function a() {
                    c === i && (c = void 0)
                }
                var i = e(n, t);
                return c && c.cancel(), c = i, i.then(a, a), i
            }

            function r() {
                u ? (u = !1, o()) : (n.removeClass("collapse").addClass("collapsing"), i({
                    height: n[0].scrollHeight + "px"
                }).then(o))
            }

            function o() {
                n.removeClass("collapsing"), n.addClass("collapse in"), n.css({
                    height: "auto"
                })
            }

            function l() {
                if (u) u = !1, s(), n.css({
                    height: 0
                });
                else {
                    n.css({
                        height: n[0].scrollHeight + "px"
                    });
                    n[0].offsetWidth;
                    n.removeClass("collapse in").addClass("collapsing"), i({
                        height: 0
                    }).then(s)
                }
            }

            function s() {
                n.removeClass("collapsing"), n.addClass("collapse")
            }
            var c, u = !0;
            t.$watch(a.collapse, function(e) {
                e ? l() : r()
            })
        }
    }
}]), angular.module("ui.bootstrap.accordion", ["ui.bootstrap.collapse"]).constant("accordionConfig", {
    closeOthers: !0
}).controller("AccordionController", ["$scope", "$attrs", "accordionConfig", function(e, t, n) {
    this.groups = [], this.closeOthers = function(a) {
        var i = angular.isDefined(t.closeOthers) ? e.$eval(t.closeOthers) : n.closeOthers;
        i && angular.forEach(this.groups, function(e) {
            e !== a && (e.isOpen = !1)
        })
    }, this.addGroup = function(e) {
        var t = this;
        this.groups.push(e), e.$on("$destroy", function(n) {
            t.removeGroup(e)
        })
    }, this.removeGroup = function(e) {
        var t = this.groups.indexOf(e);
        t !== -1 && this.groups.splice(t, 1)
    }
}]).directive("accordion", function() {
    return {
        restrict: "EA",
        controller: "AccordionController",
        transclude: !0,
        replace: !1,
        templateUrl: "template/accordion/accordion.html"
    }
}).directive("accordionGroup", function() {
    return {
        require: "^accordion",
        restrict: "EA",
        transclude: !0,
        replace: !0,
        templateUrl: "template/accordion/accordion-group.html",
        scope: {
            heading: "@",
            isOpen: "=?",
            isDisabled: "=?"
        },
        controller: function() {
            this.setHeading = function(e) {
                this.heading = e
            }
        },
        link: function(e, t, n, a) {
            a.addGroup(e), e.$watch("isOpen", function(t) {
                t && a.closeOthers(e)
            }), e.toggleOpen = function() {
                e.isDisabled || (e.isOpen = !e.isOpen)
            }
        }
    }
}).directive("accordionHeading", function() {
    return {
        restrict: "EA",
        transclude: !0,
        template: "",
        replace: !0,
        require: "^accordionGroup",
        link: function(e, t, n, a, i) {
            a.setHeading(i(e, function() {}))
        }
    }
}).directive("accordionTransclude", function() {
    return {
        require: "^accordionGroup",
        link: function(e, t, n, a) {
            e.$watch(function() {
                return a[n.accordionTransclude]
            }, function(e) {
                e && (t.html(""), t.append(e))
            })
        }
    }
}), angular.module("ui.bootstrap.alert", []).controller("AlertController", ["$scope", "$attrs", function(e, t) {
    e.closeable = "close" in t
}]).directive("alert", function() {
    return {
        restrict: "EA",
        controller: "AlertController",
        templateUrl: "template/alert/alert.html",
        transclude: !0,
        replace: !0,
        scope: {
            type: "@",
            close: "&"
        }
    }
}), angular.module("ui.bootstrap.bindHtml", []).directive("bindHtmlUnsafe", function() {
    return function(e, t, n) {
        t.addClass("ng-binding").data("$binding", n.bindHtmlUnsafe), e.$watch(n.bindHtmlUnsafe, function(e) {
            t.html(e || "")
        })
    }
}), angular.module("ui.bootstrap.buttons", []).constant("buttonConfig", {
    activeClass: "active",
    toggleEvent: "click"
}).controller("ButtonsController", ["buttonConfig", function(e) {
    this.activeClass = e.activeClass || "active", this.toggleEvent = e.toggleEvent || "click"
}]).directive("btnRadio", function() {
    return {
        require: ["btnRadio", "ngModel"],
        controller: "ButtonsController",
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r.$render = function() {
                t.toggleClass(i.activeClass, angular.equals(r.$modelValue, e.$eval(n.btnRadio)))
            }, t.bind(i.toggleEvent, function() {
                var a = t.hasClass(i.activeClass);
                a && !angular.isDefined(n.uncheckable) || e.$apply(function() {
                    r.$setViewValue(a ? null : e.$eval(n.btnRadio)), r.$render()
                })
            })
        }
    }
}).directive("btnCheckbox", function() {
    return {
        require: ["btnCheckbox", "ngModel"],
        controller: "ButtonsController",
        link: function(e, t, n, a) {
            function i() {
                return o(n.btnCheckboxTrue, !0)
            }

            function r() {
                return o(n.btnCheckboxFalse, !1)
            }

            function o(t, n) {
                var a = e.$eval(t);
                return angular.isDefined(a) ? a : n
            }
            var l = a[0],
                s = a[1];
            s.$render = function() {
                t.toggleClass(l.activeClass, angular.equals(s.$modelValue, i()))
            }, t.bind(l.toggleEvent, function() {
                e.$apply(function() {
                    s.$setViewValue(t.hasClass(l.activeClass) ? r() : i()), s.$render()
                })
            })
        }
    }
}), angular.module("ui.bootstrap.carousel", ["ui.bootstrap.transition"]).controller("CarouselController", ["$scope", "$timeout", "$transition", function(e, t, n) {
    function a() {
        i();
        var n = +e.interval;
        !isNaN(n) && n >= 0 && (o = t(r, n))
    }

    function i() {
        o && (t.cancel(o), o = null)
    }

    function r() {
        l ? (e.next(), a()) : e.pause()
    }
    var o, l, s = this,
        c = s.slides = e.slides = [],
        u = -1;
    s.currentSlide = null;
    var p = !1;
    s.select = e.select = function(i, r) {
        function o() {
            if (!p) {
                if (s.currentSlide && angular.isString(r) && !e.noTransition && i.$element) {
                    i.$element.addClass(r);
                    i.$element[0].offsetWidth;
                    angular.forEach(c, function(e) {
                            angular.extend(e, {
                                direction: "",
                                entering: !1,
                                leaving: !1,
                                active: !1
                            })
                        }), angular.extend(i, {
                            direction: r,
                            active: !0,
                            entering: !0
                        }), angular.extend(s.currentSlide || {}, {
                            direction: r,
                            leaving: !0
                        }), e.$currentTransition = n(i.$element, {}),
                        function(t, n) {
                            e.$currentTransition.then(function() {
                                l(t, n)
                            }, function() {
                                l(t, n)
                            })
                        }(i, s.currentSlide)
                } else l(i, s.currentSlide);
                s.currentSlide = i, u = d, a()
            }
        }

        function l(t, n) {
            angular.extend(t, {
                direction: "",
                active: !0,
                leaving: !1,
                entering: !1
            }), angular.extend(n || {}, {
                direction: "",
                active: !1,
                leaving: !1,
                entering: !1
            }), e.$currentTransition = null
        }
        var d = c.indexOf(i);
        void 0 === r && (r = d > u ? "next" : "prev"), i && i !== s.currentSlide && (e.$currentTransition ? (e.$currentTransition.cancel(), t(o)) : o())
    }, e.$on("$destroy", function() {
        p = !0
    }), s.indexOfSlide = function(e) {
        return c.indexOf(e)
    }, e.next = function() {
        var t = (u + 1) % c.length;
        if (!e.$currentTransition) return s.select(c[t], "next")
    }, e.prev = function() {
        var t = u - 1 < 0 ? c.length - 1 : u - 1;
        if (!e.$currentTransition) return s.select(c[t], "prev")
    }, e.isActive = function(e) {
        return s.currentSlide === e
    }, e.$watch("interval", a), e.$on("$destroy", i), e.play = function() {
        l || (l = !0, a())
    }, e.pause = function() {
        e.noPause || (l = !1, i())
    }, s.addSlide = function(t, n) {
        t.$element = n, c.push(t), 1 === c.length || t.active ? (s.select(c[c.length - 1]), 1 == c.length && e.play()) : t.active = !1
    }, s.removeSlide = function(e) {
        var t = c.indexOf(e);
        c.splice(t, 1), c.length > 0 && e.active ? t >= c.length ? s.select(c[t - 1]) : s.select(c[t]) : u > t && u--
    }
}]).directive("carousel", [function() {
    return {
        restrict: "EA",
        transclude: !0,
        replace: !0,
        controller: "CarouselController",
        require: "carousel",
        templateUrl: "template/carousel/carousel.html",
        scope: {
            interval: "=",
            noTransition: "=",
            noPause: "="
        }
    }
}]).directive("slide", function() {
    return {
        require: "^carousel",
        restrict: "EA",
        transclude: !0,
        replace: !0,
        templateUrl: "template/carousel/slide.html",
        scope: {
            active: "=?"
        },
        link: function(e, t, n, a) {
            a.addSlide(e, t), e.$on("$destroy", function() {
                a.removeSlide(e)
            }), e.$watch("active", function(t) {
                t && a.select(e)
            })
        }
    }
}), angular.module("ui.bootstrap.dateparser", []).service("dateParser", ["$locale", "orderByFilter", function(e, t) {
    function n(e) {
        var n = [],
            a = e.split("");
        return angular.forEach(i, function(t, i) {
            var r = e.indexOf(i);
            if (r > -1) {
                e = e.split(""), a[r] = "(" + t.regex + ")", e[r] = "$";
                for (var o = r + 1, l = r + i.length; o < l; o++) a[o] = "", e[o] = "$";
                e = e.join(""), n.push({
                    index: r,
                    apply: t.apply
                })
            }
        }), {
            regex: new RegExp("^" + a.join("") + "$"),
            map: t(n, "index")
        }
    }

    function a(e, t, n) {
        return 1 === t && n > 28 ? 29 === n && (e % 4 === 0 && e % 100 !== 0 || e % 400 === 0) : 3 !== t && 5 !== t && 8 !== t && 10 !== t || n < 31
    }
    this.parsers = {};
    var i = {
        yyyy: {
            regex: "\\d{4}",
            apply: function(e) {
                this.year = +e
            }
        },
        yy: {
            regex: "\\d{2}",
            apply: function(e) {
                this.year = +e + 2e3
            }
        },
        y: {
            regex: "\\d{1,4}",
            apply: function(e) {
                this.year = +e
            }
        },
        MMMM: {
            regex: e.DATETIME_FORMATS.MONTH.join("|"),
            apply: function(t) {
                this.month = e.DATETIME_FORMATS.MONTH.indexOf(t)
            }
        },
        MMM: {
            regex: e.DATETIME_FORMATS.SHORTMONTH.join("|"),
            apply: function(t) {
                this.month = e.DATETIME_FORMATS.SHORTMONTH.indexOf(t)
            }
        },
        MM: {
            regex: "0[1-9]|1[0-2]",
            apply: function(e) {
                this.month = e - 1
            }
        },
        M: {
            regex: "[1-9]|1[0-2]",
            apply: function(e) {
                this.month = e - 1
            }
        },
        dd: {
            regex: "[0-2][0-9]{1}|3[0-1]{1}",
            apply: function(e) {
                this.date = +e
            }
        },
        d: {
            regex: "[1-2]?[0-9]{1}|3[0-1]{1}",
            apply: function(e) {
                this.date = +e
            }
        },
        EEEE: {
            regex: e.DATETIME_FORMATS.DAY.join("|")
        },
        EEE: {
            regex: e.DATETIME_FORMATS.SHORTDAY.join("|")
        }
    };
    this.parse = function(t, i) {
        if (!angular.isString(t) || !i) return t;
        i = e.DATETIME_FORMATS[i] || i, this.parsers[i] || (this.parsers[i] = n(i));
        var r = this.parsers[i],
            o = r.regex,
            l = r.map,
            s = t.match(o);
        if (s && s.length) {
            for (var c, u = {
                    year: 1900,
                    month: 0,
                    date: 1,
                    hours: 0
                }, p = 1, d = s.length; p < d; p++) {
                var f = l[p - 1];
                f.apply && f.apply.call(u, s[p])
            }
            return a(u.year, u.month, u.date) && (c = new Date(u.year, u.month, u.date, u.hours)), c
        }
    }
}]), angular.module("ui.bootstrap.position", []).factory("$position", ["$document", "$window", function(e, t) {
    function n(e, n) {
        return e.currentStyle ? e.currentStyle[n] : t.getComputedStyle ? t.getComputedStyle(e)[n] : e.style[n]
    }

    function a(e) {
        return "static" === (n(e, "position") || "static")
    }
    var i = function(t) {
        for (var n = e[0], i = t.offsetParent || n; i && i !== n && a(i);) i = i.offsetParent;
        return i || n
    };
    return {
        position: function(t) {
            var n = this.offset(t),
                a = {
                    top: 0,
                    left: 0
                },
                r = i(t[0]);
            r != e[0] && (a = this.offset(angular.element(r)), a.top += r.clientTop - r.scrollTop, a.left += r.clientLeft - r.scrollLeft);
            var o = t[0].getBoundingClientRect();
            return {
                width: o.width || t.prop("offsetWidth"),
                height: o.height || t.prop("offsetHeight"),
                top: n.top - a.top,
                left: n.left - a.left
            }
        },
        offset: function(n) {
            var a = n[0].getBoundingClientRect();
            return {
                width: a.width || n.prop("offsetWidth"),
                height: a.height || n.prop("offsetHeight"),
                top: a.top + (t.pageYOffset || e[0].documentElement.scrollTop),
                left: a.left + (t.pageXOffset || e[0].documentElement.scrollLeft)
            }
        },
        positionElements: function(e, t, n, a) {
            var i, r, o, l, s = n.split("-"),
                c = s[0],
                u = s[1] || "center";
            i = a ? this.offset(e) : this.position(e), r = t.prop("offsetWidth"), o = t.prop("offsetHeight");
            var p = {
                    center: function() {
                        return i.left + i.width / 2 - r / 2
                    },
                    left: function() {
                        return i.left
                    },
                    right: function() {
                        return i.left + i.width
                    }
                },
                d = {
                    center: function() {
                        return i.top + i.height / 2 - o / 2
                    },
                    top: function() {
                        return i.top
                    },
                    bottom: function() {
                        return i.top + i.height
                    }
                };
            switch (c) {
                case "right":
                    l = {
                        top: d[u](),
                        left: p[c]()
                    };
                    break;
                case "left":
                    l = {
                        top: d[u](),
                        left: i.left - r
                    };
                    break;
                case "bottom":
                    l = {
                        top: d[c](),
                        left: p[u]()
                    };
                    break;
                default:
                    l = {
                        top: i.top - o,
                        left: p[u]()
                    }
            }
            return l
        }
    }
}]), angular.module("ui.bootstrap.datepicker", ["ui.bootstrap.dateparser", "ui.bootstrap.position"]).constant("datepickerConfig", {
    formatDay: "dd",
    formatMonth: "MMMM",
    formatYear: "yyyy",
    formatDayHeader: "EEE",
    formatDayTitle: "MMMM yyyy",
    formatMonthTitle: "yyyy",
    datepickerMode: "day",
    minMode: "day",
    maxMode: "year",
    showWeeks: !0,
    startingDay: 0,
    yearRange: 20,
    minDate: null,
    maxDate: null
}).controller("DatepickerController", ["$scope", "$attrs", "$parse", "$interpolate", "$timeout", "$log", "dateFilter", "datepickerConfig", function(e, t, n, a, i, r, o, l) {
    var s = this,
        c = {
            $setViewValue: angular.noop
        };
    this.modes = ["day", "month", "year"], angular.forEach(["formatDay", "formatMonth", "formatYear", "formatDayHeader", "formatDayTitle", "formatMonthTitle", "minMode", "maxMode", "showWeeks", "startingDay", "yearRange"], function(n, i) {
        s[n] = angular.isDefined(t[n]) ? i < 8 ? a(t[n])(e.$parent) : e.$parent.$eval(t[n]) : l[n]
    }), angular.forEach(["minDate", "maxDate"], function(a) {
        t[a] ? e.$parent.$watch(n(t[a]), function(e) {
            s[a] = e ? new Date(e) : null, s.refreshView()
        }) : s[a] = l[a] ? new Date(l[a]) : null
    }), e.datepickerMode = e.datepickerMode || l.datepickerMode, e.uniqueId = "datepicker-" + e.$id + "-" + Math.floor(1e4 * Math.random()), this.activeDate = angular.isDefined(t.initDate) ? e.$parent.$eval(t.initDate) : new Date, e.isActive = function(t) {
        return 0 === s.compare(t.date, s.activeDate) && (e.activeDateId = t.uid, !0)
    }, this.init = function(e) {
        c = e, c.$render = function() {
            s.render()
        }
    }, this.render = function() {
        if (c.$modelValue) {
            var e = new Date(c.$modelValue),
                t = !isNaN(e);
            t ? this.activeDate = e : r.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.'), c.$setValidity("date", t)
        }
        this.refreshView()
    }, this.refreshView = function() {
        if (this.element) {
            this._refreshView();
            var e = c.$modelValue ? new Date(c.$modelValue) : null;
            c.$setValidity("date-disabled", !e || this.element && !this.isDisabled(e))
        }
    }, this.createDateObject = function(e, t) {
        var n = c.$modelValue ? new Date(c.$modelValue) : null;
        return {
            date: e,
            label: o(e, t),
            selected: n && 0 === this.compare(e, n),
            disabled: this.isDisabled(e),
            current: 0 === this.compare(e, new Date)
        }
    }, this.isDisabled = function(n) {
        return this.minDate && this.compare(n, this.minDate) < 0 || this.maxDate && this.compare(n, this.maxDate) > 0 || t.dateDisabled && e.dateDisabled({
            date: n,
            mode: e.datepickerMode
        })
    }, this.split = function(e, t) {
        for (var n = []; e.length > 0;) n.push(e.splice(0, t));
        return n
    }, e.select = function(t) {
        if (e.datepickerMode === s.minMode) {
            var n = c.$modelValue ? new Date(c.$modelValue) : new Date(0, 0, 0, 0, 0, 0, 0);
            n.setFullYear(t.getFullYear(), t.getMonth(), t.getDate()), c.$setViewValue(n), c.$render()
        } else s.activeDate = t, e.datepickerMode = s.modes[s.modes.indexOf(e.datepickerMode) - 1]
    }, e.move = function(e) {
        var t = s.activeDate.getFullYear() + e * (s.step.years || 0),
            n = s.activeDate.getMonth() + e * (s.step.months || 0);
        s.activeDate.setFullYear(t, n, 1), s.refreshView()
    }, e.toggleMode = function(t) {
        t = t || 1, e.datepickerMode === s.maxMode && 1 === t || e.datepickerMode === s.minMode && t === -1 || (e.datepickerMode = s.modes[s.modes.indexOf(e.datepickerMode) + t])
    }, e.keys = {
        13: "enter",
        32: "space",
        33: "pageup",
        34: "pagedown",
        35: "end",
        36: "home",
        37: "left",
        38: "up",
        39: "right",
        40: "down"
    };
    var u = function() {
        i(function() {
            s.element[0].focus()
        }, 0, !1)
    };
    e.$on("datepicker.focus", u), e.keydown = function(t) {
        var n = e.keys[t.which];
        if (n && !t.shiftKey && !t.altKey)
            if (t.preventDefault(), t.stopPropagation(), "enter" === n || "space" === n) {
                if (s.isDisabled(s.activeDate)) return;
                e.select(s.activeDate), u()
            } else !t.ctrlKey || "up" !== n && "down" !== n ? (s.handleKeyDown(n, t), s.refreshView()) : (e.toggleMode("up" === n ? 1 : -1), u())
    }
}]).directive("datepicker", function() {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/datepicker.html",
        scope: {
            datepickerMode: "=?",
            dateDisabled: "&"
        },
        require: ["datepicker", "?^ngModel"],
        controller: "DatepickerController",
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r && i.init(r)
        }
    }
}).directive("daypicker", ["dateFilter", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/day.html",
        require: "^datepicker",
        link: function(t, n, a, i) {
            function r(e, t) {
                return 1 !== t || e % 4 !== 0 || e % 100 === 0 && e % 400 !== 0 ? s[t] : 29
            }

            function o(e, t) {
                var n = new Array(t),
                    a = new Date(e),
                    i = 0;
                for (a.setHours(12); i < t;) n[i++] = new Date(a), a.setDate(a.getDate() + 1);
                return n
            }

            function l(e) {
                var t = new Date(e);
                t.setDate(t.getDate() + 4 - (t.getDay() || 7));
                var n = t.getTime();
                return t.setMonth(0), t.setDate(1), Math.floor(Math.round((n - t) / 864e5) / 7) + 1
            }
            t.showWeeks = i.showWeeks, i.step = {
                months: 1
            }, i.element = n;
            var s = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            i._refreshView = function() {
                var n = i.activeDate.getFullYear(),
                    a = i.activeDate.getMonth(),
                    r = new Date(n, a, 1),
                    s = i.startingDay - r.getDay(),
                    c = s > 0 ? 7 - s : -s,
                    u = new Date(r);
                c > 0 && u.setDate(-c + 1);
                for (var p = o(u, 42), d = 0; d < 42; d++) p[d] = angular.extend(i.createDateObject(p[d], i.formatDay), {
                    secondary: p[d].getMonth() !== a,
                    uid: t.uniqueId + "-" + d
                });
                t.labels = new Array(7);
                for (var f = 0; f < 7; f++) t.labels[f] = {
                    abbr: e(p[f].date, i.formatDayHeader),
                    full: e(p[f].date, "EEEE")
                };
                if (t.title = e(i.activeDate, i.formatDayTitle), t.rows = i.split(p, 7), t.showWeeks) {
                    t.weekNumbers = [];
                    for (var g = l(t.rows[0][0].date), m = t.rows.length; t.weekNumbers.push(g++) < m;);
                }
            }, i.compare = function(e, t) {
                return new Date(e.getFullYear(), e.getMonth(), e.getDate()) - new Date(t.getFullYear(), t.getMonth(), t.getDate())
            }, i.handleKeyDown = function(e, t) {
                var n = i.activeDate.getDate();
                if ("left" === e) n -= 1;
                else if ("up" === e) n -= 7;
                else if ("right" === e) n += 1;
                else if ("down" === e) n += 7;
                else if ("pageup" === e || "pagedown" === e) {
                    var a = i.activeDate.getMonth() + ("pageup" === e ? -1 : 1);
                    i.activeDate.setMonth(a, 1), n = Math.min(r(i.activeDate.getFullYear(), i.activeDate.getMonth()), n)
                } else "home" === e ? n = 1 : "end" === e && (n = r(i.activeDate.getFullYear(), i.activeDate.getMonth()));
                i.activeDate.setDate(n)
            }, i.refreshView()
        }
    }
}]).directive("monthpicker", ["dateFilter", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/month.html",
        require: "^datepicker",
        link: function(t, n, a, i) {
            i.step = {
                years: 1
            }, i.element = n, i._refreshView = function() {
                for (var n = new Array(12), a = i.activeDate.getFullYear(), r = 0; r < 12; r++) n[r] = angular.extend(i.createDateObject(new Date(a, r, 1), i.formatMonth), {
                    uid: t.uniqueId + "-" + r
                });
                t.title = e(i.activeDate, i.formatMonthTitle), t.rows = i.split(n, 3)
            }, i.compare = function(e, t) {
                return new Date(e.getFullYear(), e.getMonth()) - new Date(t.getFullYear(), t.getMonth())
            }, i.handleKeyDown = function(e, t) {
                var n = i.activeDate.getMonth();
                if ("left" === e) n -= 1;
                else if ("up" === e) n -= 3;
                else if ("right" === e) n += 1;
                else if ("down" === e) n += 3;
                else if ("pageup" === e || "pagedown" === e) {
                    var a = i.activeDate.getFullYear() + ("pageup" === e ? -1 : 1);
                    i.activeDate.setFullYear(a)
                } else "home" === e ? n = 0 : "end" === e && (n = 11);
                i.activeDate.setMonth(n)
            }, i.refreshView()
        }
    }
}]).directive("yearpicker", ["dateFilter", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/year.html",
        require: "^datepicker",
        link: function(e, t, n, a) {
            function i(e) {
                return parseInt((e - 1) / r, 10) * r + 1
            }
            var r = a.yearRange;
            a.step = {
                years: r
            }, a.element = t, a._refreshView = function() {
                for (var t = new Array(r), n = 0, o = i(a.activeDate.getFullYear()); n < r; n++) t[n] = angular.extend(a.createDateObject(new Date(o + n, 0, 1), a.formatYear), {
                    uid: e.uniqueId + "-" + n
                });
                e.title = [t[0].label, t[r - 1].label].join(" - "), e.rows = a.split(t, 5)
            }, a.compare = function(e, t) {
                return e.getFullYear() - t.getFullYear()
            }, a.handleKeyDown = function(e, t) {
                var n = a.activeDate.getFullYear();
                "left" === e ? n -= 1 : "up" === e ? n -= 5 : "right" === e ? n += 1 : "down" === e ? n += 5 : "pageup" === e || "pagedown" === e ? n += ("pageup" === e ? -1 : 1) * a.step.years : "home" === e ? n = i(a.activeDate.getFullYear()) : "end" === e && (n = i(a.activeDate.getFullYear()) + r - 1), a.activeDate.setFullYear(n)
            }, a.refreshView()
        }
    }
}]).constant("datepickerPopupConfig", {
    datepickerPopup: "yyyy-MM-dd",
    currentText: "Today",
    clearText: "Clear",
    closeText: "Done",
    closeOnDateSelection: !0,
    appendToBody: !1,
    showButtonBar: !0
}).directive("datepickerPopup", ["$compile", "$parse", "$document", "$position", "dateFilter", "dateParser", "datepickerPopupConfig", function(e, t, n, a, i, r, o) {
    return {
        restrict: "EA",
        require: "ngModel",
        scope: {
            isOpen: "=?",
            currentText: "@",
            clearText: "@",
            closeText: "@",
            dateDisabled: "&"
        },
        link: function(l, s, c, u) {
            function p(e) {
                return e.replace(/([A-Z])/g, function(e) {
                    return "-" + e.toLowerCase()
                })
            }

            function d(e) {
                if (e) {
                    if (angular.isDate(e) && !isNaN(e)) return u.$setValidity("date", !0), e;
                    if (angular.isString(e)) {
                        var t = r.parse(e, f) || new Date(e);
                        return isNaN(t) ? void u.$setValidity("date", !1) : (u.$setValidity("date", !0), t)
                    }
                    return void u.$setValidity("date", !1)
                }
                return u.$setValidity("date", !0), null
            }
            var f, g = angular.isDefined(c.closeOnDateSelection) ? l.$parent.$eval(c.closeOnDateSelection) : o.closeOnDateSelection,
                m = angular.isDefined(c.datepickerAppendToBody) ? l.$parent.$eval(c.datepickerAppendToBody) : o.appendToBody;
            l.showButtonBar = angular.isDefined(c.showButtonBar) ? l.$parent.$eval(c.showButtonBar) : o.showButtonBar, l.getText = function(e) {
                return l[e + "Text"] || o[e + "Text"]
            }, c.$observe("datepickerPopup", function(e) {
                f = e || o.datepickerPopup, u.$render()
            });
            var h = angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");
            h.attr({
                "ng-model": "date",
                "ng-change": "dateSelection()"
            });
            var v = angular.element(h.children()[0]);
            c.datepickerOptions && angular.forEach(l.$parent.$eval(c.datepickerOptions), function(e, t) {
                v.attr(p(t), e)
            }), l.watchData = {}, angular.forEach(["minDate", "maxDate", "datepickerMode"], function(e) {
                if (c[e]) {
                    var n = t(c[e]);
                    if (l.$parent.$watch(n, function(t) {
                            l.watchData[e] = t
                        }), v.attr(p(e), "watchData." + e), "datepickerMode" === e) {
                        var a = n.assign;
                        l.$watch("watchData." + e, function(e, t) {
                            e !== t && a(l.$parent, e)
                        })
                    }
                }
            }), c.dateDisabled && v.attr("date-disabled", "dateDisabled({ date: date, mode: mode })"), u.$parsers.unshift(d), l.dateSelection = function(e) {
                angular.isDefined(e) && (l.date = e), u.$setViewValue(l.date), u.$render(), g && (l.isOpen = !1, s[0].focus())
            }, s.bind("input change keyup", function() {
                l.$apply(function() {
                    l.date = u.$modelValue
                })
            }), u.$render = function() {
                var e = u.$viewValue ? i(u.$viewValue, f) : "";
                s.val(e), l.date = d(u.$modelValue)
            };
            var $ = function(e) {
                    l.isOpen && e.target !== s[0] && l.$apply(function() {
                        l.isOpen = !1
                    })
                },
                b = function(e, t) {
                    l.keydown(e)
                };
            s.bind("keydown", b), l.keydown = function(e) {
                27 === e.which ? (e.preventDefault(), e.stopPropagation(), l.close()) : 40 !== e.which || l.isOpen || (l.isOpen = !0)
            }, l.$watch("isOpen", function(e) {
                e ? (l.$broadcast("datepicker.focus"), l.position = m ? a.offset(s) : a.position(s), l.position.top = l.position.top + s.prop("offsetHeight"), n.bind("click", $)) : n.unbind("click", $)
            }), l.select = function(e) {
                if ("today" === e) {
                    var t = new Date;
                    angular.isDate(u.$modelValue) ? (e = new Date(u.$modelValue), e.setFullYear(t.getFullYear(), t.getMonth(), t.getDate())) : e = new Date(t.setHours(0, 0, 0, 0))
                }
                l.dateSelection(e)
            }, l.close = function() {
                l.isOpen = !1, s[0].focus()
            };
            var y = e(h)(l);
            h.remove(), m ? n.find("body").append(y) : s.after(y), l.$on("$destroy", function() {
                y.remove(), s.unbind("keydown", b), n.unbind("click", $)
            })
        }
    }
}]).directive("datepickerPopupWrap", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        templateUrl: "template/datepicker/popup.html",
        link: function(e, t, n) {
            t.bind("click", function(e) {
                e.preventDefault(), e.stopPropagation()
            })
        }
    }
}), angular.module("ui.bootstrap.dropdown", []).constant("dropdownConfig", {
    openClass: "open"
}).service("dropdownService", ["$document", function(e) {
    var t = null;
    this.open = function(i) {
        t || (e.bind("click", n), e.bind("keydown", a)), t && t !== i && (t.isOpen = !1), t = i
    }, this.close = function(i) {
        t === i && (t = null, e.unbind("click", n), e.unbind("keydown", a))
    };
    var n = function(e) {
            var n = t.getToggleElement();
            e && n && n[0].contains(e.target) || t.$apply(function() {
                t.isOpen = !1
            })
        },
        a = function(e) {
            27 === e.which && (t.focusToggleElement(), n())
        }
}]).controller("DropdownController", ["$scope", "$attrs", "$parse", "dropdownConfig", "dropdownService", "$animate", function(e, t, n, a, i, r) {
    var o, l = this,
        s = e.$new(),
        c = a.openClass,
        u = angular.noop,
        p = t.onToggle ? n(t.onToggle) : angular.noop;
    this.init = function(a) {
        l.$element = a, t.isOpen && (o = n(t.isOpen), u = o.assign, e.$watch(o, function(e) {
            s.isOpen = !!e
        }))
    }, this.toggle = function(e) {
        return s.isOpen = arguments.length ? !!e : !s.isOpen
    }, this.isOpen = function() {
        return s.isOpen
    }, s.getToggleElement = function() {
        return l.toggleElement
    }, s.focusToggleElement = function() {
        l.toggleElement && l.toggleElement[0].focus()
    }, s.$watch("isOpen", function(t, n) {
        r[t ? "addClass" : "removeClass"](l.$element, c), t ? (s.focusToggleElement(), i.open(s)) : i.close(s), u(e, t), angular.isDefined(t) && t !== n && p(e, {
            open: !!t
        })
    }), e.$on("$locationChangeSuccess", function() {
        s.isOpen = !1
    }), e.$on("$destroy", function() {
        s.$destroy()
    })
}]).directive("dropdown", function() {
    return {
        restrict: "CA",
        controller: "DropdownController",
        link: function(e, t, n, a) {
            a.init(t)
        }
    }
}).directive("dropdownToggle", function() {
    return {
        restrict: "CA",
        require: "?^dropdown",
        link: function(e, t, n, a) {
            if (a) {
                a.toggleElement = t;
                var i = function(i) {
                    i.preventDefault(), t.hasClass("disabled") || n.disabled || e.$apply(function() {
                        a.toggle()
                    })
                };
                t.bind("click", i), t.attr({
                    "aria-haspopup": !0,
                    "aria-expanded": !1
                }), e.$watch(a.isOpen, function(e) {
                    t.attr("aria-expanded", !!e)
                }), e.$on("$destroy", function() {
                    t.unbind("click", i)
                })
            }
        }
    }
}), angular.module("ui.bootstrap.modal", ["ui.bootstrap.transition"]).factory("$$stackedMap", function() {
    return {
        createNew: function() {
            var e = [];
            return {
                add: function(t, n) {
                    e.push({
                        key: t,
                        value: n
                    })
                },
                get: function(t) {
                    for (var n = 0; n < e.length; n++)
                        if (t == e[n].key) return e[n]
                },
                keys: function() {
                    for (var t = [], n = 0; n < e.length; n++) t.push(e[n].key);
                    return t
                },
                top: function() {
                    return e[e.length - 1]
                },
                remove: function(t) {
                    for (var n = -1, a = 0; a < e.length; a++)
                        if (t == e[a].key) {
                            n = a;
                            break
                        }
                    return e.splice(n, 1)[0]
                },
                removeTop: function() {
                    return e.splice(e.length - 1, 1)[0]
                },
                length: function() {
                    return e.length
                }
            }
        }
    }
}).directive("modalBackdrop", ["$timeout", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/modal/backdrop.html",
        link: function(t, n, a) {
            t.backdropClass = a.backdropClass || "", t.animate = !1, e(function() {
                t.animate = !0
            })
        }
    }
}]).directive("modalWindow", ["$modalStack", "$timeout", function(e, t) {
    return {
        restrict: "EA",
        scope: {
            index: "@",
            animate: "="
        },
        replace: !0,
        transclude: !0,
        templateUrl: function(e, t) {
            return t.templateUrl || "template/modal/window.html"
        },
        link: function(n, a, i) {
            a.addClass(i.windowClass || ""), n.size = i.size, t(function() {
                n.animate = !0, a[0].querySelectorAll("[autofocus]").length || a[0].focus()
            }), n.close = function(t) {
                var n = e.getTop();
                n && n.value.backdrop && "static" != n.value.backdrop && t.target === t.currentTarget && (t.preventDefault(), t.stopPropagation(), e.dismiss(n.key, "backdrop click"))
            }
        }
    }
}]).directive("modalTransclude", function() {
    return {
        link: function(e, t, n, a, i) {
            i(e.$parent, function(e) {
                t.empty(), t.append(e)
            })
        }
    }
}).factory("$modalStack", ["$transition", "$timeout", "$document", "$compile", "$rootScope", "$$stackedMap", function(e, t, n, a, i, r) {
    function o() {
        for (var e = -1, t = f.keys(), n = 0; n < t.length; n++) f.get(t[n]).value.backdrop && (e = n);
        return e
    }

    function l(e) {
        var t = n.find("body").eq(0),
            a = f.get(e).value;
        f.remove(e), c(a.modalDomEl, a.modalScope, 300, function() {
            a.modalScope.$destroy(), t.toggleClass(d, f.length() > 0), s()
        })
    }

    function s() {
        if (u && o() == -1) {
            var e = p;
            c(u, p, 150, function() {
                e.$destroy(), e = null
            }), u = void 0, p = void 0
        }
    }

    function c(n, a, i, r) {
        function o() {
            o.done || (o.done = !0, n.remove(), r && r())
        }
        a.animate = !1;
        var l = e.transitionEndEventName;
        if (l) {
            var s = t(o, i);
            n.bind(l, function() {
                t.cancel(s), o(), a.$apply()
            })
        } else t(o)
    }
    var u, p, d = "modal-open",
        f = r.createNew(),
        g = {};
    return i.$watch(o, function(e) {
        p && (p.index = e)
    }), n.bind("keydown", function(e) {
        var t;
        27 === e.which && (t = f.top(), t && t.value.keyboard && (e.preventDefault(), i.$apply(function() {
            g.dismiss(t.key, "escape key press")
        })))
    }), g.open = function(e, t) {
        f.add(e, {
            deferred: t.deferred,
            modalScope: t.scope,
            backdrop: t.backdrop,
            keyboard: t.keyboard
        });
        var r = n.find("body").eq(0),
            l = o();
        if (l >= 0 && !u) {
            p = i.$new(!0), p.index = l;
            var s = angular.element("<div modal-backdrop></div>");
            s.attr("backdrop-class", t.backdropClass), u = a(s)(p), r.append(u)
        }
        var c = angular.element("<div modal-window></div>");
        c.attr({
            "template-url": t.windowTemplateUrl,
            "window-class": t.windowClass,
            size: t.size,
            index: f.length() - 1,
            animate: "animate"
        }).html(t.content);
        var g = a(c)(t.scope);
        f.top().value.modalDomEl = g, r.append(g), r.addClass(d)
    }, g.close = function(e, t) {
        var n = f.get(e);
        n && (n.value.deferred.resolve(t), l(e))
    }, g.dismiss = function(e, t) {
        var n = f.get(e);
        n && (n.value.deferred.reject(t), l(e))
    }, g.dismissAll = function(e) {
        for (var t = this.getTop(); t;) this.dismiss(t.key, e), t = this.getTop()
    }, g.getTop = function() {
        return f.top()
    }, g
}]).provider("$modal", function() {
    var e = {
        options: {
            backdrop: !0,
            keyboard: !0
        },
        $get: ["$injector", "$rootScope", "$q", "$http", "$templateCache", "$controller", "$modalStack", function(t, n, a, i, r, o, l) {
            function s(e) {
                return e.template ? a.when(e.template) : i.get(angular.isFunction(e.templateUrl) ? e.templateUrl() : e.templateUrl, {
                    cache: r
                }).then(function(e) {
                    return e.data
                })
            }

            function c(e) {
                var n = [];
                return angular.forEach(e, function(e) {
                    (angular.isFunction(e) || angular.isArray(e)) && n.push(a.when(t.invoke(e)))
                }), n
            }
            var u = {};
            return u.open = function(t) {
                var i = a.defer(),
                    r = a.defer(),
                    u = {
                        result: i.promise,
                        opened: r.promise,
                        close: function(e) {
                            l.close(u, e)
                        },
                        dismiss: function(e) {
                            l.dismiss(u, e)
                        }
                    };
                if (t = angular.extend({}, e.options, t), t.resolve = t.resolve || {}, !t.template && !t.templateUrl) throw new Error("One of template or templateUrl options is required.");
                var p = a.all([s(t)].concat(c(t.resolve)));
                return p.then(function(e) {
                    var a = (t.scope || n).$new();
                    a.$close = u.close, a.$dismiss = u.dismiss;
                    var r, s = {},
                        c = 1;
                    t.controller && (s.$scope = a, s.$modalInstance = u, angular.forEach(t.resolve, function(t, n) {
                        s[n] = e[c++]
                    }), r = o(t.controller, s), t.controllerAs && (a[t.controllerAs] = r)), l.open(u, {
                        scope: a,
                        deferred: i,
                        content: e[0],
                        backdrop: t.backdrop,
                        keyboard: t.keyboard,
                        backdropClass: t.backdropClass,
                        windowClass: t.windowClass,
                        windowTemplateUrl: t.windowTemplateUrl,
                        size: t.size
                    })
                }, function(e) {
                    i.reject(e)
                }), p.then(function() {
                    r.resolve(!0)
                }, function() {
                    r.reject(!1)
                }), u
            }, u
        }]
    };
    return e
}), angular.module("ui.bootstrap.pagination", []).controller("PaginationController", ["$scope", "$attrs", "$parse", function(e, t, n) {
    var a = this,
        i = {
            $setViewValue: angular.noop
        },
        r = t.numPages ? n(t.numPages).assign : angular.noop;
    this.init = function(r, o) {
        i = r, this.config = o, i.$render = function() {
            a.render()
        }, t.itemsPerPage ? e.$parent.$watch(n(t.itemsPerPage), function(t) {
            a.itemsPerPage = parseInt(t, 10), e.totalPages = a.calculateTotalPages()
        }) : this.itemsPerPage = o.itemsPerPage
    }, this.calculateTotalPages = function() {
        var t = this.itemsPerPage < 1 ? 1 : Math.ceil(e.totalItems / this.itemsPerPage);
        return Math.max(t || 0, 1)
    }, this.render = function() {
        e.page = parseInt(i.$viewValue, 10) || 1
    }, e.selectPage = function(t) {
        e.page !== t && t > 0 && t <= e.totalPages && (i.$setViewValue(t), i.$render())
    }, e.getText = function(t) {
        return e[t + "Text"] || a.config[t + "Text"]
    }, e.noPrevious = function() {
        return 1 === e.page
    }, e.noNext = function() {
        return e.page === e.totalPages
    }, e.$watch("totalItems", function() {
        e.totalPages = a.calculateTotalPages()
    }), e.$watch("totalPages", function(t) {
        r(e.$parent, t), e.page > t ? e.selectPage(t) : i.$render()
    })
}]).constant("paginationConfig", {
    itemsPerPage: 10,
    boundaryLinks: !1,
    directionLinks: !0,
    firstText: "First",
    previousText: "Previous",
    nextText: "Next",
    lastText: "Last",
    rotate: !0
}).directive("pagination", ["$parse", "paginationConfig", function(e, t) {
    return {
        restrict: "EA",
        scope: {
            totalItems: "=",
            firstText: "@",
            previousText: "@",
            nextText: "@",
            lastText: "@"
        },
        require: ["pagination", "?ngModel"],
        controller: "PaginationController",
        templateUrl: "template/pagination/pagination.html",
        replace: !0,
        link: function(n, a, i, r) {
            function o(e, t, n) {
                return {
                    number: e,
                    text: t,
                    active: n
                }
            }

            function l(e, t) {
                var n = [],
                    a = 1,
                    i = t,
                    r = angular.isDefined(u) && u < t;
                r && (p ? (a = Math.max(e - Math.floor(u / 2), 1), i = a + u - 1, i > t && (i = t, a = i - u + 1)) : (a = (Math.ceil(e / u) - 1) * u + 1, i = Math.min(a + u - 1, t)));
                for (var l = a; l <= i; l++) {
                    var s = o(l, l, l === e);
                    n.push(s)
                }
                if (r && !p) {
                    if (a > 1) {
                        var c = o(a - 1, "...", !1);
                        n.unshift(c)
                    }
                    if (i < t) {
                        var d = o(i + 1, "...", !1);
                        n.push(d)
                    }
                }
                return n
            }
            var s = r[0],
                c = r[1];
            if (c) {
                var u = angular.isDefined(i.maxSize) ? n.$parent.$eval(i.maxSize) : t.maxSize,
                    p = angular.isDefined(i.rotate) ? n.$parent.$eval(i.rotate) : t.rotate;
                n.boundaryLinks = angular.isDefined(i.boundaryLinks) ? n.$parent.$eval(i.boundaryLinks) : t.boundaryLinks, n.directionLinks = angular.isDefined(i.directionLinks) ? n.$parent.$eval(i.directionLinks) : t.directionLinks, s.init(c, t), i.maxSize && n.$parent.$watch(e(i.maxSize), function(e) {
                    u = parseInt(e, 10), s.render()
                });
                var d = s.render;
                s.render = function() {
                    d(), n.page > 0 && n.page <= n.totalPages && (n.pages = l(n.page, n.totalPages))
                }
            }
        }
    }
}]).constant("pagerConfig", {
    itemsPerPage: 10,
    previousText: "« Previous",
    nextText: "Next »",
    align: !0
}).directive("pager", ["pagerConfig", function(e) {
    return {
        restrict: "EA",
        scope: {
            totalItems: "=",
            previousText: "@",
            nextText: "@"
        },
        require: ["pager", "?ngModel"],
        controller: "PaginationController",
        templateUrl: "template/pagination/pager.html",
        replace: !0,
        link: function(t, n, a, i) {
            var r = i[0],
                o = i[1];
            o && (t.align = angular.isDefined(a.align) ? t.$parent.$eval(a.align) : e.align, r.init(o, e))
        }
    }
}]), angular.module("ui.bootstrap.tooltip", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).provider("$tooltip", function() {
    function e(e) {
        var t = /[A-Z]/g,
            n = "-";
        return e.replace(t, function(e, t) {
            return (t ? n : "") + e.toLowerCase()
        })
    }
    var t = {
            placement: "top",
            animation: !0,
            popupDelay: 0
        },
        n = {
            mouseenter: "mouseleave",
            click: "click",
            focus: "blur"
        },
        a = {};
    this.options = function(e) {
        angular.extend(a, e);
    }, this.setTriggers = function(e) {
        angular.extend(n, e)
    }, this.$get = ["$window", "$compile", "$timeout", "$parse", "$document", "$position", "$interpolate", function(i, r, o, l, s, c, u) {
        return function(i, p, d) {
            function f(e) {
                var t = e || g.trigger || d,
                    a = n[t] || t;
                return {
                    show: t,
                    hide: a
                }
            }
            var g = angular.extend({}, t, a),
                m = e(i),
                h = u.startSymbol(),
                v = u.endSymbol(),
                $ = "<div " + m + '-popup title="' + h + "tt_title" + v + '" content="' + h + "tt_content" + v + '" placement="' + h + "tt_placement" + v + '" animation="tt_animation" is-open="tt_isOpen"></div>';
            return {
                restrict: "EA",
                scope: !0,
                compile: function(e, t) {
                    var n = r($);
                    return function(e, t, a) {
                        function r() {
                            e.tt_isOpen ? d() : u()
                        }

                        function u() {
                            M && !e.$eval(a[p + "Enable"]) || (e.tt_popupDelay ? w || (w = o(m, e.tt_popupDelay, !1), w.then(function(e) {
                                e()
                            })) : m()())
                        }

                        function d() {
                            e.$apply(function() {
                                h()
                            })
                        }

                        function m() {
                            return w = null, y && (o.cancel(y), y = null), e.tt_content ? (v(), b.css({
                                top: 0,
                                left: 0,
                                display: "block"
                            }), D ? s.find("body").append(b) : t.after(b), x(), e.tt_isOpen = !0, e.$digest(), x) : angular.noop
                        }

                        function h() {
                            e.tt_isOpen = !1, o.cancel(w), w = null, e.tt_animation ? y || (y = o($, 500)) : $()
                        }

                        function v() {
                            b && $(), b = n(e, function() {}), e.$digest()
                        }

                        function $() {
                            y = null, b && (b.remove(), b = null)
                        }
                        var b, y, w, D = !!angular.isDefined(g.appendToBody) && g.appendToBody,
                            k = f(void 0),
                            M = angular.isDefined(a[p + "Enable"]),
                            x = function() {
                                var n = c.positionElements(t, b, e.tt_placement, D);
                                n.top += "px", n.left += "px", b.css(n)
                            };
                        e.tt_isOpen = !1, a.$observe(i, function(t) {
                            e.tt_content = t, !t && e.tt_isOpen && h()
                        }), a.$observe(p + "Title", function(t) {
                            e.tt_title = t
                        }), a.$observe(p + "Placement", function(t) {
                            e.tt_placement = angular.isDefined(t) ? t : g.placement
                        }), a.$observe(p + "PopupDelay", function(t) {
                            var n = parseInt(t, 10);
                            e.tt_popupDelay = isNaN(n) ? g.popupDelay : n
                        });
                        var T = function() {
                            t.unbind(k.show, u), t.unbind(k.hide, d)
                        };
                        a.$observe(p + "Trigger", function(e) {
                            T(), k = f(e), k.show === k.hide ? t.bind(k.show, r) : (t.bind(k.show, u), t.bind(k.hide, d))
                        });
                        var E = e.$eval(a[p + "Animation"]);
                        e.tt_animation = angular.isDefined(E) ? !!E : g.animation, a.$observe(p + "AppendToBody", function(t) {
                            D = angular.isDefined(t) ? l(t)(e) : D
                        }), D && e.$on("$locationChangeSuccess", function() {
                            e.tt_isOpen && h()
                        }), e.$on("$destroy", function() {
                            o.cancel(y), o.cancel(w), T(), $()
                        })
                    }
                }
            }
        }
    }]
}).directive("tooltipPopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-popup.html"
    }
}).directive("tooltip", ["$tooltip", function(e) {
    return e("tooltip", "tooltip", "mouseenter")
}]).directive("tooltipHtmlUnsafePopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-html-unsafe-popup.html"
    }
}).directive("tooltipHtmlUnsafe", ["$tooltip", function(e) {
    return e("tooltipHtmlUnsafe", "tooltip", "mouseenter")
}]), angular.module("ui.bootstrap.popover", ["ui.bootstrap.tooltip"]).directive("popoverPopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            title: "@",
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/popover/popover.html"
    }
}).directive("popover", ["$tooltip", function(e) {
    return e("popover", "popover", "click")
}]), angular.module("ui.bootstrap.progressbar", []).constant("progressConfig", {
    animate: !0,
    max: 100
}).controller("ProgressController", ["$scope", "$attrs", "progressConfig", function(e, t, n) {
    var a = this,
        i = angular.isDefined(t.animate) ? e.$parent.$eval(t.animate) : n.animate;
    this.bars = [], e.max = angular.isDefined(t.max) ? e.$parent.$eval(t.max) : n.max, this.addBar = function(t, n) {
        i || n.css({
            transition: "none"
        }), this.bars.push(t), t.$watch("value", function(n) {
            t.percent = +(100 * n / e.max).toFixed(2)
        }), t.$on("$destroy", function() {
            n = null, a.removeBar(t)
        })
    }, this.removeBar = function(e) {
        this.bars.splice(this.bars.indexOf(e), 1)
    }
}]).directive("progress", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        controller: "ProgressController",
        require: "progress",
        scope: {},
        templateUrl: "template/progressbar/progress.html"
    }
}).directive("bar", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        require: "^progress",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/bar.html",
        link: function(e, t, n, a) {
            a.addBar(e, t)
        }
    }
}).directive("progressbar", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        controller: "ProgressController",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/progressbar.html",
        link: function(e, t, n, a) {
            a.addBar(e, angular.element(t.children()[0]))
        }
    }
}), angular.module("ui.bootstrap.rating", []).constant("ratingConfig", {
    max: 5,
    stateOn: null,
    stateOff: null
}).controller("RatingController", ["$scope", "$attrs", "ratingConfig", function(e, t, n) {
    var a = {
        $setViewValue: angular.noop
    };
    this.init = function(i) {
        a = i, a.$render = this.render, this.stateOn = angular.isDefined(t.stateOn) ? e.$parent.$eval(t.stateOn) : n.stateOn, this.stateOff = angular.isDefined(t.stateOff) ? e.$parent.$eval(t.stateOff) : n.stateOff;
        var r = angular.isDefined(t.ratingStates) ? e.$parent.$eval(t.ratingStates) : new Array(angular.isDefined(t.max) ? e.$parent.$eval(t.max) : n.max);
        e.range = this.buildTemplateObjects(r)
    }, this.buildTemplateObjects = function(e) {
        for (var t = 0, n = e.length; t < n; t++) e[t] = angular.extend({
            index: t
        }, {
            stateOn: this.stateOn,
            stateOff: this.stateOff
        }, e[t]);
        return e
    }, e.rate = function(t) {
        !e.readonly && t >= 0 && t <= e.range.length && (a.$setViewValue(t), a.$render())
    }, e.enter = function(t) {
        e.readonly || (e.value = t), e.onHover({
            value: t
        })
    }, e.reset = function() {
        e.value = a.$viewValue, e.onLeave()
    }, e.onKeydown = function(t) {
        /(37|38|39|40)/.test(t.which) && (t.preventDefault(), t.stopPropagation(), e.rate(e.value + (38 === t.which || 39 === t.which ? 1 : -1)))
    }, this.render = function() {
        e.value = a.$viewValue
    }
}]).directive("rating", function() {
    return {
        restrict: "EA",
        require: ["rating", "ngModel"],
        scope: {
            readonly: "=?",
            onHover: "&",
            onLeave: "&"
        },
        controller: "RatingController",
        templateUrl: "template/rating/rating.html",
        replace: !0,
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r && i.init(r)
        }
    }
}), angular.module("ui.bootstrap.tabs", []).controller("TabsetController", ["$scope", function(e) {
    var t = this,
        n = t.tabs = e.tabs = [];
    t.select = function(e) {
        angular.forEach(n, function(t) {
            t.active && t !== e && (t.active = !1, t.onDeselect())
        }), e.active = !0, e.onSelect()
    }, t.addTab = function(e) {
        n.push(e), 1 === n.length ? e.active = !0 : e.active && t.select(e)
    }, t.removeTab = function(e) {
        var a = n.indexOf(e);
        if (e.active && n.length > 1) {
            var i = a == n.length - 1 ? a - 1 : a + 1;
            t.select(n[i])
        }
        n.splice(a, 1)
    }
}]).directive("tabset", function() {
    return {
        restrict: "EA",
        transclude: !0,
        replace: !0,
        scope: {
            type: "@"
        },
        controller: "TabsetController",
        templateUrl: "template/tabs/tabset.html",
        link: function(e, t, n) {
            e.vertical = !!angular.isDefined(n.vertical) && e.$parent.$eval(n.vertical), e.justified = !!angular.isDefined(n.justified) && e.$parent.$eval(n.justified)
        }
    }
}).directive("tab", ["$parse", function(e) {
    return {
        require: "^tabset",
        restrict: "EA",
        replace: !0,
        templateUrl: "template/tabs/tab.html",
        transclude: !0,
        scope: {
            active: "=?",
            heading: "@",
            onSelect: "&select",
            onDeselect: "&deselect"
        },
        controller: function() {},
        compile: function(t, n, a) {
            return function(t, n, i, r) {
                t.$watch("active", function(e) {
                    e && r.select(t)
                }), t.disabled = !1, i.disabled && t.$parent.$watch(e(i.disabled), function(e) {
                    t.disabled = !!e
                }), t.select = function() {
                    t.disabled || (t.active = !0)
                }, r.addTab(t), t.$on("$destroy", function() {
                    r.removeTab(t)
                }), t.$transcludeFn = a
            }
        }
    }
}]).directive("tabHeadingTransclude", [function() {
    return {
        restrict: "A",
        require: "^tab",
        link: function(e, t, n, a) {
            e.$watch("headingElement", function(e) {
                e && (t.html(""), t.append(e))
            })
        }
    }
}]).directive("tabContentTransclude", function() {
    function e(e) {
        return e.tagName && (e.hasAttribute("tab-heading") || e.hasAttribute("data-tab-heading") || "tab-heading" === e.tagName.toLowerCase() || "data-tab-heading" === e.tagName.toLowerCase())
    }
    return {
        restrict: "A",
        require: "^tabset",
        link: function(t, n, a) {
            var i = t.$eval(a.tabContentTransclude);
            i.$transcludeFn(i.$parent, function(t) {
                angular.forEach(t, function(t) {
                    e(t) ? i.headingElement = t : n.append(t)
                })
            })
        }
    }
}), angular.module("ui.bootstrap.timepicker", []).constant("timepickerConfig", {
    hourStep: 1,
    minuteStep: 1,
    showMeridian: !0,
    meridians: null,
    readonlyInput: !1,
    mousewheel: !0
}).controller("TimepickerController", ["$scope", "$attrs", "$parse", "$log", "$locale", "timepickerConfig", function(e, t, n, a, i, r) {
    function o() {
        var t = parseInt(e.hours, 10),
            n = e.showMeridian ? t > 0 && t < 13 : t >= 0 && t < 24;
        if (n) return e.showMeridian && (12 === t && (t = 0), e.meridian === m[1] && (t += 12)), t
    }

    function l() {
        var t = parseInt(e.minutes, 10);
        return t >= 0 && t < 60 ? t : void 0
    }

    function s(e) {
        return angular.isDefined(e) && e.toString().length < 2 ? "0" + e : e
    }

    function c(e) {
        u(), g.$setViewValue(new Date(f)), p(e)
    }

    function u() {
        g.$setValidity("time", !0), e.invalidHours = !1, e.invalidMinutes = !1
    }

    function p(t) {
        var n = f.getHours(),
            a = f.getMinutes();
        e.showMeridian && (n = 0 === n || 12 === n ? 12 : n % 12), e.hours = "h" === t ? n : s(n), e.minutes = "m" === t ? a : s(a), e.meridian = f.getHours() < 12 ? m[0] : m[1]
    }

    function d(e) {
        var t = new Date(f.getTime() + 6e4 * e);
        f.setHours(t.getHours(), t.getMinutes()), c()
    }
    var f = new Date,
        g = {
            $setViewValue: angular.noop
        },
        m = angular.isDefined(t.meridians) ? e.$parent.$eval(t.meridians) : r.meridians || i.DATETIME_FORMATS.AMPMS;
    this.init = function(n, a) {
        g = n, g.$render = this.render;
        var i = a.eq(0),
            o = a.eq(1),
            l = angular.isDefined(t.mousewheel) ? e.$parent.$eval(t.mousewheel) : r.mousewheel;
        l && this.setupMousewheelEvents(i, o), e.readonlyInput = angular.isDefined(t.readonlyInput) ? e.$parent.$eval(t.readonlyInput) : r.readonlyInput, this.setupInputEvents(i, o)
    };
    var h = r.hourStep;
    t.hourStep && e.$parent.$watch(n(t.hourStep), function(e) {
        h = parseInt(e, 10)
    });
    var v = r.minuteStep;
    t.minuteStep && e.$parent.$watch(n(t.minuteStep), function(e) {
        v = parseInt(e, 10)
    }), e.showMeridian = r.showMeridian, t.showMeridian && e.$parent.$watch(n(t.showMeridian), function(t) {
        if (e.showMeridian = !!t, g.$error.time) {
            var n = o(),
                a = l();
            angular.isDefined(n) && angular.isDefined(a) && (f.setHours(n), c())
        } else p()
    }), this.setupMousewheelEvents = function(t, n) {
        var a = function(e) {
            e.originalEvent && (e = e.originalEvent);
            var t = e.wheelDelta ? e.wheelDelta : -e.deltaY;
            return e.detail || t > 0
        };
        t.bind("mousewheel wheel", function(t) {
            e.$apply(a(t) ? e.incrementHours() : e.decrementHours()), t.preventDefault()
        }), n.bind("mousewheel wheel", function(t) {
            e.$apply(a(t) ? e.incrementMinutes() : e.decrementMinutes()), t.preventDefault()
        })
    }, this.setupInputEvents = function(t, n) {
        if (e.readonlyInput) return e.updateHours = angular.noop, void(e.updateMinutes = angular.noop);
        var a = function(t, n) {
            g.$setViewValue(null), g.$setValidity("time", !1), angular.isDefined(t) && (e.invalidHours = t), angular.isDefined(n) && (e.invalidMinutes = n)
        };
        e.updateHours = function() {
            var e = o();
            angular.isDefined(e) ? (f.setHours(e), c("h")) : a(!0)
        }, t.bind("blur", function(t) {
            !e.invalidHours && e.hours < 10 && e.$apply(function() {
                e.hours = s(e.hours)
            })
        }), e.updateMinutes = function() {
            var e = l();
            angular.isDefined(e) ? (f.setMinutes(e), c("m")) : a(void 0, !0)
        }, n.bind("blur", function(t) {
            !e.invalidMinutes && e.minutes < 10 && e.$apply(function() {
                e.minutes = s(e.minutes)
            })
        })
    }, this.render = function() {
        var e = g.$modelValue ? new Date(g.$modelValue) : null;
        isNaN(e) ? (g.$setValidity("time", !1), a.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')) : (e && (f = e), u(), p())
    }, e.incrementHours = function() {
        d(60 * h)
    }, e.decrementHours = function() {
        d(60 * -h)
    }, e.incrementMinutes = function() {
        d(v)
    }, e.decrementMinutes = function() {
        d(-v)
    }, e.toggleMeridian = function() {
        d(720 * (f.getHours() < 12 ? 1 : -1))
    }
}]).directive("timepicker", function() {
    return {
        restrict: "EA",
        require: ["timepicker", "?^ngModel"],
        controller: "TimepickerController",
        replace: !0,
        scope: {},
        templateUrl: "template/timepicker/timepicker.html",
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r && i.init(r, t.find("input"))
        }
    }
}), angular.module("ui.bootstrap.typeahead", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).factory("typeaheadParser", ["$parse", function(e) {
    var t = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;
    return {
        parse: function(n) {
            var a = n.match(t);
            if (!a) throw new Error('Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_" but got "' + n + '".');
            return {
                itemName: a[3],
                source: e(a[4]),
                viewMapper: e(a[2] || a[1]),
                modelMapper: e(a[1])
            }
        }
    }
}]).directive("typeahead", ["$compile", "$parse", "$q", "$timeout", "$document", "$position", "typeaheadParser", function(e, t, n, a, i, r, o) {
    var l = [9, 13, 27, 38, 40];
    return {
        require: "ngModel",
        link: function(s, c, u, p) {
            var d, f = s.$eval(u.typeaheadMinLength) || 1,
                g = s.$eval(u.typeaheadWaitMs) || 0,
                m = s.$eval(u.typeaheadEditable) !== !1,
                h = t(u.typeaheadLoading).assign || angular.noop,
                v = t(u.typeaheadOnSelect),
                $ = u.typeaheadInputFormatter ? t(u.typeaheadInputFormatter) : void 0,
                b = !!u.typeaheadAppendToBody && s.$eval(u.typeaheadAppendToBody),
                y = t(u.ngModel).assign,
                w = o.parse(u.typeahead),
                D = s.$new();
            s.$on("$destroy", function() {
                D.$destroy()
            });
            var k = "typeahead-" + D.$id + "-" + Math.floor(1e4 * Math.random());
            c.attr({
                "aria-autocomplete": "list",
                "aria-expanded": !1,
                "aria-owns": k
            });
            var M = angular.element("<div typeahead-popup></div>");
            M.attr({
                id: k,
                matches: "matches",
                active: "activeIdx",
                select: "select(activeIdx)",
                query: "query",
                position: "position"
            }), angular.isDefined(u.typeaheadTemplateUrl) && M.attr("template-url", u.typeaheadTemplateUrl);
            var x = function() {
                    D.matches = [], D.activeIdx = -1, c.attr("aria-expanded", !1)
                },
                T = function(e) {
                    return k + "-option-" + e
                };
            D.$watch("activeIdx", function(e) {
                e < 0 ? c.removeAttr("aria-activedescendant") : c.attr("aria-activedescendant", T(e))
            });
            var E = function(e) {
                var t = {
                    $viewValue: e
                };
                h(s, !0), n.when(w.source(s, t)).then(function(n) {
                    var a = e === p.$viewValue;
                    if (a && d)
                        if (n.length > 0) {
                            D.activeIdx = 0, D.matches.length = 0;
                            for (var i = 0; i < n.length; i++) t[w.itemName] = n[i], D.matches.push({
                                id: T(i),
                                label: w.viewMapper(D, t),
                                model: n[i]
                            });
                            D.query = e, D.position = b ? r.offset(c) : r.position(c), D.position.top = D.position.top + c.prop("offsetHeight"), c.attr("aria-expanded", !0)
                        } else x();
                    a && h(s, !1)
                }, function() {
                    x(), h(s, !1)
                })
            };
            x(), D.query = void 0;
            var C, O = function(e) {
                    C = a(function() {
                        E(e)
                    }, g)
                },
                A = function() {
                    C && a.cancel(C)
                };
            p.$parsers.unshift(function(e) {
                return d = !0, e && e.length >= f ? g > 0 ? (A(), O(e)) : E(e) : (h(s, !1), A(), x()), m ? e : e ? void p.$setValidity("editable", !1) : (p.$setValidity("editable", !0), e)
            }), p.$formatters.push(function(e) {
                var t, n, a = {};
                return $ ? (a.$model = e, $(s, a)) : (a[w.itemName] = e, t = w.viewMapper(s, a), a[w.itemName] = void 0, n = w.viewMapper(s, a), t !== n ? t : e)
            }), D.select = function(e) {
                var t, n, i = {};
                i[w.itemName] = n = D.matches[e].model, t = w.modelMapper(s, i), y(s, t), p.$setValidity("editable", !0), v(s, {
                    $item: n,
                    $model: t,
                    $label: w.viewMapper(s, i)
                }), x(), a(function() {
                    c[0].focus()
                }, 0, !1)
            }, c.bind("keydown", function(e) {
                0 !== D.matches.length && l.indexOf(e.which) !== -1 && (e.preventDefault(), 40 === e.which ? (D.activeIdx = (D.activeIdx + 1) % D.matches.length, D.$digest()) : 38 === e.which ? (D.activeIdx = (D.activeIdx ? D.activeIdx : D.matches.length) - 1, D.$digest()) : 13 === e.which || 9 === e.which ? D.$apply(function() {
                    D.select(D.activeIdx)
                }) : 27 === e.which && (e.stopPropagation(), x(), D.$digest()))
            }), c.bind("blur", function(e) {
                d = !1
            });
            var S = function(e) {
                c[0] !== e.target && (x(), D.$digest())
            };
            i.bind("click", S), s.$on("$destroy", function() {
                i.unbind("click", S)
            });
            var V = e(M)(D);
            b ? i.find("body").append(V) : c.after(V)
        }
    }
}]).directive("typeaheadPopup", function() {
    return {
        restrict: "EA",
        scope: {
            matches: "=",
            query: "=",
            active: "=",
            position: "=",
            select: "&"
        },
        replace: !0,
        templateUrl: "template/typeahead/typeahead-popup.html",
        link: function(e, t, n) {
            e.templateUrl = n.templateUrl, e.isOpen = function() {
                return e.matches.length > 0
            }, e.isActive = function(t) {
                return e.active == t
            }, e.selectActive = function(t) {
                e.active = t
            }, e.selectMatch = function(t) {
                e.select({
                    activeIdx: t
                })
            }
        }
    }
}).directive("typeaheadMatch", ["$http", "$templateCache", "$compile", "$parse", function(e, t, n, a) {
    return {
        restrict: "EA",
        scope: {
            index: "=",
            match: "=",
            query: "="
        },
        link: function(i, r, o) {
            var l = a(o.templateUrl)(i.$parent) || "template/typeahead/typeahead-match.html";
            e.get(l, {
                cache: t
            }).success(function(e) {
                r.replaceWith(n(e.trim())(i))
            })
        }
    }
}]).filter("typeaheadHighlight", function() {
    function e(e) {
        return e.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
    }
    return function(t, n) {
        return n ? ("" + t).replace(new RegExp(e(n), "gi"), "<strong>$&</strong>") : t
    }
});
angular.module("ui.bootstrap", ["ui.bootstrap.tpls", "ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.dateparser", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdown", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead"]), angular.module("ui.bootstrap.tpls", ["template/accordion/accordion-group.html", "template/accordion/accordion.html", "template/alert/alert.html", "template/carousel/carousel.html", "template/carousel/slide.html", "template/datepicker/datepicker.html", "template/datepicker/day.html", "template/datepicker/month.html", "template/datepicker/popup.html", "template/datepicker/year.html", "template/modal/backdrop.html", "template/modal/window.html", "template/pagination/pager.html", "template/pagination/pagination.html", "template/tooltip/tooltip-html-unsafe-popup.html", "template/tooltip/tooltip-popup.html", "template/popover/popover.html", "template/progressbar/bar.html", "template/progressbar/progress.html", "template/progressbar/progressbar.html", "template/rating/rating.html", "template/tabs/tab.html", "template/tabs/tabset.html", "template/timepicker/timepicker.html", "template/typeahead/typeahead-match.html", "template/typeahead/typeahead-popup.html"]), angular.module("ui.bootstrap.transition", []).factory("$transition", ["$q", "$timeout", "$rootScope", function(e, t, n) {
    function a(e) {
        for (var t in e)
            if (void 0 !== r.style[t]) return e[t]
    }
    var i = function(a, r, o) {
            o = o || {};
            var l = e.defer(),
                s = i[o.animation ? "animationEndEventName" : "transitionEndEventName"],
                c = function(e) {
                    n.$apply(function() {
                        a.unbind(s, c), l.resolve(a)
                    })
                };
            return s && a.bind(s, c), t(function() {
                angular.isString(r) ? a.addClass(r) : angular.isFunction(r) ? r(a) : angular.isObject(r) && a.css(r), s || l.resolve(a)
            }), l.promise.cancel = function() {
                s && a.unbind(s, c), l.reject("Transition cancelled")
            }, l.promise
        },
        r = document.createElement("trans"),
        o = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            transition: "transitionend"
        },
        l = {
            WebkitTransition: "webkitAnimationEnd",
            MozTransition: "animationend",
            OTransition: "oAnimationEnd",
            transition: "animationend"
        };
    return i.transitionEndEventName = a(o), i.animationEndEventName = a(l), i
}]), angular.module("ui.bootstrap.collapse", ["ui.bootstrap.transition"]).directive("collapse", ["$transition", function(e) {
    return {
        link: function(t, n, a) {
            function i(t) {
                function a() {
                    c === i && (c = void 0)
                }
                var i = e(n, t);
                return c && c.cancel(), c = i, i.then(a, a), i
            }

            function r() {
                u ? (u = !1, o()) : (n.removeClass("collapse").addClass("collapsing"), i({
                    height: n[0].scrollHeight + "px"
                }).then(o))
            }

            function o() {
                n.removeClass("collapsing"), n.addClass("collapse in"), n.css({
                    height: "auto"
                })
            }

            function l() {
                if (u) u = !1, s(), n.css({
                    height: 0
                });
                else {
                    n.css({
                        height: n[0].scrollHeight + "px"
                    });
                    n[0].offsetWidth;
                    n.removeClass("collapse in").addClass("collapsing"), i({
                        height: 0
                    }).then(s)
                }
            }

            function s() {
                n.removeClass("collapsing"), n.addClass("collapse")
            }
            var c, u = !0;
            t.$watch(a.collapse, function(e) {
                e ? l() : r()
            })
        }
    }
}]), angular.module("ui.bootstrap.accordion", ["ui.bootstrap.collapse"]).constant("accordionConfig", {
    closeOthers: !0
}).controller("AccordionController", ["$scope", "$attrs", "accordionConfig", function(e, t, n) {
    this.groups = [], this.closeOthers = function(a) {
        var i = angular.isDefined(t.closeOthers) ? e.$eval(t.closeOthers) : n.closeOthers;
        i && angular.forEach(this.groups, function(e) {
            e !== a && (e.isOpen = !1)
        })
    }, this.addGroup = function(e) {
        var t = this;
        this.groups.push(e), e.$on("$destroy", function(n) {
            t.removeGroup(e)
        })
    }, this.removeGroup = function(e) {
        var t = this.groups.indexOf(e);
        t !== -1 && this.groups.splice(t, 1)
    }
}]).directive("accordion", function() {
    return {
        restrict: "EA",
        controller: "AccordionController",
        transclude: !0,
        replace: !1,
        templateUrl: "template/accordion/accordion.html"
    }
}).directive("accordionGroup", function() {
    return {
        require: "^accordion",
        restrict: "EA",
        transclude: !0,
        replace: !0,
        templateUrl: "template/accordion/accordion-group.html",
        scope: {
            heading: "@",
            isOpen: "=?",
            isDisabled: "=?"
        },
        controller: function() {
            this.setHeading = function(e) {
                this.heading = e
            }
        },
        link: function(e, t, n, a) {
            a.addGroup(e), e.$watch("isOpen", function(t) {
                t && a.closeOthers(e)
            }), e.toggleOpen = function() {
                e.isDisabled || (e.isOpen = !e.isOpen)
            }
        }
    }
}).directive("accordionHeading", function() {
    return {
        restrict: "EA",
        transclude: !0,
        template: "",
        replace: !0,
        require: "^accordionGroup",
        link: function(e, t, n, a, i) {
            a.setHeading(i(e, function() {}))
        }
    }
}).directive("accordionTransclude", function() {
    return {
        require: "^accordionGroup",
        link: function(e, t, n, a) {
            e.$watch(function() {
                return a[n.accordionTransclude]
            }, function(e) {
                e && (t.html(""), t.append(e))
            })
        }
    }
}), angular.module("ui.bootstrap.alert", []).controller("AlertController", ["$scope", "$attrs", function(e, t) {
    e.closeable = "close" in t
}]).directive("alert", function() {
    return {
        restrict: "EA",
        controller: "AlertController",
        templateUrl: "template/alert/alert.html",
        transclude: !0,
        replace: !0,
        scope: {
            type: "@",
            close: "&"
        }
    }
}), angular.module("ui.bootstrap.bindHtml", []).directive("bindHtmlUnsafe", function() {
    return function(e, t, n) {
        t.addClass("ng-binding").data("$binding", n.bindHtmlUnsafe), e.$watch(n.bindHtmlUnsafe, function(e) {
            t.html(e || "")
        })
    }
}), angular.module("ui.bootstrap.buttons", []).constant("buttonConfig", {
    activeClass: "active",
    toggleEvent: "click"
}).controller("ButtonsController", ["buttonConfig", function(e) {
    this.activeClass = e.activeClass || "active", this.toggleEvent = e.toggleEvent || "click"
}]).directive("btnRadio", function() {
    return {
        require: ["btnRadio", "ngModel"],
        controller: "ButtonsController",
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r.$render = function() {
                t.toggleClass(i.activeClass, angular.equals(r.$modelValue, e.$eval(n.btnRadio)))
            }, t.bind(i.toggleEvent, function() {
                var a = t.hasClass(i.activeClass);
                a && !angular.isDefined(n.uncheckable) || e.$apply(function() {
                    r.$setViewValue(a ? null : e.$eval(n.btnRadio)), r.$render()
                })
            })
        }
    }
}).directive("btnCheckbox", function() {
    return {
        require: ["btnCheckbox", "ngModel"],
        controller: "ButtonsController",
        link: function(e, t, n, a) {
            function i() {
                return o(n.btnCheckboxTrue, !0)
            }

            function r() {
                return o(n.btnCheckboxFalse, !1)
            }

            function o(t, n) {
                var a = e.$eval(t);
                return angular.isDefined(a) ? a : n
            }
            var l = a[0],
                s = a[1];
            s.$render = function() {
                t.toggleClass(l.activeClass, angular.equals(s.$modelValue, i()))
            }, t.bind(l.toggleEvent, function() {
                e.$apply(function() {
                    s.$setViewValue(t.hasClass(l.activeClass) ? r() : i()), s.$render()
                })
            })
        }
    }
}), angular.module("ui.bootstrap.carousel", ["ui.bootstrap.transition"]).controller("CarouselController", ["$scope", "$timeout", "$transition", function(e, t, n) {
    function a() {
        i();
        var n = +e.interval;
        !isNaN(n) && n >= 0 && (o = t(r, n))
    }

    function i() {
        o && (t.cancel(o), o = null)
    }

    function r() {
        l ? (e.next(), a()) : e.pause()
    }
    var o, l, s = this,
        c = s.slides = e.slides = [],
        u = -1;
    s.currentSlide = null;
    var p = !1;
    s.select = e.select = function(i, r) {
        function o() {
            if (!p) {
                if (s.currentSlide && angular.isString(r) && !e.noTransition && i.$element) {
                    i.$element.addClass(r);
                    i.$element[0].offsetWidth;
                    angular.forEach(c, function(e) {
                            angular.extend(e, {
                                direction: "",
                                entering: !1,
                                leaving: !1,
                                active: !1
                            })
                        }), angular.extend(i, {
                            direction: r,
                            active: !0,
                            entering: !0
                        }), angular.extend(s.currentSlide || {}, {
                            direction: r,
                            leaving: !0
                        }), e.$currentTransition = n(i.$element, {}),
                        function(t, n) {
                            e.$currentTransition.then(function() {
                                l(t, n)
                            }, function() {
                                l(t, n)
                            })
                        }(i, s.currentSlide)
                } else l(i, s.currentSlide);
                s.currentSlide = i, u = d, a()
            }
        }

        function l(t, n) {
            angular.extend(t, {
                direction: "",
                active: !0,
                leaving: !1,
                entering: !1
            }), angular.extend(n || {}, {
                direction: "",
                active: !1,
                leaving: !1,
                entering: !1
            }), e.$currentTransition = null
        }
        var d = c.indexOf(i);
        void 0 === r && (r = d > u ? "next" : "prev"), i && i !== s.currentSlide && (e.$currentTransition ? (e.$currentTransition.cancel(), t(o)) : o())
    }, e.$on("$destroy", function() {
        p = !0
    }), s.indexOfSlide = function(e) {
        return c.indexOf(e)
    }, e.next = function() {
        var t = (u + 1) % c.length;
        if (!e.$currentTransition) return s.select(c[t], "next")
    }, e.prev = function() {
        var t = u - 1 < 0 ? c.length - 1 : u - 1;
        if (!e.$currentTransition) return s.select(c[t], "prev")
    }, e.isActive = function(e) {
        return s.currentSlide === e
    }, e.$watch("interval", a), e.$on("$destroy", i), e.play = function() {
        l || (l = !0, a())
    }, e.pause = function() {
        e.noPause || (l = !1, i())
    }, s.addSlide = function(t, n) {
        t.$element = n, c.push(t), 1 === c.length || t.active ? (s.select(c[c.length - 1]), 1 == c.length && e.play()) : t.active = !1
    }, s.removeSlide = function(e) {
        var t = c.indexOf(e);
        c.splice(t, 1), c.length > 0 && e.active ? t >= c.length ? s.select(c[t - 1]) : s.select(c[t]) : u > t && u--
    }
}]).directive("carousel", [function() {
    return {
        restrict: "EA",
        transclude: !0,
        replace: !0,
        controller: "CarouselController",
        require: "carousel",
        templateUrl: "template/carousel/carousel.html",
        scope: {
            interval: "=",
            noTransition: "=",
            noPause: "="
        }
    }
}]).directive("slide", function() {
    return {
        require: "^carousel",
        restrict: "EA",
        transclude: !0,
        replace: !0,
        templateUrl: "template/carousel/slide.html",
        scope: {
            active: "=?"
        },
        link: function(e, t, n, a) {
            a.addSlide(e, t), e.$on("$destroy", function() {
                a.removeSlide(e)
            }), e.$watch("active", function(t) {
                t && a.select(e)
            })
        }
    }
}), angular.module("ui.bootstrap.dateparser", []).service("dateParser", ["$locale", "orderByFilter", function(e, t) {
    function n(e) {
        var n = [],
            a = e.split("");
        return angular.forEach(i, function(t, i) {
            var r = e.indexOf(i);
            if (r > -1) {
                e = e.split(""), a[r] = "(" + t.regex + ")", e[r] = "$";
                for (var o = r + 1, l = r + i.length; o < l; o++) a[o] = "", e[o] = "$";
                e = e.join(""), n.push({
                    index: r,
                    apply: t.apply
                })
            }
        }), {
            regex: new RegExp("^" + a.join("") + "$"),
            map: t(n, "index")
        }
    }

    function a(e, t, n) {
        return 1 === t && n > 28 ? 29 === n && (e % 4 === 0 && e % 100 !== 0 || e % 400 === 0) : 3 !== t && 5 !== t && 8 !== t && 10 !== t || n < 31
    }
    this.parsers = {};
    var i = {
        yyyy: {
            regex: "\\d{4}",
            apply: function(e) {
                this.year = +e
            }
        },
        yy: {
            regex: "\\d{2}",
            apply: function(e) {
                this.year = +e + 2e3
            }
        },
        y: {
            regex: "\\d{1,4}",
            apply: function(e) {
                this.year = +e
            }
        },
        MMMM: {
            regex: e.DATETIME_FORMATS.MONTH.join("|"),
            apply: function(t) {
                this.month = e.DATETIME_FORMATS.MONTH.indexOf(t)
            }
        },
        MMM: {
            regex: e.DATETIME_FORMATS.SHORTMONTH.join("|"),
            apply: function(t) {
                this.month = e.DATETIME_FORMATS.SHORTMONTH.indexOf(t)
            }
        },
        MM: {
            regex: "0[1-9]|1[0-2]",
            apply: function(e) {
                this.month = e - 1
            }
        },
        M: {
            regex: "[1-9]|1[0-2]",
            apply: function(e) {
                this.month = e - 1
            }
        },
        dd: {
            regex: "[0-2][0-9]{1}|3[0-1]{1}",
            apply: function(e) {
                this.date = +e
            }
        },
        d: {
            regex: "[1-2]?[0-9]{1}|3[0-1]{1}",
            apply: function(e) {
                this.date = +e
            }
        },
        EEEE: {
            regex: e.DATETIME_FORMATS.DAY.join("|")
        },
        EEE: {
            regex: e.DATETIME_FORMATS.SHORTDAY.join("|")
        }
    };
    this.parse = function(t, i) {
        if (!angular.isString(t) || !i) return t;
        i = e.DATETIME_FORMATS[i] || i, this.parsers[i] || (this.parsers[i] = n(i));
        var r = this.parsers[i],
            o = r.regex,
            l = r.map,
            s = t.match(o);
        if (s && s.length) {
            for (var c, u = {
                    year: 1900,
                    month: 0,
                    date: 1,
                    hours: 0
                }, p = 1, d = s.length; p < d; p++) {
                var g = l[p - 1];
                g.apply && g.apply.call(u, s[p])
            }
            return a(u.year, u.month, u.date) && (c = new Date(u.year, u.month, u.date, u.hours)), c
        }
    }
}]), angular.module("ui.bootstrap.position", []).factory("$position", ["$document", "$window", function(e, t) {
    function n(e, n) {
        return e.currentStyle ? e.currentStyle[n] : t.getComputedStyle ? t.getComputedStyle(e)[n] : e.style[n]
    }

    function a(e) {
        return "static" === (n(e, "position") || "static")
    }
    var i = function(t) {
        for (var n = e[0], i = t.offsetParent || n; i && i !== n && a(i);) i = i.offsetParent;
        return i || n
    };
    return {
        position: function(t) {
            var n = this.offset(t),
                a = {
                    top: 0,
                    left: 0
                },
                r = i(t[0]);
            r != e[0] && (a = this.offset(angular.element(r)), a.top += r.clientTop - r.scrollTop, a.left += r.clientLeft - r.scrollLeft);
            var o = t[0].getBoundingClientRect();
            return {
                width: o.width || t.prop("offsetWidth"),
                height: o.height || t.prop("offsetHeight"),
                top: n.top - a.top,
                left: n.left - a.left
            }
        },
        offset: function(n) {
            var a = n[0].getBoundingClientRect();
            return {
                width: a.width || n.prop("offsetWidth"),
                height: a.height || n.prop("offsetHeight"),
                top: a.top + (t.pageYOffset || e[0].documentElement.scrollTop),
                left: a.left + (t.pageXOffset || e[0].documentElement.scrollLeft)
            }
        },
        positionElements: function(e, t, n, a) {
            var i, r, o, l, s = n.split("-"),
                c = s[0],
                u = s[1] || "center";
            i = a ? this.offset(e) : this.position(e), r = t.prop("offsetWidth"), o = t.prop("offsetHeight");
            var p = {
                    center: function() {
                        return i.left + i.width / 2 - r / 2
                    },
                    left: function() {
                        return i.left
                    },
                    right: function() {
                        return i.left + i.width
                    }
                },
                d = {
                    center: function() {
                        return i.top + i.height / 2 - o / 2
                    },
                    top: function() {
                        return i.top
                    },
                    bottom: function() {
                        return i.top + i.height
                    }
                };
            switch (c) {
                case "right":
                    l = {
                        top: d[u](),
                        left: p[c]()
                    };
                    break;
                case "left":
                    l = {
                        top: d[u](),
                        left: i.left - r
                    };
                    break;
                case "bottom":
                    l = {
                        top: d[c](),
                        left: p[u]()
                    };
                    break;
                default:
                    l = {
                        top: i.top - o,
                        left: p[u]()
                    }
            }
            return l
        }
    }
}]), angular.module("ui.bootstrap.datepicker", ["ui.bootstrap.dateparser", "ui.bootstrap.position"]).constant("datepickerConfig", {
    formatDay: "dd",
    formatMonth: "MMMM",
    formatYear: "yyyy",
    formatDayHeader: "EEE",
    formatDayTitle: "MMMM yyyy",
    formatMonthTitle: "yyyy",
    datepickerMode: "day",
    minMode: "day",
    maxMode: "year",
    showWeeks: !0,
    startingDay: 0,
    yearRange: 20,
    minDate: null,
    maxDate: null
}).controller("DatepickerController", ["$scope", "$attrs", "$parse", "$interpolate", "$timeout", "$log", "dateFilter", "datepickerConfig", function(e, t, n, a, i, r, o, l) {
    var s = this,
        c = {
            $setViewValue: angular.noop
        };
    this.modes = ["day", "month", "year"], angular.forEach(["formatDay", "formatMonth", "formatYear", "formatDayHeader", "formatDayTitle", "formatMonthTitle", "minMode", "maxMode", "showWeeks", "startingDay", "yearRange"], function(n, i) {
        s[n] = angular.isDefined(t[n]) ? i < 8 ? a(t[n])(e.$parent) : e.$parent.$eval(t[n]) : l[n]
    }), angular.forEach(["minDate", "maxDate"], function(a) {
        t[a] ? e.$parent.$watch(n(t[a]), function(e) {
            s[a] = e ? new Date(e) : null, s.refreshView()
        }) : s[a] = l[a] ? new Date(l[a]) : null
    }), e.datepickerMode = e.datepickerMode || l.datepickerMode, e.uniqueId = "datepicker-" + e.$id + "-" + Math.floor(1e4 * Math.random()), this.activeDate = angular.isDefined(t.initDate) ? e.$parent.$eval(t.initDate) : new Date, e.isActive = function(t) {
        return 0 === s.compare(t.date, s.activeDate) && (e.activeDateId = t.uid, !0)
    }, this.init = function(e) {
        c = e, c.$render = function() {
            s.render()
        }
    }, this.render = function() {
        if (c.$modelValue) {
            var e = new Date(c.$modelValue),
                t = !isNaN(e);
            t ? this.activeDate = e : r.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.'), c.$setValidity("date", t)
        }
        this.refreshView()
    }, this.refreshView = function() {
        if (this.element) {
            this._refreshView();
            var e = c.$modelValue ? new Date(c.$modelValue) : null;
            c.$setValidity("date-disabled", !e || this.element && !this.isDisabled(e))
        }
    }, this.createDateObject = function(e, t) {
        var n = c.$modelValue ? new Date(c.$modelValue) : null;
        return {
            date: e,
            label: o(e, t),
            selected: n && 0 === this.compare(e, n),
            disabled: this.isDisabled(e),
            current: 0 === this.compare(e, new Date)
        }
    }, this.isDisabled = function(n) {
        return this.minDate && this.compare(n, this.minDate) < 0 || this.maxDate && this.compare(n, this.maxDate) > 0 || t.dateDisabled && e.dateDisabled({
            date: n,
            mode: e.datepickerMode
        })
    }, this.split = function(e, t) {
        for (var n = []; e.length > 0;) n.push(e.splice(0, t));
        return n
    }, e.select = function(t) {
        if (e.datepickerMode === s.minMode) {
            var n = c.$modelValue ? new Date(c.$modelValue) : new Date(0, 0, 0, 0, 0, 0, 0);
            n.setFullYear(t.getFullYear(), t.getMonth(), t.getDate()), c.$setViewValue(n), c.$render()
        } else s.activeDate = t, e.datepickerMode = s.modes[s.modes.indexOf(e.datepickerMode) - 1]
    }, e.move = function(e) {
        var t = s.activeDate.getFullYear() + e * (s.step.years || 0),
            n = s.activeDate.getMonth() + e * (s.step.months || 0);
        s.activeDate.setFullYear(t, n, 1), s.refreshView()
    }, e.toggleMode = function(t) {
        t = t || 1, e.datepickerMode === s.maxMode && 1 === t || e.datepickerMode === s.minMode && t === -1 || (e.datepickerMode = s.modes[s.modes.indexOf(e.datepickerMode) + t])
    }, e.keys = {
        13: "enter",
        32: "space",
        33: "pageup",
        34: "pagedown",
        35: "end",
        36: "home",
        37: "left",
        38: "up",
        39: "right",
        40: "down"
    };
    var u = function() {
        i(function() {
            s.element[0].focus()
        }, 0, !1)
    };
    e.$on("datepicker.focus", u), e.keydown = function(t) {
        var n = e.keys[t.which];
        if (n && !t.shiftKey && !t.altKey)
            if (t.preventDefault(), t.stopPropagation(), "enter" === n || "space" === n) {
                if (s.isDisabled(s.activeDate)) return;
                e.select(s.activeDate), u()
            } else !t.ctrlKey || "up" !== n && "down" !== n ? (s.handleKeyDown(n, t), s.refreshView()) : (e.toggleMode("up" === n ? 1 : -1), u())
    }
}]).directive("datepicker", function() {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/datepicker.html",
        scope: {
            datepickerMode: "=?",
            dateDisabled: "&"
        },
        require: ["datepicker", "?^ngModel"],
        controller: "DatepickerController",
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r && i.init(r)
        }
    }
}).directive("daypicker", ["dateFilter", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/day.html",
        require: "^datepicker",
        link: function(t, n, a, i) {
            function r(e, t) {
                return 1 !== t || e % 4 !== 0 || e % 100 === 0 && e % 400 !== 0 ? s[t] : 29
            }

            function o(e, t) {
                var n = new Array(t),
                    a = new Date(e),
                    i = 0;
                for (a.setHours(12); i < t;) n[i++] = new Date(a), a.setDate(a.getDate() + 1);
                return n
            }

            function l(e) {
                var t = new Date(e);
                t.setDate(t.getDate() + 4 - (t.getDay() || 7));
                var n = t.getTime();
                return t.setMonth(0), t.setDate(1), Math.floor(Math.round((n - t) / 864e5) / 7) + 1
            }
            t.showWeeks = i.showWeeks, i.step = {
                months: 1
            }, i.element = n;
            var s = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            i._refreshView = function() {
                var n = i.activeDate.getFullYear(),
                    a = i.activeDate.getMonth(),
                    r = new Date(n, a, 1),
                    s = i.startingDay - r.getDay(),
                    c = s > 0 ? 7 - s : -s,
                    u = new Date(r);
                c > 0 && u.setDate(-c + 1);
                for (var p = o(u, 42), d = 0; d < 42; d++) p[d] = angular.extend(i.createDateObject(p[d], i.formatDay), {
                    secondary: p[d].getMonth() !== a,
                    uid: t.uniqueId + "-" + d
                });
                t.labels = new Array(7);
                for (var g = 0; g < 7; g++) t.labels[g] = {
                    abbr: e(p[g].date, i.formatDayHeader),
                    full: e(p[g].date, "EEEE")
                };
                if (t.title = e(i.activeDate, i.formatDayTitle), t.rows = i.split(p, 7), t.showWeeks) {
                    t.weekNumbers = [];
                    for (var m = l(t.rows[0][0].date), f = t.rows.length; t.weekNumbers.push(m++) < f;);
                }
            }, i.compare = function(e, t) {
                return new Date(e.getFullYear(), e.getMonth(), e.getDate()) - new Date(t.getFullYear(), t.getMonth(), t.getDate())
            }, i.handleKeyDown = function(e, t) {
                var n = i.activeDate.getDate();
                if ("left" === e) n -= 1;
                else if ("up" === e) n -= 7;
                else if ("right" === e) n += 1;
                else if ("down" === e) n += 7;
                else if ("pageup" === e || "pagedown" === e) {
                    var a = i.activeDate.getMonth() + ("pageup" === e ? -1 : 1);
                    i.activeDate.setMonth(a, 1), n = Math.min(r(i.activeDate.getFullYear(), i.activeDate.getMonth()), n)
                } else "home" === e ? n = 1 : "end" === e && (n = r(i.activeDate.getFullYear(), i.activeDate.getMonth()));
                i.activeDate.setDate(n)
            }, i.refreshView()
        }
    }
}]).directive("monthpicker", ["dateFilter", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/month.html",
        require: "^datepicker",
        link: function(t, n, a, i) {
            i.step = {
                years: 1
            }, i.element = n, i._refreshView = function() {
                for (var n = new Array(12), a = i.activeDate.getFullYear(), r = 0; r < 12; r++) n[r] = angular.extend(i.createDateObject(new Date(a, r, 1), i.formatMonth), {
                    uid: t.uniqueId + "-" + r
                });
                t.title = e(i.activeDate, i.formatMonthTitle), t.rows = i.split(n, 3)
            }, i.compare = function(e, t) {
                return new Date(e.getFullYear(), e.getMonth()) - new Date(t.getFullYear(), t.getMonth())
            }, i.handleKeyDown = function(e, t) {
                var n = i.activeDate.getMonth();
                if ("left" === e) n -= 1;
                else if ("up" === e) n -= 3;
                else if ("right" === e) n += 1;
                else if ("down" === e) n += 3;
                else if ("pageup" === e || "pagedown" === e) {
                    var a = i.activeDate.getFullYear() + ("pageup" === e ? -1 : 1);
                    i.activeDate.setFullYear(a)
                } else "home" === e ? n = 0 : "end" === e && (n = 11);
                i.activeDate.setMonth(n)
            }, i.refreshView()
        }
    }
}]).directive("yearpicker", ["dateFilter", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/year.html",
        require: "^datepicker",
        link: function(e, t, n, a) {
            function i(e) {
                return parseInt((e - 1) / r, 10) * r + 1
            }
            var r = a.yearRange;
            a.step = {
                years: r
            }, a.element = t, a._refreshView = function() {
                for (var t = new Array(r), n = 0, o = i(a.activeDate.getFullYear()); n < r; n++) t[n] = angular.extend(a.createDateObject(new Date(o + n, 0, 1), a.formatYear), {
                    uid: e.uniqueId + "-" + n
                });
                e.title = [t[0].label, t[r - 1].label].join(" - "), e.rows = a.split(t, 5)
            }, a.compare = function(e, t) {
                return e.getFullYear() - t.getFullYear()
            }, a.handleKeyDown = function(e, t) {
                var n = a.activeDate.getFullYear();
                "left" === e ? n -= 1 : "up" === e ? n -= 5 : "right" === e ? n += 1 : "down" === e ? n += 5 : "pageup" === e || "pagedown" === e ? n += ("pageup" === e ? -1 : 1) * a.step.years : "home" === e ? n = i(a.activeDate.getFullYear()) : "end" === e && (n = i(a.activeDate.getFullYear()) + r - 1), a.activeDate.setFullYear(n)
            }, a.refreshView()
        }
    }
}]).constant("datepickerPopupConfig", {
    datepickerPopup: "yyyy-MM-dd",
    currentText: "Today",
    clearText: "Clear",
    closeText: "Done",
    closeOnDateSelection: !0,
    appendToBody: !1,
    showButtonBar: !0
}).directive("datepickerPopup", ["$compile", "$parse", "$document", "$position", "dateFilter", "dateParser", "datepickerPopupConfig", function(e, t, n, a, i, r, o) {
    return {
        restrict: "EA",
        require: "ngModel",
        scope: {
            isOpen: "=?",
            currentText: "@",
            clearText: "@",
            closeText: "@",
            dateDisabled: "&"
        },
        link: function(l, s, c, u) {
            function p(e) {
                return e.replace(/([A-Z])/g, function(e) {
                    return "-" + e.toLowerCase()
                })
            }

            function d(e) {
                if (e) {
                    if (angular.isDate(e) && !isNaN(e)) return u.$setValidity("date", !0), e;
                    if (angular.isString(e)) {
                        var t = r.parse(e, g) || new Date(e);
                        return isNaN(t) ? void u.$setValidity("date", !1) : (u.$setValidity("date", !0), t)
                    }
                    return void u.$setValidity("date", !1)
                }
                return u.$setValidity("date", !0), null
            }
            var g, m = angular.isDefined(c.closeOnDateSelection) ? l.$parent.$eval(c.closeOnDateSelection) : o.closeOnDateSelection,
                f = angular.isDefined(c.datepickerAppendToBody) ? l.$parent.$eval(c.datepickerAppendToBody) : o.appendToBody;
            l.showButtonBar = angular.isDefined(c.showButtonBar) ? l.$parent.$eval(c.showButtonBar) : o.showButtonBar, l.getText = function(e) {
                return l[e + "Text"] || o[e + "Text"]
            }, c.$observe("datepickerPopup", function(e) {
                g = e || o.datepickerPopup, u.$render()
            });
            var h = angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");
            h.attr({
                "ng-model": "date",
                "ng-change": "dateSelection()"
            });
            var v = angular.element(h.children()[0]);
            c.datepickerOptions && angular.forEach(l.$parent.$eval(c.datepickerOptions), function(e, t) {
                v.attr(p(t), e)
            }), l.watchData = {}, angular.forEach(["minDate", "maxDate", "datepickerMode"], function(e) {
                if (c[e]) {
                    var n = t(c[e]);
                    if (l.$parent.$watch(n, function(t) {
                            l.watchData[e] = t
                        }), v.attr(p(e), "watchData." + e), "datepickerMode" === e) {
                        var a = n.assign;
                        l.$watch("watchData." + e, function(e, t) {
                            e !== t && a(l.$parent, e)
                        })
                    }
                }
            }), c.dateDisabled && v.attr("date-disabled", "dateDisabled({ date: date, mode: mode })"), u.$parsers.unshift(d), l.dateSelection = function(e) {
                angular.isDefined(e) && (l.date = e), u.$setViewValue(l.date), u.$render(), m && (l.isOpen = !1, s[0].focus())
            }, s.bind("input change keyup", function() {
                l.$apply(function() {
                    l.date = u.$modelValue
                })
            }), u.$render = function() {
                var e = u.$viewValue ? i(u.$viewValue, g) : "";
                s.val(e), l.date = d(u.$modelValue)
            };
            var b = function(e) {
                    l.isOpen && e.target !== s[0] && l.$apply(function() {
                        l.isOpen = !1
                    })
                },
                $ = function(e, t) {
                    l.keydown(e)
                };
            s.bind("keydown", $), l.keydown = function(e) {
                27 === e.which ? (e.preventDefault(), e.stopPropagation(), l.close()) : 40 !== e.which || l.isOpen || (l.isOpen = !0)
            }, l.$watch("isOpen", function(e) {
                e ? (l.$broadcast("datepicker.focus"), l.position = f ? a.offset(s) : a.position(s), l.position.top = l.position.top + s.prop("offsetHeight"), n.bind("click", b)) : n.unbind("click", b)
            }), l.select = function(e) {
                if ("today" === e) {
                    var t = new Date;
                    angular.isDate(u.$modelValue) ? (e = new Date(u.$modelValue), e.setFullYear(t.getFullYear(), t.getMonth(), t.getDate())) : e = new Date(t.setHours(0, 0, 0, 0))
                }
                l.dateSelection(e)
            }, l.close = function() {
                l.isOpen = !1, s[0].focus()
            };
            var y = e(h)(l);
            h.remove(), f ? n.find("body").append(y) : s.after(y), l.$on("$destroy", function() {
                y.remove(), s.unbind("keydown", $), n.unbind("click", b)
            })
        }
    }
}]).directive("datepickerPopupWrap", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        templateUrl: "template/datepicker/popup.html",
        link: function(e, t, n) {
            t.bind("click", function(e) {
                e.preventDefault(), e.stopPropagation()
            })
        }
    }
}), angular.module("ui.bootstrap.dropdown", []).constant("dropdownConfig", {
    openClass: "open"
}).service("dropdownService", ["$document", function(e) {
    var t = null;
    this.open = function(i) {
        t || (e.bind("click", n), e.bind("keydown", a)), t && t !== i && (t.isOpen = !1), t = i
    }, this.close = function(i) {
        t === i && (t = null, e.unbind("click", n), e.unbind("keydown", a))
    };
    var n = function(e) {
            var n = t.getToggleElement();
            e && n && n[0].contains(e.target) || t.$apply(function() {
                t.isOpen = !1
            })
        },
        a = function(e) {
            27 === e.which && (t.focusToggleElement(), n())
        }
}]).controller("DropdownController", ["$scope", "$attrs", "$parse", "dropdownConfig", "dropdownService", "$animate", function(e, t, n, a, i, r) {
    var o, l = this,
        s = e.$new(),
        c = a.openClass,
        u = angular.noop,
        p = t.onToggle ? n(t.onToggle) : angular.noop;
    this.init = function(a) {
        l.$element = a, t.isOpen && (o = n(t.isOpen), u = o.assign, e.$watch(o, function(e) {
            s.isOpen = !!e
        }))
    }, this.toggle = function(e) {
        return s.isOpen = arguments.length ? !!e : !s.isOpen
    }, this.isOpen = function() {
        return s.isOpen
    }, s.getToggleElement = function() {
        return l.toggleElement
    }, s.focusToggleElement = function() {
        l.toggleElement && l.toggleElement[0].focus()
    }, s.$watch("isOpen", function(t, n) {
        r[t ? "addClass" : "removeClass"](l.$element, c), t ? (s.focusToggleElement(), i.open(s)) : i.close(s), u(e, t), angular.isDefined(t) && t !== n && p(e, {
            open: !!t
        })
    }), e.$on("$locationChangeSuccess", function() {
        s.isOpen = !1
    }), e.$on("$destroy", function() {
        s.$destroy()
    })
}]).directive("dropdown", function() {
    return {
        restrict: "CA",
        controller: "DropdownController",
        link: function(e, t, n, a) {
            a.init(t)
        }
    }
}).directive("dropdownToggle", function() {
    return {
        restrict: "CA",
        require: "?^dropdown",
        link: function(e, t, n, a) {
            if (a) {
                a.toggleElement = t;
                var i = function(i) {
                    i.preventDefault(), t.hasClass("disabled") || n.disabled || e.$apply(function() {
                        a.toggle()
                    })
                };
                t.bind("click", i), t.attr({
                    "aria-haspopup": !0,
                    "aria-expanded": !1
                }), e.$watch(a.isOpen, function(e) {
                    t.attr("aria-expanded", !!e)
                }), e.$on("$destroy", function() {
                    t.unbind("click", i)
                })
            }
        }
    }
}), angular.module("ui.bootstrap.modal", ["ui.bootstrap.transition"]).factory("$$stackedMap", function() {
    return {
        createNew: function() {
            var e = [];
            return {
                add: function(t, n) {
                    e.push({
                        key: t,
                        value: n
                    })
                },
                get: function(t) {
                    for (var n = 0; n < e.length; n++)
                        if (t == e[n].key) return e[n]
                },
                keys: function() {
                    for (var t = [], n = 0; n < e.length; n++) t.push(e[n].key);
                    return t
                },
                top: function() {
                    return e[e.length - 1]
                },
                remove: function(t) {
                    for (var n = -1, a = 0; a < e.length; a++)
                        if (t == e[a].key) {
                            n = a;
                            break
                        }
                    return e.splice(n, 1)[0]
                },
                removeTop: function() {
                    return e.splice(e.length - 1, 1)[0]
                },
                length: function() {
                    return e.length
                }
            }
        }
    }
}).directive("modalBackdrop", ["$timeout", function(e) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/modal/backdrop.html",
        link: function(t, n, a) {
            t.backdropClass = a.backdropClass || "", t.animate = !1, e(function() {
                t.animate = !0
            })
        }
    }
}]).directive("modalWindow", ["$modalStack", "$timeout", function(e, t) {
    return {
        restrict: "EA",
        scope: {
            index: "@",
            animate: "="
        },
        replace: !0,
        transclude: !0,
        templateUrl: function(e, t) {
            return t.templateUrl || "template/modal/window.html"
        },
        link: function(n, a, i) {
            a.addClass(i.windowClass || ""), n.size = i.size, t(function() {
                n.animate = !0, a[0].querySelectorAll("[autofocus]").length || a[0].focus()
            }), n.close = function(t) {
                var n = e.getTop();
                n && n.value.backdrop && "static" != n.value.backdrop && t.target === t.currentTarget && (t.preventDefault(), t.stopPropagation(), e.dismiss(n.key, "backdrop click"))
            }
        }
    }
}]).directive("modalTransclude", function() {
    return {
        link: function(e, t, n, a, i) {
            i(e.$parent, function(e) {
                t.empty(), t.append(e)
            })
        }
    }
}).factory("$modalStack", ["$transition", "$timeout", "$document", "$compile", "$rootScope", "$$stackedMap", function(e, t, n, a, i, r) {
    function o() {
        for (var e = -1, t = g.keys(), n = 0; n < t.length; n++) g.get(t[n]).value.backdrop && (e = n);
        return e
    }

    function l(e) {
        var t = n.find("body").eq(0),
            a = g.get(e).value;
        g.remove(e), c(a.modalDomEl, a.modalScope, 300, function() {
            a.modalScope.$destroy(), t.toggleClass(d, g.length() > 0), s()
        })
    }

    function s() {
        if (u && o() == -1) {
            var e = p;
            c(u, p, 150, function() {
                e.$destroy(), e = null
            }), u = void 0, p = void 0
        }
    }

    function c(n, a, i, r) {
        function o() {
            o.done || (o.done = !0, n.remove(), r && r())
        }
        a.animate = !1;
        var l = e.transitionEndEventName;
        if (l) {
            var s = t(o, i);
            n.bind(l, function() {
                t.cancel(s), o(), a.$apply()
            })
        } else t(o)
    }
    var u, p, d = "modal-open",
        g = r.createNew(),
        m = {};
    return i.$watch(o, function(e) {
        p && (p.index = e)
    }), n.bind("keydown", function(e) {
        var t;
        27 === e.which && (t = g.top(), t && t.value.keyboard && (e.preventDefault(), i.$apply(function() {
            m.dismiss(t.key, "escape key press")
        })))
    }), m.open = function(e, t) {
        g.add(e, {
            deferred: t.deferred,
            modalScope: t.scope,
            backdrop: t.backdrop,
            keyboard: t.keyboard
        });
        var r = n.find("body").eq(0),
            l = o();
        if (l >= 0 && !u) {
            p = i.$new(!0), p.index = l;
            var s = angular.element("<div modal-backdrop></div>");
            s.attr("backdrop-class", t.backdropClass), u = a(s)(p), r.append(u)
        }
        var c = angular.element("<div modal-window></div>");
        c.attr({
            "template-url": t.windowTemplateUrl,
            "window-class": t.windowClass,
            size: t.size,
            index: g.length() - 1,
            animate: "animate"
        }).html(t.content);
        var m = a(c)(t.scope);
        g.top().value.modalDomEl = m, r.append(m), r.addClass(d)
    }, m.close = function(e, t) {
        var n = g.get(e);
        n && (n.value.deferred.resolve(t), l(e))
    }, m.dismiss = function(e, t) {
        var n = g.get(e);
        n && (n.value.deferred.reject(t), l(e))
    }, m.dismissAll = function(e) {
        for (var t = this.getTop(); t;) this.dismiss(t.key, e), t = this.getTop()
    }, m.getTop = function() {
        return g.top()
    }, m
}]).provider("$modal", function() {
    var e = {
        options: {
            backdrop: !0,
            keyboard: !0
        },
        $get: ["$injector", "$rootScope", "$q", "$http", "$templateCache", "$controller", "$modalStack", function(t, n, a, i, r, o, l) {
            function s(e) {
                return e.template ? a.when(e.template) : i.get(angular.isFunction(e.templateUrl) ? e.templateUrl() : e.templateUrl, {
                    cache: r
                }).then(function(e) {
                    return e.data
                })
            }

            function c(e) {
                var n = [];
                return angular.forEach(e, function(e) {
                    (angular.isFunction(e) || angular.isArray(e)) && n.push(a.when(t.invoke(e)))
                }), n
            }
            var u = {};
            return u.open = function(t) {
                var i = a.defer(),
                    r = a.defer(),
                    u = {
                        result: i.promise,
                        opened: r.promise,
                        close: function(e) {
                            l.close(u, e)
                        },
                        dismiss: function(e) {
                            l.dismiss(u, e)
                        }
                    };
                if (t = angular.extend({}, e.options, t), t.resolve = t.resolve || {}, !t.template && !t.templateUrl) throw new Error("One of template or templateUrl options is required.");
                var p = a.all([s(t)].concat(c(t.resolve)));
                return p.then(function(e) {
                    var a = (t.scope || n).$new();
                    a.$close = u.close, a.$dismiss = u.dismiss;
                    var r, s = {},
                        c = 1;
                    t.controller && (s.$scope = a, s.$modalInstance = u, angular.forEach(t.resolve, function(t, n) {
                        s[n] = e[c++]
                    }), r = o(t.controller, s), t.controllerAs && (a[t.controllerAs] = r)), l.open(u, {
                        scope: a,
                        deferred: i,
                        content: e[0],
                        backdrop: t.backdrop,
                        keyboard: t.keyboard,
                        backdropClass: t.backdropClass,
                        windowClass: t.windowClass,
                        windowTemplateUrl: t.windowTemplateUrl,
                        size: t.size
                    })
                }, function(e) {
                    i.reject(e)
                }), p.then(function() {
                    r.resolve(!0)
                }, function() {
                    r.reject(!1)
                }), u
            }, u
        }]
    };
    return e
}), angular.module("ui.bootstrap.pagination", []).controller("PaginationController", ["$scope", "$attrs", "$parse", function(e, t, n) {
    var a = this,
        i = {
            $setViewValue: angular.noop
        },
        r = t.numPages ? n(t.numPages).assign : angular.noop;
    this.init = function(r, o) {
        i = r, this.config = o, i.$render = function() {
            a.render()
        }, t.itemsPerPage ? e.$parent.$watch(n(t.itemsPerPage), function(t) {
            a.itemsPerPage = parseInt(t, 10), e.totalPages = a.calculateTotalPages()
        }) : this.itemsPerPage = o.itemsPerPage
    }, this.calculateTotalPages = function() {
        var t = this.itemsPerPage < 1 ? 1 : Math.ceil(e.totalItems / this.itemsPerPage);
        return Math.max(t || 0, 1)
    }, this.render = function() {
        e.page = parseInt(i.$viewValue, 10) || 1
    }, e.selectPage = function(t) {
        e.page !== t && t > 0 && t <= e.totalPages && (i.$setViewValue(t), i.$render())
    }, e.getText = function(t) {
        return e[t + "Text"] || a.config[t + "Text"]
    }, e.noPrevious = function() {
        return 1 === e.page
    }, e.noNext = function() {
        return e.page === e.totalPages
    }, e.$watch("totalItems", function() {
        e.totalPages = a.calculateTotalPages()
    }), e.$watch("totalPages", function(t) {
        r(e.$parent, t), e.page > t ? e.selectPage(t) : i.$render()
    })
}]).constant("paginationConfig", {
    itemsPerPage: 10,
    boundaryLinks: !1,
    directionLinks: !0,
    firstText: "First",
    previousText: "Previous",
    nextText: "Next",
    lastText: "Last",
    rotate: !0
}).directive("pagination", ["$parse", "paginationConfig", function(e, t) {
    return {
        restrict: "EA",
        scope: {
            totalItems: "=",
            firstText: "@",
            previousText: "@",
            nextText: "@",
            lastText: "@"
        },
        require: ["pagination", "?ngModel"],
        controller: "PaginationController",
        templateUrl: "template/pagination/pagination.html",
        replace: !0,
        link: function(n, a, i, r) {
            function o(e, t, n) {
                return {
                    number: e,
                    text: t,
                    active: n
                }
            }

            function l(e, t) {
                var n = [],
                    a = 1,
                    i = t,
                    r = angular.isDefined(u) && u < t;
                r && (p ? (a = Math.max(e - Math.floor(u / 2), 1), i = a + u - 1, i > t && (i = t, a = i - u + 1)) : (a = (Math.ceil(e / u) - 1) * u + 1, i = Math.min(a + u - 1, t)));
                for (var l = a; l <= i; l++) {
                    var s = o(l, l, l === e);
                    n.push(s)
                }
                if (r && !p) {
                    if (a > 1) {
                        var c = o(a - 1, "...", !1);
                        n.unshift(c)
                    }
                    if (i < t) {
                        var d = o(i + 1, "...", !1);
                        n.push(d)
                    }
                }
                return n
            }
            var s = r[0],
                c = r[1];
            if (c) {
                var u = angular.isDefined(i.maxSize) ? n.$parent.$eval(i.maxSize) : t.maxSize,
                    p = angular.isDefined(i.rotate) ? n.$parent.$eval(i.rotate) : t.rotate;
                n.boundaryLinks = angular.isDefined(i.boundaryLinks) ? n.$parent.$eval(i.boundaryLinks) : t.boundaryLinks, n.directionLinks = angular.isDefined(i.directionLinks) ? n.$parent.$eval(i.directionLinks) : t.directionLinks, s.init(c, t), i.maxSize && n.$parent.$watch(e(i.maxSize), function(e) {
                    u = parseInt(e, 10), s.render();
                });
                var d = s.render;
                s.render = function() {
                    d(), n.page > 0 && n.page <= n.totalPages && (n.pages = l(n.page, n.totalPages))
                }
            }
        }
    }
}]).constant("pagerConfig", {
    itemsPerPage: 10,
    previousText: "« Previous",
    nextText: "Next »",
    align: !0
}).directive("pager", ["pagerConfig", function(e) {
    return {
        restrict: "EA",
        scope: {
            totalItems: "=",
            previousText: "@",
            nextText: "@"
        },
        require: ["pager", "?ngModel"],
        controller: "PaginationController",
        templateUrl: "template/pagination/pager.html",
        replace: !0,
        link: function(t, n, a, i) {
            var r = i[0],
                o = i[1];
            o && (t.align = angular.isDefined(a.align) ? t.$parent.$eval(a.align) : e.align, r.init(o, e))
        }
    }
}]), angular.module("ui.bootstrap.tooltip", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).provider("$tooltip", function() {
    function e(e) {
        var t = /[A-Z]/g,
            n = "-";
        return e.replace(t, function(e, t) {
            return (t ? n : "") + e.toLowerCase()
        })
    }
    var t = {
            placement: "top",
            animation: !0,
            popupDelay: 0
        },
        n = {
            mouseenter: "mouseleave",
            click: "click",
            focus: "blur"
        },
        a = {};
    this.options = function(e) {
        angular.extend(a, e)
    }, this.setTriggers = function(e) {
        angular.extend(n, e)
    }, this.$get = ["$window", "$compile", "$timeout", "$parse", "$document", "$position", "$interpolate", function(i, r, o, l, s, c, u) {
        return function(i, p, d) {
            function g(e) {
                var t = e || m.trigger || d,
                    a = n[t] || t;
                return {
                    show: t,
                    hide: a
                }
            }
            var m = angular.extend({}, t, a),
                f = e(i),
                h = u.startSymbol(),
                v = u.endSymbol(),
                b = "<div " + f + '-popup title="' + h + "tt_title" + v + '" content="' + h + "tt_content" + v + '" placement="' + h + "tt_placement" + v + '" animation="tt_animation" is-open="tt_isOpen"></div>';
            return {
                restrict: "EA",
                scope: !0,
                compile: function(e, t) {
                    var n = r(b);
                    return function(e, t, a) {
                        function r() {
                            e.tt_isOpen ? d() : u()
                        }

                        function u() {
                            D && !e.$eval(a[p + "Enable"]) || (e.tt_popupDelay ? w || (w = o(f, e.tt_popupDelay, !1), w.then(function(e) {
                                e()
                            })) : f()())
                        }

                        function d() {
                            e.$apply(function() {
                                h()
                            })
                        }

                        function f() {
                            return w = null, y && (o.cancel(y), y = null), e.tt_content ? (v(), $.css({
                                top: 0,
                                left: 0,
                                display: "block"
                            }), k ? s.find("body").append($) : t.after($), M(), e.tt_isOpen = !0, e.$digest(), M) : angular.noop
                        }

                        function h() {
                            e.tt_isOpen = !1, o.cancel(w), w = null, e.tt_animation ? y || (y = o(b, 500)) : b()
                        }

                        function v() {
                            $ && b(), $ = n(e, function() {}), e.$digest()
                        }

                        function b() {
                            y = null, $ && ($.remove(), $ = null)
                        }
                        var $, y, w, k = !!angular.isDefined(m.appendToBody) && m.appendToBody,
                            x = g(void 0),
                            D = angular.isDefined(a[p + "Enable"]),
                            M = function() {
                                var n = c.positionElements(t, $, e.tt_placement, k);
                                n.top += "px", n.left += "px", $.css(n)
                            };
                        e.tt_isOpen = !1, a.$observe(i, function(t) {
                            e.tt_content = t, !t && e.tt_isOpen && h()
                        }), a.$observe(p + "Title", function(t) {
                            e.tt_title = t
                        }), a.$observe(p + "Placement", function(t) {
                            e.tt_placement = angular.isDefined(t) ? t : m.placement
                        }), a.$observe(p + "PopupDelay", function(t) {
                            var n = parseInt(t, 10);
                            e.tt_popupDelay = isNaN(n) ? m.popupDelay : n
                        });
                        var T = function() {
                            t.unbind(x.show, u), t.unbind(x.hide, d)
                        };
                        a.$observe(p + "Trigger", function(e) {
                            T(), x = g(e), x.show === x.hide ? t.bind(x.show, r) : (t.bind(x.show, u), t.bind(x.hide, d))
                        });
                        var C = e.$eval(a[p + "Animation"]);
                        e.tt_animation = angular.isDefined(C) ? !!C : m.animation, a.$observe(p + "AppendToBody", function(t) {
                            k = angular.isDefined(t) ? l(t)(e) : k
                        }), k && e.$on("$locationChangeSuccess", function() {
                            e.tt_isOpen && h()
                        }), e.$on("$destroy", function() {
                            o.cancel(y), o.cancel(w), T(), b()
                        })
                    }
                }
            }
        }
    }]
}).directive("tooltipPopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-popup.html"
    }
}).directive("tooltip", ["$tooltip", function(e) {
    return e("tooltip", "tooltip", "mouseenter")
}]).directive("tooltipHtmlUnsafePopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-html-unsafe-popup.html"
    }
}).directive("tooltipHtmlUnsafe", ["$tooltip", function(e) {
    return e("tooltipHtmlUnsafe", "tooltip", "mouseenter")
}]), angular.module("ui.bootstrap.popover", ["ui.bootstrap.tooltip"]).directive("popoverPopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            title: "@",
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/popover/popover.html"
    }
}).directive("popover", ["$tooltip", function(e) {
    return e("popover", "popover", "click")
}]), angular.module("ui.bootstrap.progressbar", []).constant("progressConfig", {
    animate: !0,
    max: 100
}).controller("ProgressController", ["$scope", "$attrs", "progressConfig", function(e, t, n) {
    var a = this,
        i = angular.isDefined(t.animate) ? e.$parent.$eval(t.animate) : n.animate;
    this.bars = [], e.max = angular.isDefined(t.max) ? e.$parent.$eval(t.max) : n.max, this.addBar = function(t, n) {
        i || n.css({
            transition: "none"
        }), this.bars.push(t), t.$watch("value", function(n) {
            t.percent = +(100 * n / e.max).toFixed(2)
        }), t.$on("$destroy", function() {
            n = null, a.removeBar(t)
        })
    }, this.removeBar = function(e) {
        this.bars.splice(this.bars.indexOf(e), 1)
    }
}]).directive("progress", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        controller: "ProgressController",
        require: "progress",
        scope: {},
        templateUrl: "template/progressbar/progress.html"
    }
}).directive("bar", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        require: "^progress",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/bar.html",
        link: function(e, t, n, a) {
            a.addBar(e, t)
        }
    }
}).directive("progressbar", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        controller: "ProgressController",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/progressbar.html",
        link: function(e, t, n, a) {
            a.addBar(e, angular.element(t.children()[0]))
        }
    }
}), angular.module("ui.bootstrap.rating", []).constant("ratingConfig", {
    max: 5,
    stateOn: null,
    stateOff: null
}).controller("RatingController", ["$scope", "$attrs", "ratingConfig", function(e, t, n) {
    var a = {
        $setViewValue: angular.noop
    };
    this.init = function(i) {
        a = i, a.$render = this.render, this.stateOn = angular.isDefined(t.stateOn) ? e.$parent.$eval(t.stateOn) : n.stateOn, this.stateOff = angular.isDefined(t.stateOff) ? e.$parent.$eval(t.stateOff) : n.stateOff;
        var r = angular.isDefined(t.ratingStates) ? e.$parent.$eval(t.ratingStates) : new Array(angular.isDefined(t.max) ? e.$parent.$eval(t.max) : n.max);
        e.range = this.buildTemplateObjects(r)
    }, this.buildTemplateObjects = function(e) {
        for (var t = 0, n = e.length; t < n; t++) e[t] = angular.extend({
            index: t
        }, {
            stateOn: this.stateOn,
            stateOff: this.stateOff
        }, e[t]);
        return e
    }, e.rate = function(t) {
        !e.readonly && t >= 0 && t <= e.range.length && (a.$setViewValue(t), a.$render())
    }, e.enter = function(t) {
        e.readonly || (e.value = t), e.onHover({
            value: t
        })
    }, e.reset = function() {
        e.value = a.$viewValue, e.onLeave()
    }, e.onKeydown = function(t) {
        /(37|38|39|40)/.test(t.which) && (t.preventDefault(), t.stopPropagation(), e.rate(e.value + (38 === t.which || 39 === t.which ? 1 : -1)))
    }, this.render = function() {
        e.value = a.$viewValue
    }
}]).directive("rating", function() {
    return {
        restrict: "EA",
        require: ["rating", "ngModel"],
        scope: {
            readonly: "=?",
            onHover: "&",
            onLeave: "&"
        },
        controller: "RatingController",
        templateUrl: "template/rating/rating.html",
        replace: !0,
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r && i.init(r)
        }
    }
}), angular.module("ui.bootstrap.tabs", []).controller("TabsetController", ["$scope", function(e) {
    var t = this,
        n = t.tabs = e.tabs = [];
    t.select = function(e) {
        angular.forEach(n, function(t) {
            t.active && t !== e && (t.active = !1, t.onDeselect())
        }), e.active = !0, e.onSelect()
    }, t.addTab = function(e) {
        n.push(e), 1 === n.length ? e.active = !0 : e.active && t.select(e)
    }, t.removeTab = function(e) {
        var a = n.indexOf(e);
        if (e.active && n.length > 1) {
            var i = a == n.length - 1 ? a - 1 : a + 1;
            t.select(n[i])
        }
        n.splice(a, 1)
    }
}]).directive("tabset", function() {
    return {
        restrict: "EA",
        transclude: !0,
        replace: !0,
        scope: {
            type: "@"
        },
        controller: "TabsetController",
        templateUrl: "template/tabs/tabset.html",
        link: function(e, t, n) {
            e.vertical = !!angular.isDefined(n.vertical) && e.$parent.$eval(n.vertical), e.justified = !!angular.isDefined(n.justified) && e.$parent.$eval(n.justified)
        }
    }
}).directive("tab", ["$parse", function(e) {
    return {
        require: "^tabset",
        restrict: "EA",
        replace: !0,
        templateUrl: "template/tabs/tab.html",
        transclude: !0,
        scope: {
            active: "=?",
            heading: "@",
            onSelect: "&select",
            onDeselect: "&deselect"
        },
        controller: function() {},
        compile: function(t, n, a) {
            return function(t, n, i, r) {
                t.$watch("active", function(e) {
                    e && r.select(t)
                }), t.disabled = !1, i.disabled && t.$parent.$watch(e(i.disabled), function(e) {
                    t.disabled = !!e
                }), t.select = function() {
                    t.disabled || (t.active = !0)
                }, r.addTab(t), t.$on("$destroy", function() {
                    r.removeTab(t)
                }), t.$transcludeFn = a
            }
        }
    }
}]).directive("tabHeadingTransclude", [function() {
    return {
        restrict: "A",
        require: "^tab",
        link: function(e, t, n, a) {
            e.$watch("headingElement", function(e) {
                e && (t.html(""), t.append(e))
            })
        }
    }
}]).directive("tabContentTransclude", function() {
    function e(e) {
        return e.tagName && (e.hasAttribute("tab-heading") || e.hasAttribute("data-tab-heading") || "tab-heading" === e.tagName.toLowerCase() || "data-tab-heading" === e.tagName.toLowerCase())
    }
    return {
        restrict: "A",
        require: "^tabset",
        link: function(t, n, a) {
            var i = t.$eval(a.tabContentTransclude);
            i.$transcludeFn(i.$parent, function(t) {
                angular.forEach(t, function(t) {
                    e(t) ? i.headingElement = t : n.append(t)
                })
            })
        }
    }
}), angular.module("ui.bootstrap.timepicker", []).constant("timepickerConfig", {
    hourStep: 1,
    minuteStep: 1,
    showMeridian: !0,
    meridians: null,
    readonlyInput: !1,
    mousewheel: !0
}).controller("TimepickerController", ["$scope", "$attrs", "$parse", "$log", "$locale", "timepickerConfig", function(e, t, n, a, i, r) {
    function o() {
        var t = parseInt(e.hours, 10),
            n = e.showMeridian ? t > 0 && t < 13 : t >= 0 && t < 24;
        if (n) return e.showMeridian && (12 === t && (t = 0), e.meridian === f[1] && (t += 12)), t
    }

    function l() {
        var t = parseInt(e.minutes, 10);
        return t >= 0 && t < 60 ? t : void 0
    }

    function s(e) {
        return angular.isDefined(e) && e.toString().length < 2 ? "0" + e : e
    }

    function c(e) {
        u(), m.$setViewValue(new Date(g)), p(e)
    }

    function u() {
        m.$setValidity("time", !0), e.invalidHours = !1, e.invalidMinutes = !1
    }

    function p(t) {
        var n = g.getHours(),
            a = g.getMinutes();
        e.showMeridian && (n = 0 === n || 12 === n ? 12 : n % 12), e.hours = "h" === t ? n : s(n), e.minutes = "m" === t ? a : s(a), e.meridian = g.getHours() < 12 ? f[0] : f[1]
    }

    function d(e) {
        var t = new Date(g.getTime() + 6e4 * e);
        g.setHours(t.getHours(), t.getMinutes()), c()
    }
    var g = new Date,
        m = {
            $setViewValue: angular.noop
        },
        f = angular.isDefined(t.meridians) ? e.$parent.$eval(t.meridians) : r.meridians || i.DATETIME_FORMATS.AMPMS;
    this.init = function(n, a) {
        m = n, m.$render = this.render;
        var i = a.eq(0),
            o = a.eq(1),
            l = angular.isDefined(t.mousewheel) ? e.$parent.$eval(t.mousewheel) : r.mousewheel;
        l && this.setupMousewheelEvents(i, o), e.readonlyInput = angular.isDefined(t.readonlyInput) ? e.$parent.$eval(t.readonlyInput) : r.readonlyInput, this.setupInputEvents(i, o)
    };
    var h = r.hourStep;
    t.hourStep && e.$parent.$watch(n(t.hourStep), function(e) {
        h = parseInt(e, 10)
    });
    var v = r.minuteStep;
    t.minuteStep && e.$parent.$watch(n(t.minuteStep), function(e) {
        v = parseInt(e, 10)
    }), e.showMeridian = r.showMeridian, t.showMeridian && e.$parent.$watch(n(t.showMeridian), function(t) {
        if (e.showMeridian = !!t, m.$error.time) {
            var n = o(),
                a = l();
            angular.isDefined(n) && angular.isDefined(a) && (g.setHours(n), c())
        } else p()
    }), this.setupMousewheelEvents = function(t, n) {
        var a = function(e) {
            e.originalEvent && (e = e.originalEvent);
            var t = e.wheelDelta ? e.wheelDelta : -e.deltaY;
            return e.detail || t > 0
        };
        t.bind("mousewheel wheel", function(t) {
            e.$apply(a(t) ? e.incrementHours() : e.decrementHours()), t.preventDefault()
        }), n.bind("mousewheel wheel", function(t) {
            e.$apply(a(t) ? e.incrementMinutes() : e.decrementMinutes()), t.preventDefault()
        })
    }, this.setupInputEvents = function(t, n) {
        if (e.readonlyInput) return e.updateHours = angular.noop, void(e.updateMinutes = angular.noop);
        var a = function(t, n) {
            m.$setViewValue(null), m.$setValidity("time", !1), angular.isDefined(t) && (e.invalidHours = t), angular.isDefined(n) && (e.invalidMinutes = n)
        };
        e.updateHours = function() {
            var e = o();
            angular.isDefined(e) ? (g.setHours(e), c("h")) : a(!0)
        }, t.bind("blur", function(t) {
            !e.invalidHours && e.hours < 10 && e.$apply(function() {
                e.hours = s(e.hours)
            })
        }), e.updateMinutes = function() {
            var e = l();
            angular.isDefined(e) ? (g.setMinutes(e), c("m")) : a(void 0, !0)
        }, n.bind("blur", function(t) {
            !e.invalidMinutes && e.minutes < 10 && e.$apply(function() {
                e.minutes = s(e.minutes)
            })
        })
    }, this.render = function() {
        var e = m.$modelValue ? new Date(m.$modelValue) : null;
        isNaN(e) ? (m.$setValidity("time", !1), a.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')) : (e && (g = e), u(), p())
    }, e.incrementHours = function() {
        d(60 * h)
    }, e.decrementHours = function() {
        d(60 * -h)
    }, e.incrementMinutes = function() {
        d(v)
    }, e.decrementMinutes = function() {
        d(-v)
    }, e.toggleMeridian = function() {
        d(720 * (g.getHours() < 12 ? 1 : -1))
    }
}]).directive("timepicker", function() {
    return {
        restrict: "EA",
        require: ["timepicker", "?^ngModel"],
        controller: "TimepickerController",
        replace: !0,
        scope: {},
        templateUrl: "template/timepicker/timepicker.html",
        link: function(e, t, n, a) {
            var i = a[0],
                r = a[1];
            r && i.init(r, t.find("input"))
        }
    }
}), angular.module("ui.bootstrap.typeahead", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).factory("typeaheadParser", ["$parse", function(e) {
    var t = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;
    return {
        parse: function(n) {
            var a = n.match(t);
            if (!a) throw new Error('Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_" but got "' + n + '".');
            return {
                itemName: a[3],
                source: e(a[4]),
                viewMapper: e(a[2] || a[1]),
                modelMapper: e(a[1])
            }
        }
    }
}]).directive("typeahead", ["$compile", "$parse", "$q", "$timeout", "$document", "$position", "typeaheadParser", function(e, t, n, a, i, r, o) {
    var l = [9, 13, 27, 38, 40];
    return {
        require: "ngModel",
        link: function(s, c, u, p) {
            var d, g = s.$eval(u.typeaheadMinLength) || 1,
                m = s.$eval(u.typeaheadWaitMs) || 0,
                f = s.$eval(u.typeaheadEditable) !== !1,
                h = t(u.typeaheadLoading).assign || angular.noop,
                v = t(u.typeaheadOnSelect),
                b = u.typeaheadInputFormatter ? t(u.typeaheadInputFormatter) : void 0,
                $ = !!u.typeaheadAppendToBody && s.$eval(u.typeaheadAppendToBody),
                y = t(u.ngModel).assign,
                w = o.parse(u.typeahead),
                k = s.$new();
            s.$on("$destroy", function() {
                k.$destroy()
            });
            var x = "typeahead-" + k.$id + "-" + Math.floor(1e4 * Math.random());
            c.attr({
                "aria-autocomplete": "list",
                "aria-expanded": !1,
                "aria-owns": x
            });
            var D = angular.element("<div typeahead-popup></div>");
            D.attr({
                id: x,
                matches: "matches",
                active: "activeIdx",
                select: "select(activeIdx)",
                query: "query",
                position: "position"
            }), angular.isDefined(u.typeaheadTemplateUrl) && D.attr("template-url", u.typeaheadTemplateUrl);
            var M = function() {
                    k.matches = [], k.activeIdx = -1, c.attr("aria-expanded", !1)
                },
                T = function(e) {
                    return x + "-option-" + e
                };
            k.$watch("activeIdx", function(e) {
                e < 0 ? c.removeAttr("aria-activedescendant") : c.attr("aria-activedescendant", T(e))
            });
            var C = function(e) {
                var t = {
                    $viewValue: e
                };
                h(s, !0), n.when(w.source(s, t)).then(function(n) {
                    var a = e === p.$viewValue;
                    if (a && d)
                        if (n.length > 0) {
                            k.activeIdx = 0, k.matches.length = 0;
                            for (var i = 0; i < n.length; i++) t[w.itemName] = n[i], k.matches.push({
                                id: T(i),
                                label: w.viewMapper(k, t),
                                model: n[i]
                            });
                            k.query = e, k.position = $ ? r.offset(c) : r.position(c), k.position.top = k.position.top + c.prop("offsetHeight"), c.attr("aria-expanded", !0)
                        } else M();
                    a && h(s, !1)
                }, function() {
                    M(), h(s, !1)
                })
            };
            M(), k.query = void 0;
            var E, O = function(e) {
                    E = a(function() {
                        C(e)
                    }, m)
                },
                A = function() {
                    E && a.cancel(E)
                };
            p.$parsers.unshift(function(e) {
                return d = !0, e && e.length >= g ? m > 0 ? (A(), O(e)) : C(e) : (h(s, !1), A(), M()), f ? e : e ? void p.$setValidity("editable", !1) : (p.$setValidity("editable", !0), e)
            }), p.$formatters.push(function(e) {
                var t, n, a = {};
                return b ? (a.$model = e, b(s, a)) : (a[w.itemName] = e, t = w.viewMapper(s, a), a[w.itemName] = void 0, n = w.viewMapper(s, a), t !== n ? t : e)
            }), k.select = function(e) {
                var t, n, i = {};
                i[w.itemName] = n = k.matches[e].model, t = w.modelMapper(s, i), y(s, t), p.$setValidity("editable", !0), v(s, {
                    $item: n,
                    $model: t,
                    $label: w.viewMapper(s, i)
                }), M(), a(function() {
                    c[0].focus()
                }, 0, !1)
            }, c.bind("keydown", function(e) {
                0 !== k.matches.length && l.indexOf(e.which) !== -1 && (e.preventDefault(), 40 === e.which ? (k.activeIdx = (k.activeIdx + 1) % k.matches.length, k.$digest()) : 38 === e.which ? (k.activeIdx = (k.activeIdx ? k.activeIdx : k.matches.length) - 1, k.$digest()) : 13 === e.which || 9 === e.which ? k.$apply(function() {
                    k.select(k.activeIdx)
                }) : 27 === e.which && (e.stopPropagation(), M(), k.$digest()))
            }), c.bind("blur", function(e) {
                d = !1
            });
            var P = function(e) {
                c[0] !== e.target && (M(), k.$digest())
            };
            i.bind("click", P), s.$on("$destroy", function() {
                i.unbind("click", P)
            });
            var S = e(D)(k);
            $ ? i.find("body").append(S) : c.after(S)
        }
    }
}]).directive("typeaheadPopup", function() {
    return {
        restrict: "EA",
        scope: {
            matches: "=",
            query: "=",
            active: "=",
            position: "=",
            select: "&"
        },
        replace: !0,
        templateUrl: "template/typeahead/typeahead-popup.html",
        link: function(e, t, n) {
            e.templateUrl = n.templateUrl, e.isOpen = function() {
                return e.matches.length > 0
            }, e.isActive = function(t) {
                return e.active == t
            }, e.selectActive = function(t) {
                e.active = t
            }, e.selectMatch = function(t) {
                e.select({
                    activeIdx: t
                })
            }
        }
    }
}).directive("typeaheadMatch", ["$http", "$templateCache", "$compile", "$parse", function(e, t, n, a) {
    return {
        restrict: "EA",
        scope: {
            index: "=",
            match: "=",
            query: "="
        },
        link: function(i, r, o) {
            var l = a(o.templateUrl)(i.$parent) || "template/typeahead/typeahead-match.html";
            e.get(l, {
                cache: t
            }).success(function(e) {
                r.replaceWith(n(e.trim())(i))
            })
        }
    }
}]).filter("typeaheadHighlight", function() {
    function e(e) {
        return e.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
    }
    return function(t, n) {
        return n ? ("" + t).replace(new RegExp(e(n), "gi"), "<strong>$&</strong>") : t
    }
}), angular.module("template/accordion/accordion-group.html", []).run(["$templateCache", function(e) {
    e.put("template/accordion/accordion-group.html", '<div class="panel panel-default">\n  <div class="panel-heading">\n    <h4 class="panel-title">\n      <a class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading"><span ng-class="{\'text-muted\': isDisabled}">{{heading}}</span></a>\n    </h4>\n  </div>\n  <div class="panel-collapse" collapse="!isOpen">\n\t  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>')
}]), angular.module("template/accordion/accordion.html", []).run(["$templateCache", function(e) {
    e.put("template/accordion/accordion.html", '<div class="panel-group" ng-transclude></div>')
}]), angular.module("template/alert/alert.html", []).run(["$templateCache", function(e) {
    e.put("template/alert/alert.html", '<div class="alert" ng-class="[\'alert-\' + (type || \'warning\'), closeable ? \'alert-dismissable\' : null]" role="alert">\n    <button ng-show="closeable" type="button" class="close" ng-click="close()">\n        <span aria-hidden="true">&times;</span>\n        <span class="sr-only">Close</span>\n    </button>\n    <div ng-transclude></div>\n</div>\n')
}]), angular.module("template/carousel/carousel.html", []).run(["$templateCache", function(e) {
    e.put("template/carousel/carousel.html", '<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel" ng-swipe-right="prev()" ng-swipe-left="next()">\n    <ol class="carousel-indicators" ng-show="slides.length > 1">\n        <li ng-repeat="slide in slides track by $index" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a class="left carousel-control" ng-click="prev()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-left"></span></a>\n    <a class="right carousel-control" ng-click="next()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-right"></span></a>\n</div>\n')
}]), angular.module("template/carousel/slide.html", []).run(["$templateCache", function(e) {
    e.put("template/carousel/slide.html", "<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item text-center\" ng-transclude></div>\n")
}]), angular.module("template/datepicker/datepicker.html", []).run(["$templateCache", function(e) {
    e.put("template/datepicker/datepicker.html", '<div ng-switch="datepickerMode" role="application" ng-keydown="keydown($event)">\n  <daypicker ng-switch-when="day" tabindex="0"></daypicker>\n  <monthpicker ng-switch-when="month" tabindex="0"></monthpicker>\n  <yearpicker ng-switch-when="year" tabindex="0"></yearpicker>\n</div>')
}]), angular.module("template/datepicker/day.html", []).run(["$templateCache", function(e) {
    e.put("template/datepicker/day.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="{{5 + showWeeks}}"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n    <tr>\n      <th ng-show="showWeeks" class="text-center"></th>\n      <th ng-repeat="label in labels track by $index" class="text-center"><small aria-label="{{label.full}}">{{label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-show="showWeeks" class="text-center h6"><em>{{ weekNumbers[$index] }}</em></td>\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
}]), angular.module("template/datepicker/month.html", []).run(["$templateCache", function(e) {
    e.put("template/datepicker/month.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
}]), angular.module("template/datepicker/popup.html", []).run(["$templateCache", function(e) {
    e.put("template/datepicker/popup.html", '<ul class="dropdown-menu" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top+\'px\', left: position.left+\'px\'}" ng-keydown="keydown($event)">\n\t<li ng-transclude></li>\n\t<li ng-if="showButtonBar" style="padding:10px 9px 2px">\n\t\t<span class="btn-group">\n\t\t\t<button type="button" class="btn btn-sm btn-info" ng-click="select(\'today\')">{{ getText(\'current\') }}</button>\n\t\t\t<button type="button" class="btn btn-sm btn-danger" ng-click="select(null)">{{ getText(\'clear\') }}</button>\n\t\t</span>\n\t\t<button type="button" class="btn btn-sm btn-success pull-right" ng-click="close()">{{ getText(\'close\') }}</button>\n\t</li>\n</ul>\n')
}]), angular.module("template/datepicker/year.html", []).run(["$templateCache", function(e) {
    e.put("template/datepicker/year.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="3"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
}]), angular.module("template/modal/backdrop.html", []).run(["$templateCache", function(e) {
    e.put("template/modal/backdrop.html", '<div class="modal-backdrop fade {{ backdropClass }}"\n     ng-class="{in: animate}"\n     ng-style="{\'z-index\': 1040 + (index && 1 || 0) + index*10}"\n></div>\n')
}]), angular.module("template/modal/window.html", []).run(["$templateCache", function(e) {
    e.put("template/modal/window.html", '<div tabindex="-1" role="dialog" class="modal fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n    <div class="modal-dialog" ng-class="{\'modal-sm\': size == \'sm\', \'modal-lg\': size == \'lg\'}"><div class="modal-content" modal-transclude></div></div>\n</div>')
}]), angular.module("template/pagination/pager.html", []).run(["$templateCache", function(e) {
    e.put("template/pagination/pager.html", '<ul class="pager">\n  <li ng-class="{disabled: noPrevious(), previous: align}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-class="{disabled: noNext(), next: align}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n</ul>')
}]), angular.module("template/pagination/pagination.html", []).run(["$templateCache", function(e) {
    e.put("template/pagination/pagination.html", '<ul class="pagination">\n  <li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(1)">{{getText(\'first\')}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-repeat="page in pages track by $index" ng-class="{active: page.active}"><a href ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n  <li ng-if="boundaryLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(totalPages)">{{getText(\'last\')}}</a></li>\n</ul>')
}]), angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run(["$templateCache", function(e) {
    e.put("template/tooltip/tooltip-html-unsafe-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n</div>\n')
}]), angular.module("template/tooltip/tooltip-popup.html", []).run(["$templateCache", function(e) {
    e.put("template/tooltip/tooltip-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')
}]), angular.module("template/popover/popover.html", []).run(["$templateCache", function(e) {
    e.put("template/popover/popover.html", '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')
}]), angular.module("template/progressbar/bar.html", []).run(["$templateCache", function(e) {
    e.put("template/progressbar/bar.html", '<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>')
}]), angular.module("template/progressbar/progress.html", []).run(["$templateCache", function(e) {
    e.put("template/progressbar/progress.html", '<div class="progress" ng-transclude></div>')
}]), angular.module("template/progressbar/progressbar.html", []).run(["$templateCache", function(e) {
    e.put("template/progressbar/progressbar.html", '<div class="progress">\n  <div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>\n</div>')
}]), angular.module("template/rating/rating.html", []).run(["$templateCache", function(e) {
    e.put("template/rating/rating.html", '<span ng-mouseleave="reset()" ng-keydown="onKeydown($event)" tabindex="0" role="slider" aria-valuemin="0" aria-valuemax="{{range.length}}" aria-valuenow="{{value}}">\n    <i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < value && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')">\n        <span class="sr-only">({{ $index < value ? \'*\' : \' \' }})</span>\n    </i>\n</span>')
}]), angular.module("template/tabs/tab.html", []).run(["$templateCache", function(e) {
    e.put("template/tabs/tab.html", '<li ng-class="{active: active, disabled: disabled}">\n  <a ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n')
}]), angular.module("template/tabs/tabset.html", []).run(["$templateCache", function(e) {
    e.put("template/tabs/tabset.html", '<div>\n  <ul class="nav nav-{{type || \'tabs\'}}" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n</div>\n')
}]), angular.module("template/timepicker/timepicker.html", []).run(["$templateCache", function(e) {
    e.put("template/timepicker/timepicker.html", '<table>\n\t<tbody>\n\t\t<tr class="text-center">\n\t\t\t<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n\t\t\t<td ng-show="showMeridian"></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidHours}">\n\t\t\t\t<input type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2">\n\t\t\t</td>\n\t\t\t<td>:</td>\n\t\t\t<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n\t\t\t\t<input type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n\t\t\t</td>\n\t\t\t<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n\t\t</tr>\n\t\t<tr class="text-center">\n\t\t\t<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n\t\t\t<td>&nbsp;</td>\n\t\t\t<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n\t\t\t<td ng-show="showMeridian"></td>\n\t\t</tr>\n\t</tbody>\n</table>\n')
}]), angular.module("template/typeahead/typeahead-match.html", []).run(["$templateCache", function(e) {
    e.put("template/typeahead/typeahead-match.html", '<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')
}]), angular.module("template/typeahead/typeahead-popup.html", []).run(["$templateCache", function(e) {
    e.put("template/typeahead/typeahead-popup.html", '<ul class="dropdown-menu" ng-show="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\'}" style="display: block;" role="listbox" aria-hidden="{{!isOpen()}}">\n    <li ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)" role="option" id="{{match.id}}">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>\n')
}]);
! function(n, t, e) {
    "use strict";

    function o(n, t, e) {
        c.directive(n, ["$parse", "$swipe", function(o, c) {
            var i = 75,
                u = .3,
                a = 30;
            return function(r, s, l) {
                function h(n) {
                    if (!f) return !1;
                    var e = Math.abs(n.y - f.y),
                        o = (n.x - f.x) * t;
                    return d && e < i && o > 0 && o > a && e / o < u
                }
                var f, d, g = o(l[n]);
                c.bind(s, {
                    start: function(n, t) {
                        f = n, d = !0
                    },
                    cancel: function(n) {
                        d = !1
                    },
                    end: function(n, t) {
                        h(n) && r.$apply(function() {
                            s.triggerHandler(e), g(r, {
                                $event: t
                            })
                        })
                    }
                })
            }
        }])
    }
    var c = t.module("ngTouch", []);
    c.factory("$swipe", [function() {
        function n(n) {
            var t = n.touches && n.touches.length ? n.touches : [n],
                e = n.changedTouches && n.changedTouches[0] || n.originalEvent && n.originalEvent.changedTouches && n.originalEvent.changedTouches[0] || t[0].originalEvent || t[0];
            return {
                x: e.clientX,
                y: e.clientY
            }
        }
        var t = 10;
        return {
            bind: function(e, o) {
                var c, i, u, a, r = !1;
                e.on("touchstart mousedown", function(t) {
                    u = n(t), r = !0, c = 0, i = 0, a = u, o.start && o.start(u, t)
                }), e.on("touchcancel", function(n) {
                    r = !1, o.cancel && o.cancel(n)
                }), e.on("touchmove mousemove", function(e) {
                    if (r && u) {
                        var s = n(e);
                        if (c += Math.abs(s.x - a.x), i += Math.abs(s.y - a.y), a = s, !(c < t && i < t)) return i > c ? (r = !1, void(o.cancel && o.cancel(e))) : (e.preventDefault(), void(o.move && o.move(s, e)))
                    }
                }), e.on("touchend mouseup", function(t) {
                    r && (r = !1, o.end && o.end(n(t), t))
                })
            }
        }
    }]), c.config(["$provide", function(n) {
        n.decorator("ngClickDirective", ["$delegate", function(n) {
            return n.shift(), n
        }])
    }]), c.directive("ngClick", ["$parse", "$timeout", "$rootElement", function(n, e, o) {
        function c(n, t, e, o) {
            return Math.abs(n - e) < v && Math.abs(t - o) < v
        }

        function i(n, t, e) {
            for (var o = 0; o < n.length; o += 2)
                if (c(n[o], n[o + 1], t, e)) return n.splice(o, o + 2), !0;
            return !1
        }

        function u(n) {
            if (!(Date.now() - s > g)) {
                var t = n.touches && n.touches.length ? n.touches : [n],
                    e = t[0].clientX,
                    o = t[0].clientY;
                e < 1 && o < 1 || h && h[0] === e && h[1] === o || (h && (h = null), "label" === n.target.tagName.toLowerCase() && (h = [e, o]), i(l, e, o) || (n.stopPropagation(), n.preventDefault(), n.target && n.target.blur()))
            }
        }

        function a(n) {
            var t = n.touches && n.touches.length ? n.touches : [n],
                o = t[0].clientX,
                c = t[0].clientY;
            l.push(o, c), e(function() {
                for (var n = 0; n < l.length; n += 2)
                    if (l[n] == o && l[n + 1] == c) return void l.splice(n, n + 2)
            }, g, !1)
        }

        function r(n, t) {
            l || (o[0].addEventListener("click", u, !0), o[0].addEventListener("touchstart", a, !0), l = []), s = Date.now(), i(l, n, t)
        }
        var s, l, h, f = 750,
            d = 12,
            g = 2500,
            v = 25,
            p = "ng-click-active";
        return function(e, o, c) {
            function i() {
                g = !1, o.removeClass(p)
            }
            var u, a, s, l, h = n(c.ngClick),
                g = !1;
            o.on("touchstart", function(n) {
                g = !0, u = n.target ? n.target : n.srcElement, 3 == u.nodeType && (u = u.parentNode), o.addClass(p), a = Date.now();
                var t = n.touches && n.touches.length ? n.touches : [n],
                    e = t[0].originalEvent || t[0];
                s = e.clientX, l = e.clientY
            }), o.on("touchmove", function(n) {
                i()
            }), o.on("touchcancel", function(n) {
                i()
            }), o.on("touchend", function(n) {
                var e = Date.now() - a,
                    h = n.changedTouches && n.changedTouches.length ? n.changedTouches : n.touches && n.touches.length ? n.touches : [n],
                    v = h[0].originalEvent || h[0],
                    p = v.clientX,
                    m = v.clientY,
                    w = Math.sqrt(Math.pow(p - s, 2) + Math.pow(m - l, 2));
                g && e < f && w < d && (r(p, m), u && u.blur(), t.isDefined(c.disabled) && c.disabled !== !1 || o.triggerHandler("click", [n])), i()
            }), o.onclick = function(n) {}, o.on("click", function(n, t) {
                e.$apply(function() {
                    h(e, {
                        $event: t || n
                    })
                })
            }), o.on("mousedown", function(n) {
                o.addClass(p)
            }), o.on("mousemove mouseup", function(n) {
                o.removeClass(p)
            })
        }
    }]), o("ngSwipeLeft", -1, "swipeleft"), o("ngSwipeRight", 1, "swiperight")
}(window, window.angular);
window.AngularSlider = function(e, n, t) {
        function i(e) {
            p.cssText = e
        }

        function r(e, n) {
            return typeof e === n
        }

        function a() {
            u.inputtypes = function(e) {
                for (var i, r, a, l = 0, s = e.length; l < s; l++) b.setAttribute("type", r = e[l]), i = "text" !== b.type, i && (b.value = h, b.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(r) && b.style.WebkitAppearance !== t ? (c.appendChild(b), a = n.defaultView, i = a.getComputedStyle && "textfield" !== a.getComputedStyle(b, null).WebkitAppearance && 0 !== b.offsetHeight, c.removeChild(b)) : /^(search|tel)$/.test(r) || (i = /^(url|email)$/.test(r) ? b.checkValidity && b.checkValidity() === !1 : b.value != h)), g[e[l]] = !!i;
                return g
            }("search tel url email datetime date month week time datetime-local number range color".split(" "))
        }
        var l, s, o = "2.7.1",
            u = {},
            c = n.documentElement,
            f = "modernizr",
            d = n.createElement(f),
            p = d.style,
            b = n.createElement("input"),
            h = ":)",
            m = ({}.toString, {}),
            g = {},
            v = [],
            B = v.slice,
            y = {}.hasOwnProperty;
        s = r(y, "undefined") || r(y.call, "undefined") ? function(e, n) {
            return n in e && r(e.constructor.prototype[n], "undefined")
        } : function(e, n) {
            return y.call(e, n)
        }, Function.prototype.bind || (Function.prototype.bind = function(e) {
            var n = this;
            if ("function" != typeof n) throw new TypeError;
            var t = B.call(arguments, 1),
                i = function() {
                    if (this instanceof i) {
                        var r = function() {};
                        r.prototype = n.prototype;
                        var a = new r,
                            l = n.apply(a, t.concat(B.call(arguments)));
                        return Object(l) === l ? l : a
                    }
                    return n.apply(e, t.concat(B.call(arguments)))
                };
            return i
        });
        for (var w in m) s(m, w) && (l = w.toLowerCase(), u[l] = m[w](), v.push((u[l] ? "" : "no-") + l));
        return u.input || a(), u.addTest = function(e, n) {
            if ("object" == typeof e)
                for (var i in e) s(e, i) && u.addTest(i, e[i]);
            else {
                if (e = e.toLowerCase(), u[e] !== t) return u;
                n = "function" == typeof n ? n() : n, "undefined" != typeof enableClasses && enableClasses && (c.className += " " + (n ? "" : "no-") + e), u[e] = n
            }
            return u
        }, i(""), d = b = null, u._version = o, u
    }(this, this.document),
    function(e, n, t) {
        function i(e) {
            return "[object Function]" == m.call(e)
        }

        function r(e) {
            return "string" == typeof e
        }

        function a() {}

        function l(e) {
            return !e || "loaded" == e || "complete" == e || "uninitialized" == e
        }

        function s() {
            var e = g.shift();
            v = 1, e ? e.t ? b(function() {
                ("c" == e.t ? d.injectCss : d.injectJs)(e.s, 0, e.a, e.x, e.e, 1)
            }, 0) : (e(), s()) : v = 0
        }

        function o(e, t, i, r, a, o, u) {
            function c(n) {
                if (!p && l(f.readyState) && (B.r = p = 1, !v && s(), f.onload = f.onreadystatechange = null, n)) {
                    "img" != e && b(function() {
                        w.removeChild(f)
                    }, 50);
                    for (var i in C[t]) C[t].hasOwnProperty(i) && C[t][i].onload()
                }
            }
            var u = u || d.errorTimeout,
                f = n.createElement(e),
                p = 0,
                m = 0,
                B = {
                    t: i,
                    s: t,
                    e: a,
                    a: o,
                    x: u
                };
            1 === C[t] && (m = 1, C[t] = []), "object" == e ? f.data = t : (f.src = t, f.type = e), f.width = f.height = "0", f.onerror = f.onload = f.onreadystatechange = function() {
                c.call(this, m)
            }, g.splice(r, 0, B), "img" != e && (m || 2 === C[t] ? (w.insertBefore(f, y ? null : h), b(c, u)) : C[t].push(f))
        }

        function u(e, n, t, i, a) {
            return v = 0, n = n || "j", r(e) ? o("c" == n ? F : x, e, n, this.i++, t, i, a) : (g.splice(this.i++, 0, e), 1 == g.length && s()), this
        }

        function c() {
            var e = d;
            return e.loader = {
                load: u,
                i: 0
            }, e
        }
        var f, d, p = n.documentElement,
            b = e.setTimeout,
            h = n.getElementsByTagName("script")[0],
            m = {}.toString,
            g = [],
            v = 0,
            B = "MozAppearance" in p.style,
            y = B && !!n.createRange().compareNode,
            w = y ? p : h.parentNode,
            p = e.opera && "[object Opera]" == m.call(e.opera),
            p = !!n.attachEvent && !p,
            x = B ? "object" : p ? "script" : "img",
            F = p ? "script" : x,
            S = Array.isArray || function(e) {
                return "[object Array]" == m.call(e)
            },
            P = [],
            C = {},
            M = {
                timeout: function(e, n) {
                    return n.length && (e.timeout = n[0]), e
                }
            };
        d = function(e) {
            function n(e) {
                var n, t, i, e = e.split("!"),
                    r = P.length,
                    a = e.pop(),
                    l = e.length,
                    a = {
                        url: a,
                        origUrl: a,
                        prefixes: e
                    };
                for (t = 0; t < l; t++) i = e[t].split("="), (n = M[i.shift()]) && (a = n(a, i));
                for (t = 0; t < r; t++) a = P[t](a);
                return a
            }

            function l(e, r, a, l, s) {
                var o = n(e),
                    u = o.autoCallback;
                o.url.split(".").pop().split("?").shift(), o.bypass || (r && (r = i(r) ? r : r[e] || r[l] || r[e.split("/").pop().split("?")[0]]), o.instead ? o.instead(e, r, a, l, s) : (C[o.url] ? o.noexec = !0 : C[o.url] = 1, a.load(o.url, o.forceCSS || !o.forceJS && "css" == o.url.split(".").pop().split("?").shift() ? "c" : t, o.noexec, o.attrs, o.timeout), (i(r) || i(u)) && a.load(function() {
                    c(), r && r(o.origUrl, s, l), u && u(o.origUrl, s, l), C[o.url] = 2
                })))
            }

            function s(e, n) {
                function t(e, t) {
                    if (e) {
                        if (r(e)) t || (f = function() {
                            var e = [].slice.call(arguments);
                            d.apply(this, e), p()
                        }), l(e, f, n, 0, u);
                        else if (Object(e) === e)
                            for (o in s = function() {
                                    var n, t = 0;
                                    for (n in e) e.hasOwnProperty(n) && t++;
                                    return t
                                }(), e) e.hasOwnProperty(o) && (!t && !--s && (i(f) ? f = function() {
                                var e = [].slice.call(arguments);
                                d.apply(this, e), p()
                            } : f[o] = function(e) {
                                return function() {
                                    var n = [].slice.call(arguments);
                                    e && e.apply(this, n), p()
                                }
                            }(d[o])), l(e[o], f, n, o, u))
                    } else !t && p()
                }
                var s, o, u = !!e.test,
                    c = e.load || e.both,
                    f = e.callback || a,
                    d = f,
                    p = e.complete || a;
                t(u ? e.yep : e.nope, !!c), c && t(c)
            }
            var o, u, f = this.yepnope.loader;
            if (r(e)) l(e, 0, f, 0);
            else if (S(e))
                for (o = 0; o < e.length; o++) u = e[o], r(u) ? l(u, 0, f, 0) : S(u) ? d(u) : Object(u) === u && s(u, f);
            else Object(e) === e && s(e, f)
        }, d.addPrefix = function(e, n) {
            M[e] = n
        }, d.addFilter = function(e) {
            P.push(e)
        }, d.errorTimeout = 1e4, null == n.readyState && n.addEventListener && (n.readyState = "loading", n.addEventListener("DOMContentLoaded", f = function() {
            n.removeEventListener("DOMContentLoaded", f, 0), n.readyState = "complete"
        }, 0)), e.yepnope = c(), e.yepnope.executeStack = s, e.yepnope.injectJs = function(e, t, i, r, o, u) {
            var c, f, p = n.createElement("script"),
                r = r || d.errorTimeout;
            p.src = e;
            for (f in i) p.setAttribute(f, i[f]);
            t = u ? s : t || a, p.onreadystatechange = p.onload = function() {
                !c && l(p.readyState) && (c = 1, t(), p.onload = p.onreadystatechange = null)
            }, b(function() {
                c || (c = 1, t(1))
            }, r), o ? p.onload() : h.parentNode.insertBefore(p, h)
        }, e.yepnope.injectCss = function(e, t, i, r, l, o) {
            var u, r = n.createElement("link"),
                t = o ? s : t || a;
            r.href = e, r.rel = "stylesheet", r.type = "text/css";
            for (u in i) r.setAttribute(u, i[u]);
            l || (h.parentNode.insertBefore(r, h), b(t, 0))
        }
    }(this, document), AngularSlider.load = function() {
        yepnope.apply(window, [].slice.call(arguments, 0))
    }, angular.module("vr.directives.slider", ["ngTouch"]).directive("slider", ["$timeout", "$document", "$interpolate", "$swipe", function(e, n, t, i) {
        function r(e) {
            return angular.element(e)
        }

        function a(e) {
            return "" + e + "px"
        }

        function l(e, n) {
            return e.css({
                opacity: n
            })
        }

        function s(e) {
            return l(e, 0)
        }

        function o(e) {
            return l(e, 1)
        }

        function u(e, n) {
            return e.css({
                left: n
            })
        }

        function c(e) {
            var n = parseFloat(e.css("width"));
            return isNaN(n) ? e[0].offsetWidth : n
        }

        function f(e) {
            return c(e) / 2
        }

        function d(e) {
            try {
                return e.offset().left
            } catch (e) {}
            return e[0].getBoundingClientRect().left
        }

        function p(e, n) {
            return d(e) > d(n) ? d(e) - d(n) - c(n) : d(n) - d(e) - c(e)
        }

        function b(e, n) {
            return e.attr("ng-bind-template", n)
        }

        function h(e, n, t, i, r) {
            !angular.isUndefined(n) && n || (n = 0), !angular.isUndefined(t) && t && 0 != t || (t = 1 / Math.pow(10, n)), !angular.isUndefined(i) && i || (i = 0), !angular.isUndefined(e) && e || (e = 0);
            var a = (e - i) % t,
                l = a > t / 2 ? e + t - a : e - a;
            return !angular.isUndefined(r) && r || (r = l), l = Math.min(Math.max(l, i), r), parseFloat(l.toFixed(n))
        }

        function m(e, n) {
            return Math.floor(e / n + .5) * n
        }

        function g(e, n) {
            return e > 0 && !isNaN(n) ? Math.ceil(n / e) * e : n
        }

        function v(e) {
            return y + " " + e + " " + w
        }
        var B = 3,
            y = t.startSymbol(),
            w = t.endSymbol();
        return {
            restrict: "EA",
            require: "ngModel",
            scope: {
                floor: "@",
                ceiling: "@",
                step: "@",
                stepWidth: "@",
                precision: "@",
                buffer: "@",
                stickiness: "@",
                showSteps: "@",
                ngModel: "=",
                ngModelRange: "=",
                ngDisabled: "=",
                ngChange: "&",
                translateFn: "&",
                translateRangeFn: "&",
                translateCombinedFn: "&",
                scaleFn: "&",
                inverseScaleFn: "&"
            },
            template: "<span class='bar full'></span><span class='bar steps'><span class='bubble step' ng-repeat='step in stepBubbles()'></span></span><span class='bar selection'></span><span class='bar unselected low'></span><span class='bar unselected high'></span><span class='pointer low'></span><span class='pointer high'></span><span class='bubble low'></span><span class='bubble high'></span><span class='bubble middle'></span><span class='bubble selection'></span><span class='bubble limit floor'></span><span class='bubble limit ceiling'></span><input type='range' class='input low' /><input type='range' class='input high' /><input type='range' class='input selection' />",
            compile: function(t, l) {
                function y(e) {
                    e || (e = t);
                    var n = [];
                    return angular.forEach(e.children(), function(e) {
                        n.push(r(e))
                    }), n
                }

                function w(e, n, t) {
                    return {
                        fullBar: e[0],
                        stepBubs: e[1],
                        selBar: n ? e[2] : null,
                        unSelBarLow: n ? e[3] : null,
                        unSelBarHigh: n ? e[4] : null,
                        minPtr: n ? e[5] : e[2],
                        maxPtr: n ? e[6] : null,
                        lowBub: n ? e[7] : e[3],
                        highBub: n ? e[8] : null,
                        cmbBub: n ? e[9] : null,
                        selBub: n ? e[10] : null,
                        flrBub: n ? e[11] : e[4],
                        ceilBub: n ? e[12] : e[5],
                        minInput: t ? n ? e[13] : e[6] : null,
                        maxInput: t && n ? e[14] : null,
                        selInput: t && n ? e[15] : null
                    }
                }
                var x = l.showSteps,
                    F = l.stepWidth ? "stepWidth" : "step",
                    S = null != l.ngModelRange,
                    P = {},
                    C = "ngModel",
                    M = "ngModelRange",
                    I = "selectBar",
                    R = ["floor", "ceiling", "stickiness", C];
                if (P = function() {
                        for (var e = y(), n = [], t = 0, i = e.length; t < i; t++) {
                            var a = e[t];
                            a = r(a), a.css({
                                "white-space": "nowrap",
                                position: "absolute",
                                display: "block",
                                "z-index": 1
                            }), n.push(a)
                        }
                        return n
                    }(), P = w(P, !0, !0), l.translateFn && l.$set("translateFn", "" + l.translateFn + "(value)"), l.translateRangeFn && l.$set("translateRangeFnFn", "" + l.translateRangeFn + "(low,high)"), l.translateCombinedFn && l.$set("translateCombinedFnFn", "" + l.translateCombinedFn + "(low,high)"), l.scaleFn && l.$set("scaleFn", "" + l.scaleFn + "(value)"), l.inverseScaleFn && l.$set("inverseScaleFn", "" + l.inverseScaleFn + "(value)"), P.fullBar.css({
                        left: 0,
                        right: 0
                    }), AngularSlider.inputtypes.range) {
                    var k = {
                        position: "absolute",
                        margin: 0,
                        padding: 0,
                        opacity: 0,
                        height: "100%"
                    };
                    P.minInput.attr("step", v("inputSteps()")), P.minInput.attr("min", v("floor")), P.minInput.css(k), P.minInput.css("left", 0), S ? (P.minInput.attr("max", v("ngModelRange - (buffer / 2)")), P.maxInput.attr("step", v("inputSteps()")), P.maxInput.attr("min", v("ngModel + (buffer / 2)")), P.maxInput.attr("max", v("ceiling")), P.maxInput.css(k), P.selInput.attr("step", v("inputSteps()")), P.selInput.attr("min", v("ngModel")), P.selInput.attr("max", v("ngModelRange")), P.selInput.css(k)) : (P.minInput.attr("max", v("ceiling")), P.minInput.css({
                        width: "100%"
                    }), P.maxInput.remove(), P.selInput.remove())
                } else P.minInput.remove(), P.maxInput.remove(), P.selInput.remove();
                if (b(P.stepBubs.children().eq(0), v("translation(step)")), b(P.ceilBub, v("translation(ceiling)")), b(P.flrBub, v("translation(floor)")), b(P.selBub, v("rangeTranslation(" + C + "," + M + ")")), b(P.lowBub, v("translation(" + C + ")")), b(P.highBub, v("translation(" + M + ")")), b(P.cmbBub, v("combinedTranslation(" + C + "," + M + ")")), S) R.push(M), R.unshift("buffer");
                else
                    for (var N = [P.selBar, P.unSelBarLow, P.unSelBarHigh, P.maxPtr, P.selBub, P.highBub, P.cmbBub], V = 0, A = N.length; V < A; V++) t = N[V], t.remove();
                return R.unshift("precision", F), x || P.stepBubs.children().remove(), {
                    post: function(t, l, b, v) {
                        function x() {
                            if (angular.forEach(R, function(e) {
                                    t[e] = parseFloat(t[e]), e == C || e == M ? t[e] = h(t[e], t.precision, t[F], t.floor, t.ceiling) : "buffer" == e ? !t.buffer || isNaN(t.buffer) || t.buffer < 0 ? t.buffer = 0 : t.buffer = g(t[F], t.buffer) : "precision" == e ? !t.precision || isNaN(t.precision) ? t.precision = 0 : t.precision = parseInt(t.precision) : e == F ? !t[F] || isNaN(t[F]) ? t[F] = 1 / Math.pow(10, t.precision) : t[F] = parseFloat(t[F].toFixed(t.precision)) : "stickiness" == e && (isNaN(t.stickiness) ? t.stickiness = B : t.stickiness < 1 && (t.stickiness = 1)), t.decodedValues[e] = t.decodeRef(e)
                                }), S) {
                                if (t[M] < t[C]) {
                                    var e = t[M];
                                    t[M] = t[C], t[C] = e
                                }
                                var n = h(t[M] - t[C], t.precision, t[F]);
                                if (t.buffer > 0 && n < t.buffer) {
                                    var i = t.encode((t.decodedValues[C] + t.decodedValues[M]) / 2);
                                    t[C] = h(i - t.buffer / 2, t.precision, t[F], t.floor, t.ceiling), t[M] = t[C] + t.buffer, t[M] > t.ceiling && (t[M] = t.ceiling, t[C] = t.ceiling - t.buffer)
                                }
                            }
                            N = c(k.fullBar), V = f(k.minPtr), A = d(k.fullBar), E = A + N - c(k.minPtr), T = E - A, $ = t.floor, j = t.decodedValues.floor, U = t.ceiling, L = t.decodedValues.ceiling, O = U - $, W = L - j, H = m(W, t.decodedValues[F])
                        }

                        function P() {
                            function e(e) {
                                return (e - A) / T * 100
                            }

                            function b(n) {
                                return e(n) / 100 * W + j
                            }

                            function g(e) {
                                return t.encode(b(e))
                            }

                            function B(e) {
                                var n = e - j;
                                return O == W ? n = m(n, t.decodedValues[F]) / H : n /= W, 100 * n
                            }

                            function y(e) {
                                return B(t.decode(e))
                            }

                            function w(e) {
                                return a(e * T / 100)
                            }

                            function P(e) {
                                return Math.min(Math.max(e, A), E)
                            }

                            function R(n) {
                                return u(n, w(e(P(d(n)))))
                            }

                            function L(e, n, i) {
                                var r = e > 0 ? 1 : -1;
                                return n = n ? n : 100, i ? (Math.sin(Math.min(Math.abs(e / n), 1) * Math.PI - Math.PI / 2) + 1) * r * n / 6 : r * Math.pow(Math.min(Math.abs(e / n * 2), 1), t.stickiness) * n / 2
                            }

                            function q() {
                                var n = B(t.decodedValues[C]),
                                    i = y(t[C] + t[F]) - n,
                                    r = n - y(t[C] - t[F]),
                                    a = y(t[C] + t.buffer) - n,
                                    l = e(V + A),
                                    s = n + L(z, z > 0 ? i : r);
                                if (u(k.minPtr, w(s)), u(k.lowBub, w(e(d(k.minPtr) - f(k.lowBub) + V))), S) {
                                    var o = B(t.decodedValues[M]),
                                        c = y(t[M] + t[F]) - o,
                                        p = o - y(t[M] - t[F]),
                                        b = o - y(t[M] - t.buffer),
                                        h = o + L(D, D > 0 ? c : p);
                                    if (s > o - b && (s = n + L(z, a, !0), u(k.minPtr, w(s)), u(k.lowBub, w(e(d(k.minPtr) - f(k.lowBub) + V)))), h < n + a && (h = o + L(D, b, !0)), u(k.maxPtr, w(h)), u(k.highBub, w(e(d(k.maxPtr) - f(k.highBub) + V))), u(k.selBar, w(s + l)), k.selBar.css({
                                            width: w(h - s)
                                        }), u(k.selBub, w((s + h) / 2 - e(f(k.selBub) + A) + l)), u(k.cmbBub, w((s + h) / 2 - e(f(k.cmbBub) + A) + l)), k.unSelBarLow.css({
                                            left: 0,
                                            width: w(s + l)
                                        }), u(k.unSelBarHigh, w(h + l)), k.unSelBarHigh.css({
                                            right: 0
                                        }), AngularSlider.inputtypes.range) {
                                        var m = 2 * l,
                                            g = s + a / 2,
                                            v = 100 - g;
                                        g += m;
                                        var x = h - b / 2,
                                            P = s + m,
                                            I = h - s - m;
                                        h <= s + m && (P = s, I = h + m - s), k.minInput.css({
                                            width: w(x)
                                        }), k.maxInput.css({
                                            left: w(g),
                                            width: w(v)
                                        }), k.selInput.css({
                                            left: w(P),
                                            width: w(I)
                                        })
                                    }
                                }
                            }

                            function X() {
                                var e = k.lowBub;
                                R(k.lowBub), S && (R(k.highBub), R(k.selBub), p(k.lowBub, k.highBub) < 10 ? (s(k.lowBub), s(k.highBub), o(k.cmbBub), R(k.cmbBub), e = k.cmbBub) : (o(k.lowBub), o(k.highBub), s(k.cmbBub), e = k.highBub)), p(k.flrBub, k.lowBub) < 5 ? s(k.flrBub) : S && p(k.flrBub, e) < 5 ? s(k.flrBub) : o(k.flrBub), p(k.lowBub, k.ceilBub) < 5 ? s(k.ceilBub) : S && p(e, k.ceilBub) < 5 ? s(k.ceilBub) : o(k.ceilBub)
                            }

                            function _() {
                                z = 0, D = 0, ne && (q(), X(), ne.removeClass("active")), ne = null, te = null, Y = !1
                            }

                            function G(n) {
                                ne && t.$apply(function() {
                                    var i = n.clientX || n.x;
                                    if (Y) {
                                        var r = g(i) - Z,
                                            a = g(i) + ee;
                                        r < $ ? (a += $ - r, r = $) : a > U && (r -= a - U, a = U);
                                        var s = y(r),
                                            o = y(a);
                                        z = s, D = o, t[C] = r = h(r, t.precision, t[F], t.floor, t.ceiling), t[M] = a = h(a, t.precision, t[F], t.floor, t.ceiling), z -= y(r), D -= y(a)
                                    } else {
                                        var u = P(i + A - d(l) - f(ne)),
                                            c = e(u),
                                            p = t.encode(j + W * c / 100);
                                        if (z = c, S)
                                            if (t.buffer > 0) te === C ? p > t[M] - t.buffer && (p = t[M] - t.buffer) : p < t[C] + t.buffer && (p = t[C] + t.buffer);
                                            else if (te === C) {
                                            if (p > t[M]) {
                                                t[C] = t[M], t.decodedValues[C] = t.decodeRef(C), te = M;
                                                var b = k.minPtr;
                                                k.minPtr = k.maxPtr, k.maxPtr = b, k.maxPtr.removeClass("active").removeClass("high").addClass("low"), k.minPtr.addClass("active").removeClass("low").addClass("high")
                                            }
                                        } else if (p < t[C]) {
                                            t[M] = t[C], t.decodedValues[M] = t.decodeRef(M), te = C;
                                            var b = k.minPtr;
                                            k.minPtr = k.maxPtr, k.maxPtr = b, k.minPtr.removeClass("active").removeClass("low").addClass("high"), k.maxPtr.addClass("active").removeClass("high").addClass("low")
                                        }
                                        t[te] = p = h(p, t.precision, t[F], t.floor, t.ceiling), t.decodedValues[te] = t.decodeRef(te), te === C ? (z -= y(p), D = 0) : (D = z - y(p), z = 0)
                                    }
                                    t.ngChange && t.ngChange(), v.$setViewValue(t[C]), q(), X()
                                })
                            }

                            function K(e, n, i) {
                                if (!t.ngDisabled || 1 != t.ngDisabled) {
                                    var r = e.clientX || e.x;
                                    if (ne = n, te = i, ne.addClass("active"), te == I) {
                                        Y = !0;
                                        var a = g(r);
                                        Z = a - t[C], ee = t[M] - a
                                    }
                                    G(e)
                                }
                            }

                            function Q() {
                                function e(e, n, t) {
                                    function a(e) {
                                        K(e, n, t)
                                    }

                                    function l(e) {
                                        G(e), _()
                                    }
                                    e = r(e), i.bind(e, {
                                        start: a,
                                        move: G,
                                        end: l,
                                        cancel: _
                                    })
                                }

                                function t(e, n, t) {
                                    e = r(e), t = angular.isUndefined(t) ? e : r(t), i.bind(e, {
                                        start: function(e) {
                                            K(e, t, n)
                                        }
                                    })
                                }

                                function a(e) {
                                    e = r(e), i.bind(e, {
                                        move: G,
                                        end: function(e) {
                                            G(e), _()
                                        },
                                        cancel: _
                                    })
                                }
                                AngularSlider.inputtypes.range ? (e(k.minInput, k.minPtr, C), S && (e(k.maxInput, k.maxPtr, M), e(k.selInput, k.selBar, I))) : (a(n), t(k.minPtr, C), t(k.lowBub, C), t(k.flrBub, C, k.minPtr), S ? (t(k.maxPtr, M), t(k.highBub, M), t(k.ceilBub, M, k.maxPtr), t(k.selBar, I), t(k.selBub, I, k.selBar), t(k.unSelBarLow, C, k.minPtr), t(k.unSelBarHigh, M, k.maxPtr)) : (t(k.ceilBub, C, k.minPtr), t(k.fullBar, C, k.minPtr)))
                            }
                            var Y, Z, ee, ne, te;
                            x(), u(k.flrBub, 0), u(k.ceilBub, a(N - c(k.ceilBub))), q(), X(), J || (Q(), J = !0)
                        }
                        var k = w(y(l), S, AngularSlider.inputtypes.range);
                        t.decodedValues = {
                            floor: 0,
                            ceiling: 0,
                            step: 0,
                            stepWidth: 0,
                            precision: 0,
                            buffer: 0,
                            stickiness: 0,
                            ngModel: 0,
                            ngModelRange: 0
                        }, t.translation = function(e) {
                            return e = parseFloat(e).toFixed(t.precision), angular.isUndefined(b.translateFn) ? "" + e : t.translateFn({
                                value: e
                            })
                        }, t.rangeTranslation = function(e, n) {
                            return angular.isUndefined(b.translateRangeFn) ? "Range: " + t.translation((n - e).toFixed(t.precision)) : t.translateRangeFn({
                                low: e,
                                high: n
                            })
                        }, t.combinedTranslation = function(e, n) {
                            return angular.isUndefined(b.translateCombinedFn) ? t.translation(e) + " - " + t.translation(n) : t.translateCombinedFn({
                                low: e,
                                high: n
                            })
                        }, t.encode = function(e) {
                            return angular.isUndefined(b.scaleFn) || "" == b.scaleFn ? e : t.scaleFn({
                                value: e
                            })
                        }, t.decode = function(e) {
                            return angular.isUndefined(b.inverseScaleFn) || "" == b.inverseScaleFn ? e : t.inverseScaleFn({
                                value: e
                            })
                        }, 1 == Math.round(t.encode(t.decode(1))) && 100 == Math.round(t.encode(t.decode(100))) || console.warn("The scale and inverseScale functions are not perfect inverses: 1 = " + t.encode(t.decode(1)) + "  100 = " + t.encode(t.decode(100))), t.decodeRef = function(e) {
                            return t.decode(t[e])
                        }, t.inputSteps = function() {
                            return Math.pow(10, t.precision * -1)
                        };
                        for (var N = 0, V = 0, A = 0, E = 0, T = 0, $ = 0, j = 0, U = 0, L = 0, O = 0, W = 0, H = 1, z = 0, D = 0, J = !1, q = 0; q < R.length; q++) t.$watch(R[q], function() {
                            P()
                        });
                        r(window).bind("resize", function() {
                            P()
                        }), t.$on("refreshSlider", function() {
                            e(function() {
                                P()
                            })
                        }), e(function() {
                            P()
                        })
                    }
                }
            }
        }
    }]);
! function() {
    "use strict";
    var e = {
        TAB: 9,
        ENTER: 13,
        ESC: 27,
        SPACE: 32,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        HOME: 36,
        END: 35,
        BACKSPACE: 8,
        DELETE: 46,
        COMMAND: 91,
        MAP: {
            91: "COMMAND",
            8: "BACKSPACE",
            9: "TAB",
            13: "ENTER",
            16: "SHIFT",
            17: "CTRL",
            18: "ALT",
            19: "PAUSEBREAK",
            20: "CAPSLOCK",
            27: "ESC",
            32: "SPACE",
            33: "PAGE_UP",
            34: "PAGE_DOWN",
            35: "END",
            36: "HOME",
            37: "LEFT",
            38: "UP",
            39: "RIGHT",
            40: "DOWN",
            43: "+",
            44: "PRINTSCREEN",
            45: "INSERT",
            46: "DELETE",
            48: "0",
            49: "1",
            50: "2",
            51: "3",
            52: "4",
            53: "5",
            54: "6",
            55: "7",
            56: "8",
            57: "9",
            59: ";",
            61: "=",
            65: "A",
            66: "B",
            67: "C",
            68: "D",
            69: "E",
            70: "F",
            71: "G",
            72: "H",
            73: "I",
            74: "J",
            75: "K",
            76: "L",
            77: "M",
            78: "N",
            79: "O",
            80: "P",
            81: "Q",
            82: "R",
            83: "S",
            84: "T",
            85: "U",
            86: "V",
            87: "W",
            88: "X",
            89: "Y",
            90: "Z",
            96: "0",
            97: "1",
            98: "2",
            99: "3",
            100: "4",
            101: "5",
            102: "6",
            103: "7",
            104: "8",
            105: "9",
            106: "*",
            107: "+",
            109: "-",
            110: ".",
            111: "/",
            112: "F1",
            113: "F2",
            114: "F3",
            115: "F4",
            116: "F5",
            117: "F6",
            118: "F7",
            119: "F8",
            120: "F9",
            121: "F10",
            122: "F11",
            123: "F12",
            144: "NUMLOCK",
            145: "SCROLLLOCK",
            186: ";",
            187: "=",
            188: ",",
            189: "-",
            190: ".",
            191: "/",
            192: "`",
            219: "[",
            220: "\\",
            221: "]",
            222: "'"
        },
        isControl: function(t) {
            var i = t.which;
            switch (i) {
                case e.COMMAND:
                case e.SHIFT:
                case e.CTRL:
                case e.ALT:
                    return !0
            }
            return !!t.metaKey
        },
        isFunctionKey: function(e) {
            return e = e.which ? e.which : e, e >= 112 && e <= 123
        },
        isVerticalMovement: function(t) {
            return ~[e.UP, e.DOWN].indexOf(t)
        },
        isHorizontalMovement: function(t) {
            return ~[e.LEFT, e.RIGHT, e.BACKSPACE, e.DELETE].indexOf(t)
        }
    };
    void 0 === angular.element.prototype.querySelectorAll && (angular.element.prototype.querySelectorAll = function(e) {
        return angular.element(this[0].querySelectorAll(e))
    }), void 0 === angular.element.prototype.closest && (angular.element.prototype.closest = function(e) {
        for (var t = this[0], i = t.matches || t.webkitMatchesSelector || t.mozMatchesSelector || t.msMatchesSelector; t;) {
            if (i.bind(t)(e)) return t;
            t = t.parentElement
        }
        return !1
    });
    var t = 0,
        i = angular.module("ui.select", []).constant("uiSelectConfig", {
            theme: "bootstrap",
            searchEnabled: !0,
            sortable: !1,
            placeholder: "",
            refreshDelay: 1e3,
            closeOnSelect: !0,
            generateId: function() {
                return t++
            },
            appendToBody: !1
        }).service("uiSelectMinErr", function() {
            var e = angular.$$minErr("ui.select");
            return function() {
                var t = e.apply(this, arguments),
                    i = t.message.replace(new RegExp("\nhttp://errors.angularjs.org/.*"), "");
                return new Error(i)
            }
        }).directive("uisTranscludeAppend", function() {
            return {
                link: function(e, t, i, c, l) {
                    l(e, function(e) {
                        t.append(e)
                    })
                }
            }
        }).filter("highlight", function() {
            function e(e) {
                return e.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
            }
            return function(t, i) {
                return i && t ? t.replace(new RegExp(e(i), "gi"), '<span class="ui-select-highlight">$&</span>') : t
            }
        }).factory("uisOffset", ["$document", "$window", function(e, t) {
            return function(i) {
                var c = i[0].getBoundingClientRect();
                return {
                    width: c.width || i.prop("offsetWidth"),
                    height: c.height || i.prop("offsetHeight"),
                    top: c.top + (t.pageYOffset || e[0].documentElement.scrollTop),
                    left: c.left + (t.pageXOffset || e[0].documentElement.scrollLeft)
                }
            }
        }]);
    i.directive("uiSelectChoices", ["uiSelectConfig", "uisRepeatParser", "uiSelectMinErr", "$compile", function(e, t, i, c) {
        return {
            restrict: "EA",
            require: "^uiSelect",
            replace: !0,
            transclude: !0,
            templateUrl: function(t) {
                var i = t.parent().attr("theme") || e.theme;
                return i + "/choices.tpl.html"
            },
            compile: function(l, s) {
                if (!s.repeat) throw i("repeat", "Expected 'repeat' expression.");
                return function(l, s, n, a, r) {
                    var o = n.groupBy,
                        u = n.groupFilter;
                    if (a.parseRepeatAttr(n.repeat, o, u), a.disableChoiceExpression = n.uiDisableChoice, a.onHighlightCallback = n.onHighlight, o) {
                        var d = s.querySelectorAll(".ui-select-choices-group");
                        if (1 !== d.length) throw i("rows", "Expected 1 .ui-select-choices-group but got '{0}'.", d.length);
                        d.attr("ng-repeat", t.getGroupNgRepeatExpression())
                    }
                    var p = s.querySelectorAll(".ui-select-choices-row");
                    if (1 !== p.length) throw i("rows", "Expected 1 .ui-select-choices-row but got '{0}'.", p.length);
                    p.attr("ng-repeat", t.getNgRepeatExpression(a.parserResult.itemName, "$select.items", a.parserResult.trackByExp, o)).attr("ng-if", "$select.open").attr("ng-mouseenter", "$select.setActiveItem(" + a.parserResult.itemName + ")").attr("ng-click", "$select.select(" + a.parserResult.itemName + ",false,$event)");
                    var g = s.querySelectorAll(".ui-select-choices-row-inner");
                    if (1 !== g.length) throw i("rows", "Expected 1 .ui-select-choices-row-inner but got '{0}'.", g.length);
                    g.attr("uis-transclude-append", ""), c(s, r)(l), l.$watch("$select.search", function(e) {
                        e && !a.open && a.multiple && a.activate(!1, !0), a.activeIndex = a.tagging.isActivated ? -1 : 0, a.refresh(n.refresh)
                    }), n.$observe("refreshDelay", function() {
                        var t = l.$eval(n.refreshDelay);
                        a.refreshDelay = void 0 !== t ? t : e.refreshDelay
                    })
                }
            }
        }
    }]), i.controller("uiSelectCtrl", ["$scope", "$element", "$timeout", "$filter", "uisRepeatParser", "uiSelectMinErr", "uiSelectConfig", function(t, i, c, l, s, n, a) {
        function r() {
            (p.resetSearchInput || void 0 === p.resetSearchInput && a.resetSearchInput) && (p.search = g, p.selected && p.items.length && !p.multiple && (p.activeIndex = p.items.indexOf(p.selected)))
        }

        function o(e, t) {
            var i, c, l = [];
            for (i = 0; i < t.length; i++)
                for (c = 0; c < e.length; c++) e[c].name == [t[i]] && l.push(e[c]);
            return l
        }

        function u(t) {
            var i = !0;
            switch (t) {
                case e.DOWN:
                    !p.open && p.multiple ? p.activate(!1, !0) : p.activeIndex < p.items.length - 1 && p.activeIndex++;
                    break;
                case e.UP:
                    !p.open && p.multiple ? p.activate(!1, !0) : (p.activeIndex > 0 || 0 === p.search.length && p.tagging.isActivated && p.activeIndex > -1) && p.activeIndex--;
                    break;
                case e.TAB:
                    p.multiple && !p.open || p.select(p.items[p.activeIndex], !0);
                    break;
                case e.ENTER:
                    p.open && (p.tagging.isActivated || p.activeIndex >= 0) ? p.select(p.items[p.activeIndex]) : p.activate(!1, !0);
                    break;
                case e.ESC:
                    p.close();
                    break;
                default:
                    i = !1
            }
            return i
        }

        function d() {
            var e = i.querySelectorAll(".ui-select-choices-content"),
                t = e.querySelectorAll(".ui-select-choices-row");
            if (t.length < 1) throw n("choices", "Expected multiple .ui-select-choices-row but got '{0}'.", t.length);
            if (!(p.activeIndex < 0)) {
                var c = t[p.activeIndex],
                    l = c.offsetTop + c.clientHeight - e[0].scrollTop,
                    s = e[0].offsetHeight;
                l > s ? e[0].scrollTop += l - s : l < c.clientHeight && (p.isGrouped && 0 === p.activeIndex ? e[0].scrollTop = 0 : e[0].scrollTop -= c.clientHeight - l)
            }
        }
        var p = this,
            g = "";
        if (p.placeholder = a.placeholder, p.searchEnabled = a.searchEnabled, p.sortable = a.sortable, p.refreshDelay = a.refreshDelay, p.removeSelected = !1, p.closeOnSelect = !0, p.search = g, p.activeIndex = 0, p.items = [], p.open = !1, p.focus = !1, p.disabled = !1, p.selected = void 0, p.focusser = void 0, p.resetSearchInput = !0, p.multiple = void 0, p.disableChoiceExpression = void 0, p.tagging = {
                isActivated: !1,
                fct: void 0
            }, p.taggingTokens = {
                isActivated: !1,
                tokens: void 0
            }, p.lockChoiceExpression = void 0, p.clickTriggeredSelect = !1, p.$filter = l, p.searchInput = i.querySelectorAll("input.ui-select-search"), 1 !== p.searchInput.length) throw n("searchInput", "Expected 1 input.ui-select-search but got '{0}'.", p.searchInput.length);
        p.isEmpty = function() {
            return angular.isUndefined(p.selected) || null === p.selected || "" === p.selected
        }, p.activate = function(e, i) {
            p.disabled || p.open || (i || r(), t.$broadcast("uis:activate"), p.open = !0, p.activeIndex = p.activeIndex >= p.items.length ? 0 : p.activeIndex, p.activeIndex === -1 && p.taggingLabel !== !1 && (p.activeIndex = 0), c(function() {
                p.search = e || p.search, p.searchInput[0].focus()
            }))
        }, p.findGroupByName = function(e) {
            return p.groups && p.groups.filter(function(t) {
                return t.name === e
            })[0]
        }, p.parseRepeatAttr = function(e, i, c) {
            function l(e) {
                var l = t.$eval(i);
                if (p.groups = [], angular.forEach(e, function(e) {
                        var t = angular.isFunction(l) ? l(e) : e[l],
                            i = p.findGroupByName(t);
                        i ? i.items.push(e) : p.groups.push({
                            name: t,
                            items: [e]
                        })
                    }), c) {
                    var s = t.$eval(c);
                    angular.isFunction(s) ? p.groups = s(p.groups) : angular.isArray(s) && (p.groups = o(p.groups, s))
                }
                p.items = [], p.groups.forEach(function(e) {
                    p.items = p.items.concat(e.items)
                })
            }

            function a(e) {
                p.items = e
            }
            p.setItemsFn = i ? l : a, p.parserResult = s.parse(e), p.isGrouped = !!i, p.itemProperty = p.parserResult.itemName, p.refreshItems = function(e) {
                e = e || p.parserResult.source(t);
                var i = p.selected;
                if (angular.isArray(i) && !i.length || !p.removeSelected) p.setItemsFn(e);
                else if (void 0 !== e) {
                    var c = e.filter(function(e) {
                        return i.indexOf(e) < 0
                    });
                    p.setItemsFn(c)
                }
            }, t.$watchCollection(p.parserResult.source, function(e) {
                if (void 0 === e || null === e) p.items = [];
                else {
                    if (!angular.isArray(e)) throw n("items", "Expected an array but got '{0}'.", e);
                    p.refreshItems(e), p.ngModel.$modelValue = null
                }
            })
        };
        var h;
        p.refresh = function(e) {
            void 0 !== e && (h && c.cancel(h), h = c(function() {
                t.$eval(e)
            }, p.refreshDelay))
        }, p.setActiveItem = function(e) {
            p.activeIndex = p.items.indexOf(e)
        }, p.isActive = function(e) {
            if (!p.open) return !1;
            var t = p.items.indexOf(e[p.itemProperty]),
                i = t === p.activeIndex;
            return !(!i || t < 0 && p.taggingLabel !== !1 || t < 0 && p.taggingLabel === !1) && (i && !angular.isUndefined(p.onHighlightCallback) && e.$eval(p.onHighlightCallback), i)
        }, p.isDisabled = function(e) {
            if (p.open) {
                var t, i = p.items.indexOf(e[p.itemProperty]),
                    c = !1;
                return i >= 0 && !angular.isUndefined(p.disableChoiceExpression) && (t = p.items[i], c = !!e.$eval(p.disableChoiceExpression), t._uiSelectChoiceDisabled = c), c
            }
        }, p.select = function(e, i, l) {
            if (void 0 === e || !e._uiSelectChoiceDisabled) {
                if (!p.items && !p.search) return;
                if (!e || !e._uiSelectChoiceDisabled) {
                    if (p.tagging.isActivated) {
                        if (p.taggingLabel === !1)
                            if (p.activeIndex < 0) {
                                if (e = void 0 !== p.tagging.fct ? p.tagging.fct(p.search) : p.search, !e || angular.equals(p.items[0], e)) return
                            } else e = p.items[p.activeIndex];
                        else if (0 === p.activeIndex) {
                            if (void 0 === e) return;
                            if (void 0 !== p.tagging.fct && "string" == typeof e) {
                                if (e = p.tagging.fct(p.search), !e) return
                            } else "string" == typeof e && (e = e.replace(p.taggingLabel, "").trim())
                        }
                        if (p.selected && angular.isArray(p.selected) && p.selected.filter(function(t) {
                                return angular.equals(t, e)
                            }).length > 0) return void p.close(i)
                    }
                    t.$broadcast("uis:select", e);
                    var s = {};
                    s[p.parserResult.itemName] = e, c(function() {
                        p.onSelectCallback(t, {
                            $item: e,
                            $model: p.parserResult.modelMapper(t, s)
                        })
                    }), p.closeOnSelect && p.close(i), l && "click" === l.type && (p.clickTriggeredSelect = !0)
                }
            }
        }, p.close = function(e) {
            p.open && (p.ngModel && p.ngModel.$setTouched && p.ngModel.$setTouched(), r(), p.open = !1, t.$broadcast("uis:close", e))
        }, p.setFocus = function() {
            p.focus || p.focusInput[0].focus()
        }, p.clear = function(e) {
            p.select(void 0), e.stopPropagation(), c(function() {
                p.focusser[0].focus()
            }, 0, !1)
        }, p.toggle = function(e) {
            p.open ? (p.close(), e.preventDefault(), e.stopPropagation()) : p.activate()
        }, p.isLocked = function(e, t) {
            var i, c = p.selected[t];
            return c && !angular.isUndefined(p.lockChoiceExpression) && (i = !!e.$eval(p.lockChoiceExpression), c._uiSelectChoiceLocked = i), i
        };
        var f = null;
        p.sizeSearchInput = function() {
            var e = p.searchInput[0],
                i = p.searchInput.parent().parent()[0],
                l = function() {
                    return i.clientWidth * !!e.offsetParent
                },
                s = function(t) {
                    if (0 === t) return !1;
                    var i = t - e.offsetLeft - 10;
                    return i < 50 && (i = t), p.searchInput.css("width", i + "px"), !0
                };
            p.searchInput.css("width", "10px"), c(function() {
                null !== f || s(l()) || (f = t.$watch(l, function(e) {
                    s(e) && (f(), f = null)
                }))
            })
        }, p.searchInput.on("keydown", function(i) {
            var l = i.which;
            t.$apply(function() {
                var t = !1;
                if ((p.items.length > 0 || p.tagging.isActivated) && (u(l), p.taggingTokens.isActivated)) {
                    for (var s = 0; s < p.taggingTokens.tokens.length; s++) p.taggingTokens.tokens[s] === e.MAP[i.keyCode] && p.search.length > 0 && (t = !0);
                    t && c(function() {
                        p.searchInput.triggerHandler("tagged");
                        var t = p.search.replace(e.MAP[i.keyCode], "").trim();
                        p.tagging.fct && (t = p.tagging.fct(t)), t && p.select(t, !0)
                    })
                }
            }), e.isVerticalMovement(l) && p.items.length > 0 && d(), l !== e.ENTER && l !== e.ESC || (i.preventDefault(), i.stopPropagation())
        }), p.searchInput.on("paste", function(e) {
            var t = e.originalEvent.clipboardData.getData("text/plain");
            if (t && t.length > 0 && p.taggingTokens.isActivated && p.tagging.fct) {
                var i = t.split(p.taggingTokens.tokens[0]);
                i && i.length > 0 && (angular.forEach(i, function(e) {
                    var t = p.tagging.fct(e);
                    t && p.select(t, !0)
                }), e.preventDefault(), e.stopPropagation())
            }
        }), p.searchInput.on("tagged", function() {
            c(function() {
                r()
            })
        }), t.$on("$destroy", function() {
            p.searchInput.off("keyup keydown tagged blur paste")
        })
    }]), i.directive("uiSelect", ["$document", "uiSelectConfig", "uiSelectMinErr", "uisOffset", "$compile", "$parse", "$timeout", function(e, t, i, c, l, s, n) {
        return {
            restrict: "EA",
            templateUrl: function(e, i) {
                var c = i.theme || t.theme;
                return c + (angular.isDefined(i.multiple) ? "/select-multiple.tpl.html" : "/select.tpl.html")
            },
            replace: !0,
            transclude: !0,
            require: ["uiSelect", "^ngModel"],
            scope: !0,
            controller: "uiSelectCtrl",
            controllerAs: "$select",
            compile: function(l, a) {
                return angular.isDefined(a.multiple) ? l.append("<ui-select-multiple/>").removeAttr("multiple") : l.append("<ui-select-single/>"),
                    function(l, a, r, o, u) {
                        function d(e) {
                            if (h.open) {
                                var t = !1;
                                if (t = window.jQuery ? window.jQuery.contains(a[0], e.target) : a[0].contains(e.target), !t && !h.clickTriggeredSelect) {
                                    var i = ["input", "button", "textarea"],
                                        c = angular.element(e.target).scope(),
                                        s = c && c.$select && c.$select !== h;
                                    s || (s = ~i.indexOf(e.target.tagName.toLowerCase())), h.close(s), l.$digest()
                                }
                                h.clickTriggeredSelect = !1
                            }
                        }

                        function p() {
                            var t = c(a);
                            m = angular.element('<div class="ui-select-placeholder"></div>'), m[0].style.width = t.width + "px", m[0].style.height = t.height + "px", a.after(m), $ = a[0].style.width, e.find("body").append(a), a[0].style.position = "absolute", a[0].style.left = t.left + "px", a[0].style.top = t.top + "px", a[0].style.width = t.width + "px"
                        }

                        function g() {
                            null !== m && (m.replaceWith(a), m = null, a[0].style.position = "", a[0].style.left = "", a[0].style.top = "", a[0].style.width = $)
                        }
                        var h = o[0],
                            f = o[1];
                        h.generatedId = t.generateId(), h.baseTitle = r.title || "Select box", h.focusserTitle = h.baseTitle + " focus", h.focusserId = "focusser-" + h.generatedId, h.closeOnSelect = function() {
                            return angular.isDefined(r.closeOnSelect) ? s(r.closeOnSelect)() : t.closeOnSelect
                        }(), h.onSelectCallback = s(r.onSelect), h.onRemoveCallback = s(r.onRemove), h.ngModel = f, h.choiceGrouped = function(e) {
                            return h.isGrouped && e && e.name
                        }, r.tabindex && r.$observe("tabindex", function(e) {
                            h.focusInput.attr("tabindex", e), a.removeAttr("tabindex")
                        }), l.$watch("searchEnabled", function() {
                            var e = l.$eval(r.searchEnabled);
                            h.searchEnabled = void 0 !== e ? e : t.searchEnabled
                        }), l.$watch("sortable", function() {
                            var e = l.$eval(r.sortable);
                            h.sortable = void 0 !== e ? e : t.sortable
                        }), r.$observe("disabled", function() {
                            h.disabled = void 0 !== r.disabled && r.disabled
                        }), r.$observe("resetSearchInput", function() {
                            var e = l.$eval(r.resetSearchInput);
                            h.resetSearchInput = void 0 === e || e
                        }), r.$observe("tagging", function() {
                            if (void 0 !== r.tagging) {
                                var e = l.$eval(r.tagging);
                                h.tagging = {
                                    isActivated: !0,
                                    fct: e !== !0 ? e : void 0
                                }
                            } else h.tagging = {
                                isActivated: !1,
                                fct: void 0
                            }
                        }), r.$observe("taggingLabel", function() {
                            void 0 !== r.tagging && ("false" === r.taggingLabel ? h.taggingLabel = !1 : h.taggingLabel = void 0 !== r.taggingLabel ? r.taggingLabel : "(new)")
                        }), r.$observe("taggingTokens", function() {
                            if (void 0 !== r.tagging) {
                                var e = void 0 !== r.taggingTokens ? r.taggingTokens.split("|") : [",", "ENTER"];
                                h.taggingTokens = {
                                    isActivated: !0,
                                    tokens: e
                                }
                            }
                        }), angular.isDefined(r.autofocus) && n(function() {
                            h.setFocus()
                        }), angular.isDefined(r.focusOn) && l.$on(r.focusOn, function() {
                            n(function() {
                                h.setFocus()
                            })
                        }), e.on("click", d), l.$on("$destroy", function() {
                            e.off("click", d)
                        }), u(l, function(e) {
                            var t = angular.element("<div>").append(e),
                                c = t.querySelectorAll(".ui-select-match");
                            if (c.removeAttr("ui-select-match"), c.removeAttr("data-ui-select-match"), 1 !== c.length) throw i("transcluded", "Expected 1 .ui-select-match but got '{0}'.", c.length);
                            a.querySelectorAll(".ui-select-match").replaceWith(c);
                            var l = t.querySelectorAll(".ui-select-choices");
                            if (l.removeAttr("ui-select-choices"), l.removeAttr("data-ui-select-choices"), 1 !== l.length) throw i("transcluded", "Expected 1 .ui-select-choices but got '{0}'.", l.length);
                            a.querySelectorAll(".ui-select-choices").replaceWith(l)
                        });
                        var v = l.$eval(r.appendToBody);
                        (void 0 !== v ? v : t.appendToBody) && (l.$watch("$select.open", function(e) {
                            e ? p() : g()
                        }), l.$on("$destroy", function() {
                            g()
                        }));
                        var m = null,
                            $ = "",
                            b = null,
                            x = "direction-up";
                        l.$watch("$select.open", function(t) {
                            if (t) {
                                if (b = angular.element(a).querySelectorAll(".ui-select-dropdown"), null === b) return;
                                b[0].style.visibility = "hidden", n(function() {
                                    var t = c(a),
                                        i = c(b);
                                    t.top + t.height + i.height > e[0].documentElement.scrollTop + e[0].documentElement.clientHeight && (b[0].style.position = "absolute", b[0].style.top = i.height * -1 + "px", a.addClass(x)), b[0].style.visibility = ""
                                })
                            } else {
                                if (null === b) return;
                                b[0].style.position = "", b[0].style.top = "", a.removeClass(x)
                            }
                        })
                    }
            }
        }
    }]), i.directive("uiSelectMatch", ["uiSelectConfig", function(e) {
        return {
            restrict: "EA",
            require: "^uiSelect",
            replace: !0,
            transclude: !0,
            templateUrl: function(t) {
                var i = t.parent().attr("theme") || e.theme,
                    c = t.parent().attr("multiple");
                return i + (c ? "/match-multiple.tpl.html" : "/match.tpl.html")
            },
            link: function(t, i, c, l) {
                function s(e) {
                    l.allowClear = !!angular.isDefined(e) && ("" === e || "true" === e.toLowerCase())
                }
                l.lockChoiceExpression = c.uiLockChoice, c.$observe("placeholder", function(t) {
                    l.placeholder = void 0 !== t ? t : e.placeholder
                }), c.$observe("allowClear", s), s(c.allowClear), l.multiple && l.sizeSearchInput()
            }
        }
    }]), i.directive("uiSelectMultiple", ["uiSelectMinErr", "$timeout", function(t, i) {
        return {
            restrict: "EA",
            require: ["^uiSelect", "^ngModel"],
            controller: ["$scope", "$timeout", function(e, t) {
                var i, c = this,
                    l = e.$select;
                e.$evalAsync(function() {
                    i = e.ngModel
                }), c.activeMatchIndex = -1, c.updateModel = function() {
                    i.$setViewValue(Date.now()), c.refreshComponent()
                }, c.refreshComponent = function() {
                    l.refreshItems(), l.sizeSearchInput()
                }, c.removeChoice = function(i) {
                    var s = l.selected[i];
                    if (!s._uiSelectChoiceLocked) {
                        var n = {};
                        n[l.parserResult.itemName] = s, l.selected.splice(i, 1), c.activeMatchIndex = -1, l.sizeSearchInput(), t(function() {
                            l.onRemoveCallback(e, {
                                $item: s,
                                $model: l.parserResult.modelMapper(e, n)
                            })
                        }), c.updateModel()
                    }
                }, c.getPlaceholder = function() {
                    if (!l.selected.length) return l.placeholder
                }
            }],
            controllerAs: "$selectMultiple",
            link: function(c, l, s, n) {
                function a(e) {
                    return angular.isNumber(e.selectionStart) ? e.selectionStart : e.value.length
                }

                function r(t) {
                    function i() {
                        switch (t) {
                            case e.LEFT:
                                return ~g.activeMatchIndex ? u : n;
                            case e.RIGHT:
                                return ~g.activeMatchIndex && r !== n ? o : (d.activate(), !1);
                            case e.BACKSPACE:
                                return ~g.activeMatchIndex ? (g.removeChoice(r), u) : n;
                            case e.DELETE:
                                return !!~g.activeMatchIndex && (g.removeChoice(g.activeMatchIndex), r)
                        }
                    }
                    var c = a(d.searchInput[0]),
                        l = d.selected.length,
                        s = 0,
                        n = l - 1,
                        r = g.activeMatchIndex,
                        o = g.activeMatchIndex + 1,
                        u = g.activeMatchIndex - 1,
                        p = r;
                    return !(c > 0 || d.search.length && t == e.RIGHT) && (d.close(), p = i(), d.selected.length && p !== !1 ? g.activeMatchIndex = Math.min(n, Math.max(s, p)) : g.activeMatchIndex = -1, !0)
                }

                function o(e) {
                    if (void 0 === e || void 0 === d.search) return !1;
                    var t = e.filter(function(e) {
                        return void 0 !== d.search.toUpperCase() && void 0 !== e && e.toUpperCase() === d.search.toUpperCase()
                    }).length > 0;
                    return t
                }

                function u(e, t) {
                    var i = -1;
                    if (angular.isArray(e))
                        for (var c = angular.copy(e), l = 0; l < c.length; l++)
                            if (void 0 === d.tagging.fct) c[l] + " " + d.taggingLabel === t && (i = l);
                            else {
                                var s = c[l];
                                s.isTag = !0, angular.equals(s, t) && (i = l)
                            }
                    return i
                }
                var d = n[0],
                    p = c.ngModel = n[1],
                    g = c.$selectMultiple;
                d.multiple = !0, d.removeSelected = !0, d.focusInput = d.searchInput, p.$parsers.unshift(function() {
                    for (var e, t = {}, i = [], l = d.selected.length - 1; l >= 0; l--) t = {}, t[d.parserResult.itemName] = d.selected[l], e = d.parserResult.modelMapper(c, t), i.unshift(e);
                    return i
                }), p.$formatters.unshift(function(e) {
                    var t, i = d.parserResult.source(c, {
                            $select: {
                                search: ""
                            }
                        }),
                        l = {};
                    if (!i) return e;
                    var s = [],
                        n = function(e, i) {
                            if (e && e.length) {
                                for (var n = e.length - 1; n >= 0; n--) {
                                    if (l[d.parserResult.itemName] = e[n], t = d.parserResult.modelMapper(c, l), d.parserResult.trackByExp) {
                                        var a = /\.(.+)/.exec(d.parserResult.trackByExp);
                                        if (a.length > 0 && t[a[1]] == i[a[1]]) return s.unshift(e[n]), !0
                                    }
                                    if (angular.equals(t, i)) return s.unshift(e[n]), !0
                                }
                                return !1
                            }
                        };
                    if (!e) return s;
                    for (var a = e.length - 1; a >= 0; a--) n(d.selected, e[a]) || n(i, e[a]) || s.unshift(e[a]);
                    return s
                }), c.$watchCollection(function() {
                    return p.$modelValue
                }, function(e, t) {
                    t != e && (p.$modelValue = null, g.refreshComponent())
                }), p.$render = function() {
                    if (!angular.isArray(p.$viewValue)) {
                        if (!angular.isUndefined(p.$viewValue) && null !== p.$viewValue) throw t("multiarr", "Expected model value to be array but got '{0}'", p.$viewValue);
                        d.selected = []
                    }
                    d.selected = p.$viewValue, c.$evalAsync()
                }, c.$on("uis:select", function(e, t) {
                    d.selected.push(t), g.updateModel()
                }), c.$on("uis:activate", function() {
                    g.activeMatchIndex = -1
                }), c.$watch("$select.disabled", function(e, t) {
                    t && !e && d.sizeSearchInput()
                }), d.searchInput.on("keydown", function(t) {
                    var i = t.which;
                    c.$apply(function() {
                        var c = !1;
                        e.isHorizontalMovement(i) && (c = r(i)), c && i != e.TAB && (t.preventDefault(), t.stopPropagation())
                    })
                }), d.searchInput.on("keyup", function(t) {
                    if (e.isVerticalMovement(t.which) || c.$evalAsync(function() {
                            d.activeIndex = d.taggingLabel === !1 ? -1 : 0
                        }), d.tagging.isActivated && d.search.length > 0) {
                        if (t.which === e.TAB || e.isControl(t) || e.isFunctionKey(t) || t.which === e.ESC || e.isVerticalMovement(t.which)) return;
                        if (d.activeIndex = d.taggingLabel === !1 ? -1 : 0, d.taggingLabel === !1) return;
                        var i, l, s, n, a = angular.copy(d.items),
                            r = angular.copy(d.items),
                            p = !1,
                            g = -1;
                        if (void 0 !== d.tagging.fct) {
                            if (s = d.$filter("filter")(a, {
                                    isTag: !0
                                }), s.length > 0 && (n = s[0]), a.length > 0 && n && (p = !0, a = a.slice(1, a.length), r = r.slice(1, r.length)), i = d.tagging.fct(d.search), i.isTag = !0, r.filter(function(e) {
                                    return angular.equals(e, d.tagging.fct(d.search))
                                }).length > 0) return;
                            i.isTag = !0
                        } else {
                            if (s = d.$filter("filter")(a, function(e) {
                                    return e.match(d.taggingLabel)
                                }), s.length > 0 && (n = s[0]), l = a[0], void 0 !== l && a.length > 0 && n && (p = !0, a = a.slice(1, a.length), r = r.slice(1, r.length)), i = d.search + " " + d.taggingLabel, u(d.selected, d.search) > -1) return;
                            if (o(r.concat(d.selected))) return void(p && (a = r, c.$evalAsync(function() {
                                d.activeIndex = 0, d.items = a
                            })));
                            if (o(r)) return void(p && (d.items = r.slice(1, r.length)))
                        }
                        p && (g = u(d.selected, i)), g > -1 ? a = a.slice(g + 1, a.length - 1) : (a = [], a.push(i), a = a.concat(r)), c.$evalAsync(function() {
                            d.activeIndex = 0, d.items = a
                        })
                    }
                }), d.searchInput.on("blur", function() {
                    i(function() {
                        g.activeMatchIndex = -1
                    })
                })
            }
        }
    }]), i.directive("uiSelectSingle", ["$timeout", "$compile", function(t, i) {
        return {
            restrict: "EA",
            require: ["^uiSelect", "^ngModel"],
            link: function(c, l, s, n) {
                var a = n[0],
                    r = n[1];
                r.$parsers.unshift(function(e) {
                    var t, i = {};
                    return i[a.parserResult.itemName] = e, t = a.parserResult.modelMapper(c, i)
                }), r.$formatters.unshift(function(e) {
                    var t, i = a.parserResult.source(c, {
                            $select: {
                                search: ""
                            }
                        }),
                        l = {};
                    if (i) {
                        var s = function(i) {
                            return l[a.parserResult.itemName] = i, t = a.parserResult.modelMapper(c, l), t == e
                        };
                        if (a.selected && s(a.selected)) return a.selected;
                        for (var n = i.length - 1; n >= 0; n--)
                            if (s(i[n])) return i[n]
                    }
                    return e
                }), c.$watch("$select.selected", function(e) {
                    r.$viewValue !== e && r.$setViewValue(e)
                }), r.$render = function() {
                    a.selected = r.$viewValue
                }, c.$on("uis:select", function(e, t) {
                    a.selected = t
                }), c.$on("uis:close", function(e, i) {
                    t(function() {
                        a.focusser.prop("disabled", !1), i || a.focusser[0].focus()
                    }, 0, !1)
                }), c.$on("uis:activate", function() {
                    o.prop("disabled", !0)
                });
                var o = angular.element("<input ng-disabled='$select.disabled' class='ui-select-focusser ui-select-offscreen' type='text' id='{{ $select.focusserId }}' aria-label='{{ $select.focusserTitle }}' aria-haspopup='true' role='button' />");
                i(o)(c), a.focusser = o, a.focusInput = o, l.parent().append(o), o.bind("focus", function() {
                    c.$evalAsync(function() {
                        a.focus = !0
                    })
                }), o.bind("blur", function() {
                    c.$evalAsync(function() {
                        a.focus = !1
                    })
                }), o.bind("keydown", function(t) {
                    return t.which === e.BACKSPACE ? (t.preventDefault(), t.stopPropagation(), a.select(void 0), void c.$apply()) : void(t.which === e.TAB || e.isControl(t) || e.isFunctionKey(t) || t.which === e.ESC || (t.which != e.DOWN && t.which != e.UP && t.which != e.ENTER && t.which != e.SPACE || (t.preventDefault(), t.stopPropagation(), a.activate()), c.$digest()))
                }), o.bind("keyup input", function(t) {
                    t.which === e.TAB || e.isControl(t) || e.isFunctionKey(t) || t.which === e.ESC || t.which == e.ENTER || t.which === e.BACKSPACE || (a.activate(o.val()), o.val(""), c.$digest())
                })
            }
        }
    }]), i.directive("uiSelectSort", ["$timeout", "uiSelectConfig", "uiSelectMinErr", function(e, t, i) {
        return {
            require: "^uiSelect",
            link: function(t, c, l, s) {
                if (null === t[l.uiSelectSort]) throw i("sort", "Expected a list to sort");
                var n = angular.extend({
                        axis: "horizontal"
                    }, t.$eval(l.uiSelectSortOptions)),
                    a = n.axis,
                    r = "dragging",
                    o = "dropping",
                    u = "dropping-before",
                    d = "dropping-after";
                t.$watch(function() {
                    return s.sortable
                }, function(e) {
                    e ? c.attr("draggable", !0) : c.removeAttr("draggable")
                }), c.on("dragstart", function(e) {
                    c.addClass(r), (e.dataTransfer || e.originalEvent.dataTransfer).setData("text/plain", t.$index)
                }), c.on("dragend", function() {
                    c.removeClass(r)
                });
                var p, g = function(e, t) {
                        this.splice(t, 0, this.splice(e, 1)[0])
                    },
                    h = function(e) {
                        e.preventDefault();
                        var t = "vertical" === a ? e.offsetY || e.layerY || (e.originalEvent ? e.originalEvent.offsetY : 0) : e.offsetX || e.layerX || (e.originalEvent ? e.originalEvent.offsetX : 0);
                        t < this["vertical" === a ? "offsetHeight" : "offsetWidth"] / 2 ? (c.removeClass(d), c.addClass(u)) : (c.removeClass(u), c.addClass(d))
                    },
                    f = function(t) {
                        t.preventDefault();
                        var i = parseInt((t.dataTransfer || t.originalEvent.dataTransfer).getData("text/plain"), 10);
                        e.cancel(p), p = e(function() {
                            v(i)
                        }, 20)
                    },
                    v = function(e) {
                        var i = t.$eval(l.uiSelectSort),
                            s = i[e],
                            n = null;
                        n = c.hasClass(u) ? e < t.$index ? t.$index - 1 : t.$index : e < t.$index ? t.$index : t.$index + 1, g.apply(i, [e, n]), t.$apply(function() {
                            t.$emit("uiSelectSort:change", {
                                array: i,
                                item: s,
                                from: e,
                                to: n
                            })
                        }), c.removeClass(o), c.removeClass(u), c.removeClass(d), c.off("drop", f)
                    };
                c.on("dragenter", function() {
                    c.hasClass(r) || (c.addClass(o), c.on("dragover", h), c.on("drop", f))
                }), c.on("dragleave", function(e) {
                    e.target == c && (c.removeClass(o), c.removeClass(u), c.removeClass(d), c.off("dragover", h), c.off("drop", f))
                })
            }
        }
    }]), i.service("uisRepeatParser", ["uiSelectMinErr", "$parse", function(e, t) {
        var i = this;
        i.parse = function(i) {
            var c = i.match(/^\s*(?:([\s\S]+?)\s+as\s+)?([\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
            if (!c) throw e("iexp", "Expected expression in form of '_item_ in _collection_[ track by _id_]' but got '{0}'.", i);
            return {
                itemName: c[2],
                source: t(c[3]),
                trackByExp: c[4],
                modelMapper: t(c[1] || c[2])
            }
        }, i.getGroupNgRepeatExpression = function() {
            return "$group in $select.groups"
        }, i.getNgRepeatExpression = function(e, t, i, c) {
            var l = e + " in " + (c ? "$group.items" : t);
            return i && (l += " track by " + i), l
        }
    }])
}(), angular.module("ui.select").run(["$templateCache", function(e) {
    e.put("bootstrap/choices.tpl.html", '<ul class="ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu" role="listbox" ng-show="$select.items.length > 0"><li class="ui-select-choices-group" id="ui-select-choices-{{ $select.generatedId }}"><div class="divider" ng-show="$select.isGrouped && $index > 0"></div><div ng-show="$select.isGrouped" class="ui-select-choices-group-label dropdown-header" ng-bind="$group.name"></div><div id="ui-select-choices-row-{{ $select.generatedId }}-{{$index}}" class="ui-select-choices-row" ng-class="{active: $select.isActive(this), disabled: $select.isDisabled(this)}" role="option"><a href="javascript:void(0)" class="ui-select-choices-row-inner"></a></div></li></ul>'), e.put("bootstrap/match-multiple.tpl.html", '<span class="ui-select-match"><span ng-repeat="$item in $select.selected"><span class="ui-select-match-item btn btn-default btn-xs" tabindex="-1" type="button" ng-disabled="$select.disabled" ng-click="$selectMultiple.activeMatchIndex = $index;" ng-class="{\'btn-primary\':$selectMultiple.activeMatchIndex === $index, \'select-locked\':$select.isLocked(this, $index)}" ui-select-sort="$select.selected"><span class="close ui-select-match-close" ng-hide="$select.disabled" ng-click="$selectMultiple.removeChoice($index)">&nbsp;&times;</span> <span uis-transclude-append=""></span></span></span></span>'), e.put("bootstrap/match.tpl.html", '<div class="ui-select-match" ng-hide="$select.open" ng-disabled="$select.disabled" ng-class="{\'btn-default-focus\':$select.focus}"><span tabindex="-1" class="btn btn-default form-control ui-select-toggle" aria-label="{{ $select.baseTitle }} activate" ng-disabled="$select.disabled" ng-click="$select.activate()" style="outline: 0;"><span ng-show="$select.isEmpty()" class="ui-select-placeholder text-muted">{{$select.placeholder}}</span> <span ng-hide="$select.isEmpty()" class="ui-select-match-text pull-left" ng-class="{\'ui-select-allow-clear\': $select.allowClear && !$select.isEmpty()}" ng-transclude=""></span> <i class="caret pull-right" ng-click="$select.toggle($event)"></i> <a ng-show="$select.allowClear && !$select.isEmpty()" aria-label="{{ $select.baseTitle }} clear" style="margin-right: 10px" ng-click="$select.clear($event)" class="btn btn-xs btn-link pull-right"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></a></span></div>'), e.put("bootstrap/select-multiple.tpl.html", '<div class="ui-select-container ui-select-multiple ui-select-bootstrap dropdown form-control" ng-class="{open: $select.open}"><div><div class="ui-select-match"></div><input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-select-search input-xs" placeholder="{{$selectMultiple.getPlaceholder()}}" ng-disabled="$select.disabled" ng-hide="$select.disabled" ng-click="$select.activate()" ng-model="$select.search" role="combobox" aria-label="{{ $select.baseTitle }}" ondrop="return false;"></div><div class="ui-select-choices"></div></div>'), e.put("bootstrap/select.tpl.html", '<div class="ui-select-container ui-select-bootstrap dropdown" ng-class="{open: $select.open}"><div class="ui-select-match"></div><input type="text" autocomplete="off" tabindex="-1" aria-expanded="true" aria-label="{{ $select.baseTitle }}" aria-owns="ui-select-choices-{{ $select.generatedId }}" aria-activedescendant="ui-select-choices-row-{{ $select.generatedId }}-{{ $select.activeIndex }}" class="form-control ui-select-search" placeholder="{{$select.placeholder}}" ng-model="$select.search" ng-show="$select.searchEnabled && $select.open"><div class="ui-select-choices"></div></div>'), e.put("select2/choices.tpl.html", '<ul class="ui-select-choices ui-select-choices-content select2-results"><li class="ui-select-choices-group" ng-class="{\'select2-result-with-children\': $select.choiceGrouped($group) }"><div ng-show="$select.choiceGrouped($group)" class="ui-select-choices-group-label select2-result-label" ng-bind="$group.name"></div><ul role="listbox" id="ui-select-choices-{{ $select.generatedId }}" ng-class="{\'select2-result-sub\': $select.choiceGrouped($group), \'select2-result-single\': !$select.choiceGrouped($group) }"><li role="option" id="ui-select-choices-row-{{ $select.generatedId }}-{{$index}}" class="ui-select-choices-row" ng-class="{\'select2-highlighted\': $select.isActive(this), \'select2-disabled\': $select.isDisabled(this)}"><div class="select2-result-label ui-select-choices-row-inner"></div></li></ul></li></ul>'), e.put("select2/match-multiple.tpl.html", '<span class="ui-select-match"><li class="ui-select-match-item select2-search-choice" ng-repeat="$item in $select.selected" ng-class="{\'select2-search-choice-focus\':$selectMultiple.activeMatchIndex === $index, \'select2-locked\':$select.isLocked(this, $index)}" ui-select-sort="$select.selected"><span uis-transclude-append=""></span> <a href="javascript:;" class="ui-select-match-close select2-search-choice-close" ng-click="$selectMultiple.removeChoice($index)" tabindex="-1"></a></li></span>'), e.put("select2/match.tpl.html", '<a class="select2-choice ui-select-match" ng-class="{\'select2-default\': $select.isEmpty()}" ng-click="$select.toggle($event)" aria-label="{{ $select.baseTitle }} select"><span ng-show="$select.isEmpty()" class="select2-chosen">{{$select.placeholder}}</span> <span ng-hide="$select.isEmpty()" class="select2-chosen" ng-transclude=""></span> <abbr ng-if="$select.allowClear && !$select.isEmpty()" class="select2-search-choice-close" ng-click="$select.clear($event)"></abbr> <span class="select2-arrow ui-select-toggle"><b></b></span></a>'), e.put("select2/select-multiple.tpl.html", '<div class="ui-select-container ui-select-multiple select2 select2-container select2-container-multi" ng-class="{\'select2-container-active select2-dropdown-open open\': $select.open, \'select2-container-disabled\': $select.disabled}"><ul class="select2-choices"><span class="ui-select-match"></span><li class="select2-search-field"><input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="combobox" aria-expanded="true" aria-owns="ui-select-choices-{{ $select.generatedId }}" aria-label="{{ $select.baseTitle }}" aria-activedescendant="ui-select-choices-row-{{ $select.generatedId }}-{{ $select.activeIndex }}" class="select2-input ui-select-search" placeholder="{{$selectMultiple.getPlaceholder()}}" ng-disabled="$select.disabled" ng-hide="$select.disabled" ng-model="$select.search" ng-click="$select.activate()" style="width: 34px;" ondrop="return false;"></li></ul><div class="ui-select-dropdown select2-drop select2-with-searchbox select2-drop-active" ng-class="{\'select2-display-none\': !$select.open}"><div class="ui-select-choices"></div></div></div>'),
        e.put("select2/select.tpl.html", '<div class="ui-select-container select2 select2-container" ng-class="{\'select2-container-active select2-dropdown-open open\': $select.open, \'select2-container-disabled\': $select.disabled, \'select2-container-active\': $select.focus, \'select2-allowclear\': $select.allowClear && !$select.isEmpty()}"><div class="ui-select-match"></div><div class="ui-select-dropdown select2-drop select2-with-searchbox select2-drop-active" ng-class="{\'select2-display-none\': !$select.open}"><div class="select2-search" ng-show="$select.searchEnabled"><input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="combobox" aria-expanded="true" aria-owns="ui-select-choices-{{ $select.generatedId }}" aria-label="{{ $select.baseTitle }}" aria-activedescendant="ui-select-choices-row-{{ $select.generatedId }}-{{ $select.activeIndex }}" class="ui-select-search select2-input" ng-model="$select.search"></div><div class="ui-select-choices"></div></div></div>'), e.put("selectize/choices.tpl.html", '<div ng-show="$select.open" class="ui-select-choices ui-select-dropdown selectize-dropdown single"><div class="ui-select-choices-content selectize-dropdown-content"><div class="ui-select-choices-group optgroup" role="listbox"><div ng-show="$select.isGrouped" class="ui-select-choices-group-label optgroup-header" ng-bind="$group.name"></div><div role="option" class="ui-select-choices-row" ng-class="{active: $select.isActive(this), disabled: $select.isDisabled(this)}"><div class="option ui-select-choices-row-inner" data-selectable=""></div></div></div></div></div>'), e.put("selectize/match.tpl.html", '<div ng-hide="($select.open || $select.isEmpty())" class="ui-select-match" ng-transclude=""></div>'), e.put("selectize/select.tpl.html", '<div class="ui-select-container selectize-control single" ng-class="{\'open\': $select.open}"><div class="selectize-input" ng-class="{\'focus\': $select.open, \'disabled\': $select.disabled, \'selectize-focus\' : $select.focus}" ng-click="$select.activate()"><div class="ui-select-match"></div><input type="text" autocomplete="off" tabindex="-1" class="ui-select-search ui-select-toggle" ng-click="$select.toggle($event)" placeholder="{{$select.placeholder}}" ng-model="$select.search" ng-hide="!$select.searchEnabled || ($select.selected && !$select.open)" ng-disabled="$select.disabled" aria-label="{{ $select.baseTitle }}"></div><div class="ui-select-choices"></div></div>')
}]);
! function() {
    "use strict";

    function e(e, t, n) {
        "addEventListener" in window ? e.addEventListener(t, n, !1) : "attachEvent" in window && e.attachEvent("on" + t, n)
    }

    function t(e) {
        return ee + "[" + ne + "] " + e
    }

    function n(e) {
        _ && "object" == typeof window.console && console.log(t(e))
    }

    function o(e) {
        "object" == typeof window.console && console.warn(t(e))
    }

    function i() {
        n("Initialising iFrame"), r(), c(), u("background", D), u("padding", V), g(), f(), l(), p(), m(), Q = v(), x("init", "Init message from host page")
    }

    function r() {
        function e(e) {
            return "true" === e
        }
        var t = G.substr(te).split(":");
        ne = t[0], P = void 0 !== t[1] ? Number(t[1]) : P, W = void 0 !== t[2] ? e(t[2]) : W, _ = void 0 !== t[3] ? e(t[3]) : _, Z = void 0 !== t[4] ? Number(t[4]) : Z, oe = void 0 !== t[5] ? e(t[5]) : oe, H = void 0 !== t[6] ? e(t[6]) : H, q = t[7], K = void 0 !== t[8] ? t[8] : K, D = t[9], V = t[10], ue = void 0 !== t[11] ? Number(t[11]) : ue
    }

    function a(e, t) {
        return -1 !== t.indexOf("-") && (o("Negative CSS value ignored for " + e), t = ""), t
    }

    function u(e, t) {
        void 0 !== t && "" !== t && "null" !== t && (document.body.style[e] = t, n("Body " + e + ' set to "' + t + '"'))
    }

    function c() {
        void 0 === q && (q = P + "px"), a("margin", q), u("margin", q)
    }

    function l() {
        document.documentElement.style.height = "", document.body.style.height = "", n('HTML & body height set to "auto"')
    }

    function s(t) {
        function o(n) {
            e(window, n, function() {
                x(t.eventName, t.eventType)
            })
        }
        t.eventNames && Array.prototype.map ? (t.eventName = t.eventNames[0], t.eventNames.map(o)) : o(t.eventName), n("Added event listener: " + t.eventType)
    }

    function d() {
        s({
            eventType: "Animation Start",
            eventNames: ["animationstart", "webkitAnimationStart"]
        }), s({
            eventType: "Animation Iteration",
            eventNames: ["animationiteration", "webkitAnimationIteration"]
        }), s({
            eventType: "Animation End",
            eventNames: ["animationend", "webkitAnimationEnd"]
        }), s({
            eventType: "Device Orientation Change",
            eventName: "deviceorientation"
        }), s({
            eventType: "Transition End",
            eventNames: ["transitionend", "webkitTransitionEnd", "MSTransitionEnd", "oTransitionEnd", "otransitionend"]
        }), s({
            eventType: "Window Clicked",
            eventName: "click"
        }), s({
            eventType: "Window Resized",
            eventName: "resize"
        })
    }

    function f() {
        J !== K && (K in de || (o(K + " is not a valid option for heightCalculationMethod."), K = "bodyScroll"), n('Height calculation method set to "' + K + '"'))
    }

    function m() {
        !0 === H ? (d(), b()) : n("Auto Resize disabled")
    }

    function g() {
        var e = document.createElement("div");
        e.style.clear = "both", e.style.display = "block", document.body.appendChild(e)
    }

    function v() {
        function t() {
            return {
                x: void 0 !== window.pageXOffset ? window.pageXOffset : document.documentElement.scrollLeft,
                y: void 0 !== window.pageYOffset ? window.pageYOffset : document.documentElement.scrollTop
            }
        }

        function i(e) {
            var n = e.getBoundingClientRect(),
                o = t();
            return {
                x: parseInt(n.left, 10) + parseInt(o.x, 10),
                y: parseInt(n.top, 10) + parseInt(o.y, 10)
            }
        }

        function r(e) {
            function t(e) {
                var t = i(e);
                n("Moving to in page link (#" + o + ") at x: " + t.x + " y: " + t.y), R(t.y, t.x, "scrollToOffset")
            }
            var o = e.split("#")[1] || "",
                r = decodeURIComponent(o),
                a = document.getElementById(r) || document.getElementsByName(r)[0];
            a ? t(a) : (n("In page link (#" + o + ") not found in iFrame, so sending to parent"), R(0, 0, "inPageLink", "#" + o))
        }

        function a() {
            "" !== location.hash && "#" !== location.hash && r(location.href)
        }

        function u() {
            function t(t) {
                function n(e) {
                    e.preventDefault(), r(this.getAttribute("href"))
                }
                "#" !== t.getAttribute("href") && e(t, "click", n)
            }
            Array.prototype.forEach.call(document.querySelectorAll('a[href^="#"]'), t)
        }

        function c() {
            e(window, "hashchange", a)
        }

        function l() {
            setTimeout(a, U)
        }
        return Array.prototype.forEach && document.querySelectorAll ? (n("Setting up location.hash handlers"), u(), c(), l()) : o("In page linking not fully supported in this browser! (See README.md for IE8 workaround)"), {
            findTarget: r
        }
    }

    function p() {
        oe && (n("Enable public methods"), window.parentIFrame = {
            close: function() {
                x("close", "parentIFrame.close()", 0, 0)
            },
            getId: function() {
                return ne
            },
            moveToAnchor: function(e) {
                Q.findTarget(e)
            },
            reset: function() {
                L("parentIFrame.reset")
            },
            scrollTo: function(e, t) {
                R(t, e, "scrollTo")
            },
            scrollToOffset: function(e, t) {
                R(t, e, "scrollToOffset")
            },
            sendMessage: function(e, t) {
                R(0, 0, "message", JSON.stringify(e), t)
            },
            setHeightCalculationMethod: function(e) {
                K = e, f()
            },
            setTargetOrigin: function(e) {
                n("Set targetOrigin: " + e), re = e
            },
            size: function(e, t) {
                var n = "" + (e ? e : "") + (t ? "," + t : "");
                z(), x("size", "parentIFrame.size(" + n + ")", e, t)
            }
        })
    }

    function y() {
        0 !== Z && (n("setInterval: " + Z + "ms"), setInterval(function() {
            x("interval", "setInterval: " + Z)
        }, Math.abs(Z)))
    }

    function h(t) {
        function o(t) {
            (void 0 === t.height || void 0 === t.width || 0 === t.height || 0 === t.width) && (n("Attach listerner to " + t.src), e(t, "load", function() {
                x("imageLoad", "Image loaded")
            }))
        }
        t.forEach(function(e) {
            if ("attributes" === e.type && "src" === e.attributeName) o(e.target);
            else if ("childList" === e.type) {
                var t = e.target.querySelectorAll("img");
                Array.prototype.forEach.call(t, function(e) {
                    o(e)
                })
            }
        })
    }

    function b() {
        function e() {
            var e = document.querySelector("body"),
                o = {
                    attributes: !0,
                    attributeOldValue: !1,
                    characterData: !0,
                    characterDataOldValue: !1,
                    childList: !0,
                    subtree: !0
                },
                i = new t(function(e) {
                    x("mutationObserver", "mutationObserver: " + e[0].target + " " + e[0].type), h(e)
                });
            n("Enable MutationObserver"), i.observe(e, o)
        }
        var t = window.MutationObserver || window.WebKitMutationObserver;
        t ? 0 > Z ? y() : e() : (o("MutationObserver not supported in this browser!"), y())
    }

    function w() {
        function e(e) {
            function t(e) {
                var t = /^\d+(px)?$/i;
                if (t.test(e)) return parseInt(e, B);
                var o = n.style.left,
                    i = n.runtimeStyle.left;
                return n.runtimeStyle.left = n.currentStyle.left, n.style.left = e || 0, e = n.style.pixelLeft, n.style.left = o, n.runtimeStyle.left = i, e
            }
            var n = document.body,
                o = 0;
            return "defaultView" in document && "getComputedStyle" in document.defaultView ? (o = document.defaultView.getComputedStyle(n, null), o = null !== o ? o[e] : 0) : o = t(n.currentStyle[e]), parseInt(o, B)
        }
        return document.body.offsetHeight + e("marginTop") + e("marginBottom")
    }

    function T() {
        return document.body.scrollHeight
    }

    function E() {
        return document.documentElement.offsetHeight
    }

    function S() {
        return document.documentElement.scrollHeight
    }

    function I() {
        for (var e = document.querySelectorAll("body *"), t = e.length, o = 0, i = (new Date).getTime(), r = 0; t > r; r++) e[r].getBoundingClientRect().bottom > o && (o = e[r].getBoundingClientRect().bottom);
        return i = (new Date).getTime() - i, n("Parsed " + t + " HTML elements"), n("LowestElement bottom position calculated in " + i + "ms"), o
    }

    function O() {
        return [w(), T(), E(), S()]
    }

    function A() {
        return Math.max.apply(null, O())
    }

    function N() {
        return Math.min.apply(null, O())
    }

    function M() {
        return Math.max(w(), I())
    }

    function k() {
        return Math.max(document.documentElement.scrollWidth, document.body.scrollWidth)
    }

    function x(e, t, o, i) {
        function r() {
            e in {
                reset: 1,
                resetPage: 1,
                init: 1
            } || n("Trigger event: " + t)
        }

        function a() {
            X = m, se = g, R(X, se, e)
        }

        function u() {
            return ce && e in j
        }

        function c() {
            function e(e, t) {
                var n = Math.abs(e - t) <= ue;
                return !n
            }
            return m = void 0 !== o ? o : de[K](), g = void 0 !== i ? i : k(), e(X, m) || W && e(se, g)
        }

        function l() {
            return !(e in {
                init: 1,
                interval: 1,
                size: 1
            })
        }

        function s() {
            return K in ie
        }

        function d() {
            n("No change in size detected")
        }

        function f() {
            l() && s() ? L(t) : e in {
                interval: 1
            } || (r(), d())
        }
        var m, g;
        u() ? n("Trigger event cancelled: " + e) : c() ? (r(), z(), a()) : f()
    }

    function z() {
        ce || (ce = !0, n("Trigger event lock on")), clearTimeout(le), le = setTimeout(function() {
            ce = !1, n("Trigger event lock off"), n("--")
        }, U)
    }

    function C(e) {
        X = de[K](), se = k(), R(X, se, e)
    }

    function L(e) {
        var t = K;
        K = J, n("Reset trigger event: " + e), z(), C("reset"), K = t
    }

    function R(e, t, o, i, r) {
        function a() {
            void 0 === r ? r = re : n("Message targetOrigin: " + r)
        }

        function u() {
            var a = e + ":" + t,
                u = ne + ":" + a + ":" + o + (void 0 !== i ? ":" + i : "");
            n("Sending message to host page (" + u + ")"), ae.postMessage(ee + u, r)
        }
        a(), u()
    }

    function F(e) {
        function t() {
            return ee === ("" + e.data).substr(0, te)
        }

        function r() {
            G = e.data, ae = e.source, i(), Y = !1, setTimeout(function() {
                $ = !1
            }, U)
        }

        function a() {
            $ ? n("Page reset ignored by init") : (n("Page size reset by host page"), C("resetPage"))
        }

        function u() {
            return e.data.split("]")[1]
        }

        function c() {
            return "iFrameResize" in window
        }

        function l() {
            return e.data.split(":")[2] in {
                true: 1,
                false: 1
            }
        }
        t() && (Y && l() ? r() : "reset" === u() ? a() : e.data === G || c() || o("Unexpected message (" + e.data + ")"))
    }
    var H = !0,
        B = 10,
        D = "",
        P = 0,
        q = "",
        V = "",
        W = !1,
        j = {
            resize: 1,
            click: 1
        },
        U = 128,
        X = 1,
        Y = !0,
        J = "offset",
        K = J,
        $ = !0,
        G = "",
        Q = {},
        Z = 32,
        _ = !1,
        ee = "[iFrameSizer]",
        te = ee.length,
        ne = "",
        oe = !1,
        ie = {
            max: 1,
            scroll: 1,
            bodyScroll: 1,
            documentElementScroll: 1
        },
        re = "*",
        ae = window.parent,
        ue = 0,
        ce = !1,
        le = null,
        se = 1,
        de = {
            offset: w,
            bodyOffset: w,
            bodyScroll: T,
            documentElementOffset: E,
            scroll: S,
            documentElementScroll: S,
            max: A,
            min: N,
            grow: A,
            lowestElement: M
        };
    e(window, "message", F)
}();
