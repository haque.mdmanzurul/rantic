'use strict';

angular.module('mean.system').controller('NewsflashController', ['$scope', '$rootScope', '$location', 'Global', 'Settings', 'Menus',
    function($scope, $rootScope, $location, Global, Settings, Menus) {
        $scope.global = Global;
        $scope.newsflash = {};
       
        $scope.initNewsflash = function initNewsflash(){
           
           
          Settings.query({settingName: 'newsflash'}, function(result){
            if(Global.hasRole('provider') && result && result.value && result.value.provider){
              $scope.newsflash = result.value.provider;
            } else if(Global.hasRole('customer') && result && result.value && result.value.customer) {
              $scope.newsflash = result.value.customer;
            }


           

          });
        };
    }
]);
