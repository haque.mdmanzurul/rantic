'use strict';

angular.module('mean.system').controller('APIController', ['API','$scope', '$filter','ngTableParams', '$sce', '$q', function (API,$scope, $filter, ngTableParams, $sce, $q) {
  
	$scope.findOrders = function() {

		$scope.tableParams = new ngTableParams({
          page: 1,
          count: 150,
           sorting: { 
          "created": "desc"
        }
         
      }, {
          total: 0,
          getData: function($defer, params) {
            API.get(params.url(),  function(result) {
            	params.total(result.total);
            	$defer.resolve(result.data);
         	 });
          }
      });

    }


}]);