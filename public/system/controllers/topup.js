'use strict';

angular.module('mean.system').controller('TopUpController', ['$scope', '$rootScope', 'Global', '$modalInstance', 'data', 'Payments',
  function($scope, $rootScope, Global, $modalInstance, data, Payments) {
    $scope.Global = Global;

    $scope.price = 25;
    $scope.country = 'USA';
    $scope.cardName = Global.user.name;
    $scope.cardEmail = Global.user.username;
    $scope.orderId = "";
    $scope.productId = "";

    $scope.submit = function($event){
      if($scope.country === 'USA' && !$scope.state){
        alert('Please specify a state');
      } else if(!$scope.zip){
        alert('Please fill in the zip field before proceeding');
      } else if(!$scope.phone){
        alert('Please fill in the phone field before proceeding');
      } else if(!$scope.address){
        alert('Please fill in the address field before proceeding');
      } else if(!$scope.cardEmail){
        alert('Please fill in the cardEmail field before proceeding');
      } else if(!$scope.cardName){
        alert('Please fill in the cardName field before proceeding');
      } else if(!$scope.price || $scope.price <= 0){
        alert('Please fill in the amount you would like to top up');
      } else if(!$scope.city){
        alert('Please fill in the city field before proceeding');
      } else {
        var payment = new Payments({
            price: $scope.price
        });

        payment.$save(
          // success
          function(response) {
            $scope.orderId = response._id;
            $scope.productId = response._id;
            $($event.target).parent().submit();
            $modalInstance.close();
            // window.location.href = 'http://www.rantic.com/payment/index.php/purchase?amount=' + numeral(response.price).format('0.00') + '&id=' + response._id;
          },
          // error
          function(response){
            vex.dialog.alert(response.data);
            $modalInstance.close();
          }
        );
      }

      $event.preventDefault();
    }
  }
]);
