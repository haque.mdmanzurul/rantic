'use strict';

angular.module('mean.system').controller('SettingsController', ['$scope', '$rootScope', '$location', 'Global', 'Settings', 'Menus',
    function($scope, $rootScope, $location, Global, Settings, Menus) {
        $scope.global = Global;
        $scope.newsflash = {};
        $scope.disableButton = false;

        $scope.initSettings = function initNewsflash(){
          Settings.query({settingName: 'newsflash'}, function(result){
            $scope.newsflash = result;

          });
        };

        $scope.saveSettings = function saveSettings(){
          $scope.disableButton = true;
          Settings.save($scope.newsflash, function(result){
            vex.dialog.alert('Settings Saved');
            $scope.disableButton = false;
            window.location.reload();
          }, function(result){
            vex.dialog.alert('Error while saving the settings');
            $scope.disableButton = false;
          });
        };


    }
]);
