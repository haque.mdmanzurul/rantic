'use strict';




angular.module('mean.system').controller('ManualController', ['user_actions','Services','$scope', '$state', '$stateParams', '$location', '$http', '$filter', 'Global', 'Payments', 'ngTableParams', '$sce', '$q', function (user_actions,Services,$scope, $state, $stateParams, $location, $http, $filter, Global, Payments, ngTableParams, $sce, $q) {


  $scope.verify = function(topup) {
    var data = {};
    data.id = topup._id;
    user_actions.verify(data, function(result) {
      vex.dialog.alert("Verification complete");
      $scope.findOrders(true);
    }, function(err) {
      vex.dialog.alert(err);
    });
  }
	$scope.findOrders = function(refresh) {
    if(refresh) return $scope.tableParams.reload();
	   $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
           sorting: { 
          "create_time": "desc"
        }
         
      }, {
          total: 0,
          getData: function($defer, params) {
          	var data = {};
          	data.actionsId = $stateParams.actionsId;
          	data.data = params.url();
            user_actions.manual(data,  function(result) {
            console.log(result.data)
            //  params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });

}

  }]);