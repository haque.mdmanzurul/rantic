'use strict';




angular.module('mean.system').controller('CommentsController', ['Comments','Services','$scope', '$state', '$stateParams', '$location', '$http', '$filter', 'Global', 'Payments', 'ngTableParams', '$sce', '$q', function (Comments,Services,$scope, $state, $stateParams, $location, $http, $filter, Global, Payments, ngTableParams, $sce, $q) {
  $scope.payment = null;
  
  $scope.initService = function() {

     Services.query(function(result) {
         $scope.services = _.filter(result.data, function(service){
          return !service.disabled;
        });
        $scope.quantity = 1;
      });
       
     
    };
  
  $scope.serviceFinder = function(column) {
      $scope.initService();
       var promise = $q.defer();
      var list = [];
     var timeout = setInterval(function(){
        if($scope.services){
          _.each($scope.services, function(service){
            //_.each(vendor, function(service){
              list.push({
                id: service.value,
                title: $filter('cut')(service.label, true, 45)
              });
            //});
          });
          promise.resolve(list);
          clearInterval(timeout);
        }
      }, 10);

      return promise;


  }

  $scope.findLogs = function() {

      $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
           sorting: { 
          "created": "desc"
        }
         
      }, {
          total: 0,
          getData: function($defer, params) {
            Comments.logs(params.url(),  function(result) {

             console.log(result);
            
              params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });


  }

  $scope.find = function() {
    
      
      
      $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
           sorting: { 
          "created": "desc"
        }
         
      }, {
          total: 0,
          getData: function($defer, params) {
            Comments.query(params.url(),  function(result) {
               console.log(result.data);
                 $scope.payments = result.data;
            
              params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });

      /*Payments.query(function(payments) {
          $scope.payments = payments;
      });*/
  };

 

  $scope.findOne = function() {
    
  };

  $scope.goCreateService = function(){
    $state.go('create payment');
  }


  




}]);
