'use strict';




angular.module('mean.system').controller('ActionsController', ['user_actions','Services','$scope', '$state', '$stateParams', '$location', '$http', '$filter', 'Global', 'Payments', 'ngTableParams', '$sce', '$q', function (user_actions,Services,$scope, $state, $stateParams, $location, $http, $filter, Global, Payments, ngTableParams, $sce, $q) {

	$scope.findUserActions = function() {

	   $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
           sorting: { 
          "create_time": "desc"
        }
         
      }, {
          total: 0,
          getData: function($defer, params) {
          	var data = {};
          	data.actionsId = $stateParams.actionsId;
          	data.data = params.url();
            user_actions.query(data,  function(result) {
            
            //  params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });

}

  }]);