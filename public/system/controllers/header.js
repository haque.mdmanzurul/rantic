'use strict';

angular.module('mean.system').controller('HeaderController', ['Users', '$scope', '$rootScope', '$location', 'Global', 'Payments', 'Menus', 'dialogs', '$state',
    function(Users, $scope, $rootScope, $location, Global, Payments, Menus, dialogs, $state) {
        $scope.global = Global;
        $scope.menus = {};


        $scope.updateUserInfo = function() {
         
         Users.me(function(result) {

            Global.user = result;
            
         });
           

        }

       

        // Default hard coded menu items for main menu
        var defaultMainMenu = [
         {

            'roles': ['admin'],
            'title': 'Subscriptions',
            'link': 'subscriptions'

        },

        {
            'roles': ['authenticated'],
            'title': 'Orders',
            'link': 'all orders',
            'href': 'orders'
        },
        {
            'roles': ['authenticated'],
            'title': 'API',
            'link': 'api listing',
            'href': 'api/list'

        },
        {
            'roles': ['authenticated'],
            'title': 'API Help',
            'link': 'api help',
            'href': 'api/help'

        },
         {
            'roles': ['customer'],
            'title': 'Place an Order',
            'link': 'create order',
            'href': 'orders/create'
        }, {
            'roles': ['admin'],
            'title': 'Users',
            'link': 'manage users',
            'href': 'users'
        }, {
            'roles': ['admin'],
            'title': 'Services',
            'link': 'all services',
            'href': 'services'
        }, {
            'roles': ['admin'],
            'title': 'Discounts',
            'link': 'all discounts',
            'href': 'discounts'
        }, {
            'roles': ['admin'],
            'title': 'Payments',
            'link': 'all payments',
            'href': 'payments'
        }, {
            'roles': ['admin'],
            'title': 'Newsflash',
            'link': 'settings',
            'href': 'settings'
        },{
            'roles': ['admin'],
            'title': 'Comments',
            'link': 'comments',
            'href': 'comments'

        },{
            'roles': ['admin'],
            'title': 'Logs',
            'link': 'logs',
            'href': 'logs'

        },
        {
            'roles': ['admin'],
            'title': 'API Orders',
            'link': 'API',
            'href': 'API'

        },
        {
            'roles': ['admin'],
            'title': 'Blacklists',
            'link': 'blacklist',
            'href': 'backlist'

        },
         {
            'roles': ['admin'],
            'title': 'Email Generator',
            'link': 'emails',
            'href': 'emails'

        },{
            'roles': ['admin'],
            'title': 'Email Subscriptions',
            'link': 'emailsubs',
            'href': 'emailsubs'

        }
      
        ];

        // Query menus added by modules. Only returns menus that user is allowed to see.
        function queryMenu(name, defaultMenu) {

            Menus.menu_post({
                name: name,
                defaultMenu: defaultMenu
            }, function(menu) {
                $scope.menus[name] = menu.data;
            });
        }

        // Query server for menus and check permissions
        queryMenu('main', defaultMainMenu);

        $scope.isCollapsed = false;

        $scope.hasRole = function(which){
            return Global.hasRole(which);
        };

        $rootScope.$on('loggedin', function() {

            queryMenu('main', defaultMainMenu);


            $scope.global = {
                authenticated: !! $rootScope.user,
                user: $rootScope.user
            };
        });

        $(".account > .nav-pills > li > a").click(function(e){
          if($(".nav-pills", $(this).parent()).length){
            $(".nav-pills", $(this).parent()).toggle();
            e.preventDefault();
            return false;
          }
        });

    }
]);
