'use strict';

angular.module('mean.system').controller('IndexController', ['$scope', '$location','Global', '$state', function ($scope, $location, Global, $state) {
    $scope.global = Global;

    $scope.initIndex = function(){
    	if(Global.authenticated){
			  $state.go('all orders')
    	} else if(window.widgetMode){
        $state.go('create order');
      }
    };
}]);
