'use strict';

//Orders service used for services REST endpoint
angular.module('mean.system').factory('Subscriptions', ['$resource', function($resource) {
    return $resource('subscriptions', {
        paymentId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      
      query: {
      	method: 'GET',
      	isArray: false
      }      
    });
}]);
