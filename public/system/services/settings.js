'use strict';

//Settings service used for settings REST endpoint
angular.module('mean.system').factory('Settings', ['$resource', function($resource) {
    return $resource('settings/:settingName', {
        settingName: '@name'
    }, {
        query: {
          method: 'GET',
        	isArray: false
        },
        save: {
          method: 'POST',
          url: 'settings/:settingName'
        }
    });
}]);
