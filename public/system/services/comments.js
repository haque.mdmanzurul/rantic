'use strict';

//Orders service used for services REST endpoint
angular.module('mean.system').factory('Comments', ['$resource', function($resource) {
    return $resource('comments/:commentsId', {
        paymentId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false
      },
      logs: {
        method: 'GET',
        url: 'logs'
      }
      
    });
}]);
