'use strict';

//Global service for global variables
angular.module('mean.system').factory('Global', [
    function() {
        var _this = this;
        _this._data = {
            user: window.user,
            authenticated: !! window.user,
            hasRole: function(which){
                if(!_this._data.user) return false;
            	return _.find(_this._data.user.roles, function(role){
            		if(role === which){
            			return true;
            		}
            	});
            }
        };
        return _this._data;
    }
]);