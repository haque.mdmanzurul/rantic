'use strict';

//Orders service used for services REST endpoint
angular.module('mean.system').factory('user_actions', ['$resource', function($resource) {
    return $resource('user_actions/:actionsId', {
        actionsId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false,
        url: 'user_actions/:actionsId'
      },
      logs: {
        method: 'GET',
        url: 'logs'
      },
      manual: {
        method: 'GET',
        url: 'manual_verify'
      },
      verify: {
        method: "GET",
        url: "manual_action"
      }
      
    });
}]);
