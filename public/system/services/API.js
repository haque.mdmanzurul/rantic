'use strict';

//Orders service used for services REST endpoint
angular.module('mean.system').factory('API', ['$resource', function($resource) {
    return $resource('API_orders', {}
      ,{
      get: {
          method: 'GET'
      }
      
      
    });
}]);
