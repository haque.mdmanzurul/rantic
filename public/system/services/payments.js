'use strict';

//Payments service used for payments REST endpoint
angular.module('mean.system').factory('Payments', ['$resource', function($resource) {
    return $resource('payments/:orderId', {
        orderId: '@_id'
    }, {
        claim: {
            method: 'POST',
            url: 'payments/:orderId/approve'
        }
    });
}]);