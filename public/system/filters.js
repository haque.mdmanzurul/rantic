'use strict';

angular.module('mean').filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();

          if(text.length < 3){
            itemMatches = true;
            break;
          }

          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

angular.module('mean').filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});

angular.module('mean').filter('numeral', function () {
    return function (value, format) {
        if (!_.isNumber(value)) value = 0;
        if (!format) format = '0,0';

        return numeral(value).format(format);
    };
});

angular.module('mean').filter('fromNow', function () {
    return function (value, format) {
        return moment(value).fromNow();
    };
});

angular.module('mean').filter('prependHttp', function () {
    return function (value, format) {
        if(!_.isString(value)){
            return value;
        }

        if(!value.match(/^http(s)\:\/\/?/i)){
            return 'http://' + value;
        }

        return value;
    };
});
