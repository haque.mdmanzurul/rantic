'use strict';

//Users service used for users REST endpoint
angular.module('mean.users').factory('Users', ['$resource', function($resource) {
    return $resource('users/:userId', {
        userId: '@_id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
        	method: 'GET',
        	isArray: false
        },
        delete: {
            method: 'POST',
            url: 'deleteUser'
        },
        me: {
            method: "GET",
            url: "users/me"
        },
         generate: {
            method: "POST",
            url: "users/generateApiKey"
        }
    });
}]);