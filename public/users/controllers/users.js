'use strict';

angular.module('mean.users').controller('UsersController', ['$scope', '$stateParams', '$location', '$http', '$filter', 'Global', 'Payments', 'Users', 'ngTableParams', function ($scope, $stateParams, $location, $http, $filter, Global, Payments, Users, ngTableParams) {
    $scope.global = Global;

    $scope.select2OptionsRoles = {
        width: '100%',
        multiple: true,
        simple_tags: true,
        createSearchChoice: function(term, data) {
            return undefined;
        },
        tags: ['authenticated', 'customer', 'provider']  // Can be empty list.
    };

    var services = [
        { text: 'Facebook likes from one of the following countires: USA, UK, Canada, Australia', id: 'fb_premium'},
        { text: 'Facebook worldwide guaranteed page likes', id: 'fb_worldwide'},
        { text: 'Facebook budget page likes', id: 'fb_budget'},
        { text: 'Facebook likes from specified country (Targeted likes)', id: 'fb_targeted'},
        { text: 'Facebook photo likes', id: 'fb_photo'},
        { text: 'Facebook post likes', id: 'fb_post'},
        { text: 'Facebook comment likes', id: 'fb_comment'},
        { text: 'Instagram followers', id: 'ig_followers'},
        { text: 'Instagram likes', id: 'ig_likes'},
        { text: 'Instagram comments', id: 'ig_comments'},
        { text: 'Twitter followers', id: 'tw_followers'},
        { text: 'Twitter USA followers', id: 'tw_usa'},
        { text: 'Twitter favorites', id: 'tw_favs'},
        { text: 'Twitter retweets', id: 'tw_retweets'},
        { text: 'Youtube views', id: 'yt_views'},
        { text: 'Youtube likes', id: 'yt_likes'},
        { text: 'Youtube subscribers', id: 'yt_subscribers'},
        { text: 'Youtube comments', id: 'yt_comments'},
        { text: 'Youtube dislikes', id: 'yt_dislikes'},
        { text: 'Pinterest followers', id: 'pt_followers'},
        { text: 'Pinterest likes', id: 'pt_likes'},
        { text: 'Pinterest repins', id: 'pt_repins'},
        { text: 'Google+ circles', id: 'g_circles'},
        { text: 'Google+ ones', id: 'g_ones'},
        { text: 'Google+ comments', id: 'g_comments'},
        { text: 'Soundcloud followers', id: 'sc_followers'},
        { text: 'Soundcloud downloads', id: 'sc_downloads'},
        { text: 'Soundcloud plays', id: 'sc_plays'},
        { text: 'VK subscribers', id: 'vk_subscribers'},
        { text: 'VK likes', id: 'vk_likes'},
        { text: 'VK shares', id: 'vk_shares'},
        { text: 'Vine followers', id: 'vn_followers'},
        { text: 'Vine likes', id: 'vn_likes'},
        { text: 'Vimeo likes', id: 'vm_likes'},
        { text: 'Vimeo comments', id: 'vm_comments'},
        { text: 'Vimeo views', id: 'vm_views'},
        { text: 'Vimeo followers', id: 'vm_followers'},
        { text: 'Dailymotion favorites', id: 'dm_favorites'},
        { text: 'Dailymotion comments', id: 'dm_comments'},
        { text: 'Dailymotion views', id: 'dm_views'},
        { text: 'Dailymotion followers', id: 'dm_followers'},
        { text: 'Reverbnation plays', id: 'rn_plays'},
        { text: 'Website traffic (iframe)', id: 'wt_iframe'},
        { text: 'Website traffic (popunder fullscreen)', id: 'wt_popunder'},
        { text: 'Website traffic (iframe targeted any country)', id: 'wt_iframe_targeted'},
        { text: 'Website traffic (popunder targeted any country full screen)', id: 'wt_popunder_targeted'},
        { text: 'Custom video / player views and plays (real traffic will be sent works on funnyordie and many other sites except youtube)', id: 'wt_custom'},
        { text: 'Facebook website likes', id: 'fb_website'},
        { text: 'Facebook USA website likes', id: 'fb_usa'},
        { text: 'Twitter tweets', id: 'tw_tweets'},
        { text: 'Linkedin shares', id: 'li_shares'},
        { text: 'Pinterest pins', id: 'pt_pins'},
        { text: 'Google+ shares', id: 'g_shares'}
    ];
    $scope.generateApiKey = function(user) {

        Users.generate({userId: user._id}, function(response) {
           
            if(response.key) {
                $scope.user.api_key = response.key;
                vex.dialog.alert('New API Key has been saved!');
            }
            else   vex.dialog.alert('Oops, something went wrong, try again!');

        }, function(err) {
            vex.dialog.alert(err.data);
        });
    }
    $scope.deleteUser = function(id) {
       
        Users.delete({id: id}, function() {
            $location.path('/users');

        })
    }
    $scope.select2OptionsServices = {
        width: '100%',
        multiple: true,
        simple_tags: true,
        createSearchChoice: function(term, data) {
            return undefined;
        },
        tags: services  // Can be empty list.
    };

    $scope.getServiceText = function(id){
        var text = '';
        _.each(services, function(service){
            if(id === service.id){
                text = service.text;
                return false;
            }
        });
        return text;
    };

    $scope.hasRole = function(which, user){
        if(!user){
            user = $scope.user;
        }

        return _.find(user.roles, function(role){
            if(role === which){
                return true;
            }
        });
    };

    $scope.find = function() {
        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 50
        }, {
            total: 0,
            getData: function($defer, params) {
                Users.query(params.url(), function(result) {
                    $scope.users = result.data;
                    params.total(result.total);
                    $defer.resolve(result.data);
                });
            }
        });

        /*Users.query(function(users) {
            $scope.users = users;
        });*/
    };

    $scope.update = function() {
        var user = $scope.user;
        if (!user.updated) {
            user.updated = [];
        }
        user.updated.push(new Date().getTime());

        user.$update(function() {
            $location.path('/users');
        });
    };

    $scope.findOne = function() {
        Users.get({
            userId: $stateParams.userId
        }, function(user) {
            if(!user.profits) user.profits = {};
            $scope.user = user;
        });
    };
}]);