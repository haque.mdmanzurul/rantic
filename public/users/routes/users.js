'use strict';

//Setting up route
angular.module('mean.users').config(['$stateProvider',
    function($stateProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0')
                    $timeout(deferred.resolve, 0);

                // Not Authenticated
                else {
                    $timeout(function() {
                        deferred.reject();
                    }, 0);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('manage users', {
                url: '/users',
                templateUrl: 'public/users/views/list.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('edit user', {
                url: '/users/:userId',
                templateUrl: 'public/users/views/edit2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
    }
]);
