'use strict';

//Orders service used for discounts REST endpoint
angular.module('mean.discounts').factory('Codes', ['$resource', function($resource) {
    return $resource('discounts/evaluate/:discountName', {
        discountName: '@_id'
    }, {
      query: {
      	method: 'GET',
      	isArray: false
      }
    });
}]);
