'use strict';

//Orders service used for discounts REST endpoint
angular.module('mean.discounts').factory('Discounts', ['$resource', function($resource) {
    return $resource('discounts/:discountId', {
        discountId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false
      }
    });
}]);
