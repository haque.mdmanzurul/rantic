'use strict';

angular.module('mean.discounts').controller('DiscountsController', ['$scope', '$state', '$stateParams', '$location', '$http', '$filter', 'Global', 'Discounts', 'ngTableParams', '$sce', '$q', 'Services', function ($scope, $state, $stateParams, $location, $http, $filter, Global, Discounts, ngTableParams, $sce, $q, Services) {
  $scope.discount = {
    services: []
  };
  $scope.services = [];

  $scope.find = function() {
      $scope.tableParams = new ngTableParams({
          page: 1,
          count: 50,
          sorting: {
              "created": "desc"
          }
      }, {
          total: 0,
          getData: function($defer, params) {
            Discounts.query(params.url(), params.$params.filter, function(result) {
              $scope.discounts = result.data;
              params.total(result.total);
              $defer.resolve(result.data);
            });
          }
      });
  };

  $scope.update = function() {
    $scope.disableButton = true;
    var discount = $scope.discount;
    discount.$update(function() {
        $location.path('/discounts');
    });
  };

  $scope.remove = function(){
    $scope.discount.$remove(function(){
        $location.path('/discounts');
    });
  };

  $scope.create = function(){
    var self = this;
    $scope.disableButton = true;

    var discount = new Discounts({
        code: $scope.discount.code,
        price: $scope.discount.price,
        percent: $scope.discount.percent,
        services: $scope.discount.services,
        min_price: $scope.discount.min_price
    });

    discount.$save(
      function(response) {
        $location.path('/discounts');
      },
      function(response) {
        vex.dialog.alert("Error while saving discount");
      }
    );
  };

  $scope.findOne = function() {
      $scope.initServices();
      Discounts.get({
          discountId: $stateParams.discountId
      }, function(discount) {
          $scope.discount = discount;
      });
  };

  $scope.goCreateDiscount = function(){
    $state.go('create discount');
  };

  $scope.initServices = function(){
    Services.query(function(result) {
      $scope.services = result.data;
      $scope.quantity = 1;
    });
  };

  $scope.groupByCategory = function (item){
    return item.category || 'Other Services';
  };
}]);
