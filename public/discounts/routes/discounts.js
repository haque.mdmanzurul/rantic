'use strict';

//Setting up route
angular.module('mean.discounts').config(['$stateProvider',
    function($stateProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0')
                    $timeout(deferred.resolve, 0);

                // Not Authenticated
                else {
                    $timeout(function() {
                        deferred.reject();
                    }, 0);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('all discounts', {
                url: '/discounts',
                templateUrl: 'public/discounts/views/list.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            .state('create discount', {
                url: '/discounts/create',
                templateUrl: 'public/discounts/views/create_new.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })
            /* .state('edit discount', {
                url: '/discounts/:discountId/edit',
                templateUrl: 'public/discounts/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })*/
            .state('discount by id', {
                url: '/discounts/:discountId',
                templateUrl: 'public/discounts/views/view_new.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);
