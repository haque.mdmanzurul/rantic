'use strict';

//Setting up route
angular.module('mean.auth').config(['$stateProvider',
    function($stateProvider) {
        //================================================
        // Check if the user is not conntect
        //================================================
        var checkLoggedOut = function($q, $timeout, $http, $location) {
            // Initialize a new promise
          //  var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                console.log("IUSER --- ", user);
                if (user !== '0') {

                    $location.url('/order');

                }

                // Not Authenticated
                else {
                  //  $timeout(deferred.resolve, 0);

                }
            });

        //    return deferred.promise;
        };
        //================================================


        // states for my app
        $stateProvider
            .state('auth.login', {
                url: '/login',
                templateUrl: 'public/auth/views/login2.html',
                resolve: {
                    loggedin: checkLoggedOut
                }
            })
            .state('auth.register', {
                url: '/register',
                templateUrl: 'public/auth/views/register2.html',
                resolve: {
                    loggedin: checkLoggedOut
                }
            })
            .state('auth.reset', {
                url: '/resetPassword/:resetToken',
                templateUrl: 'public/auth/views/reset.html',
                resolve: {
                    loggedin: checkLoggedOut
                }
            });
    }
]);
