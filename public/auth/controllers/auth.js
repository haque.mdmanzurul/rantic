'use strict';

angular.module('mean.controllers.login', [])
    .controller('ResetCtrl', ['$scope', '$stateParams', '$rootScope','Global','$http','$location', function($scope, $stateParams, $rootScope, Global, $http, $location) {
        $scope.user = {};

        $scope.reset = function(){
            $http.post('/resetPassword', {
                resetToken: $stateParams.resetToken,
                password: $scope.user.password,
                confirmPassword: $scope.user.confirmPassword,
            })
                .success(function(){
                    vex.dialog.alert('Password reset successfully');
                    $location.url('/login');
                })
                .error(function(errors){
                    if(_.isArray(errors)){
                        var errorsText = [];
                        _.each(errors, function(error){
                            errorsText.push(error.msg);
                        });

                        vex.dialog.alert('The following problems were found:<br>' + errorsText.join('<br>'));
                    } else {
                        vex.dialog.alert(errors);
                    }
                });
        };
    }])
    .controller('LoginCtrl', ['$scope','$rootScope','Global','$http','$location', function($scope, $rootScope, Global, $http, $location) {
        // This object will be filled by the form
        $scope.user = {};

        $scope.forgotPassword = function(){
            vex.dialog.buttons.YES.text = 'Request password reset';
            vex.dialog.buttons.NO.text = 'Oops, no!';

            vex.dialog.prompt({
              message: 'Should I go ahead and attempt to reset the passowrd? An email will be sent with more detailed instructions to the email associated with your account.',
              placeholder: 'Enter your email address',
              callback: function(reason) {
                if(reason){
                    $http.post('/users/forgotPassword/' + reason, {})
                        .success(function(response){
                            vex.dialog.buttons.YES.text = 'Great, thank you!';
                            vex.dialog.alert(response);
                        })
                        .error(function() {
                            vex.dialog.buttons.YES.text = 'OK';
                            vex.dialog.alert(response);
                        });
                }
              }
            });
        };

        // Register the login() function
        $scope.login = function(){
            $http.post('/login', {
                email: $scope.user.email,
                password: $scope.user.password
            })
                .success(function(user){

                    // authentication OK
                    $scope.loginError = 0;
                    window.user = Global.user = $rootScope.user = user;
                    $rootScope.$emit('loggedin');
                    $location.url('/orders');
                })
                .error(function() {
                    $scope.loginerror = 'Authentication failed.';
                });
        };
    }])
    .controller('RegisterCtrl', ['$scope','$rootScope','Global','$http','$location', function($scope, $rootScope, Global, $http, $location) {
        $scope.user = {};

        $scope.register = function(){
            $scope.usernameError = null;
            $scope.registerError = null;
            $http.post('/register', {
                email: $scope.user.email,
                password: $scope.user.password,
                confirmPassword: $scope.user.confirmPassword,
                username: $scope.user.username,
                name: $scope.user.name
            })
                .success(function(user){
                    // authentication OK
                    $scope.registerError = 0;
                    window.user = Global.user = $rootScope.user = user;
                    $rootScope.$emit('loggedin');
                    $location.url('/orders');
                })
                .error(function(error){
                    // Error: authentication failed
                    if (error === 'This email already registered. You may have received your password by email. Make sure you check in the SPAM folder.') {
                        $scope.usernameError = error;
                    }
                    else {
                        $scope.registerError = error;
                       
                    }
                });
        };
    }]);
