'use strict';

//Orders service used for services REST endpoint
angular.module('mean.payments').factory('Payments', ['$resource', function($resource) {
    return $resource('payments/:paymentId', {
        paymentId: '@_id'
    }, {
      save: {
          method: 'POST'
      },
      update: {
          method: 'PUT'
      },
      remove: {
          method: 'DELETE'
      },
      query: {
      	method: 'GET',
      	isArray: false
      },
      process: {
          method: 'POST',
          url: 'payments/:paymentId/process'
      },
      purchase: {
        method: 'GET',
        url: 'purchase/:paymentId'
      }
    });
}]);
