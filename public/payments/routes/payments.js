'use strict';

//Setting up route
angular.module('mean.payments').config(['$stateProvider',
    function($stateProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0')
                    $timeout(deferred.resolve, 0);

                // Not Authenticated
                else {
                    $timeout(function() {
                        deferred.reject();
                    }, 0);
                    $location.url('/login');
                }
            });

            return deferred.promise;
        };

        // states for my app
        $stateProvider
            .state('all payments', {
                url: '/payments',
                templateUrl: 'public/payments/views/list2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })

            .state('checkout', {
                url: '/checkout',
                templateUrl: 'public/payments/views/checkout2.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })

            /* .state('edit payment', {
                url: '/payments/:serviceId/edit',
                templateUrl: 'public/payments/views/edit.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            })*/
            .state('payment by id', {
                url: '/payments/:paymentId',
                templateUrl: 'public/payments/views/view.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            });
    }
]);
