var mongoose = require('mongoose');
var moment = require('moment'), config = require('./server/config/config');

mongoose.Promise = require('bluebird');
mongoose.connect(config.db);

require('./server/models/order.js');
var Order = mongoose.model('Order');
var week = moment().subtract(7, 'day').format('YYYY-MM-DD');



Order.updateMany({
	checkout: true, paid: true, cancelled: false, verified: false, completed: false, refund: false,
	created: { $lt: week}

}, {
	$set: {
		verified: true, completed: true 
	}}, {
		multi: true
	}).exec(function(err, result ) {
	console.log(err);
	console.log(result);
	//mongoose.connection.close();
});






//  db.orders.update({checkout: true, paid: true, cancelled: false, verified: false, completed: false},{ $set: { verified: true, completed: true }},{multi: true});
