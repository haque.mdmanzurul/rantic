var mongoose = require('mongoose'),
	path = require('path'),
	_ = require('lodash'),
	async = require('async'),
	emailTemplates = require('swig-email-templates'),
	nodemailer = require('nodemailer'),
	transport = nodemailer.createTransport("sendmail");

// var mandrill = require('mandrill-api/mandrill');
// var mandrill_client = new mandrill.Mandrill('g1HgZsSdZJob5projeF2JA');

var mailgun = require('mailgun-js')({
	apiKey: 'key-54e8e9b4999d67b45ed0629c54b4fd14',
	domain: 'rantic.com'
});

var options = {
  	root: path.join(__dirname, "..", "views", "emails"),
  	// any other swig options allowed here
};

var messageDefaults = {
	"from": "Rantic Support <support@rantic.com>",
};

exports.commentPosted = function(order, comment, callback){
	//console.log('order comment', comment.user.id);
	emailTemplates(options, function(err, render) {
		render('comment-posted.html', {order: order, comment: comment, orderId: order._id.toString()}, function(err, html, text) {
			async.series([
				function(_callback){
					if(comment.user._id.toString() != order.user._id.toString()){
						var message = _.extend(messageDefaults, {
		          "html": html,
		          "text": text,
		          "subject": "A comment was posted on your order",
		          "to": order.user.name + " <" + order.user.email + ">",
		          "o:tag": [
		            "rantic",
		            "comment"
		          ],
							"o:user_id": order.user._id.toString(),
		        });

						mailgun.messages().send(message, _callback);
					} else {
						console.log('Mail would be sent to self. Skipping...');
						_callback();
					}
				},
				function(_callback){
					/* var mailOptionsAdmin = {
							from: "noreply@rantic.com",
							to: 'support@rantic.com',
							subject: "A comment was posted on an order",
							html: html,
							generateTextFromHTML: true
					}; */

					var message = _.extend(messageDefaults, {
						"html": html,
						"text": text,
						"subject": "Rantic support",
						"to": "Rantic Administration <support@rantic.com>",
						"o:tag": [
							"rantic",
							"comment-admin"
						]
					});

					//transport.sendMail(mailOptionsAdmin);
					try {
						if(comment.user.roles.indexOf('customer') != -1) {
							mailgun.messages().send(message, _callback);
						} else {
							_callback();
						}
					} catch(e) {  }
				}
			], callback);
		});
	});
};

exports.orderCreated = function(order){
	var Order = mongoose.model('Order'),
	Payment = mongoose.model('Payment');
	emailTemplates(options, function(err, render) {

		if(order.order) {
			order = order.order;
				if(!order.payment) return false;
				Payment.find({parent_id: order.payment.toString()}, function(err, data) {
					async.eachSeries(data, function(item, cb) {
						Order.findOne({_id: item.order}, function(err, result) {
							result.populate('user', function(err, full_data) {
								sendMail(full_data);
								return cb();
							});

						});

					}, function done() {
						return;
					});
				});

		}
		else sendMail(order);

		function sendMail(order){
		 console.log("SENDING EMAIL!");
		if(order && order.service && order.service.label){
		render('order-created.html', order, function(err, html, text) {
			var message = _.extend(messageDefaults, {
				"html": html,
				"text": text,
				"subject": "Your order has been taken",
				"to": (order.user.name || user.name) + " <" + (order.user.email || user.email) + ">",
				"o:tags": [
					"rantic",
					"order"
				],
				"o:user_id": order.user._id.toString() || user._id.toString()
			});
			mailgun.messages().send(message, function() {
			});
		});
}
}

	});
};


exports.extend_subscription = function(user, order) {

		emailTemplates(options, function(err, render) {
		render('subscription_extend.html', {user: user, order: order}, function(err, html, text) {
			var message = _.extend(messageDefaults, {
				"html": html,
				"text": text,
				"subject": "Topup your account to extend recurring payment!",
				"to": user.name + " <" + user.email + ">",
				"o:tag": [
					"rantic"
				],
				"o:user_id": user._id
			});

			mailgun.messages().send(message);
		});
	});
};


exports.userAutoCreated = function(user){
	emailTemplates(options, function(err, render) {
		render('user-auto-created.html', user, function(err, html, text) {
			var message = _.extend(messageDefaults, {
				"html": html,
				"text": text,
				"subject": "Your new account at panel.rantic.com",
				"to": user.name + " <" + user.email + ">",
				"o:tag": [
					"rantic",
					"registration"
				],
				"o:user_id": user._id.toString()
			});

			mailgun.messages().send(message);
		});
	});
};

exports.resetPassword = function(user){
	emailTemplates(options, function(err, render) {
		render('reset-password.html', user, function(err, html, text) {
			var message = _.extend(messageDefaults, {
				"html": html,
				"text": text,
				"subject": "Password reset has been requested",
				"to": user.name + " <" + user.email + ">",
				"o:tag": [
					"rantic",
					"password-reset-requested"
				],
				"o:user_id": user._id.toString()
			});

			mailgun.messages().send(message);
		});
	});
};

exports.resetPasswordDone = function(user){
	emailTemplates(options, function(err, render) {
		render('reset-password-done.html', user, function(err, html, text) {
			var message = _.extend(messageDefaults, {
				"html": html,
				"text": text,
				"subject": "Password has been reset",
				"to": user.name + " <" + user.email + ">",
				"o:tag": [
					"rantic",
					"password-reset-done"
				],
				"o:user_id": user._id.toString()
			});

			mailgun.messages().send(message);
		});
	});
};

exports.twoCheckoutError = function(payment, error, callback){
	emailTemplates(options, function(err, render) {
		render('two-checkout-error.html', {payment: payment, error: error, paymentId: payment._id.toString()}, function(err, html, text) {
			var message = _.extend(messageDefaults, {
				"html": html,
				"text": text,
				"subject": "2Checkout API Error",
				"to": "Rantic Administrator <jackpov@hotmail.com>",
				"o:tag": [
					"rantic",
					"two-checkout-error"
				]
			});

			mailgun.messages().send(message, callback);
		});
	});
};
