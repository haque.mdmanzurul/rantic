'use strict'

var config = require('./../config/config.js');
var debug = require('debug')('soundcloud');
var lookup = require('soundcloud-lookup');
var key = config.soundcloud.clientID;

module.exports = soundcloud;

function soundcloud(options) {
  return new soundcloud.SoundCloud(options);
}

soundcloud.SoundCloud = function(options) {
  var self = this;

  self.load = function(url, callback){
    lookup(url, key, function(err, data){
      callback(err, data);
    });
  };

  self.plays = function(url, callback){
  	self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.kind === 'track') return callback(err, 0);

      callback(err, json.playback_count);
  	});
  };

  self.followers = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.kind === 'user') return callback(err, 0);

      callback(err, json.followers_count);
    });
  };

  self.likes = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.kind === 'track') return callback(err, 0);

      callback(err, json.favoritings_count);
    });
  };
 
  self.downloads = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.kind === 'track') return callback(err, 0);

      callback(err, json.download_count);
    });
  };

  return process.nextTick(function() {});
};
