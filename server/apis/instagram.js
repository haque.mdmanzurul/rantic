'use strict'

var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var count_shares = require('count-shares');
var util = require('util');
var ig = require('instagram-node').instagram();
var config = require('./../config/config.js');

ig.use({ access_token: config.instagram.accessToken });
ig.use({ client_id: config.instagram.clientID,
         client_secret: config.instagram.clientSecret });

exports.isPublicUser = function(options, callback) {
    var regex = /(https?:\/\/)?(www)?instagram\.com\/([^\/]*?)\/?$/i;
    var match = options.order.url.match(regex);
    var id = null;

    if(match){
        id = match[3];
    } else {
        return callback(new Error('Cannot extract post id from URL'));
    }

    async.waterfall([
        function(_callback){
            ig.user_search(id, function(err, users, limit) {
                if(!users){
                    _callback(null, 0);
                }

                var found = false;
                _.each(users, function(user){
                    if(user.username === id){
                        _callback(err, user.id);
                        found = true;
                        return false;
                    }
                });

                if(!found){
                    _callback(new Error('User not found'));
                }
            });
        },
        function(user_id, _callback){
            ig.user(user_id, function(err, result, limit) {
                if(err){
                    if(err.code === 400){
                        return callback(null, false);
                    }
                }

                callback(null, true);
            });
        }
    ], function(err){
        callback(null, true);
    });
}

exports.isPublicPost = function(options, callback) {
    var regex = /(https?:\/\/)?(www)?instagram\.com\/p\/(.*?)\/?$/i;
    var match = options.order.url.match(regex);
    var id = null;

    if(match){
        id = match[3];
    } else {
        return callback(new Error('Cannot extract post id from URL'));
    }

    async.waterfall([
        function(_callback){
            request(
                {
                  timeout: 5000,
                  url: 'http://api.instagram.com/oembed?url=http://instagr.am/p/' + id,
                  json: true,
                  headers: {
                    'User-Agent': random_ua.generate()
                  }
                },
                function(err, response, json){
                    _callback(err, json.media_id);
                }
             );
        },
        function(media_id, _callback){
            ig.media(media_id, function(err, result, limit) {
                if(err){
                    if(err.code === 400){
                        return callback(null, false);
                    }
                }

                _callback(null, true);
            });
        }
    ], function(err, count){
        callback(null, true);
    });
}