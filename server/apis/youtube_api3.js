'use strict'

var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var moment = require('moment');
var redis = require("redis");
var get_youtube_id = require('get-youtube-id');
var client = redis.createClient();
var config = require('./../config/config.js');
var apiKey = config.youtube.apiKey;
var debug = require('debug')('youtube');

module.exports = youtube;

function youtube(options) {
  return new youtube.YoutTube(options);
}

youtube.YoutTube = function(options) {
  var self = this;

  self.load = function(url, callback){
    if(!options.job){
      // dummy function in case no job specified
      options.job = {};
      options.job.log = function(){};
    }

    options.job.log('Attempting to parse URL', url);
    var regex = "youtu\.be\/(.*)$";
  	var query = querystring.parse( urllib.parse(url).query );
    var match = url.match(regex);
  	var videoId = query.v;

    //var videoId = get_youtube_id(url);
    options.job.log('Video ID is', videoId);

    if(!videoId && match && match[1]){
      videoId = match[1];
    } else if(!videoId) {
      return callback(new Error('empty Video ID'));
    }

  	async.waterfall([
      function(_callback){
        // first, we shall check the timestamp of the last cached response
        client.get("yt_timestamp_" + videoId, _callback);
      },
  		function(timestamp, _callback){
        // if we're fresher than 30 minutes, let's use that
        if(moment(parseInt(timestamp)).diff(moment()) < 60*30 && 1===2){
          options.job.log('There seems to be a cached copy of this request less than 30 minutes old, using that');
          client.get("yt_cache_" + videoId, function(err, cache){
            _callback(err, {statusCode:200}, JSON.parse(cache));
          });
        } else {
          // otherwise - request from the API server
          options.job.log('No cached copy found, getting response from the API servers');
          debug('https://www.googleapis.com/youtube/v3/videos?id='+videoId+'&key='+apiKey+'&part=statistics,status');
    			request(
    				{
    				  timeout: 5000,
    				  //url: 'https://gdata.youtube.com/feeds/api/videos/' + videoId + '?v=2&alt=json',
              url: 'https://www.googleapis.com/youtube/v3/videos?id='+videoId+'&key='+apiKey+'&part=statistics,status',
    				  json: true,
    				  headers: {
    				    'User-Agent': random_ua.generate()
    				  }
    				},
    			 	function(err, response, json){
              // let's save stuff to the cache
      				async.series([
                function(__callback){
                  client.set("yt_timestamp_" + videoId, moment().unix(), __callback);
                },
                function(__callback){
                  client.set("yt_cache_" + videoId, JSON.stringify(json), __callback);
                }
              ], function(err){
                _callback(err, response, json);
              })
      			}
    			);
        }
  		},
  		function(response, json, _callback){
  			if (response.statusCode === 200) {
  				_callback(null, json);
  			} else {
  				_callback(new Error('Request failed with status code: ' + response.statusCode));
  			}
  		}
  	], function(err, json){
  		callback(err, json);
  	});
  };

  self.views = function(url, callback){
  	self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.items) return callback(err, 0);
      if(!json.items[0]) return callback(err, 0);
  		callback(err, json['items'][0]['statistics']['viewCount']);
  	})
  };

  self.favorites = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.items) return callback(err, 0);
      if(!json.items[0]) return callback(err, 0);
      callback(err, json['items'][0]['statistics']['favoriteCount']);
    })
  };

  self.likes = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.items) return callback(err, 0);
      if(!json.items[0]) return callback(err, 0);
      callback(err, json['items'][0]['statistics']['likeCount']);
    })
  };

  self.checkRtsp = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, false);
      if(!json.items) return callback(err, false);
      if(!json.entry['media$group']) return callback(err, false);
      if(!json.entry['media$group']['media$content']) return callback(err, false);

      var content = json.entry['media$group']['media$content'];
      var found = false;
      _.each(content, function(item){
        if(item.url.match(/^rtsp/i)){
          found = true;
          return false;
        }
      });

      callback(err, found);
    });
  };
  self.subscribers = function(url, callback) {
    var user_fix =   options.order.url.match('youtube(.*)\/(.*)')[2];
    var url = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername='+user_fix+"&key="+apiKey;

    if(!options.order.url.match('user')) { url = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id='+user_fix+"&key="+apiKey;  }
    
    request({timeout: 5000,url: url ,json: true, headers: {   'User-Agent': random_ua.generate() }   },
            function(err, response, data){
                
              try {
           
              
    callback(null, data['items'][0]['statistics']['subscriberCount']);

  }catch(e) { callback(null, null); console.log('unable to get youtube subscribers.')}


             });
  }




  
  self.dislikes = function(url, callback){
      self.load(url, function(err, json){
     if(!json) return callback(err, 0);
      if(!json.items) return callback(err, 0);
      if(!json.items[0]) return callback(err, 0);

      callback(err, json['items'][0]['statistics']['dislikeCount']);
     
    })
  };

 

  self.comments = function(url, callback){
    self.load(url, function(err, json){
      if(!json) return callback(err, 0);
      if(!json.items) return callback(err, 0);
      if(!json.items[0]) return callback(err, 0);
      callback(err, json['items'][0]['statistics']['commentCount']);
    })
  };

  return process.nextTick(function() {});
};
