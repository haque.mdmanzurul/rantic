'use strict'

// Basic dependencies
var kue = require('kue');
var jobs = kue.createQueue();
var async = require('async');
var request = require('request');
var random_ua = require('random-ua');
var cheerio = require('cheerio');
var urllib = require('url');
var _ = require('lodash');
var Promise = require("bluebird");

var debug = require('debug')('job');

// Order model (to get service, url e.t.c)
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var Services = mongoose.model('Service');
var fs = require('fs-extra');

// Various parsers
var parse = {




	musically_likes: require('./../parsers/autosmo.js'),
	musically_followers: require('./../parsers/autosmo.js'),	


	yt_usa_new: require('./../parsers/eat_cheap.js'),
	Youtube_views_new: require('./../parsers/eat_cheap.js'),
	vm_likes: require('./../parsers/autosmo.js'),
	vm_followers: require('./../parsers/autosmo.js'),
	tb_followers: require('./../parsers/likesmania.js'),
	tb_reblogs: require('./../parsers/likesmania.js'),
	tb_likes:   require('./../parsers/likesmania.js'),
	datpiff_views: require('./../parsers/likesmania.js'),
	datpiff_downloads: require('./../parsers/likesmania.js'),
	datpiff_streams: require('./../parsers/likesmania.js'),
	g_likes: require('./../parsers/smmlite.js'),
	
	rn_plays: require('./../parsers/likesmania.js'),

	ig_likes_20: require('./../parsers/autosmo.js'),
	
	ig_custom:   require('./../parsers/smmlite.js'),
	ig_mentions_popular: require('./../parsers/autosmo.js'),
	ig_mentions_hashtag: require('./../parsers/autosmo.js'),
	ig_mentions_followers: require('./../parsers/autosmo.js'),
	ig_mentions_likers: require('./../parsers/autosmo.js'),
	ig_mentions: require('./../parsers/autosmo.js'),


	tw_tweets: require('./../parsers/smmlite.js'), //
	dm_followers: require('./../parsers/dm_followers.js'),
	
	
	yt_usa_subscribers: require('./../parsers/yt_subscribers.js'),
	yt_comments: require('./../parsers/yt_comments.js'),
	
	
	li_shares: require('./../parsers/li_shares.js'),
	pt_pins: require('./../parsers/pt_pins.js'),
	
	
	g_comments: require('./../parsers/g_comments.js'),


	fb_premium: require('./../parsers/fb_page.js'),
	
	fb_budget: require('./../parsers/fb_page.js'),
	fb_targeted: require('./../parsers/fb_page.js'),
	fb_comment: require('./../parsers/fb_photo.js'),
	



	
	yt_subscribers: require('./../parsers/realfans.js'),
	fb_status_comments: require('./../parsers/realfans.js'),
	
	fb_eu: require('./../parsers/realfans.js'),
	fb_english: require('./../parsers/realfans.js'),
	fb_latin_likes: require('./../parsers/realfans.js'),
	fb_italian: require('./../parsers/realfans.js'),
	fb_arabic_likes: require('./../parsers/realfans.js'),
	fb_worldwide: require('./../parsers/realfans.js'),
	fb_usa: require('./../parsers/realfans.js'),
	fb_photo: require('./../parsers/realfans.js'),
	fb_views: require('./../parsers/realfans.js'),
	fb_followers: require('./../parsers/realfans.js'),
	fb_website: require('./../parsers/realfans.js'),
	yt_usa_views: require('./../parsers/realfans.js'),
	fb_reviews: require('./../parsers/realfans.js'),
	fb_vip: require('./../parsers/realfans.js'),
	fb_event: require('./../parsers/realfans.js'),
  



	fb_post: require('./../parsers/realfans.js'),

	fb_likes_unlimited: require('./../parsers/realfans.js'),
	fb_african_likes: require('./../parsers/realfans.js'),

	////



	
	
	
	
	
	
	
	
	
	
	
	


	// autosmo
	yt_premium: require('./../parsers/smmlite.js'),
	unlimited_insta: require('./../parsers/smmlite.js'),
	yt_favz: require('./../parsers/smmlite.js'),
	yt_sharez: require('./../parsers/smmlite.js'),
	ig_followers: require('./../parsers/smmlite.js'),
	ig_followers2: require('./../parsers/autosmo.js'),
	ig_usfollows:  require('./../parsers/smmlite.js'),
	ig_likes: require('./../parsers/smmlite.js'),
	ig_usalikes: require('./../parsers/smmlite.js'),
	tw_followers: require('./../parsers/autosmo.js'), 
	tw_usa: require('./../parsers/smmlite.js'), 
	tw_favs: require('./../parsers/smmlite.js'), 
	tw_retweets: require('./../parsers/autosmo.js'), 

	ig_views: require('./../parsers/smmlite.js'), 

	// socialbox

	
	pt_likes: require('./../parsers/autosmo.js'),
	pt_repins: require('./../parsers/autosmo.js'),
	vk_likes: require('./../parsers/likesmania.js'),
	vk_shares: require('./../parsers/likesmania.js'),
	vk_subscribers: require('./../parsers/likesmania.js'),



	snap_followers: require('./../parsers/autosmo.js'),
	li_followers: require('./../parsers/autosmo.js'),
	li_endorsements: require('./../parsers/autosmo.js'),
	li_connections: require('./../parsers/autosmo.js'),



	vn_revines: require('./../parsers/autosmo.js'),
	vn_followers: require('./../parsers/autosmo.js'),
	vn_likes: require('./../parsers/autosmo.js'),
	vn_loops: require('./../parsers/autosmo.js'),
	vn_comments: require('./../parsers/autosmo.js'),
	sc_likes: require('./../parsers/smmlite.js'),
	sc_comments: require('./../parsers/autosmo.js'),
	periscope_followers: require('./../parsers/autosmo.js'),
	periscope_hearts: require('./../parsers/autosmo.js'),
	spotify_followers: require('./../parsers/autosmo.js'),
	mixcloud_followers: require('./../parsers/autosmo.js'),
	mixcloud_reposts: require('./../parsers/autosmo.js'),
	mixcloud_favorites: require('./../parsers/autosmo.js'),
	mixcloud_plays: require('./../parsers/autosmo.js'),

	soundcloud_reposts: require('./../parsers/autosmo.js'),
	soundcloud_plreposts: require('./../parsers/autosmo.js'),
	soundcloud_pllikes: require('./../parsers/autosmo.js'),
	
	vm_views: require('./../parsers/smmlite.js'),
	sc_downloads: require('./../parsers/smmlite.js'),


	//tw_retweets_unlimited: require('./../parsers/socialbox.js'),
	//tw_favs_unlimited: require('./../parsers/socialbox.js'),
	//ig_likes_unlimited: require('./../parsers/socialbox.js'),






	
	dm_views: require('./../parsers/smmlite.js'),
	g_ones: require('./../parsers/smmlite.js'),
	g_shares: require('./../parsers/smmlite.js'),





	sc_plays: require('./../parsers/autosmo.js'),
	sc_followers: require('./../parsers/smmlite.js'),
	fb_views_auto: require('./../parsers/autosmo.js'),
	ig_euandusa: require('./../parsers/autosmo.js'),



	vevo_views: require('./../parsers/smmkart.js'),
	yt_views: require('./../parsers/smmkart.js'),
	yt_views_retention: require('./../parsers/smmkart.js'),



	/* smmlite */
	yt_likes: require('./../parsers/smmlite.js'),
	yt_usa: require('./../parsers/smmlite.js'),
	yt_subselite: require('./../parsers/smmlite.js'),
	yt_dislikes: require('./../parsers/smmlite.js'),
	slow_yt_likes: require('./../parsers/smmlite.js'),
	yt_dislikes_slow: require('./../parsers/smmlite.js'),
	fb_followers_elite: require('./../parsers/smmlite.js'),
	fb_comments_elite: require('./../parsers/smmlite.js'),
	fb_comments_customelite: require('./../parsers/smmlite.js'),
	fb_emoticons_elite: require('./../parsers/smmlite.js'),
	yt_custom_elite: require('./../parsers/smmlite.js'),
	yt_smlite_usa: require('./../parsers/smmlite.js'),
	ig_comments: require('./../parsers/smmlite.js'),
	yt_views_smlite_packages: require('./../parsers/smmlite.js'),
	vevo_lite: require('./../parsers/smmlite.js'),
	g_circles: require('./../parsers/smmlite.js'),
    yt_super_retention: require('./../parsers/smmlite.js'),
    fb_postlikeslite: require('./../parsers/smmlite.js'),
    pt_followers: require('./../parsers/smmlite.js'),
    yt_sm_target: require('./../parsers/smmlite.js')
	
	
	
};

Services.find({}, function(err, data) {

		for(var i in data) {
			//console.log(data[i].value);
			if ( data[i] && data[i].api_type && !data[i].disabled)	{



				parse[data[i].value] = require('./../parsers/'+data[i].api_type+".js");

			}
		}

	

jobs.process('count', function(job, done){
	async.waterfall([
		function(callback){
			// Let's load this order and see what we're dealing with
			
			job.log('loading order', job.data.order);
			Order.findOne({_id: job.data.order, url: { $ne: 'add_cart'}})
				 .exec(callback);
		},
		function(order, callback){
			if(!order){
				
				return callback(new Error('Order not found'));
			}
			if(order && order.url == 'add_cart') {
				return callback(new Error('pass'));
			}

			if(!order.service){
				
				return callback(new Error('No service found on order'));
			}

			if(!_.isFunction(parse[order.service.value])){
				
				debug('no parser for this service: %s', order.service.value);
				return callback(new Error('No parser for this service: ' + order.service.value));
			}

			


			Services.findOne({ value: order.service.value }, function(err, data) {
				var drop = {};
				var service = order.service.value;				
				var api_type
				try {
					api_type = data.api_type;
					if(!data.dropdown) {
						drop = {};
					}else {
							drop = JSON.parse(data.dropdown);
					}
				} catch(e) {
					drop = {};
				}
				//console.log("SERVICE BEFORE " + type);
				async.eachSeries(drop, function(item, cbs) {
					if(!item || !item.api_code || !item.api_url) return cbs();
					if(item.api_code == order.emoticon) {
						
						api_type = item.api_url;
						console.log("UPDATED  - " + api_type);
					}
					return cbs();
				}, function done() {
					console.log(service + ' - ' + api_type);
							//parse[order.service.value] = require('./../parsers/'+data.api_type+".js");
					if(api_type)	parse[service] = require('./../parsers/'+api_type+".js");

					return callback(err,order);

				});





			});	
		},
		function(order, callback) {


			Promise.try(function(){
				//console.log(parse);
		    parse[order.service.value]({order: order, job: job}, function(err, count){

					debug('count for service %s at url %s seems to be %d', order.service.value, order.url, count);
					job.log('count for service ' + order.service.value + ' at url ' + order.url + ' seems to be ' + count);
					callback(err, order, count);
				});
			}).catch(function(err){
		    callback(err);
			}); 

		},
		function(order, count, callback){
			count = parseInt(count);
			order.beforeCount = parseInt(order.beforeCount);
			order.afterCount = parseInt(order.afterCount);
			if(isNaN(count)) count = 0;
			if(isNaN(order.beforeCount)) order.beforeCount = 0;
			if(isNaN(order.afterCount)) order.afterCount = 0;


			if(job.data.type === 'before' || !order.beforeCount){
				
				
				
				
				order.beforeCount = count;
			}
			order.afterCount = count;
			order.save(function(err, order){
				callback(err, order);
			});
		},
		function(order, callback){
			
			if(order.afterCount >= order.beforeCount + order.orderCount && order.beforeCount >= 0){
				order.complete(null, null, callback);
			} else {
				callback(null);
			}
		}
	], function(err){
		if(err){
			
			debug('error %o', err);
		}

		setTimeout(function(){
			done(err);
		}, 1000);
	});
});
});
debug('Registered \'count\' job');
