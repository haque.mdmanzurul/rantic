'use strict';

// Payments routes use payments controller
var comments = require('../controllers/comments');
var authorization = require('./middlewares/authorization');
var redis = require("redis"),
client = redis.createClient();
// Payment authorization helpers
var hasAuthorization = function(req, res, next) {
    if (req.comments.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app) {
//    app.options('/payments', payments.options);
    app.get('/logs', isAdmin, comments.logs);
    app.get('/facebook_likes', function(req, res) {
      client.set('facebook_likes'+req.query.facebook_id,req.query.facebook_value)
      console.log('id is' + req.query.facebook_id)
      console.log('value is ' + req.query.facebook_value);
      res.send('');

    })
    app.get('/comments', isAdmin, comments.all);
    app.get('/send_email', function(req, res) {




      var path = require('path'),
  _ = require('lodash'),
  async = require('async'),
  emailTemplates = require('swig-email-templates'),
  nodemailer = require('nodemailer'),
  transport = nodemailer.createTransport("sendmail");
var _ = require('lodash');
var mongoose = require('mongoose');

var Order = mongoose.model('Order');
// var mandrill = require('mandrill-api/mandrill');
// var mandrill_client = new mandrill.Mandrill('g1HgZsSdZJob5projeF2JA');

var mailgun = require('mailgun-js')({
	apiKey: 'key-1b12a2393de1a8e7c48ee2bee7c0f186',
	domain: 'rantic.com'
});

var options = {
    root: path.join(__dirname, "..", "views", "emails"),
    // any other swig options allowed here
};

var messageDefaults = {
  "from": "Rantic Support <support@rantic.com>",
  "o:website": "panel.rantic.com"
};







  var fs = require('fs');
  var email_list = [];
  var data_find = Order.find({checkout: false });
      data_find.populate('user', 'name username')
        .populate('provider', 'name username profit').exec(function(err, data) {



          function runner(i) {

setTimeout(function() {
        if( i === data.length) return false;
          i
          if(data[i].user.username != 'support@rantic.com' && data[i].user.username != 'dannypovolotski@gmail.com' && email_list.indexOf(data[i].user.username) == -1) {
            email_list.push(data[i].user.username)

          console.log(data[i].user.name + ' - ' +data[i].user.username+' - sending email.. ' );
            var message = _.extend(messageDefaults, {

              "text": 'Sorry, our system did not notify us about comments posted on abandoned orders, if you are still requiring support please comment again or email support@rantic.com',
              "subject": "Rantic Support",
              "to": "Rantic Customer <" + data[i].user.username + ">"
            });
                mailgun.messages().send(message);

              }else { console.log('not addng' + data[i].user.username)}





        runner(i = i+1);


},1000);

}
runner(0);



  })
















    })
 //   app.post('/payments', payments.create);
 //   app.post('/payments/:paymentId', payments.create);
 //   app.post('/payments/:paymentId/approve', payments.approve);
 //   app.post('/payments/:paymentId/process', payments.process);
  //  app.post('/return/:paymentId', payments.return);
  //  app.get('/return/:paymentId', payments.return);
  //  app.get('/purchase/:paymentId', payments.purchase);
   // app.get('/paypal_recurring/:paymentId', payments.paypal_recurring);


    //app.get('/payments/:paymentId', payments.show);
    //app.put('/payments/:paymentId', authorization.requiresLogin, hasAuthorization, payments.update);
    //app.del('/payments/:paymentId', authorization.requiresLogin, hasAuthorization, isAdmin, payments.destroy);

    // Finish with setting up the paymentId param
   // app.param('commentsId', comments.comments);

};
