'use strict';

// Orders routes use iframely controller
var iframely = require('../controllers/iframely');
var authorization = require('./middlewares/authorization');

// Order authorization helpers
var hasAuthorization = function(req, res, next) {
    /* if (req.order.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    } */
    next();
};

module.exports = function(app) {

    app.get('/iframely', authorization.requiresLogin, iframely.gateway);

};