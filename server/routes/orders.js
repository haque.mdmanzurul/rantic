'use strict';

// Orders routes use orders controller
var orders = require('../controllers/orders');
var authorization = require('./middlewares/authorization');

// Order authorization helpers
var hasAuthorization = function(req, res, next) {
    if(req.user.hasRole('admin')) { return next(); }
    if (req.order.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isAdmin = function(req, res, next) {

    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app) {
    app.get("/testing", orders.testing);
    app.get('/orders', orders.all);
    app.post('/orders', orders.create);
    app.get('/orders/check', orders.check);
    app.post('/orders/admin_create',authorization.requiresLogin, isAdmin,orders.admin_create);
     app.post('/orders/edit_count',authorization.requiresLogin,isAdmin, orders.edit_count);
     app.post('/orders/edit_status',authorization.requiresLogin,isAdmin, orders.edit_status);
     app.post('/orders/abandoned_paid',authorization.requiresLogin, orders.abandoned);
    app.post('/orders/:orderId/comment', authorization.requiresLogin, orders.comment);
    app.post('/orders/:orderId/claim', authorization.requiresLogin, orders.claim);
    app.post('/orders/:orderId/cancel', authorization.requiresLogin, orders.cancel);
    app.post('/orders/:orderId/complete', authorization.requiresLogin, orders.complete);
    app.post('/orders/:orderId/approve', authorization.requiresLogin, orders.approve);
    app.post('/orders/:orderId/unapprove', authorization.requiresLogin, orders.unapprove);

    app.post('/orders/inprogress', orders.inprogress);
    app.post('/order_modify_api/:orderId', authorization.requiresLogin, isAdmin, orders.modify_api)
    app.post('/orders/api_change',authorization.requiresLogin, isAdmin, orders.change_api);
    app.post('/orders/re_order',authorization.requiresLogin, isAdmin, orders.re_order);
    app.post('/orders/return_balance', authorization.requiresLogin, isAdmin, orders.return_balance);
    app.post('/orders/return_balance_customer', authorization.requiresLogin, orders.return_balance);
    app.post('/orders/refunded', authorization.requiresLogin, isAdmin, orders.refunded);

    app.get("/abandoned_paypal/:orderId", authorization.requiresLogin, orders.abandoned_paypal);
    app.get("/abandoned_credit/:orderId", authorization.requiresLogin, orders.abandoned_credit);
    app.get('/orders/:orderId', authorization.requiresLogin, orders.show);
    app.put('/orders/:orderId', authorization.requiresLogin, hasAuthorization, orders.update);
    app.post('/orders/url', authorization.requiresLogin, isAdmin, orders.url);
    app.post('/orders/customer_url', authorization.requiresLogin, orders.url);
    app.delete('/orders/:orderId', authorization.requiresLogin, isAdmin, orders.destroy);

    // Finish with setting up the orderId param
    app.param('orderId', orders.order);

};
