'use strict';

var mean = require('meanio');

module.exports = function(app) {
    app.post('/admin/menu/:name', function(req, res) {
        var roles = (req.user ? req.user.roles : ['annonymous']);
        var menu = req.body.name ? req.body.name : 'main';
        var defaultMenu = (req.body.defaultMenu ? req.body.defaultMenu : []);

        defaultMenu.forEach(function(item, index) {
            defaultMenu[index] = item;
        });

        var items = mean.menus.get({
            roles: roles,
            menu: menu,
            defaultMenu: defaultMenu
        });

        res.jsonp({data: items});
    });
};
