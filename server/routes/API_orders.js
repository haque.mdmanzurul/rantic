'use strict';


var mongoose = require('mongoose');
var API = mongoose.model('API_order');
var async = require('async');
var authorization = require('./middlewares/authorization');

// Payment authorization helpers
var hasAuthorization = function(req, res, next) {
    if (req.comments.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app) {

	app.get('/API_orders', isAdmin, function(req, res) {

		API.find().sort(req.query.sorting)
		.skip(req.query.count*(req.query.page-1))
		.limit(parseInt(req.query.count))
		.exec(function(err, result) {
			var list = [];
			var total = [];
			async.each(result, function iterator(item, callback) {
				
				if(list.indexOf(item.order.toString()) != -1) {
					for(var i in total) {
						
						if(total[i].id == item.order.toString()) {
							
							total[i].total = ++total[i].total;
							callback();
						}
					}
					
					
				}
				
				else {
					
					list.push(item.order.toString());
					total.push({id: item.order, total: 1, create_time: item.create_time});
					
					callback();
				}
				

			}, function(done) {
				
				res.jsonp({
                    count: (req.query.count ? parseInt(req.query.count) : 0),
                	page: (req.query.page ? parseInt(req.query.page) : 1),
                	sorting: req.query.sorting,
                	data: total
              	});
			})
			
		})

	});


};