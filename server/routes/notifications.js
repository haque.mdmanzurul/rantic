'use strict';

// Orders routes use notifications controller
var notifications = require('../controllers/notifications');
var authorization = require('./middlewares/authorization');

// Order authorization helpers
var hasAuthorization = function(req, res, next) {
    if (req.order.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app) {

    app.post('/notifications/notify', notifications.notify);

    


};
