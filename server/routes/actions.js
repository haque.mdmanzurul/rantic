'use strict';

// Payments routes use payments controller
var comments = require('../controllers/comments');
var actions = require('../controllers/actions');
var authorization = require('./middlewares/authorization');
var redis = require("redis"),
client = redis.createClient();
// Payment authorization helpers
var hasAuthorization = function(req, res, next) {
    if (req.comments.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app) {

	app.get('/user_actions/:actionsId', isAdmin, actions.user);
    app.get('/manual_verify', isAdmin, actions.manual);
    app.get('/manual_action', isAdmin, actions.verify);


};

	//  app.get('/comments', isAdmin, comments.all);