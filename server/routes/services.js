'use strict';

// Orders routes use services controller
var services = require('../controllers/services');
var authorization = require('./middlewares/authorization');
var config = require('../config/env/development.js');
var request = require('request');
var path = require('path');
var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

var updateFiles = function(req, res ,next) {
  request.post("http://localhost:4000/cons", { refresher: true}, function() {
    return next();
  });
}

module.exports = function(app) {
    app.get('/services', services.all);
    app.get('/services/discount', authorization.requiresLogin, isAdmin, services.discount);
    app.post('/services/fetchDiscount', authorization.requiresLogin, isAdmin, services.fetchDiscount);
    app.post('/services/createDiscount', authorization.requiresLogin, isAdmin, services.createDiscount);
    app.post('/services/createDropdown', authorization.requiresLogin, isAdmin, services.createDropdown);
    app.post('/services/editDiscount', authorization.requiresLogin, isAdmin, services.editDiscount);
    app.post('/services', authorization.requiresLogin, isAdmin, services.create);
    app.get('/services/:serviceId', authorization.requiresLogin, isAdmin, services.show);
    app.post('/services/getApiData', authorization.requiresLogin, services.getApiData);
    app.put('/services/:serviceId', authorization.requiresLogin, isAdmin,  services.update);
    app.delete('/services/removeDiscount', authorization.requiresLogin, isAdmin, services.removeDiscount);
    app.delete('/services/:serviceId', authorization.requiresLogin, isAdmin,  services.destroy);

    // get facebook categories for api

    app.get('/instagram-hashtag', function(req, res) {
        var link = path.join(__dirname, '..', 'hashtags');

        var fs = require('fs');
        var ff = fs.readFileSync(link,'utf-8');
        var lines = ff.split("\n");
        var failed = false;

        for(var i in lines) {
            var tmp = lines[i].substr(1,lines[i].length);
            if(tmp.trim() === req.query.hashtag) failed = true;
        }

        if(failed) return res.status(500).end();
        else return res.status(200).end();

    });

    app.get('/fb_category', function(req, res) {
        var url = req.query.link;
        var category_list = ['Album','Record','Musician','Musician\\/Band'];
var phantom = require('phantom');
var failed = false;
var sitepage = null;
var phInstance = null;
phantom.create()
    .then(function(instance) {
        phInstance = instance;
        return instance.createPage();
    })
    .then(function(page){
        sitepage = page;

        return page.open(url);
    })
    .then(function(status) {

        return sitepage.property('content');
    })
    .then(function(content) {

        try {
            var data = content.split('categoryLabel":"')[1].split('",')[0];

            if(category_list.indexOf(data) !== -1)  failed = true
        } catch(e) {
            failed = false;
        }


         if(failed)  res.status(200).send({data: true});
         if(!failed) res.status(200).end();

        sitepage.close();
        phInstance.exit();
    })
    .catch(function(error) {

        phInstance.exit();
    });




    })

    // Finish with setting up the serviceId param
    app.param('serviceId', services.service);

};
