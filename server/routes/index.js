'use strict';

module.exports = function(app) {

    // Home route
    var index = require('../controllers/index');
    app.options('*', function(req, res, next){
    	return res.status(200).send();
    });

    app.get('/', index.render);
};
