'use strict';

// Payments routes use settings controller
var settings = require('../controllers/settings');

module.exports = function(app) {
    app.options('/settings', settings.options);

    app.get('/settings/:settingName', settings.load);
    app.post('/settings/:settingName', settings.update);
    app.param('settingName', settings.setting);

};
