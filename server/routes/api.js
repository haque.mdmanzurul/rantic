'use strict';

// User routes use users controller
var api = require('../controllers/api_panel.js');
var blacklist = require('../controllers/blacklist.js');
var email = require('../controllers/email_generator.js');
var isAdmin = function(req, res, next) {
    
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app, passport) {
 //   app.post('/api/example', isAdmin, users.generateApiKey);
    app.get('/api', api.create);
    app.get('/api/example', api.example);
    app.get('/api/all', api.all);
    app.param('id', api.api);

    app.get('/api/emails', email.emails);
    app.get('/api/blacklist', blacklist.query);
    app.post('/api/add_blacklist', blacklist.add);
    app.post('/api/edit_blacklist', blacklist.edit);
    app.post('/api/delete_blacklist', blacklist.delete);

    


};
