'use strict';

// Payments routes use payments controller
var payments = require('../controllers/payments');
var authorization = require('./middlewares/authorization');

// Payment authorization helpers
var hasAuthorization = function(req, res, next) {
    if (req.payment.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};
var garbage = function(req, res, next) {
    global.gc();
    return next();
}
module.exports = function(app) {
    app.options('/payments', garbage, payments.options);
    app.get('/payments', garbage, isAdmin, payments.all);

    app.post('/payments', garbage, payments.create);
    app.post('/payments/:paymentId', garbage, payments.create);
    app.post('/payments/:paymentId/approve', garbage, payments.approve);
    app.post('/payments/:paymentId/process', garbage, payments.process);
    app.post('/return/:paymentId', garbage, payments.return);
    app.get('/return/:paymentId', garbage, payments.return);
    app.get('/purchase/:paymentId', garbage, payments.purchase);


    //app.get('/payments/:paymentId', payments.show);
    //app.put('/payments/:paymentId', authorization.requiresLogin, hasAuthorization, payments.update);
    //app.del('/payments/:paymentId', authorization.requiresLogin, hasAuthorization, isAdmin, payments.destroy);

    // Finish with setting up the paymentId param
    app.param('paymentId', payments.payment);

};


