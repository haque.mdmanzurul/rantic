'use strict';

// User routes use users controller
var users = require('../controllers/users');
var config = require('../config/config');
var free_service = require('../controllers/free_service');
var isAdmin = function(req, res, next) {

    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};
var garbage = function(req, res, next) {
    global.gc();
    return next();
}
module.exports = function(app, passport) {
	app.get("/users/subscription", garbage, users.saveEmails);
    app.get('/users/getEmails', garbage, users.getEmails);
    app.get("/users/activate_user/:key",garbage, users.activate_user);
    app.post('/users/deactivated_order',garbage, users.activate);
    app.post('/users/generateApiKey',garbage, isAdmin, users.generateApiKey);
    app.post('/freeService',garbage, free_service.order);
    app.post('/freeTwitter', garbage, free_service.twitter);

    app.get('/logout', garbage, users.signout);
    app.get('/users/me', garbage, users.me);

    app.get('/users', garbage, users.all);
    app.get('/users/:userId', garbage, users.show);
    app.put('/users/:userId', garbage, users.update);
    app.delete('/users/:userId', garbage, users.destroy);
    app.post('/users/forgotPassword/:userEmail', garbage, users.sendResetRequest);
    app.get('/users/:userId/validateResetToken', garbage, users.deauth, users.validateResetToken);
    app.post('/resetPassword', garbage, users.resetPassword);
    app.post('/deleteUser', garbage, users.deleteUser);
    // Setting up the users api
    app.post('/register', garbage, users.create);

    // Setting up the userId param
    app.param('userId', garbage, users.user);
    app.param('userEmail', garbage, users.userByEmail);

    // AngularJS route to check for authentication
    app.get('/loggedin', garbage, function(req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    });

    // Setting the local strategy route
    app.post('/login', garbage, passport.authenticate('local', {
        failureFlash: true
    }), function (req,res) {
        res.send(req.user);
    });

    // Setting the facebook oauth routes
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: ['email', 'user_about_me'],
        failureRedirect: '#!/login'
    }), users.signin);

    app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '#!/login'
    }), users.authCallback);

    // Setting the github oauth routes
    /* app.get('/auth/github', passport.authenticate('github', {
        failureRedirect: '#!/login'
    }), users.signin);

    app.get('/auth/github/callback', passport.authenticate('github', {
        failureRedirect: '#!/login'
    }), users.authCallback); */

    app.get('/auth/facebook/callback2', function(req, res, next) {



            res.send('okei');

    })



    // Setting the twitter oauth routes
    app.get('/auth/twitter', passport.authenticate('twitter', {
        failureRedirect: '#!/login'
    }), users.signin);

    app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        failureRedirect: '#!/login'
    }), users.authCallback);

    // Setting the google oauth routes
    app.get('/auth/google', passport.authenticate('google', {
        failureRedirect: '#!/login',
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ]
    }), users.signin);

    app.get('/auth/google/callback', passport.authenticate('google', {
        failureRedirect: '#!/login'
    }), users.authCallback);

    // Setting the linkedin oauth routes
    app.get('/auth/linkedin', passport.authenticate('linkedin', {
        failureRedirect: '#!/login',
        scope: [ 'r_emailaddress' ]
    }), users.signin);

    app.get('/auth/linkedin/callback', passport.authenticate('linkedin', {
        failureRedirect: '#!/login'
    }), users.authCallback);

};
