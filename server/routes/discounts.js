'use strict';

// Orders routes use discounts controller
var discounts = require('../controllers/discounts');
var authorization = require('./middlewares/authorization');

var isAdmin = function(req, res, next) {
    if (!req.user.hasRole('admin')) {
        return res.send(403, 'Insufficient privileges');
    }
    next();
};

module.exports = function(app) {
    app.get('/discounts', authorization.requiresLogin, isAdmin, discounts.all);
    app.post('/discounts', authorization.requiresLogin, isAdmin, discounts.create);
    app.get('/discounts/evaluate/:discountCode', discounts.show);
    app.get('/discounts/:discountId', discounts.show);
    app.put('/discounts/:discountId', authorization.requiresLogin, isAdmin, discounts.update);
    app.delete('/discounts/:discountId', authorization.requiresLogin, isAdmin, discounts.destroy);

    // Finish with setting up the discountId param
    app.param('discountId', discounts.discount);
    app.param('discountCode', discounts.discountByCode);

};
