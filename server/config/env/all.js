'use strict';

var path = require('path');
var rootPath = path.normalize(__dirname + '/../../..');

module.exports = {
    root: rootPath,
    port: process.env.PORT || 80,
    db: process.env.MONGOHQ_URL,
    templateEngine: 'swig',

    // The secret should be set to a non-guessable string that
    // is used to compute a session hash
    sessionSecret: 'f83e2558104a9d4f6e8ee89fb30c35ba',
    // The name of the MongoDB collection to store sessions in
    sessionCollection: 'sessions'
};
