var mongoose = require('mongoose');

var Payment = mongoose.model('Payment');
var User = mongoose.model('User');
var Order = mongoose.model('Order');
var API = require('./api.js').API;
var async = require('async');


exports.multiple_orders = function (payment, recurring, req, res) {
    console.log("Never get here, eh?");


    var selection = recurring ? payment.description : payment.transactions[0].item_list.items[0].description;
    console.log("THIS - ", selection)
    var verify_id = payment.id;
    if (selection && selection.match(',')) { // multiple orders
        console.log('essa')
        var sale_id = payment.transactions[0].related_resources[0].sale.id;
        var approve_payment = recurring ? payment.description.split('Cart items: ')[1] : payment.transactions[0].item_list.items[0].description.split('Cart items: ')[1];
        var cart_order = approve_payment.split(',');
        console.log(cart_order)

        async.eachSeries(cart_order, function (item, callback) {
            Order.update({payment: item}, {
                $set: {
                    transaction: sale_id,
                    checkout: true,
                    paid: true,
                    payment_type: 'paypal',
                    verify_id: verify_id
                }
            }, function (err) {
                Payment.update({_id: item}, {$set: {approved: true, checkout: true}}, function (err) {
                    API(item, false, function () {
                        return callback();
                    })

                });
            });


        }, function done() {

            if (recurring) {
                req.session.item_price = '';
                req.session.item_id = '';
                res.redirect('/#!/success/' + req.session.item_price)
            }


        });


    } else if (selection) {
        console.log("tessa")
        var sale_id = payment.transactions[0].related_resources[0].sale.id;

        selection = recurring ? payment.description.split('Cart items: ')[1] : payment.transactions[0].item_list.items[0].description.split('Cart items: ')[1];
        console.log(selection)
        // single order
        Payment.update({_id: selection}, {$set: {approved: true, checkout: true}}, function (err) {
            Order.update({payment: selection}, {
                $set: {
                    transaction: sale_id,
                    checkout: true,
                    paid: true,
                    payment_type: 'paypal',
                    verify_id: verify_id
                }
            }, function (err) {
                Order.findOne({payment: selection}, function (err, order) {
                    console.log("FOUNd - " + err, order._id);
                    API(order._id, 'Order', function (e) {
                        if (e) console.log(e);
                        if (recurring) {
                            req.session.item_price = '';
                            req.session.item_id = '';
                            return res.redirect('/#!/success/' + req.session.item_price)
                        } else return res.redirect("/#!/success");

                    });

                })


            });
        });


    }


}


exports.verify = function (payment, recurring, type, req, res) {

    var selection = recurring ? payment.description : payment.transactions[0].item_list.items[0].description;
    var verify_id = recurring ? payment.id : payment.transactions[0].related_resources[0].sale.id;
    if (selection && selection.match(',')) { // multiple orders
        var sale_id = payment.transactions[0].related_resources[0].sale.id;
        var approve_payment = recurring ? payment.description.split('Cart items: ')[1] : payment.transactions[0].item_list.items[0].description.split('Cart items: ')[1];
        var cart_order = approve_payment.split(',');


        async.eachSeries(cart_order, function (item, callback) {


            Payment.update({_id: item}, {$set: {approved: true, checkout: true}}, function (err) {
                Order.update({payment: item}, {
                    $set: {
                        payment_type: 'paypal',
                        transaction: sale_id,
                        checkout: true,
                        paid: true,
                        payment_type: type,
                        verify_id: verify_id
                    }
                }, function (err) {
                    API(item.order, 'order', function () {
                        return callback();
                    });
                })

            });


        }, function done() {

            if (recurring) {
                req.session.item_price = '';
                req.session.item_id = '';
                res.redirect('/#!/success/' + req.session.item_price)
            }

        })


    } else if (selection) { // have only one order in cart

        selection = recurring ? payment.description.split('Cart items: ')[1] : payment.transactions[0].item_list.items[0].description.split('Cart items: ')[1];

        Payment.update({_id: selection}, {$set: {approved: true, checkout: true}}, function (err) {
            console.log(err);
            Order.update({payment: selection}, {
                $set: {
                    checkout: true,
                    paid: true,
                    payment_type: type,
                    verify_id: verify_id
                }
            }, function (err) {
                Order.findOne({payment: selection}, function (err, order) {
                    API(order._id, 'Order', function () {


                        if (recurring) {
                            req.session.item_price = '';
                            req.session.item_id = '';
                            res.redirect('/#!/success/' + req.session.item_price)
                        }
                    });
                })

            })
        })
    }


}
