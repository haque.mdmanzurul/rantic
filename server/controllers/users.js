'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Email = mongoose.model('Email'),
    async = require('async'),
    moment = require('moment'),
    _ = require('lodash'),
    request = require('request'),
    free_service = require('./free_service'),
     crypto = require('crypto');

/**
 * Auth callback
 */

exports.deauth = function(req, rex, next) {
  if(req.isAuthenticated())   req.logout();
  return next();
}
exports.saveEmails = function(req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header( 'Access-Control-Allow-Headers', 'Content-Type');
    if(req.query.email.match(/@/)) {
    var store = new Email({
        email: req.query.email
    });
    Email.update({email: req.query.email}, { email: req.query.email }, {upsert: true}, function() {

        return res.status(200).end();

    });
    } else {
        return res.status(200).end();
    }
}
exports.getEmails = function(req, res) {




    var filter = {};
    var queryParams;

    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can manage users');
    }

    if(req.query.filter && req.query.filter.email){
     //   filter.meta = {};
      filter.email = new RegExp(req.query.filter.email, 'i');
    }

    if(req.query.filter && req.query.filter.name){
      filter.name = new RegExp(req.query.filter.name, 'i');
    }

    queryParams = _.extend({}, filter);

    if(typeof req.query.page === 'undefined'){
      req.query.page = 1;
    }
    req.query.count = 1000;
    console.log(req.query);
    async.waterfall([
      function(callback){
        Email.count(queryParams, callback);
      },
      function(count, callback){
        Email.find(queryParams)
          .sort(req.query.sorting)
          .skip(req.query.count*(req.query.page-1))
          .limit(parseInt(req.query.count))
          .exec(function(err, users) {
            if (err) {
              res.render('error', {
                status: 500
              });
            } else {
              res.jsonp({
                total: count,
                count: (req.query.count ? parseInt(req.query.count) : 0),
                page: 1, //(req.query.page ? parseInt(req.query.page) : 1),
                sorting: req.query.sorting,
                data: users
              });
            }
          });
      }
    ]);

}


 exports.activate = function(req, res) {

    var host = "purchase/";
    if(!req.body.data.url.match(host)) return res.status(500).end("Invalid address, failed.");
    User.findOne({ _id: req.body.data.user}, function(err, user) {
        if(!user) return res.status(500).end("No such user");
        if(!user.not_activated) return res.status(500).end("User is already activated.");
        user.deactivate_order_link = req.body.data.url;
        user.save(function(err) {
            if(err) return res.status(500).end("Unable to save data.");
            return res.status(200).end("");
        });
    });

}

 exports.activate_user = function(req, res) {


    User.findOne({validation_key: req.params.key}, function(err, user) {
//        if(!user) return res.status(200).end("User doesn't exist or is already activated!");
        console.log(user);
        user.not_activated = false;
        user.save(function(err) {
          //  req.user = user;
             req.logIn(user, function(err) {

                if(user.deactivate_order_link) return res.redirect(user.deactivate_order_link);
                else return res.redirect("/");

             });

        });
    });
}

  exports.generateApiKey = function(req, res) {

    console.log("id: " + req.body.userId)
    User.findOne({_id: req.body.userId}, function(err, user) {
        var key = crypto.randomBytes(32).toString('hex');

            user.api_key = key;
                user.save(function(err) {
        if (err) return res.status(500).send(err.errors);
        else  return res.status(200).jsonp({key: key});

    });
    })




 }

 exports.deleteUser = function(req, res) {
    console.log(req.body.id);
      if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can delete users');
    }
    User.remove({_id: req.body.id}, function(result) { res.send(true); });
 }
exports.authCallback = function(req, res) {

    res.redirect('/');
};

/**
 * Show login form
 */
exports.signin = function(req, res) {
    if(req.isAuthenticated()) {
        return res.redirect('/');
    }
    res.redirect('#!/login');
};

/**
 * Logout
 */
exports.signout = function(req, res) {

    req.logout();
    res.redirect('/');

};

/**
 * Session
 */
exports.session = function(req, res) {
    res.redirect('/');
};

/**
 * Create user
 */        // random password gen eww.
exports.passwordGenerator = function() {

        var password = '';
        var password_values = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','0','1','2','3','4','5','6','7','8','9','!','?','/','-','.',',','<','>','{','}'];
        var password_length = password_values.length-1;

        for(var i = 0; i < 15;i++) { var random = Math.floor(Math.random()*password_length); password += password_values[random]; }

           return password;
    }

exports.create = function(req, res, next) {
   // req.body.password = exports.passwordGenerator();
    req.body.confirmPassword = req.body.password;
   var user = new User(req.body);



    user.provider = 'local';

    // because we set our user.provider to local our models/user.js validation will always be true
    req.assert('email', 'You must enter a valid email address').isEmail();
    req.assert('password', 'Password must be between 8-20 characters long').len(8, 20);
    // req.assert('username', 'Username cannot be more than 50 characters').len(1,20);
    req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).send(errors);
    }

   // for some reason instagram_user unique doesnt work




    // Hard coded for now. Will address this with the user permissions system in v0.3.5
    user.roles = ['authenticated', 'customer'];

     async.series([
        function(callback) {
    // User.findOne({instagram_user: req.body.instagram_user}, function(err, result) {
      //  if(result != null) {


      //  return res.status(400).send('This instagram user already exists!');
   // }
    callback();
  // })
 }, function() {

    user.save(function(err) {
        if (err) {
           console.log('err', err);

            if(!err.code) {
                return res.status(400).jsonp({msg: "Temporary emails not allowed."});
            }
            switch (err.code) {
                case 11000:
                    console.log('wts');
                case 11001:
                    res.status(400).send('This email already registered. You may have received your password by email. Make sure you check in the SPAM folder.');
                    break;

                default:
                    res.status(400).send('Please fill all the required fields');
            }

            return res.status(400);
        }   user.sendAutoCreateEmail(function() {     }); // send email

        req.logIn(user, function(err) {
            if (err) return next(err);
            if(req.body.instagram_user) {

             var User = mongoose.model('User');
              var data = [];

                  if(req.body.method == 'facebook') {
                         User.findOne({_id: req.user.id}, function(err, user) {
                 data.push({method: 'facebook',id: user.username, username: user.username, user_id: user._id})

                free_service.storeFree(data, req); // storing free task into db.
                free_service.startTask(data); // starting task in background

            }) }
                free_service.newOrder(user, res); // creating new free followers order





            } return res.status(200).send(user);
        });

       // res.status(200);
    });
}]);
};

/**
 * Send User
 */
exports.me = function(req, res) {

    res.jsonp(req.user || null);
};

/**
 * Show a user
 */
exports.show = function(req, res) {

    res.jsonp(req.profile);
};


/**
 * Update an user
 */
exports.update = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can edit users');
    }

    var user = req.profile;

    user = _.extend(user, req.body);

    user.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                user: user
            });
        } else {
            res.jsonp(user);
        }
    });
};

exports.resetPassword = function(req, res) {
    return res.status(200).send();
};

/**
 * Delete an user
 */

exports.destroy = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can delete users');
    }

    var user = req.profile;

    user.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                user: user
            });
        } else {
            res.jsonp(user);
        }
    });
};

exports.all = function(req, res){
    var filter = {};
    var queryParams;

    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can manage users');
    }

    if(req.query.filter && req.query.filter.email){
     //   filter.meta = {};
      filter.email = new RegExp(req.query.filter.email, 'i');
    }

    if(req.query.filter && req.query.filter.name){
      filter.name = new RegExp(req.query.filter.name, 'i');
    }

    queryParams = _.extend({}, filter);

    if(typeof req.query.page === 'undefined'){
      req.query.page = 1;
    }

    async.waterfall([
      function(callback){
        User.count(queryParams, callback);
      },
      function(count, callback){
        User.find(queryParams)
          .sort(req.query.sorting)
          .skip(req.query.count*(req.query.page-1))
          .limit(parseInt(req.query.count))
          .exec(function(err, users) {
            if (err) {
              res.render('error', {
                status: 500
              });
            } else {
              res.jsonp({
                total: count,
                count: (req.query.count ? parseInt(req.query.count) : 0),
                page: (req.query.page ? parseInt(req.query.page) : 1),
                sorting: req.query.sorting,
                data: users
              });
            }
          });
      }
    ]);
};

/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
    User
        .findOne({
            _id: id
        })
        .exec(function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('Failed to load User ' + id));
            req.profile = user;
            next();
        });
};

exports.userByEmail = function(req, res, next, email) {
    User
        .findOne({
            email: email
        })
        .exec(function(err, user) {
            if (err) return next(err);
            if (!user) return next();  //return next(new Error('Failed to load User ' + email));
            req.profile = user;
            next();
        });
};

exports.sendResetRequest = function(req, res){

    if(!req.profile) return res.send('Username with this email was not found.');


    async.series([
        function(callback){
            req.profile.newResetToken(callback);
        },
        function(callback){
            req.profile.sendResetEmail(callback);
        }
    ], function(err){
        if(err){
            return res.status(500).send('A problem occured while trying to send a password reset email');
        }

        return res.status(200).send('An email has been sent to your account with instructions');
    });

};

exports.validateResetToken = function(req, res){
    if(req.profile.isResetTokenValid && req.profile.resetToken === req.query.resetToken){
        return res.status(200).send();
    } else{
        return res.status(400).send();
    }
}

/**
 * Create user
 */
exports.resetPassword = function(req, res, next) {
    // because we set our user.provider to local our models/user.js validation will always be true
    req.assert('password', 'Password must be between 8-20 characters long').len(8, 20);
    req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400).send(errors);
    }

    User.findOne({
        resetToken: req.body.resetToken
    }).exec(function(err, user){
        if(err || !user){
            return res.status(400).send('Could not load user that matches this reset token');
        }

        if(!user.isResetTokenValid()){
            return res.status(400).send('The password reset request token is expired');
        }

        if(user.resetToken != req.body.resetToken){
            return res.status(400).send('Invalid reset token');
        }

        user.password = req.body.password;

        // Hard coded for now. Will address this with the user permissions system in v0.3.5
        user.save(function(err) {
            user.newResetToken(function(){
                user.sendResetDoneEmail(function(){
                    res.status(200).send();
                });
            });
        });
    });
};
