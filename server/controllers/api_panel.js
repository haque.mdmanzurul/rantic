'use strict';
var mongoose = require('mongoose');
var Services = mongoose.model('Service');
var User = mongoose.model('User');
var Api = mongoose.model("API");
var fromPanel = require('./api.js').API;
var fs = require('fs-extra');
var request = require('request');
var async = require('async');
var Verify = require('./orders-server').verify;
var Order = mongoose.model('Order');
var _ = require('lodash');

var folderPath = "/opt/swenzy/logs/api_created";


var getQueryParams = function(req) {
	
  var params = {};

  // anonymous users
  if (_.isEmpty(req.user)) {}
  // detects customers by way of elimination. If you're not
  // an admin or a provider, you must be a customer
  else if (!req.user.hasRole('provider') && !req.user.hasRole('admin')) {
    params.user = req.user._id;
    //params.paid = true;
  }
  // providers
  else if (
    req.user.hasRole('provider') && !req.user.hasRole('admin')
  ) {
    // params.user = req.user._id;
    params.checkout = true;
    params['service.value'] = {
      '$in': req.user.services
    };
  }

  // admins don't wanna see abandoned items
  else if (
    req.user.hasRole('admin')
  ) {
    params.checkout = true;
  }


  return params;
};


function constructExample(api, code, value, callback) {
	var comments = ["fb_comments_customelite", "ig_custom"];
	var emoji = ["fb_emoticons_elite", "yt_views_smlite_packages", "yt_sm_target"];
	var add = "https://panel.rantic.com/api/?key=YOUR_KEY&action=add&";
	var status = "https://panel.rantic.com/api/?key=YOUR_KEY&action=status&id=YOUR_API_ORDER_ID";
	if(api == "smmlite") {
		
		add += "link=http://example.com";
		add += "&service="+code;
		add += "&quantity=1000";
	
		if(comments.indexOf(value) != -1) add += "&comments=firstcomment\nsecondcomment\nthirdcomment";
		if(emoji.indexOf(value) != -1) add += "&emoticon=EMOTICON_ID";
	}

	if(api == "realfans") {
		add += "&link=http://example.com";
		add += "&service="+code;
		add += "&quantity=1000"; // in real api we need to change quantity to amount=1000
	}

	if(api == "autosmo") {
	

		add += "link=http://example.com";
		add += "&service="+code;
		add += "&quantity=1000";
	}

	if(api == "socialbox") {
		add += "link=http://example.com";
		add += "service="+code;
		add += "&quantity=1000"; // in real api we need to change quantity to count=1000
	}

	
	return callback(add,status, code);
}

exports.emails = function(req, res) {

	var filter = {};
  	var queryParams;

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

    if(req.query.filter && req.query.filter.string){
    filter['string'] = new RegExp(req.query.filter.string, 'i');
  }
  queryParams = _.extend({}, filter);
  console.log(queryParams);
  async.waterfall([
    
    function(callback) {
     Blacklist.count(queryParams, callback);
    },
    function(count, callback) {
      var query = Blacklist.find(queryParams);
      
      query.sort(req.query.sorting)
      .exec(function(err, black) {
          if (err) {
            res.render('error', {
              status: 500
            });
          } else {
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: black
            });
          }
        });
    }
  ]);


}

exports.all = function(req, res) {
	
  var filter = {from_api: true};
  var queryParams;

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

  if(req.query.filter && req.query.filter.url){
    filter.url = new RegExp(req.query.filter.url, 'i');
  }

  if(req.query.filter && req.query.filter.service){
    filter['service.value'] = req.query.filter.service;
  }
    if(req.query.filter && req.query.filter.api_id){
    filter['api_id'] = new RegExp(req.query.filter.api_id, 'i');
  }
  queryParams = _.extend({}, getQueryParams(req), filter);
console.log(queryParams);
  async.waterfall([
    function(callback) {
      if(req.query.filter && req.query.filter.username && req.user &&  req.user.hasRole('admin')){
        User.find({username: new RegExp(req.query.filter.username, 'i')}).exec(function(err, users){
          var userIds = [];
          _(users).each(function(user){
            userIds.push(user._id);
          });

          queryParams = _.extend(queryParams, {
            'user': {'$in': userIds}
          });

          callback(null);
        });
      } else {
        callback(null);
      }
    },
    function(callback) {
      Order.count(queryParams, callback);
    },
    function(count, callback) {
      var query = Order.find(queryParams);

      if(req.user && req.user.hasRole('admin')){
        query.sort({
          'unread.administrator': 'desc'
        });
      } else if(req.user && req.user.hasRole('provider')){
        query.sort({
          'unread.administrator': 'desc'
        });
      } else {
        query.sort({
          'unread.customer': 'desc'
        });
      }
      
      query.sort(req.query.sorting)
        .skip(req.query.count * (req.query.page - 1))
        .limit(req.query.count)
        .populate('user', 'name username')
        .populate('provider', 'name username profit')
        .exec(function(err, orders) {
          if (err) {
            res.render('error', {
              status: 500
            });
          } else {
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: orders
            });
          }
        });
    }
  ]);
};

exports.api = function(req, res, next, id) {
  Order.load(id, function(err, order) {
    if (err) return next(err);
    if (!order) return next(new Error('Failed to load order ' + id));
    if (req.user && req.user.hasRole('admin')) {
      order.populate(['user', 'provider', 'comments.user'], function(err, order) {
        req.order = order;
        next();
      });
    } else {
      req.order = order;
      next();
    }
  });
};
exports.example = function(req, res) {
	
	
	Services.findOne({value: req.query.data}, function(err, result) {
		if(err || !result) return res.status(500).end("No such service!");

		
		constructExample(result.api_type, result.api_nr, result.value, function(add,status, code) {

			return res.status(200).jsonp({add: add, status: status, api_code: code});

		})
		
	})

	

}

function autosmo(res, key, callback) {
	request.post({
		url: 'http://autosmo.com/api/v2',
		form:  {
			key: '74d24e843d1ff25acc1a7ab5a07f9ec8',
			action: 'status',
			id: key,
		}

	}, function(err, response, body) {
		if(err || !body) {
			res.status(200).jsonp(err)
			return callback(true);
		}
		if(body) {
			try {
				body = JSON.parse(body);
				res.status(200).jsonp({status: body.status});
				return callback(err, body);

			} catch(e) {
				
				var message = new Date() + ' -  ' + 'AUTOSMO SOMETHING WRONG API KEY:  ' + key + ' - Error msg: ' + e + '\r\n'; 
				fs.appendFile(folderPath , message);
				res.status(200).jsonp(e);
				return callback(true);
			}
			
		}
		
	})
}
function smmlite(res, key, callback) {
	request.post({
		url: 'http://smmlite.com/api/v2',
		form:  {
			key: '2b766439ce4f7207324c496843743b58',
			action: 'status',
			order: key,
		}

	}, function(err, response, body) {
		if(err || !body) {
			res.status(200).jsonp(err)
			return callback(true);
		}
		if(body) {
			try {
				body = JSON.parse(body);
				res.status(200).jsonp({status: body.status })
			} catch(e) {
				res.status(200).jsonp({status: null });
			}
			
		}
		return callback(err, body);

	});
}

function realfans(res, key, callback) {

	request('http://realfans.club/api.php', {
					timeout: 5000,
					qs: {
					action: 'status',
					order: key,
					key: '54ca1eac78290c9d311ef3b8461334566393c8b842b23e18c2abcd9c7'
			
				}					
			}, function(err, response, body) {
				if(err || !body) {
			res.status(200).jsonp(err)
			return callback(true);
		}
		if(body) {
			body = JSON.parse(body);
			res.status(200).jsonp({status: body.order_status});
			
		}
		return callback(err, body);
			});

}

function socialbox(res, key, callback) {

		request('http://api.socialsbox.com/v2/service/getorderinfo', {
					qs: {
					orderid: key,
					token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
				}					
			}, function(err, response, body) {
				if(err || !body) {
			res.status(200).jsonp(err)
			return callback(true);
		}
		if(body) {
			body = JSON.parse(body);
			res.status(200).jsonp({status: body.StatusOrder })
			
		}
		return callback(err, body);

			});

}

exports.create = function(req, res) {





	var link = req.query.link;
	var service = req.query.service;
	var action = req.query.action;
	var quantity = req.query.quantity;
	var key = req.query.key;
	var id = req.query.id;
	var speed = req.query.speed;
	var emoticon = req.query.emoticon;
	var comments = req.query.comments;
	console.log("These are comments!")
console.log(comments);

	 /* Callback to all the APIS */
	function responser(err, success, callback) {
		if(err || !success) {
			res.status(200).jsonp(err);
			return callback();
		}
		else {
			res.status(200).jsonp(success);
			return callback();
		}
	}

	if(!key) return res.status(200).jsonp({error: "Invalid key"});
	if(!action) return res.status(200).jsonp({error: "No action provided"});


	if(action == "status") {
		if(!id)  return res.status(200).jsonp({error: "No order id"});
		var orderData = {id: id};

		async.series({

			validate_key: function(callback) {
				// make sure it's the correct key and find if user is eligible to discounts.
				User.findOne({api_key: key}, function(err, user) {
					if(err || !user) {
						res.status(200).jsonp({error: "Invalid key"});
						return callback(true); // immediate break;
					}
				
					return callback();
				})

			},
			validate_api_id: function(callback) {

				Order.findOne({api_id: orderData.id, from_api: true}, function(err, key) {
					if(err || !key) {
						res.status(200).jsonp({error: "Invalid order id"});
						return callback(true);
					}
					orderData.key = key.api_type;
					return callback();
				});


			},
			apply_api: function(callback) {

				if(orderData.key == "autosmo") autosmo(res, orderData.id, callback);
				else if(orderData.key == "smmlite") smmlite(res, orderData.id, callback);
				else if(orderData.key == "realfans") realfans(res, orderData.id, callback);
				else if(orderData.key == "socialbox") socialbox(res, orderData.id, callback);
				else return callback(true);
			


				
			}
		});
		
		
	}
	
	else if(action == "add") {
		var orderData = {};
		if(!link) return res.status(200).jsonp({error: "No link provided"});
		if(!quantity) return res.status(200).jsonp({error: "No quantity"});


	
	User.findOne({"api_key": req.query.key}, function(err, user) {
		if(!user) return res.status(200).jsonp({error: "Invalid key"});
		orderData.api_discount = user.api_discount || false;
		orderData.balance = user.balance;
		orderData.user_id = user._id;
		orderData.key = user.api_key;
		Services.findOne({ api_nr: service }, function(err, data) {
			if(!data) return res.status(200).jsonp({error: "Invalid service"});
			async.series({
				create_order: function(callback) {
					var new_api = new Order({
						"service.value": data.value,
						"service.label": data.label,
						"service.price": data.price,
						"service.multiplier": data.multiplier,
						quantity: quantity,
						paid: true,
						verified: false,
						completed: false,
						old: false,
						url: link,
						api_type: data.api_type,
						orderCount: quantity,
						eat_speed: speed,
						emoticon: emoticon,
						ig_comments: JSON.stringify(comments),
						user: orderData.user_id,
						from_api: true,
						checkout: true,
						price: 0
						
					}).save(function(err, result) {
						console.log(result);
						orderData.api = result;
						return callback();
					});




				},
				verify_price: function(callback) {

					Verify(orderData.api, orderData.api._id, false, req, res, function(price, qty) {
						orderData.api.quantity = qty;

					if(orderData.api_discount) {
						console.log("Appling price");
						console.log("before discount is: " + price);
						var tmp = price*parseFloat(orderData.api_discount);
							tmp = price-tmp;
							price = tmp;
							console.log("new price is: " + price);
							
					}
					var currentBalance = (orderData.balance-price).toFixed(2);
					if(currentBalance < 0) {
						
						Order.remove({_id: orderData.api._id}, function(err) {
							res.status(200).jsonp({error: "Not enough balance"});
							return callback(true);
						});
						
					}
						orderData.price = price;
						orderData.balance = currentBalance;
						return callback();
					}, true);


				},

				register_key: function(callback) {
					fromPanel(orderData.api._id, true, function(key, err) {
						
						var message = new Date() + ' -  ' + orderData.api.service.value + ' User Key: '+orderData.key+' - API KEY:  ' + key + ' - Error msg: ' + err + '\r\n'; 
						fs.appendFile(folderPath , message);
						
						if(!key) {
							if(!err) err = {error: "Invalid query" };
							Order.remove({_id: orderData.api._id}, function(e) {
								err.warning = "If you have ( # <--- hashtag ) in your query please replace it with %23";

								res.status(200).jsonp(err);
								return callback(true);
							
							});
							
						}
						else {
							Order.update({_id: orderData.api._id},{$set: {api_id: key, price: orderData.price}}, function(err, result) {
								res.status(200).jsonp({order: key});
								return callback();
							})
					
						}
					}, true);
				}, 
				update_balance: function(callback) {
					User.update({_id: orderData.user_id}, { $set: { balance: orderData.balance}}, function(err, result) {
						return callback();
					})
				}
			
			});

		
		
	});
	})

} else return res.status(200).jsonp({error: "Invalid status"});

	
}


