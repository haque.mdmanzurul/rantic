'use strict';

var mean = require('meanio');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Order = mongoose.model('Order');
var Logs = mongoose.model('Logs');
var Payment = mongoose.model('Payment');
var async = require('async');
var _ = require('lodash');



var getQueryParams = function(req) {
  var params = {};

  // anonymous users
  if (_.isEmpty(req.user)) {}
  // detects customers by way of elimination. If you're not
  // an admin or a provider, you must be a customer
  else if (!req.user.hasRole('provider') && !req.user.hasRole('admin')) {
    params.user = req.user._id;
    //params.paid = true;
  }
  // providers
  else if (
    req.user.hasRole('provider') && !req.user.hasRole('admin')
  ) {
    // params.user = req.user._id;
    params.checkout = true;
    params['service.value'] = {
      '$in': req.user.services
    };
  }

  // admins don't wanna see abandoned items
  else if (
    req.user.hasRole('admin')
  ) {
    params.checkout = true;
  }


  return params;
};




// refactor when everything is done!!
exports.logs = function(req, res) {
  var queryParams; // is undefined!
  var query = {};
  var filter = {};
  var count = (!req.query.filter || !req.query.filter.username) ? (req.query.count * (req.query.page - 1)) : '';
  var limit = (!req.query.filter || !req.query.filter.username) ? parseInt(req.query.count) : '';
  queryParams = _.extend(queryParams, filter);
 
 console.log(req.query.sorting);
  async.waterfall([
  function(callback) {

    Payment.count({}, callback);
  },
   function(total_count, callback) {
    

     var sorted_logger = [];

      if(req.query.filter && req.query.filter.username) {
      var logger = [];
      filter.username = new RegExp(req.query.filter.username, 'i');
      User.find(filter).exec(function(err, result) {

        filter = {};
        async.eachSeries(result, function iterator(item, callback) {
          filter.user = item._id
          var get_data = Payment.find(filter);
          get_data.populate('user','name username')
          .populate('order','_id url checkout paid cancelled verified completed')
          .exec(function(err, done) {
              async.eachSeries(done, function iterator(item, callback) {
        if(item.order && !item.order.paid)  logger.push({message: 'order created', data: item});
        if(item.order && item.order.checkout && item.order.paid)   logger.push({message: 'order inprogress', data: item});
        if(item.order && !item.order.paid && !item.order.checkout)  logger.push({message: 'order abandoned', data: item});
        if(item.order && item.order.paid && item.order.completed) logger.push({message: 'order completed', data: item});
        if(item.billing && item.billing.topup && !item.checkout && !item.order) { logger.push({message: 'Topup created', data: item}); }
        if(item.billing && item.billing.topup && item.checkout && !item.order) {
          console.log(item._id);
         logger.push({message: 'TopUp', data: item}); 
       }

        if(!item.approved && !item.checkout && item.billing && !item.billing.topup)  logger.push({message: 'payment created', data: item});
        
        if(item.approved && item.checkout && item.order)  logger.push({message: 'payment successful', data: item});
         callback(null);
         }, function done() {



           Order.find(filter).populate('user', 'name username')
           .exec(function(err, data) {
          async.eachSeries(data, function(item, callback) {
           // console.log(item.payment );
            if(item.checkout && item.paid && !item.payment)  {
              console.log(item.url);
              var data = {};
              data.order = item;
              data.user = item.user;
              data.created = item.created;
              data.price = item.price;

              logger.push({message: 'order inprogress', data: data}); 
            }
            callback();

          }, function done() {
            sorted_logger = logger.slice(0); // copy array
            sorted_logger.sort(function(a,b) {
          
              return b.data.created-a.data.created;
            });
            res.jsonp({
              total: total_count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: sorted_logger,
              order: null  
            });

        

           

          })
        })


         });    

           
       

         });
           
        })

      })
    }else {








    
      var logger = [];
      var logs = Payment.find(filter);
        logs.sort(req.query.sorting)
        .skip(count)
        .limit(limit)
        .populate('user', 'name username')
        .populate('order','_id url checkout paid cancelled verified completed')
        .exec(function(err, data) {
         
      async.eachSeries(data, function(item, callback) {
        
        if(item.order && !item.order.paid)  logger.push({message: 'order created', data: item});
        if(item.order && item.order.checkout && item.order.paid)   logger.push({message: 'order inprogress', data: item});
        if(item.order && !item.order.paid && !item.order.checkout)  logger.push({message: 'order abandoned', data: item});
        if(item.order && item.order.paid && item.order.completed) logger.push({message: 'order completed', data: item});
        
        if(item.billing && item.billing.topup && !item.checkout) { logger.push({message: 'Topup created', data: item}); }
        if(item.billing && item.billing.topup && item.checkout) {
          console.log(item._id);
         logger.push({message: 'TopUp', data: item}); 
       }

        if(!item.approved && !item.checkout && item.billing && !item.billing.topup)  logger.push({message: 'payment created', data: item});
        



        if(item.approved && item.checkout && item.order)  logger.push({message: 'payment successful', data: item});
         callback(null);

      }, function done() {
        Order.find({})
        .sort(req.query.sorting)
        .skip(count)
        .limit(limit)
        .populate('user', 'name username')
        .exec(function(err, data) {
          async.eachSeries(data, function(item, callback) {
           // console.log(item.payment );
            if(item.checkout && item.paid && !item.payment)  {
              console.log(item.url);
              var data = {};
              data.order = item;
              data.user = item.user;
              data.created = item.created;
              data.price = item.price;

              logger.push({message: 'order inprogress', data: data}); 
            }
            callback();

          }, function done() {
            var sorted_logger = logger.slice(0); // copy array
            sorted_logger.sort(function(a,b) {
          
              return b.data.created-a.data.created;
            });
            res.jsonp({
              total: total_count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: sorted_logger,
              order: null  
            });

          })
        })
         



      });
});

}
      }]);
















}


exports.all = function(req, res) {



 var filter = {};
  var queryParams;
  var order_id = [];

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

  if(req.query.filter && req.query.filter.url){
    filter.url = new RegExp(req.query.filter.url, 'i');
  }

  if(req.query.filter && req.query.filter.service){
    filter['service.value'] = req.query.filter.service;
  }
 







  queryParams = _.extend({},  filter);

function comment_loop(callback2, data, callback) {
		var keys = Object.keys(data);
		keys.forEach(function(i) {
				
    					search_comment(data[i]._id, function(data, item_count) {
    						
    						callback(data, item_count);
    					})

    			
    		});
    			}


function search_comment(id, callback) {
	filter['_id'] = id;

	
	 queryParams = _.extend(queryParams, filter);

	 var get_data = Order.find(queryParams, { comments: { $slice: -1 }});


	get_data.sort(req.query.sort)
  //.skip(req.query.count * (req.query.page - 1))
        .limit(parseInt(req.query.count))
        .populate('user', 'name username').exec(function(err, data) {
        
        	if(data[0] && String(data[0].comments[0].user) === String(data[0].user._id))  {
        		
        		callback(data, 10);
        	}else callback([], 10);
        	
    			
    });


}
  var total_counter = 0;
  async.waterfall([
 
  	
    function(callback) {
    	
    	var get_count = Order.find({comments: { $exists: true, $not: {$size: 0}}},{ comments: { $slice: -1}});
    		get_count.populate('user', 'name username')
    	    .populate('provider', 'name username profit').exec(function(err, data) {
    			
    				 var userIds = [];
    				 var runner = 0;
        		  _(data).each(function(user){
        		  	runner++;
				       		 
        		  	if(user.user && user.user._id && String(user.comments[0].user) == String(user.user._id))  {
        		  		total_counter++;
        		  	
        		  		
        		  	}
        		  

          });
        		  callback();

    	})
    },
     
    function(callback) {
    	
    	
    		
			

			
    		var get_data = Order.find({comments: { $exists: true, $not: {$size: 0}}});
    		get_data.sort(req.query.sorting).skip(req.query.count * (req.query.page - 1))
       	.limit(parseInt(req.query.count))
        .populate('user', 'name username')
        .populate('provider', 'name username profit').exec(function(err, result) {
    			var total_loop = result.length;
    			var email_users = [];
    			var curr_holder;
    			comment_loop(callback, result, function(data, item_count) {
    			
    				total_loop--;
    				
    				 if(data.length) order_id.push(data); 
    			
    				
    			if(total_loop == 0)  {
    				 order_id.sort(function(a, b) {
        a = new Date(a[0].created).getTime();
        b = new Date(b[0].created).getTime();
        return  parseInt(b)- parseInt(a);
});

    			if(req.query.filter && req.query.filter.name && req.user.hasRole('admin')) {

    				var keys = Object.keys(order_id);
    					var total_keys = keys.length;
    				keys.forEach(function(key) {
    					total_keys--;
    				if(order_id[key][0].user.name.match(new RegExp(req.query.filter.name,'i'))) {
    							email_users.push(order_id[key]);
    						}
    				if(total_keys == 0) {

    				}


    			})
    					}
    			if(req.query.filter && req.query.filter.username && req.user.hasRole('admin')) {
    					
    					var keys = Object.keys(order_id);
    					var total_keys = keys.length;
    				keys.forEach(function(key) {
    					total_keys--;
    				
    					if(order_id[key][0].user.username.match(new RegExp(req.query.filter.username,'i'))) {
    							email_users.push(order_id[key]);
    						}
    					if(total_keys == 0) {
    						// EOF

    							 res.jsonp({
              total: total_counter, // Order.count(queryParams, callback);
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: email_users
            });

    					}
    					
    					
    				})
    			 



    	 
      				}



      				else {
      					
      					console.log(total_counter + 'smoooooht')
    			 res.jsonp({
              total: total_counter, //order_id.length, // Order.count(queryParams, callback);
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: order_id
            });
    			}
    			}
    			})
    	
    			
    	})
    		
    }
   
    
   
  ]);





















	
}
