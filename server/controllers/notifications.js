'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var User = mongoose.model('User');
var Payment = mongoose.model('Payment');
var Notification = mongoose.model('Notification');

var fs = require('fs');
var ObjectId = mongoose.Types.ObjectId;
var API = require('./api.js').API;
var async = require('async');
var _ = require('lodash');
var Twocheckout = require('2checkout-node');
var kue = require('kue');
var jobs = kue.createQueue();
var config = require('./../config/config');
var User = mongoose.model('User');


/**
 * Create an order
 */
var winston = require('winston');
  winston.loggers.add('paypal_error', {
    console: {
      level: 'info',
      colorize: true,
    },
    file: {
      filename: config.main_folder + '/logs/paypal_error'
    }
  });

winston.loggers.add('2checkout_ins', {
 file: {
   filename: config.main_folder + '/logs/2checkout-ins'
 }
});


var checkout = winston.loggers.get('2checkout_ins');

function saveMsg(message) {
        return checkout.info(message);
}


exports.notify = function(req, res, next) {
  saveMsg(req.body); // save ins request in a log
  var notification = new Notification(req.body);
  var payment;

  var tco = new Twocheckout({
    sellerId: config.two_checkout.sellerId, 
    privateKey: config.two_checkout.privateKey, 
    secretWord: "tango"
  });

  async.series([
    function(callback){
    //  console.log('body', req.body);
      if (tco.notification.valid(req.body)) {
        callback()
      } else {
       console.log("jay fail");
        callback(new Error('Invalid INS'));
      }
    },
    function(callback){
      console.log('saving!');
      notification.save(function(err, saved){
        notification = saved;
        callback(err);
      });
    },
    function(callback){
	if(!req.body.vendor_order_id && !req.body.item_id_1) var order_id = null;
      else {
        var order_id = new ObjectId(req.body.vendor_order_id || req.body.item_id_1);
        
      }
      Payment.findOne({_id: order_id}).populate([{path: 'user', model: 'User'}, {path: 'order', model: 'Order'}]).exec(function(err, found){
        if(err){
        
          return callback(err);
        }

        if(!found){
          
          return callback(new Error('Not found'));
        }

        payment = found;
        callback();
      });
    },
    function(callback){

     
      if(notification.get('message_type') == 'ORDER_CREATED') {
        
            var parent_id = payment._id.toString();
            Payment.find({parent_id: parent_id}, function(err, result) {
             
               
              if(result) {

              async.eachSeries(result, function iterator(item, cb) {
                var sales = notification.get('sale_id');
                Order.update({ _id: item.order}, { $set: {transaction: sales,sale_id: notification.get('sale_id') , checkout: true, paid: true, verified: true,  payment_type: '2checkout'}},{upsert: true}, function(err) {   
                  Payment.update({_id: item._id}, { $set: { approved: true, checkout: true }},{upsert: true}, function(err) { return cb(); 
                  });
                });

                  
                }, function done() {



                return callback(null); 
                                

            })
              // didn't find such payment, fail quietly.
          } else return callback(true)


            })




      }

      if(
        (
          notification.get('invoice_status') == 'approved'
          || notification.get('invoice_status') == 'deposited'
          || notification.get('fraud_status')  == 'pass'
        )
        && notification.get('fraud_status') != 'fail' && notification.get('fraud_status') != 'wait' && notification.get('message_type') != 'ORDER_CREATED'
      ){
       
 
    
            var parent_id = payment._id.toString();
            Payment.find({parent_id: parent_id}, function(err, result) {
              
              
              if(result) {
               
              async.eachSeries(result, function iterator(item, cb) {
                    var sales = notification.get('sale_id');
//                    API(item.order,'order', function(key) {

                      Order.update({_id: item.order}, { $set: {payment_type: '2checkout',transaction: sales, sale_id: notification.get('sale_id'), checkout: true, paid: true, verified: false}},{upsert: true}, function(err, data) {
			API(item.order, 'order', function(key) {

                        return cb();
                      });

                    });
                    
                    
                    
                }, function done() {
                  

                 
                      payment.approved = true;
                      payment.checkout = true;
                      payment.fraud = false;

                       payment.save(function(err, saved){
                        return callback(false);
                      });

                

            })

          } else return callback(true)


            })
       
          
      } else if(notification.get('fraud_status') === 'fail') {
       
        var parent_id = payment._id.toString();
       
          Payment.find({parent_id: parent_id}, function(err, result) {
            
            if(result) {
          async.eachSeries(result, function iterator(item, cb) {

             
                Order.update({_id: item.order}, { $set: { verified: false, cancelled: true}}, function(err) {
                  
                  return cb();
                });
                
                
            }, function done() {
                            
                   payment.save(function(err, saved){
                      return res.status(200).send('FRAUD FAIL');
                     
                  });

              
         

        })

      } else return callback(true);

        })




       
        
      } else if(payment.checkout == false) {
        payment.checkout = true;
        payment.save(function(err, saved){
          return res.status(200).send('CHECKOUT STATUS UPDATED');
        });
      } else {
        return res.status(200).send('NO CHANGE');
        // callback();
      }
    },
    function(callback){
      if(payment.order){
        if(payment.user.name === 'Rantic Customer' && notification.get('customer_name')){
          payment.user.name = notification.get('customer_name');
        }

        jobs.create('count', {
          title: payment.order.url,
          order: payment.order._id,
          type: 'before'
       }).priority('critical').save();

        payment.order.checkout = true;
        payment.order.paid = true;
        payment.order.verified = false;
        payment.fraud = false;
        payment.order.save(function(err, saved){
          callback(err);
        });
      } else {
        callback(null);
        // payment.user.balance = parseFloat(payment.user.balance) + parseFloat(payment.price);
        //payment.user.save(function(err, saved){
          //callback(err);
        //});
      }
    }
  ], function(err){
    if(err && err.toString() === 'Error: Not found'){
      return res.status(200).send('NOT FOUND');
    } else if(err){
      return res.status(500).send('ERROR');
    }

    else return 
  });
};
