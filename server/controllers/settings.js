'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Setting = mongoose.model('Setting');
var User = mongoose.model('User');
var async = require('async');
var _ = require('lodash');
var re_weburl = require('./../config/util').re_weburl;
var re_email = require('./../config/util').re_email;
var email = require('./../services/email.js');
var kue = require('kue');
var jobs = kue.createQueue();

exports.setting = function(req, res, next, name){
  Setting.findOne({name: name}).exec(function(err, setting){
    req.requestedSetting = setting;
    next(err);
  });
}

exports.load = function(req, res, next){
  res.jsonp(req.requestedSetting);
};

exports.update = function(req, res, next){
  req.requestedSetting.value = req.body.value;
  req.requestedSetting.save(function(err, saved){
    if(err){
      return res.jsonp(err);
    }

    res.jsonp(saved);
  });
};

exports.options = function(req, res, next){
    return res.status(200).jsonp();
};
