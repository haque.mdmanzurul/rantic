'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Discount = mongoose.model('Discount');
var async = require('async');
var _ = require('lodash');

exports.discount = function(req, res, next, id) {
  Discount.findOne({_id: id}, function(err, discount) {
    if (err) return next(err);
    if (!discount) return next(new Error('Failed to load discount ' + id));
    req.discount = discount;
    next();
  });
};

exports.discountByCode = function(req, res, next, id) {
  Discount.findOne({code: new RegExp(id, 'i')}, function(err, discount) {
    if (err) return next(err);
    if (!discount) return res.send('');
    req.discount = discount;
    next();
  });
};

exports.show = function(req, res) {
  res.jsonp(req.discount);
};


exports.all = function(req, res) {
  var filter = {};
  var queryParams;

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

  queryParams = _.extend({}, filter);

  async.waterfall([
    function(callback) {
      console.log('queryParams', queryParams);
      Discount.count(queryParams, callback);
    },
    function(count, callback) {
      Discount.find(queryParams)
        .sort(req.query.sorting || {})
        .skip(req.query.count * (req.query.page - 1))
        .limit(parseInt(req.query.count))
        .exec(function(err, discounts) {
          if (err) {
            res.render('error', {
              status: 500
            });
          } else {
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: discounts
            });
          }
        });
    }
  ]);
};

exports.update = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can edit discounts');
    }

    var discount = req.discount;
    discount = _.extend(discount, req.body);
    console.log('updater');
    console.log(discount);
    discount.save(function(err, discount) {
        if (err) {
            return res.status(500).jsonp(err);
        } else {
            res.jsonp(discount);
        }
    });
};

exports.destroy = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can delete discounts');
    }

    var discount = req.discount;

    discount.remove(function(err) {
        if (err) {
            return res.status(500).jsonp(err);
        } else {
            res.jsonp(discount);
        }
    });
};

exports.create = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can create discounts');
    }

    var discount = new Discount({});
    discount = _.extend(discount, req.body);
console.log(discount);
console.log("????");
    discount.save(function(err, discount) {
        if (err) {
            return res.status(500).jsonp(err);
        } else {
            res.jsonp(discount);
        }
    });
};
