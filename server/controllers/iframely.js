'use strict';

/**
 * Module dependencies.
 */
var request = require('request'),
    _ = require('lodash');

/**
 * iFramely gateway
 */
exports.gateway = function(req, res) {
    // http://iframely.com/iframely?uri=
    request({
        url: 'http://iframely.com/iframely',
        qs: {
            uri: req.query.uri
        },
        json: true
    }, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        res.jsonp(body);
      }
    })
};
