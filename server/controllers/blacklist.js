'use strict';
var request = require('request');
var mongoose = require('mongoose');
var Blacklist = mongoose.model('Blacklist');
var User = mongoose.model("User");
var async = require('async');

var _ = require('lodash');







exports.delete = function(req, res) {
	var id  = req.body.data;

	Blacklist.remove({ _id: id}, function(err) {
		if(err) return res.status(500).end("No such item");
		return res.status(200).end("");
	});
}

exports.edit = function(req, res) {
	var client = req.body.data;

	Blacklist.findOne({_id: client._id}, function(err, data) {
		console.log(data);
		if(!data) return res.status(500).end("No such item.");
		data.string = client.string;
		data.save(function(err) {
			if(err) return res.status(500).end("Unable to save item");
			return res.status(200).end("");
		});
	});

	
}


exports.add = function(req, res) {
	var query = req.body.data;
	if(!query) return res.status(500).end("No string");
	Blacklist.find({string: query}, function(err, data) {
		
		if(data.length) return res.status(500).end("It's already blacklisted!");
		var create = new Blacklist({
			string: query
		}).save(function(err) {
			console.log(err);
			if(err) return res.status(500).end("Unable to save!");
			return res.status(200).end('');
		});
	});

}
exports.query = function(req, res) {


	var filter = {};
  	var queryParams;

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

    if(req.query.filter && req.query.filter.string){
    filter['string'] = new RegExp(req.query.filter.string, 'i');
  }
  queryParams = _.extend({}, filter);
  console.log(queryParams);
  async.waterfall([
    
    function(callback) {
     Blacklist.count(queryParams, callback);
    },
    function(count, callback) {
      var query = Blacklist.find(queryParams);
      
      query.sort(req.query.sorting)
        .skip(req.query.count * (req.query.page - 1))
        .limit(parseInt(req.query.count))
        .exec(function(err, black) {
          if (err) {
            res.render('error', {
              status: 500
            });
          } else {
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: black
            });
          }
        });
    }
  ]);

}