'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Payment = mongoose.model('Payment');
var User = mongoose.model('User');
var Order = mongoose.model('Order');
var Blacklist = mongoose.model('Blacklist');

var API = require('./api.js').API;
var async = require('async');
var _ = require('lodash');
var re_weburl = require('./../config/util').re_weburl;
var re_email = require('./../config/util').re_email;
var email = require('./../services/email.js');
var kue = require('kue');
var jobs = kue.createQueue();
var Twocheckout = require('2checkout-node');
var paypal = require('paypal-rest-sdk');
var fs = require('fs');
var config = require('./../config/config');
var paypalRecurring = require('./paypal_recurring');
var moment = require('moment');

paypal.configure({
    'mode': config.paypal.mode,
    'client_id': config.paypal.client_id,
    'client_secret': config.paypal.client_secret

});



var getQueryParams = function(req){
    var params = {};

    // anonymous users
    if(_.isEmpty(req.user)){}
    // detects customers by way of elimination. If you're not
    // an admin or a provider, you must be a customer
    else if (
        !req.user.hasRole('provider')
        && !req.user.hasRole('admin')
    ){
        params.user = req.user._id;
    }
    // providers
    else if (
        req.user.hasRole('provider')
        && !req.user.hasRole('admin')
    ){
        // params.user = req.user._id;
        params['service.value'] = {'$in' : req.user.services};
    }

    return params;
};

/**
 * Find payment by id
 */
exports.payment = function(req, res, next, id) {
    Payment
        .findOne({"_id": id})
        .populate('order')
        .exec(function(err, payment) {
            if (err) return next(err);
            if (!payment) return next(new Error('Failed to load payment ' + id));
            req.payment = payment;
            next();
        });
};

exports.options = function(req, res, next){
    return res.status(200).jsonp();
};

/**
 * Create an payment
 */
exports.create = function(req, res, next) {
    if(req.body.user) req.body.user = req.body.user.toLowerCase();

    req.cart = {};
    req.cart.id_array = []; // array for holding cart payment ids.
    var add_cart = function(original_payment, price, username,parent_id, callback) {


        var payment = new Payment({
            price: price,
            billing:  {},
            recurring: req.body.recurring
        });

        User.findOne({email:  new RegExp(username,'i')}, function(err, user){

            if(user){
                // if the user already exists, let's attach him to the order
                payment.user = user;
                payment.fraud = true;
                payment.parent_id = parent_id;
                payment.save(function(err){

                    payment.paymentId = undefined;

                    req.cart.id_array.push(payment._id); // store id in array for later user.

                    callback(null, null);


                });

            } else {
                // if he doesn't, we first random generate a password
                require('crypto').randomBytes(8, function(ex, buf) {
                    var password = buf.toString('hex');

                    // then create the user object
                    user = new User({
                        username: username,
                        instagram_user: username,
                        email: username,
                        name: 'Rantic Customer',
                        roles: ['authenticated', 'customer'],
                        password: password,
                    });

                    // save it..
                    user.save(function(err, userr){
                        if(err) {
                            if(!err.code) return res.status(400).send("Temporary emails not allowed!");

                        }

                        payment.user = user;
                        payment.fraud = true;
                        payment.parent_id = parent_id;
                        //payment.needs_activation = true;

                        payment.save(function(err){
                            payment.paymentId = undefined;


                            req.cart.id_array.push(payment._id); // store id in array for later user.

                            callback(null, null);
                        });



                    });
                });
            }
        });




    }
    if(req.body.user) req.body.user = req.body.user.toLowerCase();


    if(!req.user && !req.body.user){
        return res.status(400).send('Cannot create an order without an email.');
    }

    if(req.body.price <= 0){

        res.status(400).send('Cannot process empty payments. Please make sure you\'re not trying to place an empty or negative payment.');
    } else {

        var payment = new Payment({
            price: req.body.price,
            billing: req.body.billing || {},
            recurring: req.body.recurring,
            topup: false,
            manual_verify: false,
            payment_type: req.body.payment_type ? req.body.payment_type : false
        });

        async.waterfall([

            function(callback) {
                if(req.body.url == "add_cart") return callback(null);

                if(!req.body.cart) return callback(null);
                async.eachSeries(req.body.cart, function(item, cb) {
                    Blacklist.find({}, function(err, data) {
                        async.eachSeries(data, function(black, caller) {
                            if(item.url.match(black.string)) return res.status(500).end("Address is blacklisted!");
                            return caller();
                        }, function done() {
                            return cb();
                        })
                    });

                }, function done() {
                    return callback(null);

                });

            },
            function(callback){
                // tie the payment to the current logged in user
                if(req.user){
                    //if(req.user.not_activated) return res.status(500).end("Please check your mail to verify your email first, then you can proceed with the payment.");
                    payment.user = req.user;
                    callback(null);
                }
                else {
                    if(!req.body.user.match(re_email)){
                        return res.status(400).send('The Email doesn\'t look valid. Please enter a valid email address.');
                    }

                    User.findOne({email: new RegExp(req.body.user,'i')}, function(err, user){
                        if(user){

                            //if(!req.user && user.not_activated) return res.status(500).end("Please check your mail to verify your email first, then you can proceed with the payment.");
                            // if the user already exists, let's attach him to the order
                            payment.user = user;
                            callback(err);
                        } else {
                            // if he doesn't, we first random generate a password
                            require('crypto').randomBytes(8, function(ex, buf) {
                                var password = buf.toString('hex');

                                // then create the user object
                                user = new User({
                                    username: req.body.user,
                                    instagram_user: req.body.user,
                                    email: req.body.user,
                                    name: 'Rantic Customer',
                                    roles: ['authenticated', 'customer'],
                                    password: password,
                                });

                                // save it..
                                user.save(function(err, userr){

                                    if(err) {
                                        if(!err.code) return res.status(400).send("Temporary emails not allowed!");
                                    }

                                    user.sendAutoCreateEmail(function(){
                                        // attach it to the payment
                                        payment.user = user;
                                        //payment.needs_activation = true;

                                        // and viola!
                                        callback(err);
                                    });
                                });
                            });
                        }
                    });
                }
            },
            function(callback){


                payment.save(function(err){


                    payment.paymentId = undefined;
                    callback(err, payment._id);
                });
            }
        ], function(err, parent_id){
            if(err){

                return res.jsonp(new Error('Error saving payment'));
            }

            if(req.body.url == 'add_cart') { // create cart payments


                async.eachSeries(req.body.cart, function iterator(item, callback) {
                    if(item && item.email) item.email = item.email.toLowerCase();
                    item.email = (req.user) ? req.user.email : item.email;

                    add_cart(payment, item.price,item.email,parent_id,callback);

                }, function done() {

                    res.jsonp({payment: payment, id_array: req.cart.id_array});
                })



            }else  {   res.jsonp(payment);  }
        });
    }
};

exports.approve = function(req, res) {
    async.series([
            function(callback){
                req.payment.approve(callback);
            }
        ],
        function(err, callback){
            if(err){
                return res.jsonp(new Error('Error while approving payment'));
            }

            res.jsonp(payment);
        });
};

exports.purchase = function(req, res){
    console.log(req);
    /* Reduce item price if user still has balance */

    var tco = new Twocheckout({
        sellerId: config.two_checkout.sellerId,
        privateKey: config.two_checkout.privateKey,
        sandbox: config.two_checkout.sandbox
    });



    // Setup checkout params
    var params = {
        mode: '2CO',
        li_0_type: 'product',
        li_0_name: 'Rantic Order',
        li_0_description: (req.query.id_array) ? req.query.id_array : '', // only for add cart payments to get order ids
        li_0_price: parseFloat(req.payment.price).toFixed(2),
        li_0_recurrence: req.body.recurring ? '1 Month' : false,
        x_receipt_link_url: req.query.origin + '/return/' + req.payment._id.toString(),
        return_url: req.query.origin + '/return/' + req.payment._id.toString(),

        purchase_step: 'billing-information',
        li_0_product_id: req.payment._id.toString()
    };



    if(req.user == 'admin' && req.user && req.user.balance > 0 || req.user && req.user.refund_balance > 0) {
        /* If user has more balance than the actual cost don't do payment */
        if(parseFloat(req.user.balance) >= parseFloat(req.payment.price) || parseFloat(req.user.refund_balance) >= parseFloat(req.payment.price)) {
            if(!req.user.refund_balance) req.user.balance = parseFloat(req.user.balance) - parseFloat(req.payment.price);
            else req.user.refund_balance = parseFloat(req.user.refund_balance) - parseFloat(req.payment.price);

            req.user.save(function(err) {


                req.payment.paid = true;

                req.payment.save(function(err) {
                    Payment.find({ parent_id: req.payment.id}, function(err, list) {

                        async.eachSeries(list, function(item, callback) {

                            Payment.update({_id: item._id}, { $set: { approved: true, checkout: true }}, function(err) {
                                Order.findOne({payment: item._id}, function(err, data) {
                                    data.checkout = true;
                                    data.paid = true;
                                    data.verified = false;
                                    data.save(function() {
                                        API(data._id, 'order', function() {
                                            return callback();
                                        });
                                    });
                                });


                            });

                        }, function done() {
                            return res.redirect('/');
                        });


                    });

                });

            });

        } else {


            req.payment.price = req.payment.price - req.user.balance;
            req.user.balance = 0;
            req.user.save(function(err) {
                req.payment.save(function(err) {
                    return payNow();
                });

            });

        }


    } else return payNow();
    function payNow() {



        if(req.query.paymentMethod === 'Paypal'){
            //var descriptions =
            var  descriptions = " URL: https://panel.rantic.com/#!/orders/" + req.payment._id;



            var create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "paypal"
                },
                "redirect_urls": {
                    "return_url": req.query.origin + '/return/' + req.payment._id.toString(),
                    "cancel_url": req.query.origin
                },
                "transactions": [{
                    item_list: {
                        items: []
                    }

                    ,
                    "amount": {
                        "currency": "USD",
                        "total": parseFloat(req.payment.price).toFixed(2)
                    }
                }]
            };



            async.waterfall([
                function getOrders(callback) {
                    Payment.find({parent_id: req.payment._id}, function(err, data) {
                        console.log('Purchase Step - Payment Create', data);
                        if(err || !data) return callback(err);
                        async.eachSeries(data, function(item, cb) {
                            Order.findOne({_id: item.order}, function(err, order) {
                                if(err || !order) return callback(err);

                                console.log(parseFloat(order.price).toFixed(2));
                                create_payment_json.transactions[0].item_list.items.push({
                                    "name": 'Rantic Order',
                                    "sku": order._id.toString(),
                                    "price": parseFloat(order.price).toFixed(2),
                                    "currency": "USD",
                                    "quantity": 1,//order.order_tmp,
                                    "description": "https://panel.rantic.com/#!/orders/" + order._id + " Service: " + order.service.label + " Total quantity: " + order.order_tmp + ' Cart items: ' + req.query.id_array
                                });
                                return cb();
                            });
                        }, callback);
                    });
                }, function initalizePayment(callback) {
                    console.log(parseFloat(req.payment.price).toFixed(2));

                    paypal.payment.create(create_payment_json, function (error, payment) {
                        console.log('Paypal Create Payment call response: ', payment);
                        if (error) {

                            console.log('Paypal Payment create error: ', error);

                        } else {
                            _(payment.links).forEach(function(link){
                                if(link.rel === 'approval_url'){
                                    console.log('Redirect to paypal: ', link.href);
                                    res.redirect(link.href);
                                    return false;
                                }
                            });
                        }
                    });

                }
            ])


        } else {
            req.query.id_array = (typeof(req.query.id_array) == 'string') ? [req.query.id_array] : req.query.id_array.splice(",");


            var link = tco.checkout.link(params);
            res.redirect(link);

        }

    }

};

exports.return = function(req, res) {

    var config = require('./../config/config');
    var folder_path = config.paypal_log;
    console.log('Payment Return call log path: ', folder_path);

    var user;
    req.payment.populate('user', function(err, payment){
        user = payment.user;
    });

    if(req.query.paypal_direct == 'fail'){
        fs.appendFile(folder_path, 'Payment failed: ' + req.payment.user._id+"\n", function() {
            req.payment.failed = true;
            req.payment.save(function(){
                return res.redirect('/');
            });
        });
        return;
    }

    if(req.query.paypal_direct == 'cancel'){
        fs.appendFile(folder_path, 'Payment cancelled: ' + req.payment.user._id+"\n", function() {

            req.payment.cancelled = true;
            req.payment.save(function(){
                return res.redirect('/');
            });

        });

        return;
    }
    console.log('Paypal Notify Start Series Process: ', req.payment);
    async.series([
            function(callback){
                if(req.query.paymentId && req.query.PayerID){
                    var execute_payment_json = {
                        "payer_id": req.query.PayerID,
                        "transactions": [{
                            "amount": {
                                "currency": "USD",
                                "total":  parseFloat(req.payment.price).toFixed(2)
                            }
                        }]
                    };

                    console.log('Paypal Payment JSON: ', folder_path);
                    paypal.payment.execute(req.query.paymentId, execute_payment_json, function (error, payment) {
                        console.log('Paypal Notify Payment data: ', JSON.stringify(payment));
                        if (error) {
                            console.log('Paypal Notify Response error: ', error);
                            fs.appendFile(folder_path, error, function() {
                                var date = moment().format();
                                fs.appendFile(folder_path,  "\n User: " + req.payment.user._id + " Date: " + date + "\n", function() {

                                    return callback(error);
                                });
                            });
                        } else {
                            console.log('Paypal Notify Payment data transaction: ', JSON.stringify(payment.transactions));
                            if(payment.transactions[0].related_resources[0].sale.reason_code && payment.transactions[0].related_resources[0].sale.reason_code == 'ECHECK') {
                                paypalRecurring.verify(payment,false,'paypal');

                            }

                            else if(payment.transactions[0].item_list.items[0].description && payment.transactions[0].item_list.items[0].description.match('Cart items:')) {
                                console.log("PAYPAL  KEKKKKKKKKKKK");
                                paypalRecurring.multiple_orders(payment,false, req, res);

                            }
                            else if(payment.state === 'approved' && payment.transactions[0].item_list.items.length > 0) {

                                _.forEach(payment.transactions[0].item_list.items, function (item) {
                                    console.log('Paypal Notify Payment data transaction: ', item);
                                    Order.update({_id: item.sku}, {
                                        $set: {
                                            checkout: true,
                                            paid: true,
                                            transaction: payment.transactions[0].related_resources[0].sale.id,
                                            payment_type: 'paypal'
                                        }
                                    }, function (err, count) {
                                        API(item.sku, 'Order', function (e) {
                                            if (e) console.log('Sending to API error', e);

                                        });
                                        console.log('Order Updated and Payment Id', count, err);
                                    });

                                    //Update the payment reference
                                    Payment.update({paymentId: req.payment.paymentId, parent_id: req.payment._id, order: item.sku}, {
                                        $set: {
                                            approved: true,
                                            checkout: true,
                                            fraud: false,
                                            payment_type: 'paypal',
                                            verify_id: req.query.paymentId
                                        }
                                    }, function (err, count) {
                                        console.log('Payment Updated', count, err);
                                    });
                                });

                                return res.redirect('/#!/success/');



                            }
                            else callback();

                        }
                    });
                } else {
                    callback();
                }
            },
            function(callback) {


                if(req.body.total && parseInt(req.body.total) != parseInt(req.payment.price)) {
                    console.log('Invalid price: ', req.body.total, req.payment.price);
                    return callback("Invalid price");
                } else return callback();
            },
            function(callback){

                console.log('Before Fold Callback: ', req.body);


                if(req.body.li_0_description && req.body.li_0_description.match(',')) { // multiple orders
                    //req.payment.approve(callback);
                    var cart_order = req.body.li_0_description.split(',');



                    async.eachSeries(cart_order, function(item, cb) {


                        Payment.update({_id: item}, { $set: { approved: true, checkout: true }}, function(err) {
                            Order.update({ payment: item}, { $set: { checkout: true, paid: true}}, function(err) {

                                return cb();
                            });
                        });


                    }, function done() {
                        return callback();
                    });




                } else if(req.body.li_0_description) { // have only one order in cart
                    // req.payment.approve(callback);

                    Payment.update({_id: req.body.li_0_description}, { $set: { approved: true, checkout: true }}, function(err) {
                        Order.update({ payment: req.body.li_0_description}, { $set: { checkout: true, paid: true}}, function(err) {
                            API(Order._id, 'Order', function (e) {
                                if (e) console.log('Sending to API error', e);

                            });
                            return callback();
                        });
                    });
                } else {

                    return res.redirect('/#!/fold/');
                }

            },
            function(callback){

                req.payment.populate('user', function(err, payment){

                    user = payment.user;
                    callback(err);
                });
            },
            function(callback){

                req.logIn(user, callback);
            }
        ],
        function(err, callback){
            if(err) {

                return res.redirect('/#/error');
            } else  return res.redirect('/#!/success/'+parseFloat(req.payment.price).toFixed(2));
        });
};


exports.process = function(req, res){



    var message;
    var isFail;
    var tco = new Twocheckout({

        sellerId: config.two_checkout.sellerId,
        privateKey: config.two_checkout.privateKey,
        sandbox: config.two_checkout.sandbox

    });

    var params = {
        "merchantOrderId": req.payment._id.toString(),
        "token": req.body.token,
        "currency": "USD",
        "total": parseFloat(req.payment.price).toFixed(2),
        "billingAddr": {
            "name": req.payment.billing.name,
            "addrLine1": req.payment.billing.addrLine1,
            "city": req.payment.billing.city,
            "state": req.payment.billing.state,
            "zipCode": req.payment.billing.zipCode,
            "country": req.payment.billing.country,
            "email": req.payment.billing.email,
            "phoneNumber": req.payment.billing.phoneNumber
        }
    };

    async.series([
            function(callback){
                tco.checkout.authorize(params, function (error, data) {
                    if (error) {
                        isFail = true;
                        message = error.message;
                        callback();
                    } else {
                        req.payment.data = data;
                        message = data.response.responseMsg;
                        callback();
                    }
                });
            },
            function(callback){
                req.payment.token = req.body.token;
                req.payment.message = message;

                if(isFail){
                    req.payment.populate('user', function(err, payment){



                    });
                    console.log('Tow"checkout Payment Failed', req.payment);
                    req.payment.failed = true;
                    req.payment.save(function(err, saved){
                        callback(err || new Error(message));
                    });
                } else {
                    req.payment.populate('user', function(err, payment){


                    });
                    console.log('Tow"checkout Payment Success', req.payment);
                    req.payment.approve(callback);
                }
            }
        ],
        function(err){
            if(err){
                if(message){

                    return email.twoCheckoutError(req.payment, message, function() {
                        res.status(500).jsonp(message);
                    });
                } else {
                    return res.jsonp('Error while approving payment');
                }
            }

            return res.jsonp(req.payment);
        });
}

exports.all = function(req, res) {
    var filter = {};

    var queryParams;
    var total_services;

    if (typeof req.query.page === 'undefined'){
        req.query.page = 1;
    }




    queryParams = _.extend({}, getQueryParams(req), filter);



    async.waterfall([
        function(callback) {
            if(req.query.filter && req.query.filter.service){
                Order.find({'service.value': req.query.filter.service}).exec(function(err, users){
                    var userIds = [];
                    _(users).each(function(user){
                        userIds.push(user.user);
                    });

                    queryParams = _.extend(queryParams, {
                        'user': {'$in': userIds}
                    });
                    total_services = userIds.length;

                    callback(null);
                });


            }else { callback(null);}
        },



        function(callback) {
            if(req.query.filter && req.query.filter.name){
                User.find({name: new RegExp(req.query.filter.name, 'i')}).exec(function(err, users){
                    var userIds = [];
                    _(users).each(function(user){
                        userIds.push(user._id);
                    });

                    queryParams = _.extend(queryParams, {
                        'user': {'$in': userIds}
                    });

                    callback(null);
                });


            }else { callback(null);}
        },

        function(callback) {
            if(req.query.filter && req.query.filter.username){
                User.find({username: new RegExp(req.query.filter.username, 'i')}).exec(function(err, users){
                    var userIds = [];
                    _(users).each(function(user){
                        userIds.push(user._id);
                    });

                    queryParams = _.extend(queryParams, {
                        'user': {'$in': userIds}
                    });

                    callback(null);
                });


            }else { callback(null);}
        },
        function(callback) {
            Payment.count(queryParams, callback);
        },
        function(count, callback) {

            Payment.find(queryParams)
                .sort(req.query.sorting)
                .skip(req.query.count * (req.query.page - 1))
                .limit(parseInt(req.query.count))
                .populate('user', 'name username email')
                .populate('order', 'url service.label service.value')
                // .populate('provider', 'name username profit')
                .exec(function(err, payments) {
                    if (err) {

                        res.render('error', {
                            status: 500
                        });
                    } else {
                        if(req.query.filter && req.query.filter.service){
                            var filtered_data = [];
                            for(var i = 0; i < payments.length;i++) {
                                if(payments[i].order) {

                                    if(payments[i].order.service.value == req.query.filter.service) { filtered_data.push(payments[i]); }

                                }

                            }

                        }

                        res.jsonp({
                            total: count,
                            count: (req.query.count ? parseInt(req.query.count) : 0),
                            page: (req.query.page ? parseInt(req.query.page) : 1),
                            sorting: req.query.sorting,
                            data: !filtered_data ? payments : filtered_data
                        });
                    }
                });
        }
    ]);
};
