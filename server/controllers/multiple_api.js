var mongoose = require('mongoose');
var Round = mongoose.model('Api_rotation');

exports.check = function(data, cb, remaining) {
  var serviceList = ["ig_likes"];
  var api = [527, 526, 170];
  if(serviceList.indexOf(data.service.value) !== -1) {
    Round.findOne({ order_id: data._id}, function(err, order) {
      var nextApi = false;
      if(order && order.total <= 0) {
        order.next_api = false;

        order.save(function() {
          return cb(false);
        })
      }

      else if(!order) {

        var count = data.quantity - 20000;
        var api_qty = data.quantity < 20000 ? data.quantity : 20000;
        nextApi = true;
        var newApi = new Round({
          order_id: data._id,
          total: count,
          next_api: nextApi,
          next_api_nr: 1

        });
        newApi.save(function() {
          var API = require("./api.js").API;

          API(data._id,'order', function(result) {
            return cb(true);
          }, false, api[0], api_qty);

        });
    } else {
      if(!order.next_api) {
        order.next_api = false;

        order.save(function() {
          return cb(false);
        });
      } else {
        var count = order.total;
      if(remaining) count = count + parseInt(remaining);
      var currentAPI = order.next_api_nr;
      var api_qty = 0;
      if(order.next_api_nr == 0) {
        api_qty = count < 20000 ? count : 20000;
        count = count - 20000;
        if(count > 0)   nextApi = true;
          order.next_api_nr = 1;


      }
      else if(order.next_api_nr == 1) {
        api_qty = count <= 15000 ? count : 15000;
        count = count - 15000;

          order.next_api_nr = 2;
        if(count > 0)  nextApi = true;

      } else if(order.next_api_nr == 2) {
        api_qty = count < 50000 ? count : 50000;
        count = count - 50000;

        order.next_api_nr = 0;
        if(count > 0)     nextApi = true;

      } else { return cb(false); }
      order.total = count;
      order.next_api = nextApi;
      order.save(function() {
        var API = require("./api.js").API;
        var Order = mongoose.model('Order');
        console.log("TYRA - " + order.order_id)
        Order.update({_id: order.order_id}, { $set: {
          completed: false,
          verified: false,
          api_id: false
        }}, function() {
          API(order.order_id, 'order', function(err, result) {
            console.log("UPDATING API????!!!", err, result);
            console.log(order);
            return cb(true);
          }, false, api[currentAPI], api_qty);

        })

      });

    }
    }
  })

} else   return cb(false);
}
