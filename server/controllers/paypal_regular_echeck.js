var paypal = require('paypal-rest-sdk');
var config = require('./../config/config');
var mongoose = require('mongoose');
var async = require('async');
mongoose.connect(config.db);
require('./../models/order.js');
var Order = mongoose.model('Order');
var winston = require('winston');
var API = require('./api.js').API;
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.File)({
      name: 'info',
      filename: 'logs/paypal_regular',
      level: 'info'
    })
  ]
});

paypal.configure({
    'mode': config.paypal.mode,
    'client_id': config.paypal.client_id, 
    'client_secret': config.paypal.client_secret 
    
});




// This is for regular payments, paid with echeck aka bank transfer, takes up to a week for payment to go through.




Order.find({verified: true, paid: true, completed: false, payment_type: 'paypal', recurring: false}, function(err, response) {


	// finds all payments, calls to paypal api to get current state, if completed change state
	async.eachSeries(response, function iterator(item, callback) {

		paypal.order.get(item.verify_id, function (error, order) {
			if(error) return callback();
   	  if(order) {
   
     		if(order.state == 'completed') {
          API(item._id,'order', function(key) {

            Order.update({
            verify_id: item.verify_id
          }, { $set: {
            // change order status, goes to inprogress
            verified: false
            }}, function(err) {
              logger.info('Payment completed: ' + item._id);
              return callback();
            });


          });
     			
     		} else {
     			logger.info('Payment still pending! ' + item._id);
     			return callback();
     		}
    } else return callback();

	})


	}, function done() {
			// closes script, otherwise it stays hanging
		mongoose.connection.close()
	});

});



