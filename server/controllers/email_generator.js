'use strict';
var mongoose = require('mongoose');
var Services = mongoose.model('Service');
var User = mongoose.model('User');
var Api = mongoose.model("API");
var fromPanel = require('./api.js').API;
var fs = require('fs-extra');
var request = require('request');
var async = require('async');
var Verify = require('./orders-server').verify;
var Order = mongoose.model('Order');
var _ = require('lodash');

var folderPath = "/opt/swenzy/logs/api_created";//'/home/xc_global/latest_rantic/logs/api_created';




exports.emails = function(req, res) {
   global.gc();
  var filter = {};
  var queryParams;
  var list = [];
  var action = {}
  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }
    if(req.query.filter && req.query.filter.service) {
      filter['service.value'] = req.query.filter.service;
    }

  filter['url'] = { $ne: "add_cart"};
  filter['paid'] = true;
  filter['cancelled'] = false;
  filter['checkout'] = true;
  filter['price'] = { $gte: 1 };

  if(req.query.filter && req.query.filter.status) {
    var status = req.query.filter.status;
    if(status == 'Complete') {
      filter.completed = true;
      filter.verified = true;
      filter.refund = false;
      filter.refunded = false;
      }

    else if(status == 'Inprogress') {

     filter.completed = false;
     filter.verified = false;
     filter.cancelled = false;
     filter.paid = true;
     filter.checkout = true;
     filter.refund = false;
     filter.refunded = false;

   } else if(status == 'Cancelled') {
     filter.completed = false;
     filter.verified = false;
     filter.cancelled = true;

   } else if(status == 'Abandoned') {
      filter.completed = false;
      filter.verified = false;
      filter.cancelled = false;
      filter.paid = false;
      filter.checkout = false;
    }
  }

  if(!req.query.filter) filter['price'] = { $gte: 999999999999999999999999999999 };
  if(req.query.filter && req.query.filter.multiple_services && req.query.filter.multiple_services != "ALL") {
      if(typeof req.query.filter.multiple_services != 'object') filter['service.value'] = req.query.filter.multiple_services;
      else  filter['service.value'] = { $in: req.query.filter.multiple_services };
      delete req.query.filter.multiple_services;

  }
  if(req.query.filter && req.query.filter.email) {
    User.findOne({ email: req.query.filter.email}, function(err, data) {
      if(data) filter['user'] = data._id;
        return main(sendResponse);
    });

  } else return main(sendResponse);


    function sendResponse(data) {

  return res.jsonp({
              total: 1,
              count: 1,
              page: 1,
              sorting: req.query.sorting,
              data: data
            });


    }

    function main(callbacker) {
	var special = false;
	
	if(req.query.filter && req.query.filter.multiple_services && req.query.filter.multiple_services == "ALL") {
	  filter['price'] = { $gte: 1 };
	  special = true;
	}
	queryParams = _.extend({}, filter);
	if(special) return doAll();
	
	
	function doAll() {
	console.time("measure");	  
	  Order.find(queryParams).count(function(err, total) {
	    var start = 0;
	    var maxi = 50000;

	    var times = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	    async.eachSeries(times, function(i, cbm) {
	     if(total <= maxi && start == 0) maxi = total;
	     if(total <= 0) return cbm();
	console.log("SEARCH - ", start, total);		  
	    var search = Order.find(queryParams).populate("user", "_id email").skip(start).limit(maxi).lean().exec(function(err, black) {
	       async.eachSeries(black, function(item, cb) {
	
			
             if(!item || !item.user) return process.nextTick(cb);
              var addFilter = (filter['service.value']) ? true : false;
              if(list.indexOf(item.user.email) == -1) list.push(item.user.email);
              if(!action[item.user.email]) action[item.user.email] = {price: 0, email: item.user.email, service: (req.query.filter && req.query.filter.service || addFilter) ? item.service.label : '' };
              action[item.user.email].price += item.price;
		return process.nextTick(cb); 
	}, function done() {
	      
	      
	      total -= maxi;
	      start += maxi;
	      return cbm();
	      
	    });
	});
	    }, function done() {
		
	        var black = [];
              list = [];
              var price = 1; // default value for faster search
              if(req.query.filter && req.query.filter.price) price = req.query.filter.price;

             var newObject = {};

            for(var i in action) {
              if(action[i].price >= price) newObject[i] = action[i];
            }

            function sortObject(obj) {
              var arr = [];

              for(var prop in obj) {
                if(obj.hasOwnProperty(prop)) {
                  arr.push({
                    price: obj[prop].price,
                    email: obj[prop].email,
                    service: obj[prop].service
                  });
                }
              }

              arr.sort(function(a,b) {
                return b.price-a.price;
              });
              return arr;
            }

            action = sortObject(newObject);
 console.timeEnd("measure");
	return callbacker(action);

	      
	  });
	  
	  });
}

      var query = Order.find(queryParams)
      
	query.populate("user", "_id email").lean().exec(function(err, black) {
          if (err) return res.status(500).end("Error 1112");
							
            async.eachSeries(black, function(item, cb) {
	
	
             if(!item || !item.user) return process.nextTick(cb);
              var addFilter = (filter['service.value']) ? true : false;
              if(list.indexOf(item.user.email) == -1) list.push(item.user.email);
              if(!action[item.user.email]) action[item.user.email] = {price: 0, email: item.user.email, service: (req.query.filter && req.query.filter.service || addFilter) ? item.service.label : '' };
              action[item.user.email].price += item.price;
              return process.nextTick(cb); //return cb();
//		return cb();
	}, function done() {
	  
	      black = [];
              list = [];
              var price = 1; // default value for faster search
              if(req.query.filter && req.query.filter.price) price = req.query.filter.price;

             var newObject = {};

            for(var i in action) {
              if(action[i].price >= price) newObject[i] = action[i];
            }

            function sortObject(obj) {
              var arr = [];

              for(var prop in obj) {
                if(obj.hasOwnProperty(prop)) {
                  arr.push({
                    price: obj[prop].price,
                    email: obj[prop].email,
                    service: obj[prop].service
                  });
                }
              }

              arr.sort(function(a,b) {
                return b.price-a.price;
              });
              return arr;
            }

            action = sortObject(newObject);


	return callbacker(action);

          global.gc();
	      
	});


    })

    }
}
