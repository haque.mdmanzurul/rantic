'use strict';

var mean = require('meanio');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Order = mongoose.model('Order');
var Payment = mongoose.model('Payment');
var Settings = mongoose.model('Setting');

var async = require('async');
var _ = require('lodash');
var order_data = [];
var getQueryParams = function(req) {
  
  var params = {};

  // anonymous users
  if (_.isEmpty(req.user)) {}
  // detects customers by way of elimination. If you're not
  // an admin or a provider, you must be a customer
  else if (!req.user.hasRole('provider') && !req.user.hasRole('admin')) {
    params.user = req.user._id;
    //params.paid = true;
  }
  // providers
  else if (
    req.user.hasRole('provider') && !req.user.hasRole('admin')
  ) {
    // params.user = req.user._id;
    params.checkout = true;
    params['service.value'] = {
      '$in': req.user.services
    };
  }

  // admins don't wanna see abandoned items
  else if (
    req.user.hasRole('admin')
  ) {
    params.checkout = true;
  }


  return params;
};



exports.verify = function(req, res) {
  // Also need to update balance xD
  Payment.update({_id: req.query.id},{ $set: { manual_verify: false }}, function(err, data) {
     Settings.findOne({name: "newsflash"}).exec(function(err, data) {
        if(err) return res.status(500).end(err);
        Payment.findOne({_id: req.query.id}, function(err, payments) {
          console.log(req.user);

         var topupPrice = 0;
          if(payments.price >= data.value.topup.price) {
            var tmp_price = payments.price*(data.value.topup.percentage/100);
            topupPrice = req.user.balance + tmp_price + payments.price;
          } else topupPrice = req.user.balance + payments.price;

         User.update({_id: req.user._id},{ $set: { balance: topupPrice }}, function(err) {
          if(err) return res.status(500).end(err);
          return res.status(200).end('');
         });
         
        });
       

      });


   
  });



}

exports.manual = function(req, res) {
  var filter = {};
  var queryParams;

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

  queryParams = _.extend({}, getQueryParams(req), filter);
console.log(queryParams);
  async.waterfall([
    function(callback) {
      if(req.query.filter && req.query.filter.username && req.user &&  req.user.hasRole('admin')){
        User.find({username: new RegExp(req.query.filter.username, 'i')}).exec(function(err, users){
          var userIds = [];
          _(users).each(function(user){
            userIds.push(user._id);
          });

          queryParams = _.extend(queryParams, {
            'user': {'$in': userIds}
          });

          callback(null);
        });
      } else {
        callback(null);
      }
    },
    function(callback) {
      Order.count({ manual_verify: true, checkout: true, paid: true }, callback);
    },
    function(count, callback) {
      var query = Order.find({manual_verify: true, checkout: true, paid: true});

      
     // query.sort(req.query.sorting)
        query.skip(req.query.count * (req.query.page - 1))
        .limit(req.query.count)
        .populate('user', 'name username')
        .populate('provider', 'name username profit')
        .exec(function(err, orders) {
          order_data = orders;
         return callback(null);
        });
    },
    function (callback) {
      Payment.count({ manual_verify: true, checkout: true, approved: true, completed: true, payment_type: true }, callback);
    },
    function(count, callback) {
      var query = Payment.find({manual_verify: true, checkout: true, approved: true, completed: true, payment_type: true});

      
      
     // query.sort(req.query.sorting)
        query.skip(req.query.count * (req.query.page - 1))
        .limit(req.query.count)
        .populate('user', 'name username')
        .populate('provider', 'name username profit')
        .exec(function(err, payments) {

           if (err) {
            res.render('error', {
              status: 500
            });
          } else {
            var result = order_data.concat(payments);
            console.log("UH OH");
            console.log(result);
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: result
            });
          }
         
        });

    }
  ]);
}
exports.user = function(req, res) {
 	 var order_id = [];
 	 var filter = {};
  var queryParams;
  var query = {};
  queryParams = _.extend(queryParams, filter);
 


 	var sorted_logger = [];
    var logger = [];
      filter.user= req.params.actionsId
   
          var get_data = Payment.find(filter);
          get_data.populate('user','name username')
          .populate('order','_id url checkout paid cancelled verified completed')
          .exec(function(err, done) {
              async.eachSeries(done, function iterator(item, callback) {
        if(item.order && !item.order.paid)  logger.push({message: 'order created', data: item});
        if(item.order && item.order.checkout && item.order.paid)   logger.push({message: 'order inprogress', data: item});
        if(item.order && !item.order.paid && !item.order.checkout)  logger.push({message: 'order abandoned', data: item});
        if(item.order && item.order.paid && item.order.completed) logger.push({message: 'order completed', data: item});
        if(item.billing && item.billing.topup && !item.checkout && !item.order) { logger.push({message: 'Topup created', data: item}); }
        if(item.billing && item.billing.topup && item.checkout && !item.order) {
          console.log(item._id);
         logger.push({message: 'TopUp', data: item}); 
       }

        if(!item.approved && !item.checkout && item.billing && !item.billing.topup)  logger.push({message: 'payment created', data: item});
        
        if(item.approved && item.checkout && item.order)  logger.push({message: 'payment successful', data: item});
         callback(null);
         }, function done() {



           Order.find(filter).populate('user', 'name username')
           .exec(function(err, data) {
          async.eachSeries(data, function(item, callback) {
           // console.log(item.payment );
            if(item.checkout && item.paid && !item.payment)  {
              console.log(item.url);
              var data = {};
              data.order = item;
              data.user = item.user;
              data.created = item.created;
              data.price = item.price;

              logger.push({message: 'order inprogress', data: data}); 
            }
            callback();

          }, function done() {
            sorted_logger = logger.slice(0); // copy array
            sorted_logger.sort(function(a,b) {
          
              return b.data.created-a.data.created;
            });
            res.jsonp({
              
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: sorted_logger,
              order: null  
            });

        

           

          })
        })


         });    

           
       

         });
      
      }



