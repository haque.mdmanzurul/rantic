'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var User = mongoose.model('User');
var ServiceModel = mongoose.model("Service");
var Blacklist = mongoose.model('Blacklist');
var Payment = mongoose.model('Payment');
var Verify = require('./orders-server').verify;
var Discount = mongoose.model('Discount');
var async = require('async');
var _ = require('lodash');
var re_weburl = require('./../config/util').re_weburl;
var re_email = require('./../config/util').re_email;
var email = require('./../services/email.js');
var kue = require('kue');
var jobs = kue.createQueue();
var API = require('./api.js').API;
var paypal = require('paypal-rest-sdk');
var config = require('./../config/config');
var Twocheckout = require('2checkout-node');
var util = require('../services/util')

paypal.configure({
  'mode': config.paypal.mode,
  'client_id': config.paypal.client_id,
  'client_secret': config.paypal.client_secret

});

var winston = require('winston');

winston.loggers.add('order', {
  console: {
    level: 'info',
    colorize: true,
    label: 'order'
  },
  file: {
    filename: '/opt/swenzy/logs/order-errors'
  }
});

var logging = winston.loggers.get('order');

var getQueryParams = function (req) {
  var params = {};

  // anonymous users
  if (_.isEmpty(req.user)) params.invalid_session = true;
  // detects customers by way of elimination. If you're not
  // an admin or a provider, you must be a customer
  else if (!req.user.hasRole('provider') && !req.user.hasRole('admin')) {
    params.user = req.user._id;
    //params.paid = true;
  }
  // providers
  else if (
    req.user.hasRole('provider') && !req.user.hasRole('admin')
  ) {
    // params.user = req.user._id;
    params.checkout = true;
    params['service.value'] = {
      '$in': req.user.services
    };
  }

  // admins don't wanna see abandoned items
  else if (
    req.user.hasRole('admin')
  ) {
    //  params.checkout = true;
  }


  return params;
};

/**
 * Find order by id
 */

exports.modify_api = function (req, res) {
  Order.update({
    _id: req.params.orderId
  }, {
    $set: {
      api_id: req.body.api_id
    }
  }, function (err) {
    return res.status(200).end("");
  })
}

exports.inprogress = function (req, res) {
  var url = req.body.url;
  var service = req.body.service;
  Order.findOne({
    url: url,
    "service.value": service,
    completed: false,
    paid: true,
    cancelled: false,
    verified: false,
    checkout: true,
    refund: false,
    refunded: false
  }, function (err, data) {

    if (data) return res.status(500).end("There is already an order for that URL that is in-progress. Please wait until it's delivered.")
    else return res.status(200).end();

  });

}


exports.url = function (req, res) {



  if (req.user.hasRole("customer")) {

    /* Make sure that the order belongs to the user, otherwise shitfuck */

    Order.findOne({
      _id: req.body.order_id
    }, function (err, order) {
      if (!order.refund) return res.status(500).jsonp({
        msg: "Order not marked as refund"
      });
      if (order.user.toString() !== req.user._id.toString()) return res.status(500).jsonp({
        msg: "Invalid order id"
      });

      order.url = req.body.url;
      order.refund = false;
      order.api_id = null;
      order.save(function () {
        API(order._id, 'order', function (result) {
          console.log(result);
          console.log(order._id);
          return res.status(200).end();
        });
      });

    });

  } else {
    Order.update({
      _id: req.body.order_id
    }, {
      $set: {
        url: req.body.url
      }
    }, function (err) {
      if (err) return res.status(500).jsonp({
        msg: "Failed to edit url"
      });
      return res.status(200).end();
    });

  }

}
exports.refunded = function (req, res) {
  Order.update({
    _id: req.body.order_id
  }, {
    $set: {
      refunded: true
    }
  }, function (err, resp) {
    if (err) return res.status(500).jsonp({
      error: err
    });
    return res.status(200).end();
  });
}
exports.return_balance = function (req, res) {
  var order_id = req.body.order_id;

  if (req.user.hasRole("customer")) {



    Order.findOne({
      _id: order_id
    }, function (err, order) {
      if (!order.refund) return res.status(500).jsonp({
        msg: "Order not marked as refund"
      });
      if (order.user.toString() !== req.user._id.toString()) return res.status(500).jsonp({
        msg: "Invalid order id"
      });

      // Update order status
      order.refunded = true;
      order.save(function () {

        if (!req.user.refund_balance) req.user.refund_balance = order.price;
        else req.user.refund_balance += order.price;
        // update balance
        req.user.save(function () {
          return res.status(200).end();
        });

      });



    });




  } else {

    Order.findOne({
      _id: order_id
    }, function (err, data) {
      if (err) return res.status(500).jsonp({
        message: 'No such OrderID'
      });
      else {
        User.findOne({
          _id: data.user
        }, function (err, user) {
          if (err) return res.status(500).jsonp({
            message: "No such user."
          });
          else {

            user.balance = user.balance + data.price;
            user.save(function (err) {
              data.completed = true;
              data.verified = true;
              data.save(function (order_err) {
                if (err) return res.status(500).jsonp({
                  message: 'Unable to restore balance.'
                });
                if (order_err) return res.status(500).jsonp({
                  message: 'Unable to close order.'
                });
                else return res.status(200).end();
              })

            })

          }
        })


      }
    });

  }


}
exports.re_order = function (req, res) {
  var service = req.body.service;
  var update = {};
  if (req.body.comments) update.ig_comments = req.body.comments;
  if (req.body.hashtag) update.hashtag = req.body.hashtag;
  if (req.body.followers) update.followers = req.body.followers;
  if (req.body.ig_media) update.ig_media = req.body.ig_media;
  if (req.body.ig_mentions) update.ig_mentions = req.body.ig_mentions;
  update.api_id = null;
  update.quantity = req.body.orderCount;
  update.orderCount = req.body.orderCount;
  update.completed = false;
  update.verified = false;
  update.refund = false;
  update.refunded = false;
  update.url = req.body.url;
  ServiceModel.findOne({
    value: req.body.service
  }, function (err, data) {
    if (data.dropdown && typeof req.body.dropdown_index === "number") {
      var dropdown = JSON.parse(data.dropdown);
      if (dropdown[req.body.dropdown_index] && dropdown[req.body.dropdown_index].api_code) update.emoticon = dropdown[req.body.dropdown_index].api_code;
    }

    Order.update({
      _id: req.body.order_id
    }, {
      $set: update
    }, function (resp) {
      API(req.body.order_id, 'order', function (result) {
        console.log("rebuy", result);
        res.status(200).end();
      });

    });


  });






}

exports.change_api = function (req, res) {
  Order.update({
    _id: req.body.order_id
  }, {
    $set: {
      api_id: parseInt(req.body.new_key)
    }
  }, function (err, resp) {
    if (err) return res.status(500).send('Unable to change key!');
    return res.status(200).send('OK');
  });
}
exports.abandoned_paypal = function (req, res) {

  var origin = "https://panel.rantic.com";
  var create_payment_json = {
    "intent": "sale",
    "payer": {
      "payment_method": "paypal"
    },
    "redirect_urls": {
      "return_url": req.query.origin + '/return/' + req.order.payment.toString(),
      "cancel_url": origin
    },
    "transactions": [{
      "item_list": {
        "items": [{
          "name": 'Rantic Order',
          "sku": req.order.payment.toString(),
          "price": parseFloat(req.order.price).toFixed(2),
          "currency": "USD",
          "quantity": 1,
          "description": 'Cart items: ' + req.order.payment

        }]
      },
      // "custom": (req.query.add_cart) ? 'Cart items: ' + req.query.id_array : '',
      "amount": {
        "currency": "USD",
        "total": parseFloat(req.order.price).toFixed(2)
      }
    }]
  };
  paypal.payment.create(create_payment_json, function (error, payment) {

    if (error) {

      //  throw error;
    } else {
      _(payment.links).forEach(function (link) {
        if (link.rel === 'approval_url') {

          res.redirect(link.href);
          return false;
        }
      });
    }
  });

}
exports.abandoned_credit = function (req, res) {

  var tco = new Twocheckout({
    sellerId: config.two_checkout.sellerId,
    privateKey: config.two_checkout.privateKey,
    sandbox: config.two_checkout.sandbox
  });
  var params = {
    mode: '2CO',
    li_0_type: 'product',
    li_0_name: 'Rantic Order',
    li_0_description: req.order.payment, // only for add cart payments to get order ids
    li_0_price: parseFloat(req.order.price).toFixed(2),
    li_0_recurrence: false,
    x_receipt_link_url: req.query.origin + '/return/' + req.order.payment.toString(),
    return_url: req.query.origin + '/return/' + req.order.payment.toString(),

    purchase_step: 'billing-information',
    li_0_product_id: req.order.payment.toString()
  };

  var link = tco.checkout.link(params);
  return res.redirect(link);


}
exports.abandoned = function (req, res) {
  console.log(req.body);
  console.log("fuck off");
  return false;
  async.waterfall([
    function (callback) {
      Order.findOne({
        _id: req.body._id
      }, function (err, order) {

        return callback(null, order);
      })

    },
    function (order, callback) {

      Payment.findOne({
        order: req.body._id
      }, function (err, payment) {
        return callback(null, order, payment);
      })

    },
    function (order, payment, callback) {
      if (!order.discount.code) return callback(null, order, payment, false);
      Discount.findOne({
        code: new RegExp(order.discount.code, 'i')
      }, function (err, discount) {
        return callback(null, order, payment, discount);

      });

    },
    function (order, payment, discount, callback) {

      Verify(order, payment, discount, req, res, function (err, pay, verified_price, qty) {

        var account_balance = req.user.balance - order.price;
        account_balance = req.user.refund_balance ? req.user.refund_balance - order.price : account_balance;
        if (!req.user) return res.status(500).jsonp({
          error: "No user"
        });
        /*
                    if(req.user == 'adminsdjw' && req.user && req.user.balance > 0 || req.user == "adminsi11" && req.user && req.user.refund_balance > 0) {
                      if(parseFloat(req.user.balance) >= parseFloat(order.price) || parseFloat(req.user.refund_balance) >= parseFloat(order.price)) {

                        Payment.update({ order: req.body._id }, { $set: { approved: true, checkout: true  }}, function(err, resp) {
                          Order.update({ _id: req.body._id }, { $set: { quantity: qty, orderCount: qty, completed: false, verified: false, cancelled: false, paid: true, checkout: true, topup: false   }}, function(err, resp) {
                            var balance = req.user.refund_balance ?  { refund_balance: account_balance } : { balance: account_balance };
                            User.update({_id: req.body.userid }, { $set: balance }, function(err, resp) {
                              return res.status(200).jsonp({balance: account_balance});
                            });
                           });
                        });

                      } else {

                        order.price = order.price - req.user.balance;
                        order.price = req.user.refund_balance ? order.price - req.user.refund_balance : order.price;
                        if(!req.user.refund_balance) req.user.balance = 0;
                        else req.user.refund_balance = 0;
                        payment.price = order.price;
                        order.save(function() {
                          req.user.save(function() {
                            payment.save(function() {
                              if(req.body.method == 'paypal') return res.status(200).jsonp({continue: "paypal"});// paypal_payment(req, order.payment, order.price);


                              return res.status(200).jsonp({continue: "cc", price: order.price, id: order.payment});
                            });
                          });
                        });


                      }
                    }
        */
        // else {
        if (req.body.method == 'paypal') return res.status(200).jsonp({
          continue: "paypal"
        }) //return paypal_payment(req, order.payment, order.price);
        return res.status(200).jsonp({
          continue: "cc",
          price: order.price,
          id: order.payment
        })

        //}







      });
    }

  ]);



}
exports.edit_status = function (req, res) {
  if (req.body.status.label == 'Complete') {


    Order.update({
      _id: req.body._id
    }, {
      $set: {
        completed: true,
        verified: true,
        refund: false,
        refunded: false
      }
    }, function (err, resp) {
      res.send('done');
    })
  } else if (req.body.status.label == 'Inprogress') {

    Order.update({
      _id: req.body._id
    }, {
      $set: {
        completed: false,
        verified: false,
        cancelled: false,
        paid: true,
        checkout: true,
        manual_verify: false,
        topup: false,
        refund: false,
        refunded: false
      }
    }, function (err, resp) {

      API(req.body._id, 'order', function (key) {
        res.send('done');

      });
    });
  } else if (req.body.status.label == 'Cancelled') {

    Order.update({
      _id: req.body._id
    }, {
      $set: {
        completed: false,
        verified: false,
        cancelled: true
      }
    }, function (err, resp) {
      res.send('done');
    })
  } else if (req.body.status.label == 'Abandoned') {

    Order.update({
      _id: req.body._id
    }, {
      $set: {
        completed: false,
        verified: false,
        cancelled: false,
        paid: false,
        checkout: false
      }
    }, function (err, resp) {
      res.send('done');
    })
  }

}
exports.edit_count = function (req, res) {

  if (req.body.beforeCount2) req.body.beforeCount = req.body.beforeCount2;
  if (req.body.afterCount2) req.body.afterCount = req.body.afterCount2; // if there was no count initially.

  if (req.body.beforeCount) Order.update({
    _id: req.body._id
  }, {
    $set: {
      beforeCount: parseInt(req.body.beforeCount)
    }
  }, function (err, data) {
    res.send('done'); // let client side know that we are done.
  })
  else Order.update({
    _id: req.body._id
  }, {
    $set: {
      afterCount: parseInt(req.body.afterCount)
    }
  }, function (err, data) {
    res.send('done');
  })

}
exports.order = function (req, res, next, id) {
  if (!id) return next("no such id");
  Order.load(id, function (err, order) {

    if (err) return next(err);
    if (!order) {
      var orderContainer = [];
      Payment.find({
        parent_id: id
      }, function (err, payments) {
        if (err) return next(err);
        if (!payments) return next(err);
        async.eachSeries(payments, function (item, cb) {
          Order.load(item.order, function (err, order) {
            if (req.user && req.user.hasRole('admin')) {
              order.populate(['user', 'provider', 'comments.user'], function (err, order) {
                orderContainer.push(order);
                return cb();

              });
            } else {
              orderContainer.push(order);
              return cb();

            }
          });
        }, function () {
          if (req.order) {
            req.order = orderContainer[0];
            req.order.show_cart = orderContainer;
            req.order.full_cart = true;
          }
          next();
        });
      });
      // return next(new Error('Failed to load order ' + id));
    } else if (req.user && req.user.hasRole('admin')) {
      order.populate(['user', 'provider', 'comments.user'], function (err, order) {
        req.order = order;
        next();
      });
    } else {
      req.order = order;
      next();
    }
  });
};

exports.comment = function (req, res) {
  var order = req.order;
  var newComment = {
    text: req.body.text,
    date: new Date(),
    user: req.user
  };

  order.comments.push(newComment);

  if (req.user._id.toString() === order.user._id.toString()) {
    order.unread.provider++;
    order.unread.administrator++;
  } else if (order.provider && req.user._id.toString() === order.provider._id.toString()) {
    order.unread.customer++;
    order.unread.administrator++;
  } else if (req.user.hasRole('admin')) {
    order.unread.customer++;
    order.unread.provider++;
  }

  order.save(function (err, saved) {
    if (err) {
      res.status(500).jsonp(err);
    }

    email.commentPosted(order, newComment, function () {
      res.status(200).jsonp(saved);
    });
  });
};

/**
 * Create an order
 */
exports.admin_create = function (req, res) {


  var order = new Order(req.body);
  order.checkout = true;
  order.paid = true;
  order.save(function (err, order) {
    API(order._id, 'order');
    User.update({
      _id: req.body.user
    }, {
      $set: {
        balance: req.body.balance
      }
    }, function (err, data) {

      jobs.create('count', {
        title: order.url,
        order: order._id,
        type: 'before'
      }).priority('critical').save();
      res.jsonp(order);
    })
  })

}
exports.create = function (req, res, next) {
  var fix_instagram = ["ig_followers", "ig_followers2", "ig_euandusa", "ig_hq_follows", "ig_usfollows"];
  if (req.body.panel_cart && !req.body.url.match('http://|https://') && !req.body.url.match(re_weburl) && req.body.service.value != 'snap_followers' && req.body.service.value != 'periscope_followers' && req.body.service.value != 'ig_followers' && req.body.service.value != 'ig_followers2' && req.body.service.value != 'ig_euandusa' && req.body.url != 'add_cart' && req.body.service.value != 'sc_followers' && req.body.service.value != "musically_followers") {
    req.body.url = 'http://' + req.body.url;
  }
  if (req.body.service.label == 'Free followers') {

    var order = new Order(req.body);

    order.save(function (err, order) {});

  }
  if (req.body.user) {
    return module.exports.admin_create(req, res);
  } // let the admin create new order.




  if (req.user) {
    var balance = req.user.balance;
  }


  //if (req.user && req.user.not_activated) return res.status(500).end("You need to activate your account first!");
  if (req.body.service.value == 'tw_followers') {
    var tmp = req.body.url.split(".com/")[1];
    req.body.url = "https://twitter.com/" + tmp;
  }
  if (fix_instagram.indexOf(req.body.service.value) !== -1) {
    if (req.body.url.match("@")) {
      var tmp = req.body.url.split("@")[1];
      req.body.url = "https://www.instagram.com/" + tmp;

    }
    if (req.body.url.match(".com")) {
      var tmp = req.body.url.split(".com/")[1];
      req.body.url = "https://www.instagram.com/" + tmp;

    } else req.body.url = "https://www.instagram.com/" + req.body.url;

  }


  if (!req.body.service) {

    return res.status(400).send('Something is wrong. Please try again later.');
  }


  if (req.body.service.pattern) {

    var pattern = new RegExp(req.body.service.pattern, 'i');

    if (!pattern.test(req.body.url) && req.body.service.value != 'ig_followers') {
      logging.info('URL not supported - ' + req.body.url + ' service - ' + req.body.service.value);
      return res.status(400).send('The URL you entered is not supported with the chosen service.');
    }
  }

  var minimum = req.body.service.minimum || 1;
  if (balance) balance = balance.toFixed(2);
  if (req.body.quantity * req.body.service.multiplier < minimum && !req.body.cart) {
    logging.info('Minimum quantity not met - ' + minimum + ' qty. Service - ' + req.body.service.value);
    res.status(400).send('Minimum quantity is ' + minimum);
  } else if (req.body.price <= 0) {
    logging.info('Order price less or equal to 0, possible hacking attempt. Service - ' + req.body.service.value);
    return res.status(400).send('Cannot process empty orders. Please make sure you\'re not trying to place an empty or negative order.');
  } else if (!req.body.url) {
    logging.info('No url provided for service - ' + req.body.service.value);
    return res.status(400).send('Please enter a URL.');
  } else if (!req.body.url.match(re_weburl) && req.body.service.value != 'snap_followers' && req.body.service.value != 'periscope_followers' && req.body.service.value != 'ig_followers2' && req.body.service.value != 'ig_followers' && req.body.service.value != 'ig_euandusa' && req.body.service.value != "musically_followers" && req.body.url != 'add_cart' && req.body.service.value != 'sc_followers') {
    logging.info('URL not supported - ' + req.body.url + ' service - ' + req.body.service.value);
    return res.status(400).send('The URL doesn\'t look valid.');
  } else {


    var order = new Order(req.body);

    // tie the order to the current logged in user
    if (!req.user && !req.body.payment) {
      return res.status(400).send('Either a user or a payment need to be referenced to create an order.');
    }







    async.waterfall([
      function (callback) {


        if (req.body.url == "add_cart") return callback(null);


        Blacklist.find({}, function (err, data) {
          async.eachSeries(data, function (black, caller) {
            if (req.body.url.match(black.string)) return res.status(500).end("Address is blacklisted!");
            return caller();
          }, function done() {
            return callback(null);
          });
        });



      },
      function (callback) {
        if (req.body.payment) {

          Payment.findOne({
            _id: req.body.payment
          }).populate('user', 'name username').exec(function (err, payment) {
            if (!payment) {
              return res.status(403).send('Cannot find payment attached to order.');
            } else if (!_.isEmpty(payment.order)) {
              return res.status(403).send('The referenced payment is attached to another order.');
            }

            callback(err, payment);
          });
        } else {
          callback(null, null);
        }
      },
      function (payment, callback) {
        if (payment) {
          order.user = payment.user;
        } else {
          order.user = req.user;
          order.checkout = true;

        }


        if (order.service.value.match(/^ig_followers/i) && 1 == 2) {
          var instagram = require('./../apis/instagram.js');
          instagram.isPublicUser({
            order: order
          }, function (err, isPublic) {
            if (!isPublic) {
              return res.status(400).send('This instagram profile seems to be private. Please make sure the instagram profile is public and we can send traffic to it before placing the order');
            } else {
              return callback(null, payment);
            }
          });
        } else if (order.service.value.match(/^ig_/i) && 1 == 2) {
          var instagram = require('./../apis/instagram.js');
          instagram.isPublicPost({
            order: order
          }, function (err, isPublic) {
            if (!isPublic) {
              return res.status(400).send('This instagram post seems to be private. Please make sure the instagram post is public and we can send traffic to it before placing the order');
            } else {
              return callback(null, payment);
            }
          });
        } else {
          return callback(null, payment);
        }
      },
      function (payment, callback) {
        if (!req.body.discount) {
          return callback(null, payment, false);
        }

        Discount.findOne({
          code: new RegExp(req.body.discount, 'i')
        }, function (err, discount) {
          if (discount) {
            order.discount = {
              price: discount.price,
              percent: discount.percent,
              code: discount.code,
              services: discount.services
            }
          }
          callback(null, payment, discount);
        });
      },
      function (payment, discount, callback) {


        // verify that price is correct and no hacking attempts
        if (!order.service.value) return callback(null, payment);

        Verify(order, payment, discount, req, res, function (err, pay, verified_price, qty) {
          order.quantity = qty;
          order.orderCount = qty;
          order.order_tmp = req.body.quantity;

          return callback(null, payment);
        })



      },
      function (payment, callback) {
        if (payment) {

          // if we have an attached payment to this order, we reference it
          order.payment = payment;
          order.paid = false;

          callback(null, payment);
        } else {




          async.series([
            function (callback) {
              if (req.user) {
                req.user.save(function (err, user) {



                  if (!order.topup) order.paid = true;
                  callback(null, null);
                });
              } else callback(null, null);
            }
          ], function (err, result) {
            callback(err, null);
          })



        }
      },
      function (payment, callback) {
        // let's find all the providers that can take care of this order
        User.find({
            roles: {
              '$in': ['provider']
            },
            services: {
              '$in': [order.service.value]
            }
          })
          .exec(function (err, users) {
            callback(err, users, payment);
          });
      },
      function (users, payment, callback) {

        // if only one provider exists - we give him the order automatically
        if (users.length === 1) {
          order.provider = users[0];
        }

        callback(null, payment);
      },
      function (payment, callback) {

        order.save(function (err, order) {

          return callback(err, order, payment);

        });
      },
      function (order, payment, callback) {
        if (payment) {
          payment.order = order;
          payment.save(function (err) {
            callback(err, payment);
          });
        } else {
          callback(null);
        }
      }
    ], function (err, payment) {
      if (err) return next(err);
      // we'll try to scrape the 'before' count

      if (!payment || !payment.fraud) {
        jobs.create('count', {
          title: order.url,
          order: order._id,
          type: 'before'
        }).priority('critical').save();
      }
      if (!req.body.cart) res.jsonp(order);
    });
  }
};

/**
 * Update an order
 */
exports.update = function (req, res) {


  if (!req.user.hasRole('admin')) {
    return res.status(403).send('Only administrators can edit orders');
  }

  var order = req.order;

  order = _.extend(order, req.body);

  order.save(function (err) {
    if (err) {
      return res.send('users/signup', {
        errors: err.errors,
        order: order
      });
    } else {
      User.update({
        _id: req.body.user._id
      }, {
        $set: {
          balance: req.body.user.balance
        }
      }, function (err, data) {
        jobs.create('count', {
          title: order.url,
          order: order._id,
          type: 'before'
        }).priority('critical').save();
        res.jsonp(order);
      })



    }
  });
};

/**
 * Delete an order
 */
exports.destroy = function (req, res) {
  var order = req.order;

  order.remove(function (err) {
    if (err) {
      return res.send('users/signup', {
        errors: err.errors,
        order: order
      });
    } else {
      res.jsonp(order);
    }
  });
};

/**
 * Show an order
 */
exports.show = function (req, res) {
  async.series([
    function (callback) {
      if (req && req.user && req.user._id) {
        if (req.user && req.user._id.toString() === req.order.user._id.toString()) {
          req.order.unread.customer = 0;
        } else if (req.order.provider && req.user._id.toString() === req.order.provider._id.toString()) {
          req.order.unread.provider = 0;
        } else if (req.user && req.user.hasRole('admin')) {
          req.order.unread.administrator = 0;
        }
      }
      callback();
    },
    function (callback) {
      req.order.save(callback);
    }
  ], function (err) {
    if (req.order.full_cart) res.jsonp({
      cart: req.order.show_cart
    });
    else res.jsonp(req.order);
  });
};

exports.cancel = function (req, res) {
  if (!req.user.hasRole('provider') && !req.user.hasRole('admin')) {
    return res.status(403).send('Only providers and administrators can cancel orders');
  } else if (!req.order.provider && !req.user.hasRole('admin')) {
    return res.status(403).send('You can only cancel an order once it has been claimed by you');
  }

  if (req.order.provider) {
    if (req.user._id.toString() != req.order.provider._id.toString() && !req.user.hasRole('admin')) {
      return res.status(403).send('You can only cancel your own orders');
    }
  }

  req.order.reason = req.query.reason;
  req.order.cancelled = true;
  req.order.save(function (err) {
    if (err) {
      return res.status(500).send('Error while cancelling order');
    }


    res.jsonp(req.order);
  });
}

exports.complete = function (req, res) {
  if (!req.user.hasRole('provider') && !req.user.hasRole('admin')) {
    return res.status(403).send('Only providers and administrators can complete orders');
  } else if (req.order.completed) {
    return res.status(403).send('Order is already completed');
  }

  req.order.complete(req, res, function (err, order) {
    if (err) {
      return res.status(500).send('Error while completing order');
    }

    res.jsonp(order);
  });
}

exports.approve = function (req, res) {
  if (!req.user.hasRole('admin')) {
    return res.status(403).send('Only administrators can approve orders as verified');
  } else if (req.order.verified) {
    return res.status(403).send('Order is already verified');
  }

  req.order.approve(req, res, function (err, order) {
    if (err) {
      return res.status(500).send('Error while approving order');
    }

    res.jsonp(order);
  });
}

exports.unapprove = function (req, res) {
  if (!req.user.hasRole('admin')) {
    return res.status(403).send('Only administrators can unapprove orders as verified');
  } else if (!req.order.verified && !req.order.completed) {
    return res.status(403).send('Order is already marked as not completed and not verified');
  }

  req.order.unapprove(req, res, function (err, order) {
    if (err) {
      return res.status(500).send('Error while unapproving order');
    }

    res.jsonp(order);
  });
}

exports.claim = function (req, res) {
  if (!req.user.hasRole('provider')) {
    return res.status(403).send('Only providers can claim orders');
  }

  async.waterfall([
    function (callback) {
      callback(null, req.order);
    },
    function (order, callback) {
      if (order.provider) {
        return res.status(403).send('This order has already been claimed');
      }

      order.provider = req.user;

      order.save(function (err, order) {
        callback(err, order);
      });
    },
    function (order, callback) {
      Order.populate(order, {
        path: "provider"
      }, function (err, _order) {
        callback(err, _order);
      })
    }
  ], function (err, order) {
    if (err) {
      return res.status(500).send('Error while claiming order');
    }

    res.jsonp(order);
  })


};
exports.testing = function (req, res) {

  Order.count({}, function (e, resp) {
    return res.status(200).jsonp({
      data: resp
    });
  });

};

exports.all = function (req, res) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  var filter = {};
  var queryParams;
  var session = getQueryParams(req);
  if (session.invalid_session) return res.status(200).jsonp({
    session: true
  });
  if (typeof req.query.page === 'undefined') {
    req.query.page = 1;
  }

  if (req.query.filter && req.query.filter.url) {
    filter.url = new RegExp(req.query.filter.url, 'i');
  }

  if (req.query.filter && req.query.filter.service) {
    filter['service.value'] = req.query.filter.service;
  }
  if (req.query.filter && req.query.filter.api_id) {
    filter['api_id'] = new RegExp(req.query.filter.api_id, 'i');
  }
  if (req.query.filter && req.query.filter.status) {

    req.query.filter.status = JSON.parse(req.query.filter.status);
    if (req.query.filter.status.hasOwnProperty('paid')) filter['paid'] = req.query.filter.status.paid;
    if (req.query.filter.status.hasOwnProperty('checkout')) filter['checkout'] = req.query.filter.status.checkout;
    if (req.query.filter.status.hasOwnProperty('cancelled')) filter['cancelled'] = req.query.filter.status.cancelled;
    if (req.query.filter.status.hasOwnProperty('completed')) filter['completed'] = req.query.filter.status.completed;
    if (req.query.filter.status.hasOwnProperty('refund')) filter['refund'] = req.query.filter.status.refund;
    if (req.query.filter.status.hasOwnProperty('refunded')) filter['refunded'] = req.query.filter.status.refunded;


  }
  queryParams = _.extend({}, getQueryParams(req), filter);
  queryParams.count

  async.waterfall([
    function (callback) {
      if (req.query.filter && req.query.filter.username && req.user && req.user.hasRole('admin')) {
        User.find({
          username: new RegExp(req.query.filter.username, 'i')
        }).exec(function (err, users) {

          var userIds = [];
          _(users).each(function (user) {
            userIds.push(user._id);
          });

          queryParams = _.extend(queryParams, {
            'user': {
              '$in': userIds
            }
          });

          callback(null);
        });
      } else {
        callback(null);
      }
    },
    function (callback) {
      Order.count(queryParams, callback);
    },
    function (count, callback) {
      console.time('rookie');
      var query = Order.find(queryParams);
      if (req.user && req.user.hasRole('admin')) {
        query.sort({
          'unread.administrator': 'desc'
        });
      } else if (req.user && req.user.hasRole('provider')) {
        query.sort({
          'unread.administrator': 'desc'
        });
      } else {
        query.sort({
          'unread.customer': 'desc'
        });
      }
      query //.sort(req.query.sorting)
        .skip(req.query.count * (req.query.page - 1))
        .limit(parseInt(req.query.count))
        .populate('user', 'name username')
        .populate('provider', 'name username profit')
        .lean()
        .exec(function (err, orders) {
          if (err) {
             console.log(err);
            res.render('error', {
              status: 500
            });
          } else {
            console.timeEnd('rookie');
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: orders
            });
          }
        });
    }
  ]);
};

exports.check = function (req, res) {
  var lastQuarter = new Date();
  lastQuarter.setDate(lastQuarter.getDate() -120);
  Order.find({ "created": {$gte: lastQuarter}}).exec(function (err, orders) {    
    orders = orders.filter(v => {      
      // return (v.payment_type != '2checkout') && (v.verify_id == '') && (v.checkout == true) && (v.paid == true);
      // order.verified && !order.completed && !order.topup && !order.refund
      return (v.payment_type != '2checkout') && (v.verify_id == '') && (v.verified == true) && (v.completed == false) && (v.topup == false) && (v.refund == false);
    })
    util.checkPaypal(orders);
    res.status(200).send();    
  })  
};