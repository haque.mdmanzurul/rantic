var request = require('request'),
    mongoose = require('mongoose'),
    Order = mongoose.model('Order'),
    Free = mongoose.model('Free'),
    schedule = require('node-schedule'),
    User = mongoose.model('User'),
    config = require('../config/config'),
    fs = require('fs');
var API = require('./api.js').API;
var users = require('./users');
var twit = require('twit');
exports.twitter = function(req, res) {

// captcha fixing
 request.post({headers: {'content-type' : 'application/x-www-form-urlencoded'},url: 'https://www.google.com/recaptcha/api/siteverify',body: "secret=6Ley3wkTAAAAAPo1lNZyv-ZQ2qc4X8joQn2ABgIo&response="+req.body.captcha_response}, function(err, data, body){
    try {
        var parser = JSON.parse(body);
        if(parser.success) continueScript();
        else {
            res.status(500).send('Invalid captcha!');
            return false;
        }
} catch(e) {
    res.status(500).send('something went wrong');
    return false;
}
  
   })
function continueScript() {
    if(req.body.twitter_user) req.body.twitter_user = req.body.twitter_user.toLowerCase(); 
    if(req.body.email) req.body.email = req.body.email.toLowerCase();
     req.assert('email', 'You must enter a valid email address').isEmail();
      var errors = req.validationErrors();
    if (errors) {
        return res.status(500).send('Invalid email!');
        return false;
    }
  
        Free.findOne({
            $or: [ { twitter_user: req.body.twitter_user}, 
            { email: req.body.email}]
    
        }, function(err, result) {
            if(result)  {
                res.status(500).send('Twitter user / email already registered!');
                return false;
            }
            else {
               
                request.get("https://twitter.com/" + req.body.twitter_user, function(err, resp, data) {
                    
                    if(!data.match("Sorry, that page")) {
                        continueOrder(req.body.twitter_user);
                    } else {
                        res.status(500).send('Such twitter account does not exist!');
                        return false;
                    }
                });
               
                 function continueOrder(twitter_id) {
                    var freeOrder = new Free(req.body);
                        freeOrder.save(function(err) {
                            User.findOne({
                                email: req.body.email
                            }, function(err, data) {
                                if(data)  {
                                    createOrder(data._id, req.body.twitter_user);
                                    res.status(200).send('Login to your account to see your order ' + req.body.email);
                                }   else  {
                                        createUser(req.body.twitter_user, req.body.email, function(user_id) {
                                            res.status(200).send('Check your email to see your order.');
                                            createOrder(user_id, req.body.twitter_user);
                                        });
                                    }
                            });
            
           
                        });
               
                }

            }
        });
}


function createOrder(user_id, url) {
    var newOrder = new Order({
        user: user_id,
        paid: true,
        url: 'http://twitter.com/'+ url,
        quantity: 100,
        orderCount: 100,
        checkout: true,
        'service.value': 'tw_usa',
        'service.label': 'Free twitter order'

    });
    newOrder.save(function(err, data) {
       
        API(data._id,'order');
    });
}

function createUser(username,email, callback) {
     require('crypto').randomBytes(8, function(ex, buf) {
                                var password = buf.toString('hex');

                                // then create the user object
                                user = new User({
                                    username: username,
                                    email: email,
                                    name: 'Rantic Customer',
                                    roles: ['authenticated', 'customer'],
                                    password: password
                                });

                                // save it..
                                user.save(function(err, userr){
                                       user.sendAutoCreateEmail(function(){
                                          callback(user._id);
                                        })
                                  

});
});
} 


}


exports.order = function(req, res) {

    request.post({headers: {'content-type' : 'application/x-www-form-urlencoded'},url: 'https://www.google.com/recaptcha/api/siteverify',body: "secret=6Ley3wkTAAAAAPo1lNZyv-ZQ2qc4X8joQn2ABgIo&response="+req.body.captcha_response}, function(err, data, body){
    try {
        var parser = JSON.parse(body);
        if(parser.success) continueScript();
        else {
            res.status(500).send('Invalid captcha!');
            return false;
        }
} catch(e) {
    res.status(500).send('Captcha failure');
    return false;
}
  
   })

    
    function continueScript() {


          if(req.body.instagram_user) req.body.instagram_user = req.body.instagram_user.toLowerCase(); 
          if(req.body.email) req.body.email = req.body.email.toLowerCase();

           req.assert('email', 'You must enter a valid email address').isEmail();
      var errors = req.validationErrors();
    if (errors) {
        return res.status(500).send('Invalid email!');
        return false;
    }
    
        Free.findOne({
            $or: [ { instagram_user: req.body.instagram_user}, 
            { email: req.body.email}]
    
        }, function(err, result) {
            if(result)  {
                res.status(500).send('Instagram user / email already registered!');
                return false;
            }
            else {
                request('https://instagram.com/' + req.body.instagram_user, function(err, data, body) {
                    console.log(err);
                  
                if(err) return res.status(500).send("Instagram error");
                if(body && body.split('"owner": {"id": "')[1]) {
                            var userid = body.split('"owner": {"id": "')[1].split('"')[0];
                            return continueOrder(userid);
                        } else {
                             res.status(500).send('Unable to get instagram id');
                             return false;
                            
                        }
                 });

                
             

            }
        });

    }

function continueOrder(userid) {

    Free.findOne({
        ig_user: userid
    }, function(err,user) {
        
        if(user) {
            res.status(500).send('Instagram user / email already registered!');
                return false;
       } else {

        req.body.ig_user = userid;
        var freeOrder = new Free(req.body);
        freeOrder.save(function(err) {
           
            User.findOne({
                email: req.body.email
            }, function(err, data) {
                if(data)  {
                    createOrder(data._id, req.body.instagram_user);
                     res.status(200).send('Login to your account to see your order ' + req.body.email);
                }
                else  {
                    createUser(req.body.instagram_user, req.body.email, function(user_id) {
                         res.status(200).send('Check your email to see your order.');
                        createOrder(user_id, req.body.instagram_user);
                    })
                }
            })
            
          
        });




       }
    })



     
    }



function createOrder(user_id, url) {
    var newOrder = new Order({
        user: user_id,
        paid: true,
        url: 'http://instagram.com/'+ url,
        quantity: 100,
        orderCount: 100,
        checkout: true,
        'service.value': 'ig_followers',
        'service.label': 'Free ig order'

    });
    newOrder.save(function(err, data) {
       
        API(data._id,'order');
    });
}

function createUser(username,email, callback) {
     require('crypto').randomBytes(8, function(ex, buf) {
                                var password = buf.toString('hex');

                                // then create the user object
                                user = new User({
                                    username: username,
                                    instagram_user: username,
                                    email: email,
                                    name: 'Rantic Customer',
                                    roles: ['authenticated', 'customer'],
                                    password: password
                                });

                                // save it..
                                user.save(function(err, userr){
                                       user.sendAutoCreateEmail(function(){
                                          callback(user._id);
                                        })
                                  

});
});
} 


  
       
    }
 
   







