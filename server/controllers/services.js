'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Service = mongoose.model('Service');
var ApiData = mongoose.model("Apidata");
var Discount = mongoose.model('Servicediscount');
var async = require('async');
var _ = require('lodash');

exports.removeDiscount = function(req, res) {
        console.log(req.body)
        console.log(req.params);
        console.log(req.query)
        Discount.remove({service: req.query.service}, function(err, resp) {
                return res.status(200).end();
        });
}
exports.createDropdown = function(req, res) {

  Service.update({value: req.body.service},{ $set: {
    dropdown: req.body.data
  }}, function(err) {

    console.log(err);
    return res.status(200).end('');

  });

}


exports.editDiscount = function(req, res) {

   var data = JSON.parse(req.body.data);
   var serviceList = JSON.parse(req.body.service);
   var tmp = [];
   for(var i in serviceList) {
    if(tmp.indexOf(serviceList[i]) == -1) tmp.push(serviceList[i]);
   }
   serviceList = tmp;

    var service = { service: [], price: [], percentage: []}
    for(var i in data) {
      if(data[i].price.length) {
        service.service.push(data[i].service);
        service.price.push(data[i].price);
        service.percentage.push(data[i].percentage);
      }


  }

  Discount.update({_id: req.body.id},{$set: {
    service: serviceList,
    price: service.price,
    percentage: service.percentage
  }}, function(err) {
    var removeList = [];
    async.eachSeries(serviceList, function(item, cb) {


      Service.update({value: item}, { $set: {
        discount_settings: JSON.stringify({price: service.price, percentage: service.percentage }),
        discount: true,
        discount_id: req.body.id

      }}, function() {
        return cb();
      });
    }, function done() {
      Service.find({discount_id: req.body.id}, function(err, list) {
        async.eachSeries(list, function(item, cb) {
          if(serviceList.indexOf(item.value) === -1) {
            Service.update({value: item.value}, { $set: {
              discount_settings: [],
              discount_id: ""
            }}, function(err) {
              return cb();
            });
          } else return cb();
        }, function done() {

            if(err) return res.status(500).jsonp({data: err});
            return res.status(200).end("");

        })
      })



    });

  });


}
exports.fetchDiscount = function(req, res) {
  Discount.findOne({_id: req.body.serviceId}, function(err, result) {
    if(err) return res.status(500).jsonp({data: err});
    return res.status(200).jsonp({data: result});
  });
}
exports.createDiscount = function(req, res) {
  var data = JSON.parse(req.body.data);
  var service = { price: [], percentage: []};
   var serviceList = JSON.parse(req.body.service);
console.log(data);
console.log(serviceList);
  for(var i in data) {
    if(data[i].price) {
      service.price.push(data[i].price);
      service.percentage.push(data[i].percentage);
    }



  }



  var submit = new Discount({
    service: serviceList,
    price: service.price,
    percentage: service.percentage
  }).save(function(err, data) {

     async.eachSeries(serviceList, function(item, cb) {

      Service.update({value: item}, { $set: {
        discount_settings: JSON.stringify({price: service.price, percentage: service.percentage }),
        discount: true,
        discount_id: data._id

      }}, function() {
        return cb();
      });
    }, function done() {

      if(err) return res.status(500).jsonp({data: err});
      return res.status(200).end("");

    });


  });
}

exports.discount = function(req, res) {

 async.waterfall([
    function(callback) {
      Discount.count({}, callback);
    },
    function(count, callback) {
      Discount.find({})
        .sort(req.query.sorting || {'label': 'asc'})
        .skip(req.query.count * (req.query.page - 1))
        .limit(parseInt(req.query.count))
        .exec(function(err, services) {
          if (err) {
            res.render('error', {
              status: 500
            });
          } else {
            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: services
            });
          }
        });
  }]);

}

exports.getApiData = function(req, res) {
   ApiData.find({}, function(err, data) {

      return res.status(200).jsonp({data: data});

   });
}
exports.service = function(req, res, next, id) {
        if(id == "removeDiscount") return next();
     Service.findOne({_id: id}, function(err, service) {
    if (err) return next(err);
    if (!service) return next(new Error('Failed to load service ' + id));
    if (req.user.hasRole('admin')) {
      service.populate(['user', 'comments.user'], function(err, service) {
        req.service
        req.service = service;
        next();
      });
    } else {
      req.service = service;
      next();
    }
  });





};

exports.show = function(req, res) {
  res.jsonp(req.service);
};

exports.all = function(req, res) {
  var maxAge = 1000*60*60;
	res.setHeader("Cache-Control", 'no-cache')
  // res.setHeader('Cache-Control', 'max-age=' + maxAge);
  var filter = {};
  var queryParams;

  if (typeof req.query.page === 'undefined'){
    req.query.page = 1;
  }

  if(req.query.filter && req.query.filter.label){
    filter.label = new RegExp(req.query.filter.label, 'i');
  }

  if(req.query.filter && req.query.filter.value){
    filter.value = new RegExp(req.query.filter.value, 'i');
  }

  queryParams = _.extend({}, filter);

  async.waterfall([
    function(callback) {
      Service.count(queryParams, callback);
    },
    function(count, callback) {
      Service.find(queryParams)
	.sort({ 'priority': 'desc' })
	//.sort({ 'label': 'desc' })
	//.sort({'priority': 'desc', 'label': 'asc'})
        //.sort(req.query.sorting || {'label': 'asc'})
        .skip(req.query.count * (req.query.page - 1))
        .limit(parseInt(req.query.count))
        .exec(function(err, services) {
          if (err) {
            res.render('error', {
              status: 500
            });
          } else {



            res.jsonp({
              total: count,
              count: (req.query.count ? parseInt(req.query.count) : 0),
              page: (req.query.page ? parseInt(req.query.page) : 1),
              sorting: req.query.sorting,
              data: services
            });
          }
        });
    }
  ]);
};

exports.update = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can edit services');
    }

    var service = req.service;
    service = _.extend(service, req.body);


        service.save(function(err, service) {
        if (err) {
            return res.status(500).jsonp(err);
        } else {
            res.jsonp(service);
        }
    });








};

exports.create = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can create services');
    }
    Service.count({}, function(err, count) {
      // start from 700, for rantic api
      console.log("Total amount of services: " + count);
      req.body.api_nr = 700 + count;
      var service = new Service({});
      service = _.extend(service, req.body);

      service.save(function(err, service) {
        if (err) {
            return res.status(500).jsonp(err);
        } else {
            res.jsonp(service);
        }
      });


    });

};

exports.destroy = function(req, res) {
    if(!req.user.hasRole('admin')){
        return res.status(403).send('Only administrators can delete services');
    }

    var service = req.service;

    service.remove(function(err) {
        if (err) {
            return res.status(500).jsonp(err);
        } else {
            res.jsonp(service);
        }
    });
};
