'use strict';
var request = require('request');
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var Services = mongoose.model('Service');
var fs = require('fs-extra');
var helper = require('./api-helper.js');
var multiple = require("./multiple_api.js");
var API_store = require('./helper/api_orders');
var API = mongoose.model("API");
var ApiData = mongoose.model("Apidata");
			var async = require('async');

// pm2 start server --node-args="--expose-gc"


	var logger = function(service, order_id, error, cb) {
		var message = new Date() + ' -  ' + service + ' - Order Id:  ' + order_id + ' - Error msg: ' + error;
		fs.appendFile('/opt/swenzy/logs/' + service, message, function() {
			cb();
		})
	}



	var parse_url = function(url) {
	var url = url.match('//') ? url.split('//')[0].toLowerCase()+'//'+url.split('//')[1] : url;
		return url;
}

exports.API = function(key, method, callback, fromAPI, roundService, qty) {
	if(!method) var query = {payment: key};
	else var query = {_id: key};

	var selectDB = "Order";

		mongoose.model(selectDB).findOne(query, function(err, data) {

			if(err) return callback(null);

		if(data) {


			if(data.manual_verify) return callback(null);
			if(data.topup) return callback(null);
			if(data.cancelled) return callback(null);
			if(data.countries) var countries = data.countries;
			if(!data.countries) var countries = false;
			if(data.completed) return callback(null);
			if(data.refund) return callback(null);
			if(data.refunded) return callback(null);

			data.quantity = data.recurring ? data.orderCount : data.quantity;
			data.url = parse_url(data.url);

			getService(data, data.service.value, function(err, service, type, fullService) {
				if(err) {
					if(callback) return callback(null);
					else return;
				}
				else {
					if(roundService && qty){


						 data.quantity = qty;

				 }
				 var drop = {}
					if(!fullService.dropdown) {
						fullService.dropdown = {};
						drop = {};
					}
					else {
						try {
							drop = JSON.parse(fullService.dropdown);
						} catch(e) {
						 drop = {};
						  }
					}
						async.eachSeries(drop, function(item, cbs) {
							if(!item || ! item.api_code || !item.api_url) return cbs();
							if(item.api_code == data.emoticon) {

								type = item.api_url;
								service = item.api_code;

							}
							return cbs();
						}, function done() {
				if(data.service.value == "yt_subselite") {
					if(data.quantity < fullService.qty_below) {
						service = fullService.qty_below_api;
					//	type = "famearmy";
					}
				}




				if(data.service.value == "ig_hq_follows") {
					if(data.quantity <= 15000) 	service = 409;
					else if(data.quantity > 15000 && data.quantity <= 50000) 	service = 411;

					else if(data.quantity > 50000) 	service = 410;


				}
				if(roundService) service = roundService;

				if(err) {
					if(callback) return callback(null);
				}

				else if (type == 'realfans') {

				  getKey2(fromAPI, data.speed_service,key, data.url, service, data.quantity, method, countries, false,function(api_id, err) {
     				if(callback) return callback(api_id, err);
					return;
                  });
				}
				else if(type == 'smmlite') {
					getSmmlite(fromAPI, key, data.url, service, data.quantity, method, false, function(api_id, err) {
					  if(callback) return callback(api_id, err);
				})
				}
				else if(type == 'likesmania') {

                                        getLikesMania(fromAPI, key, data.url, service, data.quantity, method, false, function(api_id, err) {
                                          if(callback) return callback(api_id, err);
                                        })

                                }

				else if(type == 'famearmy') {

					getLikesMania(fromAPI, key, data.url, service, data.quantity, method, false, function(api_id, err) {
					  if(callback) return callback(api_id, err);
					})

				}
				else if(type == 'autosmo') {

					getKey(fromAPI, key, data.url, service, data.quantity, method, false, function(api_id, err) {
					  if(callback) return callback(api_id, err);
					return;
				})

				} else return callback(null, null);
			});

		}

						});






		}


		});




function getService(data, service, callback) {

	/* check if we need to register with multiple apis */
	if(!roundService) {
	multiple.check(data, function(progress) {
		if(progress) return callback(true);
		else {
			Services.findOne({value: service}, function(err, result) {
				if(err) return callback(err);
				if(!result) return callback('no service');
				return callback(null, result.api_code, result.api_type, result);
			})
		}
	});
} else {
	Services.findOne({value: service}, function(err, result) {
		if(err) return callback(err);
		if(!result) return callback('no service');
		return callback(null, result.api_code, result.api_type, result);
	})

}

}
function getEat(fromAPI,id, link, type, quantity, method, callback) {
	if(method) var query = {_id: id};
	else var query = {payment: id};


	var selectDB = "Order";


	mongoose.model(selectDB).findOne(query, function(err, data) {

		if(data && data.api_id && data.api_id != 0) return callback(data.api_id);
		else {
			if(data && data.paid === true && !data.verified && !data.old && !data.api_id && data.api_id != 0 && !data.completed) {


				sendRequest__(link,quantity, data, function(key) {
					return callback(key);
				});


			} else return callback(false);
		}
	});


	function sendRequest__(link,quantity, data, callback) {

		if(link.match("watch\\?v=")) link = link.split("watch?v=")[1];
		if(data.service.value === 'yt_usa_new') var type = 'US';
		else var type = 'WW';
		var url = 'http://panel.eatatcheap.com/api?key=1717d0627bef9167b1a603d5acba79bbe851557f&vid='+link+'&qt='+quantity+'&typ='+type+'&speed='+data.eat_speed;


		request.get(url, function(err, response, body) {

		logger('eatcheap', id, body + ' -  ' + ' - ' + err, function() {		})
		try {



			if(body && body.match("200")) {

				Order.update(query, { $set: { api_id: "InProgress"}}, function(err) {
					API_store.insert(id); // save api request to database
					return callback(true);
				})


			} else 	return callback(false);




		} catch(e)  {

			return callback(false); }

});




	}



}

function getSocialBox(fromAPI, id, link, type, quantity, method, callback) {

	if(method) var query = {_id: id};
	else var query = {payment: id};

	var selectDB = "Order";


	mongoose.model(selectDB).findOne(query, function(err, data) {

		if(data && data.api_id && data.api_id != 0) return callback(data.api_id);
		else {
			if(data && data.paid === true && !data.verified && !data.old && !data.api_id && !data.completed) {
				sendRequest__(data.recurring,function(key, err) {
					return callback(key, err);
				});
			} else return callback(false);
		}
	});

	function selectPackage(amount) {
		var packages = {
			20: 'Simple',
			50: 'Basic',
			100: 'Popular',
			250: 'Pro',
			600: 'Premium',
			900: 'Ultra',
			1500: 'Vip',
			3000: 'Platinum'
		}
		return packages[amount];
	}
	function sendRequest__(recurring,callback) {


		var http_req = {
			regular: {
				url: 'http://api.socialsbox.com' + type,
				form:  {
				token: 'c5d9ce640fc64ca49cf7013fe7594bf8',
				link:  link,
				country: 'MIX',
				count: quantity,
				}

			},
			recurring: {
				url: 'http://api.socialsbox.com' + type,
				form:  {
				'token': 'c5d9ce640fc64ca49cf7013fe7594bf8',
				'link':  link,
				'package': selectPackage(quantity),
				'period': 'month'

				}

			}
		}

		var make_request = recurring ? http_req.recurring : http_req.regular;
		request.post(make_request, function(err, response, body) {

		logger('socialbox', id, body + ' -  ' + selectPackage(quantity) + ' - ' + err + '\r\n', function() {		})
		try {


			var data = JSON.parse(body);


			if(data && data.Id < 10) return callback(false, {error: "Invalir URL / parameters"});
			if(data && data.Id) {

				API_store.insert(id); // save api request to database
				Order.update(query, { $set: { api_id: data.Id, emoticon: type}}, function(err) {


					return callback(data.Id, err);
				});

			} else return callback(false, err);

	} catch(e)  { return callback(false, err); }

});




	}


}
function getKey2(fromAPI,order_speed,id, link, type, quantity, method, countries, url, callback) {
	if(method) var query = {_id: id};
	else var query = {payment: id};

	var selectDB = "Order";


	mongoose.model(selectDB).findOne(query, function(err, data) {

		if(data && data.api_id && data.api_id != 0) callback(data.api_id);
		else {
			if(data && data.paid === true && !data.verified && !data.old && !data.api_id && !data.completed) {
				sendRequest__(data.recurring, function(key, err) {
					if(callback) return callback(key, err);
				});
			} else return callback(false, err);
		}
	})



	function sendRequest__(recurring,callback) {
		if(type === 'youtube_views_targeted' || type === 'facebook_page_likes_targeted_g1') {
			if(countries) {
				var tmp = countries.toString();
				link = link + ' (' + tmp + ')';
			}	else link = link + ' (United States, Canada)';
		}
		if(order_speed) {
			if(type == 'facebook_page_likes_ww_slow') type = 'facebook_page_likes_ww_fast';
			if(type == 'facebook_page_likes_usa') type = 'facebook_page_likes_highspeed';
		}


		request("http://realfans.club/api.php", {
		timeout: 5000,
		qs: {
			action: 'add',
			service: type,
			amount: recurring ? 1000 : quantity,
			link: link,
			key: '54ca1eac78290c9d311ef3b8461334566393c8b842b23e18c2abcd9c7'

		}
		}, function(err, response, body) {



				logger('realfans', id, body + ' -  ' + err + ' \r\n', function() {		}) // don't really have to do anything

		try {

			var data = JSON.parse(body);

			if(data && data.status == 'fail') return callback(false, {error: data.message});
			if(data && data.order) {


				API_store.insert(id);
				Order.update(query, { $set: { api_id: data.order, emoticon: type }}, function(err) {

					if(callback) return callback(data.order);
					return;

				});

			} else return callback(false, err);

	} catch(e) { console.log(e);

	 return callback(false, err);

	}

});




	}



}

// getSmmlite


function getSmmlite(fromAPI,id, link, type, quantity, method, api_url, callback) {
	console.log("CALLED ", link);
	//return callback(false, {error: "test"});
	if(method) var query = {_id: id};
	else var query = {payment: id};

	var selectDB = "Order";

	mongoose.model(selectDB).findOne(query, function(err, data) {

		if(data && data.api_id && data.api_id != 0 && data.api_id != "false") {
			callback(data.api_id);
		}
		else {
			if(data && data.paid === true && !data.verified && !data.old && !data.completed) {

					sendRequest(link,data, function(key, err) {
						return callback(key, err);
					});
			} else {
				return callback(false);
			}
		}
	})

	function sendRequest(link,data, callback) {
	var formData = {
			key: '2b766439ce4f7207324c496843743b58',
			action: 'add',
			link: link,
			service: type,
			quantity: quantity
	};

	if(link.match('vevo') && data.service.value == "vevo_lite") {
					// convert vevo url to youtube

		request({url: link, headers: {
    		'User-Agent': 'request'
 		}}, function(err, response, body) {

 			// we have youtube id
 			if(!body || !body.match('youTubeId')) return callback(false);
 			var id = body.split('youTubeId":"')[1].split('"')[0];

 			link = 'https://www.youtube.com/watch?v='+id;
 			formData.link = link;
 			return sendPost();

		});


	}



	if(data.service.value == "spotify_plays") {
		if(quantity < 5000)  formData.type = 583;
	}
	if(data.service.value == "fb_emoticons_elite" || data.service.value == "unlimited_fb" || data.service.value == "musically_followers" || data.service.value == "musically_likes" || data.service.value == "yt_views_smlite_packages" || data.service.value == "yt_sm_target" || data.service.value == "tw_targeted") {
		formData.service = data.emoticon;

		return sendPost();
	}
	if(type == 144) {
		if(link.match(".com")) {
			var tmp = link.split(".com/")[1];
			link = "https://www.instagram.com/"+tmp;
		} else link = "https://www.instagram.com/"+link
	}
	if(type == 136 && fromAPI || type == 258 && fromAPI) {

		data.ig_comments = data.ig_comments.replace(/\/n/g,"\n");
		data.ig_comments = data.ig_comments.substr(1, data.ig_comments.length-2);
		formData.comments = data.ig_comments;
		return sendPost();
	}
	else if(data.service.value == "ig_custom" || data.service.value == "fb_comments_customelite" || type == 136 || type == 10 || type == 66 || type == 135 || type == 258 || type == 549) {
		helper.ig_custom(false,data.ig_comments, function(comments_list) {
			formData.comments = comments_list;
			return sendPost();
		});
	} else return sendPost();



	function sendPost() {

	request.post({
		url: "http://smmlite.com/api/v2",
		form:  formData

	}, function(err, response, body) {
		logger('smmlite', id,'service id ' + formData.service + ' - ' +  body + ' -  ' + err + '\r\n', function() {		})

		try {

			var data = JSON.parse(body);
			if(data && data.error) return callback(false, data);
			if(data && data.order) {


				API_store.insert(id);
				Order.update(query, { $set: { api_id: data.order, emoticon: type}}, function(err) {

					return callback(data.order, err);
				});

			} else return callback(false, err);



	} catch(e)  { return callback(false, err); }

});


}


}


}











function getLikesMania(fromAPI, id, link, type, quantity, method, url, callback) {
	console.log("called", link);
	if(method) var query = {_id: id};
	else var query = {payment: id};

	var selectDB = "Order";

	mongoose.model(selectDB).findOne(query, function(err, data) {
		if(data.service.value == "yt_sm_target" || data.service.value == "tw_targeted") {
                type = data.emoticon;
        }
        console.log(link, id, type);

		if(data && data.api_id && data.api_id != 0) callback(data.api_id);
		else {
			if(data && data.paid === true && !data.verified && !data.old &&  !data.api_id && !data.completed) {


					sendRequest(link, function(key) {
						return callback(key);
					});
			} else return callback(false);
		}
	})

	function sendRequest(link, callback) {


	request.post({
		url: "https://famearmy.com/api/v2",
		form:  {
			key: 'b0b15e356b9d4d6b5ca6739b06bdb3fc',
			action: 'add',
			link: link,
			service: type,
			quantity: quantity
		}

	}, function(err, response, body) {

		logger('likesmania', id, body + ' -  ' + err + '\r\n', function() {		})

		try {

			var data = JSON.parse(body);
			if(data && data.order) {


				API_store.insert(id);
				Order.update(query, { $set: { api_id: data.order, emoticon: type}}, function(err) {

					return callback(data.order);
				});

			} else return callback(false, err);



	} catch(e)  { return callback(false, err); }

});
}


}








function getKey(fromAPI, id, link, type, quantity, method, url, callback) {
	// return callback(false, true);
	if(method) var query = {_id: id};
	else var query = {payment: id};
	var selectDB = "Order";


	mongoose.model(selectDB).findOne(query, function(err, data) {

		if(data.api_id) console.log("SUP");
		if(data && data.api_id && data.api_id != 0) callback(data.api_id);
		else {
			if(data && data.paid === true && !data.verified && !data.old &&  !data.api_id && !data.completed) {
				// send only ig users for followers api

				if(type === 72 || type === 73) {
					if(link.match('stagram.com')) link = link.split('stagram.com/')[1];
				}
				if(type === 25) {
					if(link.match('twitter.com/')) link = link.split('twitter.com/')[1];
				}

					sendRequest(data,link, function(key, err) {
						return callback(key, err);
					});
			} else return callback(false);
		}
	})

	function sendRequest(order_data,link, callback) {
	if(type == 184 && link.match('http')) link = link.split("http://")[1];
	if(type == 182) {
		if(link.match(".com")) {
			var tmp = link.split(".com/")[1];
			link = "https://www.instagram.com/"+tmp;
		} else link = "https://www.instagram.com/"+link;
	}

	var http_req = {

				url: "https://autosmo.com/api/v2",
				form:  {
				key: '74d24e843d1ff25acc1a7ab5a07f9ec8',
				action: 'add',
				link:  link

			}
	};

	if(type == 97) {

			http_req.form.quantity = quantity;
			http_req.form.hashtag = order_data.hashtag;
			http_req.form.service = type;
			http_req.url = url;
			http_req.form.media = link;
			sendPost();

	} else if(type == 96) {

		http_req.form.quantity = quantity;
			http_req.form.username = order_data.followers;
			http_req.form.service = type;
			http_req.url = url;
			sendPost();

	} else if(type == 95) {
		  http_req.form.quantity = quantity;
			http_req.form.media = order_data.ig_media;
			http_req.form.service = type;
			http_req.url = url;
			sendPost();

	} else if(type == 97) {
		helper.ig_custom(true,order_data.ig_mentions, function(names) {
			http_req.form.usernames = names;
			http_req.form.quantity = quantity;
			http_req.form.service = type;
			http.req.form.media = link;
			http_req.url = url;
			sendPost();
		})
	} else if(type == 92) {
		http_req.form.quantity = quantity;
		http_req.form.service = type;
		http_req.url = url;
		sendPost();
	} else if(type == 3) {
			helper.ig_custom(false,order_data.ig_comments, function(comments_list) {
				http_req.form.comments = comments_list;
				http_req.form.service = type;
				return sendPost();
			});
	}
	else {
		http_req.form.service = type;
		http_req.form.quantity = quantity;
		sendPost();
	}


	function sendPost() {
		var service_list = [98,94,96,95,97,92];
		request.post(http_req, function(err, response, body) {

		logger('autosmo', id, body + ' -  ' + err + ' \r\n', function() {		})
		try {
			var data = JSON.parse(body);
			if(data) {

				if(data.error) return callback(false, data);
				if(service_list.indexOf(type) != -1) var service_type = data.order;
				else var service_type = data.order;
				API_store.insert(id);
				Order.update(query, { $set: { api_id: service_type, emoticon: type}}, function(err) {

					return callback(service_type);
				});

			} else return callback(false, data);

	} catch(e)  { return callback(false, err); }

});


	}

}


}


// smmkart




function getSmmkart(fromAPI, id, link, type, quantity, method, callback) {
	if(method) var query = {_id: id};
	else var query = {payment: id};

	var selectDB = "Order";


	mongoose.model(selectDB).findOne(query, function(err, data) {

		if(data && data.api_id && data.api_id != 0) callback(data.api_id);
		else {
			if(data && data.paid === true && !data.verified && !data.old &&  !data.api_id && !data.completed) {

				if(link.match('vevo')) {
					// convert vevo url to youtube

					request({url: link, headers: {
    				'User-Agent': 'request'
 					}}, function(err, response, body) {

 						// we have youtube id
 						 if(!body || !body.match('youTubeId')) return callback(false);
 						var id = body.split('youTubeId":"')[1].split('"')[0];

 						link = 'https://www.youtube.com/watch?v='+id;

 						sendRequest(link, function(key) {
							return callback(key, err);
						});

					});


				} else {
					sendRequest(link, function(key) {
						return callback(key);
					});
				}
			} else return callback(false);
		}
	})

	function sendRequest(link, callback) {

	request.post({
		url: 'http://reseller.smmkart.com/api/',
		form:  {
			apikey: '164430d6479258c83b909456a7802939',
			action: 'add',
			url: link,
			serviceid: type,
			qty: quantity
		}

	}, function(err, response, body) {
		logger('smmkart', id, body + ' -  ' + err + '\r\n', function() {		})
		try {

			var data = JSON.parse(body);
			if(data && data.orderid) {
				API_store.insert(id);
				Order.update(query, { $set: { api_id: data.orderid, emoticon: type}}, function(err) {

					return callback(data.orderid);
				});

			} else return callback(false, err);

	} catch(e)  { return callback(false, err); }

});
}


}
}
