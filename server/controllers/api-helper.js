exports.ig_custom = function(mentions,comment, callback) {
	try {

	var comments = JSON.parse(comment);
		var comment_list = '';
		var separator = mentions ? ' ' : '\n';
		for(var i in comments) {
			var name = comments[i].name;
			comment_list += name+separator;
		}

		
		return callback(comment_list.substr(0,comment_list.length-1));
	} catch (e) {
		return callback([]);
	}
}