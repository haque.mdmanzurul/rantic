var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var User = mongoose.model('User');
var Payment = mongoose.model('Payment');
var Service_check = mongoose.model('Service');
var Discount = mongoose.model('Discount');
var Setting = mongoose.model('Setting');
var winston = require('winston');

winston.loggers.add('order', {
    console: {
      level: 'info',
      colorize: true,
      label: 'order'
    },
    file: {
      filename: '/opt/swenzy/logs/order-errors'
    }
  });

  var logging = winston.loggers.get('order');

exports.balance = function(user_id, callback) {
	User.findOne({_id: user_id}, function(err, result) {
		if(!result) callback(false);
		else return callback(result.balance);
	});
}

exports.verify = function(order, payment,discount, req, res, callback, fromPanel) {


 Service_check.findOne({value: order.service.value}, function(err, data) {

          if(!data) {
            logging.info('Service not found, order id -  ' + order._id);
            return res.status(403).send('Invalid service.');
          }
          else {

           // we need to do things differently a little if it's unlimited service
            var unlimited = {
              20: 0,
              50: 1,
              100: 2,
              250: 3,
              600: 4,
              900: 5,
              1500: 6,
              3000: 7

            };
            /* custom for  yt_usa_new,  Youtube_views_new */
            var custom = {
              "FA": 0.5,
              "SF": 0.75
            };

            if(data.value == "yt_views_smlite_packages") {

            try {
              var drop = JSON.parse(data.dropdown);
                  data.price = parseFloat(drop[order.indexer].fixed_price);



            } catch(e)  {
              logging.info('Unable to parse service dropdown for yt_views_smlite_packages, order id -  ' + order._id);
              return res.status(403).send({error: 'attempt101', message:'Parse error.' });
            }


           }

            var real_price = ((data.price/data.multiplier)*order.orderCount).toFixed(2);
            if(order.order_tmp) {
              if(order.quantity != order.order_tmp && order.quantity != order.orderCount) {
                logging.info('Order count has been tampered, order id -  ' + order._id);
                return res.status(403).send({error: 'attempt101', message:'Action has been logged.' });
              }
              real_price = ((data.price/data.multiplier)*order.order_tmp).toFixed(2);
            } else {
              if(order.quantity != order.orderCount) {
                 logging.info('Order count has been tampered, order id -  ' + order._id);
                return res.status(403).send({error: 'attempt101', message:'Action has been logged.' });
              }
            }
	    if(data.value == "fb_postlikeslite") {
		if(order.quantity >= 25 && order.quantity <= 100) {
			if(order.price != 1.99) {
				 logging.info('Order count has been tampered, order id -  ' + order._id);
                  return res.status(403).send({error: 'attempt101', message:'Action has been logged.' });
                } else return callback(null, payment,order.price, parseInt(order.quantity) );
			}
		}
            if(data.value == "ig_likes" || data.value == "ig_usalikes") {
              if(order.quantity >= 20 && order.quantity <= 100) {
                if(order.price != 0.99)  {
                  logging.info('Order count has been tampered, order id -  ' + order._id);
                  return res.status(403).send({error: 'attempt101', message:'Action has been logged.' });
                } else return callback(null, payment,order.price, parseInt(order.quantity) );
              }

            }

            var check_discount = real_price;
            if(data.category && data.category.match('Unlimited Services')) {

              if(data.value == 'fb_likes_unlimited') data.price = data.packages[0].price;
              else data.price = data.packages[unlimited[order.orderCount]].price;

              //data.price = data.packages[0].price;
              var real_price = data.price;
              check_discount = real_price;

            }

            // calculate server price, make sure user is not cheating


            // check for discount
            if(discount && discount.services && discount.services.indexOf(order.service.value) !== -1)  {
              real_price = real_price - parseFloat(discount.price);
              real_price = real_price - (real_price * parseFloat(discount.percent));
              real_price = real_price.toFixed(2);

            }

            // speed service is + 50%
            if(order.speed_service) {
              real_price = parseFloat(real_price) + (real_price * 2);
              real_price = real_price.toFixed(2);
              check_discount = real_price;


            }
           if(order.eat_speed) {

            if(order.eat_speed == "FA") {
              real_price = parseFloat(real_price) * 0.5 + parseFloat(real_price);
            }
            if(order.eat_speed == "SF") {
              real_price = parseFloat(real_price) * 0.75 + parseFloat(real_price);
            }
           }
           if(data.value == "unlimited_fb" || data.value == "musically_followers" || data.value == "musically_likes") {

            try {
              var drop = JSON.parse(data.dropdown);
              for(var i in drop) {
                if(drop[i].api_code == order.emoticon) {
                  var real_price = parseFloat(drop[i].fixed_price);
                }
              }



            } catch(e) {

              logging.info('Unable to parse service dropdown for '+data.value+', order id -  ' + order._id)
             return res.status(403).send({error: 'attempt101', message:'Parse error.' });

            }
           }

           console.log("real price - " + Math.round(real_price) + " || order price - " + Math.round(order.price));
          if(discount && discount.min_price && check_discount < discount.min_price) {
            logging.info('Unable to apply discount for '+data.value+', order id -  ' + order._id)
             return res.status(403).send({error: 'attempt101',
              message:'This discount can only be applied if you purchase over $'+discount.min_price});

          }
          else if(Math.round(order.price) != Math.round(real_price) && !fromPanel)  {
            if(req.body.payment) {

             logging.info('Incorrect prices. Real price - '+Math.round(real_price)+' order price - '+Math.round(order.price)+ ' for '+data.value+', order id -  ' + order._id)
              // removes current payment and add_cart payment also

               Order.remove({payment: { $in: [order._id, payment.parent_id]}}, function() {

               Payment.remove({_id: { $in: [payment.parent_id, payment._id] }}, function() {
                    // remove current order also
                     return res.status(403).send({error: 'attempt101', message:'Order prices are incorrect.'});
                 })


              })
            } else  {
              logging.info('Incorrect prices. No payment from user. Real price - '+Math.round(real_price)+' order price - '+Math.round(order.price)+ ' for '+data.value+', order id -  ' + order._id)

              return res.status(403).send({error: 'attempt101', message: 'Order prices are incorrect.'});
            }



          }

          else {

              var qty = order.quantity;
              if(data.discount) {
                  try {
                      var tmp = JSON.parse(data.discount_settings);
                   } catch(e) { var tmp = {}; }
                var settings = [];

                for(var i in tmp.price) {
                  if(tmp.price[i])   settings.push({price: tmp.price[i], percentage: tmp.percentage[i] });
                }

                var range = false;

                settings.sort(function(a,b) {
                  return a.price - b.price;
                });
                for(var i in settings) {
                  if(settings[i].price) {
                    if(parseFloat(real_price) >= parseFloat(settings[i].price)) range = i;
                  }
                }

                if(range) {
                    var tmp_qty = settings[range].percentage/100;
                      qty = order.quantity*tmp_qty;
                      qty = qty + order.quantity;

                      var one_th = ['dm_views','vm_views', 'fb_views_auto','yt_views_smlite_packages'];
                      var fixed_quantity = [
                        'pt_likes','pt_repins','pt_followers','vk_likes','vk_shares','vk_subscribers','vn_revines','vn_followers','vn_likes','vn_loops',
                        'yt_dislikes','yt_usa','sc_downloads','sc_plays','dm_views','g_circles','g_ones','g_shares'
                      ]
                      if(one_th.indexOf(order.service.value) !== -1) {
                        qty = Math.round(qty/1000)*1000;

                      } else if(fixed_quantity.indexOf(order.service.value)) {
                        qty = Math.round(qty/100)*100;
                      }


                }

                  if(fromPanel) return callback(real_price, parseInt(qty));
                  else return callback(null, payment,order.price, parseInt(qty) );


              }
              else {

                if(fromPanel) return callback(real_price, parseInt(qty));
                else return callback(null, payment,order.price, parseInt(qty ));
              }
            }






      }
   });



}
