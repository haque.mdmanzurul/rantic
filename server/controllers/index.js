'use strict';

var mean = require('meanio');

exports.render = function(req, res) {
    
    var modules = [];

    // Preparing angular modules list with dependencies
    for (var name in mean.modules) {
        modules.push({
            name: name,
            module: 'mean.' + name,
            angularDependencies: mean.modules[name].angularDependencies
        });
    }

    // Send some basic starting info to the view
    res.render('index', {
        user: req.user ? JSON.stringify({
            name: req.user.name,
            _id: req.user._id,
            username: req.user.username,
            balance: (req.user ? req.user.balance : 0),
            refund_balance: ( req.user ? req.user.refund_balance : 0),
            profit: (req.user ? req.user.profit : 0),
            profits: (req.user ? req.user.profits : {}),
            discount: (req.user ? req.user.discount : 0),
            roles: (req.user ? req.user.roles : ['annonymous'])
        }) : 'null',
        modules: JSON.stringify(modules)
    });
};


exports.widget = function(req, res) {
   
    var modules = [];

    // Preparing angular modules list with dependencies
    for (var name in mean.modules) {
        modules.push({
            name: name,
            module: 'mean.' + name,
            angularDependencies: mean.modules[name].angularDependencies
        });
    }

    // Send some basic starting info to the view
    res.render('widget', {
        user: req.user ? JSON.stringify({
            name: req.user.name,
            _id: req.user._id,
            username: req.user.username,
            balance: (req.user ? req.user.balance : 0),
            profit: (req.user ? req.user.profit : 0),
            profits: (req.user ? req.user.profits : {}),
            discount: (req.user ? req.user.discount : 0),
            roles: (req.user ? req.user.roles : ['annonymous'])
        }) : 'null',
        modules: JSON.stringify(modules)
    });
};
