var async = require('async');
var _ = require('lodash');
var cronJob = require('cron').CronJob;
var kue = require('kue');
var jobs = kue.createQueue();
var debug = require('debug')('cron');
var process = require("child_process")
var spawn = process.spawn
var execFile = process.execFile
// Order model (to get orders with missing counts)
var mongoose = require('mongoose');
var Order = mongoose.model('Order');

var runCount = function(){
	console.log('goodsai!');
	async.waterfall([
		function(callback){
			Order
				.find({
					completed: false,
					cancelled: false,
					checkout: true,
					verified: false,
					refunded: false,
					url: { $ne: 'add_cart' }
					//'service.value': /^sc_likes.*/
				})
				.sort({
					created: "desc"
				})
				.exec(function(err, orders){
					callback(err, orders);
			});
		},
		function(orders, callback){
			debug('found %d orders', orders.length);
			_.each(orders, function(order){
				debug('creating job for url %s (order %s)', order.url, order._id);
				console.log("Order: " + order.url);
				jobs.create('count', {
					title: order.url,
					order: order._id,
					type: (order.beforeCount === 0 ? 'before' : null)
				}).save();
			});
		}
	]);
};

// Will run every hour
new cronJob('0 0 * * * *', function(){
    runCount();
    process.exec('killall phantomjs', function(err, stdout, stderr) {
})
}, null, true, "Asia/Jerusalem");
process.exec('killall phantomjs', function(err, stdout, stderr) {
})
runCount();
