'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

var DiscountSchema = new Schema({
  services: [{
    type: String,
    trim: true
  }],
  code: {
    type: String,
    trim: true,
    index: { unique: true }
  },
  percent: {
    type: Number,
    default: 0
  },
  price: {
    type: Number,
    default: 0
  },
  min_price: {
    type: Number,
    default: 0
  }
}, {collection: 'discounts'});

mongoose.model('Discount', DiscountSchema);
