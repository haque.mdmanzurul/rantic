'use strict';

/**
 * This is for the page where you see if and 
    how many times a particular order has been submitted by the api
 */
var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

var LogSchema = new Schema({
	 create_time: {
	 	type: Date,
	 	default: Date.now
	 },
	 
    order: {
        type: Schema.ObjectId,
        ref: 'Order'
    }
    });

mongoose.model('API_order', LogSchema);
