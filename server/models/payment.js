'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    moment = require('moment'),
    crypto = require('crypto'),
    email = require('./../services/email.js');


/**
 * Payment Schema
 */
var PaymentSchema = new Schema({
    payment_type: {
        type: Boolean,
        default: false
    },
    manual_verify: {
        type: Boolean,
        default: false
    },

    completed: {
        type: Boolean,
        default: false
    },
    needs_activation: {
        type: Boolean,
        default: false
    },
    topup: { 
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    price: {
        type: Number,
        default: 0
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    meta: {},
    billing: {},
    data: {},
    order: {
        type: Schema.ObjectId,
        ref: 'Order'
    },
    parent_id: {
        type: String

    },
    cancelled: {
        type: Boolean,
        default: false
    },

    approved: {
        type: Boolean,
        default: false
    },
    checkout: {
        type: Boolean,
        default: false
    },
    failed: {
        type: Boolean,
        default: false
    },
    fraud: {
        type: Boolean,
        default: false
    },
    paymentId: {
        type: String,
        default: ''
    },
    recurring: {
      'type': 'Boolean',
      'default': false
    }
});


/**
 * Methods
 */
PaymentSchema.methods.approve = function(callback){
    var self = this;

    if(self.approved){
      return callback(null, self);
    }

    async.waterfall([
        function(_callback){
            self
              .populate('user order', function(err, payment){
                _callback(err, payment);
              });
        },
        function(payment, _callback){
          if(payment.order){
            payment.order.checkout = true;
            payment.order.paid = true;
            payment.order.save(function(err, order){
              _callback(err, payment);
            });
          } else {
            
            if(!payment.topup)    {
                payment.user.balance = payment.user.balance + payment.price;

            payment.user.save(function(err, user){
              _callback(err, payment);
            });
            } else _callback(null, payment);
          }
        },
        function(payment, _callback){
          console.log("AI MAMMITA!");
          email.orderCreated(payment);
          payment.approved = true;
          payment.failed = false;
          payment.checkout = true;
          payment.save(_callback);
        }
    ], function(err, payment){
        callback(err, payment);
    });
};

/**
 * Statics
 */
PaymentSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

/**
 * Pre-save hook
 */
PaymentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    // console.log('user', this.user);
    this.paymentId = crypto.createHash("sha")
                         .update(this.user + moment().format('X'))
                         .digest("hex");
    next();
});

mongoose.model('Payment', PaymentSchema);
