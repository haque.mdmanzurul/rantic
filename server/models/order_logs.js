'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

/**
 * Setting Schema
 */
var LogSchema = new Schema({
	 create_time: {
	 	type: Date,
	 	default: Date.now
	 },
	 action_type: {
	 	type: String
	 }, 	
 	 user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    provider: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    price: {
        type: Number
    },
    order: {
        type: Schema.ObjectId,
        ref: 'Order'
    },
    fail: {
        type: Boolean,
        default: false
    },
    payment: {
        type: Schema.ObjectId,
        ref: 'Payment'
    }});

mongoose.model('Logs', LogSchema);
