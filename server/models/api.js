'use strict';
var async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

var Comments = new Schema({
    text      : String,
    date      : Date,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

var ApiSchema = new Schema({
    api_type: { type: String },
    emoticon: {
        type: String
    },
    eat_speed: {
        type: String
    },
    ig_mentions: {
        type: String
    },
    ig_media: {
        type: String
    },
    followers: {
        type: String
    },
     hashtag: {
        type: String
     },
     ig_comments_emoticon: {
        type: String
     },
     ig_comments: {
        type: String
     },
     transaction: {
        type: String
     },
     sale_id: {
        type: String
     },
     email_resend: {
        type: Boolean,
        default: false
     },
     last_updated: {
        type: Date,
        default: moment().add(1,'months')
     },
     payment_type: {
        type: String,
        default: ''
     },
     speed_service: {
        type: Boolean,
        default: false
     },
     verify_id: {
        type: String,
        default: ''
     },
     recurring: {
        type: Boolean,
        default: false
    },
    api_id:{
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
    url: {
        type: String,
        default: '',
        trim: true
    },
    type: {
        type: String,
        default: '',
        trim: true
    },
    old: {
	type: Boolean,
	default: false
    },
    completed: {
        type: Boolean,
        default: false
    },
    verified: {
        type: Boolean,
        default: false
    },
    cancelled: {
        type: Boolean,
        default: false
    },
    paid: {
        type: Boolean,
        default: false
    },
    checkout: {
        type: Boolean,
        default: false
    },
    reason: {
        type: String,
        default: '',
        trim: true
    },
    service: {
        label: {
            type: String,
            default: ''
        },
        value: {
            type: String,
            default: ''
        },
        multiplier: {
            type: Number,
            default: 0
        },
        price: {
            type: Number,
            default: 0
        }
    },
    discount: {
      code: {
        type: String,
        trim: true
      },
      type: {
        type: String,
        trim: true
      },
      percent: {
        type: Number,
        default: 0
      },
      price: {
        type: Number,
        default: 0
      },
      services: {}
    },
    countries: [{
        type: String,
        default: ''
    }],
    iframely: {},
    quantity: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        default: 0
    },
    orderCount: {
        type: Number,
        default: 0
    },
    beforeCount: {
        type: Number,
        default: 0
    },
    afterCount: {
        type: Number,
        default: 0
    },
     user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    provider: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    
    comments  : [Comments],
    unread : {
      customer: {
        type: Number,
        default: 0
      },
      provider: {
        type: Number,
        default: 0
      },
      administrator: {
        type: Number,
        default: 0
      }
    }
});

mongoose.model('API', ApiSchema);