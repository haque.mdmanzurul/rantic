'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

/**
 * Setting Schema
 */
var SettingSchema = new Schema({
  name: 'String',
  value: {},
  topup_price: Number,
  topup_percentage: Number
});

mongoose.model('Setting', SettingSchema);
