var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    crypto = require('crypto'),
    moment = require('moment');

 var UserSchema = new Schema({

  twitter_user: {
  	type: String,
  	default: false
  },
 	instagram_user: {
 		type: String,
 		default: false
 	},
 	date: {
 		type: Date,
 		default: Date.now
 	},
 	email: {
 		type: String
 	},
 	ig_user: {
 		type: Number
 	},
 	twitter_id: {
 		type: Number
 	}
 });

 mongoose.model('Free', UserSchema);