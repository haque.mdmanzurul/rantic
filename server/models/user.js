'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    crypto = require('crypto'),
    moment = require('moment'),
    email = require('./../services/email.js'),
    fs = require('fs');

/**
 * User Schemaa

 */
var UserSchema = new Schema({
    
    tmp_password: {
        type: String
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    deactivate_order_link: {
        type: String
    },
    validation_key: {
        type: String
    },
    not_activated: {
        type: Boolean,
        default: false 
    },
    api_discount: { type: Number },
    api_key: {
        type: String
    },
	instagram_user: {
		type: String
		
	},
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true
    },
    username: {
        type: String,
        unique: true
    },
    roles: [{
        type: String,
        default: 'authenticated'
    }],
    hashed_password: String,
    provider: String,
    resetToken: String,
    resetTokenDate: Date,
    services: [String],
    discount: {
        type: Number,
        default: 0
    },
    profit: {
        type: Number,
        default: 0.35
    },
    profits: {},
    salt: String,
    balance: {
        type: Number,
        default: 0
    },
    refund_balance: {
        type: Number,
        default: 0
    },
    facebook: {},
    twitter: {},
    github: {},
    google: {},
    linkedin: {}
});

/**
 * Virtuals
 */
UserSchema.virtual('password').set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.hashPassword(password);
}).get(function() {
    return this._password;
});

/**
 * Validations
 */
var validatePresenceOf = function(value) {
    return value && value.length;
};

// The 4 validations below only apply if you are signing up traditionally.
UserSchema.path('name').validate(function(name) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof name === 'string' && name.length > 0);
}, 'Name cannot be blank');

UserSchema.path('email').validate(function(email) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof email === 'string' && email.length > 0);
}, 'Email cannot be blank');

UserSchema.path('username').validate(function(username) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof username === 'string' && username.length > 0);
}, 'Username cannot be blank');

UserSchema.path('hashed_password').validate(function(hashed_password) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof hashed_password === 'string' && hashed_password.length > 0);
}, 'Password cannot be blank');


/**
 * Pre-save hook
 */
UserSchema.pre('save', function(next) {
    var self = this;
    
    if (!self.isNew) return next();
    // /opt/swenzy
    fs.readFile('/opt/swenzy/server/models/temp_emails.txt','utf-8', function(err, data) {
     
        if(!data) {

            return next();
        }
        var tmp = data.split("\n");
        var email = self.email;
        email = email.split("@")[1];
        
        for(var i in tmp) {
            if(tmp[i] == email) return next(new Error("Temporary")); // console.log("Failsor1")
        }

    // lock in username to be same as email

    self.username = self.email;
    self.tmp_password = self.password;

    if(_.isEmpty(self.roles)){
        self.roles = ['authenticated', 'customer'];
    }
    /* verify email
    
    .txt file, list of invalid emails, if in list, gtfo!

    */
 

    if(!self.resetToken){
        require('crypto').randomBytes(48, function(ex, buf) {
          self.resetToken = buf.toString('hex');
          self.validation_key = crypto.randomBytes(32).toString('hex');
          // Very not DRY
          if (!validatePresenceOf(self.password) && !self.provider)
              next(new Error('Invalid password'));
          else
              next();
        });
    } else {
        if (!validatePresenceOf(self.password) && !self.provider)
            next(new Error('Invalid password'));
        else
            next();
    }

    });
});

/**
 * Methods
 */
UserSchema.methods = {

    /**
     * HasRole - check if the user has required role
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
     generateApiKey: function() {
        return crypto.randomBytes(32).toString('hex');
    },
    hasRole: function(role) {
        var roles = this.roles;
        return (roles.indexOf('admin') !== -1 || roles.indexOf(role) !== -1);
    },
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function(plainText) {
        plainText = plainText.trim();
        return this.hashPassword(plainText) === this.hashed_password;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Hash password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    hashPassword: function(password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
    },

    // Regenerate reset token
    newResetToken: function(callback){
        var self = this;

        require('crypto').randomBytes(48, function(ex, buf) {
            self.resetToken = buf.toString('hex');
            self.save(function(err, user){
                callback(err, user);
            });
        });
    },

    // Is the reset token still vaild (1 week old or less)
    isResetTokenValid: function(){
        var self = this;

        if(moment(self.resetTokenDate).diff(moment()) < 60*60*24*7){
            return true;
        } else {
            return false;
        }
    },

    sendAutoCreateEmail: function(callback){
        email.userAutoCreated(this);
        return callback(null);
    },

    sendResetEmail: function(callback){
        email.resetPassword(this);
        return callback(null);
    },

    sendResetDoneEmail: function(callback){
        email.resetPasswordDone(this);
        return callback(null);
    }
};

mongoose.model('User', UserSchema);
