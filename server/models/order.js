'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    email = require('./../services/email.js');


var Comments = new Schema({
    text: String,
    date: Date,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

/**
 * Order Schema
 */
var OrderSchema = new Schema({
    indexer: {
        type: Number
    },
    refund: {
        type: Boolean,
        default: false
    },
    refunded: {
        type: Boolean,
        default: false
    },
    my_test: {
        type: Boolean,
        default: false
    },
    topup_payment: {
        type: String
    },
    topup: {
        type: Boolean,
        default: false
    },
    order_tmp: {
        type: Number,
        default: 0
    },
    manual_verify: {
        type: Boolean,
        default: false
    },
    api_type: {type: String},
    from_api: {
        type: Boolean,
        default: false
    },
    emoticon: {
        type: String
    },
    dropdown_index: {
        type: Number
    },
    eat_speed: {
        type: String
    },
    ig_mentions: {
        type: String
    },
    ig_media: {
        type: String
    },
    followers: {
        type: String
    },
    hashtag: {
        type: String
    },
    ig_comments_emoticon: {
        type: String
    },
    ig_comments: {
        type: String
    },
    transaction: {
        type: String
    },
    sale_id: {
        type: String
    },
    email_resend: {
        type: Boolean,
        default: false
    },
    last_updated: {
        type: Date,
        default: moment().add(1, 'months')
    },
    payment_type: {
        type: String,
        default: ''
    },
    speed_service: {
        type: Boolean,
        default: false
    },
    verify_id: {
        type: String,
        default: ''
    },
    recurring: {
        type: Boolean,
        default: false
    },
    api_id: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
    url: {
        type: String,
        default: '',
        trim: true
    },
    type: {
        type: String,
        default: '',
        trim: true
    },
    old: {
        type: Boolean,
        default: false
    },
    completed: {
        type: Boolean,
        default: false
    },
    verified: {
        type: Boolean,
        default: false
    },
    cancelled: {
        type: Boolean,
        default: false
    },
    paid: {
        type: Boolean,
        default: false
    },
    checkout: {
        type: Boolean,
        default: false
    },
    reason: {
        type: String,
        default: '',
        trim: true
    },
    service: {
        label: {
            type: String,
            default: ''
        },
        value: {
            type: String,
            default: ''
        },
        multiplier: {
            type: Number,
            default: 0
        },
        price: {
            type: Number,
            default: 0
        }
    },
    discount: {
        code: {
            type: String,
            trim: true
        },
        type: {
            type: String,
            trim: true
        },
        percent: {
            type: Number,
            default: 0
        },
        price: {
            type: Number,
            default: 0
        },
        services: {}
    },
    countries: [{
        type: String,
        default: ''
    }],
    iframely: {},
    quantity: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        default: 0
    },
    orderCount: {
        type: Number,
        default: 0
    },
    beforeCount: {
        type: Number,
        default: 0
    },
    afterCount: {
        type: Number,
        default: 0
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    provider: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    payment: {
        type: Schema.ObjectId,
        ref: 'Payment'
    },
    comments: [Comments],
    unread: {
        customer: {
            type: Number,
            default: 0
        },
        provider: {
            type: Number,
            default: 0
        },
        administrator: {
            type: Number,
            default: 0
        }
    }
});

OrderSchema.virtual('status').set(function (status) {
}).get(function () {
    /*
        <span ng-if="!order.paid && !order.checkout" class='order-status-cancelled'>
            Abandoned Checkout
        </span>
        <span ng-if="!order.paid && order.checkout" class='order-status-verifying'>
            Authorizing Payment
        </span>
        <span ng-if="order.paid && order.completed && !order.cancelled && !order.verified" class='order-status-verifying'>
            Verifying
        </span>
        <span ng-if="order.paid && order.completed && !order.cancelled && order.verified" class='order-status-completed'>
            Completed
        </span>
        <span ng-if="order.paid && order.cancelled" class='order-status-cancelled'>
            Cancelled
        </span>
        <span ng-if="order.paid && !order.completed && !order.cancelled && !order.provider" class='order-status-queued'>
            Queued
        </span>
        <span ng-if="order.paid && !order.completed && !order.cancelled && order.provider" class='order-status-progress'>
            In Progress
        </span>
    */

    if (!this.paid && !this.checkout) {
        return 0;
    } else if (!this.paid) {
        return 1;
    } else if (this.paid && !this.completed && !this.cancelled && !this.provider) {
        return 2;
    } else if (this.paid && !this.completed && !this.cancelled && this.provider) {
        return 3;
    } else if (this.paid && this.completed && !this.cancelled && !this.verified) {
        return 4;
    } else if (this.paid && this.completed && !this.cancelled && this.verified) {
        return 5;
    } else if (this.paid && this.cancelled) {
        return 6;
    } else {
        return -1;
    }
});

/**
 * Validations
 */
OrderSchema.path('url').validate(function (url) {
    return url.length;
}, 'Url cannot be blank');

/**
 * Methods
 */





OrderSchema.methods.complete = function (req, res, callback) {
    var self = this;

    async.waterfall([
        function (_callback) {
            self.populate({path: 'provider'}, function (err, order) {
                _callback(err, order);
            });
        },
        function (order, _callback) {
            if (req) {
                if (!req.user.hasRole('admin')) {
                    if (!order.provider) {
                        return _callback(new Error('You can only complete an order once it has been claimed by you'));
                    }
                    if (req.user._id.toString() != order.provider._id.toString()) {
                        return _callback(new Error('You can only complete your own orders'));
                    }
                }
            }

            order.completed = true;
            order.verified = true;
            order.my_test = true;
            if (order.afterCount >= order.beforeCount + order.orderCount && order.beforeCount >= 0) {


                if (order.provider) {
                    if (!_.isEmpty(order.provider.profits[order.service.value])) {
                        order.provider.balance = order.provider.balance + (order.price * order.provider.profits[order.service.value]);
                    } else {
                        order.provider.balance = order.provider.balance + (order.price * order.provider.profit);
                    }

                    order.provider.save(function (err, provider) {
                        _callback(err, order);
                    });
                } else {
                    _callback(null, order);
                }
            } else {
                //  order.verified = false;
                _callback(null, order);
            }
        },
        function (order, _callback) {

            order.save(_callback);
        }
    ], function (err, order) {
        callback(err, order);
    });
};

OrderSchema.methods.approve = function (req, res, callback) {
    var self = this;

    async.waterfall([
        function (_callback) {
            self.populate({path: 'provider'}, function (err, order) {
                _callback(err, order);
            })
        },
        function (order, _callback) {
            if (!req.user.hasRole('admin')) {
                return _callback(new Error('Only an administrator can approve orders as verified'));
            }

            if (!order.verified) {
                order.verified = true;

                if (order.provider) {
                    if (!_.isEmpty(order.provider.profits[order.service.value])) {
                        order.provider.balance = order.provider.balance + (order.price * order.provider.profits[order.service.value]);
                    } else {
                        order.provider.balance = order.provider.balance + (order.price * order.provider.profit);
                    }

                    order.provider.save(function (err, provider) {
                        _callback(err, order);
                    });
                } else {
                    _callback(null, order);
                }
            } else {
                _callback(null, order);
            }
        },
        function (order, _callback) {
            order.save(_callback);
        }
    ], function (err, order) {
        callback(err, order);
    });
};

OrderSchema.methods.unapprove = function (req, res, callback) {
    var self = this;

    async.waterfall([
        function (_callback) {
            self.populate({path: 'provider'}, function (err, order) {
                _callback(err, order);
            })
        },
        function (order, _callback) {
            if (!req.user.hasRole('admin')) {
                return _callback(new Error('Only an administrator can approve orders as verified'));
            }

            order.verified = false;
            order.completed = false;
            _callback(null, order);
        },
        function (order, _callback) {
            order.save(_callback);
        }
    ], function (err, order) {
        callback(err, order);
    });
};

/**
 * Statics
 */
OrderSchema.statics.load = function (id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').populate('provider', 'name username profit').exec(cb);
};

mongoose.model('Order', OrderSchema);

/**
 * Pre-save hook
 */
OrderSchema.pre('save', function (next) {
    var self = this;
    if (!self.isNew) return next();

    async.series([
        function (callback) {
            self.populate('user', callback);
        },
        function (callback) {

            if (self.price == 0) return callback();
            email.orderCreated(self);
            callback();
        }
    ], function (err) {
        next();
    });
});
