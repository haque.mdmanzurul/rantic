'use strict';


var async = require('async'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;


var ServiceSchema = new Schema({
  service: {
    type: String
  },
  price: {
    type: String
  },
  percentage: {
    type: String
  }

});
  

mongoose.model('Servicediscount', ServiceSchema);
