'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;
  
var ServiceSchema = new Schema({
  
  api_name: {
    type: String
  },
  api_url: {
    type: String
  }

});


mongoose.model('Apidata', ServiceSchema);
