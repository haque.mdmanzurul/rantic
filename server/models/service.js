'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  _ = require('lodash');

var Packages = new Schema({
  price: 'Number',
  quantity: 'Number',
  label: 'String'
});

/**
 * Service Schema
 */
var ServiceSchema = new Schema({
  admin_priority: {
    type: Number,
    default: 0
  },
  maximum: {
    type: Number
  },
  min_quantity: {
    type: Number
  },
  fixed_price: {
    type: Number
  },
  dropdown: {
    type: String
  },
  discount_id: {
    type: String
  },
  discount: {
    type: Boolean,
    default: false
  },
  discount_settings: {
    type: String
  },
  qty_below: {
    type: Number
  },
  qty_below_api: {
    type: Number
  },
  api_nr: { type: String },
  api_price: { type: Number},
  api_type: { type: String },
  api_code: { type: String },
  api_url: { type: String },
  description: 'String',
  pattern: 'String',
  label: 'String',
  value: 'String',
  category: 'String',
  multiplier: 'Number',
  price: 'Number',
  minimum: 'Number',
  targeted: 'Boolean',
  disabled: 'Boolean',
  recurring: {
    'type': 'Boolean',
    'default': false
  },
  faster_speed: {
    type: Boolean,
    default: false
  },
  countries: [{
      type: String,
      default: ''
  }],
  packages: [Packages]
});

mongoose.model('Service', ServiceSchema);
