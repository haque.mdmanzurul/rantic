'use strict';

/**
 * Module dependencies.
 */
var async = require('async'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('lodash');

/**
 * Notification Schema
 */
var NotificationSchema = new Schema({}, { strict: false });

mongoose.model('Notification', NotificationSchema);
