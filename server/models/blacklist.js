'use strict';
var async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var BlacklistSchema = new Schema({
    string: { type: String },
    created: {
        type: Date,
        default: Date.now
    }
  
});

mongoose.model('Blacklist', BlacklistSchema);