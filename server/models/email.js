'use strict';
var async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var EmailSchema = new Schema({
    email: { type: String },
    created: {
        type: Date,
        default: Date.now
    }
  
});

mongoose.model('Email', EmailSchema);