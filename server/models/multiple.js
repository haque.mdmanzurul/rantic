'use strict';
var async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;

  

var RotationSchema = new Schema({
    order_id: { type: String },
    total: { type: Number },
    next_api: { type: Boolean },
    next_api_nr: { type: Number},
    created: {
        type: Date,
        default: Date.now
    }

});

mongoose.model('Api_rotation', RotationSchema);
