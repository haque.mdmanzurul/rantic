'use strict'

module.exports = sc_followers;

function sc_followers(options, callback) {
  var soundcloud = require('./../apis/soundcloud.js')(options);
  soundcloud.followers(options.order.url, function(err, followers){
    callback(err, followers);
  });
}
