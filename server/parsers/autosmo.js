'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;
var smmliteLoader = require('./smmlite.js');


module.exports = smo;

function smo(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(!data) callback(null, null);

			
			else if(data && !data.api_id && data.api_id != 0) {   API(data._id,'order', function(key) {
				callback(null, null)
			});
			}
			
			else {


			

			var api_key = data.api_id;
			request.post({
		url: 'https://autosmo.com/api/v2',
		form:  {
			key: '74d24e843d1ff25acc1a7ab5a07f9ec8',
			action: 'status',
			id: data.api_id,
		}

	}, function(err, response, body) {


		

		if(body) {
			try {
				var data = JSON.parse(body);
					if(data && data.start_count) {
				
				if(data.status == "Canceled") {
					Order.update({_id: options.order._id}, { $set: { refund: true, refunded: false }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else if(data.remains == 0 || data.status == "Completed") {
					 Order.update({_id: options.order._id}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else {
					var finalCount = 0; //parseInt(options.order.beforeCount)+(parseInt(options.order.orderCount)-parseInt(data.remains));
					callback(null, finalCount);
				}


			} else  callback(null, null);
			} catch(e) {
				callback(null, null);
			}
			
			
		
		} else callback(null, null);
	});
		
			}
		})

	}


	function autosmo(key) {
		/* if less than 10k do smmlite, otherwise continue 

	1.) include smmlite at the top

	2.) check quantity

	3.) call method

	*/
	//if(options.order.service.value === "ig_usalikes" && options.order.quantity < 10000) {
	//	return smmliteLoader(options, callback);

	//}
	 var key = key ? key : options.order.api_id;
	 if(key) {

	 	
	 	request.post({
		url: 'https://autosmo.com/api/v2',
		form:  {
			key: '74d24e843d1ff25acc1a7ab5a07f9ec8',
			action: 'status',
			id: key
		}

	}, function(err, response, body) {
		

		
			try {
				var data = JSON.parse(body);
				if(data && data.start_count ) {
				if(data.status == "Canceled") {
					Order.update({_id: options.order._id}, { $set: { refund: true, refunded: false }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else if(data.remains == 0 || data.status == "Completed") {
					 Order.update({_id: options.order._id}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else {
					var finalCount = 0; //parseInt(options.order.beforeCount)+(options.order.orderCount-data.remains);
					callback(null, finalCount);
				}


			} else autosmo2();
			} catch(e) {
				callback(null, null);
			}
			
			
		
	});

	 	

	 } else autosmo2();
	


}
	autosmo();

	
}
