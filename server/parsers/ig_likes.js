'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;



var count_shares = require('count-shares');
var ig = require('instagram-node').instagram();

ig.use({ access_token: config.instagram.accessToken });
ig.use({ client_id: config.instagram.clientID,
         client_secret: config.instagram.clientSecret });


module.exports = smo;

function smo(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(err) console.log('pass');
			else if(!data.api_id) {   API(data._id,'order', function(key) {
				autosmo(key);
			});
			}
			
			else {

				request('http://autosmo.com/api.php', {
					qs: {
					action: 'status',
					order: data.api_id,
					key: 'a7cd92ad7edd26b877aa6bcc8946cab1'
			
				}				
			}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			
			if(data && data.start_count && data.count) {
				
				
				if(!options.order.beforeCount)	callback(null, data.start_count);
				else {
					var finalCount = parseInt(options.order.beforeCount)+(parseInt(options.order.orderCount)-parseInt(data.count));
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});

			}
		})

	}


	function autosmo(key) {
	
	if(key) var key = key;
	else var key = options.order.api_id
	request.post({
		url: 'http://autosmo.com/api.php',
		form:  {
			key: 'a7cd92ad7edd26b877aa6bcc8946cab1',
			action: 'status',
			id: key
		}

	}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			if(data && data.start_count && data.count) {
				if(!options.order.beforeCount)	callback(null, data.start_count);
				else {
					var finalCount = parseInt(options.order.beforeCount)+(options.order.orderCount-data.count);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});


}
	if(options.order.api_id) autosmo();
	else if(!options.order.api_id && options.order.beforeCount > 0) {

		if(options.order.url.match('taken-by')) options.order.url = options.order.url.split('/?taken-by')[0];
	
	var regex = /(https?:\/\/)?(www)?instagram\.com\/p\/(.*?)\/?$/i;
	var match = options.order.url.match(regex);
	var id = null;

	if(match){
		id = match[3];
	} else {
		return callback(new Error('Cannot extract post id from URL'));
	}

	async.waterfall([
		function(_callback){
			request(
				{
				  timeout: 5000,
				  url: 'http://api.instagram.com/oembed?url=http://instagr.am/p/' + id,
				  json: true,
				  headers: {
				    'User-Agent': random_ua.generate()
				  }
				},
			 	function(err, response, json){
			 		if(json && json.media_id) _callback(err, json.media_id);
			 		else _callback(err, null);
			 	}
			 );
		},
		function(media_id, _callback){
			ig.media(media_id, function(err, result, limit) {
				if(!result){
					return _callback(null, 0);
				} else if(!result.likes){
					return _callback(null, 0);
				}

				_callback(err, result.likes.count);
			});
		}
	], function(err, count){
		callback(err, count);
	});



	}
	else setTimeout(function() { autosmo(); }, 15000);
	
}