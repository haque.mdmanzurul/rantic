'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;



module.exports = socialbox;

function socialbox(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(err) console.log('pass');
			else if(data && !data.api_id) {   API(data._id,'order', function(key) {
				autosmo(key);
			});
			}
			
			else {

				request('http://api.socialsbox.com/v2/service/getorderinfo', {
					qs: {
					orderid: data.api_id,
					token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
				}					
			}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			
			if(data && data.Start && data.Current) {
				
				
				if(!options.order.beforeCount)	callback(null, data.Start);
				else {
					var finalCount = parseInt(data.Current);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});

			}
		})

	}


	function autosmo(key) {
	
	if(key) var key = key;
	else var key = options.order.api_id
	request('http://api.socialsbox.com/v2/service/getorderinfo', {
		qs: {
			orderid: key,
			token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
		}		

	}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			if(data && data.Start && data.Current) {
				if(!options.order.beforeCount)	callback(null, data.Start);
				else {
					var finalCount = parseInt(data.Current);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});


}
	if(options.order.api_id) autosmo();
	else if(!options.order.api_id && options.order.beforeCount > 0) {



  var youtube = require('./../apis/youtube_api3.js')(options);

  youtube.dislikes(options.order.url, function(err, dislikes){
    callback(err, dislikes);
  });




	}
	else setTimeout(function() { autosmo(); }, 15000);
	
}





