'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;



module.exports = socialbox;

function socialbox(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(!data) callback(null, null);
			else if(data && !data.api_id) {   API(data._id,'order', function(key) {
				callback(null, null);
			});
			}
			
			else {

				request('http://api.socialsbox.com/v2/service/getorderinfo', {
					qs: {
					orderid: data.api_id,
					token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
				}					
			}, function(err, response, body) {
		

		if(body) {
			try {
				var data = JSON.parse(body);
				if(data && data.Start && data.Current) {
				
				
				if(!options.order.beforeCount)	callback(null, data.Start);
				else {
					var finalCount = parseInt(data.Current);
					callback(null, finalCount);
				}


			} else  callback(null, null);

			} catch(e) {
				callback(null, null);
			}
			
			
			
		}
	});

			}
		})

	}


	function autosmo(key) {
	
	var key = key ? key : options.order.api_id;
	if(key) {
	request('http://api.socialsbox.com/v2/service/getorderinfo', {
		qs: {
			orderid: key,
			token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
		}		

	}, function(err, response, body) {
		

		
			try {
				var data = JSON.parse(body);
				if(data && data.Start && data.Current) {
				if(!options.order.beforeCount)	callback(null, data.Start);
				else {
					var finalCount = parseInt(data.Current);
					callback(null, finalCount);
				}


			} else autosmo2();
			} catch(e) {
				callback(null, null);
			}
			
			
		
	});
} else autosmo2();

}
	autosmo();

	
}