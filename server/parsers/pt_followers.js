'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;



module.exports = socialbox;

function socialbox(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(err) console.log('pass');
			else if(data && !data.api_id) {   API(data._id,'order', function(key) {
				autosmo(key);
			});
			}
			
			else {

				request('http://api.socialsbox.com/v2/service/getorderinfo', {
					qs: {
					orderid: data.api_id,
					token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
				}					
			}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			
			if(data && data.Start && data.Current) {
				
				
				if(!options.order.beforeCount)	callback(null, data.Start);
				else {
					var finalCount = parseInt(data.Current);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});

			}
		})

	}


	function autosmo(key) {
	
	if(key) var key = key;
	else var key = options.order.api_id
	request('http://api.socialsbox.com/v2/service/getorderinfo', {
		qs: {
			orderid: key,
			token: 'c5d9ce640fc64ca49cf7013fe7594bf8'
			
		}		

	}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			if(data && data.Start && data.Current) {
				if(!options.order.beforeCount)	callback(null, data.Start);
				else {
					var finalCount = parseInt(data.Current);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});


}
	if(options.order.api_id) autosmo();
	else if(!options.order.api_id && options.order.beforeCount > 0) {

		try{
		var username = options.order.url.split('pinterest.com/')[1];
		// remove all the crap from the end
		if(username.match('/')) username = username.split('/')[0];
		
 		request('https://api.pinterest.com/v1/users/'+username+'/?access_token=AS_ELP4CVZp3SRgUyl4KG4-m1W2DFAupgDdItmJCi3MQLEAn2wAAAAA&fields=counts', function(err, resp, data) {
 			var data = JSON.parse(data);
 			
 			if(data && data.data && data.data.counts && data.data.counts.followers) {
 			var followers = data.data.counts.followers ? data.data.counts.followers : 0;
 			 callback(null, followers);
 			} else callback(null, null);
 		});
 		


	} catch(e){
                callback(null, 0);
        }



	}

	else setTimeout(function() { autosmo(); }, 15000);
	
}















