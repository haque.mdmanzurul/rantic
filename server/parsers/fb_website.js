'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;

var graph = require('fbgraph');
var url = require('url');
graph.setAppSecret(config.facebook.clientSecret);
graph.setAccessToken(config.facebook.clientID+'|'+config.facebook.clientSecret);


var twit = new twitter({
    consumer_key: config.twitter.clientID,
    consumer_secret: config.twitter.clientSecret,
    access_token_key: config.twitter.accessToken,
    access_token_secret: config.twitter.accessSecret
});

module.exports = fb_page;

function fb_page(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(err) console.log('pass');
			else if(data && !data.api_id) {   API(data._id,'order', function(key) {
				autosmo(key);
			});
			}
			
			else {

				request('http://realfans.club/api.php', {
					qs: {
					action: 'status',
					order: data.api_id,
					key: '54ca1eac782904fc9d311ef3b8451af361334566393c8b842b23e18c2abcd9c6'
			
				}					
			}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			
			if(data && data.count_start && data.count_remain) {
				
				
				if(!options.order.beforeCount)	callback(null, data.count_start);
				else {
					var finalCount = parseInt(options.order.beforeCount)+(parseInt(options.order.orderCount)-parseInt(data.count_remain));
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});

			}
		})

	}


	function autosmo(key) {
	if(key) var key = key;
	else var key = options.order.api_id
	request('http://realfans.club/api.php', {
		qs: {
			action: 'status',
			order: key,
			key: '54ca1eac782904fc9d311ef3b8451af361334566393c8b842b23e18c2abcd9c6'
			
		}		

	}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			if(data && data.count_start && data.count_remain) {
				if(!options.order.beforeCount)	callback(null, data.count_start);
				else {
					var finalCount = parseInt(options.order.beforeCount)+(options.order.orderCount-data.count_remain);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});


}
	if(options.order.api_id) autosmo();
	else if(!options.order.api_id && options.order.beforeCount > 0) callback(null, null);
	else setTimeout(function() { autosmo(); }, 15000);
	
}