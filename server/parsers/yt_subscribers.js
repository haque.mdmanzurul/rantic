'use strict'

var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');

module.exports = yt_subscribers;

function yt_subscribers(options, callback) {
  var youtube = require('./../apis/youtube_api3.js')(options);
 

  youtube.subscribers(options.order.url, function(err, subscribers){
  
  	console.log(subscribers);
    callback(err, subscribers);
  });
}
