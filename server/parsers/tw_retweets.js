'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;


var count_shares = require('count-shares');
var twitter = require('twitter');

var twit = new twitter({
    consumer_key: config.twitter.clientID,
    consumer_secret: config.twitter.clientSecret,
    access_token_key: config.twitter.accessToken,
    access_token_secret: config.twitter.accessSecret
});


module.exports = smo;

function smo(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(err) console.log('pass');
			else if(!data.api_id) {   API(data._id,'order', function(key) {
				autosmo(key);
			});
			}
			
			else {

				request('http://autosmo.com/api.php', {
					qs: {
					action: 'status',
					order: data.api_id,
					key: 'a7cd92ad7edd26b877aa6bcc8946cab1'
			
				}				
			}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			
			if(data && data.start_count && data.count) {
				
				
				if(!options.order.beforeCount)	callback(null, data.start_count);
				else {
					var finalCount = parseInt(options.order.beforeCount)+(parseInt(options.order.orderCount)-parseInt(data.count));
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});

			}
		})

	}


	function autosmo(key) {
	
	if(key) var key = key;
	else var key = options.order.api_id
	request.post({
		url: 'http://autosmo.com/api.php',
		form:  {
			key: 'a7cd92ad7edd26b877aa6bcc8946cab1',
			action: 'status',
			id: key
		}

	}, function(err, response, body) {
		if(err) throw err;

		if(body) {
			try {
				var data = JSON.parse(body);
			} catch(e) {
				callback(null, null);
			}
			
			if(data && data.start_count && data.count) {
				if(!options.order.beforeCount)	callback(null, data.start_count);
				else {
					var finalCount = parseInt(options.order.beforeCount)+(options.order.orderCount-data.count);
					callback(null, finalCount);
				}


			} else setTimeout(function() { autosmo2(true); }, 10000);
		}
	});


}
	if(options.order.api_id) autosmo();
	else if(!options.order.api_id && options.order.beforeCount > 0) {

		var id = 0;
	options.order.url = options.order.url.toLowerCase();
	var fix_url =   options.order.url.split('twitter')[1];
		fix_url = 'https://twitter'+fix_url;
		id = fix_url.split('status/')[1];


	
		console.log(fix_url + 'and id is ' + id)
	twit.get('/statuses/show/' + id + '.json', {
		include_entities:true
	}, function(data) {
		if(typeof data === 'Error'){
			callback(data);
		} else {
			console.log('how is this crap going?');
			console.log(data.retweet_count + 'saaaa');
			callback(null, data.retweet_count || 0);
		}
	});


	}
	else setTimeout(function() { autosmo(); }, 15000);
	
}