'use strict'

var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var ig = require('instagram-node').instagram();
var config = require('./../config/config.js');

ig.use({ access_token: config.instagram.accessToken });
ig.use({ client_id: config.instagram.clientID,
         client_secret: config.instagram.clientSecret });

module.exports = ig_comments;

function ig_comments(options, callback) {
	if(options.order.url.match('taken-by')) options.order.url = options.order.url.split('/?taken-by')[0];
//	options.order.url = options.order.url.toLowerCase();
	var regex = /(https?:\/\/)?(www)?instagram\.com\/p\/(.*?)\/?$/i;
	var fix_url =   options.order.url.split('instagram')[1];
		fix_url = 'https://instagram'+fix_url;
		
	var match = fix_url.match(regex);
	var id = null;
	console.log(fix_url + 'id is ' + match[3]);
	if(match){
		id = match[3]; //.toLowerCase();
	} else {
		return callback(new Error('Cannot extract post id from URL'));
	}

	async.waterfall([
		function(_callback){
			request(
				{
				  timeout: 5000,
				  url: 'http://api.instagram.com/oembed?url=http://instagr.am/p/' + id,
				  json: true,
				  headers: {
				    'User-Agent': random_ua.generate()
				  }
				},
			 	function(err, response, json){
			 		if(json && json.media_id) return _callback(err, json.media_id);
			 		else return _callback(err, null);
			 	}
			 );
		},
		function(media_id, _callback){
			ig.media(media_id, function(err, result, limit) {
				if(!result){
					return _callback(null, 0);
				} else if(!result.comments){
					return _callback(null, 0);
				}

				_callback(err, result.comments.count);
			});
		}
	], function(err, count){
		callback(err, count);
	});
}
