'use strict'

var request = require('request');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');

var config = require('./../config/config.js');
var url = require('url');


module.exports = rn_plays;

function rn_plays(options, callback) {

	request(options.order.url, function(err, resp, body) {
	
	if(body) { 

	
	try {

		var data = body.split('text-green smaller')[1];
            data = data.split('</li>')[0];
            data = data.split('</span>')[1];
            data = data.trim();
            // removing comma
            if(data.match(',')) { data = data.split(','); data = data[0]+''+data[1]; }
            console.log('sending back ' + data);
            callback(null, data);


	
}catch(err) { callback(null, null); }


} else callback(null, null);
});

}