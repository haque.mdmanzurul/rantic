'use strict'

var request = require('request');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var graph = require('fbgraph');
var config = require('./../config/config.js');

graph.setAppSecret(config.facebook.clientSecret);
graph.setAccessToken(config.facebook.clientID+'|'+config.facebook.clientSecret);

module.exports = fb_comment;

function fb_comment(options, callback) {
	var regex = /^http(?:s?):\/\/(?:www\.)?facebook\.com.*?(v|fbid|posts)[=\/](\d+).*?comment_id=([0-9]*)($|&)/i;
	var match = options.order.url.match(regex);
	var comment = null;

	if(match){
		comment = match[2] + '_' + match[3];
	} else {
		console.log('unable to extract comment')
		return callback(new Error('Cannot extract comment id from URL'));
	}

	graph.get(comment + '/likes?summary=1', function(err, response) {
		if(err){
			return callback(err);
		}
		console.log('got total' + response.summary.total_count);
		callback(err, response.summary.total_count);
	});
}