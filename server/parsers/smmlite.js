'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;
var MULTIPLE = require("./../controllers/multiple_api.js");
var famearmy = require('./famearmy.js');

var fs = require('fs-extra');
var logger = function(service, order_id, error, cb) {
		var message = new Date() + ' -  ' + service + ' - Order Id:  ' + order_id + ' - Error msg: ' + error;
		fs.appendFile('/opt/swenzy/logs/' + service, message, function() {
			return cb();
		});
}


module.exports = smo;

function smo(options, callback) {

	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(!data) callback(null, null);
			if(data && !data.api_id) {   API(data._id,'order', function(key) {
				callback(null, null);
			});
			}

			else {

			var api_key = data.api_id;

			request.post({
		url: 'http://smmlite.com/api/v2',
		form:  {
			key: '2b766439ce4f7207324c496843743b58',
			action: 'status',
			order: data.api_id,
		}

	}, function(err, response, body) {




		if(body) {
			try {
				var data = JSON.parse(body);
				logger('smmlite-status', api_key, data.status, function() { });
				if(data && data.start_count) {

				if(data.status == "Canceled") {
					Order.update({_id: options.order._id}, { $set: { refund: true, refunded: false }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else if(data.status == "Partial") {
					MULTIPLE.check(options.order, function(progess) {
						if(progress) return callback(null, null);
						Order.update({_id: options.order._id}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
								 callback(null, null);
						 })
					}, data.remaining);
				}
				else if(data.status == "Completed") {
					MULTIPLE.check(options.order, function(progress) {
						if(progress) return callback(null, null);
						else {
							Order.update({_id: options.order._id}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
								 callback(null, null);
						 	});
					 	}
					})

				}
				else {
					var finalCount = 0;//parseInt(options.order.beforeCount)+(parseInt(options.order.orderCount)-parseInt(data.remains));
					callback(null, finalCount);
				}


			} else  callback(null, null);

			} catch(e) {
				callback(null, null);
			}



		} else callback(null, null);
	});



			}
		})

	}


	function autosmo(key) {
		//if(options.order.service.value === "yt_subselite" && !options.order.api_id && options.order.quantity < 5000) {
		//}
	//if(options.order.service.value === "yt_subselite" && !options.order.api_id && options.order.quantity < 5000) {
	//	return famearmy(options, callback);

//	}
	var key = key ? key : options.order.api_id;
	if(key) {

			request.post({
		url: 'http://smmlite.com/api/v2',
		form:  {
			key: '2b766439ce4f7207324c496843743b58',
			action: 'status',
			order: key
		}

	}, function(err, response, body) {
		if(err) callback(null, null)

		if(body)
			try {
				var data = JSON.parse(body);

				logger('smmlite-status', key, data.status, function() { });
					if(data && data.start_count) {

				if(data.status == "Canceled") {
					Order.update({_id: options.order._id}, { $set: { refund: true, refunded: false }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else if(data.status == "Partial") {
					MULTIPLE.check(options.order, function(progress) {
						if(progress) return callback(null, null);
						Order.update({_id: options.order._id}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
								 callback(null, null);
						 })
					}, data.remaining);
				}
				else if(data.status == "Completed") {
					MULTIPLE.check(options.order, function(progress) {
						if(progress) return callback(null, null);
						else {
					 Order.update({_id: options.order._id}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
      					callback(null, null);
    				})
					}
				})
			}
				else {
					var finalCount = 0; //parseInt(options.order.beforeCount)+(options.order.orderCount-data.remains);
					callback(null, finalCount);
				}


			} else autosmo2();
			} catch(e) {
				callback(null, null);
			}


		});



	} else autosmo2();

	}

 autosmo();


}
