'use strict'
var mongoose = require('mongoose');
var Order = mongoose.model('Order');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var random_ua = require('random-ua');
var _ = require('lodash');
var async = require('async');
var urllib = require('url');
var util = require('util');
var twitter = require('twitter');
var config = require('./../config/config.js');
var API = require('./../controllers/api.js').API;


module.exports = smo;

function smo(options, callback) {
	
	function autosmo2(multiple) {
		if(multiple) callback(null, null);
		Order.findOne({_id: options.order._id}, function(err, data) {
			if(!data) callback(null, null);
			if(data && !data.api_id) {   API(data._id,'order', function(key) {
				callback(null, null);
			});
			}
			
			else {
			var api_key = data.api_id;
			request.post({
		url: 'https://famearmy.com/api/v2',
		form:  {
			key: 'b0b15e356b9d4d6b5ca6739b06bdb3fc',
			action: 'status',
			id: data.api_id,
		}

	}, function(err, response, body) {


		

		if(body) {
			try {
				var data = JSON.parse(body);
				if(data && data.start_count) {
				
				
				if(!options.order.beforeCount)	callback(null, data.start_count);
				else if(data.status == "Canceled") {
					Order.update({_id: options.order._id}, { $set: { refund: true, refunded: false }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else if(data.status == "Completed") {
					 Order.update({api_id: api_key}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else {
					var finalCount = 0; //parseInt(options.order.beforeCount)+(parseInt(options.order.orderCount)-parseInt(data.remains));
					callback(null, finalCount);
				}


			} else  callback(null, null);

			} catch(e) {
				callback(null, null);
			}
			
			
			
		} else callback(null, null);
	});
	
			}
		})

	}


	function autosmo(key) {
	
	var key = key ? key : options.order.api_id;
	if(key) {
		
			request.post({
		url: 'https://famearmy.com/api/v2',
		form:  {
			key: 'b0b15e356b9d4d6b5ca6739b06bdb3fc',
			action: 'status',
			id: key
		}

	}, function(err, response, body) {
		if(err) callback(null, null)

		if(body) 
			try {
				var data = JSON.parse(body);
					if(data && data.start_count) {
				if(!options.order.beforeCount)	callback(null, data.start_count);
				else if(data.status == "Canceled") {
					Order.update({_id: options.order._id}, { $set: { refund: true, refunded: false }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else if(data.status == "Completed") {
					 Order.update({api_id: key}, { $set: { completed: true, verified: true, my_test: true }}, function(err, resp) {
      					callback(null, null);
    				})
				}
				else {
					var finalCount = 0; //parseInt(options.order.beforeCount)+(options.order.orderCount-data.remains);
					callback(null, finalCount);
				}


			} else autosmo2();
			} catch(e) {
				callback(null, null);
			}
			
		
		
		});

	} else autosmo2();

	}

 autosmo();


}
	
	
