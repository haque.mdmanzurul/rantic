'use strict'

module.exports = sc_likes;

function sc_likes(options, callback) {
  var soundcloud = require('./../apis/soundcloud.js')(options);
  soundcloud.likes(options.order.url, function(err, likes){
    callback(err, likes);
  });
}
